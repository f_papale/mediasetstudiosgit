package com.crismasecurity.mediasetStudios.web;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;


//@ManagedBean(name= "languageBean") // JSF managed
//@SessionScoped // JSF managed
//@ViewScoped // JSF managed

//Spring managed
@Controller ("languageBean") 
@Scope("session")
//@Scope("view")
public class LanguageBean implements Serializable{

	private static final long serialVersionUID = -7941035963910235077L;

	private String localeCode;
	private Locale currentLocale;

	private static Map<String,Object> countries;
	static{
		countries = new LinkedHashMap<String,Object>();
		countries.put("English", Locale.ENGLISH); //label, value
		countries.put("Italiano", Locale.ITALIAN);
	}

	public LanguageBean() {
		System.out.println("!!!! Costruttore di LanguageBean !!!");
		
    }

	@PostConstruct
	public void init() {
		System.out.println("INIT LanguageBean");
		
		currentLocale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
		
		localeCode = currentLocale.getLanguage();
	}
	
	
	
	public Map<String, Object> getCountriesInMap() {
		return countries;
	}


	public String getLocaleCode() {
		return localeCode;
	}


	public void setLocaleCode(String localeCode) {
		this.localeCode = localeCode;
	}

	//value change event listener
	public void countryLocaleCodeChanged(ValueChangeEvent e){

		String newLocaleValue = e.getNewValue().toString();

		//loop country map to compare the locale code
		for (Map.Entry<String, Object> entry : countries.entrySet()) {
			if(entry.getValue().toString().equals(newLocaleValue)){
				System.out.println("Imposto language: " + newLocaleValue);
        		FacesContext.getCurrentInstance().getViewRoot().setLocale((Locale)entry.getValue());
        		setCurrentLocale((Locale)entry.getValue());
        	  }
		}
	}

	public Locale getCurrentLocale() {
		return currentLocale;
	}

	public void setCurrentLocale(Locale currentLocale) {
		this.currentLocale = currentLocale;
	}

}
