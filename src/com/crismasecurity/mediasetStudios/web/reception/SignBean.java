package com.crismasecurity.mediasetStudios.web.reception;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import com.crismasecurity.mediasetStudios.core.entity.Location;
import com.crismasecurity.mediasetStudios.core.entity.SpectatorEpisode;
import com.crismasecurity.mediasetStudios.core.services.LocationService;
import com.crismasecurity.mediasetStudios.core.services.SignListener;
import com.crismasecurity.mediasetStudios.core.services.SignService;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;
import com.crismasecurity.mediasetStudios.web.utils.jsf.FacesUtils;



//@ManagedBean(name= EmployeesBean.BEAN_NAME) // JSF managed
//@SessionScoped // JSF managed
//@ViewScoped // JSF managed

//Spring managed
@Controller (SignBean.BEAN_NAME) 
//@Scope("session")
@Scope("view")

public class SignBean extends BaseBean {


	private static final long serialVersionUID = 534706599202360395L;
	
	public static final String BEAN_NAME = "signBean";
    public String getBeanName() { return BEAN_NAME; } 
    @Autowired
    private LocationService locationService; 
    @Autowired
    private SignService signService; 
    private Long selectedDesk=null;
	private List<SelectItem> desks;
	private String signValue;
	
    
	public SignBean() {
		getLog().info("!!!! Costruttore di SignBean !!!");
    }

	@PostConstruct
	public void init() {
		getLog().info("INIT SignBean!");
		
		String locationIdString = FacesUtils.getCookieValue("MEDIASET_STUDIOS_SELECTED_LOCATION");
		getLog().info("Cookie - location salvata: " + locationIdString);
		
		desks = new ArrayList<SelectItem>();
		desks.add(new SelectItem(null, " "));
//		List<CheckinDesk> desklist = receptionService.getCheckinDesks();
		List<Location> desklist = locationService.getLocationFinder().and("locationType.id").eq(2l).setDistinct().list();
//		for(CheckinDesk sptype : desklist) {
//			SelectItem xx = new SelectItem(sptype.getId(), sptype.getName() + (sptype.getProductionCenter()!=null?(" (" + sptype.getProductionCenter().getName()+")"):""));
//			desks.add(xx);
//		}
		for(Location sptype : desklist) {
			SelectItem xx = new SelectItem(sptype.getId(), sptype.getDescription() + (sptype.getProductionCenter()!=null?(" (" + sptype.getProductionCenter().getName()+")"):""));
			desks.add(xx);
		}		
		
		if (locationIdString!=null && !locationIdString.equals("")) {
			Location loc = locationService.getLocationById(Long.parseLong(locationIdString));
			if (loc!=null) {
				getLog().info("Selezionato automaticamente location:" + loc);
				selectedDesk = loc.getId();
//				deskChanged();
			}
			else {
				getLog().info("Nessuna location trovata nel cookie");
			}
		}
	}

	@PreDestroy
	public void destroy() {
		getLog().info("Destroy SignBean!");
		
	}
	
	public void deskChanged() {
		if (selectedDesk!=null) FacesUtils.setCookie("MEDIASET_STUDIOS_SELECTED_LOCATION", selectedDesk.toString(), 60*60*24*30); // un mese
	}
	
	public void resetDesk() {
		if (selectedDesk!=null) 
			FacesUtils.setCookie("MEDIASET_STUDIOS_SELECTED_LOCATION", "", 0); //Reset
		selectedDesk=null;
	}	
	
	public boolean haveUserRole(String roles) {
		
		String[] listRoles = roles.split(",");
		for (int i = 0; i < listRoles.length; i++) {
			listRoles[i] = listRoles[i].trim();
			
		}
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication!=null && authentication.getAuthorities()!=null) {
			//String currentPrincipalName = authentication.getName();
			for (GrantedAuthority r : authentication.getAuthorities()) {
				//System.out.println("Ruolo:"+r.getAuthority());
				for (int i = 0; i < listRoles.length; i++) {
					if (r.getAuthority()!=null && r.getAuthority().equals(listRoles[i])) {
						//System.out.println("trovato ruolo !!!");
						return true;
					}					
				}
			}
		}
		return false;
	}	
	
	
	public void sendSign(ActionEvent e) {
		
		if (selectedDesk==null) {
			addWarningMessage("Select desk!");
			return;
		}
		
		Location desk = locationService.getLocationById(selectedDesk);
		//CheckinDesk desk = receptionService.getCheckinDeskById(selectedDesk);
		signService.signReceived(desk, signValue);
		signValue=new String("");
	}
	
//	public synchronized SpectatorEpisode getSelectedSpectator() {
//		List<SignListener> lis = signService.getSignListeners();
//		if (lis!=null) {
//			for (SignListener li : lis) {
//				//getLog().info("Listener - desk:" + (li.getDesk()!=null?li.getDesk().getId():"null") + "    spettatore:" + (li.getSelectedSpectator()!=null && li.getSelectedSpectator().getSpectator()!=null?li.getSelectedSpectator().getSpectator().getFullName():"null") + "   listener:" + li);
//				if (li.getDesk()!=null && li.getDesk().getId().equals(selectedDesk) && li.getSelectedSpectator()!=null){
//					//getLog().info("Listener trovato.... vedo spettatore selezionato...:" + (li.getSelectedSpectator()!=null && li.getSelectedSpectator().getSpectator()!=null?li.getSelectedSpectator().getSpectator().getFullName():"null"));
//					return li.getSelectedSpectator();
//				}
//			}
//		}
//		return null;
//	}
	
	public SpectatorEpisode getSelectedSpectator() {
		List<SignListener> lis = signService.getSignListeners();
		if (lis!=null) {
			for (SignListener li : lis) {
				//getLog().info("Listener - desk:" + (li.getDesk()!=null?li.getDesk().getId():"null") + "    spettatore:" + (li.getSelectedSpectator()!=null && li.getSelectedSpectator().getSpectator()!=null?li.getSelectedSpectator().getSpectator().getFullName():"null") + "   listener:" + li);
				if (li.getDesk()!=null && li.getDesk().getId().equals(selectedDesk) && li.getSelectedSpectator()!=null){
					//getLog().info("Listener trovato.... vedo spettatore selezionato...:" + (li.getSelectedSpectator()!=null && li.getSelectedSpectator().getSpectator()!=null?li.getSelectedSpectator().getSpectator().getFullName():"null"));
					return li.getSelectedSpectator();
				}
		
			}
		}
		return null;
	}	
	
	public String getSignValue() {
		return signValue;
	}

	public void setSignValue(String signValue) {
		this.signValue = signValue;
	}

	public List<SelectItem> getDesks() {
		return desks;
	}

	public void setDesks(List<SelectItem> desks) {
		this.desks = desks;
	}

	public Long getSelectedDesk() {
		return selectedDesk;
	}

	public void setSelectedDesk(Long selectedDesk) {
		this.selectedDesk = selectedDesk;
	}
	
}
