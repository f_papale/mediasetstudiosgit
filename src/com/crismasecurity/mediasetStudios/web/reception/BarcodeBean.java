package com.crismasecurity.mediasetStudios.web.reception;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Hashtable;
import java.util.Map;
import java.util.Vector;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.imageio.ImageIO;

import org.primefaces.event.CaptureEvent;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.LuminanceSource;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeReader;

/**
 *
 * @author r
 */
@ManagedBean(name = "barcodeBean")
@SessionScoped
public class BarcodeBean implements Serializable {

	private static final long serialVersionUID = -4956604465450406749L;
	
	private String resultText;
	
	
	private String cameraWidth="360";
	private String cameraHeight="540";
	private String cameraRotation="portrait";
	
	
	private Hashtable<DecodeHintType, Object> hints;

	

	
    /**
     * Creates a new instance of PhotoController
     */
    public BarcodeBean() {
    	
    	Vector formats = new Vector<>();
        formats.add(BarcodeFormat.QR_CODE);                
        hints = new Hashtable<DecodeHintType, Object>();
    	hints.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);
    	hints.put(DecodeHintType.POSSIBLE_FORMATS, formats);
    	hints.put(DecodeHintType.PURE_BARCODE, Boolean.FALSE);
    	
    }
    
    
    public void changeCameraOrientation(ActionEvent e) {
    	Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
    	String orientation = params.get("orientation");
    	
    	System.out.println("Orientation changed to:" + orientation);
    	
    	if (orientation!=null && orientation.equals("landscape")) {
    		cameraWidth="540";
    		cameraHeight="360";
    		cameraRotation="landscape";
    	}
    	else if (orientation!=null && orientation.equals("portrait")) {
    		cameraWidth="360";
    		cameraHeight="540";
    		cameraRotation="portrait";
    	}
    }
    

    public void oncapture(CaptureEvent captureEvent) {
    	
    	InputStream in = null;
    	BufferedImage image = null;
        try {
        	//System.out.println("oncapture ... rowdata:" + captureEvent.getRawData());
        	System.out.println("oncapture ... ");
            if (captureEvent != null) {

            	
                           
                in = new ByteArrayInputStream(captureEvent.getData());
                image = ImageIO.read(in);

                
                String re =qrDecodeFromImage(image);
                if(re == null)
                {
                    System.out.println("Image does not contain any barcode");
                    setResultText("");
                }
                else
                {
                    System.out.println(re);
                    setResultText("text:" + re);
                } 
               
                
              
                
                
//              Result result = null;
            	
            	
//          	InputStream ina = new ByteArrayInputStream(captureEvent.getData());
//          	FileUtils.writeToFile("C:\\Users\\Crisma\\Desktop\\iii\\" + System.currentTimeMillis()+".png", ina);
//          	ina.close();
//          	
//          	PlanarYUVLuminanceSource source = new PlanarYUVLuminanceSource(captureEvent.getData(), 1024, 768, 0, 0, 1024,768, false);
//            BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
          	
                
                
                
//             
//            	LuminanceSource source = new BufferedImageLuminanceSource(image);
//            	BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
//            	com.google.zxing.Reader reader = new MultiFormatReader();
//            	MultipleBarcodeReader bcReader = new GenericMultipleBarcodeReader(reader);
//            	StringBuilder sb = new StringBuilder();
//            	for (Result result : bcReader.decodeMultiple(bitmap, hints)) {
//            		sb.append(result.getText()).append(" ");
//            	}
//            	if (sb.toString().length()>0) {
//            		System.out.println("\n\nTROVATO; " + sb.toString() + "\n\n\n");
//            	}
//            	setResultText(sb.toString());
            	
            	
                
                

                
//                BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(
//                        new BufferedImageLuminanceSource(image)));
//                    Result dResult = new MultiFormatReader().decode(binaryBitmap,
//                    		hints);
                
                
                
//                BufferedImageLuminanceSource source = new BufferedImageLuminanceSource(image);
//                HybridBinarizer hb = new HybridBinarizer(source);
////                BitMatrix bm = hb.getBlackMatrix();
////                MultiDetector detector = new MultiDetector(bm);
////                DetectorResult dResult = detector.detect();
//                BinaryBitmap bitmap = new BinaryBitmap(hb);
//
//                Reader reader = new QRCodeReader();
//                Result dResult = reader.decode(bitmap);
//                if(dResult == null)
//                {
//                    System.out.println("Image does not contain any barcode");
//                }
//                else
//                {
////                    BitMatrix QRImageData = dResult.getBits();
////                    Decoder decoder = new Decoder();
////                    DecoderResult decoderResult = decoder.decode(QRImageData);
////                    String QRString = decoderResult.getText();
////                    System.out.println(QRString);
////                    setResultText("text:" + QRString);
//                    
//                    System.out.println(dResult.getText());
//                    setResultText("text:" + dResult.getText());
//                } 
                
                
                
                
//                System.out.println("Image before: height:" + image.getHeight() + "   weight:" + image.getWidth());
//                
//                int w = image.getWidth();
//                int h = image.getHeight();
//                BufferedImage after = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
//                AffineTransform at = new AffineTransform();
//                at.scale(3.0, 3.0);
//                AffineTransformOp scaleOp = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
//                after = scaleOp.filter(image, after);
//                System.out.println("Image after: height:" + after.getHeight() + "   weight:" + after.getWidth());

//                LuminanceSource source = new BufferedImageLuminanceSource(image);
//                BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
//
//                result = new MultiFormatReader().decode(bitmap);
//                if (result != null) {
//                    setResultText("Format:" + result.getBarcodeFormat().name() + "    rext:" + result.getText());
//                }
            }
        } catch (Throwable ex) {
            ex.printStackTrace();
            try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
    }
    
 
    /**
     * @return the resultText
     */
    public String getResultText() {
        return resultText;
    }

    /**
     * @param resultText the resultText to set
     */
    public void setResultText(String resultText) {
        this.resultText = resultText;
    }
    
    
    
    public String qrDecodeFromImage(BufferedImage img) {
        if(img!=null) {
            LuminanceSource bfImgLuminanceSource = new BufferedImageLuminanceSource(img);
            BinaryBitmap binaryBmp = new BinaryBitmap(new HybridBinarizer(bfImgLuminanceSource));
            
            QRCodeReader qrReader = new QRCodeReader();
            Result result;
            try {
                result = qrReader.decode(binaryBmp);
                return result.getText();
            } catch (Throwable e) {e.printStackTrace();} 
        }
        return null;
    }
    
    
    
    
    
  


	public String getCameraWidth() {
		return cameraWidth;
	}


	public void setCameraWidth(String cameraWidth) {
		this.cameraWidth = cameraWidth;
	}


	public String getCameraHeight() {
		return cameraHeight;
	}


	public void setCameraHeight(String cameraHeight) {
		this.cameraHeight = cameraHeight;
	}


	public String getCameraRotation() {
		return cameraRotation;
	}


	public void setCameraRotation(String cameraRotation) {
		this.cameraRotation = cameraRotation;
	}


	


    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
