package com.crismasecurity.mediasetStudios.web.reception;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.omnifaces.util.Ajax;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CaptureEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleSelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.ByteArrayContent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.chart.DonutChartModel;
import org.primefaces.push.EventBus;
import org.primefaces.push.EventBusFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import com.crismasecurity.mediasetStudios.core.entity.Audit;
import com.crismasecurity.mediasetStudios.core.entity.DocumentSpectator;
import com.crismasecurity.mediasetStudios.core.entity.DocumentType;
import com.crismasecurity.mediasetStudios.core.entity.Episode;
import com.crismasecurity.mediasetStudios.core.entity.Liberatoria;
import com.crismasecurity.mediasetStudios.core.entity.Location;
import com.crismasecurity.mediasetStudios.core.entity.Media;
import com.crismasecurity.mediasetStudios.core.entity.MediaStatusType;
import com.crismasecurity.mediasetStudios.core.entity.Spectator;
import com.crismasecurity.mediasetStudios.core.entity.SpectatorEpisode;
import com.crismasecurity.mediasetStudios.core.entity.SpectatorType;
import com.crismasecurity.mediasetStudios.core.security.services.AppUserService;
import com.crismasecurity.mediasetStudios.core.security.vos.security.AppUserVO;
import com.crismasecurity.mediasetStudios.core.services.AuditService;
import com.crismasecurity.mediasetStudios.core.services.DocumentSpectatorService;
import com.crismasecurity.mediasetStudios.core.services.DocumentTypeService;
import com.crismasecurity.mediasetStudios.core.services.EpisodeService;
import com.crismasecurity.mediasetStudios.core.services.LocationService;
import com.crismasecurity.mediasetStudios.core.services.MediaService;
import com.crismasecurity.mediasetStudios.core.services.MediaStatusTypeService;
import com.crismasecurity.mediasetStudios.core.services.ReceptionService;
import com.crismasecurity.mediasetStudios.core.services.SignListener;
import com.crismasecurity.mediasetStudios.core.services.SignService;
import com.crismasecurity.mediasetStudios.core.services.SpectatorEpisodeService;
import com.crismasecurity.mediasetStudios.core.services.SpectatorService;
import com.crismasecurity.mediasetStudios.core.services.SpectatorTypeService;
import com.crismasecurity.mediasetStudios.core.utils.DateUtils;
import com.crismasecurity.mediasetStudios.core.utils.FileUtils;
import com.crismasecurity.mediasetStudios.core.utils.PropertiesUtils;
import com.crismasecurity.mediasetStudios.core.utils.ObjectDiff.ObjectDiff;
import com.crismasecurity.mediasetStudios.web.NavigationBean;
import com.crismasecurity.mediasetStudios.web.management.LazySpectatorDataModel;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;
import com.crismasecurity.mediasetStudios.web.utils.jsf.FacesUtils;
import com.crismasecurity.mediasetStudios.web.utils.reporting.JasperReportDetails;
import com.crismasecurity.mediasetStudios.web.utils.reporting.JasperReportsFactory;
import com.crismasecurity.mediasetStudios.webservices.AntennaTransitsListener;
import com.crismasecurity.mediasetStudios.webservices.MediasetStudiosWebServices;
import com.crismasecurity.mediasetStudios.webservices.desktopReader.DesktopReaderListener;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.LuminanceSource;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeReader;



//@ManagedBean(name= EmployeesBean.BEAN_NAME) // JSF managed
//@SessionScoped // JSF managed
//@ViewScoped // JSF managed

//Spring managed
@Controller (ReceptionBean.BEAN_NAME) 
//@Scope("session")
@Scope("view")
public class ReceptionBean extends BaseBean implements SignListener, DesktopReaderListener, AntennaTransitsListener {


	private static final long serialVersionUID = 534706599202360395L;



	public static final String BEAN_NAME = "receptionBean";
    public String getBeanName() { return BEAN_NAME; } 


    @Autowired
    private SpectatorEpisodeService spectatorEpisodeService;
    @Autowired
    private LocationService locationService; 
    @Autowired
    private MediasetStudiosWebServices mediasetStudiosWebServices; 
    @Autowired
	private MediaService mediaService;
    @Autowired
	private MediaStatusTypeService mediaStatusTypeService;
    @Autowired
    private EpisodeService episodeService;
    @Autowired
    private SpectatorService spectatorService; 
    @Autowired
    private SpectatorTypeService spectatorTypeService; 
    @Autowired
    private SignService signService; 
    @Autowired
    private ReceptionService receptionService;
    @Autowired
	private DocumentSpectatorService documentSpectatorService;

	@Autowired
	private DocumentTypeService documentTypeService;
    
    @Autowired
    private JasperReportsFactory jasperReportsFactory;

    
	@Autowired
	private AuditService auditService;
	    
    @Autowired
    private NavigationBean navigationBean;

	@Autowired
	private AppUserService userService;
    
    private LazySpectatorDataModel lazyModelSpectator;
    private Date filterFrom;
	private Date filterTo;
	
    private String spectatorGuid;
    
    private boolean scannerVisible;
    
	private SpectatorEpisode spectatorToEdit = null;
	private List<SpectatorEpisode> spectators = null;
	private List<SpectatorEpisode> selectedSpectators = null;
	private Long selectedSpectatorType=null;
	private List<SelectItem> spectatorTypes;

	
	// BarcodeReader
	private final static String CAMERA_WIDTH = "1024"; // 540
	private final static String CAMERA_HEIGHT = "1024"; // 360
	
	private String cameraWidth=CAMERA_WIDTH;
	private String cameraHeight=CAMERA_HEIGHT;
	private String cameraRotation="portrait";
    
	
//	private String testAutoupdateStr;
	
	
	private Long selectedDesk=null;
	private List<SelectItem> desks;
	
	private String sessionId = null;
	

	private DonutChartModel checkinChartModel;
	private DonutChartModel checkinChartModel2;
	private String chartTitle = "";
	private String chartTitle2 = "";
	private String chartSubTitle = "";
	private String chartSubTitle2 = "";
	
	private Long selectedEpisode=null;
	
	private long episodeSpectatorsNums = 0;
	private long episodeSpectatorsNumsWithCheckin = 0;
	private long episodeSpectatorsNumsWithCheckout = 0;
	
	private List<SelectItem> episodes;
	
	private WatchDirForScanThread scanMonitoringThread = null;
	

	private StreamedContent fileLiberatoria;
	
	
	private Date episodeDate;
	private List<Episode> episodesList;
	
	private boolean allOrPrecheckinOnly; // true=all
	private boolean allOrCheckinOnly; // true=all
	private int validityDays;
	private AppUserVO userSession;
	private ObjectDiff oDiff; 
	
//    auditService.addAudit(new Audit(uu.getLastName() + " " + uu.getFirstName()+" ("+navigationBean.getUserName()+")", "Checkin", "SpectatorEpisode", spToDel.getId(), "", "", oldHtml, null, oDiff.getDiff(Spectator.copy(spToDel), null)));
	/**
     * 
     */
	public ReceptionBean() {
		getLog().info("!!!! Costruttore di ReceptionBean !!!");
//		
		scannerVisible = false;
		episodeDate = new Date();
		allOrPrecheckinOnly=false;
		allOrCheckinOnly=false;
    }

	@PostConstruct
	public void init() {
		getLog().info("INIT ReceptionBean!");
		
		userSession = userService.getUserByLogin(navigationBean.getUserName());
		oDiff = new ObjectDiff();
		validityDays = Integer.parseInt(PropertiesUtils.getInstance().readProperty("liberatorie.validitydays","180"));
		
		signService.addSignListener(this);
		mediasetStudiosWebServices.addDesktopReaderListener(this);
		mediasetStudiosWebServices.addAntennaTransitsListener(this);
		
		
		lazyModelSpectator = new LazySpectatorDataModel(spectatorService);
		
		spectatorTypes = new ArrayList<SelectItem>();
		List<SpectatorType> sptypelist = spectatorTypeService.getSpectatorType();
//		getLog().info(sptypelist.size());
		spectatorTypes.add(new SelectItem(null, " "));
		for(SpectatorType sptype : sptypelist) {
			SelectItem xx = new SelectItem(sptype.getId(), sptype.getDescription());
			spectatorTypes.add(xx);
		}

		
		String locationIdString = FacesUtils.getCookieValue("MEDIASET_STUDIOS_SELECTED_LOCATION");
		getLog().info("Cookie - location salvata: " + locationIdString);
		desks = new ArrayList<SelectItem>();
		desks.add(new SelectItem(null, " "));
		
		List<Location> desklist = locationService.getLocationFinder().and("locationType.id").eq(2l).setDistinct().list();
		for(Location sptype : desklist) {
			SelectItem xx = new SelectItem(sptype.getId(), sptype.getDescription() + (sptype.getProductionCenter()!=null?(" (" + sptype.getProductionCenter().getName()+")"):""));
			desks.add(xx);
		}
		if (locationIdString!=null && !locationIdString.equals("")) {
			Location loc = locationService.getLocationById(Long.parseLong(locationIdString));
			if (loc!=null) {
				getLog().info("Selezionato automaticamente location:" + loc);
				selectedDesk = loc.getId();
				deskChanged();
			}
			else {
				getLog().info("Nessuna location trovata nel cookie");
			}
		}
		
		//List<CheckinDesk> desklist = receptionService.getCheckinDesks();
//		for(CheckinDesk sptype : desklist) {
//			SelectItem xx = new SelectItem(sptype.getId(), sptype.getName() + (sptype.getProductionCenter()!=null?(" (" + sptype.getProductionCenter().getName()+")"):""));
//			desks.add(xx);
//		}
				
		checkinChartModel=presetCheckinChartModel();
		setCheckinChartModel2(presetCheckinChartModel());
		
		// Prendo la sessionId
		FacesContext fCtx = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) fCtx.getExternalContext().getSession(false);
		sessionId = session.getId();
//		setFocusOnCheckin();
		getLog().info("sessionId:" + sessionId);
	}

	@PreDestroy
	public void destroy() {
		getLog().info("Destroy ReceptionBean!"); 
		
		signService.removeSignListener(this);
		mediasetStudiosWebServices.removeDesktopReaderListener(this);
		mediasetStudiosWebServices.removeAntennaTransitsListener(this);
	}
	
	private void doDestroy() {
//		destroy();	
	}
	
	private Date lastHeartbeat = null;
	
	
	private DonutChartModel presetCheckinChartModel () {
		DonutChartModel chartModel = new DonutChartModel();
		chartModel.setTitle(null);
//		chartModel.setLegendPosition("c");
//		chartModel.setLegendPlacement(LegendPlacement.INSIDE);
		chartModel.setShowDataLabels(false);
		chartModel.setShowDatatip(false);
		chartModel.setShadow(false);
		chartModel.setMouseoverHighlight(false);
		chartModel.setSeriesColors("f7ba01,00a2ff");	
		return chartModel;
	}
	
	
	public void heartbeat() {
		lastHeartbeat = new Date();
		
		// Aggiornamento statistiche a tempo ...per chi non partecipa alle operazioni di checkin/checkout
		if (selectedEpisode!=null) {
			episodeSpectatorsNums = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(selectedEpisode).setDistinct().count();
			episodeSpectatorsNumsWithCheckin = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(selectedEpisode).and("checkin").eq(Boolean.TRUE).setDistinct().count();
			episodeSpectatorsNumsWithCheckout = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(selectedEpisode).and("checkout").eq(Boolean.TRUE).setDistinct().count();
			
			updateGraph();
		}
	}
	
	private List<SpectatorEpisode> orderedSpectators(List<SpectatorEpisode> spectators) {
	Comparator<SpectatorEpisode> forFullSurname = (n1, n2) -> n1.getSpectator().getFullSurname().compareTo(n2.getSpectator().getFullSurname());
	List<SpectatorEpisode> res = spectators.stream()
			.sorted(forFullSurname)
			.collect(Collectors.toList());
	res.size(); // avoid lazy
	int indexCounter=0;
	for (SpectatorEpisode se : res) {
		indexCounter++;
		se.setOrder(indexCounter);
	}
	return res ; 
	}
	
	
	public void refreshList() {
		getLog().info("refreshList...");
		//Carico gli spettatori
		if (!allOrPrecheckinOnly) { // solo precheckin
			getLog().info("solo Precheckin...");
			spectators = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(selectedEpisode).and("preCheckin").eq(Boolean.TRUE).setDistinct().list();
			//spectators.size(); // avoid lazy
			spectators=orderedSpectators(spectators);
			updateGraph();
		}
		else {
			// tutti
			getLog().info("tutti...");
			spectators = episodeService.getEpisodeById(selectedEpisode).getSpectators();
			//spectators.size(); // avoid lazy
			spectators=orderedSpectators(spectators);
			updateGraph();
		}
	}
	
	public void unlockAllBadge() {
		getLog().info("unlockAllEpisodeBadge...");
		List<SpectatorEpisode> lse = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(selectedEpisode).and("media.id").isNotNull().setDistinct().list();
		if(!lse.isEmpty()) {
			for (SpectatorEpisode sp : lse) {
				Media mediaToEdit = new Media();
				mediaToEdit=sp.getMedia();
				sp.setMedia(null);
				try {
					spectatorEpisodeService.updateSpectatorEpisode(sp);
					MediaStatusType mst = mediaStatusTypeService.getMediaStatusTypeFinder().and("idStatus").eq(new Integer(1)).setDistinct().list().get(0);
					mediaToEdit.setMediaStatusType(mst);
					mediaService.updateMedia(mediaToEdit);									
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	
	public void allOrPrecheckinOnlyChanged() {
		getLog().info("allOrPrecheckinOnlyChanged...");
		//Carico gli spettatori
		if (!allOrPrecheckinOnly) { // solo precheckin
			getLog().info("solo Precheckin...");
			spectators = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(selectedEpisode).and("preCheckin").eq(Boolean.TRUE).setDistinct().list();
			//spectators.size(); // avoid lazy
			spectators=orderedSpectators(spectators);
			updateGraph();
		}
		else {
			// tutti
			getLog().info("tutti...");
			spectators = episodeService.getEpisodeById(selectedEpisode).getSpectators();
			//spectators.size(); // avoid lazy
			spectators=orderedSpectators(spectators);
			updateGraph();
		}
	}
	
	public void allOrCheckinOnlyChanged() {
		getLog().info("allOrCheckinOnlyChanged...");
		//Carico gli spettatori
		if (!allOrCheckinOnly) { // solo checkin
			getLog().info("solo checkin...");
			spectators = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(selectedEpisode).and("checkin").eq(Boolean.TRUE).setDistinct().list();
			//spectators.size(); // avoid lazy
			spectators=orderedSpectators(spectators);
			updateGraph();
		}
		else {
			// tutti
			getLog().info("tutti...");
			spectators = episodeService.getEpisodeById(selectedEpisode).getSpectators();
			spectators.size(); // avoid lazy
			spectators=orderedSpectators(spectators);
			updateGraph();
		}
	}
	
	

	public void episodeDateChanged() {
		getLog().info("episodeDateChanged:" + episodeDate + "   selectedDesk:" + selectedDesk);
		
//		Calendar cal = Calendar.getInstance();
//		cal.setTime(episodeDate);
//		cal.set(Calendar.HOUR, 23);
//		cal.set(Calendar.MINUTE, 59);
//		cal.set(Calendar.SECOND, 59);
//		Date from = cal.getTime();
//		cal.setTime(episodeDate);
//		cal.set(Calendar.HOUR, 0);
//		cal.set(Calendar.MINUTE, 0);
//		cal.set(Calendar.SECOND, 1);
//		Date toDate = cal.getTime();
//		episodesList = episodeService.getEpisodeFinder().and("studio.productionCenter.locations.id").eq(selectedDesk)/*.and("episodeStatus.idStatus").eq(4)*/.and("dateFrom").le(from).and("dateTo").ge(toDate).setDistinct().list();
//		getLog().info("episodesList size:" + episodesList.size());
		Calendar cal = Calendar.getInstance();
		cal.setTime(episodeDate);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		Date toDate = cal.getTime();
		cal.setTime(episodeDate);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 1);
		Date from = cal.getTime();
		
//		System.out.println("1 - from:" + from + "   to:" + toDate);
		episodesList = episodeService.getEpisodeFinder().and("studio.productionCenter.locations.id").eq(selectedDesk)/*.and("episodeStatus.idStatus").eq(4)*/.and("dateFrom").ge(from).and("dateTo").le(toDate).setDistinct().list();
		getLog().info("episodesList size:" + episodesList.size());
		selectedEpisode = null;
		epSel=null;
		
		episodeSpectatorsNums = 0;
		episodeSpectatorsNumsWithCheckin = 0;
		episodeSpectatorsNumsWithCheckout=0;
		
	}
	
	private Long selectEpisodeConfirmId;
	public void selectEpisode(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("episodeId")) {
    			selectedEpisode = (Long)((UIParameter)com).getValue();
    			epSel=null;
    			
    			Episode sp = episodeService.getEpisodeById(selectedEpisode);
    			if (!sp.isCheckinStarted() && !sp.isCheckoutStarted()) {
    				// Chiedo conferma perchè non è partito ne il chechin ne il checkout
    				selectEpisodeConfirmId = selectedEpisode;
    				selectedEpisode = null;
    				epSel=null;
    				
    				return;
    			}
    			else selectEpisodeConfirmId=null;
    			
    			selectEpisodeConfirm(e);
    			
//    			episodeSpectatorsNums = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(selectedEpisode).setDistinct().count();
//    			episodeSpectatorsNumsWithCheckin = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(selectedEpisode).and("checkin").eq(Boolean.TRUE).setDistinct().count();
//    			episodeSpectatorsNumsWithCheckout = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(selectedEpisode).and("checkout").eq(Boolean.TRUE).setDistinct().count();
//    			
//    			episodeChanged();
    			break;
			}
		}
	}
	public void selectEpisodeConfirm (ActionEvent e) {
		getLog().info("selectEpisodeConfirm....");
		if (selectEpisodeConfirmId!=null) {
			selectedEpisode = selectEpisodeConfirmId;
			selectEpisodeConfirmId=null;
			epSel=null;
		}
		
//		episodeSpectatorsNums = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(selectedEpisode).setDistinct().count();
//		episodeSpectatorsNumsWithCheckin = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(selectedEpisode).and("checkin").eq(Boolean.TRUE).setDistinct().count();
//		episodeSpectatorsNumsWithCheckout = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(selectedEpisode).and("checkout").eq(Boolean.TRUE).setDistinct().count();
		
		episodeChanged();
		RequestContext.getCurrentInstance().execute("PF('spectatorTableWidget').filter();");
	}
	public void selectEpisodeConfirmCancel (ActionEvent e) {
		getLog().info("selectEpisodeConfirmCancel....");
		selectEpisodeConfirmId=null;
	}
	
	
	private Long selectEpisodeCheckinReopenConfirmId;
	public void startCheckinEpisode(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("episodeIda")) {
    			getLog().info("START CHECKIN !!!!!!!!!!!!!!!!!!");
    			selectedEpisode = (Long)((UIParameter)com).getValue();
    			getLog().info("START CHECKIN !!!!!!!!!!!!!!!!!! for episode id " + selectedEpisode);
    			Episode ep = episodeService.getEpisodeById(selectedEpisode);
    			epSel=null;
    			
    			if (ep.isCheckoutStarted()) {
    				// Chiedo conferma se tornare al checkin!!!!
    				selectEpisodeCheckinReopenConfirmId = selectedEpisode;
    				selectedEpisode = null;
    				return;
    			}
    			else selectEpisodeCheckinReopenConfirmId=null;
    			
    			
    			selectEpisodeCheckinReopenConfirm(e);
    			
    			
//    			ep.setCheckinStarted(true);
//    			ep.setCheckoutStarted(false); // serve se sto tornando indietro
//    			try {
//    				ep = episodeService.updateEpisode(ep);
//				} catch (Exception e1) {
//					e1.printStackTrace();
//				}
//    			episodeDateChanged(); // aggiorno lista
//    			
//    			selectedEpisode = ep.getId();
//    			
//    			episodeSpectatorsNums = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(selectedEpisode).setDistinct().count();
//    			episodeSpectatorsNumsWithCheckin = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(selectedEpisode).and("checkin").eq(Boolean.TRUE).setDistinct().count();
//    			episodeSpectatorsNumsWithCheckout = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(selectedEpisode).and("checkout").eq(Boolean.TRUE).setDistinct().count();
//    			
//    			episodeChanged();
    			break;
			}
		}
	}
	public void selectEpisodeCheckinReopenConfirm (ActionEvent e) {
		getLog().info("selectEpisodeConfirm....");
		if (selectEpisodeCheckinReopenConfirmId!=null) {
			selectedEpisode = selectEpisodeCheckinReopenConfirmId;
			selectEpisodeCheckinReopenConfirmId=null;
		}
		
		Episode ep = episodeService.getEpisodeById(selectedEpisode);
		ep.setCheckinStarted(true);
		ep.setCheckoutStarted(false); // serve se sto tornando indietro
		try {
			ep = episodeService.updateEpisode(ep);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		episodeDateChanged(); // aggiorno lista
		
		selectedEpisode = ep.getId();
		epSel=null;
		
//		episodeSpectatorsNums = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(selectedEpisode).setDistinct().count();
//		episodeSpectatorsNumsWithCheckin = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(selectedEpisode).and("checkin").eq(Boolean.TRUE).setDistinct().count();
//		episodeSpectatorsNumsWithCheckout = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(selectedEpisode).and("checkout").eq(Boolean.TRUE).setDistinct().count();
		
		episodeChanged();
		
		
		
		RequestContext.getCurrentInstance().execute("PF('spectatorTableWidget').filter();");
	}
	public void selectEpisodeCheckinReopenConfirmCancel (ActionEvent e) {
		getLog().info("selectEpisodeCheckinReopenConfirmCancel....");
		selectEpisodeCheckinReopenConfirmId=null;
	}

	public void estartCheckoutEpisode(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("episodeIdb")) {
    			getLog().info("START CHECKOUT !!!!!!!!!!!!!!!!!!"); 
    			selectedEpisode = (Long)((UIParameter)com).getValue();
    			getLog().info("START CHECKOUT !!!!!!!!!!!!!!!!!! for episode id " + selectedEpisode);
    			Episode ep = episodeService.getEpisodeById(selectedEpisode);
    			ep.setCheckoutStarted(true);
    			try {
					ep = episodeService.updateEpisode(ep);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
    			episodeDateChanged(); // aggiorno lista
    			selectedEpisode = ep.getId();
    			epSel=null;
    			
//    			episodeSpectatorsNums = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(selectedEpisode).setDistinct().count();
//    			episodeSpectatorsNumsWithCheckin = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(selectedEpisode).and("checkin").eq(Boolean.TRUE).setDistinct().count();
//    			episodeSpectatorsNumsWithCheckout = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(selectedEpisode).and("checkout").eq(Boolean.TRUE).setDistinct().count();
    			
    			episodeChanged();
    			break;
			}
		}
	}
	
	public String getCheckinBackgroudColor() {
		if (getEpisode()!=null && !getEpisode().isCheckinStarted() && !getEpisode().isCheckoutStarted()) return "grey";
		
		if (getEpisode()!=null && getEpisode().isCheckinStarted() && !getEpisode().isCheckoutStarted()) return "green";
		else return "grey";
	}
	
	public String getCheckoutBackgroudColor() {
		if (getEpisode()!=null && !getEpisode().isCheckinStarted() && !getEpisode().isCheckoutStarted()) return "grey";
		
		if (getEpisode()!=null && getEpisode().isCheckoutStarted()) return "green";
		else return "grey";
	}
	
	public void backToChangeEpisode() {
		selectedEpisode = null;
		epSel=null;
		
		episodeSpectatorsNums = 0;
		episodeSpectatorsNumsWithCheckin = 0;
		episodeSpectatorsNumsWithCheckout = 0;

	}

	public void deleteDocumentFile() {
		
		try {
			
			spectatorToEdit = receptionService.getSpectatorEpisodeByIdNoLazy(spectatorToEdit.getId());
			
			try {
//				DocumentSpectator ds = documentSpectatorService.getDocumentSpectatorFinder().and("spectator.id").eq(spectatorToEdit.getSpectator().getId()).and("documentType.idDocumentType").eq(new Integer(1)).setDistinct().result();
				DocumentSpectator ds = null;
				List<DocumentSpectator> dss = documentSpectatorService.getDocumentSpectatorFinder().and("spectator.id").eq(spectatorToEdit.getSpectator().getId()).and("documentType.idDocumentType").eq(new Integer(1)).setDistinct().list();
				if (dss!=null && dss.size()>0) ds = dss.get(0);
				if (ds!=null) {
					documentSpectatorService.deleteDocumentSpectator(ds.getId());
				}
				
			} catch (Exception e) {
				getLog().info("errore nel salvataggio del tipo documento sul DB  " + e.toString());
			}

			
//			spectatorToEdit.getSpectator().setDocumentPath("");
			spectatorToEdit.getSpectator().setDocumentFileDownload(null);
//			spectatorService.updateSpectator(spectatorToEdit.getSpectator());
//			// Ricarico
//			spectatorToEdit = receptionService.getSpectatorEpisodeById(spectatorToEdit.getId());
			
			
			// GENERO PDF (se c'è già la firma)
			if (spectatorToEdit.getSign()!=null && !spectatorToEdit.getSign().equals("")) {
				try {
//					String filesDir = PropertiesUtils.getInstance().readProperty("allegati.uploaded.document.dir");
					
					getLog().info("Genero pdf...");
					JasperReportDetails det = new JasperReportDetails();
		            // Nome report
		            det.setDescription("Contract");
		            // Path report
		            det.setMasterReportUrl(File.separator+"WEB-INF"+File.separator+"reports"+File.separator+"liberatoria_new.jasper");
		            // Subreports url
		            Properties srUrls = new Properties();
		            //srUrls.put("SUBREPORT_URL", File.separator+"WEB-INF"+File.separator+"reports"+File.separator+"contracts"+File.separator+"subRep.jasper");
		            det.setSubReportUrls(srUrls);
		            // Parametri da passare al report
		            Map<String, Object> reportParameters = new HashMap<String, Object>();
		            
		            reportParameters.put("REPORT_RESOURCES_PATH", getAbsolutePath()+ File.separator+"WEB-INF"+File.separator+"reports"+File.separator);
	            
		            Location des = locationService.getLocationById(selectedDesk);
		            reportParameters.put("DESK", des);
		            reportParameters.put("STUDIO", des.getProductionCenter());
		            
		            reportParameters.put("SIGN", new java.io.ByteArrayInputStream(redrawSignature(extractSignature(spectatorToEdit.getSign()))));
		            
		            reportParameters.put("ID_DOCUMENT", null);
		            
		            det.setReportParameters(reportParameters);
		            
		            // Dati da visualizzare (un oggetto per ogni areaDetail in iReport)
		            Collection<SpectatorEpisode> beans = new ArrayList<SpectatorEpisode>();
	//	            Collection<ReportContratto> beans = new ArrayList<ReportContratto>();
	//	            beans.add(reportContratto);
		            beans.add(spectatorToEdit);
	
		            Map<String, Object> reportParams = new HashMap<String, Object>();
		            reportParams.put("REPORT_TITLE", "Liberatoria/Consenso");
	
		            ByteArrayOutputStream baos = jasperReportsFactory.complieReportAsPdfByteArrayOutputStream(det, reportParams, beans);
		            
		            // Solo per test !!!!!!!!!!!!!!!!
	//	            FileUtils.writeToFile("C:\\Users\\Crisma\\Desktop\\liberatoria.pdf", new ByteArrayInputStream(baos.toByteArray()));
		            
	//	            spectatorToEdit.setPdfSign(baos.toByteArray());
//		            spectatorToEdit.setPdfSignBase64Str(Base64.getEncoder().encodeToString(baos.toByteArray()));
		            
		            Liberatoria lib = spectatorToEdit.getLiberatoria();
		            if (lib==null) lib = new Liberatoria();
		            lib.setSpectatorEpisode(spectatorToEdit);
		            lib.setPdfSignBase64Str(Base64.getEncoder().encodeToString(baos.toByteArray()));
		            spectatorToEdit.setLiberatoria(lib);
		        }catch (Exception e){
		                e.printStackTrace();
		        }
			
				try {
					spectatorToEdit = receptionService.updateSpectatorEpisode(spectatorToEdit);
	
					if (spectatorToEdit.getLiberatoria()!=null && spectatorToEdit.getLiberatoria().getPdfSignBase64Str()!=null) {
						fileLiberatoria = new DefaultStreamedContent(new ByteArrayInputStream(Base64.getDecoder().decode(spectatorToEdit.getLiberatoria().getPdfSignBase64Str())), "application/pdf", "liberatoria.pdf");
					}
					
				} catch (Throwable e1) {
					e1.printStackTrace();
					addWarningMessage("ERRORE salvataggio liberatoria!");
				}
				
				// Notifico il mio browser (legato alla sessionId)
				EventBus eventBus = EventBusFactory.getDefault().eventBus();
				eventBus.publish("/sign/"+ sessionId, "");
			}			
		} catch (Throwable e) {
			addWarningMessage("ERRORE salvataggio spettatore!");
			e.printStackTrace();
		}
	}
	
	public synchronized void handleFileUpload(FileUploadEvent event) {
		

//		String filesDir = PropertiesUtils.getInstance().readProperty("allegati.uploaded.document.dir");
		getLog().info("handleFileUpload: file " + event.getFile().getFileName());
		try {
			byte[] doc = null;
			// transformiamo il file appena caricato in Blob
			try {
				doc = IOUtils.toByteArray(event.getFile().getInputstream());

				// salviamo il documento sul DB
				
//				DocumentSpectator ds = documentSpectatorService.getDocumentSpectatorFinder().and("spectator.id").eq(spectatorToEdit.getSpectator().getId()).and("documentType.idDocumentType").eq(new Integer(1)).setDistinct().result();
				DocumentSpectator ds = null;
				List<DocumentSpectator> dss = documentSpectatorService.getDocumentSpectatorFinder().and("spectator.id").eq(spectatorToEdit.getSpectator().getId()).and("documentType.idDocumentType").eq(new Integer(1)).setDistinct().list();
				if (dss!=null && dss.size()>0) ds = dss.get(0);
				if (ds==null) {
					DocumentType documentTypeToAdd = documentTypeService.getDocumentTypeFinder().and("idDocumentType").eq(new Integer(1)).setDistinct().result();
					DocumentSpectator docsp = new DocumentSpectator(documentTypeToAdd, spectatorToEdit.getSpectator());
					docsp.setDocInBlob(doc);
					docsp.setDescription("Upload");
					docsp.setDocumentCreationDate(new Date());
					docsp = documentSpectatorService.addDocumentSpectator(docsp);
					//addInfoMessage(getMessagesBoundle().getString("documentSpectator.saved"));
				}
				else {
					ds.setDocInBlob(doc);
					ds.setDescription("Upload");
					ds = documentSpectatorService.updateDocumentSpectator(ds);
				}

			} catch (Exception e) {
				getLog().info("errore nel salvataggio del tipo documento sul DB  " + e.toString());
			}
		
//			String filepath = System.currentTimeMillis()+"_"+ event.getFile().getFileName();
//			FileUtils.writeToFile(filesDir+"/"+filepath, event.getFile().getInputstream(), true);
//
//			spectatorToEdit.getSpectator().setDocumentPath(filepath);
//			
//			spectatorService.updateSpectator(spectatorToEdit.getSpectator());
			
			// Ricarico
			spectatorToEdit = receptionService.getSpectatorEpisodeByIdNoLazy(spectatorToEdit.getId());

			try {
				if (doc!=null) {
					ByteArrayContent cont = new ByteArrayContent(doc);
					cont.setName("Upload");
					spectatorToEdit.getSpectator().setDocumentFileDownload(cont);
				}
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			

		
			// GENERO PDF (se c'è già la firma)
			if (spectatorToEdit.getSign()!=null && !spectatorToEdit.getSign().equals("")) {
				try {
					getLog().info("Genero pdf...");
					JasperReportDetails det = new JasperReportDetails();
		            // Nome report
		            det.setDescription("Contract");
		            // Path report
		            det.setMasterReportUrl(File.separator+"WEB-INF"+File.separator+"reports"+File.separator+"liberatoria_new.jasper");
		            // Subreports url
		            Properties srUrls = new Properties();
		            //srUrls.put("SUBREPORT_URL", File.separator+"WEB-INF"+File.separator+"reports"+File.separator+"contracts"+File.separator+"subRep.jasper");
		            det.setSubReportUrls(srUrls);
		            // Parametri da passare al report
		            Map<String, Object> reportParameters = new HashMap<String, Object>();
		            
		            reportParameters.put("REPORT_RESOURCES_PATH", getAbsolutePath()+ File.separator+"WEB-INF"+File.separator+"reports"+File.separator);
		            
		            
		            Location des = locationService.getLocationById(selectedDesk);
		            reportParameters.put("DESK", des);
		            reportParameters.put("STUDIO", des.getProductionCenter());
		            reportParameters.put("SIGN", new java.io.ByteArrayInputStream(redrawSignature(extractSignature(spectatorToEdit.getSign()))));
		            
		            if (doc!=null)
		            	reportParameters.put("ID_DOCUMENT", new java.io.ByteArrayInputStream(doc));
		            else reportParameters.put("ID_DOCUMENT", null);
		            
		            
		            
		            det.setReportParameters(reportParameters);
		            
		            // Dati da visualizzare (un oggetto per ogni areaDetail in iReport)
		            Collection<SpectatorEpisode> beans = new ArrayList<SpectatorEpisode>();
	//	            Collection<ReportContratto> beans = new ArrayList<ReportContratto>();
	//	            beans.add(reportContratto);
		            beans.add(spectatorToEdit);
	
		            Map<String, Object> reportParams = new HashMap<String, Object>();
		            reportParams.put("REPORT_TITLE", "Liberatoria/Consenso");
	
		            ByteArrayOutputStream baos = jasperReportsFactory.complieReportAsPdfByteArrayOutputStream(det, reportParams, beans);
		            
		            // Solo per test !!!!!!!!!!!!!!!!
	//	            FileUtils.writeToFile("C:\\Users\\Crisma\\Desktop\\liberatoria.pdf", new ByteArrayInputStream(baos.toByteArray()));
		            
	//	            spectatorToEdit.setPdfSign(baos.toByteArray());
		            
		            
		            Liberatoria lib = spectatorToEdit.getLiberatoria();
		            if (lib==null) lib = new Liberatoria();
		            lib.setSpectatorEpisode(spectatorToEdit);
		            lib.setPdfSignBase64Str(Base64.getEncoder().encodeToString(baos.toByteArray()));
		            spectatorToEdit.setLiberatoria(lib);
//		            spectatorToEdit.setPdfSignBase64Str(Base64.getEncoder().encodeToString(baos.toByteArray()));
		        }catch (Exception e){
		                e.printStackTrace();
		        }
			
			
				try {
					spectatorToEdit = receptionService.updateSpectatorEpisodeNoLazy(spectatorToEdit);
	
					if (spectatorToEdit.getLiberatoria()!=null && spectatorToEdit.getLiberatoria().getPdfSignBase64Str()!=null) {
						fileLiberatoria = new DefaultStreamedContent(new ByteArrayInputStream(Base64.getDecoder().decode(spectatorToEdit.getLiberatoria().getPdfSignBase64Str())), "application/pdf", "liberatoria.pdf");
					}
					setFocusOnCheckin();
					
				} catch (Throwable e1) {
					e1.printStackTrace();
					addWarningMessage("ERRORE salvataggio liberatoria!");
				}
				
			}
		
		
			// Notifico il mio browser (legato alla sessionId)
			EventBus eventBus = EventBusFactory.getDefault().eventBus();
			eventBus.publish("/sign/"+ sessionId, "");

			//addInfoMessage(getMessagesBoundle().getString("attacchement.added"), "form_edit:messagesMasterCompanyAllegati5");
		} catch (Throwable e) {
			addWarningMessage("ERRORE salvataggio spettatore!");
			e.printStackTrace();
		}
    }
	
	
	public void deskChanged() {
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(episodeDate);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		Date toDate = cal.getTime();
		cal.setTime(episodeDate);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 1);
		Date from = cal.getTime();
		getLog().info("deskChanged - from:" + from + "   to:" + toDate);
		episodesList = episodeService.getEpisodeFinder().and("studio.productionCenter.locations.id").eq(selectedDesk)/*.and("episodeStatus.idStatus").eq(4)*/.and("dateFrom").ge(from).and("dateTo").le(toDate).setDistinct().list();
		
		
		
		
		episodes = new ArrayList<SelectItem>();
		episodes.add(new SelectItem(null, " "));
		List<Episode> episodesl = episodeService.getEpisodeFinder().and("studio.productionCenter.locations.id").eq(selectedDesk).setDistinct().list();
//		Comparator<Episode> bySeason = (Episode p1, Episode p2)->p1.getId().compareTo(p2.getId());			
//		episodesl.sort(bySeason);
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		for(Episode sptype : episodesl) {
			SelectItem xx = new SelectItem(sptype.getId(), sptype.getProduction().getName() + " del " + (sptype.getDateFrom()!=null?formatter.format(sptype.getDateFrom()):""));
			episodes.add(xx);
		}
		selectedEpisode = null;
		epSel=null;
		
		fileLiberatoria=null;
		
		if (selectedDesk!=null) FacesUtils.setCookie("MEDIASET_STUDIOS_SELECTED_LOCATION", selectedDesk.toString(), 60*60*24*30); // un mese
	}
	
	public void resetDesk() {
		selectedDesk=null;
		selectedEpisode=null;
		epSel=null;
		fileLiberatoria=null;
		FacesUtils.setCookie("MEDIASET_STUDIOS_SELECTED_LOCATION", "", 0); //Reset
	}
	
	public boolean haveUserRole(String roles) {
		
		String[] listRoles = roles.split(",");
		for (int i = 0; i < listRoles.length; i++) {
			listRoles[i] = listRoles[i].trim();
			
		}
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication!=null && authentication.getAuthorities()!=null) {
			//String currentPrincipalName = authentication.getName();
			for (GrantedAuthority r : authentication.getAuthorities()) {
				//System.out.println("Ruolo:"+r.getAuthority());
				for (int i = 0; i < listRoles.length; i++) {
					if (r.getAuthority()!=null && r.getAuthority().equals(listRoles[i])) {
						//System.out.println("trovato ruolo !!!");
						return true;
					}					
				}
			}
		}
		return false;
	}
	
	@SuppressWarnings("deprecation")
	public void setFocusOnCheckin() {
//		do_checkin
		boolean do_checkin=false;
		try {
			do_checkin= (spectatorToEdit.getMedia()!= null) ;
			do_checkin= do_checkin && (spectatorToEdit.getSignDate() == null); 
			do_checkin= do_checkin && (spectatorToEdit.getLiberatoria() != null);
			do_checkin= do_checkin && (spectatorToEdit.getLiberatoria().getPdfSignBase64Str() != null);
			do_checkin= do_checkin && (spectatorToEdit.getLiberatoria().getPdfSignBase64Str() != "");
			if (do_checkin) RequestContext.getCurrentInstance().execute("PF('do_checkin').focus()");			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	
//	private void updateGraph() {
////		getLog().info("updateGraph...");
//		if (getEpisode()!=null) {
//			long spCount = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(getEpisode().getId()).setDistinct().count();
//			//getEpisode().getSpectators().size(); // 
//			//getLog().info("esecuzione spectator size in:" + (System.currentTimeMillis()-tt) + " ms");
//			//tt = System.currentTimeMillis();
//			long spCountWithCheckin = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(getEpisode().getId()).and("checkin").eq(Boolean.TRUE).setDistinct().count();
//			//getEpisode().getSpectatorsNumberWithCheckin(); // 
//			//getLog().info("esecuzione getEpisode().getSpectatorsNumberWithCheckin() in:" + (System.currentTimeMillis()-tt) + " ms");
//			/******************/
//			
//			Map<String, Number> circle = null;
//			if(checkinChartModel.getData()!=null && checkinChartModel.getData().size() > 0) {
//				circle = checkinChartModel.getData().get(0);
//				circle.clear();
//			}
//			else {
//				circle = new LinkedHashMap<String, Number>();
//				checkinChartModel.addCircle(circle);
//			}
//			
//			if (spCount>0) 
//					circle.put(((int)((100*spCountWithCheckin)/spCount)+"%"), spCountWithCheckin); 
//			else 
//					circle.put("0%", spCountWithCheckin); 
//			
//			circle.put(" ", spCount-spCountWithCheckin);
//
//			chartTitle = ""+spCountWithCheckin;
//			if (spCount>0) {
//				chartSubTitle = ((int)((100*spCountWithCheckin)/spCount)+"%");
//				//System.out.println("chartSubTitle="+chartSubTitle);
//			}else { 
//				chartSubTitle = "0%";
//			}
//		}
//	}

	
	private void updateGraph() {
//		getLog().info("updateGraph...");
		if (getEpisode()!=null) {
			// Posti disponibili
			long spCountAvailable = getEpisode().getRequestedSpectators();
			// Spettatori Previsione
			long spCount = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(getEpisode().getId()).setDistinct().count();
			// Spettatori che hanno fatto checkin
			long spCountWithCheckin = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(getEpisode().getId()).and("checkin").eq(Boolean.TRUE).setDistinct().count();
			// Spettatori che hanno fatto checkout
			long spCountWithCheckOut = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(getEpisode().getId()).and("checkout").eq(Boolean.TRUE).setDistinct().count();
			//getEpisode().getSpectators().size(); // 
			//getLog().info("esecuzione spectator size in:" + (System.currentTimeMillis()-tt) + " ms");
			//tt = System.currentTimeMillis();
			// Spettatori ancora presenti all'interno
//			long spEntered=spCountWithCheckin-spCountWithCheckOut;
			long spEntered=spCountWithCheckin;
			//getEpisode().getSpectatorsNumberWithCheckin(); // 
			//getLog().info("esecuzione getEpisode().getSpectatorsNumberWithCheckin() in:" + (System.currentTimeMillis()-tt) + " ms");
			
			/******************/
			
			Map<String, Number> circle = null;
			if(checkinChartModel.getData()!=null && checkinChartModel.getData().size() > 0) {
				circle = checkinChartModel.getData().get(0);
				circle.clear();
			}
			else {
				circle = new LinkedHashMap<String, Number>();
				checkinChartModel.addCircle(circle);
			}
			
			if (spCountWithCheckin>0) 
//				circle.put(((int)(100*spEntered/spCount)+"%"), spEntered);
				circle.put(((int)(100*spEntered/spCount)+"%"), spEntered);
			else 
				circle.put("0%", spEntered); 
			
			circle.put(" ", spCount-spEntered);
			chartTitle = ""+spEntered;
			
			if (spCountWithCheckin>0) {
				chartSubTitle = ((int)(100*spEntered/spCount)+"%");
//				System.out.println("chartSubTitle="+chartSubTitle);
			}else { 
				chartSubTitle = "0%";
			}
			/******************/
			
			Map<String, Number> circle1 = null;
			if(checkinChartModel2.getData()!=null && checkinChartModel2.getData().size() > 0) {
				circle1 = checkinChartModel2.getData().get(0);
				circle1.clear();
			}
			else {
				circle1 = new LinkedHashMap<String, Number>();
				checkinChartModel2.addCircle(circle);
			}
			
			if (spCountWithCheckin>0) 
				circle1.put(((int)(100*spEntered/spCountAvailable)+"%"), spEntered); 
			else 
				circle1.put("0%", spEntered); 
			
			circle1.put(" ", spCountAvailable-spEntered);

			chartTitle = ""+spEntered;
			
			if (spCountWithCheckin>0) {
				chartSubTitle2 = ((int)(100*spEntered/spCountAvailable)+"%");
//				System.out.println("chartSubTitle="+chartSubTitle);
			}else { 
				chartSubTitle2 = "0%";
			}
		}
	}
	
	
	public void episodeChanged() {
		getLog().info("episodeChanged..." + selectedEpisode);
		
		//Carico gli spettatori
		if (selectedEpisode!=null) {
			//spectators = episodeService.getEpisodeById(selectedEpisode).getSpectators();
			
			if (getEpisode().isCheckinStarted() && !getEpisode().isCheckoutStarted()) {
				allOrPrecheckinOnly=false;
				long tt = System.currentTimeMillis();
				//spectators = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(selectedEpisode).and("preCheckin").eq(Boolean.TRUE).setDistinct().list();
				spectators = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(selectedEpisode).and("preCheckin").eq(Boolean.TRUE).setDistinct().list();
				getLog().info("esecuzione query 1 in:" + (System.currentTimeMillis()-tt) + " ms");
			}
			else if (getEpisode().isCheckinStarted() && getEpisode().isCheckoutStarted()) {
				long tt = System.currentTimeMillis();
				allOrCheckinOnly=false;
				spectators = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(selectedEpisode).and("checkin").eq(Boolean.TRUE).setDistinct().list();
				getLog().info("esecuzione query 2 in:" + (System.currentTimeMillis()-tt) + " ms");
			}
			else {
				long tt = System.currentTimeMillis();
				spectators = episodeService.getEpisodeById(selectedEpisode).getSpectators();
				spectators.size(); // avoid lazy
				getLog().info("esecuzione query 3 in:" + (System.currentTimeMillis()-tt) + " ms");
			}
			long tt = System.currentTimeMillis();

			spectators=orderedSpectators(spectators);
			getLog().info("esecuzione order in:" + (System.currentTimeMillis()-tt) + " ms");
			
			tt = System.currentTimeMillis();
			updateGraph();
			getLog().info("esecuzione updateGraph in:" + (System.currentTimeMillis()-tt) + " ms");
			
			episodeSpectatorsNums = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(selectedEpisode).setDistinct().count();
			episodeSpectatorsNumsWithCheckin = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(selectedEpisode).and("checkin").eq(Boolean.TRUE).setDistinct().count();
			episodeSpectatorsNumsWithCheckout = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(selectedEpisode).and("checkout").eq(Boolean.TRUE).setDistinct().count();
			spectatorRowNum = -1;
		}
	}
	
	
	private int spectatorRowNum = -1;
//	public String getSpectatorRowNumElString(){
//		if (spectatorRowNum>-1) {
//			getLog().info(":form_employees:spectator_table:@row("+spectatorRowNum+")");
////			return ":form_employees:spectator_table:@row("+spectatorRowNum+")";
//			
//			return "@widgetVar(spectatorTableWidget):@row("+spectatorRowNum+")";
//		}
//		return "";
//	}
	
	public void updatespectatorsTable(DataTable spectatorsTable) {
		System.out.println("updatespectatorsTable .... " + spectatorRowNum);
		
		if (spectatorRowNum>-1) {
			spectatorsTable.setValue(filteredTableList!=null?filteredTableList:spectators); // Questo è importanete perchè i dati che ha il binding non sono gli stessi del bean
			Ajax.updateRow(spectatorsTable, spectatorRowNum);
			//System.out.println("Spettatore:" + spectators.get(spectatorRowNum));
		}
	}
	
	
	
	
	public void singleSpectatorChanged(SpectatorEpisode spectator) { 
		getLog().info("singleSpectatorChanged...spectator:" + spectator + "     episode:" + selectedEpisode );
		
		//Carico gli spettatori
		if (spectator!=null) {
			
//			spectatorRowNum = spectators.indexOf(spectator); //già fatto in row selected 
//			getLog().info("rownumSpectator:" + spectatorRowNum);
//			 Rimpiazzo in modo da aggiornare dati
			

			if (spectators.indexOf(spectator)>-1) spectators.set(spectators.indexOf(spectator), spectator);
			if (filteredTableList!=null && filteredTableList.indexOf(spectator)>-1) filteredTableList.set(filteredTableList.indexOf(spectator), spectator);
			
//			spectator.setOrder(spectators.get(spectators.indexOf(spectatorToEdit)).getOrder());
//			spectators.set(spectatorRowNum, spectator);
			
			// AGGIRNO SOLO RIGA !!!!...questo non va , va fatto lato pagina
			//RequestContext.getCurrentInstance().update("form_employees:spectator_table:@row("+spectatorRowNum+")");
			

			
			long tt = System.currentTimeMillis();
			updateGraph();
			getLog().info("esecuzione updateGraph in:" + (System.currentTimeMillis()-tt) + " ms");
			tt = System.currentTimeMillis();
			
			episodeSpectatorsNums = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(selectedEpisode).setDistinct().count();
			episodeSpectatorsNumsWithCheckin = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(selectedEpisode).and("checkin").eq(Boolean.TRUE).setDistinct().count();
			episodeSpectatorsNumsWithCheckout = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(selectedEpisode).and("checkout").eq(Boolean.TRUE).setDistinct().count();
			
			getLog().info("esecuzione 3 query:" + (System.currentTimeMillis()-tt) + " ms");
		}

	}
	
	private Episode epSel=null;
	public Episode getEpisode() {
		if (epSel!=null && epSel.getId()!=null && selectedEpisode!=null && epSel.getId().longValue()==selectedEpisode.longValue()) {
			return epSel;
		}
		epSel = episodeService.getEpisodeById(selectedEpisode);
		return epSel;
	}
	
	public void closeSpectator() {
		getLog().info("closeSpectator");
		
		if (scanMonitoringThread!=null) {
			scanMonitoringThread.stopMonitoring();
			scanMonitoringThread=null;
		}
		// update gui
		try {
			if (spectatorToEdit!=null) spectatorToEdit.setLiberatoria(null); // altrimenti rimane caricata
		} catch (Throwable e) {}
		
		if (spectatorToEdit!=null) spectatorRowNum = filteredTableList!=null?filteredTableList.indexOf(spectatorToEdit):spectators.indexOf(spectatorToEdit); //aggiorno indice ! perchè nel frattempo potrebbe essere arrivato ad esempio un transito
		if (spectatorToEdit!=null) singleSpectatorChanged(spectatorToEdit);
		//episodeChanged();
		//RequestContext.getCurrentInstance().update(":form_employees:updateSpectsList");
		spectatorToEdit = null;
	}
	
	
	public void markSpectatorToReview() {
		getLog().info("Mark Spectator to review");
		Long id = spectatorToEdit.getId();
		boolean flag = spectatorToEdit.isSpectatorToReview();
		boolean globalFlag = flag;
		try {
			Spectator sp = spectatorService.getSpectatorById(spectatorToEdit.getSpectator().getId());
			for (SpectatorEpisode se : sp.getEpisodes()) {
				if (!se.getId().equals(id)) {
//					se.setSpectatorToReview(flag || se.isSpectatorToReview());
					globalFlag = globalFlag || se.isSpectatorToReview();
				}else{
					se.setSpectatorToReview(flag);
					receptionService.updateSpectatorEpisode(se);	
				}			
			}
			sp.setToReview(globalFlag);
			spectatorService.updateSpectator(sp);	

		} catch (Exception e2) {
			getLog().error(e2.getMessage());
		}
	}
	
	public void checkinSpectator(ActionEvent e) {
		getLog().info("checkinSpectator");
		
		
//		spectatorToEdit = receptionService.getSpectatorEpisodeById(spectatorToEdit.getId());
		spectatorToEdit = receptionService.getSpectatorEpisodeByIdNoLazy(spectatorToEdit.getId());
		SpectatorEpisode oldSpect = SpectatorEpisode.copy(spectatorToEdit);
//		// Controllo che ci sia la firma e il documento!
//		if (spectatorToEdit.getSign()==null || spectatorToEdit.getSign().equals("")) {
//			addWarningMessage("Non è possibile effettuaare il checkin senza firma su liberatoria!");
//			FacesContext.getCurrentInstance().validationFailed();
//			return;
//		}
//		// Verifico se c'è un documento di identità
//        DocumentSpectator ds = null;
//		// transformiamo il file appena caricato in Blob
//		try {
////			ds = documentSpectatorService.getDocumentSpectatorFinder().and("spectator.id").eq(spectatorToEdit.getSpectator().getId()).and("documentType.idDocumentType").eq(new Integer(1)).setDistinct().result();
//			List<DocumentSpectator> dss = documentSpectatorService.getDocumentSpectatorFinder().and("spectator.id").eq(spectatorToEdit.getSpectator().getId()).and("documentType.idDocumentType").eq(new Integer(1)).setDistinct().list();
//			if (dss!=null && dss.size()>0) ds = dss.get(0);
//		} catch (Exception ex) {
//			ex.printStackTrace();
//		}
//		if (ds==null) {
//			addWarningMessage("Non è possibile effettuaare il checkin senza l'acquisizione del documento di riconoscimento!");
//			FacesContext.getCurrentInstance().validationFailed();
//			return;
//		}
		if (spectatorToEdit.isCheckin()) {
			addWarningMessage("Checkin già effettuato!");
			FacesContext.getCurrentInstance().validationFailed();
			return;
		}
		
		
		try {
			spectatorToEdit.setCheckin(true);
			spectatorToEdit.setCheckinDate(new Date());
			
			spectatorToEdit.setSign(null); // cancello firma per GDPR			
			getLog().info("prima di update");
			
			spectatorToEdit = receptionService.updateSpectatorEpisode(spectatorToEdit);
			SpectatorEpisode newSpect =SpectatorEpisode.copy(spectatorToEdit);
			auditService.addAudit(new Audit(userSession.getLastName() + " " + userSession.getFirstName()+" ("+navigationBean.getUserName()+")", "Checkin", "SpectatorEpisode", spectatorToEdit.getId(), "", "", "", "", oDiff.getDiff(oldSpect, newSpect)));
			oldSpect=null;
			newSpect=null;
			getLog().info("dopo update");
			
			
		} catch (Throwable e1) {
			e1.printStackTrace();
			addWarningMessage("ERRORE salvataggio spettatore!");
			spectatorToEdit = null;
			if (scanMonitoringThread!=null) {
				scanMonitoringThread.stopMonitoring();
				scanMonitoringThread=null;
			}
			return;
		}
		
		// Aggiorno interfaccia
//		spectatorToEdit.setLiberatoria(null); // altrimenti rimane caricata
		spectatorRowNum = filteredTableList!=null?filteredTableList.indexOf(spectatorToEdit):spectators.indexOf(spectatorToEdit); //aggiorno indice ! perchè nel frattempo potrebbe essere arrivato ad esempio un transito
		spectatorToEdit.setOrder(spectators.get(spectators.indexOf(spectatorToEdit)).getOrder());

		singleSpectatorChanged(spectatorToEdit);

		//RequestContext.getCurrentInstance().update(":form_employees:updateSpectsList");

		
		spectatorToEdit = null;
		fileLiberatoria=null;
		if (scanMonitoringThread!=null)  {
			scanMonitoringThread.stopMonitoring();
			scanMonitoringThread=null;
		}
		
		
		// update gui
		//episodeChanged();
		
		RequestContext.getCurrentInstance().update("spectatorDlg");
		//RequestContext.getCurrentInstance().execute("PF('spectatorTableWidget').clearFilters()");
		clearAllFilters("form_employees:spectator_table");
	}

	public void checkoutSpectator(ActionEvent e) {
		getLog().info("checkoutSpectator");
		
		spectatorToEdit = receptionService.getSpectatorEpisodeByIdNoLazy(spectatorToEdit.getId());
		
		// Controllo che ci sia la firma e il documento!
		if (!spectatorToEdit.isCheckin()) {
			addWarningMessage("Non è ancora stato effettuato il checkin!");
			FacesContext.getCurrentInstance().validationFailed();
			return;
		}
		if (spectatorToEdit.isCheckout()) {
			addWarningMessage("Checkout già effettuato!");
			FacesContext.getCurrentInstance().validationFailed();
			return;
		}
				
		try {
			spectatorToEdit.setCheckout(true);
			spectatorToEdit.setCheckoutDate(new Date());
			getLog().info("prima di update");
			SpectatorEpisode oldSpect = SpectatorEpisode.copy(receptionService.getSpectatorEpisodeByIdNoLazy(spectatorToEdit.getId()));
			spectatorToEdit = receptionService.updateSpectatorEpisode(spectatorToEdit);
			SpectatorEpisode newSpect =SpectatorEpisode.copy(spectatorToEdit);
			auditService.addAudit(new Audit(userSession.getLastName() + " " + userSession.getFirstName()+" ("+navigationBean.getUserName()+")", "Checkout", "SpectatorEpisode", spectatorToEdit.getId(), "", "", "", "", oDiff.getDiff(oldSpect, newSpect)));
			oldSpect=null;
			newSpect=null;			
			getLog().info("dopo update");
			// libero il badge
			removeBadgeFromSpectator(null);
			
		} catch (Throwable e1) {
			e1.printStackTrace();
			addWarningMessage("ERRORE salvataggio spettatore!");
			spectatorToEdit = null;
			if (scanMonitoringThread!=null) {
				scanMonitoringThread.stopMonitoring();
				scanMonitoringThread=null;
			}
		}
		
		// Aggiorno interfaccia
		spectatorToEdit.setLiberatoria(null); // altrimenti rimane caricata
		spectatorRowNum = filteredTableList!=null?filteredTableList.indexOf(spectatorToEdit):spectators.indexOf(spectatorToEdit); //aggiorno indice ! perchè nel frattempo potrebbe essere arrivato ad esempio un transito
		singleSpectatorChanged(spectatorToEdit);
				
		//RequestContext.getCurrentInstance().update(":form_employees:updateSpectsList");
		
		
		spectatorToEdit = null;
		fileLiberatoria = null;
		if (scanMonitoringThread!=null) {
			scanMonitoringThread.stopMonitoring();
			scanMonitoringThread=null;
		}
		
		// update gui
		//episodeChanged();
		
		try {
			RequestContext.getCurrentInstance().update("spectatorDlg");
		} catch (Throwable e2) {
			getLog().warn(e2.getMessage());
		}
		
		//clearAllFilters(":form_spec:spectator_table");
	}
	
	private void clearAllFilters(String dataTableName) {
	    DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent(dataTableName);
	    if (!((dataTable==null)||(dataTable.getFilters().isEmpty()))) {
	        dataTable.reset();
	
	        RequestContext requestContext = RequestContext.getCurrentInstance();
	        requestContext.update(dataTableName);
	    }
	}
	
	public void removeBadgeFromSpectator(ActionEvent e ) {
		getLog().info("removeBadgeFromSpectator...");
		if (spectatorToEdit!=null) {
			SpectatorEpisode oldSpect = SpectatorEpisode.copy(receptionService.getSpectatorEpisodeByIdNoLazy(spectatorToEdit.getId()));
			Media m = spectatorToEdit.getMedia();
			if (m!=null) {
				m.setMediaStatusType(mediaStatusTypeService.getMediaStatusTypeByIdStatus(1)); // stato libero
				try {
					mediaService.updateMedia(m);
					getLog().info("removeBadgeFromSpectator...setting "+m.getMediaStatusType().getDescription());
				} catch (Exception e2) {
					getLog().error("removeBadgeFromSpectator...setting "+m.getMediaStatusType().getDescription());
					e2.printStackTrace();
				}
				spectatorToEdit = receptionService.getSpectatorEpisodeByIdNoLazy(spectatorToEdit.getId());
				spectatorToEdit.setMedia(null);
				try {
					spectatorToEdit = receptionService.updateSpectatorEpisode(spectatorToEdit);
					SpectatorEpisode newSpect =SpectatorEpisode.copy(spectatorToEdit);
					auditService.addAudit(new Audit(userSession.getLastName() + " " + userSession.getFirstName()+" ("+navigationBean.getUserName()+")", "UnassignBadge", "SpectatorEpisode", spectatorToEdit.getId(), "", "", "", "", oDiff.getDiff(oldSpect, newSpect)));
					oldSpect=null;
					newSpect=null;
				} catch (Exception e1) {
					addWarningMessage("ERRORE salvataggio spettatore in removeBadgeFromSpectator!");
					e1.printStackTrace();
				}
			}
		}
	}
	
	
	public void openCameraScanner(ActionEvent e ) {
		getLog().info("openCameraScanner...");
		
		scannerVisible = true;
	}
	
	
	public void closeCameraScanner(ActionEvent e ) {
		getLog().info("closeCameraScanner...");
		
		scannerVisible = false;
		
		RequestContext.getCurrentInstance().execute("PF('poolAcquire').stop()"); 
		RequestContext.getCurrentInstance().execute("PF('pc').dettach()");
	}
	
	
	
	public void spectatorGuidChanged() {
		getLog().info("spectatorGuidChanged..." + spectatorGuid);
		
		// Effettuo ricerca dello spettatore 
		
		spectatorToEdit = null;
		if (scanMonitoringThread!=null) {
			scanMonitoringThread.stopMonitoring();
			scanMonitoringThread=null;
		}
		if (spectators!=null) {
			for (SpectatorEpisode s : spectators) {
				if (s.getGuid()!=null && s.getGuid().equals(spectatorGuid)) {
					spectatorToEdit = s;
					break;
				}
			}
			
		}
		
//		spectatorToEdit = spectatorService.getSpectatorFinder().and("guid").eq(spectatorGuid).setDistinct().result();
		
//		if (spectatorToEdit==null) spectatorToEdit=new Spectator();
		
		if (spectatorToEdit!=null) {
			selectedSpectatorType = spectatorToEdit.getSpectator().getSpectatorType()!=null?spectatorToEdit.getSpectator().getSpectatorType().getId():null;		
			String pathDirScan = PropertiesUtils.getInstance().readProperty("scanner.dir_for_scanned_files");
			// elimino eventuali scansioni precedenti e faccio partire il monitoraggio della cartella per vedere se arriva un nuovo file di scansione
			//CheckinDesk des = receptionService.getCheckinDeskById(selectedDesk);
			Location des = locationService.getLocationById(selectedDesk);
			// elimino file che iniziano per des.getName()
			final String startNameFile = des.getName();
			final File[] files = (new File(pathDirScan)).listFiles( new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
					 return name.matches( startNameFile+".*" ); // tutti i file che iniziano con il nome del desk
				}
			});
			if (files!=null) {
				for ( final File file : files ) {
				    if ( !file.delete() ) {
				    	getLog().error( "Can't remove " + file.getAbsolutePath() );
				    }
				}
			}

			if (scanMonitoringThread==null) {
		        scanMonitoringThread =  new WatchDirForScanThread(pathDirScan, des.getName(), this);
		        (new Thread(scanMonitoringThread)).start();
			}
	        
			
	        // Verifico se c'è un documento di identità
	        DocumentSpectator ds = null;
			// transformiamo il file appena caricato in Blob
			try {
//				ds = documentSpectatorService.getDocumentSpectatorFinder().and("spectator.id").eq(spectatorToEdit.getSpectator().getId()).and("documentType.idDocumentType").eq(new Integer(1)).setDistinct().result();
				List<DocumentSpectator> dss = documentSpectatorService.getDocumentSpectatorFinder().and("spectator.id").eq(spectatorToEdit.getSpectator().getId()).and("documentType.idDocumentType").eq(new Integer(1)).setDistinct().list();
				if (dss!=null && dss.size()>0) ds = dss.get(0);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			try {
				if (ds!=null) {
					ByteArrayContent cont = new ByteArrayContent(ds.getDocInBlob());
					cont.setName(ds.getDescription());
					spectatorToEdit.getSpectator().setDocumentFileDownload(cont);
				}
			} catch (Exception e1) {
				e1.printStackTrace();
			}

			if (spectatorToEdit.getLiberatoria()!=null && spectatorToEdit.getLiberatoria().getPdfSignBase64Str()!=null) {
				fileLiberatoria = new DefaultStreamedContent(new ByteArrayInputStream(Base64.getDecoder().decode(spectatorToEdit.getLiberatoria().getPdfSignBase64Str())), "application/pdf", "liberatoria.pdf");
			}
		}
		getLog().info("Trovato:" + spectatorToEdit);
		getLog().info("spectator type:" + selectedSpectatorType);
		spectatorGuid="";
		RequestContext.getCurrentInstance().execute("PF('spectatorDlgWdgt').show()");
	}
	

	
	
	
	public void onSpectatorRowSelectCheckbox(SelectEvent event) {
		getLog().info("onSpectatorRowSelectCheckbox  - selectedSpectators:" + (selectedSpectators!=null?selectedSpectators.size():"null"));
	}
	public void onSpectatorRowUnselectCheckbox(UnselectEvent event) {
		getLog().info("onSpectatorRowUnselectCheckbox  - selectedSpectators:" + (selectedSpectators!=null?selectedSpectators.size():"null"));
	}
	public void onSpectatortoggleSelect(ToggleSelectEvent event) {
		getLog().info("onSpectatortoggleSelect  - selectedSpectators:" + (selectedSpectators!=null?selectedSpectators.size():"null"));
	}
	
	private List<SpectatorEpisode> filteredTableList = null;
//	public void onFilter(FilterEvent event) {
	@SuppressWarnings("unchecked")
	public void onSortOrFilter() {
		getLog().info("onSortOrFilter called" );
		DataTable table = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form_employees:spectator_table");
		if (table.getFilteredValue()!=null) {
			getLog().info("sort or filter values size:" + table.getFilteredValue().size());
			filteredTableList = table.getFilteredValue();
			// Debug
//			for(SpectatorEpisode s : filteredTableList){
//				getLog().info("sort or filter ----" + s.getId());
//			}
		}
		else {
			getLog().info("list sort or filtered null");
			filteredTableList=null;
		}
			
//		DataTable table = (DataTable) event.getSource();
//		if (event.getData()!=null) {
//			getLog().info("filter values size2:" + event.getData().size());
//			filteredTableList = (List<SpectatorEpisode>)event.getData();
//		      
//			for(SpectatorEpisode s : filteredTableList){
//				getLog().info("filter2 ----" + s.getId());
//			}
//		}
//		if (table.getFilteredValue()!=null) {
//			getLog().info("filter values size:" + table.getFilteredValue().size());
//	      
//			filteredTableList = table.getFilteredValue();
//	      
//			for(SpectatorEpisode s : filteredTableList){
//				getLog().info("filter ----" + s.getId());
//			}
//		}
//		else getLog().info("list filtered null");
	}
	
	
	public void onSpectatorRowSelect(SelectEvent event) {
		
		getLog().info("onSpectatorRowSelect:" + ((SpectatorEpisode) event.getObject()).getId());
		spectatorToEdit = receptionService.getSpectatorEpisodeByIdNoLazy(((SpectatorEpisode) event.getObject()).getId());
//		if (spectatorToEdit.getLiberatoria() == null) {
//			spectatorToEdit.setLiberatoria(new Liberatoria());
//			spectatorToEdit.getLiberatoria().setSpectatorEpisode(spectatorToEdit);
//		}
//		
		if (filteredTableList!=null) spectatorRowNum = filteredTableList.indexOf(spectatorToEdit);
		else spectatorRowNum = spectators.indexOf(spectatorToEdit);
		
//		Integer rowIndex = ((DataTable)event.getSource()).getFilRowIndex();
//		System.out.println(rowIndex + "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
//		DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form:MyTable");
//      dataTable.selsetSelection(getNextObj());
//		event.
		
		try {
			
			String pathDirScan = PropertiesUtils.getInstance().readProperty("scanner.dir_for_scanned_files");
			// elimino eventuali scansioni precedenti e faccio partire il monitoraggio della cartella per vedere se arriva un nuovo file di scansione
			//CheckinDesk des = receptionService.getCheckinDeskById(selectedDesk);
			Location des = locationService.getLocationById(selectedDesk);
			// elimino file che iniziano per des.getName()
			final String startNameFile = des.getName();
			final File[] files = (new File(pathDirScan)).listFiles( new FilenameFilter() {
				
				@Override
				public boolean accept(File dir, String name) {
					 //return name.matches( startNameFile+".*" ); // tutti i file che iniziano con il nome del desk
					 if(name.startsWith(startNameFile))
		            {
		                return true;
		            }
		            else
		            {
		                return false;
		            }
				}
			});
			
			if (files==null) getLog().error("!!!Errore configurazione proprietà scanner.dir_for_scanned_files !!!");
			
			for ( final File file : files ) {
			    if ( !file.delete() ) {
			    	getLog().error( "Can't remove " + file.getAbsolutePath() );
			    }
			}
			
			if (scanMonitoringThread==null) {
		        scanMonitoringThread =  new WatchDirForScanThread(pathDirScan, des.getName(), this);
		        (new Thread(scanMonitoringThread)).start();
			}
			
			// Verifico se c'è un documento di identità
	        DocumentSpectator ds = null;
			// transformiamo il file appena caricato in Blob.
			try {
//				ds = documentSpectatorService.getDocumentSpectatorFinder().and("spectator.id").eq(spectatorToEdit.getSpectator().getId()).and("documentType.idDocumentType").eq(new Integer(1)).setDistinct().result();
				List<DocumentSpectator> dss = documentSpectatorService.getDocumentSpectatorFinder().and("spectator.id").eq(spectatorToEdit.getSpectator().getId()).and("documentType.idDocumentType").eq(new Integer(1)).setDistinct().list();
				if (dss!=null && dss.size()>0) ds = dss.get(0);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			if (ds!=null) {
				getLog().info("Documento trovato ....creo content download");
				ByteArrayContent cont = new ByteArrayContent(ds.getDocInBlob());
				cont.setName(ds.getDescription());
				spectatorToEdit.getSpectator().setDocumentFileDownload(cont);
			}
			
			/*if (spectatorToEdit.getPdfSign()!=null) {
				fileLiberatoria = new DefaultStreamedContent(new ByteArrayInputStream(spectatorToEdit.getPdfSign()), "application/pdf", "liberatoria.pdf");
			}*/
//			Se la liberatoria esiste gia la prendo
			if (spectatorToEdit.getLiberatoria()!=null && spectatorToEdit.getLiberatoria().getPdfSignBase64Str()!=null && ! spectatorToEdit.getLiberatoria().getPdfSignBase64Str().isEmpty()) {
				fileLiberatoria = new DefaultStreamedContent(new ByteArrayInputStream(Base64.getDecoder().decode(spectatorToEdit.getLiberatoria().getPdfSignBase64Str())), "application/pdf", "liberatoria.pdf");
//				fileLiberatoria = new DefaultStreamedContent(new ByteArrayInputStream(Base64.getDecoder().decode(spectatorToEdit.getPdfSignBase64Str())), "application/pdf");
			} else {
//				Fabrizio
//				Altrimenti ne creo una nuova recuperandola dall'ultima liberatoria entro i 180gg. 
				SpectatorEpisode mostRecentParticipation = spectatorEpisodeService.getMostRecentParticipation(spectatorToEdit, DateUtils.getDateWithoutTime(spectatorToEdit.getEpisode().getDateFrom()), validityDays);				
//				Se ha una partecipazione con liberatoria valida
				if (! mostRecentParticipation.equals(spectatorToEdit)) {
					if (mostRecentParticipation.getLiberatoria()!=null && mostRecentParticipation.getLiberatoria().getPdfSignBase64Str()!=null) {
//						spectatorToEdit.setLiberatoria(mostRecentParticipation.getLiberatoria());
						if (spectatorToEdit.getLiberatoria()!=null && spectatorToEdit.getLiberatoria().getPdfSignBase64Str().isEmpty()) {
							spectatorToEdit.getLiberatoria().setPdfSignBase64Str(mostRecentParticipation.getLiberatoria().getPdfSignBase64Str());
						}else {
							Liberatoria lib = new Liberatoria();
							lib.setPdfSignBase64Str(mostRecentParticipation.getLiberatoria().getPdfSignBase64Str());	
							lib.setSpectatorEpisode(spectatorToEdit);
							spectatorToEdit.setLiberatoria(lib);
						}
						spectatorToEdit.setSign(mostRecentParticipation.getSign());						
						spectatorToEdit.setSignDate(mostRecentParticipation.getSignDate()!=null?mostRecentParticipation.getSignDate():mostRecentParticipation.getCheckinDate());
						try {
							spectatorToEdit = receptionService.updateSpectatorEpisodeNoLazy(spectatorToEdit);
							setFocusOnCheckin();
						} catch (Exception e2) {
							e2.printStackTrace();
						}
						fileLiberatoria = new DefaultStreamedContent(new ByteArrayInputStream(Base64.getDecoder().decode(spectatorToEdit.getLiberatoria().getPdfSignBase64Str())), "application/pdf", "liberatoria.pdf");
					}					
				}
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		if (spectatorToEdit!=null && spectatorToEdit.getSpectator().getSpectatorType()!=null) selectedSpectatorType = spectatorToEdit.getSpectator().getSpectatorType().getId();
		else selectedSpectatorType = null;
    }
 
    public void onSpectatorRowUnselect(UnselectEvent event) {
    	getLog().info("onSpectatorRowUnselect:" + ((SpectatorEpisode) event.getObject()).getId());
    	spectatorToEdit=null;
    	if (scanMonitoringThread!=null) {
    		scanMonitoringThread.stopMonitoring();
    		scanMonitoringThread=null;
    	}
    	fileLiberatoria = null;
    }
	
	
	
    public void checkouAllSelectedSpectators(ActionEvent e) {
		getLog().info("checkouAllSelectedSpectators");
			
		if (selectedSpectators!=null) {
			for (SpectatorEpisode sp : selectedSpectators) {
				spectatorToEdit=sp;
				spectatorToEdit = receptionService.getSpectatorEpisodeByIdNoLazy(spectatorToEdit.getId());
				try {
					spectatorToEdit.setCheckout(true);
					spectatorToEdit.setCheckoutDate(new Date());
					spectatorToEdit = receptionService.updateSpectatorEpisode(spectatorToEdit);
					
					// libero il badge
					removeBadgeFromSpectator(null);
					
				} catch (Throwable e1) {
					e1.printStackTrace();
					addWarningMessage("ERRORE salvataggio spettatore!");
				}
			}
			selectedSpectators.clear();
		}
		
		spectatorToEdit = null;

		// update gui
		episodeChanged();
	}
    
    
	
//	private Long testToDelete = null;
//	public void confirmDeleteTest(ActionEvent e) {
//    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
//    	for (UIComponent com : component.getChildren()) {
//    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("testId")) {
//    			testToDelete = ((Long)((UIParameter)com).getValue());
//			}
//		}
//    	
//	}
//	public void cancelDelete(ActionEvent ev) {
//	    	getLog().info("\nDelete Test canceled.");
//	    	testToDelete= null;
//	}
//	public void delete(ActionEvent ev) {
//    	getLog().info("\nDelete Test ...");
//    	try {
//        	
//    		spectatorService.deleteSpectator(testToDelete);
////    		employees=null;
//    		addInfoMessage(getMessagesBoundle().getString("employee.deleted"));
//		} catch (Exception e) {
//			e.printStackTrace();
//			addErrorMessage(getMessagesBoundle().getString("employee.deletingError"), e);
//		}
//    	
//	}
//	
//	
//	
//	
//	public void addTest(ActionEvent e) {
//		spectatorToEdit = new Spectator();
//	}
//	
//
//	public void modifyTest(ActionEvent e) {
//    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
//    	for (UIComponent com : component.getChildren()) {
//    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("testId")) {
//    			spectatorToEdit = spectatorService.getSpectatorById((Long)((UIParameter)com).getValue());
//			}
//		}
//	}
//	public void cancelEditTest(ActionEvent e) {
//		spectatorToEdit = null;
//	}
//	public void saveTest(ActionEvent e) {
//    	getLog().info("Salvo" + spectatorToEdit);
//    	
//
//    	if (spectatorToEdit.getName()==null || spectatorToEdit.getName().trim().equals("")) {
//    		addWarningMessage("nome spettatore richiesto!");
//    		return;
//    	}
//    	
//    	
//    	try {
//    		spectatorToEdit = spectatorService.updateSpectator(spectatorToEdit);
//		
//			addInfoMessage(getMessagesBoundle().getString("employee.saved"));
//		} catch (Exception e1) {
//			addErrorMessage(getMessagesBoundle().getString("employee.savingError"), e1);
//			e1.printStackTrace();
//		}
//    	spectatorToEdit = null;
//	}
//	
//

	
	
//	public void filterByDate() {
//		getLog().info("Filter by date !");
//		
//		Calendar cal = Calendar.getInstance();
//		if (filterFrom!=null) {
//			cal.setTime(filterFrom);
//			cal.set(Calendar.HOUR_OF_DAY, 0);
//			cal.set(Calendar.MINUTE, 0);
//			cal.set(Calendar.SECOND, 1);
//			filterFrom = cal.getTime();
//		}
//		if (filterTo!=null) {
//			cal.setTime(filterTo);
//			cal.set(Calendar.HOUR_OF_DAY, 23);
//			cal.set(Calendar.MINUTE, 59);
//			cal.set(Calendar.SECOND, 59);
//			filterTo = cal.getTime();
//		}
//		
//		
//		((LazySpectatorDataModel)lazyModelTest).setFilterFrom(filterFrom);
//		((LazySpectatorDataModel)lazyModelTest).setFilterTo(filterTo);
//	}
	



	// ******************* Barcode reader 
    public void changeCameraOrientation(ActionEvent e) {
    	Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
    	String orientation = params.get("orientation");
    	
    	getLog().info("Orientation changed to:" + orientation);
    	
    	if (orientation!=null && orientation.equals("landscape")) {
    		cameraWidth=CAMERA_WIDTH;
    		cameraHeight=CAMERA_HEIGHT;
    		cameraRotation="landscape";
    	}
    	else if (orientation!=null && orientation.equals("portrait")) {
    		cameraWidth=CAMERA_HEIGHT;
    		cameraHeight=CAMERA_WIDTH;
    		cameraRotation="portrait";
    	}
    }
    

    public void oncapture(CaptureEvent captureEvent) {
    	

    	InputStream in = null;
    	BufferedImage image = null;
        try {
        	//getLog().info("oncapture ... rowdata:" + captureEvent.getRawData());
        	getLog().info("oncapture ... ");
            if (captureEvent != null) {

            	
                           
                in = new ByteArrayInputStream(captureEvent.getData());
                image = ImageIO.read(in);
                
                //ImageIO.write(image, "jpg", new File("C:\\Users\\Crisma\\Desktop\\aaa\\aaa_"+System.currentTimeMillis()+".jpg"));
                
                String re =qrDecodeFromImage(image);
                if(re == null)
                {
                    //getLog().info("Image does not contain any barcode");
                }
                else
                {
                    getLog().info(re);
                    spectatorGuid=re;
                    closeCameraScanner(null);
                    RequestContext.getCurrentInstance().update("form_employees:spectatorGuid");
                    //RequestContext.getCurrentInstance().update("cameraScannerDlg");
                    RequestContext.getCurrentInstance().update("cameraScannerPanel");
                    
                    RequestContext.getCurrentInstance().execute("document.getElementById('form_employees\\:cerca_guid').click()"); // Trigger onchange.
                } 
               
            }
        } catch (Throwable ex) {
            ex.printStackTrace();
            try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
    }
    
    public String qrDecodeFromImage(BufferedImage img) {
        if(img!=null) {
            LuminanceSource bfImgLuminanceSource = new BufferedImageLuminanceSource(img);
            BinaryBitmap binaryBmp = new BinaryBitmap(new HybridBinarizer(bfImgLuminanceSource));
            
            QRCodeReader qrReader = new QRCodeReader();
            Result result;
            try {
                result = qrReader.decode(binaryBmp);
                return result.getText();
            } catch (Throwable e) {e.printStackTrace();} 
        }
        return null;
    }
    
	// **********************************

	
	


	public Date getFilterFrom() {
		return filterFrom;
	}

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public Date getFilterTo() {
		return filterTo;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}

	public LazySpectatorDataModel getLazyModelSpectator() {
		return lazyModelSpectator;
	}

	public void setLazyModelSpectator(LazySpectatorDataModel lazyModelSpectator) {
		this.lazyModelSpectator = lazyModelSpectator;
	}

	public SpectatorEpisode getSpectatorToEdit() {
		return spectatorToEdit;
	}

	public void setSpectatorToEdit(SpectatorEpisode spectatorToEdit) {
		this.spectatorToEdit = spectatorToEdit;
	}

	public String getSpectatorGuid() {
		return spectatorGuid;
	}

	public void setSpectatorGuid(String spectatorGuid) {
		this.spectatorGuid = spectatorGuid;
	}

	public boolean isScannerVisible() {
		return scannerVisible;
	}

	public void setScannerVisible(boolean scannerVisible) {
		this.scannerVisible = scannerVisible;
	}

	public String getCameraWidth() {
		return cameraWidth;
	}

	public void setCameraWidth(String cameraWidth) {
		this.cameraWidth = cameraWidth;
	}

	public String getCameraHeight() {
		return cameraHeight;
	}

	public void setCameraHeight(String cameraHeight) {
		this.cameraHeight = cameraHeight;
	}

	public String getCameraRotation() {
		return cameraRotation;
	}

	public void setCameraRotation(String cameraRotation) {
		this.cameraRotation = cameraRotation;
	}

	public Long getSelectedSpectatorType() {
		return selectedSpectatorType;
	}

	public void setSelectedSpectatorType(Long selectedSpectatorType) {
		this.selectedSpectatorType = selectedSpectatorType;
	}

	public List<SelectItem> getSpectatorTypes() {
		return spectatorTypes;
	}
	
	@Override
	public synchronized void signReceived(Location desk, String sign) {
		
		getLog().info("Ricevuta firma da desk:" + desk);
		
		if (lastHeartbeat==null || ((new Date()).getTime()-lastHeartbeat.getTime())> (1000*11)) { // se non ricevo heartbeat per più di 11 secondi
			getLog().info("!!! bean non collegato alla pagina (tab o browser chiuso dall'utente senza aver fatto logout) !!!  ... aspetto che scada la sessione");
			// se non sono collegato pulisco la lista spettatori per liberare spazio
			//spectators = null; TODO !!!
//			destroy();
			doDestroy();
			return;
		}
		
		// Controllo se è il mio desk !!!!!!!!!
		if (desk==null || desk.getId()==null || !desk.getId().equals(selectedDesk)) {
			getLog().info("non è il mio desk!");
			return;
		}
		
		String error = null;
		
		if (spectatorToEdit==null || spectatorToEdit.getId()==null) error = "Selezionare uno spettatore per ricevere la firma!";
		else {
			// Spettatore selezionato
			
			spectatorToEdit = receptionService.getSpectatorEpisodeByIdNoLazy(spectatorToEdit.getId());
			
			
			getLog().info("Doc identità....");
			// Se c'è carico documento identità
			byte[] doc = null;
			// transformiamo il file appena caricato in Blob
			try {
//				DocumentSpectator ds = documentSpectatorService.getDocumentSpectatorFinder().and("spectator.id").eq(spectatorToEdit.getSpectator().getId()).and("documentType.idDocumentType").eq(new Integer(1)).setDistinct().result();
				DocumentSpectator ds = null;
				List<DocumentSpectator> dss = documentSpectatorService.getDocumentSpectatorFinder().and("spectator.id").eq(spectatorToEdit.getSpectator().getId()).and("documentType.idDocumentType").eq(new Integer(1)).setDistinct().list();
				if (dss!=null && dss.size()>0) ds = dss.get(0);
				
				if (ds!=null) {
					doc = ds.getDocInBlob();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			getLog().info("Doc identità....FINE");
			
			
			
			
			error = "";
			
			spectatorToEdit.setSign(sign);
			
//			String filesDir = PropertiesUtils.getInstance().readProperty("allegati.uploaded.document.dir");
			
			// GENERO PDF
			try {
				
				JasperReportDetails det = new JasperReportDetails();
	            // Nome report
	            det.setDescription("Contract");
	            // Path report
	            det.setMasterReportUrl(File.separator+"WEB-INF"+File.separator+"reports"+File.separator+"liberatoria_new.jasper");
	            // Subreports url
	            Properties srUrls = new Properties();
	            //srUrls.put("SUBREPORT_URL", File.separator+"WEB-INF"+File.separator+"reports"+File.separator+"contracts"+File.separator+"subRep.jasper");
	            det.setSubReportUrls(srUrls);
	            // Parametri da passare al report
	            Map<String, Object> reportParameters = new HashMap<String, Object>();
	            
	            reportParameters.put("REPORT_RESOURCES_PATH", getAbsolutePath()+ File.separator+"WEB-INF"+File.separator+"reports"+File.separator);
	            
	            
	            Location des = locationService.getLocationById(selectedDesk);
	            reportParameters.put("DESK", des);
	            reportParameters.put("STUDIO", des.getProductionCenter());
	            reportParameters.put("SIGN", new java.io.ByteArrayInputStream(redrawSignature(extractSignature(sign))));
	            
	            if (doc!=null)
	            	reportParameters.put("ID_DOCUMENT", new java.io.ByteArrayInputStream(doc));
	            else reportParameters.put("ID_DOCUMENT", null);
	            
	            
	            det.setReportParameters(reportParameters);
	            
	            // Dati da visualizzare (un oggetto per ogni areaDetail in iReport)
	            Collection<SpectatorEpisode> beans = new ArrayList<SpectatorEpisode>();
//	            Collection<ReportContratto> beans = new ArrayList<ReportContratto>();
//	            beans.add(reportContratto);
	            beans.add(spectatorToEdit);

	            Map<String, Object> reportParams = new HashMap<String, Object>();
	            reportParams.put("REPORT_TITLE", "Liberatoria/Consenso");

	            ByteArrayOutputStream baos = jasperReportsFactory.complieReportAsPdfByteArrayOutputStream(det, reportParams, beans);
	            
	            // Solo per test !!!!!!!!!!!!!!!!
//	            FileUtils.writeToFile("C:\\Users\\Crisma\\Desktop\\liberatoria.pdf", new ByteArrayInputStream(baos.toByteArray()));
	            
//	            spectatorToEdit.setPdfSign(baos.toByteArray());
	            Liberatoria lib = spectatorToEdit.getLiberatoria();
	            if (lib==null) lib = new Liberatoria();
	            lib.setPdfSignBase64Str(Base64.getEncoder().encodeToString(baos.toByteArray()));
	            lib.setSpectatorEpisode(spectatorToEdit);
	            spectatorToEdit.setLiberatoria(lib);
	            
	            //spectatorToEdit.setPdfSignBase64Str(Base64.getEncoder().encodeToString(baos.toByteArray()));
	        }catch (Exception e){
	            e.printStackTrace();
	        }
			
			getLog().info("Fine generazione pdf");
			
			try {
				getLog().info("update spectator....");
//				Siccome aggiorno la firma allora imposto la data della firma
				spectatorToEdit.setSignDate(new Date());
				
				spectatorToEdit = receptionService.updateSpectatorEpisodeNoLazy(spectatorToEdit);
				getLog().info("done!");
				
				if (doc!=null) {
					ByteArrayContent cont = new ByteArrayContent(doc);
					cont.setName("Document ID");
					spectatorToEdit.getSpectator().setDocumentFileDownload(cont);
				}
				
				/*if (spectatorToEdit.getPdfSign()!=null) {
					fileLiberatoria = new DefaultStreamedContent(new ByteArrayInputStream(spectatorToEdit.getPdfSign()), "application/pdf", "liberatoria.pdf");
				}*/
				if (spectatorToEdit.getLiberatoria()!=null && spectatorToEdit.getLiberatoria().getPdfSignBase64Str()!=null) {
					fileLiberatoria = new DefaultStreamedContent(new ByteArrayInputStream(Base64.getDecoder().decode(spectatorToEdit.getLiberatoria().getPdfSignBase64Str())), "application/pdf", "liberatoria.pdf");
				}
				setFocusOnCheckin();
				
				getLog().info("fine caricamento docs");
			} catch (Throwable e1) {
				e1.printStackTrace();
				error = "ERRORE salvataggio firma!";
			}
			
			
//			// TEST - Salvo immagine firma
//		    try {
//				byte[] imageBytes =  redrawSignature(extractSignature(sign));
//				Path path = Paths.get("C:\\Users\\Crisma\\Desktop\\sign.png");
//				Files.write(path, imageBytes);
//			} catch (IOException e) {
//				e.printStackTrace();
//			} 
		}
		
		// ATT ... se error è null, allora non scatta l'ajax update lato pagina
		
		// Notifico il mio browser (legato alla sessionId)
		EventBus eventBus = EventBusFactory.getDefault().eventBus();
		eventBus.publish("/sign/"+ sessionId, error);
	}

	
	
	
	
	
	
	
	
	
	
	
	
	  public String getAbsolutePath() throws UnsupportedEncodingException {

			String path = this.getClass().getClassLoader().getResource("").getPath();
			String fullPath = URLDecoder.decode(path, "UTF-8");
			String pathArr[] = fullPath.split("/WEB-INF/classes/");
//			getLog().info(fullPath);
//			getLog().info(pathArr[0]);
			fullPath = pathArr[0];
			String reponsePath = "";
			// to read a file from webcontent
			//reponsePath = new File(fullPath).getPath() + File.separatorChar + "newfile.txt";
			reponsePath = new File(fullPath).getPath();
//			getLog().info("getAbsolutePath:" + reponsePath);
			return reponsePath;

		}
		
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
    private static final String IMAGE_FORMAT = "png"; 
    private static final int SIGNATURE_HEIGHT = 400; 
    private static final int SIGNATURE_WIDTH = 1000; 
    /** 
     * A point along a line within a signature. 
     */ 
    private static class Point { 
        private int x; 
        private int y; 
        public Point(float x, float y) { 
            this.x = Math.round(x); 
            this.y = Math.round(y); 
        } 
    }
    /** 
     * Extract the signature lines and points from the JSON encoding. 
     * 
     * @param  jsonEncoding  the JSON representation of the signature 
     * @return  the retrieved lines and points 
     */ 
    private static List<List<Point>> extractSignature(String jsonEncoding) { 
        List<List<Point>> lines = new ArrayList<List<Point>>(); 
        Matcher lineMatcher = 
                Pattern.compile("(\\[(?:,?\\[-?[\\d\\.]+,-?[\\d\\.]+\\])+\\])"). 
                matcher(jsonEncoding); 
        while (lineMatcher.find()) { 
            Matcher pointMatcher = 
                    Pattern.compile("\\[(-?[\\d\\.]+),(-?[\\d\\.]+)\\]"). 
                    matcher(lineMatcher.group(1)); 
            List<Point> line = new ArrayList<Point>(); 
            lines.add(line); 
            while (pointMatcher.find()) { 
                line.add(new Point(Float.parseFloat(pointMatcher.group(1)), 
                        Float.parseFloat(pointMatcher.group(2)))); 
            } 
        } 
        return lines; 
    } 
	
    /** 
     * Redraw the signature from its lines definition. 
     * 
     * @param  lines  the individual lines in the signature 
     * @return  the corresponding signature image 
     * @throws  IOException  if a problem generating the signature 
     */ 
    private static byte[] redrawSignature(List<List<Point>> lines) throws IOException { 
        BufferedImage signature = new BufferedImage( 
                SIGNATURE_WIDTH, SIGNATURE_HEIGHT, BufferedImage.TYPE_BYTE_GRAY); 
        Graphics2D g = (Graphics2D)signature.getGraphics(); 
        g.setColor(Color.WHITE); 
        g.fillRect(0, 0, signature.getWidth(), signature.getHeight()); 
        g.setColor(Color.BLACK); 
        g.setStroke(new BasicStroke(2, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND)); 
        g.setRenderingHint( 
                RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON); 
        Point lastPoint = null; 
        for (List<Point> line : lines) { 
            for (Point point : line) { 
                if (lastPoint != null) { 
                    g.drawLine(lastPoint.x, lastPoint.y, point.x, point.y); 
                } 
                lastPoint = point; 
            } 
            lastPoint = null; 
        } 
        ByteArrayOutputStream output = new ByteArrayOutputStream(); 
        ImageIO.write(signature, IMAGE_FORMAT, output); 
//        ImageIO.write(signature, IMAGE_FORMAT, output); 
        
        
//        ImageManipulationUtils imu = new ImageManipulationUtils(output.toByteArray());
//        imu.resize(signature.getWidth()/4, signature.getHeight()/4, false);
//        return imu.getByteArray();
        
        return output.toByteArray(); 
    }

	public Long getSelectedDesk() {
		return selectedDesk;
	}

	public void setSelectedDesk(Long selectedDesk) {
		this.selectedDesk = selectedDesk;
	}

	public List<SelectItem> getDesks() {
		return desks;
	}

	public void setDesks(List<SelectItem> desks) {
		this.desks = desks;
	}

	public DonutChartModel getCheckinChartModel() {
		return checkinChartModel;
	}

	public void setCheckinChartModel(DonutChartModel checkinChartModel) {
		this.checkinChartModel = checkinChartModel;
	}

	public String getChartTitle() {
		return chartTitle;
	}

	public void setChartTitle(String chartTitle) {
		this.chartTitle = chartTitle;
	}

	public String getChartSubTitle() {
		return chartSubTitle;
	}

	public void setChartSubTitle(String chartSubTitle) {
		this.chartSubTitle = chartSubTitle;
	}

	public Long getSelectedEpisode() {
		return selectedEpisode;
	}

	public void setSelectedEpisode(Long selectedEpisode) {
		this.selectedEpisode = selectedEpisode;
	}

	public List<SelectItem> getEpisodes() {
		return episodes;
	}

	public void setEpisodes(List<SelectItem> episodes) {
		this.episodes = episodes;
	}

	public List<SpectatorEpisode> getSpectators() {
		//getLog().info("getSpectators...");
		return spectators;
	}

	//public synchronized boolean scanReceived(Path child) {
	public synchronized boolean scanReceived(String child) {
		getLog().info("ReceptionBean - scan detected:" + child);
		
		if (lastHeartbeat==null || ((new Date()).getTime()-lastHeartbeat.getTime())> (1000*11)) { // se non ricevo heartbeat per più di 11 secondi
			getLog().info("!!! bean non collegato alla pagina (tab o browser chiuso dall'utente senza aver fatto logout) !!!  ... aspetto che scada la sessione");
//			destroy();
			doDestroy();
			return false;
		}
		
		String file = child.toString();
		
		//getLog().info("1");
		spectatorToEdit = receptionService.getSpectatorEpisodeByIdNoLazy(spectatorToEdit.getId());
		//getLog().info("2");
		
		byte[] doc = null;
		// transformiamo il file appena caricato in Blob
		try {
			doc = FileUtils.getBytesFromFile(file);

			// salviamo il documento sul DB
			// Ricarico
			//spectatorToEdit = receptionService.getSpectatorEpisodeById(spectatorToEdit.getId());
			DocumentSpectator ds = documentSpectatorService.getDocumentSpectatorBySpectatorIdAndDocumentTypeId(spectatorToEdit.getSpectator().getId(), new Integer(1));
			//getLog().info("3");
			if (ds!=null) {
				//getLog().info("3del");
				documentSpectatorService.deleteDocumentSpectator(ds.getId());
				//getLog().info("3del2");
				ds = null;
			}
			if (ds==null) {
				//getLog().info("3a");
				DocumentType documentTypeToAdd = documentTypeService.getDocumentTypeByTypeId(new Integer(1));
				//getLog().info("3aa");
				DocumentSpectator docsp = new DocumentSpectator(documentTypeToAdd, spectatorToEdit.getSpectator());
				//getLog().info("3b");
				docsp.setDocInBlob(doc);
				docsp.setDescription("scanner_upload.jpg");
				docsp.setDocumentCreationDate(new Date());
				//getLog().info("3c");
				docsp = documentSpectatorService.addDocumentSpectator(docsp);
				//addInfoMessage(getMessagesBoundle().getString("documentSpectator.saved"));
				//getLog().info("3d");
			}
//			else {
//				ds.setDocInBlob(doc);
//				ds.setDescription("scanner_upload.jpg");
//				ds = documentSpectatorService.updateDocumentSpectator(ds);
//			}

		} catch (Exception e) {
			e.printStackTrace();
		}


		// Carico il file
		

		try {
			//getLog().info("4");
			// GENERO PDF (se c'è già la firma)
			if (spectatorToEdit.getSign()!=null && !spectatorToEdit.getSign().equals("") && doc!=null) {
				try {
					getLog().info("Genero pdf...");
					JasperReportDetails det = new JasperReportDetails();
		            // Nome report
		            det.setDescription("Contract");
		            // Path report
		            det.setMasterReportUrl(File.separator+"WEB-INF"+File.separator+"reports"+File.separator+"liberatoria_new.jasper");
		            // Subreports url
		            Properties srUrls = new Properties();
		            //srUrls.put("SUBREPORT_URL", File.separator+"WEB-INF"+File.separator+"reports"+File.separator+"contracts"+File.separator+"subRep.jasper");
		            det.setSubReportUrls(srUrls);
		            // Parametri da passare al report
		            Map<String, Object> reportParameters = new HashMap<String, Object>();
		            
		            reportParameters.put("REPORT_RESOURCES_PATH", getAbsolutePath()+ File.separator+"WEB-INF"+File.separator+"reports"+File.separator);
		            
		            
		            Location des = locationService.getLocationById(selectedDesk);
		            reportParameters.put("DESK", des);
		            reportParameters.put("STUDIO", des.getProductionCenter());
		            reportParameters.put("SIGN", new java.io.ByteArrayInputStream(redrawSignature(extractSignature(spectatorToEdit.getSign()))));
		            
//		            if (spectatorToEdit.getSpectator().getDocumentPath()!=null && !spectatorToEdit.getSpectator().getDocumentPath().equals(""))
//		            	reportParameters.put("ID_DOCUMENT", new java.io.ByteArrayInputStream(FileUtils.getBytesFromFile(filesDir+"/"+spectatorToEdit.getSpectator().getDocumentPath())));
//		            else reportParameters.put("ID_DOCUMENT", null);
		            if (doc!=null)
		            	reportParameters.put("ID_DOCUMENT", new java.io.ByteArrayInputStream(doc));
//		            else reportParameters.put("ID_DOCUMENT", null);
		            
		            det.setReportParameters(reportParameters);
		            
		            // Dati da visualizzare (un oggetto per ogni areaDetail in iReport)
		            Collection<SpectatorEpisode> beans = new ArrayList<SpectatorEpisode>();
	//	            Collection<ReportContratto> beans = new ArrayList<ReportContratto>();
	//	            beans.add(reportContratto);
		            beans.add(spectatorToEdit);
	
		            Map<String, Object> reportParams = new HashMap<String, Object>();
		            reportParams.put("REPORT_TITLE", "Liberatoria/Consenso");
		            
		            //getLog().info("4 a");
		    		
		            
		            ByteArrayOutputStream baos = jasperReportsFactory.complieReportAsPdfByteArrayOutputStream(det, reportParams, beans);
		            
		            //getLog().info("4 b");
		            // Solo per test !!!!!!!!!!!!!!!!
	//	            FileUtils.writeToFile("C:\\Users\\Crisma\\Desktop\\liberatoria.pdf", new ByteArrayInputStream(baos.toByteArray()));

		            //getLog().info("4 c0"); 
		            spectatorToEdit = receptionService.getSpectatorEpisodeByIdNoLazy(spectatorToEdit.getId());
		    		//getLog().info("4 c1"); 
		    		
	//	            spectatorToEdit.setPdfSign(baos.toByteArray());
		            Liberatoria lib = spectatorToEdit.getLiberatoria();
		            if (lib==null) lib = new Liberatoria();
		            lib.setSpectatorEpisode(spectatorToEdit);
		            lib.setPdfSignBase64Str(Base64.getEncoder().encodeToString(baos.toByteArray()));
		            
		            //getLog().info("4 c2"); 
		            if (lib.getId()==null) lib = spectatorEpisodeService.addLiberatoria(lib);
		            else lib = spectatorEpisodeService.updateLiberatoria(lib);
		            //getLog().info("4 c3"); 
		            // Ricarico per prendere la liberatoria! workaround
		    		//getLog().info("4 c4"); 
		            spectatorToEdit = receptionService.getSpectatorEpisodeByIdNoLazy(spectatorToEdit.getId());
		    		//getLog().info("4 c5"); 
//					spectatorToEdit = receptionService.updateSpectatorEpisode(spectatorToEdit);
//					getLog().info("4 e"); 
					
		    		//getLog().info(spectatorToEdit.getLiberatoria()); 
		    		//getLog().info("4 c6"); 
		    		
					if (spectatorToEdit.getLiberatoria()!=null && spectatorToEdit.getLiberatoria().getPdfSignBase64Str()!=null) {
						fileLiberatoria = new DefaultStreamedContent(new ByteArrayInputStream(Base64.getDecoder().decode(spectatorToEdit.getLiberatoria().getPdfSignBase64Str())), "application/pdf", "liberatoria.pdf");
					}
					//getLog().info("4 f"); 
				} catch (Throwable e1) {
					e1.printStackTrace();
					addWarningMessage("ERRORE salvataggio liberatoria!");
				}
			}
			
			//getLog().info("5");
			
			try {
//				if (spectatorToEdit.getSpectator().getDocumentPath()!=null && !spectatorToEdit.getSpectator().getDocumentPath().equals("")) {
//					ByteArrayContent cont = new ByteArrayContent(FileUtils.getBytesFromFile(filesDir+"/"+spectatorToEdit.getSpectator().getDocumentPath()));
				if (doc!=null) {
					ByteArrayContent cont = new ByteArrayContent(doc);
					cont.setName("Document ID");
					spectatorToEdit.getSpectator().setDocumentFileDownload(cont);
				}
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			
			//getLog().info("6");
			// Notifico il mio browser (legato alla sessionId)
			EventBus eventBus = EventBusFactory.getDefault().eventBus();
			eventBus.publish("/sign/"+ sessionId, "");
			
			getLog().info("Fine scanReceived");
			//addInfoMessage(getMessagesBoundle().getString("attacchement.added"), "form_edit:messagesMasterCompanyAllegati5");
		} catch (Throwable e) {
			addWarningMessage("ERRORE salvataggio spettatore!");
			e.printStackTrace();
		}
		
		return true;
	}

	@Override
	public Location getDesk() {
		if (selectedDesk!=null) return locationService.getLocationById(selectedDesk);
		else return null;
	}

	@Override
	public SpectatorEpisode getSelectedSpectator() {
		
		if (lastHeartbeat==null || ((new Date()).getTime()-lastHeartbeat.getTime())> (1000*11)) { // se non ricevo heartbeat per più di 11 secondi
			getLog().info("!!! bean non collegato alla pagina (tab o browser chiuso dall'utente senza aver fatto logout) !!!  ... aspetto che scada la sessione");
			// se non sono collegato pulisco la lista spettatori per liberare spazio
			//spectators = null; TODO !!!
//			destroy();
			doDestroy();
			return null;
		}
		
		return spectatorToEdit;
	}

	public StreamedContent getFileLiberatoria() {
		return fileLiberatoria;
	}

	public Date getEpisodeDate() {
		return episodeDate;
	}

	public void setEpisodeDate(Date episodeDate) {
		this.episodeDate = episodeDate;
	}

	public List<Episode> getEpisodesList() {
		return episodesList;
	}

	@Override
	public synchronized void eventReceived(Location location, Media media, String errorMsg, String badgeId) {
		getLog().info("Ricevuto evento dal desktop reader desk:" + location + "   media:" + media + "   Error:" + errorMsg);
		
		
		if (lastHeartbeat==null || ((new Date()).getTime()-lastHeartbeat.getTime())> (1000*11)) { // se non ricevo heartbeat per più di 11 secondi
			getLog().info("!!! bean non collegato alla pagina (tab o browser chiuso dall'utente senza aver fatto logout) !!!  ... aspetto che scada la sessione");
//			destroy();
			doDestroy();
			return;
		}
		// Controllo se è il mio desk !!!!!!!!!
		if (location==null || location.getId()==null || !location.getId().equals(selectedDesk)) {
			getLog().info("non è il mio desk!");
			return;
		}
		
		if (errorMsg==null) {
			String error = null;
//			getLog().info("1");
			Episode ep = getEpisode();
//			getLog().info("2");
			if ((spectatorToEdit==null || spectatorToEdit.getId()==null) && ep!=null && ep.isCheckoutStarted()) {
				
				if (media.getMediaStatusType().getIdStatus()==2) { // stato assegnato
					error = "!checkout!";
				
					//prendere spettatore per codice badge assegnato e poi fare il checkout
					media = mediaService.getMediaById(media.getId());
					spectatorToEdit = receptionService.getSpectatorEpisodeFinder().and("media").eq(media).setDistinct().result();
					if (spectatorToEdit==null) {
						error = "Badge non associato a nessuno spettatore!";
						spectatorRowNum=-1;
					}
					else {
						if (selectedSpectators!=null) selectedSpectators.clear();
						checkoutSpectator(null);
						
						
//						if (getEpisode().isCheckinStarted() && !getEpisode().isCheckoutStarted() && !allOrPrecheckinOnly) {
//							spectators = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(selectedEpisode).and("preCheckin").eq(Boolean.TRUE).setDistinct().list();
//						}
//						else if (getEpisode().isCheckinStarted() && !getEpisode().isCheckoutStarted() && allOrPrecheckinOnly) {
//							spectators = getEpisode().getSpectators();
//						}
//						else if (getEpisode().isCheckinStarted() && getEpisode().isCheckoutStarted() && !allOrCheckinOnly) {
//							spectators = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(selectedEpisode).and("preCheckin").eq(Boolean.TRUE).setDistinct().list();
//						}
//						else if (getEpisode().isCheckinStarted() && getEpisode().isCheckoutStarted() && allOrCheckinOnly) spectators = getEpisode().getSpectators();
						
						spectatorRowNum = filteredTableList!=null?filteredTableList.indexOf(spectatorToEdit):spectators.indexOf(spectatorToEdit); //aggiorno indice ! perchè nel frattempo potrebbe essere arrivato ad esempio un transito
//						RequestContext.getCurrentInstance().update(":form_employees:updateStatisticAndList");
					}
				}
				else {
					error = "Impossibile effettuare il checkout di un badge che sio trova nello stato " + media.getMediaStatusType().getDescription();
					spectatorRowNum=-1;
				}
			}
			else if (spectatorToEdit!=null && spectatorToEdit.getId()!=null){
				// Spettatore selezionato
//				getLog().info("3");
				spectatorToEdit = receptionService.getSpectatorEpisodeByIdNoLazy(spectatorToEdit.getId());
				SpectatorEpisode oldSpect = SpectatorEpisode.copy(spectatorToEdit);
//				getLog().info("4");
				error = "";
				
				if (media.getMediaStatusType().getIdStatus()==1) { // stato libero

//					getLog().info("5");
					media = mediaService.getMediaById(media.getId());
//					getLog().info("6");
					spectatorToEdit.setMedia(media);
					try {
						spectatorToEdit = receptionService.updateSpectatorEpisodeNoLazy(spectatorToEdit);
						SpectatorEpisode newSpect =SpectatorEpisode.copy(spectatorToEdit);
						auditService.addAudit(new Audit(userSession.getLastName() + " " + userSession.getFirstName()+" ("+navigationBean.getUserName()+")", "AssignBadge", "SpectatorEpisode", spectatorToEdit.getId(), "", "", "", "", oDiff.getDiff(oldSpect, newSpect)));
						oldSpect=null;
						newSpect=null;
//						getLog().info("7");
						// Ricarico il documento di identità
						try {
							DocumentSpectator ds = null;
//							ds = documentSpectatorService.getDocumentSpectatorFinder().and("spectator.id").eq(spectatorToEdit.getSpectator().getId()).and("documentType.idDocumentType").eq(new Integer(1)).setDistinct().result();
							List<DocumentSpectator> dss = documentSpectatorService.getDocumentSpectatorFinder().and("spectator.id").eq(spectatorToEdit.getSpectator().getId()).and("documentType.idDocumentType").eq(new Integer(1)).setDistinct().list();
							if (dss!=null && dss.size()>0) ds = dss.get(0);

							if (ds!=null) {
								ByteArrayContent cont = new ByteArrayContent(ds.getDocInBlob());
								cont.setName(ds.getDescription());
								spectatorToEdit.getSpectator().setDocumentFileDownload(cont);
							}
						} catch (Exception e1) {
							e1.printStackTrace();
						}
//						getLog().info("8");
						media.setMediaStatusType(mediaStatusTypeService.getMediaStatusTypeByIdStatus(2)); // stato assegnato
						mediaService.updateMedia(media);
						setFocusOnCheckin();
//						getLog().info("9");
					} catch (Exception e1) {
						addWarningMessage("ERRORE salvataggio spettatore!");
						e1.printStackTrace();
						//error = "ERRORE salvataggio spettatore!";
					}
					
//					getLog().info("10");
					spectatorRowNum = filteredTableList!=null?filteredTableList.indexOf(spectatorToEdit):spectators.indexOf(spectatorToEdit); //aggiorno indice ! perchè nel frattempo potrebbe essere arrivato ad esempio un transito

//					spectatorToEdit.setOrder(spectators.get(spectatorRowNum).getOrder());	
					if (spectators.indexOf(spectatorToEdit)>-1) spectatorToEdit.setOrder(spectators.get(spectators.indexOf(spectatorToEdit)).getOrder());
//					spectators.set(spectatorRowNum, spectatorToEdit); // Rimpiazzo in modo da aggiornare dati
					if (spectators.indexOf(spectatorToEdit)>-1) spectators.set(spectators.indexOf(spectatorToEdit), spectatorToEdit); // Rimpiazzo in modo da aggiornare dati
					if (filteredTableList!=null && filteredTableList.indexOf(spectatorToEdit)>-1) filteredTableList.set(filteredTableList.indexOf(spectatorToEdit), spectatorToEdit);

//					RequestContext.getCurrentInstance().update(":form_employees:updateStatisticAndList");
//					RequestContext.getCurrentInstance().update(":form_employees:updateSpectsList");
				} else {
					error = "Impossibile assegnare il badge. Si trova nello stato: " + media.getMediaStatusType().getDescription();
					spectatorRowNum=-1;
				}
			} else {
				error = "Azione non permessa!";
				spectatorRowNum=-1;
			}
//			GetLog().info("11");
//			ATT ... se error è null, allora non scatta l'ajax update lato pagina.

//			Notifico il mio browser (legato alla sessionId)
			EventBus eventBus = EventBusFactory.getDefault().eventBus();
			eventBus.publish("/sign/"+ sessionId, error);
//			getLog().info("12");
		} else {
//			Notifico il mio browser (legato alla sessionId)
			EventBus eventBus = EventBusFactory.getDefault().eventBus();
			eventBus.publish("/sign/"+ sessionId, errorMsg);
		}
		getLog().info("Fine eventReceived");
	}

	public List<SpectatorEpisode> getSelectedSpectators() {
		return selectedSpectators;
	}

	public void setSelectedSpectators(List<SpectatorEpisode> selectedSpectators) {
		this.selectedSpectators = selectedSpectators;
	}

	public boolean isAllOrPrecheckinOnly() {
		return allOrPrecheckinOnly;
	}

	public void setAllOrPrecheckinOnly(boolean allOrPrecheckinOnly) {
		this.allOrPrecheckinOnly = allOrPrecheckinOnly;
	}

	public boolean isAllOrCheckinOnly() {
		return allOrCheckinOnly;
	}

	public void setAllOrCheckinOnly(boolean allOrCheckinOnly) {
		this.allOrCheckinOnly = allOrCheckinOnly;
	}

	@Override
	public synchronized void transitCheckoutReceived(SpectatorEpisode sep) {
		getLog().info("Ricevuto transito in uscita (checkout) per spettatore:" + sep.getSpectator().getFullName());
		
		
		if (lastHeartbeat==null || ((new Date()).getTime()-lastHeartbeat.getTime())> (1000*11)) { // se non ricevo heartbeat per più di 11 secondi
			getLog().info("!!! bean non collegato alla pagina (tab o browser chiuso dall'utente senza aver fatto logout) !!!  ... aspetto che scada la sessione");
//			destroy();
			doDestroy();
			return;
		}
		
		// se non è il mio episodio lo scarto
		if (getEpisode()!=null && getEpisode().getId()!=null && getEpisode().getId().longValue()!=sep.getEpisode().getId().longValue()) {
			getLog().info(" non è il mio episodio lo scarto.... (episodio corrente "+getEpisode().getId()+"   episodio ricevuto: "+sep.getEpisode().getId()+") ");
			return;
		}
				
		//if (selectedSpectators!=null) selectedSpectators.clear();
		
		
//		if (getEpisode()!=null && getEpisode().isCheckinStarted() && !getEpisode().isCheckoutStarted() && !allOrPrecheckinOnly) {
//			spectators = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(selectedEpisode).and("preCheckin").eq(Boolean.TRUE).setDistinct().list();
//		}
//		else if (getEpisode()!=null && getEpisode().isCheckinStarted() && !getEpisode().isCheckoutStarted() && allOrPrecheckinOnly) {
//			spectators = getEpisode().getSpectators();
//		}
//		else if (getEpisode()!=null && getEpisode().isCheckinStarted() && getEpisode().isCheckoutStarted() && !allOrCheckinOnly) {
//			spectators = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(selectedEpisode).and("preCheckin").eq(Boolean.TRUE).setDistinct().list();
//		}
//		else if (getEpisode()!=null && getEpisode().isCheckinStarted() && getEpisode().isCheckoutStarted() && allOrCheckinOnly) spectators = getEpisode().getSpectators();
		
			
		//aggiorno statistiche uscita
//		long tt = System.currentTimeMillis();
		if (spectators!=null) {
			if (selectedEpisode!=null) episodeSpectatorsNumsWithCheckout = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(selectedEpisode).and("checkout").eq(Boolean.TRUE).setDistinct().count();
	//		getLog().info("tempo per query: " + (System.currentTimeMillis()-tt));
				
			spectatorRowNum = filteredTableList!=null?filteredTableList.indexOf(sep):spectators.indexOf(sep); //aggiorno indice ! perchè nel frattempo potrebbe essere arrivato ad esempio un transito
	//		sep.setOrder(spectators.get(spectatorRowNum).getOrder());
			if (spectators.indexOf(sep)>-1) sep.setOrder(spectators.get(spectators.indexOf(sep)).getOrder());
			
	//		spectators.set(spectatorRowNum, sep); // Rimpiazzo in modo da aggiornare dati
			if (spectators.indexOf(sep)>-1) spectators.set(spectators.indexOf(sep), sep); // Rimpiazzo in modo da aggiornare dati
			if (filteredTableList!=null && filteredTableList.indexOf(sep)>-1) filteredTableList.set(filteredTableList.indexOf(sep), sep);
			
	//		RequestContext.getCurrentInstance().update(":form_employees:updateStatisticAndList");
			
			
			
			// ATT ... se error è null, allora non scatta l'ajax update lato pagina
			getLog().info("notifico checkuot....");
			// Notifico il mio browser (legato alla sessionId)
			EventBus eventBus = EventBusFactory.getDefault().eventBus();
			eventBus.publish("/sign/"+ sessionId, "transitCheckout");
		}
	}

	@Override
	public synchronized void transitCheckinReceived(SpectatorEpisode sep) {
		getLog().info("Ricevuto transito in entrata (checkin) per spettatore:" + sep.getSpectator().getFullName());
		
		
		if (lastHeartbeat==null || ((new Date()).getTime()-lastHeartbeat.getTime())> (1000*11)) { // se non ricevo heartbeat per più di 11 secondi
			getLog().info("!!! bean non collegato alla pagina (tab o browser chiuso dall'utente senza aver fatto logout) !!!  ... aspetto che scada la sessione");
//			destroy();
			doDestroy();
			return;
		}
		
		// se non è il mio episodio lo scarto
		if (getEpisode()!=null && sep.getEpisode().getId()!=null && getEpisode().getId().longValue()!=sep.getEpisode().getId().longValue()) return;
		
		
		//if (selectedSpectators!=null) selectedSpectators.clear();
		
//		
//		if (getEpisode()!=null && getEpisode().isCheckinStarted() && !getEpisode().isCheckoutStarted() && !allOrPrecheckinOnly) {
//			spectators = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(selectedEpisode).and("preCheckin").eq(Boolean.TRUE).setDistinct().list();
//		}
//		else if (getEpisode()!=null && getEpisode().isCheckinStarted() && !getEpisode().isCheckoutStarted() && allOrPrecheckinOnly) {
//			spectators = getEpisode().getSpectators();
//		}
//		else if (getEpisode()!=null && getEpisode().isCheckinStarted() && getEpisode().isCheckoutStarted() && !allOrCheckinOnly) {
//			spectators = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(selectedEpisode).and("preCheckin").eq(Boolean.TRUE).setDistinct().list();
//		}
//		else if (getEpisode()!=null && getEpisode().isCheckinStarted() && getEpisode().isCheckoutStarted() && allOrCheckinOnly) spectators = getEpisode().getSpectators();
		
			
		if (spectators!=null) {
			
			spectatorRowNum = filteredTableList!=null?filteredTableList.indexOf(sep):spectators.indexOf(sep); //aggiorno indice ! perchè nel frattempo potrebbe essere arrivato ad esempio un transito
	//		sep.setOrder(spectators.get(spectatorRowNum).getOrder());
			if (spectators.indexOf(sep)>-1) sep.setOrder(spectators.get(spectators.indexOf(sep)).getOrder());
	//		spectators.set(spectatorRowNum, sep); // Rimpiazzo in modo da aggiornare dati
			if (spectators.indexOf(sep)>-1) spectators.set(spectators.indexOf(sep), sep); // Rimpiazzo in modo da aggiornare dati
			if (filteredTableList!=null && filteredTableList.indexOf(sep)>-1) filteredTableList.set(filteredTableList.indexOf(sep), sep);
			
	//		RequestContext.getCurrentInstance().update(":form_employees:updateStatisticAndList");
			
			
			// ATT ... se error è null, allora non scatta l'ajax update lato pagina
			
			// Notifico il mio browser (legato alla sessionId)
			getLog().info("notifico checkin....  entranceDate: " + sep.getEntranceDate());
			EventBus eventBus = EventBusFactory.getDefault().eventBus();
			eventBus.publish("/sign/"+ sessionId, "transitCheckin");
		}
	}
	
	@Override
	public void transitNoReturnedReceived(SpectatorEpisode sep) {
		getLog().info("Rilevato transito in uscita (check) SENZA RICONSEGNA BADGE per spettatore:" + sep.getSpectator().getFullName());
		
		
		if (lastHeartbeat==null || ((new Date()).getTime()-lastHeartbeat.getTime())> (1000*11)) { // se non ricevo heartbeat per più di 11 secondi
			getLog().info("!!! bean non collegato alla pagina (tab o browser chiuso dall'utente senza aver fatto logout) !!!  ... aspetto che scada la sessione");
//			destroy();
			doDestroy();
			return;
		}
		
		
		//spectatorRowNum = spectators.indexOf(sep); //aggiorno indice ! perchè nel frattempo potrebbe essere arrivato ad esempio un transito
		//spectators.set(spectatorRowNum, sep); // Rimpiazzo in modo da aggiornare dati
		//RequestContext.getCurrentInstance().update(":form_employees:updateStatisticAndList");
		//RequestContext.getCurrentInstance().update(":form_employees:updateSpectsList");
		
		// ATT ... se error è null, allora non scatta l'ajax update lato pagina
		
		// Notifico il mio browser (legato alla sessionId)
		EventBus eventBus = EventBusFactory.getDefault().eventBus();
		eventBus.publish("/sign/"+ sessionId, "Rilevato transito in uscita SENZA RICONSEGNA BADGE per spettatore " + sep.getSpectator().getFullName()); // Questo messaggio va su finestrella errore
		
	}

	public long getEpisodeSpectatorsNums() {
		return episodeSpectatorsNums;
	}

	public void setEpisodeSpectatorsNums(long episodeSpectatorsNums) {
		this.episodeSpectatorsNums = episodeSpectatorsNums;
	}

	public long getEpisodeSpectatorsNumsWithCheckin() {
		return episodeSpectatorsNumsWithCheckin;
	}

	public void setEpisodeSpectatorsNumsWithCheckin(long episodeSpectatorsNumsWithCheckin) {
		this.episodeSpectatorsNumsWithCheckin = episodeSpectatorsNumsWithCheckin;
	}

	public long getEpisodeSpectatorsNumsWithCheckout() {
		return episodeSpectatorsNumsWithCheckout;
	}

	public void setEpisodeSpectatorsNumsWithCheckout(long episodeSpectatorsNumsWithCheckout) {
		this.episodeSpectatorsNumsWithCheckout = episodeSpectatorsNumsWithCheckout;
	}

	public int getSpectatorRowNum() {
		return spectatorRowNum;
	}

	public Long getSelectEpisodeConfirmId() {
		return selectEpisodeConfirmId;
	}

	public void setSelectEpisodeConfirmId(Long selectEpisodeConfirmId) {
		this.selectEpisodeConfirmId = selectEpisodeConfirmId;
	}

	public Long getSelectEpisodeCheckinReopenConfirmId() {
		return selectEpisodeCheckinReopenConfirmId;
	}

	public void setSelectEpisodeCheckinReopenConfirmId(Long selectEpisodeCheckinReopenConfirmId) {
		this.selectEpisodeCheckinReopenConfirmId = selectEpisodeCheckinReopenConfirmId;
	}

	public DonutChartModel getCheckinChartModel2() {
		return checkinChartModel2;
	}

	public void setCheckinChartModel2(DonutChartModel checkinChartModel2) {
		this.checkinChartModel2 = checkinChartModel2;
	}

	public String getChartSubTitle2() {
		return chartSubTitle2;
	}

	public void setChartSubTitle2(String chartSubTitle2) {
		this.chartSubTitle2 = chartSubTitle2;
	}

	public String getChartTitle2() {
		return chartTitle2;
	}

	public void setChartTitle2(String chartTitle2) {
		this.chartTitle2 = chartTitle2;
	}
}
