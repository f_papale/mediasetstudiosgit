package com.crismasecurity.mediasetStudios.web.reception;

import java.io.File;
import java.io.FilenameFilter;
import java.nio.file.WatchService;
import org.apache.logging.log4j.*;

/**
 * Class name: ManageCiasAlarmThread
 *
 * Description: 
 * 
 *
 * Company: CrismaItalia
 *
 * @author Crisma
 * @date 05/nov/2014
 *
 */
public class WatchDirForScanThread implements Runnable {


    final static Logger logger = LogManager.getLogger(WatchDirForScanThread.class);
	private String dirPath;
	private String filename;
	private ReceptionBean context;
	
	private WatchService watcher =null;
	private boolean stopped = false;
	
	public WatchDirForScanThread (String dirPath, String filename, ReceptionBean context) {
		super();
		
		this.context = context;
		this.dirPath = dirPath;
		this.filename = filename;		
	}
	

	
	public void stopMonitoring() {
		stopped = true;
		
	}
	

	 public void run() {
	    logger.info("Starting thread ...");
		File folder = new File(dirPath);
		 
	    //Implementing FilenameFilter to retrieve only txt files
	
	    FilenameFilter txtFileFilter = new FilenameFilter()
	    {
	        @Override
	        public boolean accept(File dir, String name)
	        {
	            if(name.startsWith(filename))
	            {
	                return true;
	            }
	            else
	            {
	                return false;
	            }
	        }
	    };
	
	    //Passing txtFileFilter to listFiles() method to retrieve only txt files
	
	    String lastName = "";
	    while (!stopped) {
	    	File[] files = folder.listFiles(txtFileFilter);
	    	
		    for (File file : files)
		    {
		        //System.out.println(file.getAbsolutePath());
		        if (file.exists() && !lastName.equals(file.getAbsolutePath())) {
			        if (context.scanReceived(file.getAbsolutePath())) {
			        	try {
			        		file.delete();
						} catch (Exception e) {
							e.printStackTrace();
						}
			        	
			        }
			        lastName = file.getAbsolutePath();
		        }
		    }
		    //lastName = "";
		    try {
				Thread.sleep( 500 );
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	    logger.info("FINE thread scan files !!!!!!!!!");
	
	 }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
//	public void stopMonitoring() {
//		stopped = true;
//		if (watcher!=null) {
//			try {
//				//key.cancel();
//	            watcher.close();
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//        }
//	}
	
//    public void run() {
//    	logger.info("Starting thread ...");
//    	WatchKey keya = null;
//    	try {
//			
//	    	Path dira = Paths.get(dirPath);
//			watcher = FileSystems.getDefault().newWatchService();
//			Map<WatchKey,Path> keys = new HashMap<WatchKey,Path>();
//			//WatchKey keya = dira.register(watcher, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.ENTRY_MODIFY);
//			keya = dira.register(watcher,  StandardWatchEventKinds.ENTRY_CREATE);
//	        keys.put(keya, dira);
//	    	
//	        for (;;) {
//	        	 
//	        	System.out.println("monitoring ...");
//	            // wait for key to be signalled
//	            WatchKey key;
//	            try {
//	                key = watcher.take();
//	            } catch (InterruptedException x) {
//	            	x.printStackTrace();
//	                return;
//	            }
//	 
//	            Path dir = keys.get(key);
//	            if (dir == null) {
//	                System.err.println("WatchKey not recognized!!");
//	                continue;
//	            }
//	            
//	            // Prevent receiving two separate ENTRY_MODIFY events: file modified
//	            // and timestamp updated. Instead, receive one ENTRY_MODIFY event
//	            // with two counts.
//	            Thread.sleep( 500 );
//
//	            for (WatchEvent<?> event: key.pollEvents()) {
//	                Kind<?> kind = event.kind();
//	 
//	                // TBD - provide example of how OVERFLOW event is handled
//	                if (kind == StandardWatchEventKinds.OVERFLOW) {
//	                    continue;
//	                }
//	 
//	                // Context for directory entry event is the file name of entry
//	                WatchEvent<Path> ev = cast(event);
//	                Path name = ev.context();
//	                Path child = dir.resolve(name);
//	 
//	                // print out event
//	                System.out.format("%s: %s\n", event.kind().name(), child);
//	                
//	                if (child!=null) {
//	                	String fileTmp = child.toString().substring(child.toString().lastIndexOf("\\")+1);
//	                	if (fileTmp!=null && fileTmp.startsWith(filename) && child.toFile().exists()) {
//	                		Thread.sleep( 700 );
//	                		context.scanReceived(child);
//	                		
////	                		try {
////	                			child.toFile().delete();
////							} catch (Exception e) {
////								e.printStackTrace();
////							}
//	                		break;
//	                	}
//		                else System.out.println("File per altro desk: " + child);
//	                }
//	                
//	            }
//	 
//	            // reset key and remove from set if directory no longer accessible
//	            boolean valid = key.reset();
//	            if (!valid) {
//	                keys.remove(key);
//	 
//	                // all directories are inaccessible
//	                if (keys.isEmpty()) {
//	                    break;
//	                }
//	            }
//	        }
//	        
//	        
//    	} catch (ClosedWatchServiceException ex) {
//    		logger.info("Stop monitoring !!!!!!!!!");
//    	} catch (Exception e) {
//			e.printStackTrace();
//		}
//    	finally {
//    		if (keya!=null) {
//    			keya.cancel();
//    		}
//		}
//		//////////////
//        
//    	logger.info("FINE thread !!!!!!!!!");
//    }
//    
//    
//    @SuppressWarnings("unchecked")
//    static <T> WatchEvent<T> cast(WatchEvent<?> event) {
//        return (WatchEvent<T>)event;
//    }
    
	
}