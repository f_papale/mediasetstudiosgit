package com.crismasecurity.mediasetStudios.web.reception;

import org.primefaces.push.annotation.OnMessage;
import org.primefaces.push.annotation.PathParam;
import org.primefaces.push.annotation.PushEndpoint;
import org.primefaces.push.annotation.Singleton;
import org.primefaces.push.impl.JSONEncoder;

@PushEndpoint("/badge/{sessionId}")
@Singleton
public class BadgeResource {
	
	@PathParam("sessionId")
	private String sessionId;
	
	
	public BadgeResource() {
		
	}
	
    @OnMessage(encoders = {JSONEncoder.class})
    public String onMessage(String count) {
        return count;
    }
    

}
