package com.crismasecurity.mediasetStudios.web.reception;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.imageio.ImageIO;

import org.primefaces.context.RequestContext;
import org.primefaces.event.CaptureEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import com.crismasecurity.mediasetStudios.core.entity.Agency;
import com.crismasecurity.mediasetStudios.core.entity.AgencySpectator;
import com.crismasecurity.mediasetStudios.core.entity.AgencyUser;
import com.crismasecurity.mediasetStudios.core.entity.Audit;
import com.crismasecurity.mediasetStudios.core.entity.Episode;
import com.crismasecurity.mediasetStudios.core.entity.EpisodeStatusType;
import com.crismasecurity.mediasetStudios.core.entity.Location;
import com.crismasecurity.mediasetStudios.core.entity.Spectator;
import com.crismasecurity.mediasetStudios.core.entity.SpectatorEpisode;
import com.crismasecurity.mediasetStudios.core.entity.SpectatorType;
import com.crismasecurity.mediasetStudios.core.security.services.AppUserService;
import com.crismasecurity.mediasetStudios.core.security.vos.security.AppUserVO;
import com.crismasecurity.mediasetStudios.core.services.AgencyService;
import com.crismasecurity.mediasetStudios.core.services.AgencySpectatorService;
import com.crismasecurity.mediasetStudios.core.services.AgencyUserService;
import com.crismasecurity.mediasetStudios.core.services.AuditService;
import com.crismasecurity.mediasetStudios.core.services.BlackListSpectatorService;
import com.crismasecurity.mediasetStudios.core.services.CountryCodeService;
import com.crismasecurity.mediasetStudios.core.services.EpisodeService;
import com.crismasecurity.mediasetStudios.core.services.EpisodeStatusTypeService;
import com.crismasecurity.mediasetStudios.core.services.LocationService;
import com.crismasecurity.mediasetStudios.core.services.ProductionService;
import com.crismasecurity.mediasetStudios.core.services.ReceptionService;
import com.crismasecurity.mediasetStudios.core.services.SpectatorEpisodeService;
import com.crismasecurity.mediasetStudios.core.services.SpectatorService;
import com.crismasecurity.mediasetStudios.core.services.SpectatorTypeService;
import com.crismasecurity.mediasetStudios.core.utils.Gender;
import com.crismasecurity.mediasetStudios.core.utils.PropertiesUtils;
import com.crismasecurity.mediasetStudios.core.utils.ObjectDiff.ObjectDiff;
import com.crismasecurity.mediasetStudios.web.LoginBean;
import com.crismasecurity.mediasetStudios.web.NavigationBean;
import com.crismasecurity.mediasetStudios.web.management.utils.ManageFiscalCode;
import com.crismasecurity.mediasetStudios.web.utils.MakeHTML;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;
import com.crismasecurity.mediasetStudios.web.utils.jsf.FacesUtils;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.LuminanceSource;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeReader;
import com.widee.base.hibernate.criteria.Finder;


@Controller (PrecheckinBean.BEAN_NAME) 
@Scope("view")
public class PrecheckinBean extends BaseBean {

	private static final long serialVersionUID = 4859379465657695066L;

	public static final String BEAN_NAME = "precheckinBean";
    public String getBeanName() { return BEAN_NAME; } 


    @Autowired
    private LocationService locationService; 
    @Autowired
    private EpisodeService episodeService;
    @Autowired
    private ReceptionService receptionService;
    
    @Autowired
	private BlackListSpectatorService blackListSpectatorService;
    @Autowired
    private CountryCodeService countryCodeService;
    @Autowired   
	private AuditService auditService;
	@Autowired
    private AppUserService userService;
	@Autowired
	private EpisodeStatusTypeService episodeStatusTypeService;
	@Autowired
	private SpectatorEpisodeService spectatorEpisodeService;
	@Autowired
	private SpectatorService spectatorService;
    @Autowired
    private NavigationBean navigationBean;	
    @Autowired
	private SpectatorTypeService spectatorTypeService;
    @Autowired
    private AgencyUserService agencyUserService;
    @Autowired
	private AgencyService agencyService;    
    @Autowired
	private ProductionService productionService;
    @Autowired 
	private AgencySpectatorService agencySpectatorService;
	@Autowired
	private LoginBean loginBean;
//	private AppUserVO userSession;
    private Date filterFrom;
	private Date filterTo;
	
    private String spectatorGuid;
    
    private boolean scannerVisible;
    
//  E' lo spettatore attualmente selezionato
	private SpectatorEpisode spectatorEpisodeToEdit = null;
//	E' la lista degli spettatori del corrente episodio
	private List<SpectatorEpisode> spectators = null;
//	E' la lista degli spettatori per i quali è fatto il pre-checkin
	private List<SpectatorEpisode> selectedSpectators = null;
	
	private List<SelectItem> spectatorTypes = null;
	private Long selectedSpectatorType = null;
	
//  E' lo spettatore da inserire
	private Spectator spectatorToInsert = null;	
	private AgencyUser currentAgencyUser=null;
	
	// BarcodeReader
	private final static String CAMERA_WIDTH = "1024"; // 540
	private final static String CAMERA_HEIGHT = "1024"; // 360
	
	private String cameraWidth=CAMERA_WIDTH;
	private String cameraHeight=CAMERA_HEIGHT;
	private String cameraRotation="portrait";
    
//	ID della postazione Desk
	private Long selectedDesk=null;
	private List<SelectItem> desks;
//	ID della puntata attualmente selezionata	
	private Long selectedEpisode=null;
	private List<SelectItem> episodes;
//	Data della puntata
	private Date episodeDate;
//	Lista delle puntate
	private List<Episode> episodesList;

	private String fisCodWarningStyle;
	private String checkCfInserted;

	private Long selectedAgency;
	private Long selectedProduction;
	private boolean spectatorNotAddedToEpisode;
	private String descriptionAddEpisode;
	private String loggedUser;
	private Episode episodeToEdit = null;
	private boolean episodeTerminated = false;
	private ObjectDiff oDiff;
	private String unwantedMessage;



	/**
     * 
     */
	public PrecheckinBean() {
		getLog().info("!!!! Costruttore di PrecheckinBean !!!");
		scannerVisible = false;
		episodeDate = new Date();
		unwantedMessage =PropertiesUtils.getInstance().readProperty("unwanted.message",getMessagesBoundle().getString("spectatorEpisodeBean.unwanted"));
    }

	@PostConstruct
	public void init() {
		getLog().info("INIT PrecheckinBean!");
		oDiff = new ObjectDiff();
//		***********************************************************************************************
		presetCurrentUser();	
//		presetAgencyList();
		presetSpectatorTypeList();
//		***********************************************************************************************
		
		String locationIdString = FacesUtils.getCookieValue("MEDIASET_STUDIOS_SELECTED_LOCATION");
		getLog().info("Cookie - location salvata: " + locationIdString);
		
		desks = new ArrayList<SelectItem>();
		desks.add(new SelectItem(null, " "));
		
		List<Location> desklist = locationService.getLocationFinder().and("locationType.id").eq(2l).setDistinct().list();
		for(Location sptype : desklist) {
			SelectItem xx = new SelectItem(sptype.getId(), sptype.getDescription() + (sptype.getProductionCenter()!=null?(" (" + sptype.getProductionCenter().getName()+")"):""));
			desks.add(xx);
		}
		
		if (locationIdString!=null && !locationIdString.equals("")) {
			Location loc = locationService.getLocationById(Long.parseLong(locationIdString));
			if (loc!=null) {
				getLog().info("Selezionato automaticamente location:" + loc);
				selectedDesk = loc.getId();
				deskChanged();
			}
			else {
				getLog().info("Nessuna location trovata nel cookie");
			}
		}
		
		selectedSpectators = new ArrayList<SpectatorEpisode>();
		
	}

	@PreDestroy
	public void destroy() {
		getLog().info("Destroy PrecheckinBean!"); 

	}

	
	
	public void episodeDateChanged() {
		getLog().info("episodeDateChanged:" + episodeDate + "   selectedDesk:" + selectedDesk);
		
//		Calendar cal = Calendar.getInstance();
//		cal.setTime(episodeDate);
//		cal.set(Calendar.HOUR, 23);
//		cal.set(Calendar.MINUTE, 59);
//		cal.set(Calendar.SECOND, 59);
//		Date from = cal.getTime();
//		cal.setTime(episodeDate);
//		cal.set(Calendar.HOUR, 0);
//		cal.set(Calendar.MINUTE, 0);
//		cal.set(Calendar.SECOND, 1);
//		Date toDate = cal.getTime();
		Calendar cal = Calendar.getInstance();
		cal.setTime(episodeDate);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		Date toDate = cal.getTime();
		cal.setTime(episodeDate);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 1);
		Date from = cal.getTime();
//		episodesList = episodeService.getEpisodeFinder().and("studio.productionCenter.locations.id").eq(selectedDesk)/*.and("episodeStatus.idStatus").eq(4)*/.and("dateFrom").le(from).and("dateTo").ge(toDate).setDistinct().list();
		episodesList = episodeService.getEpisodeFinder().and("studio.productionCenter.locations.id").eq(selectedDesk)/*.and("episodeStatus.idStatus").eq(4)*/.and("dateFrom").ge(from).and("dateTo").le(toDate).setDistinct().list();
		getLog().info("episodesList size:" + episodesList.size());
		selectedEpisode = null;
		
	}
	public void selectEpisode(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("episodeId")) {
    			selectedEpisode = (Long)((UIParameter)com).getValue();
    			episodeChanged();
    			break;
			}
		}
	}
	
	public void backToChangeEpisode() {
		selectedEpisode = null;
	}

	public void deskChanged() {
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(episodeDate);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		Date toDate = cal.getTime();
		cal.setTime(episodeDate);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 1);
		Date from = cal.getTime();
		episodesList = episodeService.getEpisodeFinder().and("studio.productionCenter.locations.id").eq(selectedDesk)/*.and("episodeStatus.idStatus").eq(4)*/.and("dateFrom").ge(from).and("dateTo").le(toDate).setDistinct().list();
		
		episodes = new ArrayList<SelectItem>();
		episodes.add(new SelectItem(null, " "));
		List<Episode> episodesl = episodeService.getEpisodeFinder().and("studio.productionCenter.locations.id").eq(selectedDesk).setDistinct().list();

		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		for(Episode sptype : episodesl) {
			SelectItem xx = new SelectItem(sptype.getId(), sptype.getProduction().getName() + " del " + (sptype.getDateFrom()!=null?formatter.format(sptype.getDateFrom()):""));
			episodes.add(xx);
		}
		selectedEpisode = null;
		if (selectedDesk!=null) FacesUtils.setCookie("MEDIASET_STUDIOS_SELECTED_LOCATION", selectedDesk.toString(), 60*60*24*30); // un mese
	}
	

	public void resetDesk() {
		selectedDesk=null;
		selectedEpisode=null;
		FacesUtils.setCookie("MEDIASET_STUDIOS_SELECTED_LOCATION", "", 0); //Reset
	}
	
	public void episodeChanged() {
		getLog().info("episodeChanged..." + selectedEpisode);
		
		//Carico gli spettatori ordinati per Cognome Nome
		if (selectedEpisode!=null) {
			Comparator<SpectatorEpisode> forFullSurname = (n1, n2) -> n1.getSpectator().getFullSurname().compareTo(n2.getSpectator().getFullSurname());
			spectators = episodeService.getEpisodeById(selectedEpisode).getSpectators().stream()
					.sorted(forFullSurname)
					.collect(Collectors.toList());
			spectators.size();
			
			selectedSpectators.clear();
			int indexCounter=0;
			for (SpectatorEpisode se : spectators) {
				indexCounter++;
				se.setOrder(indexCounter);
				if (se.isPreCheckin()) selectedSpectators.add(se);
			}
			episodeToEdit = episodeService.getEpisodeById(selectedEpisode);
			selectedAgency=episodeToEdit.getProduction().getAgency().getIdAgency();
			selectedProduction=episodeToEdit.getProduction().getId();
		}
	}
	
	public long getPrecheckinedSpectatorsNumber() {
		
		//Carico gli spettatori
		if (spectators!=null) {
			long tot = 0;
			for (SpectatorEpisode se : spectators) {
				if (se.isPreCheckin()) tot++;
			}
			return tot;
		}
		else return 0;
	}

	public Episode getEpisode() {
		return episodeService.getEpisodeById(selectedEpisode);
	}
	
	public void openCameraScanner(ActionEvent e ) {
		getLog().info("openCameraScanner...");
		
		scannerVisible = true;
	}

	public void closeCameraScanner(ActionEvent e ) {
		getLog().info("closeCameraScanner...");
		
		scannerVisible = false;
		
		RequestContext.getCurrentInstance().execute("PF('poolAcquire').stop()"); 
		RequestContext.getCurrentInstance().execute("PF('pc').dettach()");
	}

	public void spectatorGuidChanged() {
		getLog().info("spectatorGuidChanged..." + spectatorGuid);
		
		// Effettuo ricerca dello spettatore 
		
		spectatorEpisodeToEdit = null;
		int indSpect = -1;
		if (spectators!=null) {
			for (SpectatorEpisode s : spectators) {
				if (s.getGuid()!=null && s.getGuid().equals(spectatorGuid)) {
					spectatorEpisodeToEdit = s;
					indSpect = spectators.indexOf(s);
					break;
				}
			}
		}
		if (spectatorEpisodeToEdit!=null) {
			try {
				if (!selectedSpectators.contains(spectatorEpisodeToEdit)) selectedSpectators.add(spectatorEpisodeToEdit);
//				SpectatorEpisode oldSpect = SpectatorEpisode.copy(spectatorEpisodeToEdit);
				spectatorEpisodeToEdit.setPreCheckin(Boolean.TRUE);
				spectatorEpisodeToEdit = receptionService.updateSpectatorEpisode(spectatorEpisodeToEdit);
//				SpectatorEpisode newSpect =SpectatorEpisode.copy(spectatorEpisodeToEdit);
//				auditService.addAudit(new Audit(loggedUser, "Prechekin", "SpectatorEpisode", spectatorEpisodeToEdit.getId(), "", "", "", "", oDiff.getDiff(oldSpect, newSpect)));
//				oldSpect=null;
//				newSpect=null;				
				spectators.set(indSpect, spectatorEpisodeToEdit); // replace

			} catch (Exception e) {
				e.printStackTrace();
			}
			spectatorEpisodeToEdit = null;
		}
		getLog().info("Trovato:" + spectatorEpisodeToEdit);
	}
	

	public void onSpectatorRowSelect(SelectEvent event) {
		if (event!=null && event.getObject()!=null && ((SpectatorEpisode) event.getObject()).getId()!=null) {
			getLog().info("onSpectatorRowSelect:" + ((SpectatorEpisode) event.getObject()).getId());
			spectatorEpisodeToEdit = receptionService.getSpectatorEpisodeById(((SpectatorEpisode) event.getObject()).getId());
			
			if (spectatorEpisodeToEdit!=null) {
				try {
//					SpectatorEpisode oldSpect = SpectatorEpisode.copy(spectatorEpisodeToEdit);
					
					spectatorEpisodeToEdit.setPreCheckin(Boolean.TRUE);
					int preindex = spectators.indexOf(spectatorEpisodeToEdit);
					spectatorEpisodeToEdit.setOrder(spectators.get(preindex).getOrder());	
					spectators.set(preindex, spectatorEpisodeToEdit);
					spectatorEpisodeToEdit = receptionService.updateSpectatorEpisode(spectatorEpisodeToEdit);
//					SpectatorEpisode newSpect =SpectatorEpisode.copy(spectatorEpisodeToEdit);
//					auditService.addAudit(new Audit(loggedUser, "Prechekin", "SpectatorEpisode", spectatorEpisodeToEdit.getId(), "", "", "", "", oDiff.getDiff(oldSpect, newSpect)));
//					oldSpect=null;
//					newSpect=null;					
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				spectatorEpisodeToEdit = null;
			}
		}
    }
 
    public void onSpectatorRowUnselect(UnselectEvent event) {
    	getLog().info("onSpectatorRowUnselect:" + ((SpectatorEpisode) event.getObject()).getId());
    	spectatorEpisodeToEdit=((SpectatorEpisode) event.getObject());
    	
    	// apro popup conferma deselezione
    }
	
	public void confirmSpectatorUnselect() {
		
		if (spectatorEpisodeToEdit!=null) {
			try {
				spectatorEpisodeToEdit = receptionService.getSpectatorEpisodeById(spectatorEpisodeToEdit.getId());
//				SpectatorEpisode oldSpect = SpectatorEpisode.copy(spectatorEpisodeToEdit);
				spectatorEpisodeToEdit.setPreCheckin(Boolean.FALSE);
				int preindex = spectators.indexOf(spectatorEpisodeToEdit);
				spectatorEpisodeToEdit.setOrder(spectators.get(preindex).getOrder());	
				spectators.set(preindex, spectatorEpisodeToEdit); 
				spectatorEpisodeToEdit = receptionService.updateSpectatorEpisode(spectatorEpisodeToEdit);
//				SpectatorEpisode newSpect =SpectatorEpisode.copy(spectatorEpisodeToEdit);
//				auditService.addAudit(new Audit(loggedUser, "Prechekin", "SpectatorEpisode", spectatorEpisodeToEdit.getId(), "", "", "", "", oDiff.getDiff(oldSpect, newSpect)));
//				oldSpect=null;
//				newSpect=null;				
			} catch (Exception e) {
				e.printStackTrace();
			}
			spectatorEpisodeToEdit = null;
		}
    }
	public void cancelConfirmSpectatorRUnselect() {
		
		selectedSpectators.add(spectatorEpisodeToEdit);
		spectatorEpisodeToEdit=null;
	}


	// ******************* Barcode reader 
    public void changeCameraOrientation(ActionEvent e) {
    	Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
    	String orientation = params.get("orientation");
    	
    	getLog().info("Orientation changed to:" + orientation);
    	
    	if (orientation!=null && orientation.equals("landscape")) {
    		cameraWidth=CAMERA_WIDTH;
    		cameraHeight=CAMERA_HEIGHT;
    		cameraRotation="landscape";
    	}
    	else if (orientation!=null && orientation.equals("portrait")) {
    		cameraWidth=CAMERA_HEIGHT;
    		cameraHeight=CAMERA_WIDTH;
    		cameraRotation="portrait";
    	}
    }
    

    public void oncapture(CaptureEvent captureEvent) {
    	InputStream in = null;
    	BufferedImage image = null;
        try {
        	//getLog().info("oncapture ... rowdata:" + captureEvent.getRawData());
        	getLog().info("oncapture ... ");
            if (captureEvent != null) {
                in = new ByteArrayInputStream(captureEvent.getData());
                image = ImageIO.read(in);
                
                //ImageIO.write(image, "jpg", new File("C:\\Users\\Crisma\\Desktop\\aaa\\aaa_"+System.currentTimeMillis()+".jpg"));
                
                String re =qrDecodeFromImage(image);
                if(re == null)
                {
                    //getLog().info("Image does not contain any barcode");
                }
                else
                {
                    getLog().info(re);
                    spectatorGuid=re;
                    closeCameraScanner(null);
                    RequestContext.getCurrentInstance().update("form_employees:spectatorGuid");
                    //RequestContext.getCurrentInstance().update("cameraScannerDlg");
                    RequestContext.getCurrentInstance().update("cameraScannerPanel");
                    
                    RequestContext.getCurrentInstance().execute("document.getElementById('form_employees\\:cerca_guid').click()"); // Trigger onchange.
                } 
               
            }
        } catch (Throwable ex) {
            ex.printStackTrace();
            try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
    }
    
    public String qrDecodeFromImage(BufferedImage img) {
        if(img!=null) {
            LuminanceSource bfImgLuminanceSource = new BufferedImageLuminanceSource(img);
            BinaryBitmap binaryBmp = new BinaryBitmap(new HybridBinarizer(bfImgLuminanceSource));
            
            QRCodeReader qrReader = new QRCodeReader();
            Result result;
            try {
                result = qrReader.decode(binaryBmp);
                return result.getText();
            } catch (Throwable e) {e.printStackTrace();} 
        }
        return null;
    }
    
// **********************************************************************************************************
// ** Edit nuovo spettatore *********************************************************************************
// **********************************************************************************************************
    
    public void addSpecBtn(){
    	getLog().info("addSpecBtn");
    	if (spectatorToInsert==null) spectatorToInsert = new Spectator();
    	descriptionAddEpisode = "";
    }
    
    
	private void presetSpectatorTypeList(){
		setSpectatorTypes(new ArrayList<SelectItem>());
		List<SpectatorType> sptypelist = spectatorTypeService.getSpectatorType();
		getSpectatorTypes().add(new SelectItem(null, " "));
		for(SpectatorType sptype : sptypelist) {
			SelectItem xx = new SelectItem(sptype.getId(), sptype.getDescription());
			getSpectatorTypes().add(xx);
		}
	}	
	
    
    
	private void presetCurrentUser() {

		try {
			long userId = userService.getUserByLogin(loginBean.getUserId()).getId();
			currentAgencyUser= agencyUserService.getAgencyUserFinder().and("user.id").eq(userId).and("flgCurrentSelected").eq(true).result();	
			loggedUser = userService.getUserByLogin(loginBean.getUserId()).getFirstName() + " " + userService.getUserByLogin(loginBean.getUserId()).getLastName();
		} catch (Exception e) {
			e.printStackTrace();
			getLog().info("Errore Preset Current User");
			addErrorMessage(getMessagesBoundle().getString("spectatorEpisode.presetCurrentUser"), e);  	
		} 
	}    
    
	public boolean haveUserRole(String roles) {
		
		String[] listRoles = roles.split(",");
		for (int i = 0; i < listRoles.length; i++) {
			listRoles[i] = listRoles[i].trim();
			
		}
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication!=null && authentication.getAuthorities()!=null) {
			//String currentPrincipalName = authentication.getName();
			for (GrantedAuthority r : authentication.getAuthorities()) {
				//System.out.println("Ruolo:"+r.getAuthority());
				for (int i = 0; i < listRoles.length; i++) {
					if (r.getAuthority()!=null && r.getAuthority().equals(listRoles[i])) {
						//System.out.println("trovato ruolo !!!");
						return true;
					}					
				}
			}
		}
		return false;
	}   
    
    public boolean spectatorToInsertInWarningBlackList() {
    	if (getSpectatorToInsert()!=null) {    		
    		Long idWarn = new Long(1l);
    		String idWarning = PropertiesUtils.getInstance().readProperty("unwanted.type.warning.id", "1");
    		if (idWarning!=null && !idWarning.equals("")) idWarn = Long.parseLong(idWarning);
    		
    		return !blackListSpectatorService.getBlackListSpectatorFinder().and("spectator.id").eq(getSpectatorToInsert().getId()).and("blackListType.id").eq(idWarn).list().isEmpty();
    	}
    	else {
    		return false;
    	}
    }
    
    
    public boolean spectatorToInsertInBlockedBlackList() {
    	if (getSpectatorToInsert()!=null) {
    		Long idBlock = new Long(2l);
    		String idBlocking = PropertiesUtils.getInstance().readProperty("unwanted.type.blocking.id", "2");
    		if (idBlocking!=null && !idBlocking.equals("")) idBlock = Long.parseLong(idBlocking);
    		
    		return !blackListSpectatorService.getBlackListSpectatorFinder().and("spectator.id").eq(getSpectatorToInsert().getId()).and("blackListType.id").eq(idBlock).list().isEmpty();
    	} else 
    		return false;
    }
     
    
    public void fiscalCodeInserted() {
    	try {
    		ManageFiscalCode mfc = new ManageFiscalCode(getSpectatorToInsert().getFiscalCode(), countryCodeService);
    		setFisCodWarningStyle("color:green");
    		setCheckCfInserted("codice fiscale inserito valido") ;
    		getSpectatorToInsert().setBirthDate(mfc.getBornDate());
    		getSpectatorToInsert().setCity(mfc.getCountryCode());
    		getSpectatorToInsert().setGender(mfc.getGender());
    	} catch(Exception e) {
    		getLog().info("errore nel codice fiscale");
    		setFisCodWarningStyle("color:red");
        	setCheckCfInserted("controlla il codice fiscale inserito " + getSpectatorToInsert().getFiscalCode() + " !!!");
    	}
    }  
  
    private boolean spectatorAddedToAnotherConcurrentEpisode(Spectator sp, Long idEpisode) {
    	
    	boolean added = false;
    	try {
    		Episode episode = episodeService.getEpisodeById(idEpisode);
    		Date startEpisode= episode.getDateFrom();
    		Date endEpisode= episode.getDateTo();
    		
    		List<Episode> spectatorEpisodes= episodeService.getEpisodeFinder()
    				.and("spectators.spectator").eq(sp)
    				.and("id").ne(idEpisode)
    				.setDistinct().list();
    		Set<Episode> concurentEpisodes = spectatorEpisodes.stream()
    				.map(s -> s)
    				.filter(s -> (
							(startEpisode.before(s.getDateFrom()) && endEpisode.after(s.getDateFrom()))    								
    						||
    						(startEpisode.after(s.getDateFrom()) && startEpisode.before(s.getDateTo()))
    						||
    						(endEpisode.after(s.getDateFrom()) && endEpisode.before(s.getDateTo()))
    						))
    				.collect(Collectors.toSet());
    		added=!concurentEpisodes.isEmpty();
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return added;
		/*
		0	0	1	1	1	1	0	0		(startEpisode) (endEpisode)
		0	0	0	1	1	0	0	0	NO  (getDateFrom()) (getDateTo())
										
		0	0	1	1	1	1	0	0	
		0	1	1	0	0	0	0	0	No
										
		0	0	1	1	1	1	0	0	
		0	0	0	0	0	1	1	0	NO
										
		0	0	1	1	1	1	0	0	
		0	1	1	1	1	1	1	0	NO Ricade nella casistica sopra
										
		0	0	1	1	1	1	0	0	
		1	1	0	0	0	0	0	0	Si
										
		0	0	1	1	1	1	0	0	
		0	0	0	0	0	0	1	1	Si
    	
 */
    	
    }    
    
    public void handleSelectSpectator(SelectEvent event) {
    	Spectator value = (Spectator) event.getObject();
    	SpectatorType sptype = value.getSpectatorType();
    	if (sptype!=null) selectedSpectatorType = sptype.getId();
    	else selectedSpectatorType=null;
    	spectatorToInsert.setSpectatorType(sptype); 
    	descriptionAddEpisode = "";
    	getLog().info("selected "+ value.getName() + " " + value.getSurname());
    	}
    
	public void addSpectatorEpisode(ActionEvent e) {
		spectatorEpisodeToEdit = new SpectatorEpisode();
		setDescriptionAddEpisode("");
	}
	  
    public List<Spectator> completeSpectator(String query) {   	
    	
    	// Il cognome è criptato quindi il like non funziona!!!!
    	List<Spectator> filteredSpectators = spectatorService.getSpectatorFinder().and("agencies.agency.idAgency").eq(selectedAgency).addOrderAsc("name").setDistinct().list();
    	filteredSpectators.removeIf(spectator -> !spectator.getSurname().toLowerCase().contains(query.toLowerCase()));
    	getLog().info("prima ricerca: " + (filteredSpectators!=null?filteredSpectators.size():"0"));
    	filteredSpectators.addAll(spectatorService.getSpectatorFinder().and("agencies.agency.idAgency").eq(selectedAgency).and("name").like(query).addOrderAsc("name").setDistinct().list());
//    	filteredSpectators.removeIf(spectator -> !spectator.getName().toLowerCase().contains(query.toLowerCase()));
    	getLog().info("seconda ricerca: " + (filteredSpectators!=null?filteredSpectators.size():"0"));
    	return filteredSpectators;
    }
	
	public boolean disableAddSpecBtn() {
		boolean res = false;
		try{
			Integer stato = episodeToEdit.getEpisodeStatus().getIdStatus();	
			
			Date datafinepuntata =  episodeToEdit.getDateTo();
			Date currentDate = new Date();
			
			// la puntata non è né nuova, né in lavorazione oppure se la data corrente è maggiore di quella di fine puntata
			if( (stato != 1 && stato != 2) || currentDate.compareTo(datafinepuntata) > 0 ) {				
				res = true;
			}
		}catch(Exception e) {
			getLog().info("controlla stato puntata" + e.toString());
		}
		return res;
	}    
    
    public boolean checkIfSpectatorNotAddedToCurrentEpisode(){
    	try {
    		
        	if(spectatorEpisodeService.getSpectatorEpisodeFinder().and("spectator.id").eq(getSpectatorToInsert().getId()).and("episode.id").eq(selectedEpisode).setDistinct().list().isEmpty()) {
        		spectatorNotAddedToEpisode = true;
        		
        	}else {
        		spectatorNotAddedToEpisode = false;
        	}
//    		getLog().info("Prova ad aggiungere  lo spettatore" + spectatorToInsert.getSurname());
    	} catch(Exception e) {
    		getLog().info("errore open dialog");
    	}
    	return spectatorNotAddedToEpisode;
    } 
    
    public void addSpectator(ActionEvent actionEvent){
    	getLog().info("Aggiungi lo spettatore " + getSpectatorToInsert().getSurname() + " " + getSpectatorToInsert().getName());
    	try {
    		// Se è un nuovo spettatore non ancora inserito nel DB
     		
    		Spectator oldSpect = spectatorService.getSpectatorById(getSpectatorToInsert().getId());
			String oldHtml = null;
			if (oldSpect!=null) {
				oldHtml = MakeHTML.makeHTML(oldSpect, "agencies", "documents", "episodes", "incidents", "documentFileDownload");
				oldSpect=null;
			}
//    		Valuto se esiste già uno spettatore con medesimo id (Nel caso sia tra quelli dell'Agenzia stessa)
//			Se lo spettatore non esiste
    		if(spectatorService.getSpectatorById(getSpectatorToInsert().getId()) == null) { 
//    			Lo spettatore non esiste allora lo ricerco per Codice fiscale
    			if (getSpectatorToInsert().getFiscalCode()!=null && !getSpectatorToInsert().getFiscalCode().trim().equals("")) {
//    				Controllo se esiste un nominativo con codice fiscale uguale
    				List<Spectator> spsss = spectatorService.getSpectatorFinder().and("fiscalCode").eq(getSpectatorToInsert().getFiscalCode()).setDistinct().list();
//    				Se l'ho trovato assegno a spectatorToInsert il valore ricavato
    				if (spsss!=null && spsss.size()>0) {
//    					Già esistente, lo assegno all'agenzia corrente!
    					setSpectatorToInsert(spsss.get(0));
    				}
    				else {
//	    				Non esiste allora popoliamo la tabella Spettatore 
    		    		//impostiamo dal dialog il valore dello spectator type 
    					getSpectatorToInsert().setSpectatorType(spectatorTypeService.getSpectatorTypeById(getSelectedSpectatorType()));
	    				setSpectatorToInsert(spectatorService.addSpectator(getSpectatorToInsert()));
	    				String newHtml = MakeHTML.makeHTML(getSpectatorToInsert(), "agencies", "documents", "episodes", "incidents", "documentFileDownload");
						AppUserVO uu = userService.getUserByLogin(navigationBean.getUserName());
						auditService.addAudit(new Audit(uu.getLastName() + " " + uu.getFirstName()+" ("+navigationBean.getUserName()+")", "Insert", "Spectator", getSpectatorToInsert().getId(), "", "", null, newHtml));
	    			}
//    			Se non riesco a trovarlo per codice fiscale allora lo ricerco per nome cognome e data di nascita	
    			}else {
    				// Controllo unicità nome, cognome, data nascita
    				Finder<Spectator> spfi = spectatorService.getSpectatorFinder();
    				if (getSpectatorToInsert().getName()!=null && !getSpectatorToInsert().getName().equals("")) spfi.and("name").eq(getSpectatorToInsert().getName());
    				if (getSpectatorToInsert().getSurname()!=null && !getSpectatorToInsert().getSurname().equals("")) spfi.and("surname").eq(getSpectatorToInsert().getSurname());
    				if (getSpectatorToInsert().getBirthDate()!=null) spfi.and("birthDate").eq(getSpectatorToInsert().getBirthDate());
    				List<Spectator> spsss = spfi.setDistinct().list();
//    				Se l'ho trovato assegno a spectatorToInsert il valore ricavato 
    				if (spsss!=null && spsss.size()>0) {
//    					Già esistente, lo assegno all'agenzia corrente!
    					setSpectatorToInsert(spsss.get(0));
    				}
//    				Se no riesco a trovarlo per nome, cognome, e data di nascita allora lo considero nuovo e l'aggiungo
    				else {
//	    				Non esiste allora popoliamo la tabella Spettatore
    		    		//impostiamo dal dialog il valore dello spectator type 
    					getSpectatorToInsert().setSpectatorType(spectatorTypeService.getSpectatorTypeById(getSelectedSpectatorType()));    					
	    				setSpectatorToInsert(spectatorService.addSpectator(getSpectatorToInsert()));
	    				
	    				String newHtml = MakeHTML.makeHTML(getSpectatorToInsert(), "agencies", "documents", "episodes", "incidents", "documentFileDownload");
						AppUserVO uu = userService.getUserByLogin(navigationBean.getUserName());
						auditService.addAudit(new Audit(uu.getLastName() + " " + uu.getFirstName()+" ("+navigationBean.getUserName()+")", "Insert", "Spectator", getSpectatorToInsert().getId(), "", "", null, newHtml));
	    			}
	    			
    			}
//    			Associamo all'AgenziaSpettatore il nominativo dello spettatore nuovo o trovato  AgenziaSpettatore
    			
    			Long agId = (currentAgencyUser!=null && currentAgencyUser.getAgency()!=null && currentAgencyUser.getAgency().getIdAgency()!=null?currentAgencyUser.getAgency().getIdAgency():selectedAgency);
    			Agency ag = agencyService.getAgencyById(agId); 
    			AgencySpectator agspToAdd = new AgencySpectator(ag, getSpectatorToInsert());
    			agspToAdd = agencySpectatorService.updateAgencySpectator(agspToAdd);

    		} else{    
//    			Se lo spettatore esiste	già
//        		Aggiorno i dati dello spettatore. Tuttavia se cambio nome devo valutare se questo non in black list altrimenti rischio di modificare
//    			quel nominativo.
    			setSpectatorToInsert(spectatorService.getSpectatorById(getSpectatorToInsert().getId()));
				if (blackListSpectatorService.isNotInBlackList(
    					getSpectatorToInsert(), 
    					null, 
    					null, 
    					currentAgencyUser,
    					loggedUser, 
    					false)) {

					//Impostiamo dal dialog il valore dello spectator type 
					getSpectatorToInsert().setSpectatorType(spectatorTypeService.getSpectatorTypeById(getSelectedSpectatorType()));
					setSpectatorToInsert(spectatorService.updateSpectator(getSpectatorToInsert()));
	
	        		String newHtml = MakeHTML.makeHTML(getSpectatorToInsert(), "agencies", "documents", "episodes", "incidents", "documentFileDownload");
					AppUserVO uu = userService.getUserByLogin(navigationBean.getUserName());
					auditService.addAudit(new Audit(uu.getLastName() + " " + uu.getFirstName()+" ("+navigationBean.getUserName()+")", "Update", "Spectator", getSpectatorToInsert().getId(), "", "", oldHtml, newHtml));
				}
    		}

//        	Associazione dello spettatore alla puntata
//			Preparo spettatoreEpisodio 
        	SpectatorEpisode spEp = new SpectatorEpisode(
        			productionService.getProductionById(selectedProduction), 
        			episodeService.getEpisodeById(selectedEpisode), 
        			getSpectatorToInsert() );
        	spEp.setDescription(getDescriptionAddEpisode());
//        	Valuto se lo spettatore non è stato già aggiunto all'episodio corrente
        	if(checkIfSpectatorNotAddedToCurrentEpisode()) {     		
//        		Valuto se lo spettatore non è già stato aggiunto ad un episodio contemporaneo
        		if (!spectatorAddedToAnotherConcurrentEpisode(spEp.getSpectator(),spEp.getEpisode().getId())) {
//        			Valuto se lo spettatore è in black list ed eventualmente invio la email
        			if (blackListSpectatorService.isNotInBlackList(
        					getSpectatorToInsert(), 
        					selectedProduction, 
        					selectedEpisode, 
        					currentAgencyUser,
        					loggedUser,
        					true)) {
//		        		Associo lo spettatore alla puntata
		            	spectatorEpisodeService.addSpectatorEpisode(spEp);
		            	
//		            	Aggiorno SpettatorePuntata appena creato assegnando al GUID il valore dell'ID 
		            	spEp.setGuid(Long.toString(spEp.getId()));
//		            	Rendo Vip se è la produzione che sta aggiungendo lo spettatore
		            	if (haveUserRole("ROLE_PRODUCTION")) spEp.setVip(true);
		            	spectatorEpisodeService.updateSpectatorEpisode(spEp);
		            	
//		            	Controlliamo se questa puntata è nuova e cambiamo lo stato da "nuovo"  a "in lavorazione"
		            	if(spEp.getEpisode().getEpisodeStatus().getIdStatus() == 1) {
		            		Episode ep = spEp.getEpisode();
		            		EpisodeStatusType epst = episodeStatusTypeService.getEpisodeStatusTypeFinder().and("idStatus").eq(2).result();
		            		ep.setEpisodeStatus(epst);
		            		ep = episodeService.updateEpisode(ep);         		
		            	}
		            	String newHtml = MakeHTML.makeHTML(spEp, "sign", "pdfSignBase64Str", "agencies", "documents", "episodes", "incidents", "documentFileDownload", "productionType", "agency", "owner", "managerEmail"
		            			, "episodes", "productionEmployee", "logo", "production"
		            			, "studio", "documentFileDownload", "spectators");
						AppUserVO uu = userService.getUserByLogin(navigationBean.getUserName());
						auditService.addAudit(new Audit(uu.getLastName() + " " + uu.getFirstName()+" ("+navigationBean.getUserName()+")", "Add Spectator in list", "SpectatorEpisode", spEp.getId(), "", "", null, newHtml));
						episodeChanged();
		            	addInfoMessage("Spettatore inserito correttamente");
		            	
		        		// Valuto se lo spettatore è in blackList ed eventualmente invio la email
        			}else{
//        				spectatorToInsert.setNote("Lo spettatore non è stato inserito poichè in blacklist SEVERE.");
        				addErrorMessage(unwantedMessage );
        			}
	        	}else {
	        		addInfoMessage("Lo spettatore non è stato inserito poichè già invitato in un altro programma con medesimi orari.");
	        	}
       		}
        	else {
        		addWarningMessage(getMessagesBoundle().getString("spectatorEpisode.alreadyAdded"));
        	}
    	} catch(javax.persistence.PersistenceException e) {
			e.printStackTrace();
			getLog().info("Errore inserimento spettatore");
			addErrorMessage("Dato già presente a sistema. Impossibile inserire lo spettatore!");
    	}catch(Exception e) {
			e.printStackTrace();
			getLog().info("Errore inserimento spettatore");
			addErrorMessage(getMessagesBoundle().getString("spectatorEpisode.addError"), e);
    	}
    	setCheckCfInserted(null);
    	setSpectatorToInsert(null);
    	
    	
    }
    
   
// **********************************************************************************************************    
    public Date getFilterFrom() {
		return filterFrom;
	}

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public Date getFilterTo() {
		return filterTo;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}

	public SpectatorEpisode getSpectatorEpisodeToEdit() {
		return spectatorEpisodeToEdit;
	}

	public void setSpectatorEpisodeToEdit(SpectatorEpisode spectatorToEdit) {
		this.spectatorEpisodeToEdit = spectatorToEdit;
	}

	public String getSpectatorGuid() {
		return spectatorGuid;
	}

	public void setSpectatorGuid(String spectatorGuid) {
		this.spectatorGuid = spectatorGuid;
	}

	public boolean isScannerVisible() {
		return scannerVisible;
	}

	public void setScannerVisible(boolean scannerVisible) {
		this.scannerVisible = scannerVisible;
	}

	public String getCameraWidth() {
		return cameraWidth;
	}

	public void setCameraWidth(String cameraWidth) {
		this.cameraWidth = cameraWidth;
	}

	public String getCameraHeight() {
		return cameraHeight;
	}

	public void setCameraHeight(String cameraHeight) {
		this.cameraHeight = cameraHeight;
	}

	public String getCameraRotation() {
		return cameraRotation;
	}

	public void setCameraRotation(String cameraRotation) {
		this.cameraRotation = cameraRotation;
	}

	public Long getSelectedDesk() {
		return selectedDesk;
	}

	public void setSelectedDesk(Long selectedDesk) {
		this.selectedDesk = selectedDesk;
	}

	public List<SelectItem> getDesks() {
		return desks;
	}

	public void setDesks(List<SelectItem> desks) {
		this.desks = desks;
	}

	public Long getSelectedEpisode() {
		return selectedEpisode;
	}

	public void setSelectedEpisode(Long selectedEpisode) {
		this.selectedEpisode = selectedEpisode;
	}

	public List<SelectItem> getEpisodes() {
		return episodes;
	}

	public void setEpisodes(List<SelectItem> episodes) {
		this.episodes = episodes;
	}

	public List<SpectatorEpisode> getSpectators() {
		return spectators;
	}

	public Date getEpisodeDate() {
		return episodeDate;
	}

	public void setEpisodeDate(Date episodeDate) {
		this.episodeDate = episodeDate;
	}

	public List<Episode> getEpisodesList() {
		return episodesList;
	}

	public List<SpectatorEpisode> getSelectedSpectators() {
		return selectedSpectators;
	}

	public void setSelectedSpectators(List<SpectatorEpisode> selectedSpectators) {
		this.selectedSpectators = selectedSpectators;
	}
//  **************************************************************************************************
	public Spectator getSpectatorToInsert() {
		return spectatorToInsert;
	}

	public void setSpectatorToInsert(Spectator spectatorToInsert) {
		this.spectatorToInsert = spectatorToInsert;
	}

	public String getCheckCfInserted() {
		return checkCfInserted;
	}

	public void setCheckCfInserted(String checkCfInserted) {
		this.checkCfInserted = checkCfInserted;
	}

	public String getFisCodWarningStyle() {
		return fisCodWarningStyle;
	}

	public void setFisCodWarningStyle(String fisCodWarningStyle) {
		this.fisCodWarningStyle = fisCodWarningStyle;
	}

	public List<SelectItem> getSpectatorTypes() {
		return spectatorTypes;
	}

	public void setSpectatorTypes(List<SelectItem> spectatorTypes) {
		this.spectatorTypes = spectatorTypes;
	}

	public Long getSelectedSpectatorType() {
		return selectedSpectatorType;
	}

	public void setSelectedSpectatorType(Long selectedSpectatorType) {
		this.selectedSpectatorType = selectedSpectatorType;
	}
	
	public List <Gender> getGenders() {
		List<Gender> lst = Arrays.asList(Gender.values());
		return lst;
	}

	public String getDescriptionAddEpisode() {
		return descriptionAddEpisode;
	}

	public void setDescriptionAddEpisode(String descriptionAddEpisode) {
		this.descriptionAddEpisode = descriptionAddEpisode;
	}

	public boolean isEpisodeTerminated() {
		if (episodeToEdit!= null) 
			return episodeToEdit.isEpisodeTerminated();
		else return false;
	}

}
