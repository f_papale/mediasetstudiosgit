package com.crismasecurity.mediasetStudios.web.reception;

import org.primefaces.push.annotation.OnMessage;
import org.primefaces.push.annotation.PathParam;
import org.primefaces.push.annotation.PushEndpoint;
import org.primefaces.push.annotation.Singleton;
import org.primefaces.push.impl.JSONEncoder;

@PushEndpoint("/sign/{sessionId}")
@Singleton
public class SignResource {
	
	@PathParam("sessionId")
	private String sessionId;
	
	
	public SignResource() {
		
	}
	
    @OnMessage(encoders = {JSONEncoder.class})
    public String onMessage(String count) {
        return count;
    }
    

}
