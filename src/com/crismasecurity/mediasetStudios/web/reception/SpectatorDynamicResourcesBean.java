package com.crismasecurity.mediasetStudios.web.reception;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;

import org.apache.commons.io.FilenameUtils;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.crismasecurity.mediasetStudios.core.entity.DocumentSpectator;
import com.crismasecurity.mediasetStudios.core.entity.SpectatorEpisode;
import com.crismasecurity.mediasetStudios.core.services.SpectatorEpisodeService;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;


/**
 *  Vedi https://stackoverflow.com/questions/8207325/display-dynamic-image-from-database-with-pgraphicimage-and-streamedcontent/12452144#12452144
 *  
 * @author Crisma
 *
 */

//@ManagedBean(name= SpectatorDynamicResourcesBean.BEAN_NAME) // JSF managed
//@ApplicationScoped
//@SessionScoped // JSF managed
//@ViewScoped // JSF managed

//Spring managed
@Controller (SpectatorDynamicResourcesBean.BEAN_NAME) 
//@Scope("application") 
@Scope("session") // !!!!!!!!!!!!!!!!!!!!!!!!!! non modificare serve per le risorse dinamiche
//@Scope("view")
public class SpectatorDynamicResourcesBean extends BaseBean {


	private static final long serialVersionUID = 8077069869531161353L;
	
	public static final String BEAN_NAME = "spectatorDynamicResourcesBean";
    public String getBeanName() { return BEAN_NAME; } 


    @Autowired
    private SpectatorEpisodeService spectatorEpisodeService;
//    @Autowired
//    private EpisodeService episodeService;
//    @Autowired
//    private SpectatorService spectatorService; 
//    @Autowired
//    private SignService signService; 
    



    
	/**
     * 
     */
	public SpectatorDynamicResourcesBean() {
		getLog().info("!!!! Costruttore di SpectatorDynamicResourcesBean !!!");
//		
    }

	@PostConstruct
	public void init() {
		getLog().info("INIT SpectatorDynamicResourcesBean!");
		
	}

	@PreDestroy
	public void destroy() {
		getLog().info("Destroy SpectatorDynamicResourcesBean!"); 
	}
	
	
	
	
	
	
	
	
	public StreamedContent getFileLiberatoria() throws IOException {
		System.out.println("getFileLiberatoria...");
        FacesContext context = FacesContext.getCurrentInstance();

        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            // So, we're rendering the HTML. Return a stub StreamedContent so that it will generate right URL.
            return new DefaultStreamedContent();
        } else {
            // So, browser is requesting the media. Return a real StreamedContent with the media bytes.
            String spectatorEpisodeId = context.getExternalContext().getRequestParameterMap().get("spectatorEpisodeId");
            
            System.out.println("getFileLiberatoria - get stream for spectatorEpisode id: " + spectatorEpisodeId);
            SpectatorEpisode se = spectatorEpisodeService.getSpectatorEpisodeById(Long.valueOf(spectatorEpisodeId));
            StreamedContent fileLiberatoria = null;
        	if (se!=null && se.getLiberatoria()!=null && se.getLiberatoria().getPdfSignBase64Str()!=null) {
        		fileLiberatoria = new DefaultStreamedContent(new ByteArrayInputStream(Base64.getDecoder().decode(se.getLiberatoria().getPdfSignBase64Str())), "application/pdf", "liberatoria.pdf");
//        		fileLiberatoria = new DefaultStreamedContent(new ByteArrayInputStream(Base64.getDecoder().decode(spectatorToEdit.getPdfSignBase64Str())), "application/pdf");
        		System.out.println("stream creato.");
        		
        	}
            return fileLiberatoria;
            
//            Media media = service.find(Long.valueOf(spectatorEpisodeId));
//            return new DefaultStreamedContent(new ByteArrayInputStream(media.getBytes()));
        }
    }

	
	
	
	
	
	
	
	/************************** Spectator Bean **************************************/
	
	private DocumentSpectator documentSpectator;
	public void setCurrentDocument(DocumentSpectator ds) {
		documentSpectator=ds;
	}
	public StreamedContent getStreamDocument() {
		StreamedContent file;
		InputStream myInputStream = new ByteArrayInputStream(documentSpectator.getDocInBlob());
		String ext = FilenameUtils.getExtension(documentSpectator.getDescription());
		file = new DefaultStreamedContent(myInputStream, ext, documentSpectator.getDescription());
		return file;
	}
	public String getCurrentDocumentExt() {
		if (documentSpectator==null) return "";
		String ext = FilenameUtils.getExtension(documentSpectator.getDescription());
		if (ext!=null) return ext.toLowerCase();
		else return "";
	}
	
	
	private String currentPdfSign;
	public void setCurrentPdfSign(String base64doc) {
		currentPdfSign=base64doc;
	}
	public StreamedContent getFileLiberatoriaForSpectatorBean() {
		DefaultStreamedContent file;
		InputStream myInputStream = new ByteArrayInputStream(Base64.getDecoder().decode(currentPdfSign));
		file = new DefaultStreamedContent(myInputStream);
		file.setName("document.pdf");
		return file;
	}
	/*****************************************************************************/
	


}
