package com.crismasecurity.mediasetStudios.web.management;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.crismasecurity.mediasetStudios.core.entity.Test;
import com.crismasecurity.mediasetStudios.core.services.TestService;
 

public class LazyTestDataModel extends LazyDataModel<Test> {
     
	private static final long serialVersionUID = -7803934729089718942L;
	
	private TestService contractService;
	
	private Date filterFrom;
	private Date filterTo;

     
    public LazyTestDataModel(TestService contractService) {
        this.contractService = contractService;
    }
     
    @Override
    public Test getRowData(String rowKey) {
    	if (rowKey==null || rowKey.trim().equals("")) return null;
    	return contractService.getTestById(Long.parseLong(rowKey));
    }
 
    @Override
    public Object getRowKey(Test audit) {
        return audit.getId();
    }
 
    @Override
    public List<Test> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
    	
    	// Converto ordinamento
    	int sortInt =0; // UNSORTED
    	if (sortOrder.equals(SortOrder.ASCENDING)) sortInt = 1;
    	if (sortOrder.equals(SortOrder.DESCENDING)) sortInt = 2;
    	
 
		if (filterFrom!=null) filters.put("date#ge", filterFrom);
		if (filterTo!=null) filters.put("date#le", filterTo);

    	
    	List<Test> ret = contractService.getTestLazyLoading(first, pageSize, sortField, sortInt, filters);
    	this.setRowCount((int)contractService.getTestLazyLoadingCount(filters));
//    	System.out.println("Numero elementi trovati: " + this.getRowCount());
//    	for (Audit audit : ret) {
//			System.out.println("Trovato: " + audit);
//		}
    	return ret;    
    }

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}


}