package com.crismasecurity.mediasetStudios.web.management;

import java.util.Calendar;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.event.ActionEvent;

import org.primefaces.component.button.Button;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.crismasecurity.mediasetStudios.core.entity.Test;
import com.crismasecurity.mediasetStudios.core.services.TestService;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;



//@ManagedBean(name= EmployeesBean.BEAN_NAME) // JSF managed
//@SessionScoped // JSF managed
//@ViewScoped // JSF managed

//Spring managed
@Controller (TestBean.BEAN_NAME) 
//@Scope("session")
@Scope("view")
public class TestBean extends BaseBean {


	private static final long serialVersionUID = 534706599202360395L;



	public static final String BEAN_NAME = "testBean";
    public String getBeanName() { return BEAN_NAME; } 


    @Autowired
    private TestService testService ;
    
    private LazyDataModel<Test> lazyModelTest;
    private Date filterFrom;
	private Date filterTo;

	
	private MenuModel model;
	 
	
	private Test testToEdit = null;
	
	public TestBean() {
		getLog().info("!!!! Costruttore di TestBean !!!");
    }

	@PostConstruct
	public void init() {
		getLog().info("INIT EmployeesBean!");

		//L'oggetto Test
		lazyModelTest = new LazyTestDataModel(testService);
		setModel(new DefaultMenuModel());
		
		 addMenu("File", "New", "Open", "Close", "Exit");
		 addMenu("Edit", "Undo", "Redo", "Cut", "Copy");
		 addMenu(addMenu("View", "Summary"), "Tools", "Settings", "Layout");
		 addMenu("Help", "Help topics", "Support");
		//menuBean();
	}

	@PreDestroy
	public void destroy() {
		getLog().info("Destroy TestBean!");
		
		
	}
	
	
	
	private Long testToDelete = null;
	
	public void confirmDeleteTest(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("testId")) {
    			testToDelete = ((Long)((UIParameter)com).getValue());
			}
		}
    	
	}
	public void cancelDelete(ActionEvent ev) {
	    	System.out.println("\nDelete Test canceled.");
	    	testToDelete= null;
	}
	public void delete(ActionEvent ev) {
    	System.out.println("\nDelete Test ...");
    	try {     	
    		testService.deleteTest(testToDelete);
    		addInfoMessage(getMessagesBoundle().getString("employee.deleted"));
		} catch (Exception e) {
			e.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("employee.deletingError"), e);
		}
    	
	}
	

	public void addTest(ActionEvent e) {
		testToEdit = new Test();
	}
	

	public void modifyTest(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("testId")) {
    			testToEdit = testService.getTestById((Long)((UIParameter)com).getValue());
			}
		}
	}
	public void cancelEditTest(ActionEvent e) {
		testToEdit = null;
	}
	public void saveTest(ActionEvent e) {
    	System.out.println("Salvo" + testToEdit);
    	

    	if (testToEdit.getTest()==null || testToEdit.getTest().trim().equals("")) {
    		addWarningMessage("test su test richiesto!");
    		return;
    	}
    	
    	
    	try {
    		testToEdit = testService.updateTest(testToEdit);
		
			addInfoMessage(getMessagesBoundle().getString("employee.saved"));
		} catch (Exception e1) {
			addErrorMessage(getMessagesBoundle().getString("employee.savingError"), e1);
			e1.printStackTrace();
		}
    	testToEdit = null;
	}
	


	
	
	public void filterByDate() {
		System.out.println("Filter by date !");
		
		Calendar cal = Calendar.getInstance();
		if (filterFrom!=null) {
			cal.setTime(filterFrom);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 1);
			filterFrom = cal.getTime();
		}
		if (filterTo!=null) {
			cal.setTime(filterTo);
			cal.set(Calendar.HOUR_OF_DAY, 23);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);
			filterTo = cal.getTime();
		}
		
		
		((LazyTestDataModel)lazyModelTest).setFilterFrom(filterFrom);
		((LazyTestDataModel)lazyModelTest).setFilterTo(filterTo);
	}
	


	public Object getTestById(long id) {
		return testService.getTestById(id);
	}

	public Test getTestToEdit() {
		return testToEdit;
	}

	public void setTestToEdit(Test empToEdit) {
		this.testToEdit = empToEdit;
	}

	

	public LazyDataModel<Test> getLazyModelTest() {
		return lazyModelTest;
	}

	public Date getFilterFrom() {
		return filterFrom;
	}

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public Date getFilterTo() {
		return filterTo;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}

	
public void menuBean() {	 


	//First submenu
	DefaultSubMenu firstSubmenu = new DefaultSubMenu("Dynamic Submenu");
	firstSubmenu.setStyle("inline");

	DefaultMenuItem item = new DefaultMenuItem("External");
	
	item.setUrl("http://www.primefaces.org");
	item.setIcon("ui-icon-home");
	firstSubmenu.addElement(item);
	 
	getModel().addElement(firstSubmenu);
	 
	//Second submenu
	DefaultSubMenu secondSubmenu = new DefaultSubMenu("Dynamic Actions");
	 
	item = new DefaultMenuItem("Save");
	item.setIcon("ui-icon-disk");
	item.setCommand("#{testBean.menuSave}");
	//item.setUpdate("messages");
	secondSubmenu.addElement(item);
	 
	item = new DefaultMenuItem("Delete");
	item.setIcon("ui-icon-close");
	item.setCommand("#{testBean.menuDelete}");
	item.setAjax(false);
	secondSubmenu.addElement(item);
	 
	item = new DefaultMenuItem("Redirect");
	item.setIcon("ui-icon-search");
	item.setCommand("#{testBean.menuRedirect}");
	secondSubmenu.addElement(item);
	 
	getModel().addElement(secondSubmenu);
}


	public void menuButton() {
		Button button= new Button();
		
	}
	 
	public MenuModel getModel() {
		return model;
	}

	public void setModel(MenuModel model) {
		this.model = model;
	}
	
	public void menuSave() {
		System.out.println("Menu Save...");
		
	}
	
	public void menuRedirect() {
		System.out.println("Menu Redirect...");
	}
	public void menuDelete() {
		System.out.println("Menu Delete...");
	}
	
	public DefaultSubMenu addMenu(String label, String... items) {
		  return addMenu(null, label, items);
		}

	public DefaultSubMenu addMenu(DefaultSubMenu parentMenu,
								String label, String... items) {
	  DefaultSubMenu theMenu = new DefaultSubMenu(label);
	  for (Object item : items) {
		  DefaultMenuItem mi = new DefaultMenuItem(item);
		  mi.setUrl("#");
		  theMenu.addElement(mi);
	  }
	  if (parentMenu == null) {
		  model.addElement(theMenu);
	  } else {
		  parentMenu.addElement(theMenu);
	  }
	  return theMenu;
	}

	public MenuModel getMenuModel() {
	  return model;
	}
}
