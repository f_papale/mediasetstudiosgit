package com.crismasecurity.mediasetStudios.web.management;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.persistence.PersistenceException;

import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import com.crismasecurity.mediasetStudios.core.entity.Apparatus;
import com.crismasecurity.mediasetStudios.core.entity.Location;
import com.crismasecurity.mediasetStudios.core.entity.LocationApparatus;
import com.crismasecurity.mediasetStudios.core.entity.LocationType;
import com.crismasecurity.mediasetStudios.core.services.ApparatusService;
import com.crismasecurity.mediasetStudios.core.services.LocationApparatusService;
import com.crismasecurity.mediasetStudios.core.services.LocationService;
import com.crismasecurity.mediasetStudios.core.services.LocationTypeService;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;

@Controller(LocationBean.BEAN_NAME)
@Scope("view")
public class LocationBean extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4492122134680053847L;

	public static final String BEAN_NAME = "locationBean";

	public String getBeanName() {
		return BEAN_NAME;
	}

	@Autowired
	private LocationService locationService;

	@Autowired
	private LocationTypeService locationTypeService;
	
	@Autowired
	private LocationApparatusService locationApparatusService;
	
	@Autowired
	private ApparatusService apparatusService;
	
	private int activeTab=0;
	
	
	private LazyDataModel<Location> lazyModelLocation;
	private List<SelectItem> locationTypes;
	private Long selectedLocationType = null;
	private Location locationToEdit = null;
	private Long locationToDelete = null;
	private Apparatus apparatusToEdit = null;
	private Long selectedApparatus = null;
	private List<SelectItem> locationApparatus;
	private List<LocationApparatus> listLocationApparatus = new ArrayList<LocationApparatus>();
	private Long apparatusToDelete = null;
	
	
	
	public LocationBean() {
		getLog().info("!!!! Costruttore di LocationBean !!!");
	}

	@PostConstruct
	public void init() {
		getLog().info("INIT LocationBean!");
		this.presentLocationTypeList();
		lazyModelLocation = new LazyLocationDataModel(locationService);
	}

	@PreDestroy
	public void destroy() {
		getLog().info("Destroy LocationBean!");

	}

	public void confirmDeleteLocation(ActionEvent e) {
		HtmlCommandLink component = (HtmlCommandLink) e.getSource();
		for (UIComponent com : component.getChildren()) {
			if (com instanceof UIParameter && ((UIParameter) com).getName().equalsIgnoreCase("locationId")) {
				locationToDelete = ((Long) ((UIParameter) com).getValue());
			}
		}

	}

	public void cancelDelete(ActionEvent ev) {
		System.out.println("\nDelete Location canceled.");
		locationToDelete = null;
	}

	public void delete(ActionEvent ev) {
		System.out.println("\nDelete Test ...");
		try {
			locationService.deleteLocation(locationToDelete);
			addInfoMessage(getMessagesBoundle().getString("location.deleted"));
		} catch (Exception e) {
			e.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("location.deletingError"), e);
		}
	}

	public void addLocation(ActionEvent e) {
		locationToEdit = new Location();
		apparatusToEdit=null;
		listLocationApparatus=null;
		this.presentLocationTypeList();
	}

	public void modifyLocation(ActionEvent e) {
		HtmlCommandLink component = (HtmlCommandLink) e.getSource();
		for (UIComponent com : component.getChildren()) {
			if (com instanceof UIParameter && ((UIParameter) com).getName().equalsIgnoreCase("locationId")) {
				locationToEdit = locationService.getLocationById((Long) ((UIParameter) com).getValue());
    			selectedLocationType=locationToEdit.getLocationType().getId();
    			listLocationApparatus = locationApparatusService.getLocationApparatusFinder().and("location.id").eq(locationToEdit.getId()).setDistinct().list();
    			
    			
			}
		}
	}
	
	public void modifyApparatus(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("apparatusId")) {
    			apparatusToEdit = apparatusService.getApparatusById((Long)((UIParameter)com).getValue());
    		}
		}
    	this.apparatusToDelete = apparatusToEdit.getId();
    	this.presentApparatus();
		this.selectedApparatus = apparatusToEdit.getId();
	}
	

	public void cancelEditLocation(ActionEvent e) {
		locationToEdit = null;
	}
	
	public void cancelEditApparatus(ActionEvent e) {
		apparatusToEdit = null;
	}
	
	
	
	public List<LocationApparatus> getApparatus(){
		if(locationToEdit.getId() == null) {
			
			if(listLocationApparatus == null) {
				listLocationApparatus = new ArrayList<LocationApparatus>();
			}
			return listLocationApparatus;
		}
		else {
			
			if(apparatusToDelete == null) {
				//FIXME - Da rivedere con Giovanni e Fabrizio in merito alle location da filtrare.
				//Per il momento sono fatte con la lista andrebbero fatte con la query
				List<LocationApparatus> tempListLocation= locationApparatusService.getLocationApparatus();
				listLocationApparatus = new ArrayList<LocationApparatus>();
				for(LocationApparatus elementLA : tempListLocation) {
					if(elementLA.getLocation() != null) {
						if(elementLA.getLocation().getId() == locationToEdit.getId()) {
							listLocationApparatus.add(elementLA);
						}
					}
				}
			//	listLocationApparatus = locationApparatusService.getLocationApparatus();
			}
			else {
				List<LocationApparatus> listTempApparatus = new ArrayList<LocationApparatus>();
				for(LocationApparatus elementLocationApparatus : listLocationApparatus) {
					//Controllo per ogni singolo elemento che sto scorrendo l'indice se e' diverso a quello che ho cancellato
					if(elementLocationApparatus.getApparatus().getId() != apparatusToDelete) {
						listTempApparatus.add(elementLocationApparatus);
					}
				}
				listLocationApparatus.clear();
				listLocationApparatus.addAll(listTempApparatus);
			}
			return listLocationApparatus;
		}
	}
	
	public void addLocationApparatus(ActionEvent e) {
		apparatusToEdit = new Apparatus();
		selectedApparatus = null;
		setActiveTab(0);
		this.presentApparatus();
		
		System.out.println("Numero elementi: " + this.locationApparatus.size());
		
	}
	
	private void presentApparatus() {
		locationApparatus = new ArrayList<SelectItem>();
		List<Apparatus> apparatusList = apparatusService.getApparatus();
		SelectItem element = new SelectItem(null, "");
		locationApparatus.add(element);
		for (Apparatus elementApparatus : apparatusList) {
			SelectItem xx = new SelectItem(elementApparatus.getId(), elementApparatus.getDescription());
			locationApparatus.add(xx);
		}
	}
	
	public void saveLocation(ActionEvent e) { 
		System.out.println("Salvo " + locationToEdit);
		if (locationToEdit.getDescription() == null || locationToEdit.getDescription().trim().equals("")) {
			addWarningMessage("Descrizione richiesta!");
			return;
		}
		if (locationToEdit.getName() == null || locationToEdit.getName().trim().equals("")) {
			addWarningMessage("Nome richiesto!");
			return;
		}
		try {
			//Imposto il valore dell'oggetto Location da memorizzare
			locationToEdit.setLocationType(locationTypeService.getLocationTypeById(selectedLocationType));
			locationToEdit = locationService.updateLocation(locationToEdit);
			
			if(locationToEdit.getId() > 0) {
				//Aggiungo alla tabella LocationApparatus le relative relazioni
				//if(!listLocationApparatus.isEmpty()) {
				if(listLocationApparatus!=null) {
					
					//Ricarico gli elementi attualmente presenti sulla tabella apparatusPostazione
					
					List<LocationApparatus> laTT = locationApparatusService.getLocationApparatusFinder().and("location.id").eq(locationToEdit.getId()).setDistinct().list();
					for(LocationApparatus tt : laTT) {
						locationApparatusService.deleteLocationApparatus(tt.getId());
					}
					
					
					
					//Effettuo nuovamente l'inserimento delle location
					for (LocationApparatus element : listLocationApparatus) {
						element.setLocation(locationToEdit);
						Apparatus elementApparatus = apparatusService.getApparatusById(element.getApparatus().getId());
						element.setApparatus(elementApparatus);
						locationApparatusService.updateLocationApparatus(element);
					}
				}
			}
			
			//procedo con il salvataggio all'interno dell'entity GapApparatus
			addInfoMessage(getMessagesBoundle().getString("location.saved"));
    	} catch (PersistenceException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();

    	} catch (DataIntegrityViolationException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();
    				
			
		} catch (Exception e1) {
			addErrorMessage(getMessagesBoundle().getString("location.savingError"), e1);
			e1.printStackTrace();
		}
		locationToEdit = null;
	}
	
	
	public void saveLocationApparatu(ActionEvent e) {
		System.out.println("Devo salvare le locations all'interno della griglia");
		try {
			
			if(selectedApparatus!=null) {
				LocationApparatus elementLocationApparatus = new LocationApparatus();
				Apparatus elementApparatus = apparatusService.getApparatusById(selectedApparatus);
				elementLocationApparatus.setApparatus(elementApparatus);
				
				//Verifico se sto modificando oppure creando un nuova Postazione
				if(locationToEdit.getId() == null) {
					if(locationToEdit.getLocationApparatus()!=null || !locationToEdit.getLocationApparatus().isEmpty()) {
						List<LocationApparatus> tempLocationApparatus = new ArrayList<LocationApparatus>();
						tempLocationApparatus.add(elementLocationApparatus);
						if(listLocationApparatus==null) {
							listLocationApparatus = new ArrayList<LocationApparatus>();
						}
						listLocationApparatus.addAll(tempLocationApparatus);
					}
					else {
						listLocationApparatus = new ArrayList<LocationApparatus>();
						listLocationApparatus.add(elementLocationApparatus);
					}
				}
				else {
					elementLocationApparatus.setLocation(locationToEdit);
					if(locationToEdit.getLocationApparatus()!=null || !locationToEdit.getLocationApparatus().isEmpty()) {
						List<LocationApparatus> tempLocationApparatus = new ArrayList<LocationApparatus>();
						tempLocationApparatus.add(elementLocationApparatus);
						listLocationApparatus.addAll(tempLocationApparatus);
					}
					else {
						listLocationApparatus = new ArrayList<LocationApparatus>();
						listLocationApparatus.add(elementLocationApparatus);
					}
				}
				
				apparatusToEdit = null;
			}
			else {
				addErrorMessage(getMessagesBoundle().getString("pippo"));
			}
			
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	
	public void confirmDeleteApparatus(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("apparatusId")) {
    			apparatusToDelete = ((Long) ((UIParameter) com).getValue());
			}
		}
    	
	}
	
	public void deleteApparatus(ActionEvent e) {
		List<LocationApparatus> listTempApparatus = new ArrayList<LocationApparatus>();
		for(LocationApparatus elementLocationApparatus : listLocationApparatus) {
			//Controllo per ogni singolo elemento che sto scorrendo l'indice se e' diverso a quello che ho cancellato
			if(elementLocationApparatus.getApparatus().getId() != apparatusToDelete) {
				listTempApparatus.add(elementLocationApparatus);
			}
		}
		listLocationApparatus.clear();
		listLocationApparatus.addAll(listTempApparatus);
	}
	
	private void presentLocationTypeList() {
		locationTypes = new ArrayList<SelectItem>();
		List<LocationType> locationTypeList = locationTypeService.getLocationType();
		SelectItem element = new SelectItem(null, "");
		locationTypes.add(element);
		for (LocationType elementApparatus : locationTypeList) {
			SelectItem xx = new SelectItem(elementApparatus.getId(), elementApparatus.getDescription());
			locationTypes.add(xx);
		}
	}
	
	
	public Object getLocationById(long id) {
		return locationService.getLocationById(id);
	}

	public Location getLocationToEdit() {
		return locationToEdit;
	}

	public void setLocationToEdit(Location locationToEdit) {
		this.locationToEdit = locationToEdit;
	}

	public Long getLocationToDelete() {
		return locationToDelete;
	}

	public void setLocationToDelete(Long locationToDelete) {
		this.locationToDelete = locationToDelete;
	}

	public LazyDataModel<Location> getLazyModelLocation() {
		return lazyModelLocation;
	}

	public List<SelectItem> getLocationTypes() {
		return locationTypes;
	}

	public void setLocationTypes(List<SelectItem> locationType) {
		this.locationTypes = locationType;
	}

	public Long getSelectedLocationType() {
		return selectedLocationType;
	}

	public void setSelectedLocationType(Long selectedLocationType) {
		this.selectedLocationType = selectedLocationType;
	}

	public int getActiveTab() {
		return activeTab;
	}

	public void setActiveTab(int activeTab) {
		this.activeTab = activeTab;
	}

	public Apparatus getApparatusToEdit() {
		return apparatusToEdit;
	}

	public void setApparatusToEdit(Apparatus apparatusToEdit) {
		this.apparatusToEdit = apparatusToEdit;
	}

	public Long getSelectedApparatus() {
		return selectedApparatus;
	}

	public void setSelectedApparatus(Long selectedApparatus) {
		this.selectedApparatus = selectedApparatus;
	}

	public List<SelectItem> getLocationApparatus() {
		return locationApparatus;
	}

	public void setLocationApparatus(List<SelectItem> locationApparatus) {
		this.locationApparatus = locationApparatus;
	}

	public Long getApparatusToDelete() {
		return apparatusToDelete;
	}

	public void setApparatusToDelete(Long apparatusToDelete) {
		this.apparatusToDelete = apparatusToDelete;
	}

	public List<LocationApparatus> getListLocationApparatus() {
		return listLocationApparatus;
	}

	public void setListLocationApparatus(List<LocationApparatus> listLocationApparatus) {
		this.listLocationApparatus = listLocationApparatus;
	}
	
	public boolean haveUserRole(String roles) {
		
		String[] listRoles = roles.split(",");
		for (int i = 0; i < listRoles.length; i++) {
			listRoles[i] = listRoles[i].trim();
			
		}
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication!=null && authentication.getAuthorities()!=null) {
			//String currentPrincipalName = authentication.getName();
			for (GrantedAuthority r : authentication.getAuthorities()) {
				//System.out.println("Ruolo:"+r.getAuthority());
				for (int i = 0; i < listRoles.length; i++) {
					if (r.getAuthority()!=null && r.getAuthority().equals(listRoles[i])) {
						//System.out.println("trovato ruolo !!!");
						return true;
					}					
				}
			}
		}
		return false;
	}
	
}
