package com.crismasecurity.mediasetStudios.web.management;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.persistence.PersistenceException;
import javax.servlet.http.HttpSession;

import org.primefaces.model.LazyDataModel;
import org.primefaces.push.EventBus;
import org.primefaces.push.EventBusFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;

import com.crismasecurity.mediasetStudios.core.entity.Location;
import com.crismasecurity.mediasetStudios.core.entity.Media;
import com.crismasecurity.mediasetStudios.core.entity.MediaStatusType;
import com.crismasecurity.mediasetStudios.core.entity.MediaType;
import com.crismasecurity.mediasetStudios.core.entity.SpectatorEpisode;
import com.crismasecurity.mediasetStudios.core.services.LocationService;
import com.crismasecurity.mediasetStudios.core.services.MediaService;
import com.crismasecurity.mediasetStudios.core.services.MediaStatusTypeService;
import com.crismasecurity.mediasetStudios.core.services.MediaTypeService;
import com.crismasecurity.mediasetStudios.core.services.SpectatorEpisodeService;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;
import com.crismasecurity.mediasetStudios.web.utils.jsf.FacesUtils;
import com.crismasecurity.mediasetStudios.webservices.MediasetStudiosWebServices;
import com.crismasecurity.mediasetStudios.webservices.desktopReader.DesktopReaderListener;

//@ManagedBean(name= MediasBean.BEAN_NAME) // JSF managed
//@SessionScoped // JSF managed
//@ViewScoped // JSF managed

//Spring managed
@Controller (MediaBean.BEAN_NAME) 
//@Scope("session")
@Scope("view")
public class MediaBean extends BaseBean implements DesktopReaderListener{


	private static final long serialVersionUID = 7468538757085467714L;

	public static final String BEAN_NAME = "mediaBean";
    public String getBeanName() { return BEAN_NAME; } 


    @Autowired
    private MediasetStudiosWebServices mediasetStudiosWebServices; 
    
    @Autowired
    private MediaService mediaService ;
    @Autowired
    private LocationService locationService; 
    @Autowired
    private MediaTypeService mediaTypeService;
    
    @Autowired
    private SpectatorEpisodeService spectatorEpisodeService;
    
    @Autowired
    private MediaStatusTypeService mediaStatusTypeService;
    
    private LazyDataModel<Media> lazyModelMedia;
    private Date filterFrom;
	private Date filterTo;
	
	
	private List<SelectItem> mediaTypes;
	private Long selectedMediaType = null;
	
	private List<SelectItem> mediaStatusTypes;
	private Long selectedMediaStatusType = null;
	
	private Media mediaToEdit = null;
	
	private Long selectedDesk=null;
	private List<SelectItem> desks;
	
	private String sessionId = null;
	

	
    
	public MediaBean() {
		getLog().info("!!!! Costruttore di MediaBean !!!");	
    }

	@PostConstruct
	public void init() {
		getLog().info("INIT MediaBean!");
		
		mediasetStudiosWebServices.addDesktopReaderListener(this);
		
		presetMediaList();
		presetMediaStatusList();		
		
		lazyModelMedia = new LazyMediaDataModel(mediaService);		
		
		
		String locationIdString = FacesUtils.getCookieValue("MEDIASET_STUDIOS_SELECTED_LOCATION");
		System.out.println("Cookie - location salvata: " + locationIdString);
		desks = new ArrayList<SelectItem>();
		desks.add(new SelectItem(null, " "));
		
		List<Location> desklist = locationService.getLocationFinder().and("locationType.id").eq(2l).setDistinct().list();
		for(Location sptype : desklist) {
			SelectItem xx = new SelectItem(sptype.getId(), sptype.getDescription() + (sptype.getProductionCenter()!=null?(" (" + sptype.getProductionCenter().getName()+")"):""));
			desks.add(xx);
		}
		if (locationIdString!=null && !locationIdString.equals("")) {
			Location loc = locationService.getLocationById(Long.parseLong(locationIdString));
			if (loc!=null) {
				System.out.println("Selezionato automaticamente location:" + loc);
				selectedDesk = loc.getId();
				deskChanged();
			}
			else {
				System.out.println("Nessuna location trovata nel cookie");
			}
		}
		
		// Prendo la sessionId
		FacesContext fCtx = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) fCtx.getExternalContext().getSession(false);
		sessionId = session.getId();
		System.out.println("sessionId:" + sessionId);
				
	}

	@PreDestroy
	public void destroy() {
		getLog().info("Destroy MediaBean!");
		
		mediasetStudiosWebServices.removeDesktopReaderListener(this);
		
	}
	
	
	public void deskChanged() {		
		if (selectedDesk!=null) FacesUtils.setCookie("MEDIASET_STUDIOS_SELECTED_LOCATION", selectedDesk.toString(), 60*60*24*30); // un mese
	}
	
	
	
	private void presetMediaList() {
		mediaTypes = new ArrayList<SelectItem>();
		List<MediaType> conts = mediaTypeService.getMediaType();
		mediaTypes.add(new SelectItem(null, " "));
		for (MediaType cont : conts) {
			SelectItem xx = new SelectItem(cont.getId(), cont.getDescription());
			mediaTypes.add(xx);
		}
	}
	
	private void presetMediaStatusList() {
		setMediaStatusTypes(new ArrayList<SelectItem>());
		List<MediaStatusType> conts = mediaStatusTypeService.getMediaStatusType();
		getMediaStatusTypes().add(new SelectItem(null, " "));
		for (MediaStatusType cont : conts) {
			SelectItem xx = new SelectItem(cont.getId(), cont.getDescription());
			getMediaStatusTypes().add(xx);
		}
	}
	
	private Long mediaToDelete = null;
	
	public void confirmDeleteMedia(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("mediaId")) {
    			mediaToDelete = ((Long)((UIParameter)com).getValue());
			}
		}
	}
	
	public void cancelDelete(ActionEvent ev) {
	    	System.out.println("\nDelete Media canceled.");
	    	mediaToDelete= null;
	}
	
	public void delete(ActionEvent ev) {
    	System.out.println("\nDelete Test ...");
    	try {
    		mediaService.deleteMedia(mediaToDelete);
    		addInfoMessage(getMessagesBoundle().getString("media.deleted"));
		} catch (Exception e) {
			e.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("media.deletingError"), e);
		}
	}
	
	public void addMedia(ActionEvent e) {
		mediaToEdit = new Media();
		selectedMediaType=null;
		selectedMediaStatusType=null;
	}
	
	public void modifyMedia(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("mediaId")) {
    			mediaToEdit = mediaService.getMediaById((Long)((UIParameter)com).getValue());
    			selectedMediaType=mediaToEdit.getMediaType().getId();
    			selectedMediaStatusType=mediaToEdit.getMediaStatusType().getId();
			}
		}
	}
	
	public void  fastUnlockMedia(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("mediaId")) {
    			mediaToEdit = mediaService.getMediaById((Long)((UIParameter)com).getValue());
    			try {
        			doUnlockMedia();
				} catch (Exception ex) {
					ex.printStackTrace();
				} 
			}
		}
	}
	
	public String getSpectatorOwnerName(Long mediaId) { 
		
//		SpectatorEpisode se= spectatorEpisodeService.getSpectatorEpisodeFinder().and("media.id").eq(mediaId).setDistinct().result();
//		if(se!=null && se.getSpectator()!=null) return se.getSpectator().getName() + " " + se.getSpectator().getSurname();
//		else return "";
		
		List<SpectatorEpisode> ses= spectatorEpisodeService.getSpectatorEpisodeFinder().and("media.id").eq(mediaId).setDistinct().list();
		String res ="";
		String sep ="";
		if (ses!=null) {
			for (SpectatorEpisode se : ses) {
				res += se.getSpectator().getName() + " " + se.getSpectator().getSurname() + "\n"+ se.getProduction().getName() + "\n" +se.getEpisode().getDateFromOnlyDateTime() + sep;
				sep="; \n\n";
			}
		}
		return res;
	}
	
	
//	public String getSpectatorOwnerName(Long mediaId) { 
//		
////		SpectatorEpisode se= spectatorEpisodeService.getSpectatorEpisodeFinder().and("media.id").eq(mediaId).setDistinct().result();
////		if(se!=null && se.getSpectator()!=null) return se.getSpectator().getName() + " " + se.getSpectator().getSurname();
////		else return "";
//		
//		List<SpectatorEpisode> ses= spectatorEpisodeService.getSpectatorEpisodeFinder().and("media.id").eq(mediaId).setDistinct().list();
//		String res ="";
//		if (ses!=null) {
//			for (SpectatorEpisode se : ses) {
//				res += se.getSpectator().getName() + " " + se.getSpectator().getSurname() + "; ";
//			}
//		}
//		return res;
//	}
	
	public void cancelEditMedia(ActionEvent e) {
		mediaToEdit = null;
	}
	
	
	public void unlockMedia(ActionEvent e) throws Exception {
		try {
			doUnlockMedia();
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}			
	}
	
	private void doUnlockMedia() throws Exception {
		try {
			if(mediaToEdit != null) {
				System.out.println("Devo sbloccare il badge");
				List<SpectatorEpisode> lse = spectatorEpisodeService.getSpectatorEpisodeFinder().and("media.id").eq(mediaToEdit.getId()).setDistinct().list();
				if(!lse.isEmpty()) {
					SpectatorEpisode sp = lse.get(0);
					sp.setMedia(null);
					spectatorEpisodeService.updateSpectatorEpisode(sp);
				}
				MediaStatusType mst = mediaStatusTypeService.getMediaStatusTypeFinder().and("idStatus").eq(new Integer(1)).setDistinct().list().get(0);
				mediaToEdit.setMediaStatusType(mst);
				mediaService.updateMedia(mediaToEdit);
			}
		}
		
		catch(Exception ex) {
			ex.printStackTrace();
		}
		mediaToEdit = null;
	}
	
	
	
	public void saveMedia(ActionEvent e) {
    	System.out.println("Salvo " + mediaToEdit);
    	

    	if (mediaToEdit.getCode()==null || mediaToEdit.getCode().trim().equals("")) {
    		addWarningMessage("Code richiesto!");
    		return;
    	}

    	if (selectedMediaType==null) {
    		addWarningMessage("Tipo media richiesto!");
    		return;
    	} 	
    	    	
    	if (getSelectedMediaStatusType()==null) {
    		addWarningMessage("Tipo Stato media richiesto!");
    		return;
    	} 	
    	
    	
    	
    	if (mediaToEdit.getId()==null) {
    		// Nuovo Badge, verifico che non esista gia' un badge con lo stesso codice o label
	    	if (mediaService.getMediaFinder().and("code").eq(mediaToEdit.getCode()).setDistinct().count()>0 || mediaService.getMediaFinder().and("label").eq(mediaToEdit.getLabel()).setDistinct().count()>0) {
	    		addWarningMessage("Esiste già un Badge con lo stesso codice!");
	    		return;
	    	}
    	}
    	
    	
    	
    	try {
    		//Imposta il valore della dropdown list nella classe
    		mediaToEdit.setMediaType(mediaTypeService.getMediaTypeById(selectedMediaType));
    		mediaToEdit.setMediaStatusType(mediaStatusTypeService.getMediaStatusTypeById(getSelectedMediaStatusType()));
    		
    		//Effettuo il controllo per verificare che non si sia gia' inserita un media con stesso codice e stesso numero
//    		if(!checkMedia(mediaToEdit.getCode(), selectedMediaType, mediaToEdit.getLabel())) {
//    			mediaToEdit = mediaService.updateMedia(mediaToEdit);
//    			
//    		}else {
//    			addWarningMessage("media gia' presente in archivio");
//    			return;
//    		}
    		
    		if(!checkMedia(mediaToEdit.getCode(), mediaToEdit.getId(), selectedMediaType, mediaToEdit.getLabel())) {
    			mediaToEdit = mediaService.updateMedia(mediaToEdit);
    		}
    		else {
    			addWarningMessage("media gia' presente in archivio");
    			return;
    		}
    	} catch (PersistenceException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();

    	} catch (DataIntegrityViolationException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();
    	    		
    	} catch (Exception e1) {
			addErrorMessage(getMessagesBoundle().getString("media.savingError"), e1);
			e1.printStackTrace();
		}
    	mediaToEdit = null;
	}
	
	public void filterByDate() {
		System.out.println("Filter by date !");
		
		Calendar cal = Calendar.getInstance();
		if (filterFrom!=null) {
			cal.setTime(filterFrom);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 1);
			filterFrom = cal.getTime();
		}
		if (filterTo!=null) {
			cal.setTime(filterTo);
			cal.set(Calendar.HOUR_OF_DAY, 23);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);
			filterTo = cal.getTime();
		}
		
		((LazyMediaDataModel)lazyModelMedia).setFilterFrom(filterFrom);
		((LazyMediaDataModel)lazyModelMedia).setFilterTo(filterTo);
	}
	
	//Check Duplicate Media
	private boolean checkMedia2(String codice, Long selectedMediaType2, String etichetta){
		try {
			//List<Media> listMedia = mediaService.getMediaFinder().and("code").eq(codice).list();
			
			List<Media> listMedia = mediaService.getMediaFinder().and("code").eq(codice).setDistinct().list();
			if(listMedia.size()==0) {
				List<Media> listMedia2 = mediaService.getMediaFinder().and("code").eq(codice).and("mediaType.id").eq(selectedMediaType2).setDistinct().list();
				if(listMedia2.size()==0) {
					List<Media> listMedia3 = mediaService.getMediaFinder().and("label").eq(etichetta).setDistinct().list();
					if(listMedia3.size()==0) {
					 return false;
					}
					return true;
				}
				return true;
			}
			else {
				return true;
			}
		}
		catch(Exception ex) {
			return true; 
		}
	}
	
	
	
	private boolean checkMedia(String codice, Long idMedia, Long selectedMediaType2, String etichetta) {
		try {
			
			if(idMedia!=null) {
				Media tempMedia = mediaService.getMediaFinder().and("id").eq(idMedia).setDistinct().list().get(0);
				if(tempMedia!=null) {
					//Verifico se il vecchio dato e' uguale al nuovo dato
					if(!tempMedia.getCode().equals(codice)) {
						//Procedo con la verifica lato basedati
						Media tempMedia2 = mediaService.getMediaFinder().and("code").eq(codice).setDistinct().list().get(0);
						if(tempMedia2!=null) {
							List<Media> listMedia2 = mediaService.getMediaFinder().and("code").eq(codice).and("mediaType.id").eq(selectedMediaType2).setDistinct().list();
							if(listMedia2.size()==0) {
								List<Media> listMedia3 = mediaService.getMediaFinder().and("label").eq(etichetta).setDistinct().list();
								if(listMedia3.size()==0) {
								 return false;
								}
								return true;
							}
						}
					}else {return false;}
				}
			}
			else {
				Media tempMedia2 = mediaService.getMediaFinder().and("code").eq(codice).setDistinct().list().get(0);
				if(tempMedia2!=null) {
					List<Media> listMedia2 = mediaService.getMediaFinder().and("code").eq(codice).and("mediaType.id").eq(selectedMediaType2).setDistinct().list();
					if(listMedia2.size()==0) {
						List<Media> listMedia3 = mediaService.getMediaFinder().and("label").eq(etichetta).setDistinct().list();
						if(listMedia3.size()==0) {
						 return false;
						}
						return true;
					}
				}
			}
		}catch(Exception ex) {
			return false;
		}
		return true;
	}
	

	
	public Object getMediaById(long id) {
		return mediaService.getMediaById(id);
	} 

	public Media getMediaToEdit() {
		return mediaToEdit;
	}

	public void setMediaToEdit(Media empToEdit) {
		this.mediaToEdit = empToEdit;
	}

	public LazyDataModel<Media> getLazyModelMedia() {
		return lazyModelMedia;
	}

	public Date getFilterFrom() {
		return filterFrom;
	}

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public Date getFilterTo() {
		return filterTo;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}

	public List<SelectItem> getMediaTypes() {
		return this.mediaTypes;
	}

	public void setMediaTypes(List<SelectItem> mediaTypes) {
		this.mediaTypes = mediaTypes;
	}

	public Long getSelectedMediaType() {
		return selectedMediaType;
	}

	public void setSelectedMediaType(Long selectedMediaType) {
		this.selectedMediaType = selectedMediaType;
	}

	public List<SelectItem> getMediaStatusTypes() {
		return mediaStatusTypes;
	}

	public void setMediaStatusTypes(List<SelectItem> mediaStatusTypes) {
		this.mediaStatusTypes = mediaStatusTypes;
	}

	public Long getSelectedMediaStatusType() {
		return selectedMediaStatusType;
	}

	public void setSelectedMediaStatusType(Long selectedMediaStatusType) {
		this.selectedMediaStatusType = selectedMediaStatusType;
	}

	
	
	
	
	private Date lastHeartbeat = new Date();
	public void heartbeat() {
		lastHeartbeat = new Date();
	}
	
	
	
	
	@Override
	public void eventReceived(Location location, Media media, String errorMsg, String badgeId) {
		System.out.println("Ricevuto evento dal desktop reader desk:" + location + "   badgeId:" + badgeId);
		
		
		if (lastHeartbeat==null || ((new Date()).getTime()-lastHeartbeat.getTime())> (1000*11)) { // se non ricevo heartbeat per più di 11 secondi
			System.out.println("!!! bean non collegato alla pagina (tab o browser chiuso dall'utente senza aver fatto logout) !!!  ... aspetto che scada la sessione");
			return;
		}
		
		// Controllo se è il mio desk !!!!!!!!!
		if (location==null || location.getId()==null || !location.getId().equals(selectedDesk)) {
			System.out.println("non è il mio desk!");
			return;
		}
		
//		if (errorMsg==null) {
			String error = "";
			
			if (mediaToEdit!=null) {
				mediaToEdit.setCode(badgeId);
				
				// ATT ... se error è null, allora non scatta l'ajax update lato pagina
				
				// Notifico il mio browser (legato alla sessionId)
				EventBus eventBus = EventBusFactory.getDefault().eventBus();
				eventBus.publish("/badge/"+ sessionId, error);
			}
//		}
//		else {
//			// Notifico il mio browser (legato alla sessionId)
//			EventBus eventBus = EventBusFactory.getDefault().eventBus();
//			eventBus.publish("/badge/"+ sessionId, errorMsg);
//		}
		
	}

	public Long getSelectedDesk() {
		return selectedDesk;
	}

	public void setSelectedDesk(Long selectedDesk) {
		this.selectedDesk = selectedDesk;
	}

	public List<SelectItem> getDesks() {
		return desks;
	}

	public void setDesks(List<SelectItem> desks) {
		this.desks = desks;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
}
