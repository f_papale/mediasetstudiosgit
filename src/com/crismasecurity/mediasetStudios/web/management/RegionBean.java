package com.crismasecurity.mediasetStudios.web.management;

import java.util.Date;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.event.ActionEvent;
import javax.persistence.PersistenceException;

import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;


import com.crismasecurity.mediasetStudios.core.entity.Region;
import com.crismasecurity.mediasetStudios.core.services.RegionService;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;

@Controller (RegionBean.BEAN_NAME) 
@Scope("view")
public class RegionBean extends BaseBean {



	/**
	 * 
	 */
	private static final long serialVersionUID = 5612949758673899558L;

	public static final String BEAN_NAME = "regionBean";
    public String getBeanName() { return BEAN_NAME; } 


    @Autowired
    private RegionService regionService ;
    
    private LazyDataModel<Region> lazyModelRegion;
    private Date filterFrom;
	private Date filterTo;
	
	private Region regionToEdit = null;
    
	public RegionBean() {
		getLog().info("!!!! Costruttore di RegionBean !!!");	
    }

	@PostConstruct
	public void init() {
		getLog().info("INIT RegionBean!");
        if(regionService.getRegion().isEmpty() || regionService.getRegion() == null) {
        	lazyModelRegion = null;
        }else {lazyModelRegion = new LazyRegionDataModel(regionService);	}
	}

	@PreDestroy
	public void destroy() {
		getLog().info("Destroy RegionBean!");

	}
	
	private Long regionToDelete = null;
	public void confirmDeleteRegion(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("regionId")) {
    			regionToDelete = ((Long)((UIParameter)com).getValue());
			}
		}
    	
	}
	
	public void cancelDelete(ActionEvent ev) {
	    	System.out.println("\nDelete Region canceled.");
	    	regionToDelete= null;
	}
	
	public void delete(ActionEvent ev) {
    	System.out.println("\nDelete Test ...");
    	try {
        	
    		regionService.deleteRegion(regionToDelete);
//    		employees=null;
    		addInfoMessage(getMessagesBoundle().getString("region.deleted"));
		} catch (Exception e) {
			e.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("region.deletingError"), e);
		}
	}
	
	public void addRegion(ActionEvent e) {
		regionToEdit = new Region();
	}
	
	public void modifyRegion(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("regionId")) {
    			regionToEdit = regionService.getRegionById((Long)((UIParameter)com).getValue());
			}
		}
	}
	
	public void cancelEditRegion(ActionEvent e) {
		regionToEdit = null;
	}
	
	public void saveRegion(ActionEvent e) {
    	System.out.println("Salvo " + regionToEdit);
    	

    	if (regionToEdit.getRegionDescription()==null || regionToEdit.getRegionDescription().trim().equals("")) {
    		addWarningMessage("test su Descrizione richiesto!");
    		return;
    	}
    	
    	
    	try {
    		regionToEdit = regionService.updateRegion(regionToEdit);
		
			addInfoMessage(getMessagesBoundle().getString("region.saved"));
    	} catch (PersistenceException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();

    	} catch (DataIntegrityViolationException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();
    				
		} catch (Exception e1) {
			addErrorMessage(getMessagesBoundle().getString("region.savingError"), e1);
			e1.printStackTrace();
		}
    	regionToEdit = null;
	}
	
	public Object getRegionById(long id) {
		return regionService.getRegionById(id);
	}

	public Region getRegionToEdit() {
		return regionToEdit;
	}

	public void setRegionToEdit(Region empToEdit) {
		this.regionToEdit = empToEdit;
	}

	public LazyDataModel<Region> getLazyModelRegion() {
		return lazyModelRegion;
	}

	/*public Date getFilterFrom() {
		return filterFrom;
	}

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public Date getFilterTo() {
		return filterTo;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}*/

}
