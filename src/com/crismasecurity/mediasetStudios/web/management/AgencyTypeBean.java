package com.crismasecurity.mediasetStudios.web.management;

import java.util.Calendar;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.event.ActionEvent;
import javax.persistence.PersistenceException;

import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;


import com.crismasecurity.mediasetStudios.core.entity.AgencyType;
import com.crismasecurity.mediasetStudios.core.services.AgencyTypeService;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;

//@ManagedBean(name= EmployeesBean.BEAN_NAME) // JSF managed
//@SessionScoped // JSF managed
//@ViewScoped // JSF managed

//Spring managed
@Controller (AgencyTypeBean.BEAN_NAME) 
//@Scope("session")
@Scope("view")
public class AgencyTypeBean extends BaseBean {

	private static final long serialVersionUID = -6065141989753335782L;

	public static final String BEAN_NAME = "agencyTypeBean";
    public String getBeanName() { return BEAN_NAME; } 


    @Autowired
    private AgencyTypeService agencyTypeService ;
    
    private LazyDataModel<AgencyType> lazyModelAgencyType;
    private Date filterFrom;
	private Date filterTo;
	
	private AgencyType agencyTypeToEdit = null;
    
	public AgencyTypeBean() {
		getLog().info("!!!! Costruttore di AgencyTypeBean !!!");	
    }

	@PostConstruct
	public void init() {
		getLog().info("INIT AgencyTypeBean!");

		setLazyModelAgencyType(new LazyAgencyTypeDataModel(agencyTypeService));	
	}

	@PreDestroy
	public void destroy() {
		getLog().info("Destroy AgencyTypeBean!");

	}
	
	private Long agencyTypeToDelete = null;
	public void confirmDeleteAgencyType(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("agencyTypeId")) {
    			agencyTypeToDelete = ((Long)((UIParameter)com).getValue());
			}
		}
    	
	}
	
	public boolean haveUserRole(String roles) {
		
		String[] listRoles = roles.split(",");
		for (int i = 0; i < listRoles.length; i++) {
			listRoles[i] = listRoles[i].trim();
			
		}
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication!=null && authentication.getAuthorities()!=null) {
			//String currentPrincipalName = authentication.getName();
			for (GrantedAuthority r : authentication.getAuthorities()) {
				//System.out.println("Ruolo:"+r.getAuthority());
				for (int i = 0; i < listRoles.length; i++) {
					if (r.getAuthority()!=null && r.getAuthority().equals(listRoles[i])) {
						//System.out.println("trovato ruolo !!!");
						return true;
					}					
				}
			}
		}
		return false;
	}	
	
	public void cancelDelete(ActionEvent ev) {
	    	System.out.println("\nDelete AgencyType canceled.");
	    	agencyTypeToDelete= null;
	}
	
	public void delete(ActionEvent ev) {
    	System.out.println("\nDelete Test ...");
    	try {
        	
    		agencyTypeService.deleteAgencyType(agencyTypeToDelete);
//    		employees=null;
    		addInfoMessage(getMessagesBoundle().getString("agencyType.deleted"));
		} catch (Exception e) {
			e.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("agencyType.deletingError"), e);
		}
	}
	
	public void addAgencyType(ActionEvent e) {
		agencyTypeToEdit = new AgencyType();
	}
	
	public void modifyAgencyType(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("agencyTypeId")) {
    			agencyTypeToEdit = agencyTypeService.getAgencyTypeById((Long)((UIParameter)com).getValue());
			}
		}
	}
	
	public void cancelEditAgencyType(ActionEvent e) {
		agencyTypeToEdit = null;
	}
	
	public void saveAgencyType(ActionEvent e) {
    	System.out.println("Salvo " + agencyTypeToEdit);
    	

    	if (agencyTypeToEdit.getDescription()==null || agencyTypeToEdit.getDescription().trim().equals("")) {
    		addWarningMessage("test su Descrizione richiesto!");
    		return;
    	}
    	
    	
    	try {
    		agencyTypeToEdit = agencyTypeService.updateAgencyType(agencyTypeToEdit);
		
			addInfoMessage(getMessagesBoundle().getString("agencyType.saved"));
    	} catch (PersistenceException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();

    	} catch (DataIntegrityViolationException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();
    				
		} catch (Exception e1) {
			addErrorMessage(getMessagesBoundle().getString("agencyType.savingError"), e1);
			e1.printStackTrace();
		}
    	agencyTypeToEdit = null;
	}
	
	public void filterByDate() {
		System.out.println("Filter by date !");
		
		Calendar cal = Calendar.getInstance();
		if (filterFrom!=null) {
			cal.setTime(filterFrom);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 1);
			filterFrom = cal.getTime();
		}
		if (filterTo!=null) {
			cal.setTime(filterTo);
			cal.set(Calendar.HOUR_OF_DAY, 23);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);
			filterTo = cal.getTime();
		}
		
		((LazyAgencyTypeDataModel)getLazyModelAgencyType()).setFilterFrom(filterFrom);
		((LazyAgencyTypeDataModel)getLazyModelAgencyType()).setFilterTo(filterTo);
	}
	
	public Object getAgencyTypeById(long id) {
		return agencyTypeService.getAgencyTypeById(id);
	}

	public AgencyType getAgencyTypeToEdit() {
		return agencyTypeToEdit;
	}

	public void setAgencyTypeToEdit(AgencyType empToEdit) {
		this.agencyTypeToEdit = empToEdit;
	}

	public LazyDataModel<AgencyType> getLazyModelAgencyType() {
		return lazyModelAgencyType;
	}

	public Date getFilterFrom() {
		return filterFrom;
	}

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public Date getFilterTo() {
		return filterTo;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}

	public void setLazyModelAgencyType(LazyDataModel<AgencyType> lazyModelAgencyType) {
		this.lazyModelAgencyType = lazyModelAgencyType;
	}

}
