package com.crismasecurity.mediasetStudios.web.management;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.event.ActionEvent;
import javax.persistence.PersistenceException;

import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import com.crismasecurity.mediasetStudios.core.entity.LocationType;
import com.crismasecurity.mediasetStudios.core.services.LocationTypeService;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;

//Spring managed
@Controller (LocationTypeBean.BEAN_NAME) 
//@Scope("session")
@Scope("view")
public class LocationTypeBean extends BaseBean{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5662158989060606869L;
	
	public static final String BEAN_NAME = "locationTypeBean";
	public String getBeanName() { return BEAN_NAME; } 

	@Autowired
	private LocationTypeService locationTypeService;

	private LazyDataModel<LocationType> lazyModelLocationType;
	private LocationType locationTypeToEdit = null;
	private Long locationTypeToDelete = null;

	
	
	public LocationTypeBean() {
		getLog().info("!!!! Costruttore di LocationTypeBean !!!");
	}
	
	@PostConstruct
	public void init() {
		getLog().info("INIT LocationTypeBean!");
		lazyModelLocationType = new LazyLocationTypeDataModel(locationTypeService);
	}
	
	@PreDestroy
	public void destroy() {
		getLog().info("Destroy LocationTypeBean!");
	}
	
	public void confirmDeleteLocationType(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("locationTypeId")) {
    			locationTypeToDelete = ((Long)((UIParameter)com).getValue());
			}
		}    	
	}
	
	public void cancelDelete(ActionEvent ev) {
    	System.out.println("\nDelete LocationType canceled.");
    	locationTypeToDelete= null;
	}
	
	public void delete(ActionEvent ev) {
    	System.out.println("\nDelete Test ...");
    	try {
    		locationTypeService.deleteLocationType(locationTypeToDelete);
    		addInfoMessage(getMessagesBoundle().getString("locationType.deleted"));
		} catch (Exception e) {
			e.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("locationType.deletingError"), e);
		}
    }
	
	public void addLocationType(ActionEvent e) {
		locationTypeToEdit = new LocationType();
	}
	
	public void modifyLocationType(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("locationTypeId")) {
    			locationTypeToEdit = locationTypeService.getLocationTypeById((Long)((UIParameter)com).getValue());
    		}
		}
	}
	
	public boolean haveUserRole(String roles) {
		
		String[] listRoles = roles.split(",");
		for (int i = 0; i < listRoles.length; i++) {
			listRoles[i] = listRoles[i].trim();
			
		}
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication!=null && authentication.getAuthorities()!=null) {
			//String currentPrincipalName = authentication.getName();
			for (GrantedAuthority r : authentication.getAuthorities()) {
				//System.out.println("Ruolo:"+r.getAuthority());
				for (int i = 0; i < listRoles.length; i++) {
					if (r.getAuthority()!=null && r.getAuthority().equals(listRoles[i])) {
						//System.out.println("trovato ruolo !!!");
						return true;
					}					
				}
			}
		}
		return false;
	}	
	
	public void cancelEditLocationType(ActionEvent e) {
		locationTypeToEdit = null;
	}
	
	public void saveLocationType(ActionEvent e) {
    	System.out.println("Salvo " + locationTypeToEdit);
    	if (locationTypeToEdit.getDescription()==null || locationTypeToEdit.getDescription().trim().equals("")) {
    		addWarningMessage("test su Descrizione richiesto!");
    		return;
    	}
    	try {
    		locationTypeToEdit = locationTypeService.updateLocationType(locationTypeToEdit);
    		addInfoMessage(getMessagesBoundle().getString("locationType.saved"));
    	} catch (PersistenceException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();

    	} catch (DataIntegrityViolationException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();
    	    		
		} catch (Exception e1) {
			addErrorMessage(getMessagesBoundle().getString("locationType.savingError"), e1);
			e1.printStackTrace();
		}
    	locationTypeToEdit = null;
	}
	
	public Object getLocationTypeById(long id) {
		return locationTypeService.getLocationTypeById(id);
	}

	public LocationType getLocationTypeToEdit() {
		return locationTypeToEdit;
	}

	public void seLocationTypeToEdit(LocationType empToEdit) {
		this.locationTypeToEdit = empToEdit;
	}

	public LazyDataModel<LocationType> getLazyModelLocationType() {
		return lazyModelLocationType;
	}
	
	
	
}
