package com.crismasecurity.mediasetStudios.web.management;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.crismasecurity.mediasetStudios.core.entity.AgencyType;
import com.crismasecurity.mediasetStudios.core.services.AgencyTypeService;
 

public class LazyAgencyTypeDataModel extends LazyDataModel<AgencyType> {

	private static final long serialVersionUID = -1084449649955220482L;

	private AgencyTypeService agencyTypeService;
	
	private Date filterFrom;
	private Date filterTo;

     
    public LazyAgencyTypeDataModel(AgencyTypeService agencyTypeService) {
        this.agencyTypeService = agencyTypeService;
    }
     
    @Override
    public AgencyType getRowData(String rowKey) {
    	if (rowKey==null || rowKey.trim().equals("")) return null;
    	return agencyTypeService.getAgencyTypeById(Long.parseLong(rowKey));
    }
 
    @Override
    public Object getRowKey(AgencyType audit) {
        return audit.getId();
    }
 
    @Override
    public List<AgencyType> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
    	
    	// Converto ordinamento
    	int sortInt =0; // UNSORTED
    	if (sortOrder.equals(SortOrder.ASCENDING)) sortInt = 1;
    	if (sortOrder.equals(SortOrder.DESCENDING)) sortInt = 2;
    	
 
		if (filterFrom!=null) filters.put("date#ge", filterFrom);
		if (filterTo!=null) filters.put("date#le", filterTo);

    	
    	List<AgencyType> ret = agencyTypeService.getAgencyTypeLazyLoading(first, pageSize, sortField, sortInt, filters);
    	this.setRowCount((int)agencyTypeService.getAgencyTypeLazyLoadingCount(filters));
//    	System.out.println("Numero elementi trovati: " + this.getRowCount());

    	return ret;    
    }

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}


}