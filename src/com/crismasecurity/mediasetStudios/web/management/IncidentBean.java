package com.crismasecurity.mediasetStudios.web.management;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.persistence.PersistenceException;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import com.crismasecurity.mediasetStudios.core.entity.BlackListSpectator;
import com.crismasecurity.mediasetStudios.core.entity.BlackListType;
import com.crismasecurity.mediasetStudios.core.entity.Episode;
import com.crismasecurity.mediasetStudios.core.entity.Incident;
import com.crismasecurity.mediasetStudios.core.entity.IncidentType;
import com.crismasecurity.mediasetStudios.core.entity.SeverityLevel;
import com.crismasecurity.mediasetStudios.core.entity.Spectator;
import com.crismasecurity.mediasetStudios.core.entity.SpectatorEpisode;
import com.crismasecurity.mediasetStudios.core.entity.SpectatorIncident;
import com.crismasecurity.mediasetStudios.core.services.BlackListSpectatorService;
import com.crismasecurity.mediasetStudios.core.services.BlackListTypeService;
import com.crismasecurity.mediasetStudios.core.services.EpisodeService;
import com.crismasecurity.mediasetStudios.core.services.IncidentService;
import com.crismasecurity.mediasetStudios.core.services.IncidentTypeService;
import com.crismasecurity.mediasetStudios.core.services.ProductionService;
import com.crismasecurity.mediasetStudios.core.services.SeverityLevelService;
import com.crismasecurity.mediasetStudios.core.services.SpectatorEpisodeService;
import com.crismasecurity.mediasetStudios.core.services.SpectatorService;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;

@Controller (IncidentBean.BEAN_NAME)  
@Scope("view")
public class IncidentBean extends BaseBean{

	private static final long serialVersionUID = -5437322277469086379L;
	
	public static final String BEAN_NAME = "incidentBean";
    public String getBeanName() { return BEAN_NAME; } 

    @Autowired
    private IncidentService incidentService;
    
    @Autowired
    private ProductionService productionService;
    
    @Autowired
    private EpisodeService episodeService;
    
    @Autowired
    private SpectatorService spectatorService;
    
    @Autowired
    private SeverityLevelService severityLevelService;
    
    @Autowired
    private IncidentTypeService incidentTypeService;
    
    @Autowired
    private BlackListTypeService blackListTypeService;
    
    @Autowired
    private BlackListSpectatorService blackListSpectatorService;
    
    @Autowired
    private SpectatorEpisodeService spectatorEpisodeService;
    
    private LazyDataModel<Incident> lazyModelIncident;
    private Incident incidentToEdit = null;
    private Long incidentToDelete = null; 
    private List<SelectItem> productionList = new ArrayList<SelectItem>();
    private Long selectedProduction = null;
    private List<SelectItem> episodeList = new ArrayList<SelectItem>();
    private Long selectedEpisode = null;
    private List<SelectItem> severityLevelList;
    private Long selectedSeverityLevel = null;
    private List<SelectItem> typeIncidentList;
    private Long selectedTypeIncident = null;
    private List<SelectItem> studioList = new ArrayList<SelectItem>();
    private Long selectedStudio = null;
    private int activeTab = 0;
    private Spectator spectatorToEdit = null;
    private Long selectedSpectatorIncident = null;
    private List<SelectItem> spectatorList = new ArrayList<SelectItem>();
    private Long selectedTypeBlackList = null;
    private List<SelectItem> typeBlackList = new ArrayList<SelectItem>();
    private Long spectatorToDelete = null;
    private String nameStudio = "";
    
    public IncidentBean() {
    	getLog().info("!!!! Costruttore di incident !!!");
    }
    
    @PostConstruct
	public void init() {
		getLog().info("INIT incidentBean!");
		getLog().info("getIncident!\n");
		if (incidentService.getIncident().isEmpty() || incidentService.getIncident() == null) {
			lazyModelIncident = null;
		} else {
			lazyModelIncident = new LazyIncidentDataModel(incidentService);
		}
		this.presetSeverityLevelList();
		this.presetTypeIncidentList();
		this.presetTypeBlackList();
    }

	@PreDestroy
	public void destroy() {
		getLog().info("Destroy IncidentBean!");

	}

	private void presetSeverityLevelList() {
		severityLevelList = new ArrayList<SelectItem>();
		List<SeverityLevel> slList = severityLevelService.getSeverityLevel();
		severityLevelList.add(new SelectItem(null, " "));
		for(SeverityLevel element : slList) {
			SelectItem xx = new SelectItem(element.getId(), element.getDescription());
			severityLevelList.add(xx);
		}
	}
	
	private void presetTypeIncidentList() {
		typeIncidentList = new ArrayList<SelectItem>();
		List<IncidentType> glList = incidentTypeService.getIncidentType();
		typeIncidentList.add(new SelectItem(null, " "));
		for(IncidentType element : glList) {
			SelectItem xx = new SelectItem(element.getId(), element.getDescription());
			typeIncidentList.add(xx);
		}
	}
	
	private void presetTypeBlackList() {
		typeBlackList = new ArrayList<SelectItem>();
		List<BlackListType> blackListType = blackListTypeService.getBlackListType();
		typeBlackList.add(new SelectItem(null, " "));
		for(BlackListType element : blackListType) {
			SelectItem xx = new SelectItem(element.getId(), element.getDescription());
			typeBlackList.add(xx);
		}
	}
	
	
	
	public void loadStudio(SelectEvent event) {
		studioList.clear();
		episodeList.clear();
		populateStudioEpisode(selectedProduction);
	}
	
	
	public void loadEpisode(ValueChangeEvent e) {
		selectedProduction = new Long("-1");
		if(e.getNewValue()!=null) {
			selectedProduction = new Long(e.getNewValue().toString()).longValue();
			episodeList.clear();
			List<Episode> epList = episodeService.getEpisode();
			episodeList.add(new SelectItem(null, " "));
			for(Episode elementEpisode : epList) {
				if(elementEpisode.getProduction().getId() == selectedProduction) {
					SelectItem xx = new SelectItem(elementEpisode.getId(), elementEpisode.getVirtualName());
					episodeList.add(xx);
				}
			}
		}
	}

	public void selectDateTime(SelectEvent event) {
		try {
			long tempSelectedEpisode = new Long(selectedEpisode);
			Episode episode = episodeService.getEpisodeById(tempSelectedEpisode);
			selectedEpisode = tempSelectedEpisode;
			selectedStudio = episode.getStudio().getId();
			nameStudio = episode.getStudio().getName();
		}
		catch(Exception ex) {
			addInfoMessage(getMessagesBoundle().getString("incident.errordateincident"));
		}
	}
	
	
	public void addSpectator(ActionEvent e) {
		spectatorToEdit = new Spectator();
		List<Spectator> spList = new ArrayList<Spectator>();
		spectatorList.add(new SelectItem(null, ""));
		for(Spectator element: spList) {
			if(!isPresentSpectator(element)) {
				SelectItem xx = new SelectItem(element.getId(), element.getSurname() + " " + element.getName());
				spectatorList.add(xx);
			}
		}
		setActiveTab(0);
	}
	
	private boolean isPresentSpectator(Spectator spectator) {
		if(incidentToEdit.getSpectatorIncident()!=null) {
			for(SpectatorIncident blSpectator : incidentToEdit.getSpectatorIncident()) {
				if(blSpectator.getSpectator().getId().equals(spectator.getId())) {
					return true;
				}
			}
		}
		else {
			return false;
		}
		return false;
	}

	public void saveIncidentSpectator(ActionEvent e) {

			BlackListSpectator blSpectator = null;
			BlackListType blType = null;
			if (selectedTypeBlackList!=null && selectedTypeBlackList.longValue()>0) {
				blSpectator = new BlackListSpectator();
				blType = blackListTypeService.getBlackListTypeById(selectedTypeBlackList);
				blSpectator.setBlackListType(blType);
				blSpectator.setSpectator(spectatorToEdit);
			}
			
			SpectatorIncident spectatorIncident = new SpectatorIncident();
			spectatorIncident.setSpectator(spectatorToEdit);
			spectatorIncident.setIncident(incidentToEdit);
			spectatorIncident.setBlackListType(blType);
			if (incidentToEdit.getSpectatorIncident()==null) incidentToEdit.setSpectatorIncident(new ArrayList<SpectatorIncident>());
			incidentToEdit.getSpectatorIncident().add(spectatorIncident);
		spectatorToEdit = null;
	}

	public void cancelEditSpectator(ActionEvent e) {
		spectatorToEdit = null;
	}
	
	
	public void modifySpectator(ActionEvent e) {
		HtmlCommandLink component = (HtmlCommandLink) e.getSource();
		for (UIComponent com : component.getChildren()) {
			if (com instanceof UIParameter && ((UIParameter) com).getName().equalsIgnoreCase("spectatorId")) {
				Long selectSpectatorToEdit = ((Long) ((UIParameter) com).getValue());
				spectatorToEdit = spectatorService.getSpectatorById(selectSpectatorToEdit);
				if (spectatorToEdit!=null) selectedSpectatorIncident = spectatorToEdit.getId();
				else selectedSpectatorIncident = null;
				
				// Lista spettatori
				List<SpectatorEpisode> speList = spectatorEpisodeService.getSpectatorEpisodeFinder().and("episode.id").eq(selectedEpisode).setDistinct().list();
				if(spectatorList.size()>0) {
					spectatorList.clear();
				}
				spectatorList.add(new SelectItem(null, ""));
				for(SpectatorEpisode element: speList) {
					if(!isPresentSpectator(element.getSpectator()) || element.getSpectator().getId().equals(selectedSpectatorIncident)) {
						SelectItem xx = new SelectItem(element.getSpectator().getId(), element.getSpectator().getSurname() + " " + element.getSpectator().getName());
						spectatorList.add(xx);
					}
				}
				
				System.out.println("modifySpectator - param:" + selectSpectatorToEdit + "   selectedSpectatorIncident:" + selectedSpectatorIncident);
				
				if(incidentToEdit.getSpectatorIncident()!=null) {
					for (SpectatorIncident s : incidentToEdit.getSpectatorIncident()) {
						if (s.getSpectator().getId()==selectSpectatorToEdit) {
							if (s.getBlackListType()!=null) selectedTypeBlackList = s.getBlackListType().getId();
							else selectedTypeBlackList=null;
							break;
						}
					}
				}
			}
		}
	}
	
	
	public void confirmDeleteSpectator(ActionEvent e) {
		HtmlCommandLink component = (HtmlCommandLink) e.getSource();
		for (UIComponent com : component.getChildren()) {
			if (com instanceof UIParameter && ((UIParameter) com).getName().equalsIgnoreCase("spectatorId")) {
				spectatorToDelete = ((Long) ((UIParameter) com).getValue());
			}
		}
	}
	
	public void deleteSpectator(ActionEvent e) {

		if(incidentToEdit.getSpectatorIncident().size()>0) {
			for(SpectatorIncident elementSpectator : incidentToEdit.getSpectatorIncident()) {
				if(elementSpectator.getSpectator().getId() == spectatorToDelete) {
					incidentToEdit.getSpectatorIncident().remove(elementSpectator);
					break;
				}
			}
		}
	}
	
	
	
	public void saveIncident(ActionEvent e) {
		try {
			
			System.out.println("Devo fare il salvataggio dell'incident");
			SeverityLevel severityLevel = severityLevelService.getSeverityLevelById(selectedSeverityLevel);
			incidentToEdit.setSeverityLevel(severityLevel);
			
			IncidentType incidentType = incidentTypeService.getIncidentTypeById(selectedTypeIncident);
			incidentToEdit.setIncidentType(incidentType);
			
			incidentToEdit = incidentService.updateIncident(incidentToEdit);
			
			// Gestione blacklist
			if(incidentToEdit.getSpectatorIncident()!=null) {
				for(SpectatorIncident spin : incidentToEdit.getSpectatorIncident()) {
					BlackListSpectator blsp = blackListSpectatorService.getBlackListSpectatorFinder().and("incident.id").eq(incidentToEdit.getIdIncident()).and("spectator.id").eq(spin.getSpectator().getId()).setDistinct().result();
					if (spin.getBlackListType()!=null) {
						// Aggiungo/aggiorno a blacklist
						if (blsp!=null) {
							// Update
							blsp.setBlackListType(spin.getBlackListType());
							blsp.setDateTimeBlackList(incidentToEdit.getDateTimeIncident());
							blsp.setReason(incidentToEdit.getLongDescription());
							blackListSpectatorService.updateBlackListSpectator(blsp);
						}
						else {
							// Add
							BlackListSpectator nblsp = new BlackListSpectator();
							nblsp.setBlackListType(spin.getBlackListType());
							nblsp.setIncident(spin.getIncident());
							nblsp.setSpectator(spin.getSpectator());
							nblsp.setDateTimeBlackList(incidentToEdit.getDateTimeIncident());
							nblsp.setReason(incidentToEdit.getLongDescription());
							blackListSpectatorService.addBlackListSpectator(nblsp);
						}
					}
					else {
						// Cancello da backlist
						if (blsp!=null) blackListSpectatorService.deleteBlackListSpectator(blsp.getIdBlackListSpectator());
					}
				}
			}
			addInfoMessage(getMessagesBoundle().getString("incident.saved"));
    	} catch (PersistenceException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();

    	} catch (DataIntegrityViolationException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();
    				
		} catch (Exception e1) {
			addErrorMessage(getMessagesBoundle().getString("incident.savingError"), e1);
			e1.printStackTrace();
		}
		incidentToEdit = null;
	}

	
	public void modifyIncident(ActionEvent e) {
		
		Long incidentId = 0L;
		HtmlCommandLink component = (HtmlCommandLink) e.getSource();
		for (UIComponent com : component.getChildren()) {
			if (com instanceof UIParameter && ((UIParameter) com).getName().equalsIgnoreCase("incidentId")) {
				incidentId = ((Long)((UIParameter) com).getValue());
			}
		}
		
		if (incidentId!=0) {
			incidentToEdit = incidentService.getIncidentById(incidentId);
			 
			if(incidentToEdit.getIncidentType()!=null) {
				selectedTypeIncident = incidentToEdit.getIncidentType().getId();
			}
			
			
			if(incidentToEdit.getSeverityLevel()!=null) {
				selectedSeverityLevel = incidentToEdit.getSeverityLevel().getId();
			}
		}
	}	
	
	public void confirmDeleteIncident(ActionEvent e) {
		HtmlCommandLink component = (HtmlCommandLink) e.getSource();
		for (UIComponent com : component.getChildren()) {
			if (com instanceof UIParameter && ((UIParameter) com).getName().equalsIgnoreCase("incidentId")) {
				incidentToDelete = ((Long) ((UIParameter) com).getValue());
			}
		}

	}

	public void cancelDelete(ActionEvent ev) {
		System.out.println("\nDelete Incident canceled.");
		incidentToDelete = null;
	}

	public void delete(ActionEvent ev) {
		System.out.println("\nDelete Test ...");
		try {
			
			incidentService.deleteIncident(incidentToDelete);
			
			addInfoMessage(getMessagesBoundle().getString("incident.deleted"));
		} catch (Exception e) {
			e.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("incident.deletingError"), e);
		}
	}

	public void addIncident(ActionEvent e) {
		incidentToEdit = new Incident();
		selectedProduction = null;
		selectedEpisode = null;
		selectedSeverityLevel = null;
		selectedStudio = null;
		selectedTypeIncident = null;
	}

	public void cancelEditIncident(ActionEvent e) {
		incidentToEdit = null;
	}

	private void populateStudioEpisode(Long productionSelected) {
		studioList.clear();
		episodeList.clear();
	
		List<Episode> listEpisode = episodeService.getEpisodeFinder().and("production.id").eq(productionSelected).setDistinct().list();		
		if(!listEpisode.isEmpty()) {
			episodeList.add(new SelectItem(null, ""));
			for(Episode elementEpisode : listEpisode) {
				if(elementEpisode.getProduction().getId().equals(productionSelected)) {
					SelectItem xx = new SelectItem(elementEpisode.getId(), elementEpisode.getVirtualName() + " " + elementEpisode.getDateFromOnlyDate());
					episodeList.add(xx);
				}
			}
		}
		
	}

	public IncidentService getIncidentService() {
		return incidentService;
	}

	public void setIncidentService(IncidentService incidentService) {
		this.incidentService = incidentService;
	}

	public LazyDataModel<Incident> getLazyModelIncident() {
		return lazyModelIncident;
	}

	public void setLazyModelIncident(LazyDataModel<Incident> lazyModelIncident) {
		this.lazyModelIncident = lazyModelIncident;
	}

	public Incident getIncidentToEdit() {
		return incidentToEdit;
	}

	public void setIncidentToEdit(Incident incidentToEdit) {
		this.incidentToEdit = incidentToEdit;
	}

	public Long getIncidentToDelete() {
		return incidentToDelete;
	}

	public void setIncidentToDelete(Long incidentToDelete) {
		this.incidentToDelete = incidentToDelete;
	}

	public Long getSelectedProduction() {
		return selectedProduction;
	}

	public void setSelectedProduction(Long selectedProduction) {
		this.selectedProduction = selectedProduction;
	}

	public List<SelectItem> getEpisodeList() {
		return episodeList;
	}

	public void setEpisodeList(List<SelectItem> episodeList) {
		this.episodeList = episodeList;
	}

	public Long getSelectedEpisode() {
		return selectedEpisode;
	}

	public void setSelectedEpisode(Long selectedEpisode) {
		this.selectedEpisode = selectedEpisode;
	}

	public List<SelectItem> getProductionList() {
		return productionList;
	}

	public void setProductionList(List<SelectItem> productionList) {
		this.productionList = productionList;
	}

	public SpectatorService getSpectatorService() {
		return spectatorService;
	}

	public void setSpectatorService(SpectatorService spectatorService) {
		this.spectatorService = spectatorService;
	}

	public List<SelectItem> getSpectatorList() {
		return spectatorList;
	}

	public void setSpectatorList(List<SelectItem> spectatorList) {
		this.spectatorList = spectatorList;
	}

	public ProductionService getProductionService() {
		return productionService;
	}

	public void setProductionService(ProductionService productionService) {
		this.productionService = productionService;
	}

	public EpisodeService getEpisodeService() {
		return episodeService;
	}

	public void setEpisodeService(EpisodeService episodeService) {
		this.episodeService = episodeService;
	}

	public IncidentTypeService getIncidentTypeService() {
		return incidentTypeService;
	}

	public void setIncidentTypeService(IncidentTypeService incidentTypeService) {
		this.incidentTypeService = incidentTypeService;
	}

	public List<SelectItem> getTypeIncidentList() {
		return typeIncidentList;
	}

	public void setTypeIncidentList(List<SelectItem> typeIncidentList) {
		this.typeIncidentList = typeIncidentList;
	}

	public Long getSelectedTypeIncident() {
		return selectedTypeIncident;
	}

	public void setSelectedTypeIncident(Long selectedTypeIncident) {
		this.selectedTypeIncident = selectedTypeIncident;
	}

	public SeverityLevelService getSeverityLevelService() {
		return severityLevelService;
	}

	public void setSeverityLevelService(SeverityLevelService severityLevelService) {
		this.severityLevelService = severityLevelService;
	}

	public List<SelectItem> getSeverityLevelList() {
		return severityLevelList;
	}

	public void setSeverityLevelList(List<SelectItem> severityLevelList) {
		this.severityLevelList = severityLevelList;
	}

	public Long getSelectedSeverityLevel() {
		return selectedSeverityLevel;
	}

	public void setSelectedSeverityLevel(Long selectedSeverityLevel) {
		this.selectedSeverityLevel = selectedSeverityLevel;
	}

	public List<SelectItem> getStudioList() {
		return studioList;
	}

	public void setStudioList(List<SelectItem> studioList) {
		this.studioList = studioList;
	}

	public Long getSelectedStudio() {
		return selectedStudio;
	}

	public void setSelectedStudio(Long selectedStudio) {
		this.selectedStudio = selectedStudio;
	}

	public int getActiveTab() {
		return activeTab;
	}

	public void setActiveTab(int activeTab) {
		this.activeTab = activeTab;
	}

	public Spectator getSpectatorToEdit() {
		return spectatorToEdit;
	}

	public void setSpectatorToEdit(Spectator spectatorToEdit) {
		this.spectatorToEdit = spectatorToEdit;
	}

	public Long getSelectedSpectatorIncident() {
		return selectedSpectatorIncident;
	}

	public void setSelectedSpectatorIncident(Long selectedSpectatorIncident) {
		this.selectedSpectatorIncident = selectedSpectatorIncident;
	}

	public Long getSelectedTypeBlackList() {
		return selectedTypeBlackList;
	}

	public void setSelectedTypeBlackList(Long selectedTypeBlackList) {
		this.selectedTypeBlackList = selectedTypeBlackList;
	}

	public List<SelectItem> getTypeBlackList() {
		return typeBlackList;
	}

	public void setTypeBlackList(List<SelectItem> typeBlackList) {
		this.typeBlackList = typeBlackList;
	}

	public String getNameStudio() {
		return nameStudio;
	}

	public void setNameStudio(String nameStudio) {
		this.nameStudio = nameStudio;
	}

	public boolean haveUserRole(String roles) {
		
		String[] listRoles = roles.split(",");
		for (int i = 0; i < listRoles.length; i++) {
			listRoles[i] = listRoles[i].trim();
			
		}
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication!=null && authentication.getAuthorities()!=null) {
			//String currentPrincipalName = authentication.getName();
			for (GrantedAuthority r : authentication.getAuthorities()) {
				//System.out.println("Ruolo:"+r.getAuthority());
				for (int i = 0; i < listRoles.length; i++) {
					if (r.getAuthority()!=null && r.getAuthority().equals(listRoles[i])) {
						//System.out.println("trovato ruolo !!!");
						return true;
					}					
				}
			}
		}
		return false;
	}	
	
    public List<Spectator> completeSpectator(String query) {   	
    	
    	// Il cognome è criptato quindi il like non funziona!!!!
    	List<Spectator> filteredSpectators = spectatorService.getSpectator();
    	filteredSpectators.removeIf(spectator -> !spectator.getSurname().toLowerCase().contains(query.toLowerCase()));
    	System.out.println("prima ricerca: " + (filteredSpectators!=null?filteredSpectators.size():"0"));
    	filteredSpectators.addAll(spectatorService.getSpectatorFinder().and("name").like(query).setDistinct().list());
    	System.out.println("seconda ricerca: " + (filteredSpectators!=null?filteredSpectators.size():"0"));
    	return filteredSpectators;
    }
}
