package com.crismasecurity.mediasetStudios.web.management;

import java.util.Calendar;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.event.ActionEvent;
import javax.persistence.PersistenceException;

import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;


import com.crismasecurity.mediasetStudios.core.entity.EmployeeType;
import com.crismasecurity.mediasetStudios.core.services.EmployeeTypeService;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;

//@ManagedBean(name= EmployeesBean.BEAN_NAME) // JSF managed
//@SessionScoped // JSF managed
//@ViewScoped // JSF managed

//Spring managed
@Controller (EmployeeTypeBean.BEAN_NAME) 
//@Scope("session")
@Scope("view")
public class EmployeeTypeBean extends BaseBean {

	private static final long serialVersionUID = 1230856500821970204L;

	public static final String BEAN_NAME = "employeeTypeBean";
    public String getBeanName() { return BEAN_NAME; } 


    @Autowired
    private EmployeeTypeService employeeTypeService ;
    
    private LazyDataModel<EmployeeType> lazyModelEmployeeType;
    private Date filterFrom;
	private Date filterTo;
	
	private EmployeeType employeeTypeToEdit = null;
    
	public EmployeeTypeBean() {
		getLog().info("!!!! Costruttore di EmployeeTypeBean !!!");	
    }

	@PostConstruct
	public void init() {
		getLog().info("INIT EmployeeTypeBean!");

		lazyModelEmployeeType = new LazyEmployeeTypeDataModel(employeeTypeService);	
	}

	@PreDestroy
	public void destroy() {
		getLog().info("Destroy EmployeeTypeBean!");

	}
	
	private Long employeeTypeToDelete = null;
	public void confirmDeleteEmployeeType(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("employeeTypeId")) {
    			employeeTypeToDelete = ((Long)((UIParameter)com).getValue());
			}
		}
    	
	}
	
	public void cancelDelete(ActionEvent ev) {
	    	System.out.println("\nDelete EmployeeType canceled.");
	    	employeeTypeToDelete= null;
	}
	
	public void delete(ActionEvent ev) {
    	System.out.println("\nDelete Test ...");
    	try {
        	
    		employeeTypeService.deleteEmployeeType(employeeTypeToDelete);
//    		employees=null;
    		addInfoMessage(getMessagesBoundle().getString("employeeType.deleted"));
		} catch (Exception e) {
			e.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("employeeType.deletingError"), e);
		}
	}
	
	public void addEmployeeType(ActionEvent e) {
		employeeTypeToEdit = new EmployeeType();
	}
	
	public void modifyEmployeeType(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("employeeTypeId")) {
    			employeeTypeToEdit = employeeTypeService.getEmployeeTypeById((Long)((UIParameter)com).getValue());
			}
		}
	}
	
	public void cancelEditEmployeeType(ActionEvent e) {
		employeeTypeToEdit = null;
	}
	
	public void saveEmployeeType(ActionEvent e) {
    	System.out.println("Salvo " + employeeTypeToEdit);
    	

    	if (employeeTypeToEdit.getDescription()==null || employeeTypeToEdit.getDescription().trim().equals("")) {
    		addWarningMessage("test su Descrizione richiesto!");
    		return;
    	}
    	
    	
    	try {
    		employeeTypeToEdit = employeeTypeService.updateEmployeeType(employeeTypeToEdit);
			addInfoMessage(getMessagesBoundle().getString("employeeType.saved"));
    	} catch (PersistenceException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();

    	} catch (DataIntegrityViolationException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();			
		} catch (Exception e1) {
			addErrorMessage(getMessagesBoundle().getString("employeeType.savingError"), e1);
			e1.printStackTrace();
		}
    	employeeTypeToEdit = null;
	}
	
	public void filterByDate() {
		System.out.println("Filter by date !");
		
		Calendar cal = Calendar.getInstance();
		if (filterFrom!=null) {
			cal.setTime(filterFrom);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 1);
			filterFrom = cal.getTime();
		}
		if (filterTo!=null) {
			cal.setTime(filterTo);
			cal.set(Calendar.HOUR_OF_DAY, 23);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);
			filterTo = cal.getTime();
		}
		
		((LazyEmployeeTypeDataModel)lazyModelEmployeeType).setFilterFrom(filterFrom);
		((LazyEmployeeTypeDataModel)lazyModelEmployeeType).setFilterTo(filterTo);
	}
	
	public Object getEmployeeTypeById(long id) {
		return employeeTypeService.getEmployeeTypeById(id);
	}

	public EmployeeType getEmployeeTypeToEdit() {
		return employeeTypeToEdit;
	}

	public void setEmployeeTypeToEdit(EmployeeType empToEdit) {
		this.employeeTypeToEdit = empToEdit;
	}

	public LazyDataModel<EmployeeType> getLazyModelEmployeeType() {
		return lazyModelEmployeeType;
	}

	public Date getFilterFrom() {
		return filterFrom;
	}

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public Date getFilterTo() {
		return filterTo;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}

}
