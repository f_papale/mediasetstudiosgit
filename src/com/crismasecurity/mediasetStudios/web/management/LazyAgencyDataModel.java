package com.crismasecurity.mediasetStudios.web.management;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.crismasecurity.mediasetStudios.core.entity.Agency;
import com.crismasecurity.mediasetStudios.core.services.AgencyService;
 

public class LazyAgencyDataModel extends LazyDataModel<Agency> {




	/**
	 * 
	 */
	private static final long serialVersionUID = 1540078380604196896L;

	private AgencyService agencyService;
	
	private Date filterFrom;
	private Date filterTo;

     
    public LazyAgencyDataModel(AgencyService agencyTypeService) {
        this.agencyService = agencyTypeService;
    }
     
    @Override
    public Agency getRowData(String rowKey) {
    	if (rowKey==null || rowKey.trim().equals("")) return null;
    	return agencyService.getAgencyById(Long.parseLong(rowKey));
    }
 
    @Override
    public Object getRowKey(Agency audit) {
        return audit.getIdAgency();
    }
 
    @Override
    public List<Agency> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
    	
    	// Converto ordinamento
    	int sortInt =0; // UNSORTED
    	if (sortOrder.equals(SortOrder.ASCENDING)) sortInt = 1;
    	if (sortOrder.equals(SortOrder.DESCENDING)) sortInt = 2;
    	
 
    	filters.put("hidden",false);
		if (filterFrom!=null) filters.put("date#ge", filterFrom);
		if (filterTo!=null) filters.put("date#le", filterTo);

    	
    	List<Agency> ret = agencyService.getAgencyLazyLoading(first, pageSize, sortField, sortInt, filters);
    	this.setRowCount((int)agencyService.getAgencyLazyLoadingCount(filters));
//    	System.out.println("Numero elementi trovati: " + this.getRowCount());
//    	for (Audit audit : ret) {
//			System.out.println("Trovato: " + audit);
//		}
    	return ret;    
    }

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}


}