package com.crismasecurity.mediasetStudios.web.management;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.event.ActionEvent;
import javax.persistence.PersistenceException;

import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import com.crismasecurity.mediasetStudios.core.entity.SpectatorType;
import com.crismasecurity.mediasetStudios.core.services.SpectatorTypeService;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;

//Spring managed
@Controller (SpectatorTypeBean.BEAN_NAME) 
//@Scope("session")
@Scope("view")
public class SpectatorTypeBean extends BaseBean{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6963862650218957884L;

	public static final String BEAN_NAME = "spectatorTypeBean";
	public String getBeanName() { return BEAN_NAME; } 

	@Autowired
	private SpectatorTypeService spectatorTypeService;
	
	private LazyDataModel<SpectatorType> lazyModelSpectatorType;
	private SpectatorType spectatorTypeToEdit = null;
	private Long spectatorTypeToDelete = null;
	
	public SpectatorTypeBean() {
		getLog().info("!!!! Costruttore di SpectatorTypeBean !!!");
	}
	
	@PostConstruct
	public void init() {
		getLog().info("INIT SpectatorTypeBean!");
		lazyModelSpectatorType = new LazySpectatorTypeDataModel(spectatorTypeService);
	}
	
	@PreDestroy
	public void destroy() {
		getLog().info("Destroy SpectatorTypeBean!");
	}
	
	public boolean haveUserRole(String roles) {
		
		String[] listRoles = roles.split(",");
		for (int i = 0; i < listRoles.length; i++) {
			listRoles[i] = listRoles[i].trim();
			
		}
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication!=null && authentication.getAuthorities()!=null) {
			//String currentPrincipalName = authentication.getName();
			for (GrantedAuthority r : authentication.getAuthorities()) {
				//System.out.println("Ruolo:"+r.getAuthority());
				for (int i = 0; i < listRoles.length; i++) {
					if (r.getAuthority()!=null && r.getAuthority().equals(listRoles[i])) {
						//System.out.println("trovato ruolo !!!");
						return true;
					}					
				}
			}
		}
		return false;
	}	
	
	public void confirmDeleteSpectatorType(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("spectatorTypeId")) {
    			spectatorTypeToDelete = ((Long)((UIParameter)com).getValue());
			}
		}    	
	}
	
	public void cancelDelete(ActionEvent ev) {
    	System.out.println("\nDelete SpectatorType canceled.");
    	spectatorTypeToDelete= null;
	}
	
	public void delete(ActionEvent ev) {
    	System.out.println("\nDelete Test ...");
    	try {
    		spectatorTypeService.deleteSpectatorType(spectatorTypeToDelete);
    		addInfoMessage(getMessagesBoundle().getString("spectatorType.deleted"));
		} catch (Exception e) {
			e.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("spectatorType.deletingError"), e);
		}
	}
	
	public void addSpectatorType(ActionEvent e) {
		spectatorTypeToEdit = new SpectatorType();
	}
	
	public void modifySpectatorType(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("spectatorTypeId")) {
    			spectatorTypeToEdit = spectatorTypeService.getSpectatorTypeById((Long)((UIParameter)com).getValue());
			}
		}
	}
	
	public void cancelEditSpectatorType(ActionEvent e) {
		spectatorTypeToEdit = null;
	}
	
	public void saveSpectatorType(ActionEvent e) {
    	System.out.println("Salvo " + spectatorTypeToEdit);
    	

    	if (spectatorTypeToEdit.getDescription()==null || spectatorTypeToEdit.getDescription().trim().equals("")) {
    		addWarningMessage("test su Descrizione richiesto!");
    		return;
    	}
    	
    	
    	try {
    		spectatorTypeToEdit = spectatorTypeService.updateSpectatorType(spectatorTypeToEdit);
		
			addInfoMessage(getMessagesBoundle().getString("spectatorType.saved"));
    	} catch (PersistenceException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();

    	} catch (DataIntegrityViolationException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();
    				
		} catch (Exception e1) {
			addErrorMessage(getMessagesBoundle().getString("spectatorType.savingError"), e1);
			e1.printStackTrace();
		}
    	spectatorTypeToEdit = null;
	}
	
	public Object getSpectatorTypeById(long id) {
		return spectatorTypeService.getSpectatorTypeById(id);
	}

	public SpectatorType getSpectatorTypeToEdit() {
		return spectatorTypeToEdit;
	}

	public void setSpectatorTypeToEdit(SpectatorType empToEdit) {
		this.spectatorTypeToEdit = empToEdit;
	}

	public LazyDataModel<SpectatorType> getLazyModelSpectatorType() {
		return lazyModelSpectatorType;
	}
	
}
