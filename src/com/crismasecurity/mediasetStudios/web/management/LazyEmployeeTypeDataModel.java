package com.crismasecurity.mediasetStudios.web.management;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.crismasecurity.mediasetStudios.core.entity.EmployeeType;
import com.crismasecurity.mediasetStudios.core.services.EmployeeTypeService;
 

public class LazyEmployeeTypeDataModel extends LazyDataModel<EmployeeType> {

	private static final long serialVersionUID = -4653671320827287068L;

	private EmployeeTypeService employeeTypeService;
	
	private Date filterFrom;
	private Date filterTo;

     
    public LazyEmployeeTypeDataModel(EmployeeTypeService employeeTypeService) {
        this.employeeTypeService = employeeTypeService;
    }
     
    @Override
    public EmployeeType getRowData(String rowKey) {
    	if (rowKey==null || rowKey.trim().equals("")) return null;
    	return employeeTypeService.getEmployeeTypeById(Long.parseLong(rowKey));
    }
 
    @Override
    public Object getRowKey(EmployeeType audit) {
        return audit.getId();
    }
 
    @Override
    public List<EmployeeType> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
    	
    	// Converto ordinamento
    	int sortInt =0; // UNSORTED
    	if (sortOrder.equals(SortOrder.ASCENDING)) sortInt = 1;
    	if (sortOrder.equals(SortOrder.DESCENDING)) sortInt = 2;
    	
 
		if (filterFrom!=null) filters.put("date#ge", filterFrom);
		if (filterTo!=null) filters.put("date#le", filterTo);

    	
    	List<EmployeeType> ret = employeeTypeService.getEmployeeTypeLazyLoading(first, pageSize, sortField, sortInt, filters);
    	this.setRowCount((int)employeeTypeService.getEmployeeTypeLazyLoadingCount(filters));
//    	System.out.println("Numero elementi trovati: " + this.getRowCount());
//    	for (Audit audit : ret) {
//			System.out.println("Trovato: " + audit);
//		}
    	return ret;    
    }

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}


}