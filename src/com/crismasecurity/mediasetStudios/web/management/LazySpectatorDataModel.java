package com.crismasecurity.mediasetStudios.web.management;

import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.crismasecurity.mediasetStudios.core.entity.Spectator;
import com.crismasecurity.mediasetStudios.core.services.SpectatorService;

public class LazySpectatorDataModel extends LazyDataModel<Spectator>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6305865097946016528L;
	
	private SpectatorService spectatorService; 
	
	
	public LazySpectatorDataModel(SpectatorService spectatorService) {
		this.spectatorService = spectatorService;
	}
	
	@Override
	public Spectator getRowData(String rowKey) {
    	if (rowKey==null || rowKey.trim().equals("")) return null;
    	return spectatorService.getSpectatorById(Long.parseLong(rowKey));
	}
	
	@Override
	public Object getRowKey(Spectator sp) {
		return sp.getId();
	}
	
	@Override
	public List<Spectator> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters){
    	// Converto ordinamento
    	int sortInt =0; // UNSORTED
    	if (sortOrder.equals(SortOrder.ASCENDING)) sortInt = 1;
    	if (sortOrder.equals(SortOrder.DESCENDING)) sortInt = 2;
    	
    	List<Spectator> listsp = spectatorService.getSpectatorLazyLoading(first, pageSize, sortField, sortInt, filters);
    	this.setRowCount((int)spectatorService.getSpectatorLazyLoadingCount(filters));
//    	System.out.println("Numero elementi trovati: " + this.getRowCount());
    	
    	return listsp;
	}

}

