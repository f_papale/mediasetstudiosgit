package com.crismasecurity.mediasetStudios.web.management;

import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.crismasecurity.mediasetStudios.core.entity.BlackListType;
import com.crismasecurity.mediasetStudios.core.services.BlackListTypeService;

public class LazyBlackListTypeDataModel extends LazyDataModel<BlackListType>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2960910410611093828L;
	
	private BlackListTypeService blackListTypeService;
	
	public LazyBlackListTypeDataModel(BlackListTypeService blackListTypeService) {
		this.blackListTypeService = blackListTypeService;
	}
	
	@Override
	public BlackListType getRowData(String rowKey) {
		if (rowKey==null || rowKey.trim().equals("")) return null;
		return blackListTypeService.getBlackListTypeById(Long.parseLong(rowKey));
	}
	
    @Override
    public Object getRowKey(BlackListType blackListType) {
    	return blackListType.getId();
    }
    
    @Override 
    public List<BlackListType> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters){
    	// Converto ordinamento
    	int sortInt = 0; // UNSORTED
    	if (sortOrder.equals(SortOrder.ASCENDING)) sortInt = 1;
    	if (sortOrder.equals(SortOrder.DESCENDING)) sortInt = 2;
    	
    	List<BlackListType> sptypelist = blackListTypeService.getBlackListTypeLazyLoading(first, pageSize, sortField, sortInt, filters);
    	this.setRowCount((int)blackListTypeService.getBlackListLoadingCount(filters));
//    	System.out.println("Numero elementi trovati: " + this.getRowCount());    	
    	return sptypelist;
    	
    }

}
