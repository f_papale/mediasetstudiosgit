package com.crismasecurity.mediasetStudios.web.management;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.event.ActionEvent;
import javax.persistence.PersistenceException;

import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;


import com.crismasecurity.mediasetStudios.core.entity.EmailQueue;
import com.crismasecurity.mediasetStudios.core.entity.Episode;
import com.crismasecurity.mediasetStudios.core.entity.EpisodeStatusType;
import com.crismasecurity.mediasetStudios.core.services.EmailQueueService;
import com.crismasecurity.mediasetStudios.core.services.EpisodeService;
import com.crismasecurity.mediasetStudios.core.services.EpisodeStatusTypeService;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;

//@ManagedBean(name= EmployeesBean.BEAN_NAME) // JSF managed
//@SessionScoped // JSF managed
//@ViewScoped // JSF managed

//Spring managed
@Controller (EmailQueueBean.BEAN_NAME) 
//@Scope("session")
@Scope("view")
public class EmailQueueBean extends BaseBean {

	private static final long serialVersionUID = 1230856500821970204L;

	public static final String BEAN_NAME = "emailQueueBean";
    public String getBeanName() { return BEAN_NAME; } 


    @Autowired
    private EmailQueueService emailQueueService ;
    
    @Autowired
    private EpisodeService episodeService ;
    
    @Autowired
    private EpisodeStatusTypeService episodeStatusTypeService ;
    
    private LazyDataModel<EmailQueue> lazyModelEmailQueue;
    private Date filterFrom;
	private Date filterTo;
	
	private EmailQueue emailQueueToEdit = null;
    
	public EmailQueueBean() {
		getLog().info("!!!! Costruttore di EmailQueueBean !!!");	
    }

	@PostConstruct
	public void init() {
		getLog().info("INIT EmailQueueBean!");

		lazyModelEmailQueue = new LazyEmailQueueDataModel(emailQueueService);	
	}

	@PreDestroy
	public void destroy() {
		getLog().info("Destroy EmailQueueBean!");

	}
	
	private Long emailQueueToDelete = null;
	public void confirmDeleteEmailQueue(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("emailQueueId")) {
    			emailQueueToDelete = ((Long)((UIParameter)com).getValue());
			}
		}
    	
	}
	
	public void cancelDelete(ActionEvent ev) {
	    	System.out.println("\nDelete EmailQueue canceled.");
	    	emailQueueToDelete= null;
	}
	
	public void delete(ActionEvent ev) {
    	System.out.println("\nDelete Test ...");
    	try {
        	
    		emailQueueService.deleteEmailQueue(emailQueueToDelete);
//    		employees=null;
    		addInfoMessage(getMessagesBoundle().getString("emailQueue.deleted"));
		} catch (Exception e) {
			e.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("emailQueue.deletingError"), e);
		}
	}

	public void purgeEmailQueue() {
	
		List<Integer> lstStates = Arrays.asList(new Integer[] {6,7,8});
		List<Long> lst=null;
		EpisodeStatusType nextStatus=null;
		
		try {
			//Preleva la lista degli IDEpisodi e elimina tutte le emailInt indistintamente
			lst=purgeEmailQueueAndGetEntityID(1,true);
			for (Long episodeID : lst) {
				Episode epis = episodeService.getEpisodeById(episodeID);
				if (lstStates.contains(epis.getEpisodeStatus().getIdStatus())){
					nextStatus =new EpisodeStatusType();
					nextStatus=episodeStatusTypeService.getEpisodeStatusTypeByIdStatus(4);
					epis.setEpisodeStatus(nextStatus);
					episodeService.updateEpisode(epis);
				}
			}
			addInfoMessage(getMessagesBoundle().getString("emailQueue.purged"));
		} catch (Exception e) {
			addErrorMessage(getMessagesBoundle().getString("emailQueue.purgeError"));
		}
	}
	
	private List<Long> purgeEmailQueueAndGetEntityID(int entityType, boolean doPurge) {
		
		List<Long> lst = new ArrayList<Long>();
		
		try {
			List<EmailQueue> emails = emailQueueService.getEmailQueue();
			for (EmailQueue email: emails) {
				if (email.getEntityType()==entityType) lst.add(email.getEntityID());
				if (doPurge) emailQueueService.deleteEmailQueue(email.getId());
			}
			addInfoMessage(getMessagesBoundle().getString("emailQueue.purged"));
		} catch (Exception e) {
			lst.clear();
			addErrorMessage(getMessagesBoundle().getString("emailQueue.purgeError"));
		}
		return lst;
	}
	
	public void addEmailQueue(ActionEvent e) {
		emailQueueToEdit = new EmailQueue();
		emailQueueToEdit.setSendStatus(1);
	}
	
	public void modifyEmailQueue(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("emailQueueId")) {
    			emailQueueToEdit = emailQueueService.getEmailQueueById((Long)((UIParameter)com).getValue());
			}
		}
	}
	
	public void cancelEditEmailQueue(ActionEvent e) {
		emailQueueToEdit = null;
	}
	
	public void saveEmailQueue(ActionEvent e) {
    	System.out.println("Salvo " + emailQueueToEdit);
    	

    	if (emailQueueToEdit.getBody()==null || emailQueueToEdit.getBody().trim().equals("")) {
    		addWarningMessage("test su Body richiesto!");
    		return;
    	}
    	try {
    		emailQueueToEdit = emailQueueService.updateEmailQueue(emailQueueToEdit);
			addInfoMessage(getMessagesBoundle().getString("emailQueue.saved"));
    	} catch (PersistenceException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();

    	} catch (DataIntegrityViolationException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();
    				
		} catch (Exception e1) {
			addErrorMessage(getMessagesBoundle().getString("emailQueue.savingError"), e1);
			e1.printStackTrace();
		}
    	emailQueueToEdit = null;
	}
	
	public void filterByDate() {
		System.out.println("Filter by date !");
		
		Calendar cal = Calendar.getInstance();
		if (filterFrom!=null) {
			cal.setTime(filterFrom);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 1);
			filterFrom = cal.getTime();
		}
		if (filterTo!=null) {
			cal.setTime(filterTo);
			cal.set(Calendar.HOUR_OF_DAY, 23);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);
			filterTo = cal.getTime();
		}
		
		((LazyEmailQueueDataModel)lazyModelEmailQueue).setFilterFrom(filterFrom);
		((LazyEmailQueueDataModel)lazyModelEmailQueue).setFilterTo(filterTo);
	}
	
	public Object getEmailQueueById(long id) {
		return emailQueueService.getEmailQueueById(id);
	}

	public EmailQueue getEmailQueueToEdit() {
		return emailQueueToEdit;
	}

	public void setEmailQueueToEdit(EmailQueue empToEdit) {
		this.emailQueueToEdit = empToEdit;
	}

	public LazyDataModel<EmailQueue> getLazyModelEmailQueue() {
		return lazyModelEmailQueue;
	}

	public Date getFilterFrom() {
		return filterFrom;
	}

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public Date getFilterTo() {
		return filterTo;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}

}
