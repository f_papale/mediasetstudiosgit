package com.crismasecurity.mediasetStudios.web.management;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.crismasecurity.mediasetStudios.core.entity.ProductionCenter;
import com.crismasecurity.mediasetStudios.core.services.ProductionCenterService;
 

public class LazyProductionCenterDataModel extends LazyDataModel<ProductionCenter> {

	private static final long serialVersionUID = -8511458584935016385L;

	private ProductionCenterService productionCenterService;
	
	private Date filterFrom;
	private Date filterTo;

     
    public LazyProductionCenterDataModel(ProductionCenterService productionCenterService) {
        this.productionCenterService = productionCenterService;
    }
     
    @Override
    public ProductionCenter getRowData(String rowKey) {
    	if (rowKey==null || rowKey.trim().equals("")) return null;
    	return productionCenterService.getProductionCenterById(Long.parseLong(rowKey));
    }
 
    @Override
    public Object getRowKey(ProductionCenter audit) {
        return audit.getId();
    }
 
    @Override
    public List<ProductionCenter> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
    	
    	// Converto ordinamento
    	int sortInt =0; // UNSORTED
    	if (sortOrder.equals(SortOrder.ASCENDING)) sortInt = 1;
    	if (sortOrder.equals(SortOrder.DESCENDING)) sortInt = 2;
    	
 
		if (filterFrom!=null) filters.put("date#ge", filterFrom);
		if (filterTo!=null) filters.put("date#le", filterTo);

    	
    	List<ProductionCenter> ret = productionCenterService.getProductionCenterLazyLoading(first, pageSize, sortField, sortInt, filters);
    	this.setRowCount((int)productionCenterService.getProductionCenterLazyLoadingCount(filters));
//    	System.out.println("Numero elementi trovati: " + this.getRowCount());

    	return ret;    
    }

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}


}