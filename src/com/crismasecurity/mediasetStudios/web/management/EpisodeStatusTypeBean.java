package com.crismasecurity.mediasetStudios.web.management;

import java.util.Calendar;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.event.ActionEvent;
import javax.persistence.PersistenceException;

import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;


import com.crismasecurity.mediasetStudios.core.entity.EpisodeStatusType;
import com.crismasecurity.mediasetStudios.core.services.EpisodeStatusTypeService;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;

//@ManagedBean(name= EmployeesBean.BEAN_NAME) // JSF managed
//@SessionScoped // JSF managed
//@ViewScoped // JSF managed

//Spring managed
@Controller (EpisodeStatusTypeBean.BEAN_NAME) 
//@Scope("session")
@Scope("view")
public class EpisodeStatusTypeBean extends BaseBean {

	private static final long serialVersionUID = -6065141989753335782L;

	public static final String BEAN_NAME = "episodeStatusTypeBean";
    public String getBeanName() { return BEAN_NAME; } 


    @Autowired
    private EpisodeStatusTypeService episodeStatusTypeService ;
    
    private LazyDataModel<EpisodeStatusType> lazyModelEpisodeStatusType;
    private Date filterFrom;
	private Date filterTo;
	
	private EpisodeStatusType episodeStatusTypeToEdit = null;
    
	public EpisodeStatusTypeBean() {
		getLog().info("!!!! Costruttore di EpisodeStatusTypeBean !!!");	
    }

	@PostConstruct
	public void init() {
		getLog().info("INIT EpisodeStatusTypeBean!");

		lazyModelEpisodeStatusType = new LazyEpisodeStatusTypeDataModel(episodeStatusTypeService);	
	}

	@PreDestroy
	public void destroy() {
		getLog().info("Destroy EpisodeStatusTypeBean!");

	}
	
	private Long episodeStatusTypeToDelete = null;
	public void confirmDeleteEpisodeStatusType(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("episodeStatusTypeId")) {
    			episodeStatusTypeToDelete = ((Long)((UIParameter)com).getValue());
			}
		}
    	
	}
	
	public void cancelDelete(ActionEvent ev) {
	    	System.out.println("\nDelete EpisodeStatusType canceled.");
	    	episodeStatusTypeToDelete= null;
	}
	
	public void delete(ActionEvent ev) {
    	System.out.println("\nDelete Test ...");
    	try {
        	
    		episodeStatusTypeService.deleteEpisodeStatusType(episodeStatusTypeToDelete);
//    		employees=null;
    		addInfoMessage(getMessagesBoundle().getString("episodeStatusType.deleted"));
		} catch (Exception e) {
			e.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("episodeStatusType.deletingError"), e);
		}
	}
	
	public void addEpisodeStatusType(ActionEvent e) {
		episodeStatusTypeToEdit = new EpisodeStatusType();
	}
	
	public void modifyEpisodeStatusType(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("episodeStatusTypeId")) {
    			episodeStatusTypeToEdit = episodeStatusTypeService.getEpisodeStatusTypeById((Long)((UIParameter)com).getValue());
			}
		}
	}
	
	public void cancelEditEpisodeStatusType(ActionEvent e) {
		episodeStatusTypeToEdit = null;
	}
	
	public void saveEpisodeStatusType(ActionEvent e) {
    	System.out.println("Salvo " + episodeStatusTypeToEdit);
    	

    	if (episodeStatusTypeToEdit.getDescription()==null || episodeStatusTypeToEdit.getDescription().trim().equals("")) {
    		addWarningMessage("test su Descrizione richiesto!");
    		return;
    	}
    	
    	
    	try {
    		episodeStatusTypeToEdit = episodeStatusTypeService.updateEpisodeStatusType(episodeStatusTypeToEdit);
			addInfoMessage(getMessagesBoundle().getString("episodeStatusType.saved"));
    	} catch (PersistenceException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();
    	} catch (DataIntegrityViolationException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();
    				
		} catch (Exception e1) {
			addErrorMessage(getMessagesBoundle().getString("episodeStatusType.savingError"), e1);
			e1.printStackTrace();
		}
    	episodeStatusTypeToEdit = null;
	}
	
	public boolean haveUserRole(String roles) {
		
		String[] listRoles = roles.split(",");
		for (int i = 0; i < listRoles.length; i++) {
			listRoles[i] = listRoles[i].trim();
			
		}
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication!=null && authentication.getAuthorities()!=null) {
			//String currentPrincipalName = authentication.getName();
			for (GrantedAuthority r : authentication.getAuthorities()) {
				//System.out.println("Ruolo:"+r.getAuthority());
				for (int i = 0; i < listRoles.length; i++) {
					if (r.getAuthority()!=null && r.getAuthority().equals(listRoles[i])) {
						//System.out.println("trovato ruolo !!!");
						return true;
					}					
				}
			}
		}
		return false;
	}
	
	public void filterByDate() {
		System.out.println("Filter by date !");
		
		Calendar cal = Calendar.getInstance();
		if (filterFrom!=null) {
			cal.setTime(filterFrom);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 1);
			filterFrom = cal.getTime();
		}
		if (filterTo!=null) {
			cal.setTime(filterTo);
			cal.set(Calendar.HOUR_OF_DAY, 23);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);
			filterTo = cal.getTime();
		}
		
		((LazyEpisodeStatusTypeDataModel)lazyModelEpisodeStatusType).setFilterFrom(filterFrom);
		((LazyEpisodeStatusTypeDataModel)lazyModelEpisodeStatusType).setFilterTo(filterTo);
	}
	
	public Object getEpisodeStatusTypeById(long id) {
		return episodeStatusTypeService.getEpisodeStatusTypeById(id);
	}

	public EpisodeStatusType getEpisodeStatusTypeToEdit() {
		return episodeStatusTypeToEdit;
	}

	public void setEpisodeStatusTypeToEdit(EpisodeStatusType empToEdit) {
		this.episodeStatusTypeToEdit = empToEdit;
	}

	public LazyDataModel<EpisodeStatusType> getLazyModelEpisodeStatusType() {
		return lazyModelEpisodeStatusType;
	}

	public Date getFilterFrom() {
		return filterFrom;
	}

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public Date getFilterTo() {
		return filterTo;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}

}
