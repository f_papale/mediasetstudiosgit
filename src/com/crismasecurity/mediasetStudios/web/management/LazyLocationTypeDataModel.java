package com.crismasecurity.mediasetStudios.web.management;

import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.crismasecurity.mediasetStudios.core.entity.LocationType;
import com.crismasecurity.mediasetStudios.core.services.LocationTypeService;

public class LazyLocationTypeDataModel extends LazyDataModel<LocationType>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2960910410611093828L;
	
	private LocationTypeService locationTypeService;
	
	public LazyLocationTypeDataModel(LocationTypeService locTypeservice) {
		this.locationTypeService = locTypeservice;
	}
	
	@Override
	public LocationType getRowData(String rowKey) {
		if (rowKey==null || rowKey.trim().equals("")) return null;
		return locationTypeService.getLocationTypeById(Long.parseLong(rowKey));
	}
	
    @Override
    public Object getRowKey(LocationType lcType) {
    	return lcType.getId();
    }
    
    @Override 
    public List<LocationType> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters){
    	// Converto ordinamento
    	int sortInt = 0; // UNSORTED
    	if (sortOrder.equals(SortOrder.ASCENDING)) sortInt = 1;
    	if (sortOrder.equals(SortOrder.DESCENDING)) sortInt = 2;
    	
    	List<LocationType> sptypelist = locationTypeService.getLocationTypeLazyLoading(first, pageSize, sortField, sortInt, filters);
    	this.setRowCount((int)locationTypeService.getLocationTypeLoadingCount(filters));
//    	System.out.println("Numero elementi trovati: " + this.getRowCount());    	
    	return sptypelist;
    	
    }

}
