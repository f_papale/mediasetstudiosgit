package com.crismasecurity.mediasetStudios.web.management;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.event.ActionEvent;
import javax.persistence.PersistenceException;

import org.primefaces.model.DualListModel;
import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import com.crismasecurity.mediasetStudios.core.entity.DistributionEmail;
import com.crismasecurity.mediasetStudios.core.entity.DistributionEmailId;
import com.crismasecurity.mediasetStudios.core.entity.DistributionList;
import com.crismasecurity.mediasetStudios.core.entity.EmailContact;
import com.crismasecurity.mediasetStudios.core.services.DistributionEmailService;
import com.crismasecurity.mediasetStudios.core.services.DistributionListService;
import com.crismasecurity.mediasetStudios.core.services.EmailContactService;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;

//@ManagedBean(name= EmployeesBean.BEAN_NAME) // JSF managed
//@SessionScoped // JSF managed
//@ViewScoped // JSF managed

//Spring managed
@Controller (EmailContactBean.BEAN_NAME) 
//@Scope("session")
@Scope("view")
public class EmailContactBean extends BaseBean {
	
	private static final long serialVersionUID = -240547605225603471L;
	public static final String BEAN_NAME = "emailContactBean";
    public String getBeanName() { return BEAN_NAME; } 

    private List<DistributionList> sourceDistributionList;
    private List<DistributionList> targetDistributionList;
    
    private DualListModel<DistributionList> distributionListes;
    
    @Autowired
    private DistributionListService distributionListService;
    
    @Autowired
    private DistributionEmailService distributionEmailService;
    
    @Autowired
    private EmailContactService emailContactService;
    
    private LazyDataModel<EmailContact> lazyModelEmailContact;
	
	private EmailContact emailContactToEdit = null;
	private Long selectedEmailContact =null;
	
	private Long emailContactToDelete = null;
	
	
	public EmailContactBean() {
		getLog().info("!!!! Costruttore di EmailContactBean !!!");	
    }

	@PostConstruct
	public void init() {
		getLog().info("INIT EmailContactBean!");
		setLazyModelEmailContact(new LazyEmailContactDataModel(emailContactService));			
	}

	@PreDestroy
	public void destroy() {
		getLog().info("Destroy EmailContactBean!");

	}
	
	private void presetDistributionList(Long selectedEmailContact) {
		targetDistributionList = new ArrayList<DistributionList>();
		distributionEmailService.getDistributionEmailFinder().and("emailContact.id").eq(selectedEmailContact).list().forEach(x -> targetDistributionList.add(x.getDistributionList()));
		sourceDistributionList=distributionListService.getDistributionList();
		
		sourceDistributionList.removeAll(targetDistributionList);

		setDistributionListes(new DualListModel<DistributionList>(sourceDistributionList, targetDistributionList));
	}
	

	public void confirmDeleteEmailContact(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("emailContactId")) {
    			emailContactToDelete = ((Long)((UIParameter)com).getValue());
			}
		}
	}
	
	public void cancelDelete(ActionEvent ev) {
	    	System.out.println("\nDelete DistributionList canceled.");
	    	emailContactToDelete= null;
	}
	
	public void delete(ActionEvent ev) {
    	System.out.println("\nDelete Test ...");
    	try {
        	
    		emailContactService.deleteEmailContact(emailContactToDelete);
    		addInfoMessage(getMessagesBoundle().getString("emailContact.deleted"));
		} catch (Exception e) {
			e.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("emailContact.deletingError"), e);
		}
	}
	
	public void addEmailContact(ActionEvent e) {
		setEmailContactToEdit(new EmailContact());
		selectedEmailContact=null;
		presetDistributionList(selectedEmailContact);
		
	}
	
	public void modifyEmailContact(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("emailContactId")) {
    			setEmailContactToEdit(emailContactService.getEmailContactById((Long)((UIParameter)com).getValue()));
    			selectedEmailContact=getEmailContactToEdit().getId();
    			presetDistributionList(selectedEmailContact);    			
			}
		}
	}
	
	
	public DistributionList getDistributionListById(Long id) {
		
		return distributionListService.getDistributionListById(id);
	}
	
	
	public boolean haveUserRole(String roles) {
		
		String[] listRoles = roles.split(",");
		for (int i = 0; i < listRoles.length; i++) {
			listRoles[i] = listRoles[i].trim();
			
		}
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication!=null && authentication.getAuthorities()!=null) {
			//String currentPrincipalName = authentication.getName();
			for (GrantedAuthority r : authentication.getAuthorities()) {
				//System.out.println("Ruolo:"+r.getAuthority());
				for (int i = 0; i < listRoles.length; i++) {
					if (r.getAuthority()!=null && r.getAuthority().equals(listRoles[i])) {
						//System.out.println("trovato ruolo !!!");
						return true;
					}					
				}
			}
		}
		return false;
	}	
		
	
	public void cancelEditEmailContact(ActionEvent e) {
		setEmailContactToEdit(null);
	}
	
	public void saveEmailContact(ActionEvent e) {
    	System.out.println("Salvo " + getEmailContactToEdit());
    	

    	if (getEmailContactToEdit().getName()==null || getEmailContactToEdit().getName().trim().equals("")) {
    		addWarningMessage("Nome richiesto!");
    		return;
    	}
    	
    	if (getEmailContactToEdit().getSurname()==null || getEmailContactToEdit().getSurname().trim().equals("")) {
    		addWarningMessage("Cognome richiesto!");
    		return;
    	}
    	
    	if (getEmailContactToEdit().getManagerEmail()==null || getEmailContactToEdit().getManagerEmail().trim().equals("")) {
    		addWarningMessage("Email richiesta!");
    		return;
    	}
    	   	
    	
    	try {
    		if (getEmailContactToEdit().getId()!=null) {
    			
    			distributionEmailService.deleteDistributionEmailByIdEC(getEmailContactToEdit().getId());
    			setEmailContactToEdit(emailContactService.updateEmailContact(getEmailContactToEdit()));
    		} else {
    			setEmailContactToEdit(emailContactService.addEmailContact(getEmailContactToEdit()));
    		}
    		
    		for (DistributionList distributionListItem : getDistributionListes().getTarget()) {
    			DistributionEmail distributionEmail = new DistributionEmail();
    			distributionEmail.setDistributionList(distributionListItem);
    			distributionEmail.setEmailContact(getEmailContactToEdit());
    			DistributionEmailId id = new DistributionEmailId(distributionListItem.getId(),getEmailContactToEdit().getId());
    			distributionEmail.setId(id);
    			distributionEmail=distributionEmailService.updateDistributionEmail(distributionEmail);
			}
    		setEmailContactToEdit(emailContactService.getEmailContactById(getEmailContactToEdit().getId()));
    		
			addInfoMessage(getMessagesBoundle().getString("distributionList.saved"));
    	} catch (PersistenceException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();

    	} catch (DataIntegrityViolationException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();
    		    		
		} catch (Exception e1) {
			addErrorMessage(getMessagesBoundle().getString("distributionList.savingError"), e1);
			e1.printStackTrace();
		}
    	setEmailContactToEdit(null);
	}

	public DualListModel<DistributionList> getDistributionListes() {
		return distributionListes;
	}

	public void setDistributionListes(DualListModel<DistributionList> distributionListes) {
		this.distributionListes = distributionListes;
	}

	public EmailContact getEmailContactToEdit() {
		return emailContactToEdit;
	}

	public void setEmailContactToEdit(EmailContact emailContactToEdit) {
		this.emailContactToEdit = emailContactToEdit;
	}

	public LazyDataModel<EmailContact> getLazyModelEmailContact() {
		return lazyModelEmailContact;
	}

	public void setLazyModelEmailContact(LazyDataModel<EmailContact> lazyModelEmailContact) {
		this.lazyModelEmailContact = lazyModelEmailContact;
	}

}
