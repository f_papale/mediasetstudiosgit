package com.crismasecurity.mediasetStudios.web.management;

import java.util.Date;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.event.ActionEvent;
import javax.persistence.PersistenceException;

import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;


import com.crismasecurity.mediasetStudios.core.entity.Province;
import com.crismasecurity.mediasetStudios.core.services.ProvinceService;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;

@Controller (ProvinceBean.BEAN_NAME) 
@Scope("view")
public class ProvinceBean extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5064288399867195273L;

	public static final String BEAN_NAME = "provinceBean";
    public String getBeanName() { return BEAN_NAME; } 


    @Autowired
    private ProvinceService provinceService ;
    
    private LazyDataModel<Province> lazyModelProvince;
    private Date filterFrom;
	private Date filterTo;
	
	private Province provinceToEdit = null;
    
	public ProvinceBean() {
		getLog().info("!!!! Costruttore di ProvinceBean !!!");	
    }

	@PostConstruct
	public void init() {
		getLog().info("INIT ProvinceBean!");
        if(provinceService.getProvince().isEmpty() || provinceService.getProvince() == null) {
        	lazyModelProvince = null;
        }else {lazyModelProvince = new LazyProvinceDataModel(provinceService);	}
	}

	@PreDestroy
	public void destroy() {
		getLog().info("Destroy ProvinceBean!");

	}
	
	private Long provinceToDelete = null;
	public void confirmDeleteProvince(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("provinceId")) {
    			provinceToDelete = ((Long)((UIParameter)com).getValue());
			}
		}
    	
	}
	
	public void cancelDelete(ActionEvent ev) {
	    	System.out.println("\nDelete Province canceled.");
	    	provinceToDelete= null;
	}
	
	public void delete(ActionEvent ev) {
    	System.out.println("\nDelete Test ...");
    	try {
        	
    		provinceService.deleteProvince(provinceToDelete);
//    		employees=null;
    		addInfoMessage(getMessagesBoundle().getString("province.deleted"));
		} catch (Exception e) {
			e.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("province.deletingError"), e);
		}
	}
	
	public void addProvince(ActionEvent e) {
		provinceToEdit = new Province();
	}
	
	public void modifyProvince(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("provinceId")) {
    			provinceToEdit = provinceService.getProvinceById((Long)((UIParameter)com).getValue());
			}
		}
	}
	
	public void cancelEditProvince(ActionEvent e) {
		provinceToEdit = null;
	}
	
	public void saveProvince(ActionEvent e) {
    	System.out.println("Salvo " + provinceToEdit);
    	

    	if (provinceToEdit.getCodProv()==null || provinceToEdit.getCodProv().trim().equals("")) {
    		addWarningMessage("test su Descrizione richiesto!");
    		return;
    	}
    	
    	
    	try {
    		provinceToEdit = provinceService.updateProvince(provinceToEdit);
		
			addInfoMessage(getMessagesBoundle().getString("province.saved"));
    	} catch (PersistenceException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();

    	} catch (DataIntegrityViolationException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();
    				
		} catch (Exception e1) {
			addErrorMessage(getMessagesBoundle().getString("province.savingError"), e1);
			e1.printStackTrace();
		}
    	provinceToEdit = null;
	}
	
	public Object getProvinceById(long id) {
		return provinceService.getProvinceById(id);
	}

	public Province getProvinceToEdit() {
		return provinceToEdit;
	}

	public void setProvinceToEdit(Province empToEdit) {
		this.provinceToEdit = empToEdit;
	}

	public LazyDataModel<Province> getLazyModelProvince() {
		return lazyModelProvince;
	}

	/*public Date getFilterFrom() {
		return filterFrom;
	}

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public Date getFilterTo() {
		return filterTo;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}*/

}
