package com.crismasecurity.mediasetStudios.web.management;

import java.util.Calendar;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.event.ActionEvent;
import javax.persistence.PersistenceException;

import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;


import com.crismasecurity.mediasetStudios.core.entity.SeverityLevel;
import com.crismasecurity.mediasetStudios.core.services.SeverityLevelService;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;

//@ManagedBean(name= EmployeesBean.BEAN_NAME) // JSF managed
//@SessionScoped // JSF managed
//@ViewScoped // JSF managed

//Spring managed
@Controller (SeverityLevelBean.BEAN_NAME) 
//@Scope("session")
@Scope("view")
public class SeverityLevelBean extends BaseBean {
	/**
	 * 
	 */
	private static final long serialVersionUID = -467802021509601839L;

	public static final String BEAN_NAME = "severityLevelBean";
    public String getBeanName() { return BEAN_NAME; } 


    @Autowired
    private SeverityLevelService severityLevelService ;
    
    private LazyDataModel<SeverityLevel> lazyModelSeverityLevel;
    private Date filterFrom;
	private Date filterTo;
	
	private SeverityLevel severityLevelToEdit = null;
    
	public SeverityLevelBean() {
		getLog().info("!!!! Costruttore di SeverityLevelBean !!!");	
    }

	@PostConstruct
	public void init() {
		getLog().info("INIT SeverityLevelBean!");
		if(severityLevelService.getSeverityLevel().isEmpty() || severityLevelService.getSeverityLevel() == null) {
			lazyModelSeverityLevel = null;
		} else {	lazyModelSeverityLevel = new LazySeverityLevelDataModel(severityLevelService);	}
	}

	@PreDestroy
	public void destroy() {
		getLog().info("Destroy SeverityLevelBean!");
	}
	
	public boolean haveUserRole(String roles) {
		
		String[] listRoles = roles.split(",");
		for (int i = 0; i < listRoles.length; i++) {
			listRoles[i] = listRoles[i].trim();
			
		}
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication!=null && authentication.getAuthorities()!=null) {
			//String currentPrincipalName = authentication.getName();
			for (GrantedAuthority r : authentication.getAuthorities()) {
				//System.out.println("Ruolo:"+r.getAuthority());
				for (int i = 0; i < listRoles.length; i++) {
					if (r.getAuthority()!=null && r.getAuthority().equals(listRoles[i])) {
						//System.out.println("trovato ruolo !!!");
						return true;
					}					
				}
			}
		}
		return false;
	}		
	
	private Long severityLevelToDelete = null;
	public void confirmDeleteSeverityLevel(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("severityLevelId")) {
    			severityLevelToDelete = ((Long)((UIParameter)com).getValue());
			}
		}
    	
	}
	
	public void cancelDelete(ActionEvent ev) {
	    	System.out.println("\nDelete SeverityLevel canceled.");
	    	severityLevelToDelete= null;
	}
	
	public void delete(ActionEvent ev) {
    	System.out.println("\nDelete Test ...");
    	try {
        	
    		severityLevelService.deleteSeverityLevel(severityLevelToDelete);
//    		employees=null;
    		addInfoMessage(getMessagesBoundle().getString("severityLevel.deleted"));
		} catch (Exception e) {
			e.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("severityLevel.deletingError"), e);
		}
	}
	
	public void addSeverityLevel(ActionEvent e) {
		severityLevelToEdit = new SeverityLevel();
	}
	
	public void modifySeverityLevel(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("severityLevelId")) {
    			severityLevelToEdit = severityLevelService.getSeverityLevelById((Long)((UIParameter)com).getValue());
			}
		}
	}
	
	public void cancelEditSeverityLevel(ActionEvent e) {
		severityLevelToEdit = null;
	}
	
	public void saveSeverityLevel(ActionEvent e) {
    	System.out.println("Salvo " + severityLevelToEdit);
    	

    	if (severityLevelToEdit.getDescription()==null || severityLevelToEdit.getDescription().trim().equals("")) {
    		addWarningMessage("test su Descrizione richiesto!");
    		return;
    	}
    	
    	
    	try {
    		severityLevelToEdit = severityLevelService.updateSeverityLevel(severityLevelToEdit);
		
			addInfoMessage(getMessagesBoundle().getString("severityLevel.saved"));
    	} catch (PersistenceException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();

    	} catch (DataIntegrityViolationException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();
    				
		} catch (Exception e1) {
			addErrorMessage(getMessagesBoundle().getString("severityLevel.savingError"), e1);
			e1.printStackTrace();
		}
    	severityLevelToEdit = null;
	}
	
	public void filterByDate() {
		System.out.println("Filter by date !");
		
		Calendar cal = Calendar.getInstance();
		if (filterFrom!=null) {
			cal.setTime(filterFrom);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 1);
			filterFrom = cal.getTime();
		}
		if (filterTo!=null) {
			cal.setTime(filterTo);
			cal.set(Calendar.HOUR_OF_DAY, 23);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);
			filterTo = cal.getTime();
		}
		
		((LazySeverityLevelDataModel)lazyModelSeverityLevel).setFilterFrom(filterFrom);
		((LazySeverityLevelDataModel)lazyModelSeverityLevel).setFilterTo(filterTo);
	}
	
	public Object getSeverityLevelById(long id) {
		return severityLevelService.getSeverityLevelById(id);
	}

	public SeverityLevel getSeverityLevelToEdit() {
		return severityLevelToEdit;
	}

	public void setSeverityLevelToEdit(SeverityLevel empToEdit) {
		this.severityLevelToEdit = empToEdit;
	}

	public LazyDataModel<SeverityLevel> getLazyModelSeverityLevel() {
		return lazyModelSeverityLevel;
	}

	public Date getFilterFrom() {
		return filterFrom;
	}

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public Date getFilterTo() {
		return filterTo;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}

}
