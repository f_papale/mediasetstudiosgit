package com.crismasecurity.mediasetStudios.web.management;

import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.crismasecurity.mediasetStudios.core.entity.AgencySpectator;
import com.crismasecurity.mediasetStudios.core.services.AgencySpectatorService;

public class LazyAgencySpectatorDataModel extends LazyDataModel<AgencySpectator>{


	/**
	 * 
	 */
	private static final long serialVersionUID = 6373517669208256181L;

	
	private AgencySpectatorService AgencySpectatorService;
	
	private Long agencyId;
	
	
	public LazyAgencySpectatorDataModel(AgencySpectatorService sptypeservice) {
		this.AgencySpectatorService = sptypeservice;
	}
	
	@Override
	public AgencySpectator getRowData(String rowKey) {
		if (rowKey==null || rowKey.trim().equals("")) return null;
		return AgencySpectatorService.getAgencySpectatorById(Long.parseLong(rowKey));
	}
	
    @Override
    public Object getRowKey(AgencySpectator agsp) {
    	return agsp.getId();
    }
    
    @Override 
    public List<AgencySpectator> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters){
    	// Converto ordinamento
    	int sortInt = 0; // UNSORTED
    	if (sortOrder.equals(SortOrder.ASCENDING)) sortInt = 1;
    	if (sortOrder.equals(SortOrder.DESCENDING)) sortInt = 2;
    	
    	if (agencyId != null) filters.put("agency.idAgency", agencyId);
    	
    	List<AgencySpectator> sptypelist = AgencySpectatorService.getAgencySpectatorLazyLoading(first, pageSize, sortField, sortInt, filters);
    	this.setRowCount((int)AgencySpectatorService.getAgencySpectatorLazyLoadingCount(filters));
//    	System.out.println("Numero elementi trovati: " + this.getRowCount());    	
    	return sptypelist;
    	
    }

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

}
