package com.crismasecurity.mediasetStudios.web.management;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpSession;

import org.primefaces.push.EventBus;
import org.primefaces.push.EventBusFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.crismasecurity.mediasetStudios.core.entity.AgencyUser;
import com.crismasecurity.mediasetStudios.core.entity.Episode;
import com.crismasecurity.mediasetStudios.core.security.services.AppUserService;
import com.crismasecurity.mediasetStudios.core.services.AgencyUserService;
import com.crismasecurity.mediasetStudios.web.LoginBean;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;

//@ManagedBean(name= EmployeesBean.BEAN_NAME) // JSF managed
//@SessionScoped // JSF managed
//@ViewScoped // JSF managed

//Spring managed
@Controller (AgencyUserBean.BEAN_NAME) 
//@Scope("session")
@Scope("view")
public class AgencyUserBean extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 275036363337340219L;

	public static final String BEAN_NAME = "agencyUserBean";
    public String getBeanName() { return BEAN_NAME; } 



    @Autowired
    private AgencyUserService agencyUserService;
    
	@Autowired
	private AppUserService userService;

	@Autowired
	private LoginBean loginBean;
	
    private AgencyUser currentAgencyUser = null;
	private Map<String, Object> agenciesUserList;
	private Map<String, AgencyUser> agenciesUser;
	private String selectedAgencyUser = null;
	private String selectedAgencyUsername = null;
//	private String loggedUser;
	private Long userId;

    
	public AgencyUserBean() {
		getLog().info("!!!! Costruttore di AgencyUserBean !!!");	
    }

	@PostConstruct
	public void init() {
		presetCurrentUser();
		presetAgenciesList();		
	}

	@PreDestroy
	public void destroy() {
		getLog().info("Destroy AgencyUserBean!");

	}
	
	
	public Object getAgencyUserByIdString(String id) {
		return agencyUserService.getAgencyUserByIdString(id);
	}
	
	private void presetCurrentUser() {
		try {
			userId = userService.getUserByLogin(loginBean.getUserId()).getId();
//			currentAgencyUser = agencyUserService.getAgencyUserFinder().and("user.id").eq(userId).and("flgCurrentSelected").eq(true).result();	
//			loggedUser = userService.getUserByLogin(loginBean.getUserId()).getFirstName() + " " + userService.getUserByLogin(loginBean.getUserId()).getLastName();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Errore Preset Current User");
			addErrorMessage(getMessagesBoundle().getString("AgencyUser.presetCurrentUser"), e);  	
		} 
	}	
	
	private void presetAgenciesList() {
		setAgenciesUser(new LinkedHashMap<String, AgencyUser>());
		setAgenciesUserList(new LinkedHashMap<String, Object>());
		for (AgencyUser cont : agencyUserService.getAgencyUserFinder().and("user.id").eq(userId).setDistinct().list()) {
			getAgenciesUserList().put(cont.getAgency().getName(), cont.getIdString());
			getAgenciesUser().put(cont.getIdString(), cont);
//			agenciesUser.put(cont.getIdString(),cont);
			if (cont.getFlgCurrentSelected()) { 
				setSelectedAgencyUser(cont.getIdString());
				setSelectedAgencyUsername(cont.getAgency().getName());
//				setSelectedAgencyUser(cont.getAgency().getName());
			}
		}
	}


	
	public void updateAgencyUser(ActionEvent e) {
		
		System.out.println(getSelectedAgencyUser());
		String key = getSelectedAgencyUser().toString();
		AgencyUser dummy = agencyUserService.getAgencyUserByIdString(key);
		try {
			for (AgencyUser agencyUser : agencyUserService.getAgencyUserFinder().and("user.id").eq(dummy.getUser().getId()).list()) {
				if (agencyUser.getIdString().equals(dummy.getIdString())) {
					if (! agencyUser.getFlgCurrentSelected()) {
						agencyUser.setFlgCurrentSelected(true);
						setSelectedAgencyUsername(agencyUser.getAgency().getName());
						agencyUserService.updateAgencyUser(agencyUser);		
					}

				} else {
					if (agencyUser.getFlgCurrentSelected()) {
						agencyUser.setFlgCurrentSelected(false);
						agencyUserService.updateAgencyUser(agencyUser);	
					}
			   }
			}			
		} catch (Exception e2) {
			e2.printStackTrace();
			System.out.println("Errore updateAgencyUser");
			addErrorMessage(getMessagesBoundle().getString("Errore nell'aggiornamento"), e2);  	
		}
	}

	public Map<String, Object> getAgenciesUserList() {
		return agenciesUserList;
	}

	public void setAgenciesUserList(Map<String, Object> agenciesUserList) {
		this.agenciesUserList = agenciesUserList;
	}

	public Map<String, AgencyUser> getAgenciesUser() {
		return agenciesUser;
	}

	public void setAgenciesUser(Map<String, AgencyUser> agenciesUser) {
		this.agenciesUser = agenciesUser;
	}

	public String getSelectedAgencyUser() {
		return selectedAgencyUser;
	}

	public void setSelectedAgencyUser(String selectedAgencyUser) {
		this.selectedAgencyUser = selectedAgencyUser;
	}

	public String getSelectedAgencyUsername() {
		return selectedAgencyUsername;
	}

	public void setSelectedAgencyUsername(String selectedAgencyUsername) {
		this.selectedAgencyUsername = selectedAgencyUsername;
	}
}
