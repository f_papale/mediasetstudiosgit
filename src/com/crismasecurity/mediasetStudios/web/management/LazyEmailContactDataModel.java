package com.crismasecurity.mediasetStudios.web.management;

import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.crismasecurity.mediasetStudios.core.entity.EmailContact;
import com.crismasecurity.mediasetStudios.core.services.EmailContactService;

public class LazyEmailContactDataModel extends LazyDataModel<EmailContact>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2960910410611093828L;
	
	private EmailContactService EmailContactService;
	
	public LazyEmailContactDataModel(EmailContactService appTypeservice) {
		this.EmailContactService = appTypeservice;
	}
	
	@Override
	public EmailContact getRowData(String rowKey) {
		if (rowKey==null || rowKey.trim().equals("")) return null;
		return EmailContactService.getEmailContactById(Long.parseLong(rowKey));
	}
	
    @Override
    public Object getRowKey(EmailContact aptype) {
    	return aptype.getId();
    }
    
    @Override 
    public List<EmailContact> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters){
    	// Converto ordinamento
    	int sortInt = 0; // UNSORTED
    	if (sortOrder.equals(SortOrder.ASCENDING)) sortInt = 1;
    	if (sortOrder.equals(SortOrder.DESCENDING)) sortInt = 2;
    	
    	List<EmailContact> sptypelist = EmailContactService.getEmailContactLazyLoading(first, pageSize, sortField, sortInt, filters);
    	this.setRowCount((int)EmailContactService.getEmailContactLoadingCount(filters));
//    	System.out.println("Numero elementi trovati: " + this.getRowCount());    	
    	return sptypelist;
    	
    }

}
