package com.crismasecurity.mediasetStudios.web.management;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.crismasecurity.mediasetStudios.core.entity.Episode;
import com.crismasecurity.mediasetStudios.core.services.EpisodeService;
 

public class LazyEpisodeDataModel extends LazyDataModel<Episode> {


	private static final long serialVersionUID = 1540078380604196896L;

	private EpisodeService episodeService;
	private Long agencyId;
	private Long ownerId;
	
	
	private Date filterFrom;
	private Date filterTo;

     
    public LazyEpisodeDataModel(EpisodeService episodeService) {
        this.episodeService = episodeService;
    }

    @Override
    public Episode getRowData(String rowKey) {
    	if (rowKey==null || rowKey.trim().equals("")) return null;
    	return episodeService.getEpisodeById(Long.parseLong(rowKey));
    }
 
    @Override
    public Object getRowKey(Episode audit) {
        return audit.getId();
    }
 
    @Override
    public List<Episode> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
    	
    	// Converto ordinamento
    	int sortInt =0; // UNSORTED
    	if (sortOrder.equals(SortOrder.ASCENDING)) sortInt = 1;
    	if (sortOrder.equals(SortOrder.DESCENDING)) sortInt = 2;
    	
 
		if (filterFrom!=null) filters.put("date#ge", filterFrom);
		if (filterTo!=null) filters.put("date#le", filterTo);
		
		if (agencyId != null) filters.put("production.agency.idAgency", agencyId);
		if (ownerId != null) filters.put("production.owner.idAgency", ownerId);

    	
    	List<Episode> ret = episodeService.getEpisodeLazyLoading(first, pageSize, sortField, sortInt, filters);
    	this.setRowCount((int)episodeService.getEpisodeLazyLoadingCount(filters));
//    	System.out.println("Numero elementi trovati: " + this.getRowCount());
//    	for (Audit audit : ret) {
//			System.out.println("Trovato: " + audit);
//		}
    	return ret;    
    }

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

	public Long getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}
}