package com.crismasecurity.mediasetStudios.web.management;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import javax.faces.model.SelectItemGroup;
import javax.persistence.PersistenceException;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.FlowEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import com.crismasecurity.mediasetStudios.core.entity.Agency;
import com.crismasecurity.mediasetStudios.core.entity.AgencyUser;
import com.crismasecurity.mediasetStudios.core.entity.DistributionList;
import com.crismasecurity.mediasetStudios.core.entity.Employee;
import com.crismasecurity.mediasetStudios.core.entity.Episode;
import com.crismasecurity.mediasetStudios.core.entity.EpisodeStatusType;
import com.crismasecurity.mediasetStudios.core.entity.Production;
import com.crismasecurity.mediasetStudios.core.entity.ProductionEmployee;
import com.crismasecurity.mediasetStudios.core.entity.ProductionType;
import com.crismasecurity.mediasetStudios.core.entity.Studio;
import com.crismasecurity.mediasetStudios.core.security.services.AppUserService;
import com.crismasecurity.mediasetStudios.core.services.AgencyService;
import com.crismasecurity.mediasetStudios.core.services.AgencyUserService;
import com.crismasecurity.mediasetStudios.core.services.DistributionListService;
import com.crismasecurity.mediasetStudios.core.services.EmployeeService;
import com.crismasecurity.mediasetStudios.core.services.EpisodeService;
import com.crismasecurity.mediasetStudios.core.services.EpisodeStatusTypeService;
import com.crismasecurity.mediasetStudios.core.services.ProductionEmployeeService;
import com.crismasecurity.mediasetStudios.core.services.ProductionService;
import com.crismasecurity.mediasetStudios.core.services.ProductionTypeService;
import com.crismasecurity.mediasetStudios.core.services.StudioService;
import com.crismasecurity.mediasetStudios.web.LoginBean;
import com.crismasecurity.mediasetStudios.web.management.utils.DurationType;
import com.crismasecurity.mediasetStudios.web.management.utils.EventSchedule;
import com.crismasecurity.mediasetStudios.web.management.utils.EventType;
import com.crismasecurity.mediasetStudios.web.management.utils.FrequencyType;


/*
import com.crismasecurity.mediasetStudios.web.management.utils.ManageFiscalCode;
*/

import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;

//@ManagedBean(name= EmployeesBean.BEAN_NAME) // JSF managed
//@SessionScoped // JSF managed
//@ViewScoped // JSF managed

//Spring managed
@Controller (ProductionBean.BEAN_NAME) 
//@Scope("session")
@Scope("view")
public class ProductionBean extends BaseBean {


	private static final long serialVersionUID = 5278743136213989941L;

	public static final String BEAN_NAME = "productionBean";
    public String getBeanName() { return BEAN_NAME; } 


    @Autowired
    private ProductionService productionService ;
    
    @Autowired
    private ProductionTypeService productionTypeService;
    
    @Autowired
    private AgencyService agencyService;

    @Autowired
    private DistributionListService distributionListService;    
    
    @Autowired
    private StudioService studioService;
    
    @Autowired
    private ProductionEmployeeService productionEmployeeService;
    
    @Autowired
    private EpisodeService episodeService ;
        
    @Autowired
    private EpisodeStatusTypeService episodeStatusTypeService;
    
    @Autowired
    private EmployeeService employeeService;

	@Autowired
	private AgencyUserService agencyUserService;
	
	@Autowired
	private AppUserService userService;
	
	@Autowired
	private LoginBean loginBean;
	
//---------------------------------------

	private LazyDataModel<Production> lazyModelProduction;
    private LazyDataModel<Episode> lazyModelEpisode;
    private Date filterFrom;
	private Date filterTo;
	private Agency filterOwner;
	private AgencyUser currentAgencyUser=null;	
	
	private Agency owner = null;
	
	private List<SelectItem> productionTypes;
	private Long selectedProductionType = null;
	
	private List<SelectItem> studios;
	private Long selectedStudio = null;
	private Long selectedStudio4Schedule = null;
	
	private List<SelectItem> episodeStatusTypes;
	private Long selectedEpisodeStatusType = null;

	private List<SelectItem> productionEmployees;
	private Long selectedProductionEmployee = null;
	
	private List<SelectItem> agencies;
	private Long selectedAgency = null;
	
	private List<SelectItem> distributionLists;
	private Long selectedDistributionList = null;
	
	private Production productionToEdit = null;	
	private Long productionToDelete = null;
		
	private Episode episodeToEdit=null;		
	private Long episodeToDelete=null;	
	
	private ProductionEmployee productionEmployeeToEdit=null;		
	private Long productionEmployeeToDelete=null;	

	private EventSchedule episodesToSchedule = null;
	private boolean weeklySelected=false; 
	
	private int activeTab=0;

	private HashMap<DayOfWeek, String> weeksDecode = new HashMap<DayOfWeek, String>();
	private HashMap<FrequencyType, String> frequencyDecode = new HashMap<FrequencyType, String>();
	private HashMap<FrequencyType, String> intervalDecode = new HashMap<FrequencyType, String>();
	
	//private StreamedContent logo;

	public ProductionBean() {
		getLog().info("!!!! Costruttore di ProductionBean !!!");	
    }

	@PostConstruct
	public void init() {
		getLog().info("INIT ProductionBean!");
		presetMapsList();
		presetCurrentUser();
		presetProductionList();
		presetDistributionList();
		presetStudioList();
		presetEpisodeStatusList();
		presetAgencyList();
		presetProductionEmployees();
		presetOwner();
		lazyModelProduction = new LazyProductionDataModel(productionService);
		setLazyModelEpisode(new LazyEpisodeDataModel(episodeService));
		filterByOwner();
	}

	@PreDestroy
	public void destroy() {
		getLog().info("Destroy ProductionBean!");

	}
	
	
	public void handleFileUpload(FileUploadEvent event) {
        String content = event.getFile().getContentType()+"#"+event.getFile().getFileName()+"#"+Base64.getEncoder().encodeToString(event.getFile().getContents());
        productionToEdit.setLogo(content);
        System.out.println(productionToEdit.getLogo());        
        addInfoMessage(event.getFile().getFileName() + " is uploaded.");
    }
	
	
	private Long  getIdLoggedAgency() {
		return Long.valueOf(currentAgencyUser.getAgency().getIdAgency());				
	}
	
	public boolean isLoggedAnProductionAgency() {
		boolean bReturn;
		try {
			bReturn=currentAgencyUser.getAgency().getAgencyType().isFlgProduction();
		} catch (Exception e) {
			bReturn=false;
		}
		return 	bReturn;			
	}
	
	private void presetMapsList() {
		weeksDecode.put(DayOfWeek.MONDAY,"Lunedì");
		weeksDecode.put(DayOfWeek.TUESDAY,"Martedì");
		weeksDecode.put(DayOfWeek.WEDNESDAY,"Mercoledì");
		weeksDecode.put(DayOfWeek.THURSDAY,"Giovedì");
		weeksDecode.put(DayOfWeek.FRIDAY,"Venerdì");
		weeksDecode.put(DayOfWeek.SATURDAY,"Sabato");
		weeksDecode.put(DayOfWeek.SUNDAY,"Domenica");
		
		intervalDecode.put(FrequencyType.DAILY, "giorni");
		intervalDecode.put(FrequencyType.WEEKLY, "settimane");
		intervalDecode.put(FrequencyType.MONTLY, "mesi");
		intervalDecode.put(FrequencyType.ANNUAL, "anni");
		
		frequencyDecode.put(FrequencyType.DAILY, "Giornaliera");
		frequencyDecode.put(FrequencyType.WEEKLY, "Settimanale");
		frequencyDecode.put(FrequencyType.MONTLY, "Mensile");
		frequencyDecode.put(FrequencyType.ANNUAL, "Annuale");
	}
	
	private void presetOwner() {

		if ((haveUserRole("ROLE_AGENCY") && haveUserRole("ROLE_PRODUCTION")) || (haveUserRole("ROLE_AGENCY") && haveUserRole("ROLE_STADMIN") )) {		
			try {
				setOwner(agencyService.getAgencyFinder().and("idAgency").eq(getIdLoggedAgency()).setDistinct().result());
				setFilterOwner(getOwner());
				
			} catch (Exception e) {
				setOwner(null);
				setFilterOwner(null);
			}
		} else {
			setFilterOwner(null);
			try {
				try {
					setOwner(agencyService.getAgencyById(getIdLoggedAgency()));	
				} catch (Exception e) {
					// Attenzione se entro come amministratore viene settato come owner la prima super agenzia censita 
					setOwner(agencyService.getAgencyFinder().and("agencyType.flgProduction").eq(true).addOrderAsc("id").setMaxResults(1).setDistinct().result()); 	
				}
				
			} catch (Exception e) {
				setOwner(null);
			}
		}
	}
	
	
	private void presetCurrentUser() {

		try {
			long userId = userService.getUserByLogin(loginBean.getUserId()).getId();
			currentAgencyUser= agencyUserService.getAgencyUserFinder().and("user.id").eq(userId).and("flgCurrentSelected").eq(true).result();	
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Errore Preset Current User");
			addErrorMessage(getMessagesBoundle().getString("spectatorEpisode.presetCurrentUser"), e);  	
		} 
	}
	
	private void presetProductionList() {
		productionTypes = new ArrayList<SelectItem>();
		List<ProductionType> conts = productionTypeService.getProductionType();
		productionTypes.add(new SelectItem(null, " "));
		for (ProductionType cont : conts) {
			SelectItem xx = new SelectItem(cont.getId(), cont.getDescription());
			productionTypes.add(xx);
		}
	}
	
	private void presetDistributionList() {
		distributionLists = new ArrayList<SelectItem>();
		List<DistributionList> conts = distributionListService.getDistributionList();
		distributionLists.add(new SelectItem(null, " "));
		for (DistributionList cont : conts) {
			SelectItem xx = new SelectItem(cont.getId(), cont.getName());
			distributionLists.add(xx);
		}
	}	
	
	private void presetProductionEmployees() {
		setProductionEmployees(new ArrayList<SelectItem>());
		List<Employee> conts = employeeService.getEmployee();
		getProductionEmployees().add(new SelectItem(null," "));
		for (Employee cont : conts) {
			SelectItem xx = new SelectItem(cont.getId(), cont.getSurname()+" "+cont.getName()+" - "+cont.getEmployeeType().getDescription());
			getProductionEmployees().add(xx);
		}		
	}
	
	private void presetStudioList() {

		setStudios(new ArrayList<SelectItem>());
		List<Studio> conts = studioService.getStudio();
		getStudios().add(new SelectItem(null, " "));
		List<SelectItemGroup> prodCenters= new ArrayList<SelectItemGroup>();
		
		for (Studio cont : conts) {
			//Definisco un ItemGroup con il nome del Centro di Producuione
			SelectItemGroup g1 = new SelectItemGroup(cont.getProductionCenter().getName());
			// Valuto se è inserito gia nella lista 
			int itemPos = prodCenters.indexOf(g1);
			SelectItem[] newItem = new SelectItem[] {new SelectItem(cont.getId(), cont.getName())};
			//Non esiste ancora un gruppo con il generico Studio
			if (itemPos==-1) {
				g1.setSelectItems(newItem);
				prodCenters.add(g1);
			}else {
				// Devo aggiornare SelectItem[] del gruppo di appartenenza del nuovo item
				g1=prodCenters.get(itemPos);
				SelectItem[] source = g1.getSelectItems();
				SelectItem[] target = new SelectItem[source.length+1];
				System.arraycopy(source,0,target,0,source.length);
				System.arraycopy(newItem,0,target,target.length,newItem.length);
				g1.setSelectItems(target);
				prodCenters.set(itemPos,g1);
			}
		}		
		for (SelectItemGroup sel: prodCenters) {
			getStudios().add(sel);
		}
	}
	
	private void presetAgencyList() {
		setAgencies(new ArrayList<SelectItem>());
		//List<Agency> conts = agencyService.getAgencyFinder().and("agencyType.flgProduction").eq(false).addOrderAsc("name").list();
		List<Agency> conts = agencyService.getAgencyFinder().addOrderAsc("name").list();
		try {
			//conts.add(0, agencyService.getAgencyById(getIdLoggedAgency()));
			getAgencies().add(new SelectItem(null, " "));
			for (Agency cont: conts) {
				SelectItem xx = new SelectItem(cont.getIdAgency(), cont.getName());
				getAgencies().add(xx);
			}
		} catch(Exception e) {
			System.out.println("errore nel recuperare l'elenco delle agenzie "  + e.toString());
		}
	}
	
	
	private void presetEpisodeStatusList() {
		setEpisodeStatusTypes(new ArrayList<SelectItem>());
		List<EpisodeStatusType> conts = episodeStatusTypeService.getEpisodeStatusType();
		getEpisodeStatusTypes().add(new SelectItem(null, " "));
		for (EpisodeStatusType cont : conts) {
			SelectItem xx = new SelectItem(cont.getId(), cont.getDescription());
			getEpisodeStatusTypes().add(xx);
		}
	}
	
	
	public void confirmDeleteProduction(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("productionId")) {
    			setProductionToDelete(((Long)((UIParameter)com).getValue()));
			}
		}
	}
	
	public void cancelDelete(ActionEvent ev) {
	    	System.out.println("\nDelete Production canceled.");
	    	setProductionToDelete(null);
	}
	
	public void delete(ActionEvent ev) {
    	System.out.println("\nDelete Test ...");
    	try {
    		productionService.deleteProduction(getProductionToDelete());
    		addInfoMessage(getMessagesBoundle().getString("production.deleted"));
		} catch (Exception e) {
			e.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("production.deletingError"), e);
		}
    	finally {
    		try {
    			productionToEdit=productionService.getProductionById(productionToEdit.getId());	
			} catch (Exception e2) {
				productionToEdit=null;
			}
    		 
    	}
	}
	
	
	public void confirmDeleteProductionEmployee(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("productionEmployeeId")) {
    			setProductionEmployeeToDelete(((Long)((UIParameter)com).getValue()));
			}
		}
	}
	
	public void deleteProductionEmployee(ActionEvent ev) {
    	System.out.println("\nDelete Employee ...");
    	try {
    		productionEmployeeService.deleteProductionEmployee(getProductionEmployeeToDelete());
    		addInfoMessage(getMessagesBoundle().getString("employee.deleted"));
		} catch (Exception e) {
			e.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("employee.deletingError"), e);
		}
    	finally {
    		productionToEdit=productionService.getProductionById(productionToEdit.getId()); 
    	}
	}
	
	
	public void cancelDeleteProductionEmployee(ActionEvent ev) {
    	System.out.println("\nDelete Employee canceled.");
    	setProductionEmployeeToDelete(null);
	}
	
	
	private void calculateStartDateAndEndDate() {
		
		Date oldStartDate =productionToEdit.getStartDate();
		Date oldEndDate =productionToEdit.getEndDate();
		try {
			Date minDate=new SimpleDateFormat( "yyyy-MM-dd" ).parse( "9999-12-31" );
			Date maxDate=new SimpleDateFormat( "yyyy-MM-dd" ).parse( "1970-01-01" );
			
			for (int iCount=0; iCount<productionToEdit.getEpisodes().size(); ++iCount) {
				Episode eDummy = productionToEdit.getEpisodes().get(iCount);
				//minDate > eDummy.getDateFrom()
				if (minDate.compareTo(eDummy.getDateFrom())>0) minDate=eDummy.getDateFrom();
				//maxDate<eDummy.getDateFrom()
				if (maxDate.compareTo(eDummy.getDateFrom())<0) maxDate=eDummy.getDateFrom();
			}
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			productionToEdit.setStartDate(formatter.parse(formatter.format(minDate)));
			productionToEdit.setEndDate(formatter.parse(formatter.format(maxDate)));
			
		} catch (Exception e) {
			productionToEdit.setStartDate(oldStartDate);
			productionToEdit.setEndDate(oldEndDate);
		}
	}
	
	public void deleteEpisode(ActionEvent ev) {
    	System.out.println("\nDelete Episode ...");
    	try {
    		episodeService.deleteEpisode(getEpisodeToDelete());
    		addInfoMessage(getMessagesBoundle().getString("episode.deleted"));
    		productionToEdit=productionService.getProductionById(productionToEdit.getId()); 
    		if (productionToEdit.getEpisodes() != null) {
    			//Imposta il numero di episodi sulla base del nimenro di Items della collection
	    		productionToEdit.setEpisodesNum(productionToEdit.getEpisodes().size());
	    		//Ricalcola data inizio e fine produzione sulla base della data della prima e dell'ultima puntata
	    		calculateStartDateAndEndDate();
	    		//Se il numero di spettatori non è impostato lo prende da quello dell aprima puntata
	    		if(productionToEdit.getSpectatorsReq()==0) {
	    			if (productionToEdit.getEpisodes().size()>0) {
	    				productionToEdit.setSpectatorsReq(productionToEdit.getEpisodes().get(0).getRequestedSpectators().intValue());
	    			}
	    		}else{
	    			for (int iCount=0; iCount<productionToEdit.getEpisodes().size(); ++iCount) {
	    				Episode eDummy = productionToEdit.getEpisodes().get(iCount);
	    				eDummy.setRequestedSpectators((long)productionToEdit.getSpectatorsReq());
	    				productionToEdit.getEpisodes().set(iCount, eDummy);
	    			}
	    		}
	    	} 	 	
    		productionToEdit = productionService.updateProduction(productionToEdit);
		} catch (Exception e) {
			e.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("episode.deletingError"), e);
		} finally {
			productionToEdit=productionService.getProductionById(productionToEdit.getId()); 
			episodeToDelete=null;
		}
	}
	
	
	public void cancelDeleteEpisode(ActionEvent ev) {
    	System.out.println("\nDelete Episode canceled.");
    	setEpisodeToDelete(null);
	}
		
	
	public void addProduction(ActionEvent e) {
		productionToEdit = new Production();
		selectedProductionType=null;
		selectedEpisodeStatusType=null;
		selectedStudio=null;
		selectedAgency=null;
		selectedProductionEmployee=null;
		selectedDistributionList=null;
		
//		logo = null;
	}
	
	
/*
	public void cloneEpisode(ActionEvent ev) {
    	System.out.println("\nClone Test ...");
		HtmlCommandLink component = (HtmlCommandLink)ev.getSource();
		for (UIComponent com : component.getChildren()) {
			if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("episodeId")) {
				episodeToEdit = new Episode();
				try {
					Episode episodeToClone= episodeService.getEpisodeById((Long)((UIParameter)com).getValue());
					episodeToEdit.setId(null);
		    		episodeToEdit.setDateFrom(episodeToClone.getDateTo());
		    		episodeToEdit.setDateTo(episodeToClone.getDateTo());
		    		episodeToEdit.setEpisodeStatus(episodeStatusTypeService.getEpisodeStatusTypeByIdStatus((episodeToClone.getSpectators().size()==0?1:2)));	    		
		    		episodeToEdit.setProduction(episodeToClone.getProduction());
		    		episodeToEdit.setStudio(episodeToClone.getStudio());
		    		episodeToEdit.setRequestedSpectators(episodeToClone.getRequestedSpectators());
		    		episodeToEdit.setEffectiveSpectators(0L);
		    		List<SpectatorEpisode> lstSpectators = new ArrayList<SpectatorEpisode>();
					for (SpectatorEpisode spectatorEpisodeTempl : episodeToClone.getSpectators()) {
						SpectatorEpisode spectatorEpisode = new SpectatorEpisode();
						spectatorEpisode.setDescription(spectatorEpisodeTempl.getDescription());
						spectatorEpisode.setEpisode(episodeToEdit);
						spectatorEpisode.setProduction(spectatorEpisodeTempl.getProduction());
						spectatorEpisode.setSpectator(spectatorEpisodeTempl.getSpectator());
						spectatorEpisode.setVip(spectatorEpisodeTempl.isVip());
						lstSpectators.add(spectatorEpisode);
					}
					episodeToEdit.setSpectators(lstSpectators);
					setSelectedEpisodeStatusType(episodeToEdit.getEpisodeStatus().getId());
					setSelectedProduction(episodeToEdit.getProduction().getId());
					setSelectedStudio(episodeToEdit.getStudio().getId());
					
				} catch (Exception e) {
					episodeToEdit=null;
					addErrorMessage("test su Stato Episodio richiesto!");
				}
			}
		}
	} 	
 */

	public void cloneProduction(ActionEvent ev) {
    	System.out.println("\nClone Test ...");
		HtmlCommandLink component = (HtmlCommandLink)ev.getSource();
		for (UIComponent com : component.getChildren()) {
			if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("productionId")) {
				productionToEdit = new Production();
				try {
					Production productionToClone= productionService.getProductionById((Long)((UIParameter)com).getValue());
					productionToEdit.setId(null);
					productionToEdit.setAgency(productionToClone.getAgency());
					productionToEdit.setOwner(productionToClone.getOwner());
					productionToEdit.setDescription(productionToClone.getDescription());
					productionToEdit.setFeatures(productionToClone.getFeatures());
					productionToEdit.setLogo(productionToClone.getLogo());
					productionToEdit.setManagerEmail(productionToClone.getManagerEmail());
					productionToEdit.setName(productionToClone.getName());
					productionToEdit.setProductionType(productionToClone.getProductionType());
					productionToEdit.setSpectatorsReq(productionToClone.getSpectatorsReq());
					productionToEdit.setEpisodesNum(0);
					List<ProductionEmployee> lstProductionEmplyee = new ArrayList<ProductionEmployee>();
					for (ProductionEmployee productionEmployeeTempl: productionToClone.getProductionEmployee()) {
						ProductionEmployee productionEmployee = new ProductionEmployee();
						productionEmployee.setEmployee(productionEmployeeTempl.getEmployee());
						productionEmployee.setProduction(productionToEdit);
						productionEmployee.setRole(productionEmployeeTempl.getRole());
						productionEmployee.setId(null);
						lstProductionEmplyee.add(productionEmployee);
					}
					productionToEdit.setProductionEmployee(lstProductionEmplyee);
					setSelectedAgency(productionToEdit.getAgency().getIdAgency());
					setSelectedProductionType(productionToEdit.getProductionType().getId());

				} catch (Exception e) {
					productionToEdit=null;
					addErrorMessage("cloneProduction");
				}
			}
		}
	} 		
	
	public void addProductionEmployee(ActionEvent e) {
		productionEmployeeToEdit = new ProductionEmployee();
		selectedProductionEmployee=null;
		setActiveTab(1);
	}
	
	public void modifyProduction(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("productionId")) {
    			productionToEdit = productionService.getProductionById((Long)((UIParameter)com).getValue());
    			selectedProductionType=productionToEdit.getProductionType().getId();
    			selectedAgency=productionToEdit.getAgency().getIdAgency();
    			try {
    				selectedDistributionList= productionToEdit.getDistributionList().getId();	
				} catch (Exception e2) {
					selectedDistributionList= null;
				}
    			System.out.println("LOGO:" + productionToEdit.getLogo());
//    			if (productionToEdit.getLogo()!=null && !productionToEdit.getLogo().equals("")) {
//    				try {
//	    				int indHash= productionToEdit.getLogo().indexOf("#");
//	    				String contentType = productionToEdit.getLogo().substring(0, indHash);
//	    				int indHash2= productionToEdit.getLogo().indexOf("#", indHash+1);
//	    				String filename = productionToEdit.getLogo().substring(indHash+1, indHash2);
//	    				String content = productionToEdit.getLogo().substring(indHash2+1);
//	    				
//	    				logo = new ByteArrayContent(Base64.getDecoder().decode(content), contentType, filename);
//    				} catch(Exception e1) {
//    					e1.printStackTrace();
//    				}
//    			}
//    			else logo = null;
    			
			}
		}
	}
	
	public void confirmDeleteEpisode(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("episodeId")) {
    			setEpisodeToDelete(((Long)((UIParameter)com).getValue()));
			}
		}
	}
	
	public void confirmDeleteEmployee(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("productionEmployeeId")) {
    			setProductionEmployeeToDelete(((Long)((UIParameter)com).getValue()));
			}
		}
	}
	
	public void modifyEpisode(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("episodeId")) {
    			episodeToEdit = episodeService.getEpisodeById((Long)((UIParameter)com).getValue());  
    			setSelectedStudio(episodeToEdit.getStudio().getId());
    			setSelectedEpisodeStatusType(episodeToEdit.getStudio().getId());
    			setActiveTab(0);
			}
		}
	}
	
	public void modifyProductionEmployee(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("productionEmployeeId")) {
    			productionEmployeeToEdit = productionEmployeeService.getProductionEmployeeById((Long)((UIParameter)com).getValue());  
    			setSelectedProductionEmployee(productionEmployeeToEdit.getEmployee().getId());	
    			setActiveTab(1);
			}
		}
	}
	
	public void cancelEditProduction(ActionEvent e) {
		productionToEdit = null;
		episodeToEdit=null;
		productionEmployeeToEdit=null;
	}
	
	public void cancelEditProductionEmployee() {
		productionEmployeeToEdit=null;		
	}
	
	public void cancelEditEpisode(ActionEvent e) {
		episodeToEdit=null;
	}
	
	public void wipeEpisodes(ActionEvent e) {
		System.out.println("Rimuovi tutti gli spettacoli....");	

		Iterator<Episode> iterator = productionToEdit.getEpisodes().iterator();
		while (iterator.hasNext()) {
			try {
				Episode episode = iterator.next();
				if (episode.getEpisodeStatus().getIdStatus()==1) {
					episodeService.deleteEpisode(episode.getId());  
				}
			} catch (Exception e1) {
				addErrorMessage(getMessagesBoundle().getString("episode.wipingError"), e1);
				e1.printStackTrace();
			}
		}
		productionToEdit=productionService.getProductionById(productionToEdit.getId()); 
	}
	
	public void cancelWipeEpisodes() {
		
	}
	
	public void confirmWipeEpisodes() {
		
	}
	
	private EpisodeStatusType getOpenStatus() {
		List<EpisodeStatusType> aa = episodeStatusTypeService.getEpisodeStatusTypeFinder().and("idStatus").like((int)1).list();
		return aa.get(0);
	}
	
	private Studio getStudio() {
		List<Studio> aa = studioService.getStudioFinder().and("name").like("Studio 1").list(); 
		return aa.get(0);
	}

/*
 * Procedura che crea gli episodi a 		
 */
	
/*	
	public void addEpisodes(ActionEvent e) {

		episodesToSchedule.buildEventSchedule(true);
		Iterator<Episode> iterator = getEpisodesToSchedule().getLstEpisodes().iterator();
		while (iterator.hasNext()) {
			try {
				episodeToEdit=iterator.next();
				episodeToEdit = episodeService.updateEpisode(episodeToEdit);
			} catch (Exception e1) {
				addErrorMessage(getMessagesBoundle().getString("episode.addingError"), e1);
				e1.printStackTrace();
			}
		}
		try {
			if (productionToEdit.getEpisodes() != null) {
	    		productionToEdit.setEpisodesNum(productionToEdit.getEpisodes().size());
	    		
	    		if(productionToEdit.getSpectatorsReq()==0) {
	    			if (productionToEdit.getEpisodes().size()>0) {
	    				productionToEdit.setSpectatorsReq(productionToEdit.getEpisodes().get(0).getRequestedSpectators().intValue());
	    			}
	    		}else {
	    			for (int iCount=0; iCount<productionToEdit.getEpisodes().size(); ++iCount) {
	    				Episode eDummy = productionToEdit.getEpisodes().get(iCount);
	    				eDummy.setRequestedSpectators((long)productionToEdit.getSpectatorsReq());
	    				productionToEdit.getEpisodes().set(iCount, eDummy);
	    			}
	    		}
	    	}
			productionToEdit = productionService.updateProduction(productionToEdit);
		} catch (Exception e2) {
			addErrorMessage(getMessagesBoundle().getString("production.UldateError after addEpisodes"), e2);
			e2.printStackTrace();
		} 
		productionToEdit=productionService.getProductionById(productionToEdit.getId()); 
		episodesToSchedule=null;
		//setActiveTab(0);
		
	}
*/
	public void addEpisodes(ActionEvent e) {

		episodesToSchedule.buildEventSchedule(true);
		Iterator<Episode> iterator = getEpisodesToSchedule().getLstEpisodes().iterator();
		while (iterator.hasNext()) {
			try {
				episodeToEdit=iterator.next();
				episodeToEdit = episodeService.updateEpisode(episodeToEdit);
			} catch (Exception e1) {
				addErrorMessage(getMessagesBoundle().getString("episode.addingError"), e1);
				e1.printStackTrace();
			}
		}
		try {
			productionToEdit=productionService.getProductionById(productionToEdit.getId()); 
			if (productionToEdit.getEpisodes() != null) {
				//Imposta il numero di episodi sulla base del nimenro di Items della collection
				productionToEdit.setEpisodesNum(productionToEdit.getEpisodes().size());
				//Ricalcola data inizio e fine produzione sulla base della data della prima e dell'ultima puntata
				calculateStartDateAndEndDate();
				//Se il numero di spettatori non è impostato lo prende da quello della prima puntata
	    		if(productionToEdit.getSpectatorsReq()==0) {
	    			if (productionToEdit.getEpisodes().size()>0) {
	    				productionToEdit.setSpectatorsReq(productionToEdit.getEpisodes().get(0).getRequestedSpectators().intValue());
	    			}
	    		}else {
	    			for (int iCount=0; iCount<productionToEdit.getEpisodes().size(); ++iCount) {
	    				Episode eDummy = productionToEdit.getEpisodes().get(iCount);
	    				eDummy.setRequestedSpectators((long)productionToEdit.getSpectatorsReq());
	    				productionToEdit.getEpisodes().set(iCount, eDummy);
	    			}
	    		}
	    		productionToEdit = productionService.updateProduction(productionToEdit);
			}
		} catch (Exception e2) {
			addErrorMessage(getMessagesBoundle().getString("production.UldateError after addEpisodes"), e2);
			e2.printStackTrace();
		} 
		productionToEdit=productionService.getProductionById(productionToEdit.getId()); 
		episodesToSchedule=null;
		episodeToEdit=null;
		episodeToDelete=null;
		setActiveTab(0);
	}
	

	public void saveProduction2(ActionEvent e) {
		/*********************/
		System.out.println("Salvo ...");
    	
    	try {
    		if (productionToEdit.getName()==null || productionToEdit.getName().trim().equals("")) {
        		addErrorMessage("Nome richiesto!");
//        		FacesContext.getCurrentInstance().validationFailed();
        		return;
        	}
//        	System.out.println("Salvo " + productionToEdit.getName().toString());
        	
        	if (selectedProductionType==null) {
        		addErrorMessage("Tipo Produzione richiesto!");
//        		FacesContext.getCurrentInstance().validationFailed();
        		return;
        	} 	
//        	System.out.println("Salvo " + productionTypeService.getProductionTypeById(selectedProductionType).toString());
        	
        	if (selectedDistributionList==null) {
        		addErrorMessage("Lista di distribuzione richiesta!");
//        		FacesContext.getCurrentInstance().validationFailed();
        		return;
        	} 	        	
//        	System.out.println("Salvo " + distributionListService.getDistributionListById(selectedDistributionList).toString());
        	
    		//Imposta il valore della dropdown list nella classe
    		productionToEdit.setProductionType(productionTypeService.getProductionTypeById(selectedProductionType));
    		productionToEdit.setAgency(agencyService.getAgencyById(selectedAgency));
    		productionToEdit.setDistributionList(distributionListService.getDistributionListById(selectedDistributionList));
    		productionToEdit.setOwner(getOwner());
	
    		if (productionToEdit.getEpisodes() != null) {
    			
	    		productionToEdit.setEpisodesNum(productionToEdit.getEpisodes().size());
	    		
	    		if(productionToEdit.getSpectatorsReq()==0) {
	    			if (productionToEdit.getEpisodes().size()>0) {
	    				productionToEdit.setSpectatorsReq(productionToEdit.getEpisodes().get(0).getRequestedSpectators().intValue());
	    			}
	    		} else {
	    			for (int iCount=0; iCount<productionToEdit.getEpisodes().size(); ++iCount) {
	    				Episode eDummy = productionToEdit.getEpisodes().get(iCount);
	    				eDummy.setRequestedSpectators((long)productionToEdit.getSpectatorsReq());
	    				productionToEdit.getEpisodes().set(iCount, eDummy);
	    			}
	    		}
	    	} 	 	
    		productionToEdit = productionService.updateProduction(productionToEdit);
    		
    		addInfoMessage(getMessagesBoundle().getString("production.saved")); 	
    	} catch (PersistenceException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();

    	} catch (DataIntegrityViolationException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();
    	    		
		} catch (Exception e1) {
			addErrorMessage(getMessagesBoundle().getString("production.savingError"), e1);
			e1.printStackTrace();
//			FacesContext.getCurrentInstance().validationFailed();
		} 
    	/******************************/
	}
	
//	public void presetEpisodes(ActionEvent e) {
	public void presetEpisodes(AjaxBehaviorEvent e) {
		

/*
 * StartDate è la DataOra di inzio di un programma 
 * EndDate è la DataOra di fine di un programma
 * UpToaDate è la data di fine della ripetizione degli spettacoli caratterizzati dalle date sopra 
 */
		
		setActiveTab(0);
		episodesToSchedule = new EventSchedule();
		
		//Compose template Episode
		Episode episodeTemplate = new Episode();
		
		Calendar c = Calendar.getInstance();
		//Ammettiamo che inizi alle 15:00
		c.setTime(productionToEdit.getStartDate());
		c.add(Calendar.HOUR_OF_DAY,  15);
		episodeTemplate.setDateFrom(c.getTime());
		
		//Ammettiamo che finisca alle 18:00 dellostesso giorno;
		c.setTime(productionToEdit.getStartDate());
		c.add(Calendar.HOUR_OF_DAY,  18);
		episodeTemplate.setDateTo(c.getTime());

		episodeTemplate.setEffectiveSpectators((long)0);
		episodeTemplate.setEpisodeStatus(getOpenStatus());
		episodeTemplate.setProduction(productionToEdit);
		episodeTemplate.setRequestedSpectators((long)productionToEdit.getSpectatorsReq());
//		episodeTemplate.setStudio(getStudio());
		//episodeTemplate.setStudio(getStudio());
		episodeTemplate.setSpectators(null);

		//Preset other properties
		List<DayOfWeek> dow = new ArrayList<DayOfWeek>();
		
		dow.add(DayOfWeek.MONDAY);
		dow.add(DayOfWeek.WEDNESDAY);
		dow.add(DayOfWeek.FRIDAY);
		
		episodesToSchedule.setEpisode(episodeTemplate);
		episodesToSchedule.setEventType(EventType.OCCASIONAL);
		episodesToSchedule.setFrequencyType(FrequencyType.DAILY);
		episodesToSchedule.setLstdayOfWeek(dow);
		episodesToSchedule.setDurationType(DurationType.UP_TO_A_DATE);
		episodesToSchedule.setUpToaDate(productionToEdit.getEndDate());
		episodesToSchedule.setEventsNumber(10);

	}
	
	public void saveProductionEmployee(ActionEvent e) {
		//System.out.println("Salvo " + productionEmployeeToEdit.toString());
		if (productionEmployeeToEdit.getRole()==null || productionEmployeeToEdit.getRole().trim().equals("")) {
    		addWarningMessage("Ruolo su Addetto richiesto!");
    		return;			
		}
		if (selectedProductionEmployee==null) {
    		addWarningMessage("Addetto richiesto!");
    		return;	
		}
		try {
			productionEmployeeToEdit.setEmployee(employeeService.getEmployeeById(selectedProductionEmployee));
			productionEmployeeToEdit.setProduction(productionToEdit);
			productionEmployeeToEdit = productionEmployeeService.updateProductionEmployee(productionEmployeeToEdit);
    	} catch (PersistenceException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();

    	} catch (DataIntegrityViolationException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();
    				
		} catch (Exception e1) {
			addErrorMessage(getMessagesBoundle().getString("employee.savingError"), e1);
			e1.printStackTrace();
		} finally {
			productionToEdit=productionService.getProductionById(productionToEdit.getId()); 
			productionEmployeeToEdit=null;	
		}
	}

	public void saveEpisode(ActionEvent e) {
		System.out.println("Salvo " + episodeToEdit);

		if (episodeToEdit.getDateFrom()==null || episodeToEdit.getDateFrom().toString().trim().equals("")) {
    		addWarningMessage("Data Da su episodio richiesta!");
    		return;
    	}
		
		if (episodeToEdit.getDateTo()==null || episodeToEdit.getDateTo().toString().trim().equals("")) {
    		addWarningMessage("Data A su episodio richiesta!");
    		return;
    	}
    	/*
    	if (selectedEpisodeStatusType==null) {
    		addWarningMessage("Stato Puntata richiesto!");
    		return;
    	} 	
		*/
		if (selectedStudio==null) {
    		addWarningMessage("Studio richiesto!");
    		return;
    	} 	
    	
		try {
    		// Imposta il valore della dropdown list nella classe
    		episodeToEdit.setStudio(studioService.getStudioById(selectedStudio));
//    		episodeToEdit.setEpisodeStatus(episodeStatusTypeService.getEpisodeStatusTypeById(selectedEpisodeStatusType));
    		episodeToEdit = episodeService.updateEpisode(episodeToEdit);

    		addInfoMessage(getMessagesBoundle().getString("episode.saved"));
			
		} catch (Exception e1) {
			addErrorMessage(getMessagesBoundle().getString("episode.savingError"), e1);
			e1.printStackTrace();
		}
		finally {
			productionToEdit=productionService.getProductionById(productionToEdit.getId()); 
			episodeToEdit=null;
		}
    	
	}
	
	public void saveProduction(ActionEvent e) {
		
    	if (productionToEdit.getName()==null || productionToEdit.getName().trim().equals("")) {
    		addWarningMessage("test su Nome richiesto!");
    		return;
    	}
    	if (selectedProductionType==null) {
    		addWarningMessage("test su Tipo Produzione richiesto!");
    		return;
    	} 	
    	
//    	System.out.println("Salvo " + productionToEdit.getDistributionList().toString());
    	if (selectedDistributionList==null) {
    		addErrorMessage("Lista di distribuzione richiesta!");
    		return;
    	} 
    	
    	try {
    		//Imposta il valore della dropdown list nella classe
    		productionToEdit.setProductionType(productionTypeService.getProductionTypeById(selectedProductionType));
    		productionToEdit.setAgency(agencyService.getAgencyById(selectedAgency));
    		productionToEdit.setDistributionList(distributionListService.getDistributionListById(selectedDistributionList));
    		productionToEdit.setOwner(getOwner());
	
    		if (productionToEdit.getEpisodes() != null) {
    			
	    		productionToEdit.setEpisodesNum(productionToEdit.getEpisodes().size());
	    		
	    		if(productionToEdit.getSpectatorsReq()==0) {
	    			if (productionToEdit.getEpisodes().size()>0) {
	    				productionToEdit.setSpectatorsReq(productionToEdit.getEpisodes().get(0).getRequestedSpectators().intValue());
	    			}
	    		}else {
	    			for (int iCount=0; iCount<productionToEdit.getEpisodes().size(); ++iCount) {
	    				Episode eDummy = productionToEdit.getEpisodes().get(iCount);
	    				eDummy.setRequestedSpectators((long)productionToEdit.getSpectatorsReq());
	    				productionToEdit.getEpisodes().set(iCount, eDummy);
	    			}
	    		}
	    	}
    		
    		productionToEdit = productionService.updateProduction(productionToEdit);
    		addInfoMessage(getMessagesBoundle().getString("production.saved")); 
    	} catch (PersistenceException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();

    	} catch (DataIntegrityViolationException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();
    	    		
		} catch (Exception e1) {
			addErrorMessage(getMessagesBoundle().getString("production.savingError"), e1);
			e1.printStackTrace();
		} finally {
			productionToEdit=productionService.getProductionById(productionToEdit.getId()); 
	    	
	    	productionToEdit = null;
	    	episodeToEdit=null;
	    	productionEmployeeToEdit=null;
		}
	}
	
	
	public void filterByDate() {
		System.out.println("Filter by date !");
		
		Calendar cal = Calendar.getInstance();
		if (filterFrom!=null) {
			cal.setTime(filterFrom);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 1);
			filterFrom = cal.getTime();
		}
		if (filterTo!=null) {
			cal.setTime(filterTo);
			cal.set(Calendar.HOUR_OF_DAY, 23);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);
			filterTo = cal.getTime();
		}
		
		((LazyProductionDataModel)lazyModelProduction).setFilterFrom(filterFrom);
		((LazyProductionDataModel)lazyModelProduction).setFilterTo(filterTo);
	}
	
	
	public void filterByOwner() {
		System.out.println("Filter by Owner !");

		if (getFilterOwner()!=null) {
			if (isLoggedAnProductionAgency()) {
				((LazyProductionDataModel)lazyModelProduction).setFilterOwner(getFilterOwner());
			} else {
				((LazyProductionDataModel)lazyModelProduction).setFilterAgency(getFilterOwner());
			}
		}
	}
 
	
   public String onFlowProcess(FlowEvent event) {
	   
	   String nextStep="";
	   System.out.println("On onFlow....");
	   
	   if(episodesToSchedule.getEventType().equals(EventType.OCCASIONAL)) { 
		   switch (event.getOldStep()) {
		   	case "eventtype":
		   		nextStep="summarytab";
		   		break;
		   	case "summarytab":
		   		nextStep="eventtype";
		   }		   
       } else {
    	   nextStep=event.getNewStep();
       }  
       return nextStep;
   }
	

   
	public Object getProductionById(long id) {
		return productionService.getProductionById(id);
	}

	public Production getProductionToEdit() {
		return productionToEdit;
	}

	public void setProductionToEdit(Production empToEdit) {
		this.productionToEdit = empToEdit;
	}

	
	public LazyDataModel<Production> getLazyModelProduction() {
		return lazyModelProduction;
	}

	public Object getEpisodeById(long id) {
		return episodeService.getEpisodeById(id);
	}

	public Episode getEpisodeToEdit() {
		return episodeToEdit;
	}

	public void setEpisodeToEdit(Episode epiToEdit) {
		this.episodeToEdit = epiToEdit;
	}	
	
	public Date getFilterFrom() {
		return filterFrom;
	}

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public Date getFilterTo() {
		return filterTo;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}

	public List<SelectItem> getProductionTypes() {
		return this.productionTypes;
	}

	public void setProductionTypes(List<SelectItem> productionTypes) {
		this.productionTypes = productionTypes;
	}

	public Long getSelectedProductionType() {
		return selectedProductionType;
	}

	public void setSelectedProductionType(Long selectedProductionType) {
		this.selectedProductionType = selectedProductionType;
	}

	public Long getSelectedStudio() {
		return selectedStudio;
	}

	public void setSelectedStudio(Long selectedStudio) {
		this.selectedStudio = selectedStudio;
	}

	public List<SelectItem> getStudios() {
		return studios;
	}

	public void setStudios(List<SelectItem> studios) {
		this.studios = studios;
	}

	public EpisodeStatusTypeService getEpisodeStatusTypeService() {
		return episodeStatusTypeService;
	}

	public void setEpisodeStatusTypeService(EpisodeStatusTypeService episodeStatusTypeService) {
		this.episodeStatusTypeService = episodeStatusTypeService;
	}

	public List<SelectItem> getEpisodeStatusTypes() {
		return episodeStatusTypes;
	}

	public void setEpisodeStatusTypes(List<SelectItem> episodeStatusTypes) {
		this.episodeStatusTypes = episodeStatusTypes;
	}

	public Long getSelectedEpisodeStatusType() {
		return selectedEpisodeStatusType;
	}

	public void setSelectedEpisodeStatusType(Long selectedEpisodeStatusType) {
		this.selectedEpisodeStatusType = selectedEpisodeStatusType;
	}

	public Long getProductionToDelete() {
		return productionToDelete;
	}

	public void setProductionToDelete(Long productionToDelete) {
		this.productionToDelete = productionToDelete;
	}

	public Long getEpisodeToDelete() {
		return episodeToDelete;
	} 

	public void setEpisodeToDelete(Long episodeToDelete) {
		this.episodeToDelete = episodeToDelete;
	}

	public EventSchedule getEpisodesToSchedule() {
		return episodesToSchedule;
	}

	public void setEpisodesToSchedule(EventSchedule episodesToSchedul) {
		this.episodesToSchedule = episodesToSchedule;
	}

	public LazyDataModel<Episode> getLazyModelEpisode() {
		return lazyModelEpisode;
	}

	public void setLazyModelEpisode(LazyDataModel<Episode> lazyModelEpisode) {
		this.lazyModelEpisode = lazyModelEpisode;
	}

	public List<SelectItem> getAgencies() {
		return agencies;
	}

	public void setAgencies(List<SelectItem> agencies) {
		this.agencies = agencies;
	}

	public Long getSelectedAgency() {
		return selectedAgency;
	}

	public void setSelectedAgency(Long selectedAgency) {
		this.selectedAgency = selectedAgency;
	}
	
    public void handleWeeklySelected(SelectEvent event) {
    	FrequencyType frequencyType = (FrequencyType)event.getObject();
    	episodesToSchedule.setFrequencyType(frequencyType);
    	setWeeklySelected(frequencyType.equals(FrequencyType.WEEKLY));
    }

    public void handleEventTypeSelected() {
    	System.out.println(this.episodesToSchedule.getEventTypeS()); 
    }
    
    public void handleDurationSelected(SelectEvent event) {
    	DurationType durationType = (DurationType)event.getObject();
    	episodesToSchedule.setDurationType(durationType);
    	System.out.println(durationType.toString());
    	
    }
    
	public boolean isWeeklySelected() {
		return weeklySelected;
	}

	public void setWeeklySelected(boolean weeklySelected) {
		this.weeklySelected = weeklySelected;
	}

	public Long getSelectedStudio4Schedule() {
		return selectedStudio4Schedule;
	}
	
	public String getSelectedStudio4ScheduleLabel() {
		return studioService.getStudioById(selectedStudio4Schedule).getName().toString();
	}

	public void setSelectedStudio4Schedule(Long selectedStudio4Schedule) {
		
		this.selectedStudio4Schedule = selectedStudio4Schedule;
		episodesToSchedule.getEpisode().setStudio(studioService.getStudioById(selectedStudio4Schedule));
	}

	public ProductionEmployee getProductionEmployeeToEdit() {
		return productionEmployeeToEdit;
	}

	public void setProductionEmployeeToEdit(ProductionEmployee employeeToEdit) {
		this.productionEmployeeToEdit = employeeToEdit;
	}

	public Long getProductionEmployeeToDelete() {
		return productionEmployeeToDelete;
	}

	public void setProductionEmployeeToDelete(Long productionEmployeeToDelete) {
		this.productionEmployeeToDelete = productionEmployeeToDelete;
	}

	public List<SelectItem> getProductionEmployees() {
		return productionEmployees;
	}

	public void setProductionEmployees(List<SelectItem> productionEmployees) {
		this.productionEmployees = productionEmployees;
	}

	public Long getSelectedProductionEmployee() {
		return selectedProductionEmployee;
	}

	public void setSelectedProductionEmployee(Long selectedProductionEmployee) {
		this.selectedProductionEmployee = selectedProductionEmployee;
	}

	public int getActiveTab() {
		return activeTab;
	}

	public void setActiveTab(int activeTab) {
		this.activeTab = activeTab;
	}

	public EmployeeService getEmployeeService() {
		return employeeService;
	}

	public void setEmployeeService(EmployeeService employeeService) {
		this.employeeService = employeeService;
	}

	public Agency getFilterOwner() {
		return filterOwner;
	}

	public void setFilterOwner(Agency filterOwner) {
		this.filterOwner = filterOwner;
	}	

	public boolean haveUserRole(String roles) {
		
		String[] listRoles = roles.split(",");
		for (int i = 0; i < listRoles.length; i++) {
			listRoles[i] = listRoles[i].trim();
			
		}
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication!=null && authentication.getAuthorities()!=null) {
			//String currentPrincipalName = authentication.getName();
			for (GrantedAuthority r : authentication.getAuthorities()) {
				//System.out.println("Ruolo:"+r.getAuthority());
				for (int i = 0; i < listRoles.length; i++) {
					if (r.getAuthority()!=null && r.getAuthority().equals(listRoles[i])) {
						//System.out.println("trovato ruolo !!!");
						return true;
					}					
				}
			}
		}
		return false;
	}

	private Agency getOwner() {
		return owner;
	}

	private void setOwner(Agency owner) {
		this.owner = owner;
	}

	public String getWeekDayLabel(DayOfWeek weekday) {
		
		String ret=weeksDecode.get(weekday);
		System.out.println(weekday.toString());
		return ret;
	}
	
	public String getFrequencyTypeLabel(FrequencyType freqType) {
		
		String ret=frequencyDecode.get(freqType);
		System.out.println(freqType.toString());
		return ret;
	}

	public String getStepLabel() {
		String stepLabel=intervalDecode.get(episodesToSchedule.getFrequencyType());
		System.out.println(stepLabel);
		return stepLabel;
	}

    public boolean isRemovableProduction(Production prod) {
    	try {
        	Set<Integer> statusEpisodes = prod.getEpisodes().stream()
        			.map(s -> s.getEpisodeStatus().getIdStatus())
        			.filter(s->!((s==1) || (s==5)))
        			.collect(Collectors.toSet());
        	return !statusEpisodes.isEmpty();
			
		} catch (Exception e) {
			return true;
		}
    }
	
	static public Date addDays(Date date, int days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, days);
				
		return cal.getTime();
	}

	public List<SelectItem> getDistributionLists() {
		return distributionLists;
	}

	public void setDistributionLists(List<SelectItem> distributionLists) {
		this.distributionLists = distributionLists;
	}

	public Long getSelectedDistributionList() {
		return selectedDistributionList;
	}

	public void setSelectedDistributionList(Long selectedDistributionList) {
		this.selectedDistributionList = selectedDistributionList;
	}

//	public StreamedContent getLogo() {
//		return logo;
//	}
	
}
