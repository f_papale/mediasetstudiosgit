package com.crismasecurity.mediasetStudios.web.management;

import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.crismasecurity.mediasetStudios.core.entity.SpectatorEpisode;
import com.crismasecurity.mediasetStudios.core.services.SpectatorEpisodeService;

public class LazySpectatorEpisodeDataModel extends LazyDataModel<SpectatorEpisode>{


	/**
	 * 
	 */
	private static final long serialVersionUID = 6147349003819873492L;
	
	private SpectatorEpisodeService spectatorEpisodeService;
	
	private Long episodeId;
	
	private Long spectatorId;
	
	
	public LazySpectatorEpisodeDataModel(SpectatorEpisodeService sptypeservice) {
		this.spectatorEpisodeService = sptypeservice;
	}
	
	@Override
	public SpectatorEpisode getRowData(String rowKey) {
		if (rowKey==null || rowKey.trim().equals("")) return null;
		return spectatorEpisodeService.getSpectatorEpisodeById(Long.parseLong(rowKey));
	}
	
    @Override
    public Object getRowKey(SpectatorEpisode sptype) {
    	return sptype.getId();
    }
    
    @Override 
    public List<SpectatorEpisode> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters){
    	System.out.println("load....");
    	// Converto ordinamento
    	int sortInt = 0; // UNSORTED
    	if (sortOrder.equals(SortOrder.ASCENDING)) sortInt = 1;
    	if (sortOrder.equals(SortOrder.DESCENDING)) sortInt = 2;
    	
    	if (episodeId != null) filters.put("episode.id", episodeId);
    	
    	if(spectatorId != null)  filters.put("spectator.id", spectatorId);
    	
    	List<SpectatorEpisode> sptypelist = spectatorEpisodeService.getSpectatorEpisodeLazyLoading(first, pageSize, sortField, sortInt, filters, false);
    	this.setRowCount((int)spectatorEpisodeService.getSpectatorEpisodeLazyLoadingCount(filters));
//    	System.out.println("Numero elementi trovati: " + this.getRowCount());    	
    	return sptypelist;
    	
    }

	public Long getEpisodeId() {
		return episodeId;
	}

	public void setEpisodeId(Long episodeId) {
		this.episodeId = episodeId;
	}

	public Long getSpectatorId() {
		return spectatorId;
	}

	public void setSpectatorId(Long spectatorId) {
		this.spectatorId = spectatorId;
	}

}
