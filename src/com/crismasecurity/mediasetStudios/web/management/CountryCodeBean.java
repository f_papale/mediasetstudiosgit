package com.crismasecurity.mediasetStudios.web.management;

import java.util.Date;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.event.ActionEvent;
import javax.persistence.PersistenceException;

import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;


import com.crismasecurity.mediasetStudios.core.entity.CountryCode;
import com.crismasecurity.mediasetStudios.core.services.CountryCodeService;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;

@Controller (CountryCodeBean.BEAN_NAME) 
@Scope("view")
public class CountryCodeBean extends BaseBean {

	private static final long serialVersionUID = -8813866279060221888L;

	public static final String BEAN_NAME = "countryCodeBean";
    public String getBeanName() { return BEAN_NAME; } 


    @Autowired
    private CountryCodeService countryCodeService ;
    
    private LazyDataModel<CountryCode> lazyModelCountryCode;
    private Date filterFrom;
	private Date filterTo;
	
	private CountryCode countryCodeToEdit = null;
    
	public CountryCodeBean() {
		getLog().info("!!!! Costruttore di CountryCodeBean !!!");	
    }

	@PostConstruct
	public void init() {
		getLog().info("INIT CountryCodeBean!");
        if(countryCodeService.getCountryCode().isEmpty() || countryCodeService.getCountryCode() == null) {
        	lazyModelCountryCode = null;
        }else {lazyModelCountryCode = new LazyCountryCodeDataModel(countryCodeService);	}
	}

	@PreDestroy
	public void destroy() {
		getLog().info("Destroy CountryCodeBean!");

	}
	
	private Long countryCodeToDelete = null;
	public void confirmDeleteCountryCode(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("countryCodeId")) {
    			countryCodeToDelete = ((Long)((UIParameter)com).getValue());
			}
		}
    	
	}
	
	public void cancelDelete(ActionEvent ev) {
	    	System.out.println("\nDelete CountryCode canceled.");
	    	countryCodeToDelete= null;
	}
	
	public void delete(ActionEvent ev) {
    	System.out.println("\nDelete Test ...");
    	try {
        	
    		countryCodeService.deleteCountryCode(countryCodeToDelete);
//    		employees=null;
    		addInfoMessage(getMessagesBoundle().getString("countryCode.deleted"));
		} catch (Exception e) {
			e.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("countryCode.deletingError"), e);
		}
	}
	
	public void addCountryCode(ActionEvent e) {
		countryCodeToEdit = new CountryCode();
	}
	
	public void modifyCountryCode(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("countryCodeId")) {
    			countryCodeToEdit = countryCodeService.getCountryCodeById((Long)((UIParameter)com).getValue());
			}
		}
	}
	
	public void cancelEditCountryCode(ActionEvent e) {
		countryCodeToEdit = null;
	}
	
	public void saveCountryCode(ActionEvent e) {
    	System.out.println("Salvo " + countryCodeToEdit);
    	

    	if (countryCodeToEdit.getCountryCode()==null || countryCodeToEdit.getCountryCode().trim().equals("")) {
    		addWarningMessage("test su Descrizione richiesto!");
    		return;
    	}
    	
    	
    	try {
    		countryCodeToEdit = countryCodeService.updateCountryCode(countryCodeToEdit);		
			addInfoMessage(getMessagesBoundle().getString("countryCode.saved"));
    	} catch (PersistenceException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();

    	} catch (DataIntegrityViolationException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();
		} catch (Exception e1) {
			addErrorMessage(getMessagesBoundle().getString("countryCode.savingError"), e1);
			e1.printStackTrace();
		}
    	countryCodeToEdit = null;
	}
	
	public Object getCountryCodeById(long id) {
		return countryCodeService.getCountryCodeById(id);
	}

	public CountryCode getCountryCodeToEdit() {
		return countryCodeToEdit;
	}

	public void setCountryCodeToEdit(CountryCode empToEdit) {
		this.countryCodeToEdit = empToEdit;
	}

	public LazyDataModel<CountryCode> getLazyModelCountryCode() {
		return lazyModelCountryCode;
	}

	/*public Date getFilterFrom() {
		return filterFrom;
	}

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public Date getFilterTo() {
		return filterTo;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}*/

}
