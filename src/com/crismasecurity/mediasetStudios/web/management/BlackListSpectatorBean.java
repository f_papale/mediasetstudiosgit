package com.crismasecurity.mediasetStudios.web.management;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.persistence.PersistenceException;

import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;

import com.crismasecurity.mediasetStudios.core.entity.Audit;
import com.crismasecurity.mediasetStudios.core.entity.BlackListSpectator;
import com.crismasecurity.mediasetStudios.core.entity.BlackListType;
import com.crismasecurity.mediasetStudios.core.entity.Spectator;
import com.crismasecurity.mediasetStudios.core.security.services.AppUserService;
import com.crismasecurity.mediasetStudios.core.security.vos.security.AppUserVO;
import com.crismasecurity.mediasetStudios.core.services.AuditService;
import com.crismasecurity.mediasetStudios.core.services.BlackListSpectatorService;
import com.crismasecurity.mediasetStudios.core.services.BlackListTypeService;
import com.crismasecurity.mediasetStudios.core.services.SpectatorService;
import com.crismasecurity.mediasetStudios.web.NavigationBean;
import com.crismasecurity.mediasetStudios.web.utils.MakeHTML;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;

//Spring managed
@Controller (BlackListSpectatorBean.BEAN_NAME) 
//@Scope("session")
@Scope("view")
public class BlackListSpectatorBean extends BaseBean{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5662158989060606869L;
	
	public static final String BEAN_NAME = "blackListSpectatorBean";
	public String getBeanName() { return BEAN_NAME; } 

	@Autowired
	private BlackListSpectatorService blackListSpectatorService;
	
	@Autowired
	private BlackListTypeService blackListTypeService;
	
	@Autowired
	private SpectatorService spectatorService;
	
    @Autowired
    private NavigationBean navigationBean;
    
	@Autowired
	private AppUserService userService;
	
	@Autowired
	private AuditService auditService;

	private LazyDataModel<BlackListSpectator> lazyModelBlackListSpectator;
	private BlackListSpectator blackListSpectatorToEdit = null;
	private Long blackListSpectatorToDelete = null;
	private List<SelectItem> blackListType;
	private String name = "";
	private Long selectedBlackListType = null;
	
	public BlackListSpectatorBean() {
		getLog().info("!!!! Costruttore di BlackListTypeBean !!!");
	}
	
	@PostConstruct
	public void init() {
		getLog().info("INIT BlackListSpectatorBean!");
		lazyModelBlackListSpectator = new LazyBlackListSpectatorDataModel(blackListSpectatorService);
		
		presetBlacklistType();
		
	}
	
	@PreDestroy
	public void destroy() {
		getLog().info("Destroy BlackListSpectatorBean!");
	}
	
	public void confirmDeleteBlackListSpectator(ActionEvent e) {
  	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
  	for (UIComponent com : component.getChildren()) {
  		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("blackListSpectatorId")) {
  			blackListSpectatorToDelete = ((Long)((UIParameter)com).getValue());
			}
		}    	
	}
	
	public void cancelDelete(ActionEvent ev) {
		System.out.println("\nDelete BlackListType canceled.");
		blackListSpectatorToDelete = null;
	}
	
	public void delete(ActionEvent ev) {
  	
  	try {
  		
  		BlackListSpectator bl = blackListSpectatorService.getBlackListSpectatorById(blackListSpectatorToDelete);
  		bl.setIncident(null);
  		//blackListSpectatorService.deleteBlackListSpectator(blackListSpectatorToDelete);
  		blackListSpectatorService.deleteBlackListSpectator(bl.getIdBlackListSpectator());
  		addInfoMessage(getMessagesBoundle().getString("blackListSpectator.deleted"));
		} catch (Exception e) {
			e.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("blackListSpectator.deletingError"), e);
		}
	}
	
	public void addBlackListSpectator(ActionEvent e) {
		blackListSpectatorToEdit = new BlackListSpectator();
	}
	
	public List<Spectator> completeSpectatorCode(String query) {

		List<Spectator> filteredSpectator = spectatorService.getSpectatorFinder().and("fullName").like(query).addOrderAsc("fullName").setDistinct().list();
		return filteredSpectator;
	}
	
	public void modifyBlackListSpectator(ActionEvent e) {
  	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
  	for (UIComponent com : component.getChildren()) {
  		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("blackListSpectatorId")) {
  			blackListSpectatorToEdit = blackListSpectatorService.getBlackListSpectatorById((Long)((UIParameter)com).getValue());
  			if (blackListSpectatorToEdit.getBlackListType()!=null) selectedBlackListType = blackListSpectatorToEdit.getBlackListType().getId();
  			else selectedBlackListType=null;
  			}
		}
	}
	
	public void cancelEditBlackListSpectator(ActionEvent e) {
		blackListSpectatorToEdit = null;
	}
	
	public void saveBlackListSpectator(ActionEvent e) {
		
		System.out.println("Salvo " + blackListSpectatorToEdit);
		/*if (blackListSpectatorToEdit.getReason()==null || blackListSpectatorToEdit.getReason().trim().equals("")) {
	  		addWarningMessage("test su Descrizione richiesto!");
	  		return;
	  	}*/
		
		
		if (selectedBlackListType!=null) blackListSpectatorToEdit.setBlackListType(blackListTypeService.getBlackListTypeById(selectedBlackListType));
		else blackListSpectatorToEdit.setBlackListType(null);
		
	  	try {
	  		blackListSpectatorToEdit = blackListSpectatorService.updateBlackListSpectator(blackListSpectatorToEdit);
			
			String newHtml = MakeHTML.makeHTML(blackListSpectatorToEdit);
			AppUserVO uu = userService.getUserByLogin(navigationBean.getUserName());
			auditService.addAudit(new Audit(uu.getLastName() + " " + uu.getFirstName()+" ("+navigationBean.getUserName()+")", "Insert", "BlackListSpectator", blackListSpectatorToEdit.getIdBlackListSpectator(), "", "", null, newHtml));

	  		addInfoMessage(getMessagesBoundle().getString("blackListSpectator.saved"));
	    	} catch (PersistenceException hibernateEx){
	    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
	    		hibernateEx.printStackTrace();
	
	    	} catch (DataIntegrityViolationException hibernateEx){
	    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
	    		hibernateEx.printStackTrace();
    		  		
			} catch (Exception e1) {
				addErrorMessage(getMessagesBoundle().getString("blackListSpectator.savingError"), e1);
				e1.printStackTrace();
			}
	  	blackListSpectatorToEdit = null;
	}
	
	
	private void presetBlacklistType() {
		blackListType = new ArrayList<SelectItem>();
		List<BlackListType> blTypeList = blackListTypeService.getBlackListType();
		blackListType.add(new SelectItem(null, " "));
		for (BlackListType blType : blTypeList) {
			SelectItem xx = new SelectItem(blType.getId(), blType.getDescription());
			blackListType.add(xx);
		}
	}
	
	public BlackListSpectatorService getBlackListSpectatorService() {
		return blackListSpectatorService;
	}

	public void setBlackListSpectatorService(BlackListSpectatorService blackListSpectatorService) {
		this.blackListSpectatorService = blackListSpectatorService;
	}

	public LazyDataModel<BlackListSpectator> getLazyModelBlackListSpectator() {
		return lazyModelBlackListSpectator;
	}

	public void setLazyModelBlackListSpectator(LazyDataModel<BlackListSpectator> lazyModelBlackListSpectator) {
		this.lazyModelBlackListSpectator = lazyModelBlackListSpectator;
	}

	public BlackListSpectator getBlackListSpectatorToEdit() {
		return blackListSpectatorToEdit;
	}

	public void setBlackListSpectatorToEdit(BlackListSpectator blackListSpectatorToEdit) {
		this.blackListSpectatorToEdit = blackListSpectatorToEdit;
	}

	public Long getBlackListSpectatorToDelete() {
		return blackListSpectatorToDelete;
	}

	public void setBlackListSpectatorToDelete(Long blackListSpectatorToDelete) {
		this.blackListSpectatorToDelete = blackListSpectatorToDelete;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<SelectItem> getBlackListType() {
		return blackListType;
	}

	public void setBlackListType(List<SelectItem> blackListType) {
		this.blackListType = blackListType;
	}

	public Long getSelectedBlackListType() {
		return selectedBlackListType;
	}

	public void setSelectedBlackListType(Long selectedBlackListType) {
		this.selectedBlackListType = selectedBlackListType;
	}
	
	public Object getSpectatorById(long id) {
		return spectatorService.getSpectatorById(id);
	}
	
}