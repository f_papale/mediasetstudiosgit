package com.crismasecurity.mediasetStudios.web.management;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.crismasecurity.mediasetStudios.core.entity.Spectator;
import com.crismasecurity.mediasetStudios.core.entity.SpectatorType;
import com.crismasecurity.mediasetStudios.core.services.SpectatorService;
import com.crismasecurity.mediasetStudios.core.services.SpectatorTypeService;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;

//Spring managed
@Controller (SpectatorBean.BEAN_NAME)
@Scope("view")
public class SpectatorBean extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5225477109754594591L;
	
	public static final String BEAN_NAME = "spectatorBean";
    public String getBeanName() { return BEAN_NAME; } 
    
    @Autowired
    private SpectatorService spectatorService;
    
    @Autowired
    private SpectatorTypeService spectatorTypeService;
    
    private LazyDataModel<Spectator> lazyModelSpectator;
    
    private List<SelectItem> spectatorTypes;
    private Long selectedSpectatorType = null;
    
    private Spectator spectatorToEdit = null;
    
    private Long spectatorToDelete = null; 
    
    public SpectatorBean() {
    	getLog().info("!!!! Costruttore di SpectatorBean !!!");
    }
    
	@PostConstruct
	public void init() {
		getLog().info("INIT SpectatorBean!");
		getLog().info("getSpectator!\n");
		System.out.println(spectatorService.getSpectator());
    	presetSpectatorList();
    	if(spectatorService.getSpectator().isEmpty() || spectatorService.getSpectator() == null) {
    		lazyModelSpectator = null;
    	}else {
		lazyModelSpectator = new LazySpectatorDataModel(spectatorService);
    	}

		}
	
	@PreDestroy
	public void destroy() {
		getLog().info("Destroy SpectatorBean!");

	}
	private void presetSpectatorList(){
		spectatorTypes = new ArrayList<SelectItem>();
		List<SpectatorType> sptypelist = spectatorTypeService.getSpectatorType();
		spectatorTypes.add(new SelectItem(null, " "));
		for(SpectatorType sptype : sptypelist) {
			SelectItem xx = new SelectItem(sptype.getId(), sptype.getDescription());
			spectatorTypes.add(xx);
		}
	}
	
	public void confirmDeleteSpectator(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("spectatorId")) {
    			spectatorToDelete = ((Long)((UIParameter)com).getValue());
			}
		}
    	
	}
	
	public void cancelDelete(ActionEvent ev) {
    	System.out.println("\nDelete Spectator canceled.");
    	spectatorToDelete= null;
	}
	
	public void delete(ActionEvent ev) {
    	System.out.println("\nDelete Test ...");
    	try {
        	
    		spectatorService.deleteSpectator(spectatorToDelete);
    		addInfoMessage(getMessagesBoundle().getString("spectator.deleted"));
		} catch (Exception e) {
			e.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("spectator.deletingError"), e);
		}
	}
	
	public void addSpectator(ActionEvent e) {
		spectatorToEdit = new Spectator();
		selectedSpectatorType = null;
	}
	
	public void modifySpectator(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("spectatorId")) {
    			spectatorToEdit = spectatorService.getSpectatorById((Long)((UIParameter)com).getValue());
    			selectedSpectatorType=spectatorToEdit.getSpectatorType().getId();
    			
			}
		}
	}
	
	public void cancelEditSpectator(ActionEvent e) {
		spectatorToEdit = null;
	}
	
	public void saveSpectator(ActionEvent e) {
    	System.out.println("Salvo " + spectatorToEdit);
    	System.out.println("descrizione azione " + e);

    	if (spectatorToEdit.getName()== null || spectatorToEdit.getName().trim().equals("")) {
    		addWarningMessage("test su Descrizione richiesto!");
    		return;
    	}
    	
    	if (selectedSpectatorType==null) {
    		addWarningMessage("test su Tipo Spettatore richiesto!");
    		return;
    	} 	
    	
    	try {
    		//Imposta il valore della dropdown list nella classe
    		spectatorToEdit.setSpectatorType(spectatorTypeService.getSpectatorTypeById(selectedSpectatorType));    		
    		spectatorToEdit = spectatorService.updateSpectator(spectatorToEdit);
		
			addInfoMessage(getMessagesBoundle().getString("spectator.saved"));
		} catch (Exception e1) {
			addErrorMessage(getMessagesBoundle().getString("spectator.savingError"), e1);
			e1.printStackTrace();
		}
    	spectatorToEdit = null;
	}

	public Spectator getSpectatorToEdit() {
		return spectatorToEdit;
	}

	public void setSpectatorToEdit(Spectator spectatorToEdit) {
		this.spectatorToEdit = spectatorToEdit;
	}
	
	public Object getSpectatorById(long id) {
		return spectatorService.getSpectatorById(id);
	}

	public LazyDataModel<Spectator> getLazyModelSpectator() {
		return lazyModelSpectator;
	}

	public List<SelectItem> getSpectatorTypes() {
		return spectatorTypes;
	}

	public void setSpectatorTypes(List<SelectItem> spectatorTypes) {
		this.spectatorTypes = spectatorTypes;
	}

	public Long getSelectedSpectatorType() {
		return selectedSpectatorType;
	}

	public void setSelectedSpectatorType(Long selectedSpectatorType) {
		this.selectedSpectatorType = selectedSpectatorType;
	}

	
}
