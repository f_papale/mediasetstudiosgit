package com.crismasecurity.mediasetStudios.web.management;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.persistence.PersistenceException;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import com.crismasecurity.mediasetStudios.core.entity.Agency;
import com.crismasecurity.mediasetStudios.core.entity.AgencyUser;
import com.crismasecurity.mediasetStudios.core.entity.Episode;
import com.crismasecurity.mediasetStudios.core.entity.EpisodeStatusType;
import com.crismasecurity.mediasetStudios.core.entity.Production;
import com.crismasecurity.mediasetStudios.core.entity.SpectatorEpisode;
import com.crismasecurity.mediasetStudios.core.entity.Studio;
import com.crismasecurity.mediasetStudios.core.security.services.AppUserService;
import com.crismasecurity.mediasetStudios.core.services.AgencyService;
import com.crismasecurity.mediasetStudios.core.services.AgencyUserService;
import com.crismasecurity.mediasetStudios.core.services.EpisodeService;
import com.crismasecurity.mediasetStudios.core.services.EpisodeStatusTypeService;
import com.crismasecurity.mediasetStudios.core.services.ProductionService;
import com.crismasecurity.mediasetStudios.core.services.StudioService;
import com.crismasecurity.mediasetStudios.web.LoginBean;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;

//@ManagedBean(name= EmployeesBean.BEAN_NAME) // JSF managed
//@SessionScoped // JSF managed
//@ViewScoped // JSF managed

//Spring managed
@Controller (EpisodeBean.BEAN_NAME) 
//@Scope("session")
@Scope("view")
public class EpisodeBean extends BaseBean {


	private static final long serialVersionUID = 5278743136213989941L;

	public static final String BEAN_NAME = "episodeBean";
    public String getBeanName() { return BEAN_NAME; } 


    @Autowired
    private EpisodeService episodeService ;
    
    @Autowired
    private EpisodeStatusTypeService episodeStatusTypeService;
    
    @Autowired
    private ProductionService productionService;
    
    @Autowired
    private StudioService studioService;
    
	@Autowired
	private AppUserService userService;
	
	@Autowired
	private AgencyUserService agencyUserService;

	@Autowired
	private AgencyService agencyService;
	
	@Autowired
	private LoginBean loginBean;
    
    private LazyDataModel<Episode> lazyModelEpisode;
    private Date filterFrom;
	private Date filterTo;
	private AgencyUser currentAgencyUser = null;
	private List<SelectItem> episodeStatusTypes;
	private Long selectedEpisodeStatusType = null;
	private List<SelectItem> productions;
	private Long selectedProduction = null;
	private List<SelectItem> studios;
	private Long selectedStudio = null;	
	private List<SelectItem> agencies = null;
	private Long selectedAgency = null;	
	private Episode episodeToEdit = null;
	private Long episodeToDelete = null;
    
	public EpisodeBean() {
		getLog().info("!!!! Costruttore di EpisodeBean !!!");	
    }


	
	
	@PostConstruct
	public void init() {
		getLog().info("INIT EpisodeBean!");
		setLazyModelEpisode(new LazyEpisodeDataModel(episodeService));
		presetCurrentUser();
		presetAgencyList();
		presetEpisodeStatusTypeList();
		presetProduzioneList();
		presetStudioList();
		
//		Solo ruolo Agenzia
		if ((haveUserRole("ROLE_AGENCY") &&(!(haveUserRole("ROLE_PRODUCTION"))))) {
//			setLazyModelEpisode(new LazyEpisodeDataModel(episodeService));
			((LazyEpisodeDataModel)getLazyModelEpisode()).setAgencyId(currentAgencyUser.getAgency().getIdAgency());
//			Ruolo Agenzia e Produzione o Amministrazione Sistema
		} else if (((haveUserRole("ROLE_AGENCY") && (haveUserRole("ROLE_PRODUCTION")))) || (haveUserRole("ROLE_STADMIN")) ) {
			if (isLoggedAnProductionAgency()) {
				((LazyEpisodeDataModel)getLazyModelEpisode()).setOwnerId(currentAgencyUser.getAgency().getIdAgency());				
			}else {
				((LazyEpisodeDataModel)getLazyModelEpisode()).setAgencyId(currentAgencyUser.getAgency().getIdAgency());				
			}
//			setLazyModelEpisode(new LazyEpisodeDataModel(episodeService));

//			Ruoli Amministratore, Sicurezza e Vigilanza
		} else if (haveUserRole("ROLE_ADMIN")|| haveUserRole("ROLE_SECURITY") || haveUserRole("ROLE_SUPERVISION") ) {
//			setLazyModelEpisode(new LazyEpisodeDataModel(episodeService));
		} else {
			setLazyModelEpisode(null);
		}			
	}

	@PreDestroy
	public void destroy() {
		getLog().info("Destroy EpisodeBean!");
	}

	public boolean haveUserRole(String roles) {
		
		String[] listRoles = roles.split(",");
		for (int i = 0; i < listRoles.length; i++) {
			listRoles[i] = listRoles[i].trim();
			
		}
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication!=null && authentication.getAuthorities()!=null) {
			//String currentPrincipalName = authentication.getName();
			for (GrantedAuthority r : authentication.getAuthorities()) {
				//System.out.println("Ruolo:"+r.getAuthority());
				for (int i = 0; i < listRoles.length; i++) {
					if (r.getAuthority()!=null && r.getAuthority().equals(listRoles[i])) {
						//System.out.println("trovato ruolo !!!");
						return true;
					}					
				}
			}
		}
		return false;
	}
	
	private void presetCurrentUser() {

		try {
			long userId = userService.getUserByLogin(loginBean.getUserId()).getId();
			setCurrentAgencyUser(agencyUserService.getAgencyUserFinder().and("user.id").eq(userId).and("flgCurrentSelected").eq(true).result());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Errore Preset Current User");
			addErrorMessage(getMessagesBoundle().getString("spectatorEpisode.presetCurrentUser"), e);
		}
	}
	
	
	private Long getIdLoggedAgency() {
		if (currentAgencyUser!=null) {
			return Long.valueOf(currentAgencyUser.getAgency().getIdAgency());	
		}else {
			return null;
		}
	}

	
	public boolean isLoggedAnProductionAgency() {
		boolean bReturn;
		try {
			bReturn=currentAgencyUser.getAgency().getAgencyType().isFlgProduction();
		} catch (Exception e) {
			bReturn=false;
		}
		return 	bReturn;			
	}
	
	
	private void presetAgencyList() {
		agencies = new ArrayList<SelectItem>();
		List<Agency> aglist = new ArrayList<Agency>();
		if (haveUserRole("ROLE_ADMIN") || haveUserRole("ROLE_SECURITY") || haveUserRole("ROLE_SUPERVISION") ) {
			aglist = agencyService.getAgency();
		} else if (haveUserRole("ROLE_AGENCY") || haveUserRole("ROLE_STADMIN"))   {
			if (getIdLoggedAgency()!=null) {
				aglist = agencyService.getAgencyFinder().and("idAgency").eq(getIdLoggedAgency()).setDistinct().list();	
			}
		} else		
			agencies.add(new SelectItem(null, " "));
		
		for (Agency ag : aglist) {
			if (ag!=null) {
				SelectItem xx = new SelectItem(ag.getIdAgency(), ag.getName());
				agencies.add(xx);				
			}
		}
		// se l'utente loggato è un agenzia, impostiamo l'agenzia di default al
		// caricamento della maschera
		if (haveUserRole("ROLE_AGENCY") || haveUserRole("ROLE_STADMIN")) {
			selectedAgency = getIdLoggedAgency();
		}
	}
	
	private void presetEpisodeStatusTypeList() {
		episodeStatusTypes = new ArrayList<SelectItem>();
		List<EpisodeStatusType> conts = episodeStatusTypeService.getEpisodeStatusType();
		episodeStatusTypes.add(new SelectItem(null, " "));
		for (EpisodeStatusType cont : conts) {
			SelectItem xx = new SelectItem(cont.getId(), cont.getDescription());
			episodeStatusTypes.add(xx);
		}
	}

	
	private void presetProduzioneList() {
		setProductions(new ArrayList<SelectItem>());
		Set<Production> set = new HashSet<Production>();
		if ((haveUserRole("ROLE_AGENCY") && (!(haveUserRole("ROLE_PRODUCTION"))))) {
			set.addAll(productionService.getProductionFinder().and("agency.idAgency").eq(getIdLoggedAgency()).setDistinct().list());	
		} else if ((haveUserRole("ROLE_AGENCY") && (haveUserRole("ROLE_PRODUCTION"))) || (haveUserRole("ROLE_STADMIN"))) {
			if (isLoggedAnProductionAgency()) {
				set.addAll(productionService.getProductionFinder().and("owner.idAgency").eq(getIdLoggedAgency()).setDistinct().list());				
			}else {
				set.addAll(productionService.getProductionFinder().and("agency.idAgency").eq(getIdLoggedAgency()).setDistinct().list());				
			}

		}else if (haveUserRole("ROLE_ADMIN")) {
			set.addAll(productionService.getProductionFinder().setDistinct().list());
		}
		
		List<Production> productions = new ArrayList<Production>(set);
    	Comparator<Production> compareBySeasonName  = (Production p1, Production p2) -> p1.compareBySeasonName(p2);
    	Collections.sort(productions, compareBySeasonName.reversed());	 
    	
		getProductions().add(new SelectItem(null, " "));
		for (Production cont : productions) {
			SelectItem xx = new SelectItem(cont.getId(), cont.getSeason() + " - " + cont.getName());
			getProductions().add(xx);
		}
	}
	
	private void presetStudioList() {
		setStudios(new ArrayList<SelectItem>());
		List<Studio> conts = studioService.getStudio();
		getStudios().add(new SelectItem(null, " "));
		for (Studio cont : conts) {
			SelectItem xx = new SelectItem(cont.getId(), "("+cont.getProductionCenter().getName()+") - "+cont.getName());
			getStudios().add(xx);
		}
	}

	public void confirmDeleteEpisode(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("episodeId")) {
    			episodeToDelete = ((Long)((UIParameter)com).getValue());
			}
		}
	}

	public void cancelDelete(ActionEvent ev) {
    	System.out.println("\nDelete Episode canceled.");
    	episodeToDelete= null;
	}

	public void delete(ActionEvent ev) {
    	System.out.println("\nDelete Test ...");
    	try {
    		episodeService.deleteEpisode(episodeToDelete);
    		addInfoMessage(getMessagesBoundle().getString("episode.deleted"));
		} catch (Exception e) {
			e.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("episode.deletingError"), e);
		}
	}
	
	public void cloneEpisode(ActionEvent ev) {
    	System.out.println("\nClone Test ...");
		HtmlCommandLink component = (HtmlCommandLink)ev.getSource();
		for (UIComponent com : component.getChildren()) {
			if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("episodeId")) {
				episodeToEdit = new Episode();
				try {
					Episode episodeToClone= episodeService.getEpisodeById((Long)((UIParameter)com).getValue());
					episodeToEdit.setId(null);
		    		episodeToEdit.setDateFrom(episodeToClone.getDateTo());
		    		episodeToEdit.setDateTo(episodeToClone.getDateTo());
		    		episodeToEdit.setEpisodeStatus(episodeStatusTypeService.getEpisodeStatusTypeByIdStatus((episodeToClone.getSpectators().size()==0?1:2)));	    		
		    		episodeToEdit.setProduction(episodeToClone.getProduction());
		    		episodeToEdit.setStudio(episodeToClone.getStudio());
		    		episodeToEdit.setRequestedSpectators(episodeToClone.getRequestedSpectators());
		    		episodeToEdit.setEffectiveSpectators(0L);
		    		List<SpectatorEpisode> lstSpectators = new ArrayList<SpectatorEpisode>();
					for (SpectatorEpisode spectatorEpisodeTempl : episodeToClone.getSpectators()) {
						SpectatorEpisode spectatorEpisode = new SpectatorEpisode();
						spectatorEpisode.setDescription(spectatorEpisodeTempl.getDescription());
						spectatorEpisode.setEpisode(episodeToEdit);
						spectatorEpisode.setProduction(spectatorEpisodeTempl.getProduction());
						spectatorEpisode.setSpectator(spectatorEpisodeTempl.getSpectator());
						spectatorEpisode.setVip(spectatorEpisodeTempl.isVip());
						lstSpectators.add(spectatorEpisode);
					}
					episodeToEdit.setSpectators(lstSpectators);
					setSelectedEpisodeStatusType(episodeToEdit.getEpisodeStatus().getId());
					setSelectedProduction(episodeToEdit.getProduction().getId());
					setSelectedStudio(episodeToEdit.getStudio().getId());
					
				} catch (Exception e) {
					episodeToEdit=null;
					addErrorMessage("test su Stato Episodio richiesto!");
				}
			}
		}
	}
	
	public void addEpisode(ActionEvent e) {
		episodeToEdit = new Episode();
		setSelectedEpisodeStatusType(null);
		setSelectedProduction(null);
		setSelectedStudio(null);
	}
	
	public void modifyEpisode(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("episodeId")) {
    			episodeToEdit = episodeService.getEpisodeById((Long)((UIParameter)com).getValue());
    			setSelectedEpisodeStatusType(episodeToEdit.getEpisodeStatus().getId());
    			setSelectedProduction(episodeToEdit.getProduction().getId());
    			setSelectedStudio(episodeToEdit.getStudio().getId());
			}
		}
	}
	
	public void cancelEditEpisode(ActionEvent e) {
		episodeToEdit = null;
	}
	
	public void saveEpisode(ActionEvent e) {
    	System.out.println("Salvo " + episodeToEdit);
    	

    	if (episodeToEdit.getDateFrom()==null || episodeToEdit.getDateFrom().toString().trim().equals("")) {
    		addWarningMessage("test su DataDa richiesto!");
    		return;
    	}

    	if (getSelectedEpisodeStatusType()==null) {
    		if (episodeToEdit.getId()==null) {
    			setSelectedEpisodeStatusType(1L);
    			episodeToEdit.setEffectiveSpectators(0L);
    		}else {
        		addWarningMessage("test su Stato Episodio richiesto!");
        		return;    			
    		}
    	} 	
 
    	if (getSelectedStudio()==null) {
    		addWarningMessage("test su Studio richiesto!");
    		return;
    	} 
    	
    	if (getSelectedProduction()==null) {
    		addWarningMessage("test su Produzione richiesto!");
    		return;
    	} 
    	
    	try {
    		//Imposta il valore della dropdown list nella classe
    		
    		episodeToEdit.setEpisodeStatus(episodeStatusTypeService.getEpisodeStatusTypeById(getSelectedEpisodeStatusType()));
    		episodeToEdit.setProduction(productionService.getProductionById(getSelectedProduction()));
    		episodeToEdit.setStudio(studioService.getStudioById(getSelectedStudio()));   		
    		episodeToEdit = episodeService.updateEpisode(episodeToEdit);		
			addInfoMessage(getMessagesBoundle().getString("episode.saved"));
    	} catch (PersistenceException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();
    	} catch (DataIntegrityViolationException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();			
		} catch (Exception e1) {
			addErrorMessage(getMessagesBoundle().getString("episode.savingError"), e1);
			e1.printStackTrace();
		}
    	episodeToEdit = null;
	}
	
	public void onDateFromChanged(SelectEvent sevt) {
		Date datefrom = (Date)sevt.getObject();
		if (episodeToEdit.getDateTo()==null) episodeToEdit.setDateTo(datefrom); 
		if(datefrom.compareTo(episodeToEdit.getDateTo()) > 0) {
			episodeToEdit.setDateTo(datefrom);
		}
	}
	
	public void filterByDate() {
		System.out.println("Filter by date !");
		
		Calendar cal = Calendar.getInstance();
		if (filterFrom!=null) {
			cal.setTime(filterFrom);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 1);
			filterFrom = cal.getTime();
		}
		if (filterTo!=null) {
			cal.setTime(filterTo);
			cal.set(Calendar.HOUR_OF_DAY, 23);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);
			filterTo = cal.getTime();
		}
		
		((LazyEpisodeDataModel)getLazyModelEpisode()).setFilterFrom(filterFrom);
		((LazyEpisodeDataModel)getLazyModelEpisode()).setFilterTo(filterTo);
	}
	
	public Object getEpisodeById(long id) {
		return episodeService.getEpisodeById(id);
	}

	public Episode getEpisodeToEdit() {
		return episodeToEdit;
	}

	public void setEpisodeToEdit(Episode empToEdit) {
		this.episodeToEdit = empToEdit;
	}

	public LazyDataModel<Episode> getLazyModelEpisode() {
		return lazyModelEpisode;
	}

	public Date getFilterFrom() {
		return filterFrom;
	}

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public Date getFilterTo() {
		return filterTo;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}

	public List<SelectItem> getEpisodeStatusTypes() {
		return this.episodeStatusTypes;
	}

	public void setEpisodeStatusTypes(List<SelectItem> episodeStatusTypes) {
		this.episodeStatusTypes = episodeStatusTypes;
	}

	public List<SelectItem> getProductions() {
		return productions;
	}

	public void setProductions(List<SelectItem> productions) {
		this.productions = productions;
	}

	public Long getSelectedProduction() {
		return selectedProduction;
	}

	public void setSelectedProduction(Long selectedProduction) {
		this.selectedProduction = selectedProduction;
	}

	public List<SelectItem> getStudios() {
		return studios;
	}

	public void setStudios(List<SelectItem> studios) {
		this.studios = studios;
	}

	public Long getSelectedStudio() {
		return selectedStudio;
	}

	public void setSelectedStudio(Long selectedStudio) {
		this.selectedStudio = selectedStudio;
	}

	public Long getSelectedEpisodeStatusType() {
		return selectedEpisodeStatusType;
	}

	public void setSelectedEpisodeStatusType(Long selectedEpisodeStatusType) {
		this.selectedEpisodeStatusType = selectedEpisodeStatusType;
	}

	public Long getSelectedAgency() {
		return selectedAgency;
	}

	public void setSelectedAgency(Long selectedAgency) {
		this.selectedAgency = selectedAgency;
	}

	public List<SelectItem> getAgencies() {
		return agencies;
	}

	public void setAgencies(List<SelectItem> agencies) {
		this.agencies = agencies;
	}

	public AgencyUser getCurrentAgencyUser() {
		return currentAgencyUser;
	}

	public void setCurrentAgencyUser(AgencyUser currentAgencyUser) {
		this.currentAgencyUser = currentAgencyUser;
	}

	private void setLazyModelEpisode(LazyDataModel<Episode> lazyModelEpisode) {
		this.lazyModelEpisode = lazyModelEpisode;
	}
}
