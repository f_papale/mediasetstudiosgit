package com.crismasecurity.mediasetStudios.web.management;

import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.crismasecurity.mediasetStudios.core.entity.IncidentType;
import com.crismasecurity.mediasetStudios.core.services.IncidentTypeService;

public class LazyIncidentTypeDataModel extends LazyDataModel<IncidentType>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2960910410611093828L;
	
	private IncidentTypeService incidentTypeService;
	
	public LazyIncidentTypeDataModel(IncidentTypeService incidentTypeService) {
		this.incidentTypeService = incidentTypeService;
	}
	
	@Override
	public IncidentType getRowData(String rowKey) {
		if (rowKey==null || rowKey.trim().equals("")) return null;
		return incidentTypeService.getIncidentTypeById(Long.parseLong(rowKey));
	}
	
    @Override
    public Object getRowKey(IncidentType inType) {
    	return inType.getId();
    }
    
    @Override 
    public List<IncidentType> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters){
    	// Converto ordinamento
    	int sortInt = 0; // UNSORTED
    	if (sortOrder.equals(SortOrder.ASCENDING)) sortInt = 1;
    	if (sortOrder.equals(SortOrder.DESCENDING)) sortInt = 2;
    	
    	List<IncidentType> sptypelist = incidentTypeService.getIncidentTypeLazyLoading(first, pageSize, sortField, sortInt, filters);
    	this.setRowCount((int)incidentTypeService.getIncidentTypeLazyLoadingCount(filters));
//    	System.out.println("Numero elementi trovati: " + this.getRowCount());    	
    	return sptypelist;
    	
    }

}
