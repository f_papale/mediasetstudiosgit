package com.crismasecurity.mediasetStudios.web.management;

import java.util.Calendar;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.event.ActionEvent;
import javax.persistence.PersistenceException;

import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;


import com.crismasecurity.mediasetStudios.core.entity.MediaType;
import com.crismasecurity.mediasetStudios.core.services.MediaTypeService;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;

//@ManagedBean(name= EmployeesBean.BEAN_NAME) // JSF managed
//@SessionScoped // JSF managed
//@ViewScoped // JSF managed

//Spring managed
@Controller (MediaTypeBean.BEAN_NAME) 
//@Scope("session")
@Scope("view")
public class MediaTypeBean extends BaseBean {

	private static final long serialVersionUID = -6065141989753335782L;

	public static final String BEAN_NAME = "mediaTypeBean";
    public String getBeanName() { return BEAN_NAME; } 


    @Autowired
    private MediaTypeService mediaTypeService ;
    
    private LazyDataModel<MediaType> lazyModelMediaType;
    private Date filterFrom;
	private Date filterTo;
	
	private MediaType mediaTypeToEdit = null;
    
	public MediaTypeBean() {
		getLog().info("!!!! Costruttore di MediaTypeBean !!!");	
    }

	@PostConstruct
	public void init() {
		getLog().info("INIT MediaTypeBean!");

		lazyModelMediaType = new LazyMediaTypeDataModel(mediaTypeService);	
	}

	@PreDestroy
	public void destroy() {
		getLog().info("Destroy MediaTypeBean!");

	}
	
	private Long mediaTypeToDelete = null;
	public void confirmDeleteMediaType(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("mediaTypeId")) {
    			mediaTypeToDelete = ((Long)((UIParameter)com).getValue());
			}
		}
    	
	}
	
	public void cancelDelete(ActionEvent ev) {
	    	System.out.println("\nDelete MediaType canceled.");
	    	mediaTypeToDelete= null;
	}
	
	public void delete(ActionEvent ev) {
    	System.out.println("\nDelete Test ...");
    	try {
        	
    		mediaTypeService.deleteMediaType(mediaTypeToDelete);
//    		employees=null;
    		addInfoMessage(getMessagesBoundle().getString("mediaType.deleted"));
		} catch (Exception e) {
			e.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("mediaType.deletingError"), e);
		}
	}
	
	public void addMediaType(ActionEvent e) {
		mediaTypeToEdit = new MediaType();
	}
	
	public void modifyMediaType(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("mediaTypeId")) {
    			mediaTypeToEdit = mediaTypeService.getMediaTypeById((Long)((UIParameter)com).getValue());
			}
		}
	}
	
	public void cancelEditMediaType(ActionEvent e) {
		mediaTypeToEdit = null;
	}
	
	public void saveMediaType(ActionEvent e) {
    	System.out.println("Salvo " + mediaTypeToEdit);
    	

    	if (mediaTypeToEdit.getDescription()==null || mediaTypeToEdit.getDescription().trim().equals("")) {
    		addWarningMessage("test su Descrizione richiesto!");
    		return;
    	}
    	
    	
    	try {
    		mediaTypeToEdit = mediaTypeService.updateMediaType(mediaTypeToEdit);
		
			addInfoMessage(getMessagesBoundle().getString("mediaType.saved"));
    	} catch (PersistenceException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();

    	} catch (DataIntegrityViolationException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();
    				
		} catch (Exception e1) {
			addErrorMessage(getMessagesBoundle().getString("mediaType.savingError"), e1);
			e1.printStackTrace();
		}
    	mediaTypeToEdit = null;
	}
	
	public boolean haveUserRole(String roles) {
		
		String[] listRoles = roles.split(",");
		for (int i = 0; i < listRoles.length; i++) {
			listRoles[i] = listRoles[i].trim();
			
		}
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication!=null && authentication.getAuthorities()!=null) {
			//String currentPrincipalName = authentication.getName();
			for (GrantedAuthority r : authentication.getAuthorities()) {
				//System.out.println("Ruolo:"+r.getAuthority());
				for (int i = 0; i < listRoles.length; i++) {
					if (r.getAuthority()!=null && r.getAuthority().equals(listRoles[i])) {
						//System.out.println("trovato ruolo !!!");
						return true;
					}					
				}
			}
		}
		return false;
	}
	
	public void filterByDate() {
		System.out.println("Filter by date !");
		
		Calendar cal = Calendar.getInstance();
		if (filterFrom!=null) {
			cal.setTime(filterFrom);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 1);
			filterFrom = cal.getTime();
		}
		if (filterTo!=null) {
			cal.setTime(filterTo);
			cal.set(Calendar.HOUR_OF_DAY, 23);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);
			filterTo = cal.getTime();
		}
		
		((LazyMediaTypeDataModel)lazyModelMediaType).setFilterFrom(filterFrom);
		((LazyMediaTypeDataModel)lazyModelMediaType).setFilterTo(filterTo);
	}
	
	public Object getMediaTypeById(long id) {
		return mediaTypeService.getMediaTypeById(id);
	}

	public MediaType getMediaTypeToEdit() {
		return mediaTypeToEdit;
	}

	public void setMediaTypeToEdit(MediaType empToEdit) {
		this.mediaTypeToEdit = empToEdit;
	}

	public LazyDataModel<MediaType> getLazyModelMediaType() {
		return lazyModelMediaType;
	}

	public Date getFilterFrom() {
		return filterFrom;
	}

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public Date getFilterTo() {
		return filterTo;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}

}
