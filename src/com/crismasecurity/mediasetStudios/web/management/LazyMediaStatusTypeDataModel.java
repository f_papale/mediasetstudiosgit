package com.crismasecurity.mediasetStudios.web.management;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.crismasecurity.mediasetStudios.core.entity.MediaStatusType;
import com.crismasecurity.mediasetStudios.core.services.MediaStatusTypeService;
 

public class LazyMediaStatusTypeDataModel extends LazyDataModel<MediaStatusType> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2630806951044102912L;

	private MediaStatusTypeService mediaStatusTypeService;
	
	private Date filterFrom;
	private Date filterTo;

     
    public LazyMediaStatusTypeDataModel(MediaStatusTypeService mediaStatusTypeService) {
        this.mediaStatusTypeService = mediaStatusTypeService;
    }
     
    @Override
    public MediaStatusType getRowData(String rowKey) {
    	if (rowKey==null || rowKey.trim().equals("")) return null;
    	return mediaStatusTypeService.getMediaStatusTypeById(Long.parseLong(rowKey));
    }
 
    @Override
    public Object getRowKey(MediaStatusType audit) {
        return audit.getId();
    }
 
    @Override
    public List<MediaStatusType> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
    	
    	// Converto ordinamento
    	int sortInt =0; // UNSORTED
    	if (sortOrder.equals(SortOrder.ASCENDING)) sortInt = 1;
    	if (sortOrder.equals(SortOrder.DESCENDING)) sortInt = 2;
    	
 
		if (filterFrom!=null) filters.put("date#ge", filterFrom);
		if (filterTo!=null) filters.put("date#le", filterTo);

    	
    	List<MediaStatusType> ret = mediaStatusTypeService.getMediaStatusTypeLazyLoading(first, pageSize, sortField, sortInt, filters);
    	this.setRowCount((int)mediaStatusTypeService.getMediaStatusTypeLazyLoadingCount(filters));
//    	System.out.println("Numero elementi trovati: " + this.getRowCount());

    	return ret;    
    }

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}


}