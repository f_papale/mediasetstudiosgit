package com.crismasecurity.mediasetStudios.web.management;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.crismasecurity.mediasetStudios.core.entity.Media;
import com.crismasecurity.mediasetStudios.core.services.MediaService;
 

public class LazyMediaDataModel extends LazyDataModel<Media> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -463330144466423592L;

	private MediaService mediaService;
	
	private Date filterFrom;
	private Date filterTo;

     
    public LazyMediaDataModel(MediaService mediaService) {
        this.mediaService = mediaService;
    }
     
    @Override
    public Media getRowData(String rowKey) {
    	if (rowKey==null || rowKey.trim().equals("")) return null;
    	return mediaService.getMediaById(Long.parseLong(rowKey));
    }
 
    @Override
    public Object getRowKey(Media media) {
        return media.getId();
    }
 
    @Override
    public List<Media> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
    	
    	// Converto ordinamento
    	int sortInt =0; // UNSORTED
    	if (sortOrder.equals(SortOrder.ASCENDING)) sortInt = 1;
    	if (sortOrder.equals(SortOrder.DESCENDING)) sortInt = 2;
    	
 
		if (filterFrom!=null) filters.put("date#ge", filterFrom);
		if (filterTo!=null) filters.put("date#le", filterTo);

    	
    	List<Media> ret = mediaService.getMediaLazyLoading(first, pageSize, sortField, sortInt, filters);
    	this.setRowCount((int)mediaService.getMediaLazyLoadingCount(filters));
//    	System.out.println("Numero elementi trovati: " + this.getRowCount());
//    	for (Audit audit : ret) {
//			System.out.println("Trovato: " + audit);
//		}
    	return ret;    
    }

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}


}