package com.crismasecurity.mediasetStudios.web.management;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.crismasecurity.mediasetStudios.core.entity.Incident;
import com.crismasecurity.mediasetStudios.core.services.IncidentService;
 

public class LazyIncidentDataModel extends LazyDataModel<Incident> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2884731038249080910L;

	private IncidentService IncidentService;
	
	private Date filterFrom;
	private Date filterTo;

     
    public LazyIncidentDataModel(IncidentService incidentService) {
        this.IncidentService = incidentService;
    }
     
    @Override
    public Incident getRowData(String rowKey) {
    	if (rowKey==null || rowKey.trim().equals("")) return null;
    	return IncidentService.getIncidentById(Long.parseLong(rowKey));
    }
 
    @Override
    public Object getRowKey(Incident audit) {
        return audit.getIdIncident();
    }
 
    @Override
    public List<Incident> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
    	
    	// Converto ordinamento
    	int sortInt =0; // UNSORTED
    	if (sortOrder.equals(SortOrder.ASCENDING)) sortInt = 1;
    	if (sortOrder.equals(SortOrder.DESCENDING)) sortInt = 2;
    	
 
		if (filterFrom!=null) filters.put("date#ge", filterFrom);
		if (filterTo!=null) filters.put("date#le", filterTo);

    	
    	List<Incident> ret = IncidentService.getIncidentLazyLoading(first, pageSize, sortField, sortInt, filters);
    	this.setRowCount((int)IncidentService.getIncidentLazyLoadingCount(filters));
    	System.out.println("Numero di Incident trovati: " + this.getRowCount());
    	return ret;    
    }

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}


}