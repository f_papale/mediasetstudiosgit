package com.crismasecurity.mediasetStudios.web.management;

import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.persistence.PersistenceException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import com.crismasecurity.mediasetStudios.core.entity.Agency;
import com.crismasecurity.mediasetStudios.core.entity.AgencyUser;
import com.crismasecurity.mediasetStudios.core.entity.Episode;
import com.crismasecurity.mediasetStudios.core.entity.Production;
import com.crismasecurity.mediasetStudios.core.security.services.AppUserService;
import com.crismasecurity.mediasetStudios.core.services.AgencyService;
import com.crismasecurity.mediasetStudios.core.services.AgencyUserService;
import com.crismasecurity.mediasetStudios.core.services.EpisodeService;
import com.crismasecurity.mediasetStudios.core.services.ProductionService;

import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;
import com.widee.base.hibernate.criteria.Finder;
import com.crismasecurity.mediasetStudios.web.LoginBean;
import com.crismasecurity.mediasetStudios.web.management.utils.CustomScheduleEvent;
import com.crismasecurity.mediasetStudios.web.management.utils.NodeFormatter;

import org.primefaces.event.ScheduleEntryMoveEvent;
import org.primefaces.event.ScheduleEntryResizeEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;
import org.primefaces.model.TreeNode;


//@ManagedBean(name= EmployeesBean.BEAN_NAME) // JSF managed
//@SessionScoped // JSF managed
//@ViewScoped // JSF managed

//Spring managed
@Controller (CalendarProductionBean.BEAN_NAME) 
//@Scope("session")
@Scope("view")
public class CalendarProductionBean extends BaseBean {

	private static final long serialVersionUID = 3758172174992954442L;

	public static final String BEAN_NAME = "calendarProductionBean";
    public String getBeanName() { return BEAN_NAME; } 
   
//    private LazyDataModel<Production> lazyModelProduction;
    private DefaultScheduleModel eventModel;
    private Date dateSelect = Calendar.getInstance().getTime();
	private Agency filterOwner;
	private Agency owner = null;
    
    //private ScheduleModel lazyEventModel;
 
    private ScheduleEvent event = new DefaultScheduleEvent();
   
    @Autowired
    private ProductionService calendarProductionService ;
   
    @Autowired 
    private EpisodeService calendarEpisodeService;
    
	@Autowired
	private AgencyUserService agencyUserService;
	
	@Autowired
	private AppUserService userService;    
    
	@Autowired
    private AgencyService agencyService;
	
	@Autowired
	private LoginBean loginBean;
	
    
    private Episode episodeToEdit; 
    private Production productionToedit;
    
    private boolean onEdit=false; 
	private List<Production> productions;
	private TreeNode root;
	
	private AgencyUser currentAgencyUser=null;	
	
    
    public CalendarProductionBean() {
    	
		getLog().info("!!!! Costruttore di CalendarProductionBean !!!");
		
    }
    
	@PostConstruct
	public void init() {

		eventModel = new DefaultScheduleModel();
		presetCurrentUser();
		presetOwner();
        root = createCalendarProduction();
		getLog().info("INIT EmployeesBean!");
	}
	
	@PreDestroy
	public void destroy() {
		getLog().info("Destroy CalendarProductionBean!");
	}
	
	//-- Private Region ----------------------------------------------------------------------------	
	
	public boolean haveUserRole(String roles) {
		
		String[] listRoles = roles.split(",");
		for (int i = 0; i < listRoles.length; i++) {
			listRoles[i] = listRoles[i].trim();
			
		}
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication!=null && authentication.getAuthorities()!=null) {
			//String currentPrincipalName = authentication.getName();
			for (GrantedAuthority r : authentication.getAuthorities()) {
				//System.out.println("Ruolo:"+r.getAuthority());
				for (int i = 0; i < listRoles.length; i++) {
					if (r.getAuthority()!=null && r.getAuthority().equals(listRoles[i])) {
						//System.out.println("trovato ruolo !!!");
						return true;
					}					
				}
			}
		}
		return false;
	}
	
	private void presetOwner() {

		if ((haveUserRole("ROLE_AGENCY") && haveUserRole("ROLE_PRODUCTION")) || (haveUserRole("ROLE_AGENCY") && haveUserRole("ROLE_STADMIN"))) {		
			try {
				setOwner(agencyService.getAgencyFinder().and("idAgency").eq(getIdLoggedAgency()).setDistinct().result());
				setFilterOwner(getOwner());
				
			} catch (Exception e) {
				setOwner(null);
				setFilterOwner(null);
			}
		} else {
			setFilterOwner(null);
			try {
				try {
					setOwner(agencyService.getAgencyById(getIdLoggedAgency()));	
				} catch (Exception e) {
					// Attenzione se entro come amministratore viene settato come owner la prima super agenzia censita
					setOwner(agencyService.getAgencyFinder().and("agencyType.flgProduction").eq(1).addOrderAsc("id").setMaxResults(1).result()); 	
				}
				
			} catch (Exception e) {
				setOwner(null);
			}
		}
	}
	
	private Object getPassedValue(ActionEvent e, String variableName) {
		
		Object o = null;
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase(variableName)) {
    			o=(Object)(((UIParameter)com).getValue());
			}
		}
    	return o;
	}
	
	
// -- Public Region ------------------------------------------------------------------------------	

	
	private void presetCurrentUser() {

		try {
			long userId = userService.getUserByLogin(loginBean.getUserId()).getId();
			currentAgencyUser= agencyUserService.getAgencyUserFinder().and("user.id").eq(userId).and("flgCurrentSelected").eq(true).result();	
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Errore Preset Current User");
			addErrorMessage(getMessagesBoundle().getString("CalendarProductionBean.presetCurrentUser"), e);  	
		} 
	}
	
	
	private int getDay(Date dummy) {
		Calendar rightNow = Calendar.getInstance();
		rightNow.setTime(dummy);
		return rightNow.get(Calendar.DAY_OF_YEAR); 
	}
	
	private int getHour(Date dummy) {
		Calendar rightNow = Calendar.getInstance();
		rightNow.setTime(dummy);
		return rightNow.get(Calendar.HOUR_OF_DAY); 
	}

	private int getMinute(Date dummy) {
		Calendar rightNow = Calendar.getInstance();
		rightNow.setTime(dummy);
		return rightNow.get(Calendar.MINUTE); 
	}

	private Date convertDate(Date templDate, Date hour) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(templDate);
		cal.set(Calendar.DAY_OF_YEAR, getDay(hour));
		cal.set(Calendar.HOUR_OF_DAY, getHour(hour));
		cal.set(Calendar.MINUTE, getMinute(hour)); 
		return cal.getTime();
	}
	
	private void refreshCalendarProduction() {
		
		eventModel.clear();
		for (Episode epis : productionToedit.getEpisodes()) {
			CustomScheduleEvent sce = new CustomScheduleEvent(epis);
			//event = new DefaultScheduleEvent(epis.getProduction().getName(), dFormat(epis.getDateFrom()), dFormat(epis.getDateTo()), sce );
			event = new DefaultScheduleEvent(epis.getProduction().getName(), epis.getDateFrom(), epis.getDateTo(), sce );
			String colorCls = "backgrdep_"+ epis.getEpisodeStatus().getIdStatus();
			((DefaultScheduleEvent)event).setStyleClass(colorCls);
			//System.out.println(dFormat(epis.getDateFrom()).toString() + " -- " + event.getStartDate().toString() + " - " + dFormat(epis.getDateTo()).toString() + " -- " + event.getEndDate().toString());
			eventModel.addEvent(event);
		}		
	}

	private void addMessage(FacesMessage message) {
	    FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public void saveEvent() {
		System.out.println("\nsaveCalendar ...");
	    
	    try {
	        CustomScheduleEvent dummy = (CustomScheduleEvent)event.getData();
	        episodeToEdit= dummy.getEpisode();
	        dummy.setDateFrom(convertDate(dummy.getDateFrom(),event.getStartDate()));
	        dummy.setDateTo(convertDate(dummy.getDateTo(),event.getEndDate()));
	        episodeToEdit.setDateFrom(dummy.getDateFrom());
	        episodeToEdit.setDateTo(dummy.getDateTo());
	        episodeToEdit.setRequestedSpectators(dummy.getRequestedSpectators());
	        
	        saveCalendarProduction();
	        refreshCalendarProduction(); 
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Salva Produzione", getMessagesBoundle().getString("calendarProduction.saved"));
	        addMessage(message);	
    	} catch (PersistenceException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();

    	} catch (DataIntegrityViolationException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();
    		        
		} catch (Exception e1) {
			addErrorMessage(getMessagesBoundle().getString("calendarProduction.savingError"), e1);
			e1.printStackTrace();
		}
	}

	public void modifyCalendarProduction(ActionEvent e) {
		System.out.println("\nmodifyCalendar ...");
		//episodeToEdit = new Episode();
		setOnEdit(true);
	}

	public void cloneCalendarProduction(ActionEvent e) {
		System.out.println("\ncloneCalendar ...");
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Clone Event", "Working progress...");
        addMessage(message);
        try {
            Episode episodeToClone =  new Episode();
            episodeToClone=episodeToEdit;
            episodeToClone= calendarEpisodeService.updateEpisode(episodeToClone);
		} catch (Exception e1) {
			addErrorMessage(getMessagesBoundle().getString("calendarProduction.cloneCalendarProduction"), e1);
			e1.printStackTrace();
		} finally {
			undoOnEdit();
		}
	}
	
	public void saveCalendarProduction() {
		System.out.println("Salvo " + episodeToEdit);
		
		try {
	    	if (episodeToEdit.getDateFrom()==null || episodeToEdit.getDateFrom().toString().trim().equals("")) {
	    		addWarningMessage("calendarProduction su calendarProduction richiesto!");
	    		return;
	    	} else {
	    		episodeToEdit = calendarEpisodeService.updateEpisode(episodeToEdit);
				addInfoMessage(getMessagesBoundle().getString("calendarProduction.saved"));
	    	}
    	} catch (PersistenceException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();

    	} catch (DataIntegrityViolationException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();
    		    	
		} catch (Exception e1) {
			addErrorMessage(getMessagesBoundle().getString("calendarProduction.savingError"), e1);
			e1.printStackTrace();
		} finally {
			//episodeToEdit = null;
			//setOnEdit(false);
			undoOnEdit();
			
		}
	}

	public void viewCalendarProduction(ActionEvent e) {
	
		undoOnEdit();
		
		Long episodeId = (Long)getPassedValue(e,"calendarEpisodeId");
		Long productionId = calendarEpisodeService.getEpisodeById(episodeId).getProduction().getId();
		productionToedit = calendarProductionService.getProductionById(productionId);
		setDateSelect(calendarEpisodeService.getEpisodeById(episodeId).getDateFrom());
		refreshCalendarProduction();
	
	}

	public void cancelEditCalendarProduction(ActionEvent e) {
		undoOnEdit();
	}
	
	public Object getCalendarProductionToEdit() {
		//System.out.println("getCalendarProductionToEdit...");
		
		//return episodeToEdit;
		return  null;
	}

	public void setCalendarProductionToEdit(Object empToEdit) {
		System.out.println("setCalendarProductionToEdit...");
		//this.episodeToEdit = (Episode) empToEdit;
	}

	 //Region Treew

    public TreeNode getRoot() {
        return root;
    }

	private Long  getIdLoggedAgency() {
		return Long.valueOf(currentAgencyUser.getAgency().getIdAgency());				
	}

    private Finder<Production> getProductionFinder(){
    	
    	Finder<Production> ret =null; 
		if (this.filterOwner!=null) {
			ret = calendarProductionService.getProductionFinder().and("owner.id").eq(this.filterOwner.getIdAgency());
		} else
		{
			ret = calendarProductionService.getProductionFinder();
		}
		return ret;
    }

	
	public TreeNode createCalendarProduction() {
    	
    	String sDummy1=null;
//    	String sDummy2=null;
    	long idDummy2=0;
    	String sDummy3=null;
    	long noMarker=(long)-1;
    	
    	TreeNode season= null;

    	TreeNode root = new DefaultTreeNode(new NodeFormatter(noMarker, "Season", "-", "-"), null);  	
    	Finder<Production> fnd=getProductionFinder();
    	
    	productions = fnd.addOrderDesc("season").addOrderAsc("name").list();
    	for (Production prod : productions) {
    		if (!prod.getSeason().equals(sDummy1)) {
    			season = new DefaultTreeNode(new NodeFormatter(noMarker,prod.getSeason(), "-", "-"), root);
    			sDummy1=prod.getSeason().trim();
    		}
    		Comparator<Episode> byDate = (e1, e2)->e2.getDateFrom().compareTo(e1.getDateFrom());
    		List<Episode> episodes = prod.getEpisodes()
    				.stream()
    				.filter(e-> e.getEpisodeStatus().getIdStatus()!=5)
    				.collect(Collectors.toList());
    		episodes.sort(byDate);
    		for (Episode epis: episodes) {
//    			if (!prod.getName().equals(sDummy2)) {
    			if (!prod.getId().equals(idDummy2)) {
    				//System.out.println("getID = " +epis.getId());
    				new DefaultTreeNode(new NodeFormatter(epis.getId(),"", prod.getName() , epis.getDateFromOnlyMonth().toUpperCase()), season);
    				idDummy2=prod.getId();
//    				sDummy2=prod.getName().trim();
    				sDummy3=epis.getDateFromOnlyMonth().toUpperCase();
    			} else {
    				if(!epis.getDateFromOnlyMonth().toUpperCase().equals(sDummy3)) {
    					//System.out.println("getID = " +epis.getId());
    					new DefaultTreeNode(new NodeFormatter(epis.getId(),"", "" , epis.getDateFromOnlyMonth().toUpperCase()), season);
    					sDummy3=epis.getDateFromOnlyMonth().toUpperCase();
    				}
    			}    			
    		}
    	}
   	
    	getLog().info("!!!! createCalendarProduction !!!");
        return root;
    }
    
    public ScheduleModel getEventModel() {
        return eventModel;
    }
 
    public ScheduleEvent getEvent() {
        return event;
    }
 
    public void setEvent(ScheduleEvent event) {
        this.event = event;
    }
    
    public void onDebug(SelectEvent selectEvent) {
    	System.out.println("onDebug...");
        //event = (ScheduleEvent) selectEvent.getObject();
        //System.out.println(event.getData());
    }
     
    public void onEventSelect(SelectEvent selectEvent) {
    	System.out.println("onEventSelect...");
    	try {
            event = (ScheduleEvent)(selectEvent.getObject());
            if (event !=null) {
            	episodeToEdit= ((CustomScheduleEvent)event.getData()).getEpisode();	
            }			
		} catch (Exception e) {
			episodeToEdit=null;
		}
        System.out.println(episodeToEdit);
    }

    public void onDateSelect(SelectEvent selectEvent) {
    	
        //event = new DefaultScheduleEvent("", (Date) selectEvent.getObject(), (Date) selectEvent.getObject(), );
    }
     
    public void onEventMove(ScheduleEntryMoveEvent event) {
		System.out.println("onEventMove...");
	    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Event moved", "Day delta:" + event.getDayDelta() + ", Minute delta:" + event.getMinuteDelta());
	    episodeToEdit= ((CustomScheduleEvent)event.getScheduleEvent().getData()).getEpisode();
	    episodeToEdit.setDateFrom(event.getScheduleEvent().getStartDate());
	    episodeToEdit.setDateTo(event.getScheduleEvent().getEndDate());
	    System.out.println(episodeToEdit.getDateFromOnlyDate()+" - " +event.getScheduleEvent().getStartDate()+" - "+event.getScheduleEvent().getEndDate());
	    addMessage(message);
	}

	public void onEventResize(ScheduleEntryResizeEvent event) {
		System.out.println("onEventResize...");
	    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Event resized", "Day delta:" + event.getDayDelta() + ", Minute delta:" + event.getMinuteDelta());
	    episodeToEdit= ((CustomScheduleEvent)event.getScheduleEvent().getData()).getEpisode();
	    episodeToEdit.setDateFrom(event.getScheduleEvent().getStartDate());
	    episodeToEdit.setDateTo(event.getScheduleEvent().getEndDate());
	    System.out.println(episodeToEdit.getDateFromOnlyDate()+" - " +event.getScheduleEvent().getStartDate() + " - "+event.getScheduleEvent().getEndDate()); 
	    addMessage(message);
	}

	public Date getDateSelect() {
		return dateSelect;
	}


	public void setDateSelect(Date dateSelect) {
		this.dateSelect = dateSelect;
	}

	public boolean isOnEdit() {
		return onEdit;
	}
	
	public void undoOnEdit() {
		setOnEdit(false);
		episodeToEdit = null;	
	}

	public void setOnEdit(boolean onEdit) {
		this.onEdit = onEdit;
	}

	private void setFilterOwner(Agency filterOwner) {
		this.filterOwner = filterOwner;
	}
	
	private Agency getOwner() {
		return owner;
	}

	private void setOwner(Agency owner) {
		this.owner = owner;
	}
}
