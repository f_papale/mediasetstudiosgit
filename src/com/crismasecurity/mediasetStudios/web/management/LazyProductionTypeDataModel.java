package com.crismasecurity.mediasetStudios.web.management;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.crismasecurity.mediasetStudios.core.entity.ProductionType;
import com.crismasecurity.mediasetStudios.core.services.ProductionTypeService;
 

public class LazyProductionTypeDataModel extends LazyDataModel<ProductionType> {

	private static final long serialVersionUID = 8035852974298835187L;

	private ProductionTypeService productionTypeService;
	
	private Date filterFrom;
	private Date filterTo;

     
    public LazyProductionTypeDataModel(ProductionTypeService productionTypeService) {
        this.productionTypeService = productionTypeService;
    }
     
    @Override
    public ProductionType getRowData(String rowKey) {
    	if (rowKey==null || rowKey.trim().equals("")) return null;
    	return productionTypeService.getProductionTypeById(Long.parseLong(rowKey));
    }
 
    @Override
    public Object getRowKey(ProductionType audit) {
        return audit.getId();
    }
 
    @Override
    public List<ProductionType> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
    	
    	// Converto ordinamento
    	int sortInt =0; // UNSORTED
    	if (sortOrder.equals(SortOrder.ASCENDING)) sortInt = 1;
    	if (sortOrder.equals(SortOrder.DESCENDING)) sortInt = 2;
    	
 
		if (filterFrom!=null) filters.put("date#ge", filterFrom);
		if (filterTo!=null) filters.put("date#le", filterTo);

    	
    	List<ProductionType> ret = productionTypeService.getProductionTypeLazyLoading(first, pageSize, sortField, sortInt, filters);
    	this.setRowCount((int)productionTypeService.getProductionTypeLazyLoadingCount(filters));
//    	System.out.println("Numero elementi trovati: " + this.getRowCount());
//    	for (Audit audit : ret) {
//			System.out.println("Trovato: " + audit);
//		}
    	return ret;    
    }

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}


}