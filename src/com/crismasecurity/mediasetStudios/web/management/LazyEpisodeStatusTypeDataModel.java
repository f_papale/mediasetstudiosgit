package com.crismasecurity.mediasetStudios.web.management;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.crismasecurity.mediasetStudios.core.entity.EpisodeStatusType;
import com.crismasecurity.mediasetStudios.core.services.EpisodeStatusTypeService;
 

public class LazyEpisodeStatusTypeDataModel extends LazyDataModel<EpisodeStatusType> {

	private static final long serialVersionUID = -4653671320827287068L;

	private EpisodeStatusTypeService episodeStatusTypeService;
	
	private Date filterFrom;
	private Date filterTo;

     
    public LazyEpisodeStatusTypeDataModel(EpisodeStatusTypeService episodeStatusTypeService) {
        this.episodeStatusTypeService = episodeStatusTypeService;
    }
     
    @Override
    public EpisodeStatusType getRowData(String rowKey) {
    	if (rowKey==null || rowKey.trim().equals("")) return null;
    	return episodeStatusTypeService.getEpisodeStatusTypeById(Long.parseLong(rowKey));
    }
 
    @Override
    public Object getRowKey(EpisodeStatusType audit) {
        return audit.getId();
    }
 
    @Override
    public List<EpisodeStatusType> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
    	
    	// Converto ordinamento
    	int sortInt =0; // UNSORTED
    	if (sortOrder.equals(SortOrder.ASCENDING)) sortInt = 1;
    	if (sortOrder.equals(SortOrder.DESCENDING)) sortInt = 2;
    	
 
		if (filterFrom!=null) filters.put("date#ge", filterFrom);
		if (filterTo!=null) filters.put("date#le", filterTo);
		//Prendo solamente gli stati applicativi e non quelli di sistema
//		filters.put("owner", "A");
		

    	
    	List<EpisodeStatusType> ret = episodeStatusTypeService.getEpisodeStatusTypeLazyLoading(first, pageSize, sortField, sortInt, filters);
    	this.setRowCount((int)episodeStatusTypeService.getEpisodeStatusTypeLazyLoadingCount(filters));
//    	System.out.println("Numero elementi trovati: " + this.getRowCount());

    	return ret;    
    }

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}


}