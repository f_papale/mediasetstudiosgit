package com.crismasecurity.mediasetStudios.web.management;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.persistence.PersistenceException;

import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;


import com.crismasecurity.mediasetStudios.core.entity.Agency;
import com.crismasecurity.mediasetStudios.core.entity.AgencyType;
import com.crismasecurity.mediasetStudios.core.entity.CountryCode;
import com.crismasecurity.mediasetStudios.core.entity.Production;
import com.crismasecurity.mediasetStudios.core.services.AgencyService;
import com.crismasecurity.mediasetStudios.core.services.AgencyTypeService;
import com.crismasecurity.mediasetStudios.core.services.CountryCodeService;
import com.crismasecurity.mediasetStudios.core.services.ProductionService;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;

//@ManagedBean(name= EmployeesBean.BEAN_NAME) // JSF managed
//@SessionScoped // JSF managed
//@ViewScoped // JSF managed

//Spring managed
@Controller (AgencyBean.BEAN_NAME) 
//@Scope("session")
@Scope("view")
public class AgencyBean extends BaseBean {


	private static final long serialVersionUID = 5278743136213989941L;

	public static final String BEAN_NAME = "agencyBean";
    public String getBeanName() { return BEAN_NAME; } 


    @Autowired
    private AgencyService agencyService ;
    
    @Autowired
    private ProductionService productionService ;
    
    @Autowired
    private AgencyTypeService agencyTypeService;
    
    @Autowired
    private CountryCodeService countryCodeService;
   
    private LazyDataModel<Agency> lazyModelAgency;
    private Date filterFrom;
	private Date filterTo;
	
	
	private List<SelectItem> agencyTypes;
	private Long selectedAgencyType = null;
	
	private Agency agencyToEdit = null;
    
	public AgencyBean() {
		getLog().info("!!!! Costruttore di AgencyBean !!!");	
    }

	@PostConstruct
	public void init() {
		getLog().info("INIT AgencyBean!");

		presetAgencyList();
		lazyModelAgency = new LazyAgencyDataModel(agencyService);			
	}

	@PreDestroy
	public void destroy() {
		getLog().info("Destroy AgencyBean!");

	}
	
	private void presetAgencyList() {
		agencyTypes = new ArrayList<SelectItem>();
		List<AgencyType> conts = agencyTypeService.getAgencyType();
		agencyTypes.add(new SelectItem(null, " "));
		for (AgencyType cont : conts) {
			SelectItem xx = new SelectItem(cont.getId(), cont.getDescription());
			agencyTypes.add(xx);
		}
	}
	
	private Long agencyToDelete = null;
	
	public void confirmDeleteAgency(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("agencyId")) {
    			agencyToDelete = ((Long)((UIParameter)com).getValue());
			}
		}
    	
	}
	
	public void cancelDelete(ActionEvent ev) {
	    	System.out.println("\nDelete Agency canceled.");
	    	agencyToDelete= null;
	}
	
	public void delete(ActionEvent ev) {
    	System.out.println("\nDelete Test ...");
    	try {
        	
    		agencyService.deleteAgency(agencyToDelete);
    		addInfoMessage(getMessagesBoundle().getString("agency.deleted"));
		} catch (Exception e) {
			e.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("agency.deletingError"), e);
		}
	}
	
	public void addAgency(ActionEvent e) {
		agencyToEdit = new Agency();
		selectedAgencyType=null;
	}
	
	public void modifyAgency(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("agencyId")) {
    			agencyToEdit = agencyService.getAgencyById((Long)((UIParameter)com).getValue());
    			selectedAgencyType=agencyToEdit.getAgencyType().getId();
    			
			}
		}
	}
	
    public List<CountryCode> completeCountryCode(String query) {      	
    	List<CountryCode> filteredCountriesCode = countryCodeService.getCountryCodeFinder().and("city").like(query).setDistinct().list();
        return filteredCountriesCode;
    }
	
	public void cancelEditAgency(ActionEvent e) {
		agencyToEdit = null;
	}
	
	public void saveAgency(ActionEvent e) {
    	System.out.println("Salvo " + agencyToEdit);
    	

    	if (agencyToEdit.getName()==null || agencyToEdit.getName().trim().equals("")) {
    		addWarningMessage("test su Descrizione richiesto!");
    		return;
    	}

    	if (selectedAgencyType==null) {
    		addWarningMessage("test su Tipo Agenzia richiesto!");
    		return;
    	} 	
    	    	
    	
    	try {
    		//Imposta il valore della dropdown list nella classe
    		agencyToEdit.setAgencyType(agencyTypeService.getAgencyTypeById(selectedAgencyType));
    		/*
    		System.out.println("Inserisco popo");
    		Spectator sp = new Spectator();
    		sp.setName("giovanni");
    		sp.setSurname("congedi");

    		agencyToEdit.addSpectator(sp, "propo");
    		*/
    		if (agencyToEdit.getIdAgency()!=null) agencyToEdit = agencyService.updateAgency(agencyToEdit);
    		else agencyToEdit = agencyService.addAgency(agencyToEdit);

		
			addInfoMessage(getMessagesBoundle().getString("agency.saved"));
    	} catch (PersistenceException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();

    	} catch (DataIntegrityViolationException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();
    				
		} catch (Exception e1) {
			addErrorMessage(getMessagesBoundle().getString("agency.savingError"), e1);
			e1.printStackTrace();
		}
    	agencyToEdit = null;
	}
	
	public void filterByDate() {
		System.out.println("Filter by date !");
		
		Calendar cal = Calendar.getInstance();
		if (filterFrom!=null) {
			cal.setTime(filterFrom);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 1);
			filterFrom = cal.getTime();
		}
		if (filterTo!=null) {
			cal.setTime(filterTo);
			cal.set(Calendar.HOUR_OF_DAY, 23);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);
			filterTo = cal.getTime();
		}
		
		((LazyAgencyDataModel)lazyModelAgency).setFilterFrom(filterFrom);
		((LazyAgencyDataModel)lazyModelAgency).setFilterTo(filterTo);
	}
	
	public Object getAgencyById(long id) {
		return agencyService.getAgencyById(id);
	}

	public Agency getAgencyToEdit() {
		return agencyToEdit;
	}

	public void setAgencyToEdit(Agency empToEdit) {
		this.agencyToEdit = empToEdit;
	}

	public LazyDataModel<Agency> getLazyModelAgency() {
		return lazyModelAgency;
	}

	public Date getFilterFrom() {
		return filterFrom;
	}

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public Date getFilterTo() {
		return filterTo;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}

	public List<SelectItem> getAgencyTypes() {
		return this.agencyTypes;
	}

	public void setAgencyTypes(List<SelectItem> agencyTypes) {
		this.agencyTypes = agencyTypes;
	}

	public Long getSelectedAgencyType() {
		return selectedAgencyType;
	}

	public void setSelectedAgencyType(Long selectedAgencyType) {
		this.selectedAgencyType = selectedAgencyType;
	}

	public CountryCodeService getCountryCodeService() {
		return countryCodeService;
	}

	public void setCountryCodeService(CountryCodeService countryCodeService) {
		this.countryCodeService = countryCodeService;
	}
	
    public boolean isRemovableAgency(Agency agency) {
/*    	
    	Set<Integer> statusEpisodes = prod.getEpisodes().stream()
    			.map(s -> s.getEpisodeStatus().getIdStatus())
    			.filter(s->!((s==1) || (s==5)))
    			.collect(Collectors.toSet());
    	return !statusEpisodes.isEmpty();
*/
    	List<Production> res = productionService.getProductionFinder().and("agency").eq(agency).setDistinct().list();
    	return !res.isEmpty();
    }
}
