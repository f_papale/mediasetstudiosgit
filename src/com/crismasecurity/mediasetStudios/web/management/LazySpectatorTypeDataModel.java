package com.crismasecurity.mediasetStudios.web.management;

import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.crismasecurity.mediasetStudios.core.entity.SpectatorType;
import com.crismasecurity.mediasetStudios.core.services.SpectatorTypeService;

public class LazySpectatorTypeDataModel extends LazyDataModel<SpectatorType>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2960910410611093828L;
	
	private SpectatorTypeService spectatorTypeService;
	
	public LazySpectatorTypeDataModel(SpectatorTypeService sptypeservice) {
		this.spectatorTypeService = sptypeservice;
	}
	
	@Override
	public SpectatorType getRowData(String rowKey) {
		if (rowKey==null || rowKey.trim().equals("")) return null;
		return spectatorTypeService.getSpectatorTypeById(Long.parseLong(rowKey));
	}
	
    @Override
    public Object getRowKey(SpectatorType sptype) {
    	return sptype.getId();
    }
    
    @Override 
    public List<SpectatorType> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters){
    	// Converto ordinamento
    	int sortInt = 0; // UNSORTED
    	if (sortOrder.equals(SortOrder.ASCENDING)) sortInt = 1;
    	if (sortOrder.equals(SortOrder.DESCENDING)) sortInt = 2;
    	
    	List<SpectatorType> sptypelist = spectatorTypeService.getSpectatorTypeLazyLoading(first, pageSize, sortField, sortInt, filters);
    	this.setRowCount((int)spectatorTypeService.getSpectatorTypeLazyLoadingCount(filters));
//    	System.out.println("Numero elementi trovati: " + this.getRowCount());    	
    	return sptypelist;
    	
    }

}
