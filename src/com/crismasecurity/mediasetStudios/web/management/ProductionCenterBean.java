
package com.crismasecurity.mediasetStudios.web.management;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.persistence.PersistenceException;

import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import com.crismasecurity.mediasetStudios.core.entity.DistributionList;
import com.crismasecurity.mediasetStudios.core.entity.Location;
import com.crismasecurity.mediasetStudios.core.entity.ProductionCenter;
import com.crismasecurity.mediasetStudios.core.services.DistributionListService;
import com.crismasecurity.mediasetStudios.core.services.LocationService;
import com.crismasecurity.mediasetStudios.core.services.ProductionCenterService;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;





//Spring managed
@Controller (ProductionCenterBean.BEAN_NAME) 
//@Scope("session")
@Scope("view")
public class ProductionCenterBean extends BaseBean {

	private static final long serialVersionUID = -658559946382470252L;

	public static final String BEAN_NAME = "productionCenterBean";
    public String getBeanName() { return BEAN_NAME; } 


    @Autowired
    private ProductionCenterService ProductionCenterService ;
      
    @Autowired
    private LocationService locationService;
    
    @Autowired
    private DistributionListService distributionListService;
       
/*    
    @Autowired
    private ProductionCenterService productionCenterService;
*/    
    private LazyDataModel<ProductionCenter> lazyModelProductionCenter;
    private Date filterFrom;
	private Date filterTo;
		
	private List<SelectItem> productionCenters;
	private Long selectedProductionCenter = null;
	private ProductionCenter productionCenterToEdit = null;
    
	private List<SelectItem> distributionLists;
	private Long selectedDistributionList = null;
	
	private Location locationToEdit = null;
	private List<SelectItem> productionCenterLocations;
	private Long selectedProductionCenterLocation = null;
	private Long locationToDelete = null;
	
	private int activeTab=0;
	
	public ProductionCenterBean() {
		getLog().info("!!!! Costruttore di ProductionCenterBean !!!");	
    }

	@PostConstruct
	public void init() {
		getLog().info("INIT ProductionCenterBean!");
		lazyModelProductionCenter = new LazyProductionCenterDataModel(ProductionCenterService);			
		presetLocations();
		presetDistributionList();
	}

	@PreDestroy
	public void destroy() {
		getLog().info("Destroy ProductionCenterBean!");
	}

	private void presetDistributionList() {
		distributionLists = new ArrayList<SelectItem>();
		List<DistributionList> conts = distributionListService.getDistributionList();
		distributionLists.add(new SelectItem(null, " "));
		for (DistributionList cont : conts) {
			SelectItem xx = new SelectItem(cont.getId(), cont.getName());
			distributionLists.add(xx);
		}
	}	
	
	
	public boolean haveUserRole(String roles) {
		
		String[] listRoles = roles.split(",");
		for (int i = 0; i < listRoles.length; i++) {
			listRoles[i] = listRoles[i].trim();
			
		}
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication!=null && authentication.getAuthorities()!=null) {
			//String currentPrincipalName = authentication.getName();
			for (GrantedAuthority r : authentication.getAuthorities()) {
				//System.out.println("Ruolo:"+r.getAuthority());
				for (int i = 0; i < listRoles.length; i++) {
					if (r.getAuthority()!=null && r.getAuthority().equals(listRoles[i])) {
						//System.out.println("trovato ruolo !!!");
						return true;
					}					
				}
			}
		}
		return false;
	}
	
	private Long productionCenterToDelete = null;
	
	public void confirmDeleteProductionCenter(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("productionCenterId")) {
    			productionCenterToDelete = ((Long)((UIParameter)com).getValue());
			}
		}
	}
	
	
	
	public void cancelDelete(ActionEvent ev) {
	    	System.out.println("\nDelete ProductionCenter canceled.");
	    	productionCenterToDelete= null;
	}
	
	public void delete(ActionEvent ev) {
    	System.out.println("\nDelete Test ...");
    	try {
        	
    		ProductionCenterService.deleteProductionCenter(productionCenterToDelete);
    		addInfoMessage(getMessagesBoundle().getString("productionCenter.deleted"));
		} catch (Exception e) {
			e.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("productionCenter.deletingError"), e);
		}
	}
	
	public void addProductionCenter(ActionEvent e) {
		productionCenterToEdit = new ProductionCenter();
		selectedDistributionList= null;
	}
	
	public void modifyProductionCenter(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("productionCenterId")) {
    			productionCenterToEdit = ProductionCenterService.getProductionCenterById((Long)((UIParameter)com).getValue());
    			locationToEdit = null;
    			try {
    				selectedDistributionList= productionCenterToEdit.getDistributionList().getId();	
				} catch (Exception e2) {
					selectedDistributionList= null;
				}
    			System.out.println("Numero elementi location su ProductionCenter: " + productionCenterToEdit.getLocations());
    		}
		}
	}
	
	public void cancelEditProductionCenter(ActionEvent e) {
		productionCenterToEdit = null;
	}
	
	
	public void addProductionCenterLocation(ActionEvent e) {
		locationToEdit = new Location();
		selectedProductionCenterLocation=null;
		setActiveTab(0);
	}
	
	
	public void cancelEditLocation(ActionEvent e) {
		System.out.println("\nDelete Employee canceled.");
		locationToEdit = null;
	}
	
	
	public void saveProductionCenterLocation(ActionEvent e) {
		System.out.println("Devo salvare le locations all'interno della griglia");
		try {
			
			if(selectedProductionCenterLocation!=null) {
				Location location = locationService.getLocationById(selectedProductionCenterLocation);
				//Verifico se all'interno della griglia sono già presenti delle location
				if(productionCenterToEdit.getLocations() == null || productionCenterToEdit.getLocations().isEmpty()) {
					List<Location> listLocation = new ArrayList<Location>();
					listLocation.add(location);
					productionCenterToEdit.setLocations(listLocation);
				}
				else {
					
					//if(!this.checkContainsLocation(productionCenterToEdit.getLocations() , locationToEdit.getId())) {
					if(!this.checkContainsLocation(productionCenterToEdit.getLocations() , location.getId())) {
						productionCenterToEdit.getLocations().add(location);
					}
					else {
						//productionCenterToEdit.getLocations().remove(locationToEdit);
						
						
						Iterator<Location> itr = productionCenterToEdit.getLocations().iterator();
				        while (itr.hasNext()) {
				            Location element = itr.next();
				            //if (element.getId() == locationToEdit.getId()) {
				            if (element.getId() == location.getId()) {
				                itr.remove();
				            }
				        }
						//productionCenterToEdit.getLocations().remove(productionCenterToEdit.getLocations().indexOf(locationToEdit));
						productionCenterToEdit.getLocations().add(location);
					}
				}
				locationToEdit = null;
			}
			else {
				addErrorMessage(getMessagesBoundle().getString("productionCenter.emptyFieldLocation"));
			}
		} catch(Exception ex) {
			ex.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("productionCenter.saveProductionCenterError"), ex);
		}
	}
	
	
	public void wipeLocations(ActionEvent e) {
		System.out.println("Rimuovi tutti gli spettacoli....");	
	}
	
	
	public void deleteLocation() {
		System.out.println("Rimuovi tutti gli spettacoli....");	
		Location tempLocation = locationService.getLocationById(locationToDelete);
		tempLocation.setProductionCenter(null);
		try {
			locationService.updateLocation(tempLocation);
			System.out.println("Numero elementi pre delete: " + productionCenterToEdit.getLocations().size());
			
			//Recupero tutti i locations inseriti all'interno della lista e me li appoggio all'interno di una successiva lisa
			//questo serve per poter resettare le lista nella griglia, dopo di che resetto la lista e metto dentro tutti ad
			//eccezione di quello cancellato
			
			List<Location> listLocation = new ArrayList<Location>();
			for(Location elementLocation : productionCenterToEdit.getLocations()) {
				if(elementLocation.getId() != tempLocation.getId()) {
					listLocation.add(elementLocation);
				}
			}
			productionCenterToEdit.getLocations().clear();
			productionCenterToEdit.setLocations(listLocation);
			presetLocations();
			
			
			System.out.println("Numero elementi post delete: " + productionCenterToEdit.getLocations().size());
		}
		catch(Exception ex) {
			ex.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("location.deletingError"), ex);
		}
		
	}
	
	
	public void confirmDeleteLocation(ActionEvent e) {
		HtmlCommandLink component = (HtmlCommandLink) e.getSource();
		for (UIComponent com : component.getChildren()) {
			if (com instanceof UIParameter && ((UIParameter) com).getName().equalsIgnoreCase("locationId")) {
				locationToDelete = ((Long) ((UIParameter) com).getValue());
			}
		}

	}
	
	
	public void modifyLocation(ActionEvent e) {
		HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("locationId")) {
    			locationToEdit = locationService.getLocationById((Long)((UIParameter)com).getValue());
    			setSelectedProductionCenterLocation(locationToEdit.getId());
    			setActiveTab(0);
    		}
		}
	}
	
	
	
	
	public void saveProductionCenter(ActionEvent e) {
    	System.out.println("Salvo " + productionCenterToEdit);   	

    	if (productionCenterToEdit.getName()==null || productionCenterToEdit.getName().trim().equals("")) {
    		addWarningMessage("test su Descrizione richiesto!");
    		return;
    	}
    	
    	if (selectedDistributionList==null) {
    		addErrorMessage("Lista di distribuzione richiesta!");
    		return;
    	} 
    	
    	try {
    		//Vincenzo Scaccia
    		//Effettuo il salvataggio dell'productionCenter
    		productionCenterToEdit.setDistributionList(distributionListService.getDistributionListById(selectedDistributionList));
    		productionCenterToEdit = ProductionCenterService.updateProductionCenter(productionCenterToEdit);
    		
    		//Recupero le informazioni sui varchi associati
    		if(productionCenterToEdit.getLocations()!=null) {
    			for(Location elementLocation: productionCenterToEdit.getLocations()) {
        			elementLocation.setProductionCenter(productionCenterToEdit);
        			locationService.updateLocation(elementLocation);
        		}
    		}
    		
    		addInfoMessage(getMessagesBoundle().getString("productionCenter.saved"));
    	} catch (PersistenceException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();

    	} catch (DataIntegrityViolationException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();
    	    		
		} catch (Exception e1) {
			addErrorMessage(getMessagesBoundle().getString("productionCenter.savingError"), e1);
			e1.printStackTrace();
		}
    	productionCenterToEdit = null;
	}
	
	private void presetLocations() {
		setProductionCenterLocations(new ArrayList<SelectItem>());
		List<Location> locations = locationService.getLocation();
		getProductionCenterLocations().add(new SelectItem(null," "));
		for (Location element : locations) {
//			if(element.getProductionCenter()==null) {
				SelectItem xx = new SelectItem(element.getId(), element.getDescription()+" - "+element.getLocationType().getDescription());
				getProductionCenterLocations().add(xx);
//			}
		}		
	}
	
	
	//Check Contains Location
	private boolean checkContainsLocation(List<Location> listLocation, Long updateId) {
		for (Location element : listLocation) {
			if (element.getId() == updateId) {
				return true;
			}
		}
		return false;
	}
	
	
	
	
	
	
	
	public void filterByDate() {
		System.out.println("Filter by date !");
		
		Calendar cal = Calendar.getInstance();
		if (filterFrom!=null) {
			cal.setTime(filterFrom);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 1);
			filterFrom = cal.getTime();
		}
		if (filterTo!=null) {
			cal.setTime(filterTo);
			cal.set(Calendar.HOUR_OF_DAY, 23);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);
			filterTo = cal.getTime();
		}
		
		((LazyProductionCenterDataModel)lazyModelProductionCenter).setFilterFrom(filterFrom);
		((LazyProductionCenterDataModel)lazyModelProductionCenter).setFilterTo(filterTo);
	}
	
	public Object getProductionCenterById(long id) {
		return ProductionCenterService.getProductionCenterById(id);
	}

	public ProductionCenter getProductionCenterToEdit() {
		return productionCenterToEdit;
	}

	public void setProductionCenterToEdit(ProductionCenter empToEdit) {
		this.productionCenterToEdit = empToEdit;
	}

	public LazyDataModel<ProductionCenter> getLazyModelProductionCenter() {
		return lazyModelProductionCenter;
	}

	public Date getFilterFrom() {
		return filterFrom;
	}

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public Date getFilterTo() {
		return filterTo;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}

	public List<SelectItem> getProductionCenters() {
		return this.productionCenters;
	}

	public void setProductionCenters(List<SelectItem> productionCenters) {
		this.productionCenters = productionCenters;
	}

	public Long getSelectedProductionCenter() {
		return selectedProductionCenter;
	}

	public void setSelectedProductionCenter(Long selectedProductionCenter) {
		this.selectedProductionCenter = selectedProductionCenter;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public int getActiveTab() {
		return activeTab;
	}

	public Location getLocationToEdit() {
		return locationToEdit;
	}

	public List<SelectItem> getProductionCenterLocations() {
		return productionCenterLocations;
	}

	public void setProductionCenterLocations(List<SelectItem> productionCenterLocations) {
		this.productionCenterLocations = productionCenterLocations;
	}

	public Long getSelectedProductionCenterLocation() {
		return selectedProductionCenterLocation;
	}

	public void setSelectedProductionCenterLocation(Long selectedProductionCenterLocation) {
		this.selectedProductionCenterLocation = selectedProductionCenterLocation;
	}
	
	public void setActiveTab(int activeTab) {
		this.activeTab = activeTab;
	}

	public List<SelectItem> getDistributionLists() {
		return distributionLists;
	}

	public void setDistributionLists(List<SelectItem> distributionLists) {
		this.distributionLists = distributionLists;
	}

	public Long getSelectedDistributionList() {
		return selectedDistributionList;
	}

	public void setSelectedDistributionList(Long selectedDistributionList) {
		this.selectedDistributionList = selectedDistributionList;
	}
	
	
}
