package com.crismasecurity.mediasetStudios.web.management;

import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.crismasecurity.mediasetStudios.core.entity.DocumentSpectator;
import com.crismasecurity.mediasetStudios.core.services.DocumentSpectatorService;

public class LazyDocumentSpectatorDataModel extends LazyDataModel<DocumentSpectator>{


	private static final long serialVersionUID = -5467100994888042129L;

	private DocumentSpectatorService DocumentSpectatorService;
	
	private Long spectatorId;
	
	
	public LazyDocumentSpectatorDataModel(DocumentSpectatorService sptypeservice) {
		this.DocumentSpectatorService = sptypeservice;
	}
	
	@Override
	public DocumentSpectator getRowData(String rowKey) {
		if (rowKey==null || rowKey.trim().equals("")) return null;
		return DocumentSpectatorService.getDocumentSpectatorById(Long.parseLong(rowKey));
	}
	
    @Override
    public Object getRowKey(DocumentSpectator docsp) {
    	return docsp.getId();
    }
    
    @Override 
    public List<DocumentSpectator> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters){
    	// Converto ordinamento
    	int sortInt = 0; // UNSORTED
    	if (sortOrder.equals(SortOrder.ASCENDING)) sortInt = 1;
    	if (sortOrder.equals(SortOrder.DESCENDING)) sortInt = 2;
    	
    	if (spectatorId != null) filters.put("spectator.id", spectatorId);
    	
    	List<DocumentSpectator> sptypelist = DocumentSpectatorService.getDocumentSpectatorLazyLoading(first, pageSize, sortField, sortInt, filters);
    	this.setRowCount((int)DocumentSpectatorService.getDocumentSpectatorLazyLoadingCount(filters));
    	System.out.println("Numero documenti  trovati: " + this.getRowCount());    	
    	return sptypelist;
    	
    }

	public Long getSpectatorId() {
		return spectatorId;
	}

	public void setSpectatorId(Long spectatorId) {
		this.spectatorId = spectatorId;
	}

}
