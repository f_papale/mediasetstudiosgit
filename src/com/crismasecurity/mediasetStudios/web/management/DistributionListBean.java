package com.crismasecurity.mediasetStudios.web.management;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.event.ActionEvent;
import javax.persistence.PersistenceException;

import org.primefaces.model.DualListModel;
import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import com.crismasecurity.mediasetStudios.core.entity.DistributionEmail;
import com.crismasecurity.mediasetStudios.core.entity.DistributionEmailId;
import com.crismasecurity.mediasetStudios.core.entity.DistributionList;
import com.crismasecurity.mediasetStudios.core.entity.EmailContact;
import com.crismasecurity.mediasetStudios.core.services.DistributionEmailService;
import com.crismasecurity.mediasetStudios.core.services.DistributionListService;
import com.crismasecurity.mediasetStudios.core.services.EmailContactService;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;

//@ManagedBean(name= EmployeesBean.BEAN_NAME) // JSF managed
//@SessionScoped // JSF managed
//@ViewScoped // JSF managed

//Spring managed
@Controller (DistributionListBean.BEAN_NAME) 
//@Scope("session")
@Scope("view")
public class DistributionListBean extends BaseBean {


	private static final long serialVersionUID = 5278743136213989941L;

	public static final String BEAN_NAME = "distributionListBean";
    public String getBeanName() { return BEAN_NAME; } 



    private List<EmailContact> sourceEmailContacts;
    private List<EmailContact> targetEmailContacts;
    
    private DualListModel<EmailContact> emailContacts;
    
    @Autowired
    private DistributionListService distributionListService;
    
    @Autowired
    private DistributionEmailService distributionEmailService;
    
    @Autowired
    private EmailContactService emailContactService;
    
    private LazyDataModel<DistributionList> lazyModelDistributionList;
	
	private DistributionList distributionListToEdit = null;
	private Long selectedDistributionList =null;
	
	private Long distributionListToDelete = null;
	
	
	public DistributionListBean() {
		getLog().info("!!!! Costruttore di DistributionListBean !!!");	
    }

	@PostConstruct
	public void init() {
		getLog().info("INIT DistributionListBean!");
		lazyModelDistributionList = new LazyDistributionListDataModel(distributionListService);			
	}

	@PreDestroy
	public void destroy() {
		getLog().info("Destroy DistributionListBean!");

	}
	
	private void presetEmailContacts(Long selectedDistributionList) {
		
//		if (selectedDistributionList!=null) {
			targetEmailContacts = new ArrayList<EmailContact>();
			distributionEmailService.getDistributionEmailFinder().and("distributionList.id").eq(selectedDistributionList).list().forEach(x -> targetEmailContacts.add(x.getEmailContact()));
			sourceEmailContacts=emailContactService.getEmailContact();
			sourceEmailContacts.removeAll(targetEmailContacts);

			setEmailContacts(new DualListModel<EmailContact>(sourceEmailContacts, targetEmailContacts));
//		}
	}
	

	public void confirmDeleteDistributionList(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("DistributionListId")) {
    			distributionListToDelete = ((Long)((UIParameter)com).getValue());
			}
		}
	}
	
	public void cancelDelete(ActionEvent ev) {
	    	System.out.println("\nDelete DistributionList canceled.");
	    	distributionListToDelete= null;
	}
	
	public void delete(ActionEvent ev) {
    	System.out.println("\nDelete Test ...");
    	try {
        	
    		distributionListService.deleteDistributionList(distributionListToDelete);
    		addInfoMessage(getMessagesBoundle().getString("distributionList.deleted"));
		} catch (Exception e) {
			e.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("distributionList.deletingError"), e);
		}
	}
	
	public void addDistributionList(ActionEvent e) {
		distributionListToEdit = new DistributionList();
		selectedDistributionList=null;
		presetEmailContacts(selectedDistributionList);
		
	}
	
	public void modifyDistributionList(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("DistributionListId")) {
    			distributionListToEdit = distributionListService.getDistributionListById((Long)((UIParameter)com).getValue());
    			selectedDistributionList=distributionListToEdit.getId();
    			presetEmailContacts(selectedDistributionList);
    			
			}
		}
	}
	
	
	
	
	public EmailContact getEmailContactById(Long id) {
		
		return emailContactService.getEmailContactById(id);
	}
	
	
	public void cancelEditDistributionList(ActionEvent e) {
		distributionListToEdit = null;
	}
	
	public void saveDistributionList(ActionEvent e) {
    	System.out.println("Salvo " + distributionListToEdit);
    	

    	if (distributionListToEdit.getName()==null || distributionListToEdit.getName().trim().equals("")) {
    		addWarningMessage("Nome richiesto!");
    		return;
    	}
    	
    	if (distributionListToEdit.getDescription()==null || distributionListToEdit.getDescription().trim().equals("")) {
    		addWarningMessage("Descrizione richiesta!");
    		return;
    	}
    	
    	try {
    		if (distributionListToEdit.getId()!=null) {
    			
    			distributionEmailService.deleteDistributionEmailByIdDL(distributionListToEdit.getId());
    			distributionListToEdit = distributionListService.updateDistributionList(distributionListToEdit);
    		} else {
    			distributionListToEdit = distributionListService.addDistributionList(distributionListToEdit);
    		}
    		for (EmailContact emailItem : getEmailContacts().getTarget()) {
    			DistributionEmail distributionEmail = new DistributionEmail();
    			distributionEmail.setEmailContact(emailItem);
    			distributionEmail.setDistributionList(distributionListToEdit);
    			DistributionEmailId id = new DistributionEmailId(distributionListToEdit.getId(),emailItem.getId());
    			distributionEmail.setId(id);
    			distributionEmail=distributionEmailService.updateDistributionEmail(distributionEmail);
			}
    		distributionListToEdit = distributionListService.getDistributionListById(distributionListToEdit.getId());
    		
			addInfoMessage(getMessagesBoundle().getString("distributionList.saved"));
    	} catch (PersistenceException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();

    	} catch (DataIntegrityViolationException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();
    				
		} catch (Exception e1) {
			addErrorMessage(getMessagesBoundle().getString("distributionList.savingError"), e1);
			e1.printStackTrace();
		}
    	distributionListToEdit = null;
	}
	
	public Object getDistributionListById(long id) {
		return distributionListService.getDistributionListById(id);
	}

	public DistributionList getDistributionListToEdit() {
		return distributionListToEdit;
	}

	public void setDistributionListToEdit(DistributionList empToEdit) {
		this.distributionListToEdit = empToEdit;
	}

	public LazyDataModel<DistributionList> getLazyModelDistributionList() {
		return lazyModelDistributionList;
	}

	public Long getselectedDistributionList() {
		return selectedDistributionList;
	}

	public void setselectedDistributionList(Long selectedDistributionList) {
		this.selectedDistributionList = selectedDistributionList;
	}

	public DualListModel<EmailContact> getEmailContacts() {
		return emailContacts;
	}

	public void setEmailContacts(DualListModel<EmailContact> emailContacts) {
		this.emailContacts = emailContacts;
	}

	public boolean haveUserRole(String roles) {
		
		String[] listRoles = roles.split(",");
		for (int i = 0; i < listRoles.length; i++) {
			listRoles[i] = listRoles[i].trim();
			
		}
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication!=null && authentication.getAuthorities()!=null) {
			//String currentPrincipalName = authentication.getName();
			for (GrantedAuthority r : authentication.getAuthorities()) {
				//System.out.println("Ruolo:"+r.getAuthority());
				for (int i = 0; i < listRoles.length; i++) {
					if (r.getAuthority()!=null && r.getAuthority().equals(listRoles[i])) {
						//System.out.println("trovato ruolo !!!");
						return true;
					}					
				}
			}
		}
		return false;
	}	
	
}
