package com.crismasecurity.mediasetStudios.web.management;

import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.crismasecurity.mediasetStudios.core.entity.Apparatus;
import com.crismasecurity.mediasetStudios.core.services.ApparatusService;

public class LazyApparatusDataModel extends LazyDataModel<Apparatus>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6305865097946016528L;
	
	private ApparatusService apparatusService; 
	
	
	public LazyApparatusDataModel(ApparatusService apparatusService) {
		this.apparatusService = apparatusService;
	}
	
	@Override
	public Apparatus getRowData(String rowKey) {
    	if (rowKey==null || rowKey.trim().equals("")) return null;
    	return apparatusService.getApparatusById(Long.parseLong(rowKey));
	}
	
	@Override
	public Object getRowKey(Apparatus ap) {
		return ap.getId();
	}
	
	@Override
	public List<Apparatus> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters){
    	// Converto ordinamento
    	int sortInt =0; // UNSORTED
    	if (sortOrder.equals(SortOrder.ASCENDING)) sortInt = 1;
    	if (sortOrder.equals(SortOrder.DESCENDING)) sortInt = 2;
    	
    	List<Apparatus> listsp = apparatusService.getApparatusLazyLoading(first, pageSize, sortField, sortInt, filters);
    	this.setRowCount((int)apparatusService.getApparatusLazyLoadingCount(filters));
    	
    	System.out.println("Numero Lazy " + this.getRowCount());
    	
    	return listsp;
	}

	
	
	
}

