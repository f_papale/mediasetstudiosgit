package com.crismasecurity.mediasetStudios.web.management;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.crismasecurity.mediasetStudios.core.entity.CountryCode;
import com.crismasecurity.mediasetStudios.core.services.CountryCodeService;
 

public class LazyCountryCodeDataModel extends LazyDataModel<CountryCode> {


	/**
	 * 
	 */
	private static final long serialVersionUID = 8946314431632827802L;

	private CountryCodeService countryCodeService;
	
	private Date filterFrom;
	private Date filterTo;

     
    public LazyCountryCodeDataModel(CountryCodeService countryCodeService) {
        this.countryCodeService = countryCodeService;
    }
     
    @Override
    public CountryCode getRowData(String rowKey) {
    	if (rowKey==null || rowKey.trim().equals("")) return null;
    	return countryCodeService.getCountryCodeById(Long.parseLong(rowKey));
    }
 
    @Override
    public Object getRowKey(CountryCode audit) {
        return audit.getId();
    }
 
    @Override
    public List<CountryCode> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
    	
    	// Converto ordinamento
    	int sortInt =0; // UNSORTED
    	if (sortOrder.equals(SortOrder.ASCENDING)) sortInt = 1;
    	if (sortOrder.equals(SortOrder.DESCENDING)) sortInt = 2;
    	
 
		if (filterFrom!=null) filters.put("date#ge", filterFrom);
		if (filterTo!=null) filters.put("date#le", filterTo);

    	
    	List<CountryCode> ret = countryCodeService.getCountryCodeLazyLoading(first, pageSize, sortField, sortInt, filters);
    	this.setRowCount((int)countryCodeService.getCountryCodeLazyLoadingCount(filters));
    	System.out.println("Numero accident Type trovati: " + this.getRowCount());
//    	for (Audit audit : ret) {
//			System.out.println("Trovato: " + audit);
//		}
    	return ret;    
    }

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}


}