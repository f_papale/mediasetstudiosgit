package com.crismasecurity.mediasetStudios.web.management;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.crismasecurity.mediasetStudios.core.entity.SeverityLevel;
import com.crismasecurity.mediasetStudios.core.services.SeverityLevelService;
 

public class LazySeverityLevelDataModel extends LazyDataModel<SeverityLevel> {


	/**
	 * 
	 */
	private static final long serialVersionUID = -3223618413219666435L;

	private SeverityLevelService severityLevelService;
	
	private Date filterFrom;
	private Date filterTo;

     
    public LazySeverityLevelDataModel(SeverityLevelService severityLevelService) {
        this.severityLevelService = severityLevelService;
    }
     
    @Override
    public SeverityLevel getRowData(String rowKey) {
    	if (rowKey==null || rowKey.trim().equals("")) return null;
    	return severityLevelService.getSeverityLevelById(Long.parseLong(rowKey));
    }
 
    @Override
    public Object getRowKey(SeverityLevel audit) {
        return audit.getId();
    }
 
    @Override
    public List<SeverityLevel> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
    	
    	// Converto ordinamento
    	int sortInt =0; // UNSORTED
    	if (sortOrder.equals(SortOrder.ASCENDING)) sortInt = 1;
    	if (sortOrder.equals(SortOrder.DESCENDING)) sortInt = 2;
    	
 
		if (filterFrom!=null) filters.put("date#ge", filterFrom);
		if (filterTo!=null) filters.put("date#le", filterTo);

    	
    	List<SeverityLevel> ret = severityLevelService.getSeverityLevelLazyLoading(first, pageSize, sortField, sortInt, filters);
    	this.setRowCount((int)severityLevelService.getSeverityLevelLazyLoadingCount(filters));
    	System.out.println("Numero di severitylevel trovati: " + this.getRowCount());
//    	for (Audit audit : ret) {
//			System.out.println("Trovato: " + audit);
//		}
    	return ret;    
    }

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}


}