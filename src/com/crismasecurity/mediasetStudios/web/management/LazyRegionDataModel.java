package com.crismasecurity.mediasetStudios.web.management;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.crismasecurity.mediasetStudios.core.entity.Region;
import com.crismasecurity.mediasetStudios.core.services.RegionService;
 

public class LazyRegionDataModel extends LazyDataModel<Region> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2884731038249080910L;

	private RegionService regionService;
	
	private Date filterFrom;
	private Date filterTo;

     
    public LazyRegionDataModel(RegionService regionService) {
        this.regionService = regionService;
    }
     
    @Override
    public Region getRowData(String rowKey) {
    	if (rowKey==null || rowKey.trim().equals("")) return null;
    	return regionService.getRegionById(Long.parseLong(rowKey));
    }
 
    @Override
    public Object getRowKey(Region audit) {
        return audit.getId();
    }
 
    @Override
    public List<Region> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
    	
    	// Converto ordinamento
    	int sortInt =0; // UNSORTED
    	if (sortOrder.equals(SortOrder.ASCENDING)) sortInt = 1;
    	if (sortOrder.equals(SortOrder.DESCENDING)) sortInt = 2;
    	
 
		if (filterFrom!=null) filters.put("date#ge", filterFrom);
		if (filterTo!=null) filters.put("date#le", filterTo);

    	
    	List<Region> ret = regionService.getRegionLazyLoading(first, pageSize, sortField, sortInt, filters);
    	this.setRowCount((int)regionService.getRegionLazyLoadingCount(filters));
    	System.out.println("Numero di region trovati: " + this.getRowCount());
    	return ret;    
    }

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}


}