package com.crismasecurity.mediasetStudios.web.management;

import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.crismasecurity.mediasetStudios.core.entity.ApparatusType;
import com.crismasecurity.mediasetStudios.core.services.ApparatusTypeService;

public class LazyApparatusTypeDataModel extends LazyDataModel<ApparatusType>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2960910410611093828L;
	
	private ApparatusTypeService apparatusTypeService;
	
	public LazyApparatusTypeDataModel(ApparatusTypeService appTypeservice) {
		this.apparatusTypeService = appTypeservice;
	}
	
	@Override
	public ApparatusType getRowData(String rowKey) {
		if (rowKey==null || rowKey.trim().equals("")) return null;
		return apparatusTypeService.getApparatusTypeById(Long.parseLong(rowKey));
	}
	
    @Override
    public Object getRowKey(ApparatusType aptype) {
    	return aptype.getId();
    }
    
    @Override 
    public List<ApparatusType> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters){
    	// Converto ordinamento
    	int sortInt = 0; // UNSORTED
    	if (sortOrder.equals(SortOrder.ASCENDING)) sortInt = 1;
    	if (sortOrder.equals(SortOrder.DESCENDING)) sortInt = 2;
    	
    	List<ApparatusType> sptypelist = apparatusTypeService.getApparatusTypeLazyLoading(first, pageSize, sortField, sortInt, filters);
    	this.setRowCount((int)apparatusTypeService.getApparatusTypeLoadingCount(filters));
//    	System.out.println("Numero elementi trovati: " + this.getRowCount());    	
    	return sptypelist;
    	
    }

}
