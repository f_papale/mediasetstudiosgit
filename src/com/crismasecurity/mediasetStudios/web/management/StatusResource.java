package com.crismasecurity.mediasetStudios.web.management;


import org.primefaces.push.annotation.OnMessage;
import org.primefaces.push.annotation.PathParam;
import org.primefaces.push.annotation.PushEndpoint;
import org.primefaces.push.annotation.Singleton;
import org.primefaces.push.impl.JSONEncoder;

@PushEndpoint("/status/{sessionId}")
@Singleton
public class StatusResource {
	
	@PathParam("sessionId")
	private String sessionId;
	
	
	public StatusResource() {
		
	}
	
    @OnMessage(encoders = {JSONEncoder.class})
    public String onMessage(String count) {
        return count;
    }
}
