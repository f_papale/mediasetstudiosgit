package com.crismasecurity.mediasetStudios.web.management;

import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.crismasecurity.mediasetStudios.core.entity.Location;
import com.crismasecurity.mediasetStudios.core.services.LocationService;

public class LazyLocationDataModel extends LazyDataModel<Location>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2960910410611093828L;
	
	private LocationService locationService;
	
	public LazyLocationDataModel(LocationService locService) {
		this.locationService = locService;
	}
	
	@Override
	public Location getRowData(String rowKey) {
		if (rowKey==null || rowKey.trim().equals("")) return null;
		return locationService.getLocationById(Long.parseLong(rowKey));
	}
	
    @Override
    public Object getRowKey(Location lcType) {
    	return lcType.getId();
    }
    
    @Override 
    public List<Location> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters){
    	// Converto ordinamento
    	int sortInt = 0; // UNSORTED
    	if (sortOrder.equals(SortOrder.ASCENDING)) sortInt = 1;
    	if (sortOrder.equals(SortOrder.DESCENDING)) sortInt = 2;
    	
    	List<Location> spList = locationService.getLocationLazyLoading(first, pageSize, sortField, sortInt, filters);
    	this.setRowCount((int)locationService.getLocationLazyLoadingCount(filters));
//    	System.out.println("Numero elementi trovati: " + this.getRowCount());    	
    	return spList;
    	
    }

}
