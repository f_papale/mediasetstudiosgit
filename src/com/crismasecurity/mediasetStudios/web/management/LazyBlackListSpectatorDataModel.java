package com.crismasecurity.mediasetStudios.web.management;

import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.crismasecurity.mediasetStudios.core.entity.BlackListSpectator;
import com.crismasecurity.mediasetStudios.core.services.BlackListSpectatorService;

public class LazyBlackListSpectatorDataModel extends LazyDataModel<BlackListSpectator>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2960910410611093828L;
	
	private BlackListSpectatorService blackListSpectatorService;
	
	public LazyBlackListSpectatorDataModel(BlackListSpectatorService blackListSpectatorService) {
		this.blackListSpectatorService = blackListSpectatorService;
	}
	
	@Override
	public BlackListSpectator getRowData(String rowKey) {
		if (rowKey==null || rowKey.trim().equals("")) return null;
		return blackListSpectatorService.getBlackListSpectatorById(Long.parseLong(rowKey));
	}
	
    @Override
    public Object getRowKey(BlackListSpectator blackListSpectator) {
    	return blackListSpectator.getIdBlackListSpectator();
    }
    
    @Override 
    public List<BlackListSpectator> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters){
    	// Converto ordinamento
    	int sortInt = 0; // UNSORTED
    	if (sortOrder.equals(SortOrder.ASCENDING)) sortInt = 1;
    	if (sortOrder.equals(SortOrder.DESCENDING)) sortInt = 2;
    	List<BlackListSpectator> sptypelist = blackListSpectatorService.getBlackListSpectatorLazyLoading(first, pageSize, sortField, sortInt, filters);
    	this.setRowCount((int)blackListSpectatorService.getBlackListLoadingCount(filters));
//    	System.out.println("Numero elementi trovati: " + this.getRowCount());    	
    	return sptypelist;
    	
    }

}
