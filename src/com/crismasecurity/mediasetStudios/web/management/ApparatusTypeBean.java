package com.crismasecurity.mediasetStudios.web.management;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.event.ActionEvent;
import javax.persistence.PersistenceException;

import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import com.crismasecurity.mediasetStudios.core.entity.ApparatusType;
import com.crismasecurity.mediasetStudios.core.services.ApparatusTypeService;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;

//Spring managed
@Controller (ApparatusTypeBean.BEAN_NAME) 
//@Scope("session")
@Scope("view")
public class ApparatusTypeBean extends BaseBean{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5662158989060606869L;
	
	public static final String BEAN_NAME = "apparatusTypeBean";
	public String getBeanName() { return BEAN_NAME; } 

	@Autowired
	private ApparatusTypeService apparatusTypeService;

	private LazyDataModel<ApparatusType> lazyModelApparatusType;
	private ApparatusType apparatusTypeToEdit = null;
	private Long apparatusTypeToDelete = null;

	
	
	public ApparatusTypeBean() {
		getLog().info("!!!! Costruttore di ApparatusTypeBean !!!");
	}
	
	@PostConstruct
	public void init() {
		getLog().info("INIT ApparatusTypeBean!");
		lazyModelApparatusType = new LazyApparatusTypeDataModel(apparatusTypeService);
	}
	
	@PreDestroy
	public void destroy() {
		getLog().info("Destroy ApparatusTypeBean!");
	}
	
	public void confirmDeleteApparatusType(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("apparatusTypeId")) {
    			apparatusTypeToDelete = ((Long)((UIParameter)com).getValue());
			}
		}    	
	}
	
	public void cancelDelete(ActionEvent ev) {
    	System.out.println("\nDelete SpectatorType canceled.");
    	apparatusTypeToDelete= null;
	}
	
	public void delete(ActionEvent ev) {
    	System.out.println("\nDelete Test ...");
    	try {
    		apparatusTypeService.deleteApparatusType(apparatusTypeToDelete);
    		addInfoMessage(getMessagesBoundle().getString("apparatusType.deleted"));
		} catch (Exception e) {
			e.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("apparatusType.deletingError"), e);
		}
    }
	
	public void addApparatusType(ActionEvent e) {
		apparatusTypeToEdit = new ApparatusType();
	}
	
	public void modifyApparatusType(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("apparatusTypeId")) {
    			apparatusTypeToEdit = apparatusTypeService.getApparatusTypeById((Long)((UIParameter)com).getValue());
    		}
		}
	}
	
	public boolean haveUserRole(String roles) {
		
		String[] listRoles = roles.split(",");
		for (int i = 0; i < listRoles.length; i++) {
			listRoles[i] = listRoles[i].trim();
			
		}
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication!=null && authentication.getAuthorities()!=null) {
			//String currentPrincipalName = authentication.getName();
			for (GrantedAuthority r : authentication.getAuthorities()) {
				//System.out.println("Ruolo:"+r.getAuthority());
				for (int i = 0; i < listRoles.length; i++) {
					if (r.getAuthority()!=null && r.getAuthority().equals(listRoles[i])) {
						//System.out.println("trovato ruolo !!!");
						return true;
					}					
				}
			}
		}
		return false;
	}	
	
	public void cancelEditApparatusType(ActionEvent e) {
		apparatusTypeToEdit = null;
	}
	
	public void saveApparatusType(ActionEvent e) {
    	System.out.println("Salvo " + apparatusTypeToEdit);
    	

    	if (apparatusTypeToEdit.getDescription()==null || apparatusTypeToEdit.getDescription().trim().equals("")) {
    		addWarningMessage("test su Descrizione richiesto!");
    		return;
    	}
    	try {
    		apparatusTypeToEdit = apparatusTypeService.updateApparatusType(apparatusTypeToEdit);
    		addInfoMessage(getMessagesBoundle().getString("apparatusType.saved"));
    	} catch (PersistenceException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();

    	} catch (DataIntegrityViolationException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();
    	    		
		} catch (Exception e1) {
			addErrorMessage(getMessagesBoundle().getString("apparatusType.savingError"), e1);
			e1.printStackTrace();
		}
    	apparatusTypeToEdit = null;
	}
	
	public Object getSpectatorTypeById(long id) {
		return apparatusTypeService.getApparatusTypeById(id);
	}

	public ApparatusType getApparatusTypeToEdit() {
		return apparatusTypeToEdit;
	}

	public void setApparatusTypeToEdit(ApparatusType empToEdit) {
		this.apparatusTypeToEdit = empToEdit;
	}

	public LazyDataModel<ApparatusType> getLazyModelApparatusType() {
		return lazyModelApparatusType;
	}
	
}
