package com.crismasecurity.mediasetStudios.web.management;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.crismasecurity.mediasetStudios.core.entity.Province;
import com.crismasecurity.mediasetStudios.core.services.ProvinceService;
 

public class LazyProvinceDataModel extends LazyDataModel<Province> {


	/**
	 * 
	 */
	private static final long serialVersionUID = 8946314431632827802L;

	private ProvinceService provinceService;
	
	private Date filterFrom;
	private Date filterTo;

     
    public LazyProvinceDataModel(ProvinceService provinceService) {
        this.provinceService = provinceService;
    }
     
    @Override
    public Province getRowData(String rowKey) {
    	if (rowKey==null || rowKey.trim().equals("")) return null;
    	return provinceService.getProvinceById(Long.parseLong(rowKey));
    }
 
    @Override
    public Object getRowKey(Province audit) {
        return audit.getId();
    }
 
    @Override
    public List<Province> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
    	
    	// Converto ordinamento
    	int sortInt =0; // UNSORTED
    	if (sortOrder.equals(SortOrder.ASCENDING)) sortInt = 1;
    	if (sortOrder.equals(SortOrder.DESCENDING)) sortInt = 2;
    	
 
		if (filterFrom!=null) filters.put("date#ge", filterFrom);
		if (filterTo!=null) filters.put("date#le", filterTo);

    	
    	List<Province> ret = provinceService.getProvinceLazyLoading(first, pageSize, sortField, sortInt, filters);
    	this.setRowCount((int)provinceService.getProvinceLazyLoadingCount(filters));
    	System.out.println("Numero province Type trovati: " + this.getRowCount());
//    	for (Audit audit : ret) {
//			System.out.println("Trovato: " + audit);
//		}
    	return ret;    
    }

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}


}