package com.crismasecurity.mediasetStudios.web.management;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.persistence.PersistenceException;

import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import com.crismasecurity.mediasetStudios.core.entity.Apparatus;
import com.crismasecurity.mediasetStudios.core.entity.ApparatusType;
import com.crismasecurity.mediasetStudios.core.services.ApparatusService;
import com.crismasecurity.mediasetStudios.core.services.ApparatusTypeService;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;

@Controller (ApparatusBean.BEAN_NAME)
@Scope("view")
public class ApparatusBean extends BaseBean{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5437322277469086379L;
	
	public static final String BEAN_NAME = "apparatusBean";
    public String getBeanName() { return BEAN_NAME; } 

    @Autowired
    private ApparatusService apparatusService;
    
    @Autowired
    private ApparatusTypeService apparatusTypeService;
    
    private LazyDataModel<Apparatus> lazyModelApparatus;
    private List<SelectItem> apparatusTypes;
    private Long selectedApparatusType = null;
    private Apparatus apparatusToEdit = null;
    private Long apparatusToDelete = null; 

    public ApparatusBean() {
    	getLog().info("!!!! Costruttore di ApparatusBean !!!");
    }
    
    @PostConstruct
	public void init() {
		getLog().info("INIT ApparatusBean!");
		getLog().info("getApparatus!\n");
		System.out.println(apparatusService.getApparatus());
		presetApparatusList();
		/*if (apparatusService.getApparatus().isEmpty() || apparatusService.getApparatus() == null) {
			lazyModelApparatus = null;
		} else {
			
		}*/
		lazyModelApparatus = new LazyApparatusDataModel(apparatusService);

	}

	@PreDestroy
	public void destroy() {
		getLog().info("Destroy ApparatusBean!");

	}

	private void presetApparatusList() {
		apparatusTypes = new ArrayList<SelectItem>();
		List<ApparatusType> apTypeList = apparatusTypeService.getApparatusType();
		apparatusTypes.add(new SelectItem(null, " "));
		for (ApparatusType apType : apTypeList) {
			SelectItem xx = new SelectItem(apType.getId(), apType.getDescription());
			apparatusTypes.add(xx);
		}
	}

	public void confirmDeleteApparatus(ActionEvent e) {
		HtmlCommandLink component = (HtmlCommandLink) e.getSource();
		for (UIComponent com : component.getChildren()) {
			if (com instanceof UIParameter && ((UIParameter) com).getName().equalsIgnoreCase("apparatusId")) {
				apparatusToDelete = ((Long) ((UIParameter) com).getValue());
			}
		}

	}

	public void cancelDelete(ActionEvent ev) {
		System.out.println("\nDelete Apparatus canceled.");
		apparatusToDelete = null;
	}

	public void delete(ActionEvent ev) {
		System.out.println("\nDelete Test ...");
		try {
			apparatusService.deleteApparatus(apparatusToDelete);
			addInfoMessage(getMessagesBoundle().getString("apparatus.deleted"));
		} catch (Exception e) {
			e.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("apparatus.deletingError"), e);
		}
	}

	public void addApparatus(ActionEvent e) {
		apparatusToEdit = new Apparatus();
		selectedApparatusType = null;
	}

	public void modifyApparatus(ActionEvent e) {
		HtmlCommandLink component = (HtmlCommandLink) e.getSource();
		for (UIComponent com : component.getChildren()) {
			if (com instanceof UIParameter && ((UIParameter) com).getName().equalsIgnoreCase("apparatusId")) {
				apparatusToEdit = apparatusService.getApparatusById((Long) ((UIParameter) com).getValue());
				selectedApparatusType = apparatusToEdit.getApparatusType().getId();

			}
		}
	}

	public boolean haveUserRole(String roles) {
		
		String[] listRoles = roles.split(",");
		for (int i = 0; i < listRoles.length; i++) {
			listRoles[i] = listRoles[i].trim();
			
		}
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication!=null && authentication.getAuthorities()!=null) {
			//String currentPrincipalName = authentication.getName();
			for (GrantedAuthority r : authentication.getAuthorities()) {
				//System.out.println("Ruolo:"+r.getAuthority());
				for (int i = 0; i < listRoles.length; i++) {
					if (r.getAuthority()!=null && r.getAuthority().equals(listRoles[i])) {
						//System.out.println("trovato ruolo !!!");
						return true;
					}					
				}
			}
		}
		return false;
	}	
	
	public void cancelEditApparatus(ActionEvent e) {
		apparatusToEdit = null;
	}

	public void saveApparatus(ActionEvent e) {
		System.out.println("Salvo " + apparatusToEdit);
		System.out.println("descrizione azione " + e);
		
		if (apparatusToEdit.getDescription() == null || apparatusToEdit.getDescription().trim().equals("")) {
			addWarningMessage("test su Descrizione richiesto!");
			return;
		}
		
		if (apparatusToEdit.getFeatures() == null || apparatusToEdit.getFeatures().trim().equals("")) {
			addWarningMessage("test su Caratteristiche richiesto!");
			return;
		}
		
		if (selectedApparatusType == null) {
			addWarningMessage("test su Tipo Apparato richiesto!");
			return;
		}

		try {
			// Imposta il valore della dropdown list nella classe
			apparatusToEdit.setApparatusType(apparatusTypeService.getApparatusTypeById(selectedApparatusType));
			apparatusToEdit = apparatusService.updateApparatus(apparatusToEdit);
			addInfoMessage(getMessagesBoundle().getString("apparatus.saved"));
    	} catch (PersistenceException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();

    	} catch (DataIntegrityViolationException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();
    				
		} catch (Exception e1) {
			addErrorMessage(getMessagesBoundle().getString("apparatus.savingError"), e1);
			e1.printStackTrace();
		}
		apparatusToEdit = null;
	}

	public Apparatus getApparatusToEdit() {
		return apparatusToEdit;
	}

	public void setApparatusToEdit(Apparatus apparatusToEdit) {
		this.apparatusToEdit = apparatusToEdit;
	}

	public Object getApparatusById(long id) {
		return apparatusService.getApparatusById(id);
	}

	public LazyDataModel<Apparatus> getLazyModelApparatus() {
		return lazyModelApparatus;
	}

	public List<SelectItem> getApparatusTypes() {
		return apparatusTypes;
	}

	public void setApparatusTypes(List<SelectItem> apparatusTypes) {
		this.apparatusTypes = apparatusTypes;
	}

	public Long getSelectedApparatusType() {
		return selectedApparatusType;
	}

	public void setSelectedApparatusType(Long selectedApparatusType) {
		this.selectedApparatusType = selectedApparatusType;
	}
}
