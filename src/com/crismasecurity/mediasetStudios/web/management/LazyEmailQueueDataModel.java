package com.crismasecurity.mediasetStudios.web.management;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.crismasecurity.mediasetStudios.core.entity.EmailQueue;
import com.crismasecurity.mediasetStudios.core.services.EmailQueueService;
 

public class LazyEmailQueueDataModel extends LazyDataModel<EmailQueue> {



	/**
	 * 
	 */
	private static final long serialVersionUID = 5742265119638870552L;

	private EmailQueueService emailQueueService;
	
	private Date filterFrom;
	private Date filterTo;

     
    public LazyEmailQueueDataModel(EmailQueueService emailQueueService) {
        this.emailQueueService = emailQueueService;
    }
     
    @Override
    public EmailQueue getRowData(String rowKey) {
    	if (rowKey==null || rowKey.trim().equals("")) return null;
    	return emailQueueService.getEmailQueueById(Long.parseLong(rowKey));
    }
 
    @Override
    public Object getRowKey(EmailQueue audit) {
        return audit.getId();
    }
 
    @Override
    public List<EmailQueue> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
    	
    	// Converto ordinamento
    	int sortInt =0; // UNSORTED
    	if (sortOrder.equals(SortOrder.ASCENDING)) sortInt = 1;
    	if (sortOrder.equals(SortOrder.DESCENDING)) sortInt = 2;
    	
 
		if (filterFrom!=null) filters.put("date#ge", filterFrom);
		if (filterTo!=null) filters.put("date#le", filterTo);

    	
    	List<EmailQueue> ret = emailQueueService.getEmailQueueLazyLoading(first, pageSize, sortField, sortInt, filters);
    	this.setRowCount((int)emailQueueService.getEmailQueueLazyLoadingCount(filters));
//    	System.out.println("Numero elementi trovati: " + this.getRowCount());
//    	for (Audit audit : ret) {
//			System.out.println("Trovato: " + audit);
//		}
    	return ret;    
    }

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}


}