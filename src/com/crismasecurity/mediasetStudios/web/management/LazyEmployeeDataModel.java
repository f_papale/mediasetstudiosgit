package com.crismasecurity.mediasetStudios.web.management;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.crismasecurity.mediasetStudios.core.entity.Employee;
import com.crismasecurity.mediasetStudios.core.services.EmployeeService;
 

public class LazyEmployeeDataModel extends LazyDataModel<Employee> {

	private static final long serialVersionUID = -6436030098787644324L;

	private EmployeeService employeeService;
	
	private Date filterFrom;
	private Date filterTo;
   
    public LazyEmployeeDataModel(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }
     
    @Override
    public Employee getRowData(String rowKey) {
    	if (rowKey==null || rowKey.trim().equals("")) return null;
    	return employeeService.getEmployeeById(Long.parseLong(rowKey));
    }
 
    @Override
    public Object getRowKey(Employee audit) {
        return audit.getId();
    }
 
    @Override
    public List<Employee> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
    	
    	// Converto ordinamento
    	int sortInt =0; // UNSORTED
    	if (sortOrder.equals(SortOrder.ASCENDING)) sortInt = 1;
    	if (sortOrder.equals(SortOrder.DESCENDING)) sortInt = 2;
    	
 
		if (filterFrom!=null) filters.put("date#ge", filterFrom);
		if (filterTo!=null) filters.put("date#le", filterTo);

    	
    	List<Employee> ret = employeeService.getEmployeeLazyLoading(first, pageSize, sortField, sortInt, filters);
    	this.setRowCount((int)employeeService.getEmployeeLazyLoadingCount(filters));
//    	System.out.println("Numero elementi trovati: " + this.getRowCount());
//    	for (Audit audit : ret) {
//			System.out.println("Trovato: " + audit);
//		}
    	return ret;    
    }

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}


}