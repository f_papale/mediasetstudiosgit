package com.crismasecurity.mediasetStudios.web.management;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.event.ActionEvent;
import javax.persistence.PersistenceException;

import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import com.crismasecurity.mediasetStudios.core.entity.BlackListType;
import com.crismasecurity.mediasetStudios.core.services.BlackListTypeService;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;

//Spring managed
@Controller (BlackListTypeBean.BEAN_NAME) 
//@Scope("session")
@Scope("view")
public class BlackListTypeBean extends BaseBean{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5662158989060606869L;
	
	public static final String BEAN_NAME = "blackListTypeBean";
	public String getBeanName() { return BEAN_NAME; } 

	@Autowired
	private BlackListTypeService blackListTypeService;

	private LazyDataModel<BlackListType> lazyModelBlackListType;
	private BlackListType blackListTypeToEdit = null;
	private Long blackListTypeToDelete = null;

	
	
	public BlackListTypeBean() {
		getLog().info("!!!! Costruttore di BlackListTypeBean !!!");
	}
	
	@PostConstruct
	public void init() {
		getLog().info("INIT ApparatusTypeBean!");
		lazyModelBlackListType = new LazyBlackListTypeDataModel(blackListTypeService);
	}
	
	@PreDestroy
	public void destroy() {
		getLog().info("Destroy BlackListTypeBean!");
	}
	
	public void confirmDeleteBlackListType(ActionEvent e) {
  	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
  	for (UIComponent com : component.getChildren()) {
  		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("blackListTypeId")) {
  			blackListTypeToDelete = ((Long)((UIParameter)com).getValue());
			}
		}    	
	}
	
	public void cancelDelete(ActionEvent ev) {
		System.out.println("\nDelete BlackListType canceled.");
		blackListTypeToDelete = null;
	}
	
	public void delete(ActionEvent ev) {
  	System.out.println("\nDelete Test ...");
  	try {
  		blackListTypeService.deleteBlackListType(blackListTypeToDelete);
  		addInfoMessage(getMessagesBoundle().getString("blackListType.deleted"));
		} catch (Exception e) {
			e.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("blackListType.deletingError"), e);
		}
  }
	
	public void addBlackListType(ActionEvent e) {
		blackListTypeToEdit = new BlackListType();
	}
	
	public void modifyBlackListType(ActionEvent e) {
  	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
  	for (UIComponent com : component.getChildren()) {
  		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("blackListTypeId")) {
  			blackListTypeToEdit = blackListTypeService.getBlackListTypeById((Long)((UIParameter)com).getValue());
  			}
		}
	}
	
	public void cancelEditBlackListType(ActionEvent e) {
		blackListTypeToEdit = null;
	}
	
	public void saveBlackListType(ActionEvent e) {
		
		System.out.println("Salvo " + blackListTypeToEdit);
		if (blackListTypeToEdit.getDescription()==null || blackListTypeToEdit.getDescription().trim().equals("")) {
	  		addWarningMessage("test su Descrizione richiesto!");
	  		return;
	  	}
	  	try {
	  		blackListTypeToEdit = blackListTypeService.updateBlackListType(blackListTypeToEdit);
	  		addInfoMessage(getMessagesBoundle().getString("apparatusType.saved"));
	    	} catch (PersistenceException hibernateEx){
	    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
	    		hibernateEx.printStackTrace();
	
	    	} catch (DataIntegrityViolationException hibernateEx){
	    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
	    		hibernateEx.printStackTrace();
    	
			} catch (Exception e1) {
				addErrorMessage(getMessagesBoundle().getString("apparatusType.savingError"), e1);
				e1.printStackTrace();
			}
	  	blackListTypeToEdit = null;
	}
	
	public Object getBlackListTypeById(long id) {
		return blackListTypeService.getBlackListTypeById(id);
	}

	public BlackListType getBlackListTypeToEdit() {
		return blackListTypeToEdit;
	}

	public void setBlackListTypeToEdit(BlackListType empToEdit) {
		this.blackListTypeToEdit = empToEdit;
	}

	public LazyDataModel<BlackListType> getLazyModelBlackListType() {
		return lazyModelBlackListType;
	}
	
	public boolean haveUserRole(String roles) {
		
		String[] listRoles = roles.split(",");
		for (int i = 0; i < listRoles.length; i++) {
			listRoles[i] = listRoles[i].trim();
			
		}
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication!=null && authentication.getAuthorities()!=null) {
			//String currentPrincipalName = authentication.getName();
			for (GrantedAuthority r : authentication.getAuthorities()) {
				//System.out.println("Ruolo:"+r.getAuthority());
				for (int i = 0; i < listRoles.length; i++) {
					if (r.getAuthority()!=null && r.getAuthority().equals(listRoles[i])) {
						//System.out.println("trovato ruolo !!!");
						return true;
					}					
				}
			}
		}
		return false;
	}	
	
}