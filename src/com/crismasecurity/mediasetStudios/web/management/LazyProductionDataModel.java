package com.crismasecurity.mediasetStudios.web.management;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.crismasecurity.mediasetStudios.core.entity.Agency;
import com.crismasecurity.mediasetStudios.core.entity.Production;
import com.crismasecurity.mediasetStudios.core.services.ProductionService;
 

public class LazyProductionDataModel extends LazyDataModel<Production> {

	private static final long serialVersionUID = -6133649383211413504L;

	private ProductionService productionService;
	
	private Date filterFrom;
	private Date filterTo;
	private Agency filterOwner;
	private Agency filterAgency;
	
	

     
    public LazyProductionDataModel(ProductionService productionService) {
        this.productionService = productionService;
    }
     
    @Override
    public Production getRowData(String rowKey) {
    	if (rowKey==null || rowKey.trim().equals("")) return null;
    	return productionService.getProductionById(Long.parseLong(rowKey));
    }
 
    @Override
    public Object getRowKey(Production audit) {
        return audit.getId();
    }
 
    @Override
    public List<Production> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
    	
    	// Converto ordinamento
    	int sortInt =0; // UNSORTED
    	if (sortOrder.equals(SortOrder.ASCENDING)) sortInt = 1;
    	if (sortOrder.equals(SortOrder.DESCENDING)) sortInt = 2;
    	
 
		if (filterFrom!=null) filters.put("date#ge", filterFrom);
		if (filterTo!=null) filters.put("date#le", filterTo);
		if (filterOwner!=null) filters.put("owner", filterOwner);
		if (getFilterAgency()!=null) filters.put("agency", getFilterAgency());
		

    	
    	List<Production> ret = productionService.getProductionLazyLoading(first, pageSize, sortField, sortInt, filters);
    	this.setRowCount((int)productionService.getProductionLazyLoadingCount(filters));
//    	System.out.println("Numero elementi trovati: " + this.getRowCount());
//    	for (Audit audit : ret) {
//			System.out.println("Trovato: " + audit);
//		}
    	return ret;    
    }
   

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}

	public Agency getFilterOwner() {
		return filterOwner;
	}

	public void setFilterOwner(Agency filterOwner) {
		this.filterOwner = filterOwner;
	}

	public Agency getFilterAgency() {
		return filterAgency;
	}

	public void setFilterAgency(Agency filterAgency) {
		this.filterAgency = filterAgency;
	}
	


}