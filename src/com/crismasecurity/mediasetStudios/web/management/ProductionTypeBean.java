package com.crismasecurity.mediasetStudios.web.management;

import java.util.Calendar;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.event.ActionEvent;
import javax.persistence.PersistenceException;

import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;


import com.crismasecurity.mediasetStudios.core.entity.ProductionType;
import com.crismasecurity.mediasetStudios.core.services.ProductionTypeService;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;

//@ManagedBean(name= EmployeesBean.BEAN_NAME) // JSF managed
//@SessionScoped // JSF managed
//@ViewScoped // JSF managed

//Spring managed
@Controller (ProductionTypeBean.BEAN_NAME) 
//@Scope("session")
@Scope("view")
public class ProductionTypeBean extends BaseBean {

	private static final long serialVersionUID = -3272094804037319786L;

	public static final String BEAN_NAME = "productionTypeBean";
    public String getBeanName() { return BEAN_NAME; } 


    @Autowired
    private ProductionTypeService productionTypeService ;
    
    private LazyDataModel<ProductionType> lazyModelProductionType;
    private Date filterFrom;
	private Date filterTo;
	
	private ProductionType productionTypeToEdit = null;
    
	public ProductionTypeBean() {
		getLog().info("!!!! Costruttore di ProductionTypeBean !!!");	
    }

	@PostConstruct
	public void init() {
		getLog().info("INIT ProductionTypeBean!");

		lazyModelProductionType = new LazyProductionTypeDataModel(productionTypeService);	
	}

	@PreDestroy
	public void destroy() {
		getLog().info("Destroy ProductionTypeBean!");

	}
	
	private Long productionTypeToDelete = null;
	public void confirmDeleteProductionType(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("productionTypeId")) {
    			productionTypeToDelete = ((Long)((UIParameter)com).getValue());
			}
		}
    	
	}
	
	
	public boolean haveUserRole(String roles) {
		
		String[] listRoles = roles.split(",");
		for (int i = 0; i < listRoles.length; i++) {
			listRoles[i] = listRoles[i].trim();
			
		}
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication!=null && authentication.getAuthorities()!=null) {
			//String currentPrincipalName = authentication.getName();
			for (GrantedAuthority r : authentication.getAuthorities()) {
				//System.out.println("Ruolo:"+r.getAuthority());
				for (int i = 0; i < listRoles.length; i++) {
					if (r.getAuthority()!=null && r.getAuthority().equals(listRoles[i])) {
						//System.out.println("trovato ruolo !!!");
						return true;
					}					
				}
			}
		}
		return false;
	}
	
	public void cancelDelete(ActionEvent ev) {
	    	System.out.println("\nDelete ProductionType canceled.");
	    	productionTypeToDelete= null;
	}
	
	public void delete(ActionEvent ev) {
    	System.out.println("\nDelete Test ...");
    	try {
        	
    		productionTypeService.deleteProductionType(productionTypeToDelete);
//    		employees=null;
    		addInfoMessage(getMessagesBoundle().getString("productionType.deleted"));
		} catch (Exception e) {
			e.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("productionType.deletingError"), e);
		}
	}
	
	public void addProductionType(ActionEvent e) {
		productionTypeToEdit = new ProductionType();
	}
	
	public void modifyProductionType(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("productionTypeId")) {
    			productionTypeToEdit = productionTypeService.getProductionTypeById((Long)((UIParameter)com).getValue());
			}
		}
	}
	
	public void cancelEditProductionType(ActionEvent e) {
		productionTypeToEdit = null;
	}
	
	public void saveProductionType(ActionEvent e) {
    	System.out.println("Salvo " + productionTypeToEdit);
    	

    	if (productionTypeToEdit.getDescription()==null || productionTypeToEdit.getDescription().trim().equals("")) {
    		addWarningMessage("test su Descrizione richiesto!");
    		return;
    	}
    	
    	
    	try {
    		productionTypeToEdit = productionTypeService.updateProductionType(productionTypeToEdit);
		
			addInfoMessage(getMessagesBoundle().getString("productionType.saved"));
    	} catch (PersistenceException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();

    	} catch (DataIntegrityViolationException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();
    				
		} catch (Exception e1) {
			addErrorMessage(getMessagesBoundle().getString("productionType.savingError"), e1);
			e1.printStackTrace();
		}
    	productionTypeToEdit = null;
	}
	
	public void filterByDate() {
		System.out.println("Filter by date !");
		
		Calendar cal = Calendar.getInstance();
		if (filterFrom!=null) {
			cal.setTime(filterFrom);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 1);
			filterFrom = cal.getTime();
		}
		if (filterTo!=null) {
			cal.setTime(filterTo);
			cal.set(Calendar.HOUR_OF_DAY, 23);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);
			filterTo = cal.getTime();
		}
		
		((LazyProductionTypeDataModel)lazyModelProductionType).setFilterFrom(filterFrom);
		((LazyProductionTypeDataModel)lazyModelProductionType).setFilterTo(filterTo);
	}
	
	public Object getProductionTypeById(long id) {
		return productionTypeService.getProductionTypeById(id);
	}

	public ProductionType getProductionTypeToEdit() {
		return productionTypeToEdit;
	}

	public void setProductionTypeToEdit(ProductionType empToEdit) {
		this.productionTypeToEdit = empToEdit;
	}

	public LazyDataModel<ProductionType> getLazyModelProductionType() {
		return lazyModelProductionType;
	}

	public Date getFilterFrom() {
		return filterFrom;
	}

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public Date getFilterTo() {
		return filterTo;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}

}
