package com.crismasecurity.mediasetStudios.web.management;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.event.ActionEvent;
import javax.persistence.PersistenceException;

import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import com.crismasecurity.mediasetStudios.core.entity.IncidentType;
import com.crismasecurity.mediasetStudios.core.services.IncidentTypeService;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;

//Spring managed
@Controller (IncidentTypeBean.BEAN_NAME) 
//@Scope("session")
@Scope("view")
public class IncidentTypeBean extends BaseBean{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5662158989060606869L;
	
	public static final String BEAN_NAME = "incidentTypeBean";
	public String getBeanName() { return BEAN_NAME; } 

	@Autowired
	private IncidentTypeService incidentTypeService;

	private LazyDataModel<IncidentType> lazyModelIncidentType;
	private IncidentType incidentTypeToEdit = null;
	private Long incidentTypeToDelete = null;

	
	
	public IncidentTypeBean() {
		getLog().info("!!!! Costruttore di IncidentTypeBean !!!");
	}
	
	@PostConstruct
	public void init() {
		getLog().info("INIT IncidentTypeBean!");
		lazyModelIncidentType = new LazyIncidentTypeDataModel(incidentTypeService);
	}
	
	@PreDestroy
	public void destroy() {
		getLog().info("Destroy ApparatusTypeBean!");
	}
	
	public void confirmDeleteIncidentType(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("incidentTypeId")) {
    			incidentTypeToDelete = ((Long)((UIParameter)com).getValue());
			}
		}    	
	}
	
	public void cancelDelete(ActionEvent ev) {
    	System.out.println("\nDelete IncidentType canceled.");
    	incidentTypeToDelete= null;
	}
	
	public void delete(ActionEvent ev) {
    	System.out.println("\nDelete Test ...");
    	try {
    		incidentTypeService.deleteIncidentType(incidentTypeToDelete);
    		addInfoMessage(getMessagesBoundle().getString("incidentType.deleted"));
		} catch (Exception e) {
			e.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("incidentType.deletingError"), e);
		}
    }
	
	public void addIncidentType(ActionEvent e) {
		incidentTypeToEdit = new IncidentType();
	}
	
	public void modifyIncidentType(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("incidentTypeId")) {
    			incidentTypeToEdit = incidentTypeService.getIncidentTypeById((Long)((UIParameter)com).getValue());
    		}
		}
	}
	
	public void cancelEditIncidentType(ActionEvent e) {
		incidentTypeToEdit = null;
	}
	
	public void saveIncidentType(ActionEvent e) {
    	System.out.println("Salvo " + incidentTypeToEdit);
    	

    	if (incidentTypeToEdit.getDescription()==null || incidentTypeToEdit.getDescription().trim().equals("")) {
    		addWarningMessage("test su Descrizione richiesto!");
    		return;
    	}
    	try {
    		incidentTypeToEdit = incidentTypeService.updateIncidentType(incidentTypeToEdit);
    		addInfoMessage(getMessagesBoundle().getString("incidentType.saved"));
    	} catch (PersistenceException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();

    	} catch (DataIntegrityViolationException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();    		
		} catch (Exception e1) {
			addErrorMessage(getMessagesBoundle().getString("incidentType.savingError"), e1);
			e1.printStackTrace();
		}
    	incidentTypeToEdit = null;
	}
	
	public Object getIncidentTypeById(long id) {
		return incidentTypeService.getIncidentTypeById(id);
	}

	public IncidentType getIncidentTypeToEdit() {
		return incidentTypeToEdit;
	}

	public void setIncidentTypeToEdit(IncidentType inciToEdit) {
		this.incidentTypeToEdit = inciToEdit;
	}

	public LazyDataModel<IncidentType> getLazyModelIncidentType() {
		return lazyModelIncidentType;
	}
	
	public boolean haveUserRole(String roles) {
		
		String[] listRoles = roles.split(",");
		for (int i = 0; i < listRoles.length; i++) {
			listRoles[i] = listRoles[i].trim();
			
		}
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication!=null && authentication.getAuthorities()!=null) {
			//String currentPrincipalName = authentication.getName();
			for (GrantedAuthority r : authentication.getAuthorities()) {
				//System.out.println("Ruolo:"+r.getAuthority());
				for (int i = 0; i < listRoles.length; i++) {
					if (r.getAuthority()!=null && r.getAuthority().equals(listRoles[i])) {
						//System.out.println("trovato ruolo !!!");
						return true;
					}					
				}
			}
		}
		return false;
	}	
	
}
