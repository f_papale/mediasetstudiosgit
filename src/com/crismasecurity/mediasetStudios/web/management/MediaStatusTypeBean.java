package com.crismasecurity.mediasetStudios.web.management;

import java.util.Calendar;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.event.ActionEvent;
import javax.persistence.PersistenceException;

import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;


import com.crismasecurity.mediasetStudios.core.entity.MediaStatusType;
import com.crismasecurity.mediasetStudios.core.services.MediaStatusTypeService;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;

//@ManagedBean(name= EmployeesBean.BEAN_NAME) // JSF managed
//@SessionScoped // JSF managed
//@ViewScoped // JSF managed

//Spring managed
@Controller (MediaStatusTypeBean.BEAN_NAME) 
//@Scope("session")
@Scope("view")
public class MediaStatusTypeBean extends BaseBean {

	private static final long serialVersionUID = -6065141989753335782L;

	public static final String BEAN_NAME = "mediaStatusTypeBean";
    public String getBeanName() { return BEAN_NAME; } 


    @Autowired
    private MediaStatusTypeService mediaStatusTypeService ;
    
    private LazyDataModel<MediaStatusType> lazyModelMediaStatusType;
    private Date filterFrom;
	private Date filterTo;
	
	private MediaStatusType mediaStatusTypeToEdit = null;
    
	public MediaStatusTypeBean() {
		getLog().info("!!!! Costruttore di MediaStatusTypeBean !!!");	
    }

	@PostConstruct
	public void init() {
		getLog().info("INIT MediaStatusTypeBean!");

		lazyModelMediaStatusType = new LazyMediaStatusTypeDataModel(mediaStatusTypeService);	
	}

	@PreDestroy
	public void destroy() {
		getLog().info("Destroy MediaStatusTypeBean!");

	}
	
	private Long mediaStatusTypeToDelete = null;
	public void confirmDeleteMediaStatusType(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("mediaStatusTypeId")) {
    			mediaStatusTypeToDelete = ((Long)((UIParameter)com).getValue());
			}
		}
    	
	}
	
	public void cancelDelete(ActionEvent ev) {
	    	System.out.println("\nDelete MediaStatusType canceled.");
	    	mediaStatusTypeToDelete= null;
	}
	
	public void delete(ActionEvent ev) {
    	System.out.println("\nDelete Test ...");
    	try {
        	
    		mediaStatusTypeService.deleteMediaStatusType(mediaStatusTypeToDelete);
//    		employees=null;
    		addInfoMessage(getMessagesBoundle().getString("mediaStatusType.deleted"));
		} catch (Exception e) {
			e.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("mediaStatusType.deletingError"), e);
		}
	}
	
	public void addMediaStatusType(ActionEvent e) {
		mediaStatusTypeToEdit = new MediaStatusType();
	}
	
	public void modifyMediaStatusType(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("mediaStatusTypeId")) {
    			mediaStatusTypeToEdit = mediaStatusTypeService.getMediaStatusTypeById((Long)((UIParameter)com).getValue());
			}
		}
	}
	
	public void cancelEditMediaStatusType(ActionEvent e) {
		mediaStatusTypeToEdit = null;
	}
	
	public void saveMediaStatusType(ActionEvent e) {
    	System.out.println("Salvo " + mediaStatusTypeToEdit);
    	

    	if (mediaStatusTypeToEdit.getDescription()==null || mediaStatusTypeToEdit.getDescription().trim().equals("")) {
    		addWarningMessage("test su Descrizione richiesto!");
    		return;
    	}
    	
    	
    	try {
    		mediaStatusTypeToEdit = mediaStatusTypeService.updateMediaStatusType(mediaStatusTypeToEdit);
		
			addInfoMessage(getMessagesBoundle().getString("mediaStatusType.saved"));
    	} catch (PersistenceException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();

    	} catch (DataIntegrityViolationException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();
    				
		} catch (Exception e1) {
			addErrorMessage(getMessagesBoundle().getString("mediaStatusType.savingError"), e1);
			e1.printStackTrace();
		}
    	
    	mediaStatusTypeToEdit = null;
	}
	
	
	public boolean haveUserRole(String roles) {
		
		String[] listRoles = roles.split(",");
		for (int i = 0; i < listRoles.length; i++) {
			listRoles[i] = listRoles[i].trim();
			
		}
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication!=null && authentication.getAuthorities()!=null) {
			//String currentPrincipalName = authentication.getName();
			for (GrantedAuthority r : authentication.getAuthorities()) {
				//System.out.println("Ruolo:"+r.getAuthority());
				for (int i = 0; i < listRoles.length; i++) {
					if (r.getAuthority()!=null && r.getAuthority().equals(listRoles[i])) {
						//System.out.println("trovato ruolo !!!");
						return true;
					}					
				}
			}
		}
		return false;
	}	
	
	public void filterByDate() {
		System.out.println("Filter by date !");
		
		Calendar cal = Calendar.getInstance();
		if (filterFrom!=null) {
			cal.setTime(filterFrom);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 1);
			filterFrom = cal.getTime();
		}
		if (filterTo!=null) {
			cal.setTime(filterTo);
			cal.set(Calendar.HOUR_OF_DAY, 23);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);
			filterTo = cal.getTime();
		}
		
		((LazyMediaStatusTypeDataModel)lazyModelMediaStatusType).setFilterFrom(filterFrom);
		((LazyMediaStatusTypeDataModel)lazyModelMediaStatusType).setFilterTo(filterTo);
	}
	
	public Object getMediaStatusTypeById(long id) {
		return mediaStatusTypeService.getMediaStatusTypeById(id);
	}

	public MediaStatusType getMediaStatusTypeToEdit() {
		return mediaStatusTypeToEdit;
	}

	public void setMediaStatusTypeToEdit(MediaStatusType empToEdit) {
		this.mediaStatusTypeToEdit = empToEdit;
	}

	public LazyDataModel<MediaStatusType> getLazyModelMediaStatusType() {
		return lazyModelMediaStatusType;
	}

	public Date getFilterFrom() {
		return filterFrom;
	}

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public Date getFilterTo() {
		return filterTo;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}

}
