package com.crismasecurity.mediasetStudios.web.management;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.crismasecurity.mediasetStudios.core.entity.Studio;
import com.crismasecurity.mediasetStudios.core.services.StudioService;
 

public class LazyStudioDataModel extends LazyDataModel<Studio> {

	private static final long serialVersionUID = 96682863926277590L;

	private StudioService studioService;
	
	private Date filterFrom;
	private Date filterTo;

     
    public LazyStudioDataModel(StudioService studioService) {
        this.studioService = studioService;
    }
     
    @Override
    public Studio getRowData(String rowKey) {
    	if (rowKey==null || rowKey.trim().equals("")) return null;
    	return studioService.getStudioById(Long.parseLong(rowKey));
    }
 
    @Override
    public Object getRowKey(Studio audit) {
        return audit.getId();
    }
 
    @Override
    public List<Studio> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
    	
    	// Converto ordinamento
    	int sortInt =0; // UNSORTED
    	if (sortOrder.equals(SortOrder.ASCENDING)) sortInt = 1;
    	if (sortOrder.equals(SortOrder.DESCENDING)) sortInt = 2;
    	
 
		if (filterFrom!=null) filters.put("date#ge", filterFrom);
		if (filterTo!=null) filters.put("date#le", filterTo);

    	
    	List<Studio> ret = studioService.getStudioLazyLoading(first, pageSize, sortField, sortInt, filters);
    	this.setRowCount((int)studioService.getStudioLazyLoadingCount(filters));
//    	System.out.println("Numero elementi trovati: " + this.getRowCount());

    	return ret;    
    }

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}


}