package com.crismasecurity.mediasetStudios.web.management;

import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.crismasecurity.mediasetStudios.core.entity.SpectatorIncident;
import com.crismasecurity.mediasetStudios.core.services.SpectatorIncidentService;

public class LazySpectatorIncidentDataModel extends LazyDataModel<SpectatorIncident>{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5742771968913831338L;

	private SpectatorIncidentService SpectatorIncidentService;
	
	private Long incidentId;
	
	private Long spectatorId;
	
	
	public LazySpectatorIncidentDataModel(SpectatorIncidentService sptypeservice) {
		this.SpectatorIncidentService = sptypeservice;
	}
	
	@Override
	public SpectatorIncident getRowData(String rowKey) {
		if (rowKey==null || rowKey.trim().equals("")) return null;
		return SpectatorIncidentService.getSpectatorIncidentById(Long.parseLong(rowKey));
	}
	
    @Override
    public Object getRowKey(SpectatorIncident sptype) {
    	return sptype.getId();
    }
    
    @Override 
    public List<SpectatorIncident> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters){
    	// Converto ordinamento
    	int sortInt = 0; // UNSORTED
    	if (sortOrder.equals(SortOrder.ASCENDING)) sortInt = 1;
    	if (sortOrder.equals(SortOrder.DESCENDING)) sortInt = 2;
    	
    	if (incidentId != null) filters.put("incident.idIncident", incidentId);
    	
    	if(spectatorId != null)  filters.put("spectator.id", spectatorId);
    	
    	List<SpectatorIncident> sptypelist = SpectatorIncidentService.getSpectatorIncidentLazyLoading(first, pageSize, sortField, sortInt, filters);
    	this.setRowCount((int)SpectatorIncidentService.getSpectatorIncidentLazyLoadingCount(filters));
//    	System.out.println("Numero elementi trovati: " + this.getRowCount());    	
    	return sptypelist;
    	
    }

	public Long getincidentId() {
		return incidentId;
	}

	public void setincidentId(Long incidentId) {
		this.incidentId = incidentId;
	}

	public Long getSpectatorId() {
		return spectatorId;
	}

	public void setSpectatorId(Long spectatorId) {
		this.spectatorId = spectatorId;
	}

}
