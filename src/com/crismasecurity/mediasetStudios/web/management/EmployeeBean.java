package com.crismasecurity.mediasetStudios.web.management;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;


import com.crismasecurity.mediasetStudios.core.entity.Employee;
import com.crismasecurity.mediasetStudios.core.entity.EmployeeType;
import com.crismasecurity.mediasetStudios.core.services.EmployeeService;
import com.crismasecurity.mediasetStudios.core.services.EmployeeTypeService;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;

//@ManagedBean(name= EmployeesBean.BEAN_NAME) // JSF managed
//@SessionScoped // JSF managed
//@ViewScoped // JSF managed

//Spring managed
@Controller (EmployeeBean.BEAN_NAME) 
//@Scope("session")
@Scope("view")
public class EmployeeBean extends BaseBean {

	private static final long serialVersionUID = 7386923144668250371L;

	public static final String BEAN_NAME = "employeeBean";
    public String getBeanName() { return BEAN_NAME; } 
   


    @Autowired
    private EmployeeService employeeService ;
    
    @Autowired
    private EmployeeTypeService employeeTypeService;
    
    private LazyDataModel<Employee> lazyModelEmployee;
    private Date filterFrom;
	private Date filterTo;
	
	
	private List<SelectItem> employeeTypes;
	private Long selectedEmployeeType = null;
	//private boolean selectedInternalExternal = false;
	
	private Employee employeeToEdit = null;
    
	public EmployeeBean() {
		getLog().info("!!!! Costruttore di EmployeeBean !!!");	
    }

	@PostConstruct
	public void init() {
		getLog().info("INIT EmployeeBean!");

		presetEmployeeList();
		lazyModelEmployee = new LazyEmployeeDataModel(employeeService);			
	}

	@PreDestroy
	public void destroy() {
		getLog().info("Destroy EmployeeBean!");
	}
	
	private void presetEmployeeList() {
		employeeTypes = new ArrayList<SelectItem>();
		List<EmployeeType> conts = employeeTypeService.getEmployeeType();
		employeeTypes.add(new SelectItem(null, " "));
		for (EmployeeType cont : conts) {
			SelectItem xx = new SelectItem(cont.getId(), cont.getDescription());
			employeeTypes.add(xx);
		}
	}
	
	private Long employeeToDelete = null;
	
	public void confirmDeleteEmployee(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("employeeId")) {
    			employeeToDelete = ((Long)((UIParameter)com).getValue());
			}
		}
	}
	
	public void cancelDelete(ActionEvent ev) {
	    	System.out.println("\nDelete Employee canceled.");
	    	employeeToDelete= null;
	}
	
	public void delete(ActionEvent ev) {
    	System.out.println("\nDelete Test ...");
    	try {
    		employeeService.deleteEmployee(employeeToDelete);
    		addInfoMessage(getMessagesBoundle().getString("employee.deleted"));
		} catch (Exception e) {
			e.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("employee.deletingError"), e);
		}
	}
	
	public void addEmployee(ActionEvent e) {
		employeeToEdit = new Employee();
		selectedEmployeeType=null;
	}
	
	public void modifyEmployee(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("employeeId")) {
    			employeeToEdit = employeeService.getEmployeeById((Long)((UIParameter)com).getValue());
    			selectedEmployeeType=employeeToEdit.getEmployeeType().getId();
			}
		}
	}
	
	public void cancelEditEmployee(ActionEvent e) {
		employeeToEdit = null;
	}
	
	public void saveEmployee(ActionEvent e) {
    	System.out.println("Salvo " + employeeToEdit);
    	

    	if (employeeToEdit.getName()==null || employeeToEdit.getName().trim().equals("")) {
    		addWarningMessage("test su Descrizione richiesto!");
    		return;
    	}

    	if (selectedEmployeeType==null) {
    		addWarningMessage("test su Tipo Addetto richiesto!");
    		return;
    	} 	
    	    	
    	
    	try {
    		//Imposta il valore della dropdown list nella classe
    		employeeToEdit.setEmployeeType(employeeTypeService.getEmployeeTypeById(selectedEmployeeType));
    		
    		employeeToEdit = employeeService.updateEmployee(employeeToEdit);
		
			addInfoMessage(getMessagesBoundle().getString("employee.saved"));
		} catch (Exception e1) {
			addErrorMessage(getMessagesBoundle().getString("employee.savingError"), e1);
			e1.printStackTrace();
		}
    	employeeToEdit = null;
	}
	
	public void filterByDate() {
		System.out.println("Filter by date !");
		
		Calendar cal = Calendar.getInstance();
		if (filterFrom!=null) {
			cal.setTime(filterFrom);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 1);
			filterFrom = cal.getTime();
		}
		if (filterTo!=null) {
			cal.setTime(filterTo);
			cal.set(Calendar.HOUR_OF_DAY, 23);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);
			filterTo = cal.getTime();
		}
		
		((LazyEmployeeDataModel)lazyModelEmployee).setFilterFrom(filterFrom);
		((LazyEmployeeDataModel)lazyModelEmployee).setFilterTo(filterTo);
	}
	
	public Object getEmployeeById(long id) {
		return employeeService.getEmployeeById(id);
	}

	public Employee getEmployeeToEdit() {
		return employeeToEdit;
	}

	public void setEmployeeToEdit(Employee empToEdit) {
		this.employeeToEdit = empToEdit;
	}

	public LazyDataModel<Employee> getLazyModelEmployee() {
		return lazyModelEmployee;
	}

	public Date getFilterFrom() {
		return filterFrom;
	}

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public Date getFilterTo() {
		return filterTo;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}

	public List<SelectItem> getEmployeeTypes() {
		return this.employeeTypes;
	}

	public void setEmployeeTypes(List<SelectItem> employeeTypes) {
		this.employeeTypes = employeeTypes;
	}

	public Long getSelectedEmployeeType() {
		return selectedEmployeeType;
	}

	public void setSelectedEmployeeType(Long selectedEmployeeType) {
		this.selectedEmployeeType = selectedEmployeeType;
	}

	public void chk() {
	 	System.out.println("Making check... " + employeeToEdit.isInternalExternal()); 
	}
	
	public boolean enab() {
		return employeeToEdit.isInternalExternal();
	}
	
	public boolean haveUserRole(String roles) {
		
		String[] listRoles = roles.split(",");
		for (int i = 0; i < listRoles.length; i++) {
			listRoles[i] = listRoles[i].trim();
			
		}
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication!=null && authentication.getAuthorities()!=null) {
			//String currentPrincipalName = authentication.getName();
			for (GrantedAuthority r : authentication.getAuthorities()) {
				//System.out.println("Ruolo:"+r.getAuthority());
				for (int i = 0; i < listRoles.length; i++) {
					if (r.getAuthority()!=null && r.getAuthority().equals(listRoles[i])) {
						//System.out.println("trovato ruolo !!!");
						return true;
					}					
				}
			}
		}
		return false;
	}	
}
