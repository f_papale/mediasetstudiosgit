package com.crismasecurity.mediasetStudios.web.management;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.crismasecurity.mediasetStudios.core.entity.DistributionList;
import com.crismasecurity.mediasetStudios.core.services.DistributionListService;
 

public class LazyDistributionListDataModel extends LazyDataModel<DistributionList> {


	/**
	 * 
	 */
	private static final long serialVersionUID = 5459008154701013428L;

	private DistributionListService DistributionListService;
	
	private Date filterFrom;
	private Date filterTo;

     
    public LazyDistributionListDataModel(DistributionListService DistributionListTypeService) {
        this.DistributionListService = DistributionListTypeService;
    }
     
    @Override
    public DistributionList getRowData(String rowKey) {
    	if (rowKey==null || rowKey.trim().equals("")) return null;
    	return DistributionListService.getDistributionListById(Long.parseLong(rowKey));
    }
 
    @Override
    public Object getRowKey(DistributionList audit) {
        return audit.getId();
    }
 
    @Override
    public List<DistributionList> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
    	
    	// Converto ordinamento
    	int sortInt =0; // UNSORTED
    	if (sortOrder.equals(SortOrder.ASCENDING)) sortInt = 1;
    	if (sortOrder.equals(SortOrder.DESCENDING)) sortInt = 2;
    	
 
		if (filterFrom!=null) filters.put("date#ge", filterFrom);
		if (filterTo!=null) filters.put("date#le", filterTo);

    	
    	List<DistributionList> ret = DistributionListService.getDistributionListLazyLoading(first, pageSize, sortField, sortInt, filters);
    	this.setRowCount((int)DistributionListService.getDistributionListLazyLoadingCount(filters));
//    	System.out.println("Numero elementi trovati: " + this.getRowCount());
//    	for (Audit audit : ret) {
//			System.out.println("Trovato: " + audit);
//		}
    	return ret;    
    }

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}


}