package com.crismasecurity.mediasetStudios.web.management;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.primefaces.util.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.util.Base64Utils;

import com.crismasecurity.mediasetStudios.core.entity.Agency;
import com.crismasecurity.mediasetStudios.core.entity.AgencySpectator;
import com.crismasecurity.mediasetStudios.core.entity.AgencyUser;
import com.crismasecurity.mediasetStudios.core.entity.Audit;
import com.crismasecurity.mediasetStudios.core.entity.BlackListSpectator;
import com.crismasecurity.mediasetStudios.core.entity.BlackListType;
import com.crismasecurity.mediasetStudios.core.entity.CountryCode;
import com.crismasecurity.mediasetStudios.core.entity.DocumentSpectator;
import com.crismasecurity.mediasetStudios.core.entity.DocumentType;
import com.crismasecurity.mediasetStudios.core.entity.Spectator;
import com.crismasecurity.mediasetStudios.core.entity.SpectatorEpisode;
import com.crismasecurity.mediasetStudios.core.entity.SpectatorIncident;
import com.crismasecurity.mediasetStudios.core.entity.SpectatorType;
import com.crismasecurity.mediasetStudios.core.security.services.AppUserService;
import com.crismasecurity.mediasetStudios.core.security.vos.security.AppUserVO;
import com.crismasecurity.mediasetStudios.core.services.AgencyService;
import com.crismasecurity.mediasetStudios.core.services.AgencySpectatorService;
import com.crismasecurity.mediasetStudios.core.services.AgencyUserService;
import com.crismasecurity.mediasetStudios.core.services.AuditService;
import com.crismasecurity.mediasetStudios.core.services.BlackListSpectatorService;
import com.crismasecurity.mediasetStudios.core.services.BlackListTypeService;
import com.crismasecurity.mediasetStudios.core.services.CountryCodeService;
import com.crismasecurity.mediasetStudios.core.services.DocumentSpectatorService;
import com.crismasecurity.mediasetStudios.core.services.DocumentTypeService;
import com.crismasecurity.mediasetStudios.core.services.EmailMakerService;
import com.crismasecurity.mediasetStudios.core.services.SpectatorEpisodeService;
import com.crismasecurity.mediasetStudios.core.services.SpectatorIncidentService;
import com.crismasecurity.mediasetStudios.core.services.SpectatorService;
import com.crismasecurity.mediasetStudios.core.services.SpectatorTypeService;
import com.crismasecurity.mediasetStudios.core.utils.Gender;
import com.crismasecurity.mediasetStudios.core.utils.PropertiesUtils;
import com.crismasecurity.mediasetStudios.core.utils.TextUtil;
import com.crismasecurity.mediasetStudios.web.LoginBean;
import com.crismasecurity.mediasetStudios.web.NavigationBean;
import com.crismasecurity.mediasetStudios.web.management.utils.ManageFiscalCode;
import com.crismasecurity.mediasetStudios.web.utils.MakeHTML;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;
import com.crismasecurity.mediasetStudios.core.utils.ObjectDiff.ObjectDiff;

//import java.util.Base64;

import com.widee.base.hibernate.criteria.Finder;

import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

//Spring managed
@Controller(SpectatorBean.BEAN_NAME)
@Scope("view")
public class SpectatorBean extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5225477109754594591L;

	public static final String BEAN_NAME = "spectatorBean";

	public String getBeanName() {
		return BEAN_NAME;
	}

	@Autowired
	private SpectatorService spectatorService;

	@Autowired
	private SpectatorEpisodeService spectatorEpisodeService;
	
	@Autowired
	private BlackListTypeService blackListTypeService;
	
//	@Autowired
//	private BlackListSpectatorService blackListSpectatorService;

	@Autowired
	private SpectatorIncidentService spectatorIncidentService;	
	
	@Autowired
	private SpectatorTypeService spectatorTypeService;

	@Autowired
	private CountryCodeService countryCodeService;

	@Autowired
	private AgencySpectatorService agencySpectatorService;

	@Autowired
	private AgencyUserService agencyUserService;

	@Autowired
	private AppUserService userService;

	@Autowired
	private AgencyService agencyService;

	@Autowired
	private DocumentSpectatorService documentSpectatorService;

	@Autowired
	private DocumentTypeService documentTypeService;

	@Autowired
	private BlackListSpectatorService blackListSpectatorService;	
	
	@Autowired
	private LoginBean loginBean;
	
	@Autowired
	private AuditService auditService;
	
    @Autowired
    private NavigationBean navigationBean;

	@Autowired
	private EmailMakerService emailMakerService;    
    
	//------------------------------------------------------------------------------------------------------	

	private Spectator spectatorToInvite = null;
	

	private LazyDataModel<Spectator> lazyModelSpectator;

	private LazyDataModel<AgencySpectator> lazyModelAgencySpectator;

	private LazyDataModel<DocumentSpectator> lazyModelDocumentSpectator;

	private LazyDataModel<SpectatorEpisode> lazyModelSpectatorEpisode;
	
	private LazyDataModel<SpectatorIncident> lazyModelSpectatorIncident;

	private List<Spectator> spectatorsNotAddedList = null;

	private List<SelectItem> spectatorTypes;
	private Long selectedSpectatorType = null;

	private Spectator spectatorToEdit = null;

	private Long spectatorToDelete = null;

	private String checkCfInserted;

	private AgencyUser currentAgencyUser = null;

	private List<AgencySpectator> selectedAgencies = null;

	private List<SelectItem> agencies = null;

	private Long selectedAgency = null;

	private Long selectedDocumentType = null;

	private Long selectedDocumentTypePhoto = null;

	private List<SelectItem> documentTypes = null;

	private List<SelectItem> documentTypePhoto = null;

	private boolean documentUploadEnabled = false;

	private Long documentSpectatorToDelete = null;

	private String spectatorPhotoAsBase64 = null;

	private String messageBlackList = "";
	
	private String loggedUser;

	private ObjectDiff oDiff; 

	private boolean  multiAgenzia ;
	private String unwantedMessage;

//-----------------------------------------------------------------------------------------------------	
	public SpectatorBean() {
		getLog().info("!!!! Costruttore di SpectatorBean !!!");
	}

	private String fisCodWarningStyle = null;
	
	private long uploadFileSize=0;

	@PostConstruct
	public void init() {
		getLog().info("INIT SpectatorBean!");
		oDiff = new ObjectDiff();
		encodeSpectator();
		getLog().info("getSpectator!\n");
		presetCurrentUser();
		presetAgencyList();
		presetSpectatorTypeList();
		setUnwantedMessage(PropertiesUtils.getInstance().readProperty("unwanted.message", getMessagesBoundle().getString("spectator_in_blocked_blacklinst")));
		spectatorsNotAddedList = new ArrayList<Spectator>();

		if (haveUserRole("ROLE_AGENCY")) {
			setLazyModelAgencySpectator(new LazyAgencySpectatorDataModel(agencySpectatorService));
			((LazyAgencySpectatorDataModel) getLazyModelAgencySpectator())
					.setAgencyId(currentAgencyUser.getAgency().getIdAgency());
		} else if (haveUserRole("ROLE_ADMIN")|| haveUserRole("ROLE_SECURITY") || haveUserRole("ROLE_SUPERVISION") ) {
			setLazyModelAgencySpectator(new LazyAgencySpectatorDataModel(agencySpectatorService));
		} else {
			setLazyModelAgencySpectator(null);
		}
	}

	@PreDestroy
	public void destroy() {
		getLog().info("Destroy SpectatorBean!");

	}

	private void presetAgencyList() {
		multiAgenzia=false;
		agencies = new ArrayList<SelectItem>();
		List<Agency> aglist = null;
		if (haveUserRole("ROLE_ADMIN") || haveUserRole("ROLE_SECURITY") || haveUserRole("ROLE_SUPERVISION") ) {
			aglist = agencyService.getAgency();
		} else if (haveUserRole("ROLE_AGENCY")) {
			aglist = agencyService.getAgencyFinder().and("idAgency").eq(getIdLoggedAgency()).setDistinct().list();
		}
		agencies.add(new SelectItem(null, " "));
		if (aglist !=null) {
			for (Agency ag : aglist) {
				SelectItem xx = new SelectItem(ag.getIdAgency(), ag.getName());
				agencies.add(xx);
			}
			multiAgenzia=true;
		}
		// se l'utente loggato è un agenzia, impostiamo l agenzia di default al
		// caricamento della maschera
		if (haveUserRole("ROLE_AGENCY")) {
			selectedAgency = getIdLoggedAgency();
		}
	}

	private void presetCurrentUser() {

		try {
			long userId = userService.getUserByLogin(loginBean.getUserId()).getId();
			setCurrentAgencyUser(agencyUserService.getAgencyUserFinder().and("user.id").eq(userId).and("flgCurrentSelected").eq(true).result());
			loggedUser = userService.getUserByLogin(loginBean.getUserId()).getFirstName() + " " + userService.getUserByLogin(loginBean.getUserId()).getLastName();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Errore Preset Current User");
			addErrorMessage(getMessagesBoundle().getString("spectatorEpisode.presetCurrentUser"), e);
		}
	}

	private void presetSpectatorTypeList() {
		spectatorTypes = new ArrayList<SelectItem>();
		List<SpectatorType> sptypelist = spectatorTypeService.getSpectatorType();
		spectatorTypes.add(new SelectItem(null, " "));
		for (SpectatorType sptype : sptypelist) {
			SelectItem xx = new SelectItem(sptype.getId(), sptype.getDescription());
			spectatorTypes.add(xx);
		}
	}

	private void presetDocumentTypeList() {
		documentTypes = new ArrayList<SelectItem>();
		// List<DocumentType> dts = documentTypeService.getDocumentType();
		// recuperiamo la lista dei tipi di documenti esclusa la fotografia
		List<DocumentType> dts = documentTypeService.getDocumentTypeFinder().and("idDocumentType").ne(4).list();
		documentTypes.add(new SelectItem(null, " "));
		for (DocumentType dt : dts) {
			SelectItem xx = new SelectItem(dt.getId(), dt.getDescription());
			documentTypes.add(xx);
		}

		// photo
		documentTypePhoto = new ArrayList<SelectItem>();
		DocumentType photo = documentTypeService.getDocumentTypeFinder().and("idDocumentType").eq(4).result();
		documentTypePhoto.add(new SelectItem(null, " "));
		SelectItem yy = new SelectItem(photo.getId(), photo.getDescription());
		documentTypePhoto.add(yy);
	}

	private Long getIdLoggedAgency() {
		return Long.valueOf(currentAgencyUser.getAgency().getIdAgency());
	}

	public void confirmDeleteSpectator(ActionEvent e) {
		HtmlCommandLink component = (HtmlCommandLink) e.getSource();
		for (UIComponent com : component.getChildren()) {
			if (com instanceof UIParameter && ((UIParameter) com).getName().equalsIgnoreCase("spectatorId")) {
				spectatorToDelete = ((Long) ((UIParameter) com).getValue());
			}
		}
	}

	public void confirmDeleteDocumentSpectator(ActionEvent e) {
		HtmlCommandLink component = (HtmlCommandLink) e.getSource();
		for (UIComponent com : component.getChildren()) {
			if (com instanceof UIParameter && ((UIParameter) com).getName().equalsIgnoreCase("documentSpectatorId")) {
				documentSpectatorToDelete = ((Long) ((UIParameter) com).getValue());
			}
		}
	}

	public void agSelected(SelectEvent event) {
		try {
			System.out.println("Selezionata l'agenzia :" + agencyService.getAgencyById(selectedAgency).getName());
			lazyModelAgencySpectator = new LazyAgencySpectatorDataModel(agencySpectatorService);
			((LazyAgencySpectatorDataModel) lazyModelAgencySpectator).setAgencyId(selectedAgency);
		} catch (Exception e) {
			if (selectedAgency == null) {
				lazyModelAgencySpectator = null;
				System.out.println("nessuna  agenzia selezionata");

			} else {
				System.out.println("Errore sull agenzia selezionata" + e.toString());
			}
		}
	}

	public void onDocumentTypeSelected(SelectEvent event) {
		System.out.println("document type selezionato ha come ID " + selectedDocumentType);

		// controlliamo se il tipo documento selezionato esiste già per questo
		// spettatore
		if (documentSpectatorService.getDocumentSpectatorFinder().and("documentType.id").eq(selectedDocumentType)
				.and("spectator.id").eq(spectatorToEdit.getId()).list().isEmpty()) {
			documentUploadEnabled = true;
		} else {
			documentUploadEnabled = false;
			addWarningMessage(getMessagesBoundle().getString("documentType.alreadyAdded"));
		}

	}

	public void onDocumentTypePhotoSelected(SelectEvent evt) {
		System.out.println("document type photo ha come ID " + selectedDocumentTypePhoto);
	}

	public void handleDocumentUpload(FileUploadEvent fuevt) {
		UploadedFile file = fuevt.getFile();
		System.out.println("il file appena caricato è " + file.getFileName());
		uploadFileSize=file.getSize();

//		transformiamo il file appena caricato in Blob
		try {
			InputStream docInputStream = file.getInputstream();
			byte[] doc = IOUtils.toByteArray(docInputStream);

//			salviamo il documento sul DB
			DocumentType documentTypeToAdd = documentTypeService.getDocumentTypeById(selectedDocumentType);
			DocumentSpectator docsp = new DocumentSpectator(documentTypeToAdd, spectatorToEdit);
			docsp.setDocInBlob(doc);
			// FIXME Verificare che si prenda il base64 invece che il Blob
//			docsp.setDocInBase64(java.util.Base64.getEncoder().encodeToString(doc));
			
			docsp.setDescription(file.getFileName());
			docsp.setDocumentCreationDate(new Date());
			docsp = documentSpectatorService.addDocumentSpectator(docsp);
			addInfoMessage(getMessagesBoundle().getString("documentSpectator.saved"));

		} catch (Exception e) {
			System.out.println("errore nel salvataggio del tipo documento sul DB  " + e.toString());
		}

	}

	public void handlePhotoUpload(FileUploadEvent fuevt) {
		UploadedFile file = fuevt.getFile();
		System.out.println("la foto appena caricato è " + file.getFileName());
		uploadFileSize=file.getSize();
//		transformiamo il file appena caricato in Blob
		try {
			// se esiste almeno una foto già caricata per questo spettatore
			if (!documentSpectatorService.getDocumentSpectatorFinder().and("spectator.id").eq(spectatorToEdit.getId())
					.and("documentType.id").eq(selectedDocumentTypePhoto).list().isEmpty()) {
				addWarningMessage(getMessagesBoundle().getString("documentType.alreadyAdded"));
			} else {
				InputStream docInputStream = file.getInputstream();
				byte[] doc = IOUtils.toByteArray(docInputStream);

				// salviamo il documento sul DB
				DocumentType documentTypeToAdd = documentTypeService.getDocumentTypeById(selectedDocumentTypePhoto);
				DocumentSpectator docsp = new DocumentSpectator(documentTypeToAdd, spectatorToEdit);
				docsp.setDocInBlob(doc);
				docsp.setDescription(file.getFileName());
				docsp.setDocumentCreationDate(new Date());
				docsp = documentSpectatorService.addDocumentSpectator(docsp);
				addInfoMessage(getMessagesBoundle().getString("documentSpectator.saved"));
				PreviewSpectatorPhoto(spectatorToEdit);
			}

		} catch (Exception e) {
			System.out.println("errore nel salvataggio del tipo documento sul DB  " + e.toString());
		}
	}

	public void handleExcelSpectatorsUpload(FileUploadEvent fuevt) {
		try {
			UploadedFile file = fuevt.getFile();
			uploadFileSize=file.getSize();
			if (file.equals(null)) { // file vuoto
				addWarningMessage(getMessagesBoundle().getString("spectator.fileNull"));
			} else {
				System.out.println("il file appena caricato è " + file.getFileName());
				InputStream fileInputStream = file.getInputstream();
				OPCPackage pkg = OPCPackage.open(fileInputStream);
				XSSFWorkbook workbook = new XSSFWorkbook(pkg);

				int numSpAdded = 0;
//				Get the first worksheet
				XSSFSheet sheet = workbook.getSheetAt(0);

//				 Iterate through each rows
				Iterator<Row> rowIterator = sheet.iterator();
				
				while (rowIterator.hasNext()) {
					// Get Each Row
					Row row = rowIterator.next();
					
					System.out.println("SIAMO iterando la riga  " + row.getRowNum());
					if (row.getRowNum() != 0) {
						Iterator<Cell> cellIterator = row.cellIterator();
						Spectator newSpectator = new Spectator();
						BlackListSpectator newBLSpectator = new BlackListSpectator();

						try {
							while (cellIterator.hasNext()) {
								Cell cell = cellIterator.next();

								int columnIndex = cell.getColumnIndex();

								switch (columnIndex + 1) {
								case 1:
//									System.out.println("nome = " + cell.getStringCellValue() + "\n");
//									newSpectator.setName(cell.getStringCellValue().toUpperCase().trim());
									newSpectator.setName(TextUtil.cleanTextContent(cell.getStringCellValue().toUpperCase().trim()));
									break;
									
								case 2:
//									System.out.println("cognome = " + cell.getStringCellValue() + "\n");
//									newSpectator.setSurname(cell.getStringCellValue().toUpperCase().trim());
									newSpectator.setSurname(TextUtil.cleanTextContent(cell.getStringCellValue().toUpperCase().trim()));
									break;
									
								case 3:
//									System.out.println("datadinascita = " + cell.getDateCellValue() + "\n");
									newSpectator.setBirthDate(cell.getDateCellValue());
									break;
									
								case 4:
//									System.out.println("codice fiscale = " + cell.getStringCellValue() + "\n");
									newSpectator.setFiscalCode(cell.getStringCellValue().toUpperCase().trim());
									break;
									
								case 5:
//									System.out.println("cellulare = " + cell.getNumericCellValue() + "\n");
									newSpectator.setMobile(String.format ("%.0f", cell.getNumericCellValue()));
									break;
									
								case 6:
//									System.out.println("email = " + cell.getStringCellValue() + "\n");
									newSpectator.setEmail(cell.getStringCellValue().trim());
									break;
									
								case 7:
//									System.out.println("sesso = " + cell.getStringCellValue() + "\n");
									if (cell.getStringCellValue().toUpperCase().trim().equals("M") || cell.getStringCellValue().toUpperCase().trim().equals("MALE"))
										newSpectator.setGender(Gender.M);
									if (cell.getStringCellValue().toUpperCase().trim().equals("F") || cell.getStringCellValue().toUpperCase().trim().equals("FEMALE"))
										newSpectator.setGender(Gender.F);
									if (cell.getStringCellValue().toUpperCase().trim().equals("UNKNOWN") || cell.getStringCellValue() == null)
										newSpectator.setGender(Gender.UNKNOWN);
									break;
								case 8: 
//									System.out.println("indesiderato = " + cell.getStringCellValue() + "\n");
									if (cell.getStringCellValue().toUpperCase().trim().equals("X")) {
										newBLSpectator.setSpectator(newSpectator);
										BlackListType newBlackListType = new BlackListType();
										newBlackListType=blackListTypeService.getBlackListTypeById(1L);
										newBLSpectator.setBlackListType(newBlackListType);
									}else if(cell.getStringCellValue().equals("-")){
										newBLSpectator.setSpectator(newSpectator);
//										BlackListType newBlackListType = new BlackListType();
									}
									break;
								case 9:
//									System.out.println("note = " + cell.getStringCellValue() + "\n");
									if ((newBLSpectator.getIdBlackListSpectator()==null)&&(!(cell.getStringCellValue().isEmpty()))) {
										newBLSpectator.setReason(cell.getStringCellValue());
									}
									break;
								}
							} // end cell iterator

							
//							Controlliamo ora se esiste già uno spettatore con lo stesso codice fiscale nell'anagrafica degli spettatori
//							se questo spettatore esiste già nell anagrafica degli spettatori
							 
							 boolean checkIfExists = false;
							 
//							 Controllo se lo spettatore esiste già	per Codice Fiscale				 
							 if (!(newSpectator.getFiscalCode()==null || newSpectator.getFiscalCode().isEmpty())) {
								 checkIfExists = !(spectatorService.getSpectatorFinder().and("fiscalCode").eq(newSpectator.getFiscalCode()).list().isEmpty());
							 	 if (checkIfExists)
							 		 newSpectator = spectatorService.getSpectatorFinder().and("fiscalCode").eq(newSpectator.getFiscalCode()).result();
							 }else if (!(newSpectator.getSurname().isEmpty() || newSpectator.getName().isEmpty() || newSpectator.getBirthDate()==null)){
//								 Controllo per Nome e Cognome e Data di Nascita
								 Finder<Spectator> spfi = spectatorService.getSpectatorFinder();
				    			 spfi.and("name").eq(newSpectator.getName());
				    			 spfi.and("surname").eq(newSpectator.getSurname());
				    			 spfi.and("birthDate").eq(newSpectator.getBirthDate());
				    			 checkIfExists=(spfi.count()>0);
				    			 if (checkIfExists) 
				    				 newSpectator = spectatorService.getSpectatorFinder().and("name").eq(newSpectator.getName()).and("surname").eq(newSpectator.getSurname()).and("birthDate").eq(newSpectator.getBirthDate()).result();
							 }else if (!(newSpectator.getSurname().isEmpty() || newSpectator.getName().isEmpty())){ 
//								 Controllo per Nome e Cognome
								 Finder<Spectator> spfi = spectatorService.getSpectatorFinder();
				    			 spfi.and("name").eq(newSpectator.getName());
				    			 spfi.and("surname").eq(newSpectator.getSurname());
				    			 checkIfExists=(spfi.count()>0);
				    			 if (checkIfExists) 
				    				 newSpectator = spectatorService.getSpectatorFinder().and("name").eq(newSpectator.getName()).and("surname").eq(newSpectator.getSurname()).result();
							 } else {
								 checkIfExists=false;
							 }
							 
//							 Se il nominativo già esiste
							 if (checkIfExists) {
								 if (blackListSpectatorService.isNotInBlackList(
										 newSpectator, 
										 null, 
										 null, 
										 currentAgencyUser,
										 loggedUser,
										 true)) {
									 
//									Verifichiamo se questo spettatore è gia associato a quest'agenzia;
//									se non è ancora stato associato a quest agenzia , lo associamo
									if(agencySpectatorService.getAgencySpectatorFinder().and("agency.idAgency").eq(selectedAgency).and("spectator.id").eq(newSpectator.getId()).list().isEmpty()) {
										AgencySpectator agsp = new AgencySpectator(agencyService.getAgencyById(selectedAgency),newSpectator);
										agsp = agencySpectatorService.addAgencySpectator(agsp);
										
										String newHtml = MakeHTML.makeHTML(agsp, "agencies", "documents", "episodes", "incidents", "documentFileDownload", "spectators");
										AppUserVO uu = userService.getUserByLogin(navigationBean.getUserName());
										
										auditService.addAudit(new Audit(uu.getLastName() + " " + uu.getFirstName()+" ("+navigationBean.getUserName()+")", "Massive load: Add Spectator to agency", "AgencySpectator", agsp.getId().getIdAgency(), "", "", null, newHtml, oDiff.getDiff(null, null) ));
									}							
//									Popoliamo la liste degli spettatori aggiunti.
									newSpectator.setNote("Spettatore già presente in anagrafica. E' stato comunque reso disponibile per l'Agenzia");
									spectatorsNotAddedList.add(newSpectator);
									numSpAdded++;
								 } else {
//										Popoliamo la liste degli spettatori non aggiunti perchè in BlackList SEVERE
										newSpectator.setNote(getUnwantedMessage());
										spectatorsNotAddedList.add(newSpectator);			        			
								 }
//							  Se questo spettatore non esiste ancora nell'anagrafica degli spettatori allora 
//							  lo inserisce e lo associa agli spettatori dell'agenzia		
							} else {
								if (!(newSpectator.getSurname()==null || newSpectator.getSurname().isEmpty() || newSpectator.getName()==null || newSpectator.getName().isEmpty())) {
//									Definisco un nuovo record di correlazione tra Agenzia e Spettatore
									List<AgencySpectator> aglist = new ArrayList<>();
									AgencySpectator agspnew = new AgencySpectator(agencyService.getAgencyById(selectedAgency), newSpectator);
									aglist.add(agspnew);
//									Associo allo spettatore l'Agenzia									
									newSpectator.setAgencies(aglist);
									newSpectator.setSpectatorType(spectatorTypeService.getSpectatorTypeByIdSpectatorType(1));
//									Prova ad aggiornare codice fiscale e altri attributi rimanenti allo Spetattore									
									try {
										ManageFiscalCode mfc = new ManageFiscalCode(newSpectator.getFiscalCode(), countryCodeService);
										newSpectator.setBirthDate(mfc.getBornDate());
										newSpectator.setCity(mfc.getCountryCode());
										newSpectator.setGender(mfc.getGender());
									} catch (Exception e) {
										System.out.println("Errore nel codice fiscale");
									}
//									Inserisce il nuovo spettatore
									newSpectator = spectatorService.addSpectator(newSpectator); 
									
									String newHtml = MakeHTML.makeHTML(newSpectator, "agencies", "documents", "episodes", "incidents", "documentFileDownload");
									AppUserVO uu = userService.getUserByLogin(navigationBean.getUserName());
									auditService.addAudit(new Audit(uu.getLastName() + " " + uu.getFirstName()+" ("+navigationBean.getUserName()+")", "Massive load: Add Spectator", "Spectator", newSpectator.getId(), "", "", null, newHtml, oDiff.getDiff(null, Spectator.clone(newSpectator))));
									numSpAdded++;
//						    		Se non ho nome e cognome non posso inserirlo	
								}else {
									newSpectator.setNote("Spettatore non aggiunto perchè in mancano o il Nome o il Cognome");
									spectatorsNotAddedList.add(newSpectator);										
								}
							}
							 
							// Controlliamo se lo devo inserire in BlackList
							if (newBLSpectator.getSpectator()!=null) {
								//Aggiorno newBLSpectator con quello eventualmente aggiunto o ripreso dall'archivio perchè già presente.
								newBLSpectator.setSpectator(newSpectator);
								try {
									if (newBLSpectator.getBlackListType()==null) {
//										Devo eliminare
//										Cerco se esiste il nominativo 
										Finder<Spectator> spfi = spectatorService.getSpectatorFinder();
										spfi.and("surname").eq(newSpectator.getSurname());
										spfi.and("name").eq(newSpectator.getName());
										if (!(newSpectator.getFiscalCode().equals(null))) {
											spfi.and("fiscalCode").eq(newSpectator.getFiscalCode());
										} else if (!(newSpectator.getBirthDate().equals(null))) {
											spfi.and("birthDate").eq(newSpectator.getBirthDate());
										}
//										Se esiste gia il nominativo
										if (!(spfi.setDistinct().result()).equals(null)) {
											newSpectator=spfi.setDistinct().result();
											List< BlackListSpectator> conts = blackListSpectatorService.getBlackListSpectatorFinder().and("spectator.id").eq(newSpectator.getId()).setDistinct().list();
											for (BlackListSpectator blackListSpectator : conts) {
												blackListSpectatorService.deleteBlackListSpectator(blackListSpectator.getIdBlackListSpectator());
											}

											try {
												List<SpectatorEpisode> spectatorEpisodes = spectatorService.getSpectatorById(newSpectator.getId()).getEpisodes();
												
												for (SpectatorEpisode spectatorEpisode : spectatorEpisodes) {
													emailMakerService.removeSpectatorFromList(spectatorEpisode);
												}											
											} catch (Exception e) {
												System.out.println("errore dati sul file Excel - riga  " + row.getRowNum()+" impossibile rimuovere il nominativo dalla blacklist");
												e.printStackTrace();
											}
											try {
												spectatorService.deleteSpectator(newSpectator.getId());	
											} catch (Exception e) {
												System.out.println("errore dati sul file Excel - riga  " + row.getRowNum()+" impossibile cancellare dalla base dati il nominativo dalla blacklist");
												e.printStackTrace();
											}										 
										}
						    			 
									}else {
										
//										Devo inserire in blackList checkBlackListSpectator
										//newBLSpectator.setSpectator(newSpectator);
										
//										Se non è già stato inserito in blacklist
										if (!checkBlackListSpectator(newBLSpectator.getSpectator())) {
											try {
												blackListSpectatorService.updateBlackListSpectator(newBLSpectator);	
											} catch (Exception e) {
												System.out.println("errore dati sul file Excel - riga  " + row.getRowNum()+" impossibile inserire in blackList il nominativo dalla blacklist");
												e.printStackTrace();
											} 
//										Se è gia stato inserito accodo una nuova ragione di inserimento.		
										} else {
											BlackListSpectator blackListSpectator = blackListSpectatorService.getBlackListSpectatorFinder().and("spectator.id").eq(newBLSpectator.getSpectator().getId()).setDistinct().result();
											blackListSpectator.setReason(blackListSpectator.getReason()+"\n\n"+newBLSpectator.getReason());
											try {
												blackListSpectatorService.updateBlackListSpectator(blackListSpectator);	
											} catch (Exception e) {
												System.out.println("errore dati sul file Excel - riga  " + row.getRowNum()+" impossibile aggiornare nella base dati la nuova motivazione aggiuntiva per il nominativo dalla blacklist "+blackListSpectator.getSpectator().getFullName());
												e.printStackTrace();
											}
										}
									}
								} catch (Exception e) {
									newBLSpectator=null;
									System.out.println("errore dati sul file Excel - riga  " + row.getRowNum()+" impossibile caricare il nominativo in blacklist");
									e.printStackTrace();
								}
							} 

						} catch (Exception e) {
							System.out.println("errore dati sul file Excel - riga  " + row.getRowNum());
							newSpectator.setNote("controlla la riga " + row.getRowNum() + " del file excel");
							spectatorsNotAddedList.add(newSpectator);
							uploadFileSize=0;
							e.printStackTrace();
						}
					}
				} // end row iterator
				addInfoMessage(getMessagesBoundle().getString("spectator.saved") + " \n Aggiunti " + numSpAdded	+ " spettatori");
			}
		} catch (Exception e) {
			addErrorMessage(getMessagesBoundle().getString("spectator.errorReadingFile") + e);
			uploadFileSize=0;
			e.printStackTrace();
		}
	}

	public void cancelDelete(ActionEvent ev) {
		System.out.println("\nDelete Spectator canceled.");
		spectatorToDelete = null;
	}

	public void cancelDeleteDocumentSpectator(ActionEvent ev) {
		System.out.println("\nDelete Document Spectator canceled.");
		documentSpectatorToDelete = null;
	}

	public void delete(ActionEvent ev) {
		System.out.println("\nDelete Test ...");
		try {

//			Acquisisco l'ID dello spettatore
			Spectator spToDel = spectatorService.getSpectatorById(spectatorToDelete);
			String oldHtml = MakeHTML.makeHTML(spToDel, "agencies", "documents", "episodes", "incidents", "documentFileDownload");
			
//			Ne elimino eventuali email pendenti 
//			ed elimino lo spettatore da eventuali liste dell'Agenzia
//			List<SpectatorEpisode> spectatorEpisodes = spectatorService.getSpectatorById(spectatorToDelete).getEpisodes();
			List<SpectatorEpisode> spectatorEpisodes = spectatorEpisodeService.getSpectatorEpisodeFinder().and("spectator.id").eq(spectatorToDelete).and("production.agency.idAgency").eq(selectedAgency).setDistinct().list();
			for (SpectatorEpisode spectatorEpisode : spectatorEpisodes) {
				emailMakerService.removeSpectatorFromList(spectatorEpisode);
				spectatorEpisodeService.deleteSpectatorEpisode(spectatorEpisode.getId());
			}
			
//			Elimino lo spettatore dalla mia Agenzia
			List<AgencySpectator> lstAgencySpectatorToDelete= agencySpectatorService.getAgencySpectatorFinder().and("spectator.id").eq(spToDel.getId()).setDistinct().list();

			boolean otherSpectatorAgencies = false;
			for (AgencySpectator agencySpectator : lstAgencySpectatorToDelete) {
				if (agencySpectator.getAgency().getIdAgency()==selectedAgency) {
					agencySpectatorService.deleteAgencySpectator(agencySpectator.getId());
					otherSpectatorAgencies = otherSpectatorAgencies || false;
				} else {
					otherSpectatorAgencies = otherSpectatorAgencies || true;
				}
			} 
//			Se non è utilizzato da altre agenzie allora lo elimino definitivamente			
			if (!otherSpectatorAgencies) spectatorService.deleteSpectator(spectatorToDelete);
			
			AppUserVO uu = userService.getUserByLogin(navigationBean.getUserName());
			auditService.addAudit(new Audit(uu.getLastName() + " " + uu.getFirstName()+" ("+navigationBean.getUserName()+")", "Delete", "Spectator", spToDel.getId(), "", "", oldHtml, null, oDiff.getDiff(Spectator.clone(spToDel), null)));
			
			addInfoMessage(getMessagesBoundle().getString("spectator.deleted"));
		} catch (Exception e) {
			e.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("spectator.deletingError"), e);
		}
	}

	public void deleteDocumentSpectator(ActionEvent ev) {
		try {
			// se il documento da cancellare è la fotografia, allora rimuoviamo anche la
			// preview della foto
			if (documentSpectatorService.getDocumentSpectatorById(documentSpectatorToDelete).getDocumentType()
					.getIdDocumentType() == 4) {
				spectatorPhotoAsBase64 = null;
			}
			documentSpectatorService.deleteDocumentSpectator(documentSpectatorToDelete);
			addInfoMessage(getMessagesBoundle().getString("documentSpectator.deleted"));

		} catch (Exception e) {
			e.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("documentSpectator.deletingError"), e);
		}
	}

	public void addSpectator(ActionEvent e) {
		spectatorToEdit = new Spectator();
		spectatorToInvite= new Spectator();
		selectedSpectatorType = null;
		lazyModelDocumentSpectator=null;
		lazyModelSpectatorEpisode=null;
		lazyModelSpectatorIncident=null;
	}

	public void modifySpectator(ActionEvent e) {
		checkCfInserted = null;

		HtmlCommandLink component = (HtmlCommandLink) e.getSource();
		for (UIComponent com : component.getChildren()) {
			if (com instanceof UIParameter && ((UIParameter) com).getName().equalsIgnoreCase("spectatorId")) {
				spectatorToEdit = spectatorService.getSpectatorById((Long) ((UIParameter) com).getValue());
				try {
					selectedSpectatorType = spectatorToEdit.getSpectatorType().getId();
				} catch (Exception exc) {
					selectedSpectatorType = null;
					System.out.println("probabilmente nuovo Spettatore senza tipo spettatore assegnato");
				}

				selectedAgencies = spectatorToEdit.getAgencies();

				// Popoliamo la lista dei document type per docs & photo
				presetDocumentTypeList();

				// Preview Photo Spectator se cè
				PreviewSpectatorPhoto(spectatorToEdit);

				// Carichiamo nel datatable tutti i documenti relatvi allo spettatore
				// Selezionato
				lazyModelDocumentSpectator = new LazyDocumentSpectatorDataModel(documentSpectatorService);
				((LazyDocumentSpectatorDataModel) lazyModelDocumentSpectator).setSpectatorId(spectatorToEdit.getId());

				// Carichiamo gli eventi ai quali ha partecipato lo spettatore
				lazyModelSpectatorEpisode = new LazySpectatorEpisodeDataModel(spectatorEpisodeService);
				((LazySpectatorEpisodeDataModel) lazyModelSpectatorEpisode).setSpectatorId(spectatorToEdit.getId());
				
				lazyModelSpectatorIncident = new LazySpectatorIncidentDataModel(spectatorIncidentService);
				((LazySpectatorIncidentDataModel) lazyModelSpectatorIncident).setSpectatorId(spectatorToEdit.getId());					
				
				if(lazyModelSpectatorIncident.getRowCount()>0) {
					messageBlackList = "User on Blacklist";
				}
			}
		}
	}

	public void PreviewSpectatorPhoto(Spectator sp) {
		try {
			DocumentType fototype = documentTypeService.getDocumentTypeFinder().and("idDocumentType").eq(4).result();
			// se esiste un documento di tipo fotografia associato a questo spettatore
			if (!documentSpectatorService.getDocumentSpectatorFinder().and("spectator.id").eq(sp.getId())
					.and("documentType.id").eq(fototype.getId()).list().isEmpty()) {
				DocumentSpectator docspec = documentSpectatorService.getDocumentSpectatorFinder().and("spectator.id")
						.eq(sp.getId()).and("documentType.id").eq(fototype.getId()).result();
				spectatorPhotoAsBase64 = Base64Utils.encodeToString(docspec.getDocInBlob());
			} else {
				spectatorPhotoAsBase64 = null;
			}
		} catch (Exception e) {
			System.out.println("errore nella preview della foto dello spettatore" + e.toString());
		}
	}

	public void cancelEditSpectator(ActionEvent e) {
		spectatorToEdit = null;
	}

	public void saveSpectator(ActionEvent e) {
		System.out.println("Salvo " + spectatorToEdit);
		System.out.println("descrizione azione " + e);

		if (spectatorToEdit.getName() == null || spectatorToEdit.getName().trim().equals("")) {
			addWarningMessage("test su Descrizione richiesto!");
			return;
		}

		if (selectedSpectatorType == null) {
			addWarningMessage("test su Tipo Spettatore richiesto!");
			return;
		}

		try {
			
			if (spectatorToEdit.getId()==null) {
				if (spectatorToEdit.getFiscalCode()!=null && !spectatorToEdit.getFiscalCode().trim().equals("")) {
					// Controllo unicità codice fiscale
					List<Spectator> lstSpectator = spectatorService.getSpectatorFinder().and("fiscalCode").eq(spectatorToEdit.getFiscalCode()).setDistinct().list();
					if (lstSpectator!=null && lstSpectator.size()>0) {
						// Già esistente, lo assegno all'agenzia corrente!
						
						// aggiungiamo il record se non c'è nella tabella spettatore agenzia -- per ora
						// lo puo fare solo l'AGENZIA
						if (haveUserRole("ROLE_AGENCY")) {
							if (agencySpectatorService.getAgencySpectatorFinder().and("spectator.id").eq(lstSpectator.get(0).getId())
									.and("agency.id").eq(currentAgencyUser.getAgency().getIdAgency()).setDistinct().list()
									.isEmpty()) {
								AgencySpectator agSpToAdd = new AgencySpectator(currentAgencyUser.getAgency(), lstSpectator.get(0));
								if (blackListSpectatorService.isNotInBlackList(
										lstSpectator.get(0), 
										 null, 
										 null, 
										 null,
										 null,
										 false)){
								agSpToAdd = agencySpectatorService.updateAgencySpectator(agSpToAdd);	
								} else {
									System.out.println("Spettatore agenzia già esistente e in Lista Indesiderati");
									Throwable cause = new Throwable("1"); 
									throw new Exception("Spettatore agenzia già esistente e in Lista Indesiderati",cause );
								}
//								agSpToAdd = agencySpectatorService.updateAgencySpectator(agSpToAdd);
							}
						} else {
							System.out.println("coppia spettatore agenzia già esistente");
						}
						
	//					throw new Exception("Spettatore già presente in anagrafica!");
					}
					else {
						// popoliamo la tabella Spettatore
						// Imposta il valore della dropdown list nella classe
						spectatorToEdit.setSpectatorType(spectatorTypeService.getSpectatorTypeById(selectedSpectatorType));
						spectatorToEdit.setToReview(false);
						resetReviews();
						spectatorToEdit = spectatorService.updateSpectator(spectatorToEdit);
		
						String newHtml = MakeHTML.makeHTML(spectatorToEdit, "agencies", "documents", "episodes", "incidents", "documentFileDownload");
						AppUserVO uu = userService.getUserByLogin(navigationBean.getUserName());
						auditService.addAudit(new Audit(uu.getLastName() + " " + uu.getFirstName()+" ("+navigationBean.getUserName()+")", "Insert", "Spectator", spectatorToEdit.getId(), "", "", null, newHtml, oDiff.getDiff(null, Spectator.clone(spectatorToEdit))));
			        	
						
						// aggiungiamo il record se non c'è nella tabella spettatore agenzia -- per ora
						// lo puo fare solo l'AGENZIA
						if (haveUserRole("ROLE_AGENCY")) {
							if (agencySpectatorService.getAgencySpectatorFinder().and("spectator.id").eq(spectatorToEdit.getId())
									.and("agency.id").eq(currentAgencyUser.getAgency().getIdAgency()).setDistinct().list()
									.isEmpty()) {
								AgencySpectator agSpToAdd = new AgencySpectator(currentAgencyUser.getAgency(), spectatorToEdit);
								
								//----------------------------
								if (blackListSpectatorService.isNotInBlackList(
										spectatorToEdit, 
										 null, 
										 null, 
										 null,
										 null,
										 false)){
								agSpToAdd = agencySpectatorService.updateAgencySpectator(agSpToAdd);	
								} else {
									System.out.println("Spettatore agenzia già esistente e in Lista Indesiderati");
									Throwable cause = new Throwable("1"); 
									throw new Exception("Spettatore agenzia già esistente e in Lista Indesiderati",cause );
								}								
								//----------------------------
//								agSpToAdd = agencySpectatorService.updateAgencySpectator(agSpToAdd);
							}
						} else {
							System.out.println("coppia spettatore agenzia già esistente");
						}
					}
				}
				else {
	
					// Controllo unicità nome, cognome, data nascita
					Finder<Spectator> spfi = spectatorService.getSpectatorFinder();
					if (spectatorToEdit.getName()!=null && !spectatorToEdit.getName().equals("")) spfi.and("name").eq(spectatorToEdit.getName());
					if (spectatorToEdit.getSurname()!=null && !spectatorToEdit.getSurname().equals("")) spfi.and("surname").eq(spectatorToEdit.getSurname());
					if (spectatorToEdit.getBirthDate()!=null) spfi.and("birthDate").eq(spectatorToEdit.getBirthDate());
					List<Spectator> spsss = spfi.setDistinct().list();
					if (spsss!=null && spsss.size()>0) {
						// Già esistente, lo assegno all'agenzia corrente!
						// aggiungiamo il record se non c'è nella tabella spettatore agenzia -- per ora
						// lo puo fare solo l'AGENZIA
						if (haveUserRole("ROLE_AGENCY")) {
							if (agencySpectatorService.getAgencySpectatorFinder().and("spectator.id").eq(spsss.get(0).getId())
									.and("agency.id").eq(currentAgencyUser.getAgency().getIdAgency()).setDistinct().list()
									.isEmpty()) {
								AgencySpectator agSpToAdd = new AgencySpectator(currentAgencyUser.getAgency(), spsss.get(0));
								if (blackListSpectatorService.isNotInBlackList(
										spsss.get(0), 
										 null, 
										 null, 
										 null,
										 null,
										 false)){
								agSpToAdd = agencySpectatorService.updateAgencySpectator(agSpToAdd);	
								} else {
									System.out.println("Spettatore agenzia già esistente e in Lista Indesiderati");
									Throwable cause = new Throwable("1"); 
									throw new Exception("Spettatore agenzia già esistente e in Lista Indesiderati",cause );
								}									
//								agSpToAdd = agencySpectatorService.updateAgencySpectator(agSpToAdd);
							}
						} else {
							System.out.println("coppia spettatore agenzia già esistente");
						}
						
	//					throw new Exception("Spettatore già presente in anagrafica!");
					}
					else {
						
	    				// popoliamo la tabella Spettatore
						// Imposta il valore della dropdown list nella classe
						spectatorToEdit.setSpectatorType(spectatorTypeService.getSpectatorTypeById(selectedSpectatorType));
						spectatorToEdit.setToReview(false); 
						resetReviews();
						spectatorToEdit = spectatorService.updateSpectator(spectatorToEdit);
	
						
						String newHtml = MakeHTML.makeHTML(spectatorToEdit, "agencies", "documents", "episodes", "incidents", "documentFileDownload");
						AppUserVO uu = userService.getUserByLogin(navigationBean.getUserName());
						auditService.addAudit(new Audit(uu.getLastName() + " " + uu.getFirstName()+" ("+navigationBean.getUserName()+")", "Insert", "Spectator", spectatorToEdit.getId(), "", "", null, newHtml, oDiff.getDiff(null, Spectator.clone(spectatorToEdit))));
			        	
			        	
						// aggiungiamo il record se non c'è nella tabella spettatore agenzia -- per ora
						// lo puo fare solo l'AGENZIA
						if (haveUserRole("ROLE_AGENCY")) {
							if (agencySpectatorService.getAgencySpectatorFinder().and("spectator.id").eq(spectatorToEdit.getId())
									.and("agency.id").eq(currentAgencyUser.getAgency().getIdAgency()).setDistinct().list()
									.isEmpty()) {
								AgencySpectator agSpToAdd = new AgencySpectator(currentAgencyUser.getAgency(), spectatorToEdit);
								
								if (blackListSpectatorService.isNotInBlackList(
										spectatorToEdit, 
										 null, 
										 null, 
										 null,
										 null,
										 false)){
								agSpToAdd = agencySpectatorService.updateAgencySpectator(agSpToAdd);	
								} else {
									System.out.println("Spettatore agenzia già esistente e in Lista Indesiderati");
									Throwable cause = new Throwable("1"); 
									throw new Exception("Spettatore agenzia già esistente e in Lista Indesiderati",cause ); 
								}								
//								agSpToAdd = agencySpectatorService.updateAgencySpectator(agSpToAdd);
							}
						} else {
							System.out.println("coppia spettatore agenzia già esistente");
						}
	    			}
				}
			}
			else {
				// UPDATE
				
				Spectator oldSpect = spectatorService.getSpectatorById(spectatorToEdit.getId());
				String oldHtml = null;
				if (oldSpect!=null) {
					oldHtml = MakeHTML.makeHTML(oldSpect, "agencies", "documents", "episodes", "incidents", "documentFileDownload");	
				}
				oldSpect = Spectator.clone(oldSpect); 

				spectatorToEdit.setSpectatorType(spectatorTypeService.getSpectatorTypeById(selectedSpectatorType));
				spectatorToEdit.setToReview(false);
				resetReviews();
//		        System.out.print("Start: "+ new Timestamp((new Date()).getTime()));
				spectatorToEdit = spectatorService.updateSpectator(spectatorToEdit);
//				System.out.println(" - End: "+ new Timestamp((new Date()).getTime()));
				Spectator newSpect =Spectator.clone(spectatorToEdit);
				
				String newHtml = MakeHTML.makeHTML(spectatorToEdit, "agencies", "documents", "episodes", "incidents", "documentFileDownload");
				AppUserVO uu = userService.getUserByLogin(navigationBean.getUserName());
				auditService.addAudit(new Audit(uu.getLastName() + " " + uu.getFirstName()+" ("+navigationBean.getUserName()+")", "Update", "Spectator", spectatorToEdit.getId(), "", "", oldHtml, newHtml, oDiff.getDiff(oldSpect, newSpect )));
				oldSpect=null;	  
				newSpect =null;
				
				// aggiungiamo il record se non c'è nella tabella spettatore agenzia 
				// lo puo fare solo l'AGENZIA
				if (haveUserRole("ROLE_AGENCY")) {
					if (agencySpectatorService.getAgencySpectatorFinder().and("spectator.id").eq(spectatorToEdit.getId())
							.and("agency.id").eq(currentAgencyUser.getAgency().getIdAgency()).setDistinct().list()
							.isEmpty()) {
						AgencySpectator agSpToAdd = new AgencySpectator(currentAgencyUser.getAgency(), spectatorToEdit);
						
						if (blackListSpectatorService.isNotInBlackList(
								spectatorToEdit, 
								 null, 
								 null, 
								 null,
								 null,
								 false)){
						agSpToAdd = agencySpectatorService.updateAgencySpectator(agSpToAdd);	
						} else {
							System.out.println("Spettatore agenzia già esistente e in Lista Indesiderati");
							Throwable cause = new Throwable("1"); 
							throw new Exception("Spettatore agenzia già esistente e in Lista Indesiderati",cause );
						}						
//						agSpToAdd = agencySpectatorService.updateAgencySpectator(agSpToAdd);
					}
				} else {
					System.out.println("coppia spettatore agenzia già esistente");
				}
			}
			//gson openjson

			addInfoMessage(getMessagesBoundle().getString("spectator.saved"));
		} catch (Exception e1) {
			if (e1.getCause().getMessage().equals("1")) {
				addErrorMessage(getUnwantedMessage());
			}else {
				addErrorMessage(getMessagesBoundle().getString("spectator.savingError"), e1);	
			}
			e1.printStackTrace();
		}
		spectatorToEdit = null;
		checkCfInserted = null;
	}

	
	private void resetReviews() {
		try {
			List<SpectatorEpisode> episodes = spectatorEpisodeService.getSpectatorEpisodeFinder().and("spectator.id").eq(spectatorToEdit.getId()).setDistinct().list();
			for (SpectatorEpisode se : episodes ) {
				se.setSpectatorToReview(false);
				spectatorEpisodeService.updateSpectatorEpisode(se);
			}				
		} catch (Exception e) {
			e.printStackTrace();	
		}
	}
	
	
	public List<CountryCode> completeCountryCode(String query) {

		List<CountryCode> filteredCountriesCode = countryCodeService.getCountryCodeFinder().and("city").like(query)
				.setDistinct().list();
		return filteredCountriesCode;
	}

	public void fiscalCodeInserted() {
		try {
			ManageFiscalCode mfc = new ManageFiscalCode(spectatorToEdit.getFiscalCode(), countryCodeService);
			fisCodWarningStyle = "color:green";
			checkCfInserted = "codice fiscale inserito valido";
			spectatorToEdit.setBirthDate(mfc.getBornDate());
			spectatorToEdit.setCity(mfc.getCountryCode());
			spectatorToEdit.setGender(mfc.getGender());

		} catch (Exception e) {
			System.out.println("errore nel codice fiscale");
			fisCodWarningStyle = "color:red";
			checkCfInserted = "controlla il codice fiscale inserito " + spectatorToEdit.getFiscalCode() + " !!!";
		}

	}

	public boolean haveUserRole(String roles) {
		
		String[] listRoles = roles.split(",");
		for (int i = 0; i < listRoles.length; i++) {
			listRoles[i] = listRoles[i].trim();
			
		}
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication!=null && authentication.getAuthorities()!=null) {
			//String currentPrincipalName = authentication.getName();
			for (GrantedAuthority r : authentication.getAuthorities()) {
				//System.out.println("Ruolo:"+r.getAuthority());
				for (int i = 0; i < listRoles.length; i++) {
					if (r.getAuthority()!=null && r.getAuthority().equals(listRoles[i])) {
						//System.out.println("trovato ruolo !!!");
						return true;
					}					
				}
			}
		}
		return false;
	}

	public void encodeSpectator() {
		switch (Integer.parseInt(PropertiesUtils.getInstance().readProperty("dummy.swapSurname","3"))) {
		case 1:
			try {
				List<Spectator> listSpectator2Encode = spectatorService.getSpectator();
				for (Spectator spectator : listSpectator2Encode) {
					try {
						spectator.setEn_surname(spectator.getSurname());
						spectator.setName(spectator.getName().toUpperCase().trim());
						spectator = spectatorService.updateSpectator(spectator );	
					}catch (Exception e) {
						
					}
				}				
			} catch (Exception e) {
				// TODO: handle exception
			}
			break;
		case 2:
			try {
				List<Spectator> listSpectator2Encode = spectatorService.getSpectator();
				for (Spectator spectator : listSpectator2Encode) {
					try {
						spectator.setSurname(spectator.getEn_surname());
						spectator = spectatorService.updateSpectator(spectator );				
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			break;
		case 3:
			break;
		default:
			break;
		}
	}
	
	public StreamedContent getStreamContent(DocumentSpectator ds) {
		StreamedContent file;
		InputStream myInputStream = new ByteArrayInputStream(ds.getDocInBlob());
		String ext = FilenameUtils.getExtension(ds.getDescription());
		file = new DefaultStreamedContent(myInputStream, ext, ds.getDescription());
		return file;
	}

	public StreamedContent getOriginalDocFromBase64Str(String base64doc) {
		DefaultStreamedContent file;
		InputStream myInputStream = new ByteArrayInputStream(Base64.decode(base64doc));
		file = new DefaultStreamedContent(myInputStream);
		file.setName("document.pdf");
		return file;
	}

	public Spectator getSpectatorToEdit() {
		return spectatorToEdit;
	}

	public void setSpectatorToEdit(Spectator spectatorToEdit) {
		this.spectatorToEdit = spectatorToEdit;
	}

	public Object getSpectatorById(long id) {
		return spectatorService.getSpectatorById(id);
	}

	public LazyDataModel<Spectator> getLazyModelSpectator() {
		return lazyModelSpectator;
	}

	public List<SelectItem> getSpectatorTypes() {
		return spectatorTypes;
	}

	public void setSpectatorTypes(List<SelectItem> spectatorTypes) {
		this.spectatorTypes = spectatorTypes;
	}

	public Long getSelectedSpectatorType() {
		return selectedSpectatorType;
	}

	public void setSelectedSpectatorType(Long selectedSpectatorType) {
		this.selectedSpectatorType = selectedSpectatorType;
	}

	public CountryCodeService getCountryCodeService() {
		return countryCodeService;
	}

	public void setCountryCodeService(CountryCodeService countryCodeServiceService) {
		this.countryCodeService = countryCodeServiceService;
	}

	public CountryCode getCountryCodeById(Long id) {
		return countryCodeService.getCountryCodeById(id);
	}

	public String getCheckCfInserted() {
		return checkCfInserted;
	}

	public void setCheckCfInserted(String checkCfInserted) {
		this.checkCfInserted = checkCfInserted;
	}

	public String getFisCodWarningStyle() {
		return fisCodWarningStyle;
	}

	public void setFisCodWarningStyle(String fisCodWarningStyle) {
		this.fisCodWarningStyle = fisCodWarningStyle;
	}

	public AgencyUser getCurrentAgencyUser() {
		return currentAgencyUser;
	}

	public void setCurrentAgencyUser(AgencyUser currentAgencyUser) {
		this.currentAgencyUser = currentAgencyUser;
	}

	public LazyDataModel<AgencySpectator> getLazyModelAgencySpectator() {
		return lazyModelAgencySpectator;
	}

	public void setLazyModelAgencySpectator(LazyDataModel<AgencySpectator> lazyModelAgencySpectator) {
		this.lazyModelAgencySpectator = lazyModelAgencySpectator;
	}

	public List<AgencySpectator> getSelectedAgencies() {
		return selectedAgencies;
	}

	public void setSelectedAgencies(List<AgencySpectator> selectedAgencies) {
		this.selectedAgencies = selectedAgencies;
	}

	public List<SelectItem> getAgencies() {
		return agencies;
	}

	public void setAgencies(List<SelectItem> agencies) {
		this.agencies = agencies;
	}

	public Long getSelectedAgency() {
		return selectedAgency;
	}

	public void setSelectedAgency(Long selectedAgency) {
		this.selectedAgency = selectedAgency;
	}

	public AgencyService getAgencyService() {
		return agencyService;
	}

	public void setAgencyService(AgencyService agencyService) {
		this.agencyService = agencyService;
	}

	public LazyDataModel<DocumentSpectator> getLazyModelDocumentSpectator() {
		return lazyModelDocumentSpectator;
	}

	public void setLazyModelDocumentSpectator(LazyDataModel<DocumentSpectator> lazyModelDocumentSpectator) {
		this.lazyModelDocumentSpectator = lazyModelDocumentSpectator;
	}

	public DocumentSpectatorService getDocumentSpectatorService() {
		return documentSpectatorService;
	}

	public void setDocumentSpectatorService(DocumentSpectatorService documentSpectatorService) {
		this.documentSpectatorService = documentSpectatorService;
	}

	public Long getSelectedDocumentType() {
		return selectedDocumentType;
	}

	public void setSelectedDocumentType(Long selectedDocumentType) {
		this.selectedDocumentType = selectedDocumentType;
	}

	public List<SelectItem> getDocumentTypes() {
		return documentTypes;
	}

	public void setDocumentTypes(List<SelectItem> documentTypes) {
		this.documentTypes = documentTypes;
	}

	public boolean isDocumentUploadEnabled() {
		return documentUploadEnabled;
	}

	public void setDocumentUploadEnabled(boolean documentUploadEnabled) {
		this.documentUploadEnabled = documentUploadEnabled;
	}

	public Long getDocumentSpectatorToDelete() {
		return documentSpectatorToDelete;
	}

	public void setDocumentSpectatorToDelete(Long documentSpectatorToDelete) {
		this.documentSpectatorToDelete = documentSpectatorToDelete;
	}

	public Long getSelectedDocumentTypePhoto() {
		return selectedDocumentTypePhoto;
	}

	public void setSelectedDocumentTypePhoto(Long selectedDocumentTypePhoto) {
		this.selectedDocumentTypePhoto = selectedDocumentTypePhoto;
	}

	public List<SelectItem> getDocumentTypePhoto() {
		return documentTypePhoto;
	}

	public void setDocumentTypePhoto(List<SelectItem> documentTypePhoto) {
		this.documentTypePhoto = documentTypePhoto;
	}

	public String getSpectatorPhotoAsBase64() {
		return spectatorPhotoAsBase64;
	}

	public void setSpectatorPhotoAsBase64(String spectatorPhotoAsBase64) {
		this.spectatorPhotoAsBase64 = spectatorPhotoAsBase64;
	}

	public LazyDataModel<SpectatorEpisode> getLazyModelSpectatorEpisode() {
		return lazyModelSpectatorEpisode;
	}

	public void setLazyModelSpectatorEpisode(LazyDataModel<SpectatorEpisode> lazyModelSpectatorEpisode) {
		this.lazyModelSpectatorEpisode = lazyModelSpectatorEpisode;
	}

	public List<Spectator> getSpectatorsNotAdded() {
		return spectatorsNotAddedList;
	}

	public void setSpectatorsNotAdded(List<Spectator> spectatorsNotAdded) {
		this.spectatorsNotAddedList = spectatorsNotAdded;
	}

	public int uploadProgress() {
		return 10;
	}
	public void onUploadCompelte() {
		System.out.println("Progress Completed");
	}

	public LazyDataModel<SpectatorIncident> getLazyModelSpectatorIncident() {
		return lazyModelSpectatorIncident;
	}

	public void setLazyModelSpectatorIncident(LazyDataModel<SpectatorIncident> lazyModelSpectatorIncident) {
		this.lazyModelSpectatorIncident = lazyModelSpectatorIncident;
	}
	
	public String getMessageBlackList() {
		return messageBlackList;
	}

	public void setMessageBlackList(String messageBlackList) {
		this.messageBlackList = messageBlackList;
	}

	public String getBlackListTypeDescription(Spectator sp) {
		String desc = null;
		BlackListSpectator blistsp = blackListSpectatorService.getBlackListSpectatorFinder().and("spectator.id").eq(sp.getId()).result();
		if(blistsp != null && blistsp.getBlackListType()!=null) {
			desc = blistsp.getBlackListType().getDescription();
		}
		return desc;
	}
	
	public boolean checkBlackListSpectator(Spectator sp) {
		
		if(sp!=null) {
			BlackListSpectator blistsp = blackListSpectatorService.getBlackListSpectatorFinder().and("spectator.id").eq(sp.getId()).result();
			if(blistsp != null) {
				return true;
			}
		}
		return false;
	}
	

//---------------------------------------------------------------------------------------------------------------------	
//	  private String getAbsolutePath() throws UnsupportedEncodingException {
//
//			String path = this.getClass().getClassLoader().getResource("").getPath();
//			String fullPath = URLDecoder.decode(path, "UTF-8");
//			String pathArr[] = fullPath.split("/WEB-INF/classes/");
//			System.out.println(fullPath);
//			System.out.println(pathArr[0]);
//			fullPath = pathArr[0];
//			String reponsePath = "";
//
//			// to read a file from webcontent
//			//reponsePath = new File(fullPath).getPath() + File.separatorChar + "newfile.txt";
//			reponsePath = new File(fullPath).getPath();
//			System.out.println("getAbsolutePath:" + reponsePath);
//			return reponsePath;
//
//		}

	public boolean isMultiAgenzia() {
		return multiAgenzia;
	}

	public void setMultiAgenzia(boolean multiAgenzia) {
		this.multiAgenzia = multiAgenzia;
	}

	private String getUnwantedMessage() {
		return unwantedMessage;
	}

	private void setUnwantedMessage(String unwantedMessage) {
		this.unwantedMessage = unwantedMessage;
	}
	
//	  public void sendInvite(ActionEvent e) {
//		HtmlCommandLink component = (HtmlCommandLink) e.getSource();
//		for (UIComponent com : component.getChildren()) {
//			if (com instanceof UIParameter && ((UIParameter) com).getName().equalsIgnoreCase("spectatorId")) {
//				Long idSpectatorToinvite =((Long) ((UIParameter) com).getValue());
//				spectatorToInvite  = spectatorService.getSpectatorById(idSpectatorToinvite);
//			}
//		}
//		
//		//Se lo spettatore esiste
//		if (spectatorToInvite != null) {
//			try {
//				System.out.println("Genero pdf...");         
//	            EmailQueue emailToSend = new EmailQueue(); 
//	            emailToSend.setFromEmail("fabrizio.papale@gmail.com");
//	            emailToSend.setToEmail("f.papale@crismasecurity.it");
//	            emailToSend.setCcEmail("");
//	            emailToSend.setSendDate(java.sql.Date.valueOf(LocalDate.now()));
//	            emailToSend.setSendStatus(0);
//	            emailToSend.setSubject("");
//	            emailToSend.setBody("Prova invio");
//	            emailToSend.setEntityType(2);
//	            emailToSend.setEntityID(spectatorToInvite.getId());
//	            emailToSend.setSendStatus(0);
////	            emailToSend.setPdfRepBase64Str(java.util.Base64.getEncoder().encodeToString(baos.toByteArray()));
//				
//	            emailToSend= emailQueueService.updateEmailQueue(emailToSend);
//			} catch (Exception e2) {
//				e2.printStackTrace();
//				System.out.println("Errore Preset Current User");
//				addErrorMessage(getMessagesBoundle().getString("spectatorEpisode.presetCurrentUser"), e2);
//			}				
//		}
//	}
}
