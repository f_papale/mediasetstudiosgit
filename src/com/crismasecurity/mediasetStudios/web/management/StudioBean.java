package com.crismasecurity.mediasetStudios.web.management;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.persistence.PersistenceException;

import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import com.crismasecurity.mediasetStudios.core.entity.ProductionCenter;
import com.crismasecurity.mediasetStudios.core.entity.Studio;
import com.crismasecurity.mediasetStudios.core.services.ProductionCenterService;
import com.crismasecurity.mediasetStudios.core.services.StudioService;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;

//@ManagedBean(name= EmployeesBean.BEAN_NAME) // JSF managed
//@SessionScoped // JSF managed
//@ViewScoped // JSF managed

//Spring managed
@Controller (StudioBean.BEAN_NAME) 
//@Scope("session")
@Scope("view")
public class StudioBean extends BaseBean {

	private static final long serialVersionUID = -6065141989753335782L;

	public static final String BEAN_NAME = "studioBean";
    public String getBeanName() { return BEAN_NAME; } 


    @Autowired
    private StudioService studioService ;
    @Autowired
    private ProductionCenterService productionCenterService;
    
    
    private LazyDataModel<Studio> lazyModelStudio;
    private Date filterFrom;
	private Date filterTo;
	
	private Studio studioToEdit = null;
	
	private List<SelectItem> productionCenters;
	private Long selectedProductionCenter = null;
    
	public StudioBean() {
		getLog().info("!!!! Costruttore di StudioBean !!!");	
    }

	@PostConstruct
	public void init() {
		presetProductionCenters();
		getLog().info("INIT StudioBean!");

		lazyModelStudio = new LazyStudioDataModel(studioService);	
	}

	@PreDestroy
	public void destroy() {
		getLog().info("Destroy StudioBean!");

	}
	
	private void presetProductionCenters() {
		setProductionCenters(new ArrayList<SelectItem>());
		List<ProductionCenter> conts = productionCenterService.getProductionCenter();
		getProductionCenters().add(new SelectItem(null, " "));
		for (ProductionCenter cont : conts) {
			SelectItem xx = new SelectItem(cont.getId(), cont.getName()+" - "+cont.getDescription());
			getProductionCenters().add(xx);
		}
	}
	
	private Long studioToDelete = null;
	public void confirmDeleteStudio(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("studioId")) {
    			studioToDelete = ((Long)((UIParameter)com).getValue());
			}
		}
    	
	}
	
	public void cancelDelete(ActionEvent ev) {
	    	System.out.println("\nDelete Studio canceled.");
	    	studioToDelete= null;
	}
	
	public void delete(ActionEvent ev) {
    	System.out.println("\nDelete Test ...");
    	try {
        	
    		studioService.deleteStudio(studioToDelete);
//    		employees=null;
    		addInfoMessage(getMessagesBoundle().getString("studio.deleted"));
		} catch (Exception e) {
			e.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("studio.deletingError"), e);
		}
	}
	
	public void addStudio(ActionEvent e) {
		studioToEdit = new Studio();
		setSelectedProductionCenter(null);
	}
	
	public void modifyStudio(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("studioId")) {
    			studioToEdit = studioService.getStudioById((Long)((UIParameter)com).getValue());
    			setSelectedProductionCenter(studioToEdit.getProductionCenter().getId());
			}
		}
	}
	
	public void cancelEditStudio(ActionEvent e) {
		studioToEdit = null;
	}
	
	public void saveStudio(ActionEvent e) {
    	System.out.println("Salvo " + studioToEdit);
    	

    	if (studioToEdit.getName()==null || studioToEdit.getName().trim().equals("")) {
    		addWarningMessage("test su Nome richiesto!");
    		return;
    	}
    	
    	
    	try {
    		studioToEdit.setProductionCenter(productionCenterService.getProductionCenterById(getSelectedProductionCenter()));
    		studioToEdit = studioService.updateStudio(studioToEdit);
		
			addInfoMessage(getMessagesBoundle().getString("studio.saved"));
    	} catch (PersistenceException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();

    	} catch (DataIntegrityViolationException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();
    				
		} catch (Exception e1) {
			addErrorMessage(getMessagesBoundle().getString("studio.savingError"), e1);
			e1.printStackTrace();
		}
    	studioToEdit = null;
	}
	
	public void filterByDate() {
		System.out.println("Filter by date !");
		
		Calendar cal = Calendar.getInstance();
		if (filterFrom!=null) {
			cal.setTime(filterFrom);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 1);
			filterFrom = cal.getTime();
		}
		if (filterTo!=null) {
			cal.setTime(filterTo);
			cal.set(Calendar.HOUR_OF_DAY, 23);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);
			filterTo = cal.getTime();
		}
		
		((LazyStudioDataModel)lazyModelStudio).setFilterFrom(filterFrom);
		((LazyStudioDataModel)lazyModelStudio).setFilterTo(filterTo);
	}
	
	public Object getStudioById(long id) {
		return studioService.getStudioById(id);
	}

	public Studio getStudioToEdit() {
		return studioToEdit;
	}

	public void setStudioToEdit(Studio empToEdit) {
		this.studioToEdit = empToEdit;
	}

	public LazyDataModel<Studio> getLazyModelStudio() {
		return lazyModelStudio;
	}

	public Date getFilterFrom() {
		return filterFrom;
	}

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public Date getFilterTo() {
		return filterTo;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}

	public List<SelectItem> getProductionCenters() {
		return productionCenters;
	}

	public void setProductionCenters(List<SelectItem> productionCenters) {
		this.productionCenters = productionCenters;
	}

	public Long getSelectedProductionCenter() {
		return selectedProductionCenter;
	}

	public void setSelectedProductionCenter(Long selectedProductionCenter) {
		this.selectedProductionCenter = selectedProductionCenter;
	}

	public boolean haveUserRole(String roles) {
		
		String[] listRoles = roles.split(",");
		for (int i = 0; i < listRoles.length; i++) {
			listRoles[i] = listRoles[i].trim();
			
		}
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication!=null && authentication.getAuthorities()!=null) {
			//String currentPrincipalName = authentication.getName();
			for (GrantedAuthority r : authentication.getAuthorities()) {
				//System.out.println("Ruolo:"+r.getAuthority());
				for (int i = 0; i < listRoles.length; i++) {
					if (r.getAuthority()!=null && r.getAuthority().equals(listRoles[i])) {
						//System.out.println("trovato ruolo !!!");
						return true;
					}					
				}
			}
		}
		return false;
	}	
	
}
