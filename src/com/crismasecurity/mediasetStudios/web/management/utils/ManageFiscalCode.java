package com.crismasecurity.mediasetStudios.web.management.utils;

import java.io.Serializable;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.CountryCode;
import com.crismasecurity.mediasetStudios.core.services.CountryCodeService;
import com.crismasecurity.mediasetStudios.core.utils.Gender;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;

public class ManageFiscalCode extends BaseBean  implements Serializable {

	private static final long serialVersionUID = 6679090527531675824L;

	private CountryCodeService countryCodeService ;
	
	private String fiscalCode;
	private Date bornDate;
	private CountryCode countryCode;
	private Gender gender; 

	public ManageFiscalCode(String fiscalCode, CountryCodeService countryCodeService) {
		super();
		this.setFiscalCode(fiscalCode);
		this.setCountryCodeService(countryCodeService);
		extractData();
	}

	
	public ManageFiscalCode(CountryCodeService countryCodeService,String fiscalCode) {
		super();
		this.setFiscalCode(fiscalCode.toUpperCase().trim());
		this.setCountryCodeService(countryCodeService);
		extractData();
	}
	
	public ManageFiscalCode(CountryCodeService countryCodeService) {
		super();
		this.setCountryCodeService(countryCodeService);
	}
	
	public void calcuateFiscalCodeData(String fiscalCode) {
		this.setFiscalCode(fiscalCode.toUpperCase().trim());
		extractData();
	}
	
	
	private void clearProperties() {
			this.fiscalCode=null;
			this.bornDate=null;
			this.countryCode=null;
			this.gender=null;
	}
	
	/*
	private void extractData() {
		//SE(VALORE((STRINGA.ESTRAI(A1;10;2)))>40;"Femmina";"Maschio")
	
		String sDecode ="ABCDEHLMPRST";
		try {
			sex=Integer.parseInt(getFiscalCode().substring(9, 10))>40?SexType.FEMALE:SexType.MALE;	
		//DATA(STRINGA.ESTRAI(A1;7;2);TROVA(STRINGA.ESTRAI( A1;9;1);"ABCDEHLMPRST");RESTO(STRINGA.ESTRAI(A1;10;2);40))
			int year= Integer.parseInt(getFiscalCode().substring(6, 8));			
			int month=getFiscalCode().substring(8, 9).compareTo(sDecode);
			int day = Integer.parseInt(getFiscalCode().substring(9, 11)) % 40;
			this.bornDate=new GregorianCalendar(year, month, day).getTime();
			this.countryCode=getCountryCodeService().getCountryCodeFinder().and("countryCode").eq(getFiscalCode().substring(11, 15)).list().get(0);
		} catch (Exception e) {
			e.printStackTrace();
			clearProperties();
			addErrorMessage(getMessagesBoundle().getString("ManageFiscalCode"), e);
		}
	}
	
	*/
	
	
	private void extractData() {
		//SE(VALORE((STRINGA.ESTRAI(A1;10;2)))>40;"Femmina";"Maschio")
		Map<String, String> mm = new HashMap<String, String>();
		mm.put("A", "01");
		mm.put("B", "02");
		mm.put("C", "03");
		mm.put("D", "04");
		mm.put("E", "05");
		mm.put("H", "06");
		mm.put("L", "07");
		mm.put("M", "08");
		mm.put("P", "09");
		mm.put("R", "10");
		mm.put("S", "11");
		mm.put("T", "12");
		String sDecode ="ABCDEHLMPRST";
		try {
			
			// Get Date
			if (!((fiscalCode.length() == 16) || (fiscalCode.length() == 0))) throw new  Exception();
			String date = fiscalCode.substring(6);
			
			gender=Integer.parseInt(getFiscalCode().substring(9, 11))>40?Gender.F:Gender.M;	
		   //DATA(STRINGA.ESTRAI(A1;7;2);TROVA(STRINGA.ESTRAI( A1;9;1);"ABCDEHLMPRST");RESTO(STRINGA.ESTRAI(A1;10;2);40))
			
			int y = Integer.parseInt(date.substring(0, 2), 10);
			int year=   Integer.parseInt(((y < 9) ? "20" : "19") + String.format("%02d", y));
			//int month=getFiscalCode().substring(8, 9).compareTo(sDecode);
			int month = Integer.parseInt(mm.get(date.substring(2, 3).toUpperCase())) ;// date.substring(2, 1);
			int day = Integer.parseInt(getFiscalCode().substring(9, 11)) % 40;
			this.bornDate=new GregorianCalendar(year, month-1, day).getTime();
			try {
				this.countryCode=getCountryCodeService().getCountryCodeFinder().and("countryCode").eq(getFiscalCode().substring(11, 15)).list().get(0);				
			} catch (Exception e) {
				this.countryCode=getCountryCodeService().getCountryCodeFinder().and("countryCode").eq("-").list().get(0);				
			}
			
		} catch (Exception e) {
			//e.printStackTrace();
			clearProperties();
			addErrorMessage(getMessagesBoundle().getString("ManageFiscalCode"), e);
		}
	}
	
	
	
	
	public String getFiscalCode() {
		return fiscalCode;
	}

	public void setFiscalCode(String fiscalCode) {
		this.fiscalCode = fiscalCode;
	}

	public Date getBornDate() {
		return bornDate;
	}


	public CountryCode getCountryCode() {
		return countryCode;
	}

	public Gender getGender() {
		return gender;
	}


	private CountryCodeService getCountryCodeService() {
		return countryCodeService;
	}


	private void setCountryCodeService(CountryCodeService countryCodeService) {
		this.countryCodeService = countryCodeService;
	}


	
}
		