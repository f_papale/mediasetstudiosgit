package com.crismasecurity.mediasetStudios.web.management.utils;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import com.crismasecurity.mediasetStudios.core.entity.CountryCode;
import com.crismasecurity.mediasetStudios.web.management.SpectatorBean;

@FacesConverter("countryCodeConverter")
public class CountryCodeConverter implements Converter {

	public Object getAsObject(FacesContext fc, UIComponent component, String value) {
		System.out.println("init converter countryCodeConverter " + value);
        if(value != null && value.trim().length() > 0) {
            try {
            	SpectatorBean bean = (SpectatorBean) fc.getApplication().createValueBinding("#{spectatorBean}").getValue(fc);
            	return bean.getCountryCodeById((value!=null?Long.parseLong(value):null));
            } catch(NumberFormatException e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid countryCode."));
            }
        }
        else {
        	System.out.println("errore countryCodeConverter " + value);
            return null;
        }
	}

	public String getAsString(FacesContext fc, UIComponent component, Object obj) {
        if(obj != null) {
            return String.valueOf(((CountryCode) obj).getId());
        }
        else {
            return null;
        }
	}

}
