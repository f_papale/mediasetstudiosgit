package com.crismasecurity.mediasetStudios.web.management.utils;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import com.crismasecurity.mediasetStudios.core.entity.DistributionList;
import com.crismasecurity.mediasetStudios.web.management.EmailContactBean;

@FacesConverter("distributionListConverter")
public class DistributionListConverter implements Converter {

	public Object getAsObject(FacesContext fc, UIComponent component, String value) {
		System.out.println("init converter DistributionListConverter " + value);
        if(value != null && value.trim().length() > 0) {
            try {
            	@SuppressWarnings("deprecation")
				EmailContactBean bean = (EmailContactBean) fc.getApplication().createValueBinding("#{emailContactBean}").getValue(fc);
            	return bean.getDistributionListById((value!=null?Long.parseLong(value):null));
            } catch(NumberFormatException e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid Distribution List."));
            }
        }
        else {
        	System.out.println("errore DistributionList Converter " + value);
            return null;
        }
	}

	public String getAsString(FacesContext fc, UIComponent component, Object obj) {
		System.out.println("getAsString:"+ obj);
        if(obj != null) {
            return String.valueOf(((DistributionList) obj).getId());
        }
        else {
            return null;
        }
	}

}
