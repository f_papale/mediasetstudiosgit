package com.crismasecurity.mediasetStudios.web.management.utils;

import java.io.Serializable;
import java.util.Date;

import com.crismasecurity.mediasetStudios.core.entity.Episode;

public class CustomScheduleEvent implements Serializable{

	private static final long serialVersionUID = -1365185095446135330L;
	
	private long episodeid;
	private String productionName; 
	private long requestedSpectators;
	private long effectiveSpectators;
	private Date dateFrom;
	private Date dateTo;
	private String episodeStatus;
	private long statusid; 
	private String studioName;
	
	private Episode episode;
	
	public CustomScheduleEvent() {
		
	}
	
	public CustomScheduleEvent(long episodeid) {
		this.setEpisodeid(episodeid);
	}
	
	public CustomScheduleEvent(Episode episode) {
		try {
			this.setEpisode(episode);
			this.setProductionName(episode.getProduction().getName());
			this.setEpisodeid(episode.getId());
			if (episode.getEpisodeStatus()!=null) this.setStatusid(episode.getEpisodeStatus().getId());
			this.setRequestedSpectators(episode.getRequestedSpectators());
			this.setDateFrom(episode.getDateFrom());
			this.setDateTo(episode.getDateTo());
			if (episode.getEffectiveSpectators()!=null) this.setEffectiveSpectators(episode.getEffectiveSpectators());
			if (episode.getEpisodeStatus()!=null) this.setEpisodeStatus(episode.getEpisodeStatus().getDescription());
			try {
				this.setStudioName(episode.getStudio().getName() + (episode.getStudio().getSite().trim()!="" ? " - " + episode.getStudio().getSite() : ""));
			}catch (Exception e) { 
				this.setStudioName("Studio non impostato");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Errore creazione CustomScheduleEvent");
//			addErrorMessage(getMessagesBoundle().getString("AgencyUser.presetCurrentUser"), e);  
		}

	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dateFrom == null) ? 0 : dateFrom.hashCode());
		result = prime * result + ((dateTo == null) ? 0 : dateTo.hashCode());
		result = prime * result + (int) (effectiveSpectators ^ (effectiveSpectators >>> 32));
		result = prime * result + ((episode == null) ? 0 : episode.hashCode());
		result = prime * result + ((episodeStatus == null) ? 0 : episodeStatus.hashCode());
		result = prime * result + (int) (episodeid ^ (episodeid >>> 32));
		result = prime * result + ((productionName == null) ? 0 : productionName.hashCode());
		result = prime * result + (int) (requestedSpectators ^ (requestedSpectators >>> 32));
		result = prime * result + (int) (statusid ^ (statusid >>> 32));
		result = prime * result + ((studioName == null) ? 0 : studioName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CustomScheduleEvent other = (CustomScheduleEvent) obj;
		if (dateFrom == null) {
			if (other.dateFrom != null)
				return false;
		} else if (!dateFrom.equals(other.dateFrom))
			return false;
		if (dateTo == null) {
			if (other.dateTo != null)
				return false;
		} else if (!dateTo.equals(other.dateTo))
			return false;
		if (effectiveSpectators != other.effectiveSpectators)
			return false;
		if (episode == null) {
			if (other.episode != null)
				return false;
		} else if (!episode.equals(other.episode))
			return false;
		if (episodeStatus == null) {
			if (other.episodeStatus != null)
				return false;
		} else if (!episodeStatus.equals(other.episodeStatus))
			return false;
		if (episodeid != other.episodeid)
			return false;
		if (productionName == null) {
			if (other.productionName != null)
				return false;
		} else if (!productionName.equals(other.productionName))
			return false;
		if (requestedSpectators != other.requestedSpectators)
			return false;
		if (statusid != other.statusid)
			return false;
		if (studioName == null) {
			if (other.studioName != null)
				return false;
		} else if (!studioName.equals(other.studioName))
			return false;
		return true;
	}

	public long getEpisodeid() {
		return episodeid;
	}
	public void setEpisodeid(long episodeid) {
		this.episodeid = episodeid;
	}

	public long getRequestedSpectators() {
		return requestedSpectators;
	}

	public void setRequestedSpectators(long requestedSpectators) {
		this.requestedSpectators = requestedSpectators;
	}

	public long getEffectiveSpectators() {
		return effectiveSpectators;
	}

	public void setEffectiveSpectators(long effectiveSpectators) {
		this.effectiveSpectators = effectiveSpectators;
	}

	public Date getDateFrom() {
		return this.dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateTo() {
		return this.dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	public String getEpisodeStatus() {
		return episodeStatus;
	}

	public void setEpisodeStatus(String episodeStatus) {
		this.episodeStatus = episodeStatus;
	}


	public Episode getEpisode() {
		return episode;
	}

	public void setEpisode(Episode episode) {
		this.episode = episode;
	}

	public String getProductionName() {
		return productionName;
	}

	public void setProductionName(String productionName) {
		this.productionName = productionName;
	}

	public String getStudioName() {
		return studioName;
	}

	public void setStudioName(String studioName) {
		this.studioName = studioName;
	}

	public long getStatusid() {
		return statusid;
	}

	public void setStatusid(long statusid) {
		this.statusid = statusid;
	}
}
