package com.crismasecurity.mediasetStudios.web.management.utils;

import java.io.Serializable;

public class NodeFormatter implements Serializable, Comparable<NodeFormatter> {

	private static final long serialVersionUID = 8377213227465140846L;

	private Long id;
	
	private String season;
    
	private String name;
     
    private String dateEpisode; 
    
    private boolean visible=false;
    
   
     
    public NodeFormatter(Long id, String season, String name, String dateEpisode) {
    	this.id=id;
    	this.season = season;
    	this.name = name;
        this.dateEpisode = dateEpisode;
        this.visible=((id>=0)?true:false);
    }
    

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
    public String getName() {
        return name;
    }
 
    public void setName(String name) {
        this.name = name;
    }
 
    public String getSeason() {
        return season;
    }
 
    public void setSeason(String season) {
        this.season = season;
    }
 
    public String getDateEpisode() {
        return dateEpisode;
    }
 
    public void setDateEpisode(String dateEpisode) {
        this.dateEpisode = dateEpisode;
    }
 
    //Eclipse Generated hashCode and equals
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((season == null) ? 0 : season.hashCode());
        result = prime * result + ((dateEpisode == null) ? 0 : dateEpisode.hashCode());
        return result;
    }
 
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        NodeFormatter other = (NodeFormatter) obj;
        if (id== null) {
            if (other.id!= null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (season == null) {
            if (other.season != null)
                return false;
        } else if (!season.equals(other.season))
            return false;
        if (dateEpisode == null) {
            if (other.dateEpisode != null)
                return false;
        } else if (!dateEpisode.equals(other.dateEpisode))
            return false;
        return true;
    }
 
    @Override
    public String toString() {
        return name;
    }
 
    public int compareTo(NodeFormatter document) {
        return this.getName().compareTo(document.getName());
    }


	public boolean isVisible() {
		return visible;
	}


	public void setVisible(boolean visible) {
		this.visible = visible;
	}
}  