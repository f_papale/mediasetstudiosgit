package com.crismasecurity.mediasetStudios.web.management.utils;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import org.springframework.beans.factory.annotation.Autowired;

import com.crismasecurity.mediasetStudios.core.entity.Spectator;
import com.crismasecurity.mediasetStudios.core.services.SpectatorService;
import com.crismasecurity.mediasetStudios.web.management.SpectatorBean;


@FacesConverter("SpectatorIncidentConverter") 
public class SpectatorIncidentConverter implements Converter {
	
    @Autowired
    private SpectatorService spectatorService;

	public Object getAsObject(FacesContext fc, UIComponent component, String value) {
		System.out.println("init converter spectatorInitConverter " + value);
        if(value != null && value.trim().length() > 0) {
            try {
            	/*SpectatorEpisodeBean bean = (SpectatorEpisodeBean) fc.getApplication().createValueBinding("#{spectatorEpisodeBean}").getValue(fc);
            	return bean.getSpectatorById((value!=null?Long.parseLong(value):null));*/
            	SpectatorBean bean = (SpectatorBean) fc.getApplication().createValueBinding("#{spectatorBean}").getValue(fc);
            	return bean.getSpectatorById((value!=null?Long.parseLong(value):null));	
            } catch(NumberFormatException e) {
            	 throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid Spectator."));
            }
        }
        else {
        	System.out.println("errore spectatorConverter " + value);
            return null;
        }
	}

	public String getAsString(FacesContext fc, UIComponent component, Object obj) {
        if(obj != null) {
            return String.valueOf(((Spectator) obj).getId());
        }
        else {
            return null;
        }
	}

	public SpectatorService getSpectatorService() {
		return spectatorService;
	}

	public void setSpectatorService(SpectatorService spectatorService) {
		this.spectatorService = spectatorService;
	}

}
