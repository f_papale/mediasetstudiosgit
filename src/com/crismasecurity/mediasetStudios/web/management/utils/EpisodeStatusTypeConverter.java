package com.crismasecurity.mediasetStudios.web.management.utils;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import com.crismasecurity.mediasetStudios.core.entity.EpisodeStatusType;
import com.crismasecurity.mediasetStudios.web.management.SpectatorEpisodeBean;

@FacesConverter("episodeStatusTypeConverter")
public class EpisodeStatusTypeConverter implements Converter {

	public Object getAsObject(FacesContext fc, UIComponent component, String value) {
		System.out.println("init converter episodeStatusTypeConverter " + value);
        if(value != null && value.trim().length() > 0) {
            try {
            	SpectatorEpisodeBean bean = (SpectatorEpisodeBean) fc.getApplication().createValueBinding("#{spectatorEpisodeBean}").getValue(fc);
            	return bean.getEpisodeStatusTypeById((value!=null?Long.parseLong(value):null));
            } catch(NumberFormatException e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid EpisodeStatusType."));
            }
        }
        else {
        	System.out.println("errore episodeStatusTypeConverter " + value);
            return null;
        }
	}
	
	public String getAsString(FacesContext fc, UIComponent component, Object obj) {
        if(obj != null) {
            return String.valueOf(((EpisodeStatusType) obj).getId());
        }
        else {
            return null;
        }
	}
}
