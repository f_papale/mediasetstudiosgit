package com.crismasecurity.mediasetStudios.web.management.utils;

public enum EventType {
	OCCASIONAL, REPEATED
}
