package com.crismasecurity.mediasetStudios.web.management.utils;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import com.crismasecurity.mediasetStudios.core.entity.EmailContact;
import com.crismasecurity.mediasetStudios.web.management.DistributionListBean;

@FacesConverter("emailContactConverter")
public class EmailContactConverter implements Converter {

	public Object getAsObject(FacesContext fc, UIComponent component, String value) {
		System.out.println("init converter EmailContactConverter " + value);
        if(value != null && value.trim().length() > 0) {
            try {
            	@SuppressWarnings("deprecation")
				DistributionListBean bean = (DistributionListBean) fc.getApplication().createValueBinding("#{distributionListBean}").getValue(fc);
            	return bean.getEmailContactById((value!=null?Long.parseLong(value):null));
            } catch(NumberFormatException e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid Email Contact."));
            }
        }
        else {
        	System.out.println("errore EmailContactConverter " + value);
            return null;
        }
	}

	public String getAsString(FacesContext fc, UIComponent component, Object obj) {
		System.out.println("getAsString:"+ obj);
        if(obj != null) {
            return String.valueOf(((EmailContact) obj).getId());
        }
        else {
            return null;
        }
	}

}
