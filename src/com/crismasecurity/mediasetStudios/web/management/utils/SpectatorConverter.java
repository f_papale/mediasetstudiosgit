package com.crismasecurity.mediasetStudios.web.management.utils;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.springframework.beans.factory.annotation.Autowired;

import com.crismasecurity.mediasetStudios.core.entity.Spectator;
import com.crismasecurity.mediasetStudios.core.services.SpectatorService;
import com.crismasecurity.mediasetStudios.web.management.SpectatorEpisodeBean;


@FacesConverter("SpectatorConverter") 
public class SpectatorConverter implements Converter {
	
    @Autowired
    private SpectatorService spectatorService;

	public Object getAsObject(FacesContext fc, UIComponent component, String value) {
		System.out.println("init converter spectatorConverter " + value);
        if(value != null && value.trim().length() > 0) {
            try {
            	SpectatorEpisodeBean bean = (SpectatorEpisodeBean) fc.getApplication().createValueBinding("#{spectatorEpisodeBean}").getValue(fc);
            	return bean.getSpectatorById((value!=null?Long.parseLong(value):null));	
            } catch(NumberFormatException e) {
            	System.out.println("inizio blocco catch");
            	SpectatorEpisodeBean spEpBean = (SpectatorEpisodeBean) fc.getApplication().createValueBinding("#{spectatorEpisodeBean}").getValue(fc);
            	Spectator sp = new Spectator();
            	sp.setSurname(value);
            	spEpBean.setSpectatorToInsert(sp);
            	return sp;
            }
        }
        else {
        	System.out.println("errore spectatorConverter " + value);
            return null;
        }
	}

	public String getAsString(FacesContext fc, UIComponent component, Object obj) {
        if(obj != null) {
            return String.valueOf(((Spectator) obj).getId());
        }
        else {
            return null;
        }
	}

	public SpectatorService getSpectatorService() {
		return spectatorService;
	}

	public void setSpectatorService(SpectatorService spectatorService) {
		this.spectatorService = spectatorService;
	}

}
