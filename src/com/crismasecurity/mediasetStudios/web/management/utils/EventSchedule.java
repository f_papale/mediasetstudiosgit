package com.crismasecurity.mediasetStudios.web.management.utils;

import java.io.Serializable;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.annotation.PostConstruct;

import com.crismasecurity.mediasetStudios.core.entity.Episode;
import com.crismasecurity.mediasetStudios.core.entity.Production;
import com.crismasecurity.mediasetStudios.core.utils.DateUtils;


public class EventSchedule implements Serializable {

	private static final long serialVersionUID = -3335805061699292354L;

	static int for_ever=10000;
	
	private	Date dateStart = new Date();
	private	Date dateEnd = new Date();
	private Production production= null;
	private Episode episode = null; 
	private EventType eventType= EventType.OCCASIONAL;
	private FrequencyType frequencyType = FrequencyType.DAILY;
	private List<DayOfWeek> lstdayOfWeek=new ArrayList<DayOfWeek>();
	private String[] selectedWeekdays;
	private List<String> frequencyTypes;
	private List<String> weekdays;
	private DurationType durationType=DurationType.ALWAYS;
	private int everyStep=1;
	private Date upToaDate;
	private int eventsNumber;
	private List<Date> lstEvents= new ArrayList<Date>();
	private List<Episode> lstEpisodes = new ArrayList<Episode>();
	public EventSchedule() {
		init();
		this.setDateStart(new Date());
		this.setDateEnd(new Date());
		this.setEventType(EventType.OCCASIONAL);
		this.setFrequencyType(FrequencyType.DAILY);
		this.setDurationType(DurationType.ALWAYS);
		this.setUpToaDate(DateUtils.getMaxDate(new Date(),10));
		this.setEventsNumber(for_ever);
		
	}
	
	public EventSchedule(
			Date dateStart, 
			Date dateEnd,
			EventType eventType, 
			FrequencyType frequencyType, 
			List<DayOfWeek> lstdayOfWeek,
			DurationType durationType, 
			Date upToaDate, 
			int eventsNumber ) {
		init();
		this.setEpisode(null);
		this.setProduction(null);
		this.setDateStart(dateStart);
		this.setDateEnd(dateEnd);
		this.setEventType(eventType);
		this.setFrequencyType(frequencyType);
		this.setLstdayOfWeek(lstdayOfWeek);
		this.setDurationType(durationType);
		
		this.setUpToaDate(upToaDate);
		this.setEventsNumber(eventsNumber);
		
		this.lstEvents=getListDate();
	}

/*	
	public EventSchedule(
			Production production, 
			FrequencyType frequencyType, 
			List<DayOfWeek> lstdayOfWeek,
			DurationType durationType, 
			Date upToaDate, 
			int eventsNumber ) {
		
		init();
		this.setProduction(production);
		this.setDateStart(production.getStartDate());
		this.setUpToaDate(production.getEndDate());
		
		if (this.getProduction().getStartDate().compareTo(this.getProduction().getEndDate())==0) {
			EventScheduleOccasional(this.getDateStart());
		} else {
			EventScheduleRepeated(
					this.getDateStart(),
					frequencyType,
					lstdayOfWeek,
					durationType,
					this.getUpToaDate(),
					eventsNumber
					);			
		}
		this.lstEpisodes=makeLstEpisode(this.production);
	}

*/
	
	public EventSchedule(
			
			Episode episode, 
			FrequencyType frequencyType, 
			List<DayOfWeek> lstdayOfWeek,
			DurationType durationType, 
			Date upToaDate, 
			int eventsNumber,
			boolean bDoLstEpisode) {
		
		init();
		this.setEpisode(episode);
		this.setDateStart(episode.getDateFrom());
		this.setDateEnd(episode.getDateTo());
		this.setUpToaDate(upToaDate);
		
		
		if (this.getEpisode().getDateFrom().compareTo(this.getEpisode().getDateTo())==0) { // Da correggere 
			EventScheduleOccasional(this.getDateStart(), this.getDateEnd());
		} else {
			lstdayOfWeek.clear();
			lstdayOfWeek=getLstdayOfWeek(selectedWeekdays);
			EventScheduleRepeated(
					this.getDateStart(),
					this.getDateEnd(),
					frequencyType,
					lstdayOfWeek,
					durationType,
					this.getUpToaDate(),
					eventsNumber
					);			
		}
		if (bDoLstEpisode)
			this.lstEpisodes=makeLstEpisode(this.episode);
	}

	@PostConstruct
	public void init() {
		setWeekdays(new ArrayList<String>());
		for (DayOfWeek df: DayOfWeek.values() ) {
			getWeekdays().add (df.toString().toUpperCase());
		}
		setFrequencyTypes(new ArrayList<String>());
		for (FrequencyType ft: FrequencyType.values()) {
			getFrequencyTypes().add(ft.toString().toUpperCase());
		}
		
	}
	
	public void buildEventSchedule(boolean bDoLstEpisode) {
		
		this.setDateStart(episode.getDateFrom());
		this.setDateEnd(episode.getDateTo());
		
		if (eventType==EventType.OCCASIONAL) {
			EventScheduleOccasional(this.getDateStart(), this.getDateEnd());
		} else {	
			lstdayOfWeek.clear();
			lstdayOfWeek=getLstdayOfWeek(selectedWeekdays);
			EventScheduleRepeated(
					this.getDateStart(),
					this.getDateEnd(),
					frequencyType,
					lstdayOfWeek,
					durationType,
					this.getUpToaDate(),
					eventsNumber
					);			
		}
		if (bDoLstEpisode)
			this.lstEpisodes=makeLstEpisode(this.episode);
	}
	
	private List<DayOfWeek> getLstdayOfWeek(String[] selectedWeekdays) {
		List<DayOfWeek> lstDayOfWeek = new ArrayList<DayOfWeek>();
		if (selectedWeekdays!=null) {
			for (String selectedDay: selectedWeekdays) {
				lstDayOfWeek.add(DayOfWeek.valueOf(selectedDay ));
			} 
		}
		return lstDayOfWeek;
	}
	public void buildListEpisodes() {
		this.lstEvents=getListDate();
		//this.lstEpisodes=makeLstEpisode(this.production);
		this.lstEpisodes=makeLstEpisode(this.getEpisode());
	}
	
	public void EventScheduleOccasional(Date dateStart, Date dateEnd) {
		
		this.setDateStart(dateStart);
		this.setDateEnd(dateEnd);
		this.setEventType(EventType.OCCASIONAL);
		this.setFrequencyType(FrequencyType.DAILY);
		this.setDurationType(DurationType.ALWAYS);
		this.setUpToaDate(DateUtils.getMaxDate(new Date(),10));
		this.setEventsNumber(for_ever);
		
		this.lstEvents=getListDate();
	}
	
	public void EventScheduleRepeated(
			Date dateStart,
			Date dateEnd,
			FrequencyType frequencyType, 
			List<DayOfWeek> lstdayOfWeek,
			DurationType durationType, 
			Date upToaDate, 
			int eventsNumber 
			) {
		this.setDateStart(dateStart);
		this.setDateEnd(dateEnd);
		this.setEventType(EventType.REPEATED);
		this.setFrequencyType(frequencyType);
		this.setLstdayOfWeek(lstdayOfWeek);
		this.setDurationType(durationType);
		this.setUpToaDate(upToaDate);
		this.setEventsNumber(eventsNumber);
		
		// E' la chiamata che crea le puntate
		this.lstEvents=getListDate();
	}
	
/*	
	private Date getMaxDate(Date startDate, int howYears) {
		Calendar c = Calendar.getInstance();
		c.setTime(startDate);
		c.add(Calendar.YEAR, howYears);
		return c.getTime();
	}
	
	private DayOfWeek getDayOfWeek(Date currDate) {

		LocalDate localDate =  currDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		return localDate.getDayOfWeek();		
	}
	
	
	private Date addDays(Date date, int days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, days);
				
		return cal.getTime();
	}
	
	
	
	private Date addWeeks(Date date, int weeks) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.WEEK_OF_YEAR, weeks);
				
		return cal.getTime();
	}
	
	private Date addMonths(Date date, int months) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, months);
				
		return cal.getTime();
	}
	
	private Date addYears(Date date, int years) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.YEAR, years);
				
		return cal.getTime();
	}
*/	
	public DurationType getDurationType() {
		return durationType;
	}

	public void setDurationType(DurationType durationType) {
		this.durationType = durationType;
	}

	public Date getUpToaDate() {
		return upToaDate;
	}

	public void setUpToaDate(Date upToaDate) {
		this.upToaDate = upToaDate;
	}

	public int getEventsNumber() {
		return eventsNumber;
	}

	public void setEventsNumber(int eventsNumber) {
		this.eventsNumber = eventsNumber;
	}
	
	public EventType getEventType() {
		return eventType;
	}

	public void setEventType(EventType eventType) {
		this.eventType = eventType;
	}	

	public List<DayOfWeek> getLstdayOfWeek() {
		return lstdayOfWeek;
	}

	public void setLstdayOfWeek(List<DayOfWeek> lstdayOfWeek) {
		this.lstdayOfWeek = lstdayOfWeek;
	}

	public Date getDateStart() {
		return dateStart;
	}

	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}
	
	public FrequencyType getFrequencyType() {
		return frequencyType;
	}

	public void setFrequencyType(FrequencyType frequencyType) {
		this.frequencyType = frequencyType;
	}	
	
	public int getEveryStep() {
		return everyStep;
	}

	public void setEveryStep(int everyStep) {
		this.everyStep = everyStep;
	}
	
/*	
	private  Date addtimeToDate(Date dRef, int hour, int minute) {		
		Calendar cal = Calendar.getInstance();
		cal.setTime(dRef);
		cal.set(Calendar.HOUR_OF_DAY,hour);
		cal.set(Calendar.MINUTE,minute);
		return cal.getTime();
	}
	

	
	private int getDayDiff(Date date1, Date date2) {
		Calendar c1 = Calendar.getInstance();
		Calendar c2 = Calendar.getInstance();
		c1.setTime(date1);
		c2.setTime(date2);
		long days= date1.getTime()-date2.getTime();
		int iDays=(int)(TimeUnit.DAYS.convert(days, TimeUnit.MILLISECONDS));
		return iDays;
	}
*/
/*	
	private int getDayOfYear(Date dRef) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(dRef);
		return cal.get(Calendar.DAY_OF_YEAR);		
	}
	
	private int getHours(Date dRef) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(dRef);
		return cal.get(Calendar.HOUR_OF_DAY);		
	}
	
	private int getMinutes(Date dRef) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(dRef);
		return cal.get(Calendar.MINUTE);		
	}
*/	
	private List<Episode> makeLstEpisode(Episode templEpisode){
	
		List<Episode> lstEpisodes = new ArrayList<Episode>();
		Iterator<Date> iterator = this.lstEvents.iterator();
		while (iterator.hasNext()) {
			Episode episodeEdit = new Episode();
			Date selectedDate = iterator.next();
			
			int delta = DateUtils.getDayDiff(selectedDate, templEpisode.getDateFrom());
					
			episodeEdit.setDateFrom(DateUtils.addDays(templEpisode.getDateFrom(),delta));
			episodeEdit.setDateTo(DateUtils.addDays(templEpisode.getDateTo(),delta));
			
			episodeEdit.setEpisodeStatus(templEpisode.getEpisodeStatus());
			episodeEdit.setProduction(templEpisode.getProduction());
			episodeEdit.setRequestedSpectators(templEpisode.getRequestedSpectators());
			episodeEdit.setEffectiveSpectators(templEpisode.getEffectiveSpectators());
			episodeEdit.setStudio(templEpisode.getStudio());
			episodeEdit.setSpectators(null);
			
			lstEpisodes.add(episodeEdit);
			System.out.println(episodeEdit);
		}
		return lstEpisodes;
	}

/*
 * Questa procedura in base alla data DateStart crea tante occorrenze di sole date di spettacoli senza orari.
 */
	private List<Date> getListDate(){
		
		List<Date> lstResult = new ArrayList<Date>();
		switch (this.getEventType()) {
		//Se è occasionale aggiungo semplicemente la data iniziale
		case OCCASIONAL:
				lstResult.add(getDateStart());
			break;
		//Se è ripetitivo devo valutare la frequenza 	
		case REPEATED:
			switch (this.getFrequencyType()) {
			//Se la frequenza è giornaliera
			case DAILY:
				switch (this.getDurationType()) {
				case ALWAYS:					
					for (Date currDate = this.getDateStart(); currDate.before(DateUtils.getMaxDate(this.getDateStart(), 1)); currDate=DateUtils.addDays(currDate, this.everyStep)) {
						lstResult.add(currDate);
					}
					break;
				case FOR_SOME_EVENTS:
					int eventsCount=0;
					for (Date currDate = this.getDateStart(); eventsCount<=this.eventsNumber; currDate=DateUtils.addDays(currDate, this.everyStep)) {
						lstResult.add(currDate);
						eventsCount++;
					}
					break;
				case UP_TO_A_DATE:
					for (Date currDate = this.getDateStart(); currDate.before(DateUtils.addDays(this.upToaDate,1)); currDate=DateUtils.addDays(currDate, this.everyStep)) {
						lstResult.add(currDate);
					}
					break;
				default:
					break;
				}			
				break;
			case WEEKLY:
				switch (this.getDurationType()) {
				case ALWAYS:					
					for (Date currDate = this.getDateStart(); currDate.before(DateUtils.getMaxDate(this.getDateStart(), 1)); currDate=DateUtils.addDays(currDate, this.everyStep)) {
						if (this.lstdayOfWeek.contains(DateUtils.getDayOfWeek(currDate))) {
							lstResult.add(currDate);							
						}
					}
					break;
				case FOR_SOME_EVENTS:
					int eventsCount=0;
					for (Date currDate = this.getDateStart(); eventsCount<=this.eventsNumber; currDate=DateUtils.addDays(currDate, this.everyStep)) {
						if (this.lstdayOfWeek.contains(DateUtils.getDayOfWeek(currDate))) {
							lstResult.add(currDate);
							eventsCount++;
						}
					}
					break;
				case UP_TO_A_DATE:
					for (Date currDate = this.getDateStart(); currDate.before(this.upToaDate); currDate=DateUtils.addDays(currDate, this.everyStep)) {
						
						if (this.lstdayOfWeek.contains(DateUtils.getDayOfWeek(currDate))) {
							lstResult.add(currDate);
						}
					}
					break;
				default:
					break;
				}			
				break;
			case MONTLY:
				switch (this.getDurationType()) {
				case ALWAYS:					
					for (Date currDate = this.getDateStart(); currDate.before(DateUtils.getMaxDate(this.getDateStart(), 1)); currDate=DateUtils.addMonths(currDate, this.everyStep)) {
							lstResult.add(currDate);							
					}
					break;
				case FOR_SOME_EVENTS:
					int eventsCount=0;
					for (Date currDate = this.getDateStart(); eventsCount<=this.eventsNumber; currDate=DateUtils.addMonths(currDate, this.everyStep)) {
						lstResult.add(currDate);
						eventsCount++;
					}
					break;
				case UP_TO_A_DATE:
					for (Date currDate = this.getDateStart(); currDate.before(this.upToaDate); currDate=DateUtils.addMonths(currDate, this.everyStep)) {
						lstResult.add(currDate);
					}
					break;
				default:
					break;
				}			
				break;
			case ANNUAL:
				switch (this.getDurationType()) {
				case ALWAYS:					
					for (Date currDate = this.getDateStart(); currDate.before(DateUtils.getMaxDate(this.getDateStart(), 10)); currDate=DateUtils.addYears(currDate, this.everyStep)) {
							lstResult.add(currDate);							
					}
					break;
				case FOR_SOME_EVENTS:
					int eventsCount=0;
					for (Date currDate = this.getDateStart(); eventsCount<=this.eventsNumber; currDate=DateUtils.addYears(currDate, this.everyStep)) {
						lstResult.add(currDate);
						eventsCount++;
					}
					break;
				case UP_TO_A_DATE:
					for (Date currDate = this.getDateStart(); currDate.before(this.upToaDate); currDate=DateUtils.addYears(currDate, this.everyStep)) {
							lstResult.add(currDate);
					}
					break;
				default:
					break;
				}			
				break;
			}
			break;
		}
		return lstResult;
	}

	public List<Date> getLstEvents() {
		return lstEvents;
	}
	
	public void makeLstEvents() {
		this.lstEvents=getListDate();		
	}

	public Episode getEpisode() {
		return episode;
	}

	public void setEpisode(Episode episode) {
		this.episode = episode;
	}

	public List<Episode> getLstEpisodes() {
		return lstEpisodes;
	}

	public void setLstEpisodes(List<Episode> lstEpisodes) {
		this.lstEpisodes = lstEpisodes;
	}

	public Production getProduction() {
		return production;
	}

	public void setProduction(Production production) {
		this.production = production;
	}

	public String getEventTypeS() {
		return this.eventType.toString().toLowerCase();
	}

	public void setEventTypeS(String eventTypeS) {
		this.eventType=EventType.valueOf(eventTypeS.toUpperCase());
	}
	
	public String getFrequencyTypeS() {
		return this.eventType.toString().toLowerCase();
	}

	public void setFrequencyTypeS(String frequencyTypeS) {
		this.frequencyType=FrequencyType.valueOf(frequencyTypeS.toUpperCase());
	}

	public String getDurationTypeS() {
		return this.durationType.toString().toLowerCase();
	}

	public void setDurationTypeS(String durationTypeS) {
		this.durationType=DurationType.valueOf(durationTypeS.toUpperCase());
	}

	public String[] getSelectedWeekdays() {
		return selectedWeekdays;
	}

	public void setSelectedWeekdays(String[] selectedWeekdays) {
		this.selectedWeekdays = selectedWeekdays;
	}

	public List<String> getWeekdays() {
		return weekdays;
	}

	public void setWeekdays(List<String> weekdays) {
		this.weekdays = weekdays;
	}

	public List<String> getFrequencyTypes() {
		return frequencyTypes;
	}

	public void setFrequencyTypes(List<String> frequencyTypes) {
		this.frequencyTypes = frequencyTypes;
	}

	public Date getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
	}
}
