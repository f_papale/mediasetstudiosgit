package com.crismasecurity.mediasetStudios.web.management.utils;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.springframework.beans.factory.annotation.Autowired;

import com.crismasecurity.mediasetStudios.core.entity.BlackListSpectator;
import com.crismasecurity.mediasetStudios.core.entity.Spectator;
import com.crismasecurity.mediasetStudios.core.services.SpectatorService;
import com.crismasecurity.mediasetStudios.web.management.BlackListSpectatorBean;


@FacesConverter("blackListSpectatorConvert")
public class blackListSpectatorConverter implements Converter {
	
    @Autowired
    private SpectatorService spectatorService;

	public Object getAsObject(FacesContext fc, UIComponent component, String value) {
		System.out.println("init converter blackListSpectatorConverter " + value);
        if(value != null && value.trim().length() > 0) {
            try {
            	//SpectatorEpisodeBean bean = (SpectatorEpisodeBean) fc.getApplication().createValueBinding("#{spectatorEpisodeBean}").getValue(fc);
            	//return bean.getSpectatorById((value!=null?Long.parseLong(value):null));	
            	
            	BlackListSpectatorBean bean = (BlackListSpectatorBean)fc.getApplication().createValueBinding("#{blackListSpectatorBean}").getValue(fc);
            	
            	Spectator spectator = (Spectator)bean.getSpectatorById(Long.parseLong(value));
            	return spectator;
            	
            } catch(NumberFormatException e) {
            	System.out.println("inizio blocco catch");
            	
            	BlackListSpectatorBean bean = (BlackListSpectatorBean)fc.getApplication().createValueBinding("#{blackListSpectatorBean}").getValue(fc);
            	BlackListSpectator sp = new BlackListSpectator();
            	
            	//sp.setSurname(value);
            	//spEpBean.setSpectatorToInsert(sp);
            	return null;	
                //throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid Spectator."));
            }
        }
        else {
        	System.out.println("errore spectatorConverter " + value);
            return null;
        }
	}

	public String getAsString(FacesContext fc, UIComponent component, Object obj) {
        if(obj != null) {
            return String.valueOf(((Spectator) obj).getId());
        }
        else {
            return null;
        }
	}

	public SpectatorService getSpectatorService() {
		return spectatorService;
	}

	public void setSpectatorService(SpectatorService spectatorService) {
		this.spectatorService = spectatorService;
	}

}
