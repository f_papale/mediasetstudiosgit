package com.crismasecurity.mediasetStudios.web.management.utils;

public enum DurationType {
	ALWAYS, UP_TO_A_DATE, FOR_SOME_EVENTS
}

