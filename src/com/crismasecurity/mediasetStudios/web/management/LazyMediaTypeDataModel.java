package com.crismasecurity.mediasetStudios.web.management;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.crismasecurity.mediasetStudios.core.entity.MediaType;
import com.crismasecurity.mediasetStudios.core.services.MediaTypeService;
 

public class LazyMediaTypeDataModel extends LazyDataModel<MediaType> {



	/**
	 * 
	 */
	private static final long serialVersionUID = -5976160381306759510L;

	private MediaTypeService mediaTypeService;
	
	private Date filterFrom;
	private Date filterTo;

     
    public LazyMediaTypeDataModel(MediaTypeService mediaTypeService) {
        this.mediaTypeService = mediaTypeService;
    }
     
    @Override
    public MediaType getRowData(String rowKey) {
    	if (rowKey==null || rowKey.trim().equals("")) return null;
    	return mediaTypeService.getMediaTypeById(Long.parseLong(rowKey));
    }
 
    @Override
    public Object getRowKey(MediaType audit) {
        return audit.getId();
    }
 
    @Override
    public List<MediaType> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
    	
    	// Converto ordinamento
    	int sortInt =0; // UNSORTED
    	if (sortOrder.equals(SortOrder.ASCENDING)) sortInt = 1;
    	if (sortOrder.equals(SortOrder.DESCENDING)) sortInt = 2;
    	
 
		if (filterFrom!=null) filters.put("date#ge", filterFrom);
		if (filterTo!=null) filters.put("date#le", filterTo);

    	
    	List<MediaType> ret = mediaTypeService.getMediaTypeLazyLoading(first, pageSize, sortField, sortInt, filters);
    	this.setRowCount((int)mediaTypeService.getMediaTypeLazyLoadingCount(filters));
//    	System.out.println("Numero elementi trovati: " + this.getRowCount());

    	return ret;    
    }

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}


}