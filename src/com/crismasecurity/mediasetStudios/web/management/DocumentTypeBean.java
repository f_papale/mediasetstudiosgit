package com.crismasecurity.mediasetStudios.web.management;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.event.ActionEvent;
import javax.persistence.PersistenceException;

import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import com.crismasecurity.mediasetStudios.core.entity.DocumentType;
import com.crismasecurity.mediasetStudios.core.services.DocumentTypeService;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;

//Spring managed
@Controller (DocumentTypeBean.BEAN_NAME) 
//@Scope("session")
@Scope("view")
public class DocumentTypeBean extends BaseBean{


	private static final long serialVersionUID = -1011801046911462263L;

	public static final String BEAN_NAME = "documentTypeBean";
	
	public String getBeanName() { return BEAN_NAME; } 

	@Autowired
	private DocumentTypeService documentTypeService;
	
	private LazyDataModel<DocumentType> lazyModelDocumentType;
	private DocumentType documentTypeToEdit = null;
	private Long documentTypeToDelete = null;
	
	public DocumentTypeBean() {
		getLog().info("!!!! Costruttore di DocumentTypeBean !!!");
	}
	
	@PostConstruct
	public void init() {
		getLog().info("INIT DocumentTypeBean!");
		lazyModelDocumentType = new LazyDocumentTypeDataModel(documentTypeService);
	}
	
	@PreDestroy
	public void destroy() {
		getLog().info("Destroy DocumentTypeBean!");
	}
	
	
	public boolean haveUserRole(String roles) {
		
		String[] listRoles = roles.split(",");
		for (int i = 0; i < listRoles.length; i++) {
			listRoles[i] = listRoles[i].trim();
			
		}
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication!=null && authentication.getAuthorities()!=null) {
			//String currentPrincipalName = authentication.getName();
			for (GrantedAuthority r : authentication.getAuthorities()) {
				//System.out.println("Ruolo:"+r.getAuthority());
				for (int i = 0; i < listRoles.length; i++) {
					if (r.getAuthority()!=null && r.getAuthority().equals(listRoles[i])) {
						//System.out.println("trovato ruolo !!!");
						return true;
					}					
				}
			}
		}
		return false;
	}
	public void confirmDeleteDocumentType(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("documentTypeId")) {
    			documentTypeToDelete = ((Long)((UIParameter)com).getValue());
			}
		}    	
	}
	
	public void cancelDelete(ActionEvent ev) {
    	System.out.println("\nDelete DocumentType canceled.");
    	documentTypeToDelete= null;
	}
	
	public void delete(ActionEvent ev) {
    	System.out.println("\nDelete Test ...");
    	try {
    		documentTypeService.deleteDocumentType(documentTypeToDelete);
    		addInfoMessage(getMessagesBoundle().getString("documentType.deleted"));
		} catch (Exception e) {
			e.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("documentType.deletingError"), e);
		}
	}
	
	public void addDocumentType(ActionEvent e) {
		documentTypeToEdit = new DocumentType();
	}
	
	public void modifyDocumentType(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("documentTypeId")) {
    			documentTypeToEdit = documentTypeService.getDocumentTypeById((Long)((UIParameter)com).getValue());
			}
		}
	}
	
	public void cancelEditDocumentType(ActionEvent e) {
		documentTypeToEdit = null;
	}
	
	public void saveDocumentType(ActionEvent e) {
    	System.out.println("Salvo " + documentTypeToEdit);
    	

    	if (documentTypeToEdit.getDescription()==null || documentTypeToEdit.getDescription().trim().equals("")) {
    		addWarningMessage("test su Descrizione richiesto!");
    		return;
    	}
    	try {
    		documentTypeToEdit = documentTypeService.updateDocumentType(documentTypeToEdit);
			addInfoMessage(getMessagesBoundle().getString("documentType.saved"));
    	} catch (PersistenceException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();

    	} catch (DataIntegrityViolationException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();			
		} catch (Exception e1) {
			addErrorMessage(getMessagesBoundle().getString("documentType.savingError"), e1);
			e1.printStackTrace();
		}
    	documentTypeToEdit = null;
	}
	
	public Object getDocumentTypeById(long id) {
		return documentTypeService.getDocumentTypeById(id);
	}

	public DocumentType getDocumentTypeToEdit() {
		return documentTypeToEdit;
	}

	public void setDocumentTypeToEdit(DocumentType empToEdit) {
		this.documentTypeToEdit = empToEdit;
	}

	public LazyDataModel<DocumentType> getLazyModelDocumentType() {
		return lazyModelDocumentType;
	}
	
}
