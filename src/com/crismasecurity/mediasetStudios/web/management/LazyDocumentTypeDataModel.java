package com.crismasecurity.mediasetStudios.web.management;

import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.crismasecurity.mediasetStudios.core.entity.DocumentType;
import com.crismasecurity.mediasetStudios.core.services.DocumentTypeService;

public class LazyDocumentTypeDataModel extends LazyDataModel<DocumentType>{


	private static final long serialVersionUID = -9169434148811064017L;
	
	private DocumentTypeService documentTypeService;
	
	public LazyDocumentTypeDataModel(DocumentTypeService sptypeservice) {
		this.documentTypeService = sptypeservice;
	}
	
	@Override
	public DocumentType getRowData(String rowKey) {
		if (rowKey==null || rowKey.trim().equals("")) return null;
		return documentTypeService.getDocumentTypeById(Long.parseLong(rowKey));
	}
	
    @Override
    public Object getRowKey(DocumentType sptype) {
    	return sptype.getId();
    }
    
    @Override 
    public List<DocumentType> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters){
    	// Converto ordinamento
    	int sortInt = 0; // UNSORTED
    	if (sortOrder.equals(SortOrder.ASCENDING)) sortInt = 1;
    	if (sortOrder.equals(SortOrder.DESCENDING)) sortInt = 2;
    	
    	List<DocumentType> sptypelist = documentTypeService.getDocumentTypeLazyLoading(first, pageSize, sortField, sortInt, filters);
    	this.setRowCount((int)documentTypeService.getDocumentTypeLazyLoadingCount(filters));
//    	System.out.println("Numero elementi trovati: " + this.getRowCount());    	
    	return sptypelist;
    	
    }

}
