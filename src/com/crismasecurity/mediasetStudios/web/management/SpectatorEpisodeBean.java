package com.crismasecurity.mediasetStudios.web.management;


import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.Iterator;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.persistence.PersistenceException;
import javax.servlet.http.HttpSession;

import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.primefaces.push.EventBus;
import org.primefaces.push.EventBusFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import com.crismasecurity.mediasetStudios.core.entity.Agency;
import com.crismasecurity.mediasetStudios.core.entity.AgencySpectator;
import com.crismasecurity.mediasetStudios.core.entity.CountryCode;
import com.crismasecurity.mediasetStudios.core.entity.EmailQueue;
import com.crismasecurity.mediasetStudios.core.entity.AgencyUser;
import com.crismasecurity.mediasetStudios.core.entity.Audit;
import com.crismasecurity.mediasetStudios.core.entity.Episode;
import com.crismasecurity.mediasetStudios.core.entity.EpisodeStatusType;
import com.crismasecurity.mediasetStudios.core.entity.Production;
import com.crismasecurity.mediasetStudios.core.entity.Spectator;
import com.crismasecurity.mediasetStudios.core.entity.SpectatorEpisode;
import com.crismasecurity.mediasetStudios.core.entity.SpectatorType;
import com.crismasecurity.mediasetStudios.core.security.services.AppUserService;
import com.crismasecurity.mediasetStudios.core.security.vos.security.AppUserVO;
import com.crismasecurity.mediasetStudios.core.services.AgencyService;
import com.crismasecurity.mediasetStudios.core.services.AgencySpectatorService;
import com.crismasecurity.mediasetStudios.core.services.CountryCodeService;
import com.crismasecurity.mediasetStudios.core.services.EmailMakerService;
import com.crismasecurity.mediasetStudios.core.services.AgencyUserService;
import com.crismasecurity.mediasetStudios.core.services.AuditService;
import com.crismasecurity.mediasetStudios.core.services.BlackListSpectatorService;
import com.crismasecurity.mediasetStudios.core.services.EpisodeService;
import com.crismasecurity.mediasetStudios.core.services.EpisodeStatusTypeService;
import com.crismasecurity.mediasetStudios.core.services.ProductionService;
import com.crismasecurity.mediasetStudios.core.services.SpectatorEpisodeService;
import com.crismasecurity.mediasetStudios.core.services.SpectatorService;
import com.crismasecurity.mediasetStudios.core.services.SpectatorTypeService;
import com.crismasecurity.mediasetStudios.core.utils.Gender;
import com.crismasecurity.mediasetStudios.core.utils.PropertiesUtils;
import com.crismasecurity.mediasetStudios.core.utils.TextUtil;
import com.crismasecurity.mediasetStudios.core.utils.ObjectDiff.ObjectDiff;
import com.crismasecurity.mediasetStudios.core.utils.timers.InterfaceMailSent;
import com.crismasecurity.mediasetStudios.core.utils.timers.EngineStatusService;
import com.crismasecurity.mediasetStudios.core.utils.timers.IntefaceChangeStatus;
import com.crismasecurity.mediasetStudios.core.utils.timers.MailingQueueService;
import com.crismasecurity.mediasetStudios.web.LoginBean;
import com.crismasecurity.mediasetStudios.web.NavigationBean;
import com.crismasecurity.mediasetStudios.web.management.utils.CustomScheduleEvent;
import com.crismasecurity.mediasetStudios.web.management.utils.ManageFiscalCode;
import com.crismasecurity.mediasetStudios.web.utils.MakeHTML;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;
import com.widee.base.hibernate.criteria.Finder;

//Spring managed
@Controller (SpectatorEpisodeBean.BEAN_NAME) 
//@Scope("session")
@Scope("view")
public class SpectatorEpisodeBean extends BaseBean implements  InterfaceMailSent, IntefaceChangeStatus{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1531970192935689463L;
	public static final String BEAN_NAME = "spectatorEpisodeBean";
	public static String getBeanName() {
		return BEAN_NAME;
	}

	@Autowired
	private EmailMakerService emailMakerService;
	
	@Autowired
	private SpectatorEpisodeService spectatorEpisodeService;
	
    @Autowired
    private ProductionService productionService;

    @Autowired
    private EpisodeService episodeService;

    @Autowired
    private EpisodeStatusTypeService episodeStatusTypeService;
    
    @Autowired
    private SpectatorService spectatorService;
    
    @Autowired
    private SpectatorTypeService spectatorTypeService;
    
    @Autowired
    private AgencyService agencyService;    
    
    @Autowired
    private AgencySpectatorService agencySpectatorService;     
    
    @Autowired
    private CountryCodeService countryCodeService;
    
    @Autowired
    private AgencyUserService agencyUserService;
    
	@Autowired
    private AppUserService userService;
	
	@Autowired
	private BlackListSpectatorService blackListSpectatorService;
	
	@Autowired
	private LoginBean loginBean;
	
	@Autowired
	private AuditService auditService;
	
    @Autowired
    private NavigationBean navigationBean;
    
	
	private long spectatorEpisodeId=0;
	
	private boolean disabledDropDwnStatus=false; 
	private boolean checkIfSpectatorIsAddedToAnotherConcurrentEpisode = false;
	private LazyDataModel<SpectatorEpisode> lazyModelSpectatorEpisode;
	private SpectatorEpisode spectatorEpisodeToEdit = null;		

	private Long spectatorEpisodeToDelete = null;	
	private List<SelectItem> productions;
	private List<SelectItem> episodes;
	private List<SelectItem> agencies;
	private Long selectedProduction = null;
	private Long selectedEpisode = null;
	private String sessionId = null;
	private Long selectedAgency = null;
	private DefaultScheduleModel eventModel;
	private ScheduleEvent event = new DefaultScheduleEvent();
	private Date dateSelect = Calendar.getInstance().getTime();
	private Spectator spectatorToInsert = null;
	private Spectator spectatorToEdit = null;	
	private AgencyUser currentAgencyUser=null;
	private Episode episodeToEdit = null;
	private boolean spectatorNotAddedToEpisode;
	private List<SelectItem> spectatorTypes = null;
	private Long selectedSpectatorType = null;
	private String checkCfInserted;
	private String fisCodWarningStyle = null;
	private List<EpisodeStatusType> episodeStatusTypeList;
	private List<Spectator> spectatorsNotAddedList = null;
	private EpisodeStatusType episodeStatusSelected =null;
	private String descriptionAddEpisode;
	private String loggedUser;
	private String unwantedMessage; 
	
	private boolean onlyList=false;
	
	private ObjectDiff oDiff; 
	
	public SpectatorEpisodeBean() {
		getLog().info("!!!! Costruttore di SpectatorEpisodeBean !!!");
		setOnlyList(Boolean.parseBoolean(PropertiesUtils.getInstance().readProperty("mail.sendOnlyList","false")));
		setUnwantedMessage(PropertiesUtils.getInstance().readProperty("unwanted.message", getMessagesBoundle().getString("spectator_in_blocked_blacklinst")));
		checkIfSpectatorIsAddedToAnotherConcurrentEpisode = Boolean.parseBoolean(PropertiesUtils.getInstance().readProperty("list.check_spectator_in_concurrent_episode","false"));
	}
	
	@PostConstruct
	public void init() {
		getLog().info("INIT SpectatorEpisodeBean!");
		oDiff = new ObjectDiff();
		eventModel = new DefaultScheduleModel();
		presetCurrentUser();
		presetAgencyList();
		
		//presetProductionList();
		presetSpectatorTypeList();
		//lazyModelSpectatorEpisode = null;
		lazyModelSpectatorEpisode = new LazySpectatorEpisodeDataModel(spectatorEpisodeService);
		spectatorsNotAddedList = new ArrayList<Spectator>();	
		
		MailingQueueService.addListener(this);
		EngineStatusService.addListener(this);
		
		// Prendo la sessionId
		FacesContext fCtx = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) fCtx.getExternalContext().getSession(false);
		sessionId = session.getId();
		System.out.println("sessionId:" + sessionId);
	}
	
	
	@PreDestroy
	public void destroy() {
		MailingQueueService.removeListener(this);
		EngineStatusService.removeListener(this);
		getLog().info("Destroy SpectatorEpisodeBean!");
	}
	
	private void presetCurrentUser() {

		try {
			long userId = userService.getUserByLogin(loginBean.getUserId()).getId();
			currentAgencyUser= agencyUserService.getAgencyUserFinder().and("user.id").eq(userId).and("flgCurrentSelected").eq(true).result();	
			loggedUser = userService.getUserByLogin(loginBean.getUserId()).getFirstName() + " " + userService.getUserByLogin(loginBean.getUserId()).getLastName();
		} catch (Exception e) {
			try {
				long userId = userService.getUserByLogin(loginBean.getUserId()).getId();
				manageAgencyUser(userId);
			} catch (Exception e2) {
				e.printStackTrace();
				System.out.println("Errore Preset Current User");
				addErrorMessage(getMessagesBoundle().getString("spectatorEpisode.presetCurrentUser"), e);
				// TODO: handle exception
			}			
		} 
	}

	
	private void manageAgencyUser(long userId) {
		agencyUserService.presetAgencyUser(userId);
	}
	
	
	
	private void presetAgencyList() {
		agencies = new ArrayList<SelectItem>();
 		List<Agency> aglist = new ArrayList<Agency>();
		if(haveUserRole("ROLE_ADMIN") || haveUserRole("ROLE_SECURITY") || haveUserRole("ROLE_SUPERVISION")) {
			aglist = agencyService.getAgency();
		} else if(haveUserRole("ROLE_AGENCY") && haveUserRole("ROLE_PRODUCTION")) {			
			//Qui deve tirare fuori le agenzie che hanno assegnata una produzione il cui owner è l'utente di produzione loggato
			List<Production> p = productionService.getProductionFinder().and("owner.idAgency").eq(getIdLoggedAgency()).setDistinct().list();
//			Comparator<Production> productionBySeasonName = (Production p1, Production p2) -> p1.compareBySeasonName(p2);
//			Collections.sort(p, productionBySeasonName);
			Iterator<Production> i = p.iterator();
			while (i.hasNext()) {
				aglist.add(i.next().getAgency());
			}
			aglist.add(0,agencyService.getAgencyById(getIdLoggedAgency()));
			aglist= aglist.stream().distinct().collect(Collectors.toList());
			
		} else if (haveUserRole("ROLE_AGENCY")) {
			aglist = agencyService.getAgencyFinder().and("idAgency").eq(getIdLoggedAgency()).setDistinct().list();
		}
		agencies.clear();
		agencies.add(new SelectItem(null, " "));
		for (Agency ag : aglist) {
			if(ag!=null) {
				SelectItem xx = new SelectItem(ag.getIdAgency(), ag.getName());
				agencies.add(xx);
			}
		}
		
		// se l'utente loggato è un agenzia, impostiamo l'agenzia di default al caricamento della maschera
//		if(haveUserRole("ROLE_AGENCY") && ! haveUserRole("ROLE_PRODUCTION")) {
			selectedAgency = getIdLoggedAgency();
			getProductionListByAgencyId(selectedAgency);
//		}
	}
	
	
	private Long  getIdLoggedAgency() {
		if (currentAgencyUser==null || currentAgencyUser.getAgency()==null) return null;
		return Long.valueOf(currentAgencyUser.getAgency().getIdAgency());				
	}

	private void presetSpectatorTypeList(){
		spectatorTypes = new ArrayList<SelectItem>();
		List<SpectatorType> sptypelist = spectatorTypeService.getSpectatorType();
		spectatorTypes.add(new SelectItem(null, " "));
		for(SpectatorType sptype : sptypelist) {
			SelectItem xx = new SelectItem(sptype.getId(), sptype.getDescription());
			spectatorTypes.add(xx);
		}
	}	
	
	public void selectedSpectator(ActionEvent actionEvent) {
		System.out.println("selezionato lo spettatore " + spectatorToInsert);
	}
	
	public void addSpectatorButton(ActionEvent actionEvent){
		if (spectatorToInsert==null) spectatorToInsert = new Spectator();
	}

	
	
    public void addSpectator(ActionEvent actionEvent){
    	boolean flgInsertSpectator=false;
    	
    	System.out.println("Aggiungi lo spettatore " + spectatorToInsert.getSurname() + " " + spectatorToInsert.getName());
    	
    	
    	 String unwantedMessage =PropertiesUtils.getInstance().readProperty("unwanted.message",getMessagesBoundle().getString("spectatorEpisodeBean.unwanted"));
    	try {
    		// Se è un nuovo spettatore non ancora inserito nel DB
     		
    		Spectator oldSpect = spectatorService.getSpectatorById(spectatorToInsert.getId());
			String oldHtml = null;
			if (oldSpect!=null) {
				oldHtml = MakeHTML.makeHTML(oldSpect, "agencies", "documents", "episodes", "incidents", "documentFileDownload");
				oldSpect=Spectator.clone(oldSpect);
			}
//    		Valuto se esiste già uno spettatore con medesimo id (Nel caso sia tra quelli dell'Agenzia stessa)
    		if(spectatorService.getSpectatorById(spectatorToInsert.getId()) == null) { 
//    			Lo spettatore non esiste allora lo ricerco per Codice fiscale
    			if (spectatorToInsert.getFiscalCode()!=null && !spectatorToInsert.getFiscalCode().trim().equals("")) {
//    				Controllo se esiste un nominativo con codice fiscale uguale
    				List<Spectator> spsss = spectatorService.getSpectatorFinder().and("fiscalCode").eq(spectatorToInsert.getFiscalCode()).setDistinct().list();
//    				Se l'ho trovato assegno a spectatorToInsert il valore ricavato
    				if (spsss!=null && spsss.size()>0) {
//    					Già esistente, lo assegno all'agenzia corrente!
    					spectatorToInsert = spsss.get(0);
    				}
    				else {
//	    				Non esiste allora popoliamo la tabella Spettatore 
    		    		//impostiamo dal dialog il valore dello spectator type 
    					spectatorToInsert.setSpectatorType(spectatorTypeService.getSpectatorTypeById(selectedSpectatorType));
	    				spectatorToInsert = spectatorService.addSpectator(spectatorToInsert);
	    				String newHtml = MakeHTML.makeHTML(spectatorToInsert, "agencies", "documents", "episodes", "incidents", "documentFileDownload");
						AppUserVO uu = userService.getUserByLogin(navigationBean.getUserName());
						auditService.addAudit(new Audit(uu.getLastName() + " " + uu.getFirstName()+" ("+navigationBean.getUserName()+")", "Insert", "Spectator", spectatorToInsert.getId(), "", "", null, newHtml, oDiff.getDiff(null, Spectator.clone(spectatorToInsert))));
	    			}
//    			Se non riesco a trovarlo per codice fiscale allora lo ricerco per nome cognome e data di nascita	
    			}else {
    				// Controllo unicità nome, cognome, data nascita
    				Finder<Spectator> spfi = spectatorService.getSpectatorFinder();
    				if (spectatorToInsert.getName()!=null && !spectatorToInsert.getName().equals("")) spfi.and("name").eq(spectatorToInsert.getName());
    				if (spectatorToInsert.getSurname()!=null && !spectatorToInsert.getSurname().equals("")) spfi.and("surname").eq(spectatorToInsert.getSurname());
    				if (spectatorToInsert.getBirthDate()!=null) spfi.and("birthDate").eq(spectatorToInsert.getBirthDate());
    				List<Spectator> spsss = spfi.setDistinct().list();
//    				Se l'ho trovato assegno a spectatorToInsert il valore ricavato 
    				if (spsss!=null && spsss.size()>0) {
//    					Già esistente, lo assegno all'agenzia corrente!
    					spectatorToInsert = spsss.get(0);
    				}
//    				Se no riesco a trovarlo per nome, cognome, e data di nascita allora lo considero nuovo e l'aggiungo
    				else {
//	    				Non esiste allora popoliamo la tabella Spettatore
    		    		//impostiamo dal dialog il valore dello spectator type 
    					spectatorToInsert.setSpectatorType(spectatorTypeService.getSpectatorTypeById(selectedSpectatorType));    					
	    				spectatorToInsert = spectatorService.addSpectator(spectatorToInsert);
	    				
	    				String newHtml = MakeHTML.makeHTML(spectatorToInsert, "agencies", "documents", "episodes", "incidents", "documentFileDownload");
						AppUserVO uu = userService.getUserByLogin(navigationBean.getUserName());
						auditService.addAudit(new Audit(uu.getLastName() + " " + uu.getFirstName()+" ("+navigationBean.getUserName()+")", "Insert", "Spectator", spectatorToInsert.getId(), "", "", null, newHtml, oDiff.getDiff(null, Spectator.clone(spectatorToInsert))));
	    			}
    			}
    			
    			if (blackListSpectatorService.isNotInBlackList(
    					spectatorToInsert, 
    					null, 
    					null, 
    					currentAgencyUser,
    					loggedUser, 
    					false)) {
        			
//        			Associamo all'AgenziaSpettatore il nominativo dello spettatore nuovo o trovato  AgenziaSpettatore

	        			Long agId = (currentAgencyUser!=null && currentAgencyUser.getAgency()!=null && currentAgencyUser.getAgency().getIdAgency()!=null?currentAgencyUser.getAgency().getIdAgency():selectedAgency);
	        			Agency ag = agencyService.getAgencyById(agId); 
	        			AgencySpectator agspToAdd = new AgencySpectator(ag, spectatorToInsert);
	        			agspToAdd = agencySpectatorService.updateAgencySpectator(agspToAdd);
    					flgInsertSpectator=true;	        			
    			}


    		} else{    		
//        		Aggiorno i dati dello spettatore. Tuttavia se cambio nome devo valutare se questo non in black list altrimenti rischio di modificare
//    			quel nominativo.
    			spectatorToInsert=spectatorService.getSpectatorById(spectatorToInsert.getId());
    			
				if (blackListSpectatorService.isNotInBlackList(
    					spectatorToInsert, 
    					null, 
    					null, 
    					currentAgencyUser,
    					loggedUser, 
    					false)) {
					
					//Impostiamo dal dialog il valore dello spectator type 
					spectatorToInsert.setSpectatorType(spectatorTypeService.getSpectatorTypeById(selectedSpectatorType));
					spectatorToInsert = spectatorService.updateSpectator(spectatorToInsert);
					Spectator newSpect = Spectator.clone(spectatorToInsert);
	        		String newHtml = MakeHTML.makeHTML(spectatorToInsert, "agencies", "documents", "episodes", "incidents", "documentFileDownload");
					AppUserVO uu = userService.getUserByLogin(navigationBean.getUserName());
					auditService.addAudit(new Audit(uu.getLastName() + " " + uu.getFirstName()+" ("+navigationBean.getUserName()+")", "Update", "Spectator", spectatorToInsert.getId(), "", "", oldHtml, newHtml, oDiff.getDiff(oldSpect, newSpect)));
					oldSpect=null;
					newSpect=null;
					flgInsertSpectator=true;
				}
    		}

    		
    		if (flgInsertSpectator) {
//            	Associazione dello spettatore alla puntata
//    			Preparo spettatoreEpisodio 
            	SpectatorEpisode spEp = new SpectatorEpisode(
            			productionService.getProductionById(selectedProduction), 
            			episodeService.getEpisodeById(selectedEpisode), 
            			spectatorToInsert );
            	spEp.setDescription(descriptionAddEpisode);
//            	Valuto se lo spettatore non è stato già aggiunto all'episodio corrente
            	if(checkIfSpectatorNotAddedToCurrentEpisode()) {     		
//            		Valuto se lo spettatore non è già stato aggiunto ad un episodio contemporaneo
            		if (!spectatorAddedToAnotherConcurrentEpisode(spEp.getSpectator(),spEp.getEpisode().getId())) {
//            			Valuto se lo spettatore è in black list ed eventualmente invio la email
            			if (blackListSpectatorService.isNotInBlackList(
            					spectatorToInsert, 
            					selectedProduction, 
            					selectedEpisode, 
            					currentAgencyUser,
            					loggedUser,
            					true)) {
//    		        		Associo lo spettatore alla puntata
    		            	spectatorEpisodeService.addSpectatorEpisode(spEp);
    		            	
//    		            	Aggiorno SpettatorePuntata appena creato assegnando al GUID il valore dell'ID 
    		            	spEp.setGuid(Long.toString(spEp.getId()));
//    		            	Rendo Vip se è la produzione che sta aggiungendo lo spettatore
    		            	if (haveUserRole("ROLE_PRODUCTION")) spEp.setVip(true);
    		            	spectatorEpisodeService.updateSpectatorEpisode(spEp);
    		            	
//    		            	Controlliamo se questa puntata è nuova e cambiamo lo stato da "nuovo"  a "in lavorazione"
    		            	if(spEp.getEpisode().getEpisodeStatus().getIdStatus() == 1) {
    		            		Episode ep = spEp.getEpisode();
    		            		EpisodeStatusType epst = episodeStatusTypeService.getEpisodeStatusTypeFinder().and("idStatus").eq(2).result();
    		            		ep.setEpisodeStatus(epst);
    		            		ep = episodeService.updateEpisode(ep);         		
    		            	}
    		            	String newHtml = MakeHTML.makeHTML(spEp, "sign", "pdfSignBase64Str", "agencies", "documents", "episodes", "incidents", "documentFileDownload", "productionType", "agency", "owner", "managerEmail"
    		            			, "episodes", "productionEmployee", "logo", "production"
    		            			, "studio", "documentFileDownload", "spectators");
    						AppUserVO uu = userService.getUserByLogin(navigationBean.getUserName());
    						auditService.addAudit(new Audit(uu.getLastName() + " " + uu.getFirstName()+" ("+navigationBean.getUserName()+")", "Add Spectator in list", "SpectatorEpisode", spEp.getId(), "", "", null, newHtml, oDiff.getDiff(null, SpectatorEpisode.copy(spEp))));
    		            	addInfoMessage("Spettatore inserito correttamente");
    		        		// Valuto se lo spettatore è in blackList ed eventualmente invio la email
            			}else{
//            				spectatorToInsert.setNote("Lo spettatore non è stato inserito poichè in blacklist SEVERE.");
            				addErrorMessage(unwantedMessage);
            			}
    	        	}else {
    	        		addInfoMessage("Lo spettatore non è stato inserito poichè già invitato in un altro programma con medesimi orari.");
    	        	}
           		}
            	else {
            		addWarningMessage(getMessagesBoundle().getString("spectatorEpisode.alreadyAdded"));
            	}   			
    		} else {
    			addErrorMessage(unwantedMessage);
    		}

    	} catch(javax.persistence.PersistenceException e) {
			e.printStackTrace();
			System.out.println("Errore inserimento spettatore");
			addErrorMessage("Dato già presente a sistema. Impossibile inserire lo spettatore!");
    	}catch(Exception e) {
			e.printStackTrace();
			System.out.println("Errore inserimento spettatore");
			addErrorMessage(getMessagesBoundle().getString("spectatorEpisode.addError"), e);
    	}
    	checkCfInserted = null;
    	spectatorToInsert=null;
    }
    
    public void addSpecBtn(){
    	System.out.println("addSpecBtn");
    	if (spectatorToInsert==null) spectatorToInsert = new Spectator();
    	descriptionAddEpisode = "";
    }
    
    
    public void closeDlgSpc(ActionEvent actionEvent){
    	System.out.println("dialog add spectator chiuso ");
    	checkCfInserted = null;
    	spectatorToInsert=null;
    }
    
   
    public Long getRequestedSpectators() {
    	if (selectedEpisode==null || episodeService.getEpisodeById(selectedEpisode)==null) return 0l;
    	return episodeService.getEpisodeById(selectedEpisode).getRequestedSpectators();
    }
    public Long getEffectiveSpectators() {
    	Map<String,Object> filters = new HashMap<String,Object>();
        if (selectedEpisode != null) filters.put("episode.id", selectedEpisode);

        return spectatorEpisodeService.getSpectatorEpisodeLazyLoadingCount(filters);
    }
    
    public boolean checkIfSpectatorNotAddedToCurrentEpisode(){
    	try {
    		
        	if(spectatorEpisodeService.getSpectatorEpisodeFinder().and("spectator.id").eq(spectatorToInsert.getId()).and("episode.id").eq(selectedEpisode).setDistinct().list().isEmpty()) {
        		spectatorNotAddedToEpisode = true;
        		
        	}else {
        		spectatorNotAddedToEpisode = false;
        	}
//    		System.out.println("Prova ad aggiungere  lo spettatore" + spectatorToInsert.getSurname());
    	} catch(Exception e) {
    		System.out.println("errore open dialog");
    	}
    	return spectatorNotAddedToEpisode;
    }  
    
    public boolean spectatorAddedToEpisode(Spectator sp, Long idEpisode) {
    	boolean added = false;
    	try {
        	if(!spectatorEpisodeService.getSpectatorEpisodeFinder().and("spectator.id").eq(sp.getId()).and("episode.id").eq(idEpisode).setDistinct().list().isEmpty()) {
        		added = true;        		
        	}    		
    	}catch(Exception e) {}    	
    	return added;
    }
   
    private boolean spectatorAddedToAnotherConcurrentEpisode(Spectator sp, Long idEpisode) {
    	
    	boolean added = false;
    	if (checkIfSpectatorIsAddedToAnotherConcurrentEpisode) {
        	try {
        		Episode episode = episodeService.getEpisodeById(idEpisode);
        		Date startEpisode= episode.getDateFrom();
        		Date endEpisode= episode.getDateTo();
        		
        		List<Episode> spectatorEpisodes= episodeService.getEpisodeFinder()
        				.and("spectators.spectator").eq(sp)
        				.and("id").ne(idEpisode)
        				.setDistinct().list();
        		Set<Episode> concurentEpisodes = spectatorEpisodes.stream()
        				.map(s -> s)
        				.filter(s -> (
    							(startEpisode.before(s.getDateFrom()) && endEpisode.after(s.getDateFrom()))    								
        						||
        						(startEpisode.after(s.getDateFrom()) && startEpisode.before(s.getDateTo()))
        						||
        						(endEpisode.after(s.getDateFrom()) && endEpisode.before(s.getDateTo()))
        						))
        				.collect(Collectors.toSet());
        		added=!concurentEpisodes.isEmpty();
    		} catch (Exception e) {
    			e.printStackTrace();
    		}    		
    	}
    	return added;
		/*
		0	0	1	1	1	1	0	0		(startEpisode) (endEpisode)
		0	0	0	1	1	0	0	0	NO  (getDateFrom()) (getDateTo())
										
		0	0	1	1	1	1	0	0	
		0	1	1	0	0	0	0	0	No
										
		0	0	1	1	1	1	0	0	
		0	0	0	0	0	1	1	0	NO
										
		0	0	1	1	1	1	0	0	
		0	1	1	1	1	1	1	0	NO Ricade nella casistica sopra
										
		0	0	1	1	1	1	0	0	
		1	1	0	0	0	0	0	0	Si
										
		0	0	1	1	1	1	0	0	
		0	0	0	0	0	0	1	1	Si
    	
 */
    	
    }
 
  
    public void handleSelectSpectator(SelectEvent event) {
    	Spectator value = (Spectator) event.getObject();
    	SpectatorType sptype = value.getSpectatorType();
    	if (sptype!=null) selectedSpectatorType = sptype.getId();
    	else selectedSpectatorType=null;
    	
    	spectatorToInsert = value;
    	//spectatorToInsert.setSpectatorType(sptype); 
    	
    	descriptionAddEpisode = "";
    	System.out.println("selected "+ value.getName() + " " + value.getSurname());
    	}
    
    public void handleUnSelectSpectator(SelectEvent event) {
    	System.out.println("unselected autocomplete spectator");
    	selectedSpectatorType = null;
    	//spectatorToInsert.setSpectatorType(null);
    	spectatorToInsert = null;
    }
	
    public List<Spectator> completeSpectator(String query) {   	
	
    	// Il cognome è criptato quindi il like non funziona!!!!
    	getLog().info("inizio ricerca con " + query);
//    	List<Spectator> filteredSpectators = spectatorService.getSpectatorFinder().and("agencies.agency.idAgency").eq(selectedAgency).addOrderAsc("name").setDistinct().list();
//    	filteredSpectators.removeIf(spectator -> !spectator.getSurname().toLowerCase().contains(query.toLowerCase()));
//    	getLog().info("prima ricerca: " + (filteredSpectators!=null?filteredSpectators.size():"0"));
//    	filteredSpectators.addAll(spectatorService.getSpectatorFinder().and("agencies.agency.idAgency").eq(selectedAgency).and("name").like(query).addOrderAsc("name").setDistinct().list());
//    	getLog().info("seconda ricerca: " + (filteredSpectators!=null?filteredSpectators.size():"0"));   	
    	List<Spectator> filteredSpectators = spectatorService.getSpectatorFinder().and("agencies.agency.idAgency").eq(selectedAgency).and("fullName").like(query).addOrderAsc("fullName").setDistinct().list();
    	
    	return filteredSpectators;
    }
    
    public boolean spectatorToInsertInWarningBlackList() {
    	if (spectatorToInsert!=null) {    		
    		Long idWarn = new Long(1l);
    		String idWarning = PropertiesUtils.getInstance().readProperty("unwanted.type.warning.id", "1");
    		if (idWarning!=null && !idWarning.equals("")) idWarn = Long.parseLong(idWarning);
    		
    		return !blackListSpectatorService.getBlackListSpectatorFinder().and("spectator.id").eq(spectatorToInsert.getId()).and("blackListType.id").eq(idWarn).list().isEmpty();
    	}
    	else {
    		return false;
    	}
    }
    
    public boolean spectatorToInsertInBlockedBlackList() {
    	if (spectatorToInsert!=null) {
    		Long idBlock = new Long(2l);
    		String idBlocking = PropertiesUtils.getInstance().readProperty("unwanted.type.blocking.id", "2");
    		if (idBlocking!=null && !idBlocking.equals("")) idBlock = Long.parseLong(idBlocking);
    		
    		return !blackListSpectatorService.getBlackListSpectatorFinder().and("spectator.id").eq(spectatorToInsert.getId()).and("blackListType.id").eq(idBlock).list().isEmpty();
    	} else 
    		return false;
    }
 
    public List<CountryCode> completeCountryCode(String query) {     	
    	List<CountryCode> filteredCountriesCode = countryCodeService.getCountryCodeFinder().and("city").like(query).setDistinct().list();
        return filteredCountriesCode;
    }
    
    public void fiscalCodeInserted() {
    	try {
    		ManageFiscalCode mfc = new ManageFiscalCode(spectatorToInsert.getFiscalCode(), countryCodeService);
    		fisCodWarningStyle = "color:green";
    		checkCfInserted = "codice fiscale inserito valido" ;
    		spectatorToInsert.setBirthDate(mfc.getBornDate());
    		spectatorToInsert.setCity(mfc.getCountryCode());
    		spectatorToInsert.setGender(mfc.getGender());
    	} catch(Exception e) {
    		System.out.println("errore nel codice fiscale");
    		fisCodWarningStyle = "color:red";
        	checkCfInserted = "controlla il codice fiscale inserito " + spectatorToInsert.getFiscalCode() + " !!!";
    	}
    }
    
	
    public void getProductionListByAgencyId(Long agId) {
    	productions = new ArrayList<SelectItem>();
    	List<Production> prodlist = new ArrayList<Production>();
    	if (agId==getIdLoggedAgency()) {
    		prodlist = productionService.getProductionFinder().and("agency.idAgency").eq(agId).setDistinct().list();
    	}else {
    		if (agId!=null) {
    			prodlist = productionService.getProductionFinder().and("agency.idAgency").eq(agId).and("owner.idAgency").eq(getIdLoggedAgency()).setDistinct().list();	
    		} else {
    			prodlist = productionService.getProductionFinder().and("owner.idAgency").eq(getIdLoggedAgency()).setDistinct().list();
    		}
    	}
    	
    	viewCalendarAllProductions(prodlist);
    	Comparator<Production> compareBySeasonName  = (Production p1, Production p2) -> p1.compareBySeasonName(p2);
    	Collections.sort(prodlist, compareBySeasonName.reversed());	
    	
    	productions.clear();
    	productions.add(new SelectItem(null, " "));
    	for(Production prod : prodlist) {
    		
        	SelectItem xx = new SelectItem(prod.getId(), prod.getSeason() +" - "+ prod.getName().toUpperCase());
        	productions.add(xx);
        } 	
    }
    
	public void getEpisodesListByProductionId(Long prodId){
		episodes =  new ArrayList<SelectItem>();
		System.out.println("cerchiamo tutti gli episodi relativi alla produzione con ID "+ prodId);
		List<Episode> eps = episodeService.getEpisodeFinder().and("production.id").eq(prodId).addOrderAsc("dateFrom").setDistinct().list();
		System.out.println("lista ok");
		episodes.add(new SelectItem(null, " "));
		for (Episode ep : eps) {
			if(ep.getProduction().getId() == prodId) {
				SelectItem xx = new SelectItem(ep.getId(), ep.getVirtualName() + " " + ep.getDateFromOnlyDate());
				episodes.add(xx);	
			}
		}
	}
	
	public void viewCalendarProduction() {
		eventModel.clear();
		Production prod = productionService.getProductionById(selectedProduction);
		for (Episode epis : prod.getEpisodes()) {
			CustomScheduleEvent sce = new CustomScheduleEvent(epis);
			event = new DefaultScheduleEvent(epis.getProduction().getName(), epis.getDateFrom(), epis.getDateTo(), sce );
			String colorCls = "backgrdep_"+ epis.getEpisodeStatus().getIdStatus();
			((DefaultScheduleEvent)event).setStyleClass(colorCls);
			eventModel.addEvent(event);
		}		
	}
	
	public void viewCalendarAllProductions(List<Production> prodlist) {
		eventModel.clear();
		for(Production prod : prodlist) {
			for (Episode epis : prod.getEpisodes()) {
				CustomScheduleEvent sce = new CustomScheduleEvent(epis);
				event = new DefaultScheduleEvent(epis.getProduction().getName(), epis.getDateFrom(), epis.getDateTo(), sce );
				String colorCls = "backgrdep_"+ epis.getEpisodeStatus().getIdStatus();
				((DefaultScheduleEvent)event).setStyleClass(colorCls);
				eventModel.addEvent(event);
			}	
		}
	}
	
	
	public void backToCalendar(ActionEvent evt) {
		selectedEpisode = null;
		spectatorToInsert = null;
		System.out.println("torna al calendario");
		viewCalendarProduction();
	}
	
	public void epSelected(SelectEvent event) {
		try {
			System.out.println("Selezionata puntata :" + episodeService.getEpisodeById(selectedEpisode).getVirtualName());
			episodeToEdit = episodeService.getEpisodeById(selectedEpisode);
			if (episodeToEdit.getSpectators()!= null) episodeToEdit.getSpectators().size();
			
	        if((episodeToEdit.getEpisodeStatus().getIdStatus()==1)&&(episodeToEdit.getSpectators().size()==0)) {
	        	EpisodeStatusType newStatus = new EpisodeStatusType();
	        	newStatus = episodeStatusTypeService.getEpisodeStatusTypeByIdStatus(2);
	        	//episodeToEdit.getEpisodeStatus().setIdStatus(2);
	        	episodeToEdit.setEpisodeStatus(newStatus);
	        	try {
					episodeToEdit=episodeService.updateEpisode(episodeToEdit);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        }
			
	        
			((LazySpectatorEpisodeDataModel)lazyModelSpectatorEpisode).setEpisodeId(selectedEpisode);
			
			// in base allo stato dell'evento selezionato popola la lista degli stati della puntata
			loadStatusTypeList(episodeToEdit);
			
		} catch(Exception e) {
			if(selectedEpisode == null) {
				episodeToEdit = null;
				lazyModelSpectatorEpisode = null;
				System.out.println("nessuna  puntata selezionata");

			} else {
				System.out.println("Errore sulla puntata selezionata");
			}
		}
	}
	
	public synchronized void onEpisodeStatusSelected(SelectEvent event) {
		try {
			episodeStatusSelected = (EpisodeStatusType)event.getObject();
//			episodeStatusSelected=episodeStatusTypeService.getEpisodeStatusTypeById(episodeStatusSelected.getId());
			statusManagement(episodeStatusSelected);
			
		} catch(Exception e) {
			addErrorMessage("Errore aggiornamento stato della puntata");
		}
	}


	private void statusManagement(EpisodeStatusType episodeStatusToEvolve ) {
		try {
			episodeToEdit = episodeService.getEpisodeById(selectedEpisode);
			if (episodeToEdit.getEpisodeStatus().getIdStatus()!=episodeStatusToEvolve.getIdStatus()) {
				
				EpisodeStatusType oldEpStatus = episodeToEdit.getEpisodeStatus();
				String oldHtml = null;
				if (oldEpStatus!=null) {
					oldHtml = MakeHTML.makeHTML(oldEpStatus);
				}
				switch (episodeStatusToEvolve.getIdStatus()) {
				case 2: //Lista in lavorazione
					setDisabledDropDwnStatus(true);
//					refreshStatusDropDownList(episodeToEdit, episodeStatusToEvolve.getIdStatus());
					emailMakerService.purgeEmail(episodeToEdit);
					setDisabledDropDwnStatus(false);
//					refreshStatusDropDownList(episodeToEdit, episodeStatusToEvolve.getIdStatus());
					break;
				case 3: //Lista Completa
					setDisabledDropDwnStatus(true);
					break;
				case 4: //Lista Approvata
					setDisabledDropDwnStatus(false);
					break;
				case 7: //Crea sola Lista
					setDisabledDropDwnStatus(true);
//					refreshStatusDropDownList(episodeToEdit, episodeStatusToEvolve.getIdStatus());
					createEmails(true,2);
					if (!isOnlyList()) createEmails(false,3);
					break;
				case 6: //Crea Lista e Inviti
					setDisabledDropDwnStatus(true);
//					refreshStatusDropDownList(episodeToEdit, episodeStatusToEvolve.getIdStatus());
					createEmails(true,6);
					break;
				case 8: //Lista/Inviti Inviati
					
					break;
				case 5: //Evento Terminato
					emailMakerService.purgeEmail(episodeToEdit);
					break;				
				default:
					break;
				} 
				episodeToEdit.setEpisodeStatus(episodeStatusToEvolve);
				episodeToEdit = episodeService.updateEpisode(episodeToEdit);
				loadStatusTypeList(episodeToEdit);
				refreshStatusDropDownList(episodeToEdit, episodeStatusToEvolve.getIdStatus());
				String newHtml = MakeHTML.makeHTML(episodeStatusToEvolve);
				AppUserVO uu = userService.getUserByLogin(navigationBean.getUserName());
				auditService.addAudit(new Audit(uu.getLastName() + " " + uu.getFirstName()+" ("+navigationBean.getUserName()+")", "Episode Status Changed", "EpisodeStatusType", episodeStatusToEvolve.getId(), "", "", oldHtml, newHtml));  	
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			addErrorMessage("Errore aggiornamento stato della puntata");
		}
	}
	
	@Override
	public synchronized void onStatusChanged(int iStatus) {
		//TODO Sistemare la gestione dell'onChangeStatus....
		System.out.println("Processing status "+iStatus  );
		if (!((selectedEpisode==null)||(selectedEpisode==0)||(episodeStatusSelected==null))) {
			try {
				episodeToEdit=episodeService.getEpisodeById(selectedEpisode);
				setDisabledDropDwnStatus(false);
				refreshStatusDropDownList(episodeToEdit, iStatus);
			} catch (Exception e) {
				e.printStackTrace();
				addErrorMessage("Errore aggiornamento stato della puntata");
			}
		}
	}

	private synchronized void refreshStatusDropDownList(Episode ep, int iStatus) {
		loadStatusTypeList(ep);
		String status="{"+Integer.toString(iStatus)+"}";
		EventBus eventBus = EventBusFactory.getDefault().eventBus();
		System.out.println("refreshStatusDropDownList");
		eventBus.publish("/status/"+ sessionId, status);		
	}
	
	public synchronized void onEpisodeStatusChanged() {
		System.out.println("stato puntata cambiato "  );
		//System.out.println("stato puntata cambiato...stato = " + ept.getIdStatus());
		System.out.println("stato puntata cambiato id = " + episodeToEdit.getEpisodeStatus().getId());
	}

	public void prodSelected(SelectEvent event) {
		try {
			System.out.println("Selezionata :" + productionService.getProductionById(selectedProduction).getName());
			getEpisodesListByProductionId(selectedProduction);
			viewCalendarProduction();			
		} catch(Exception e) {
			if(selectedProduction == null) {
				System.out.println("nessuna  produzione selezionata");
			} else {
				addErrorMessage("Errore sulla produzione selezionata");
			}			
		}
	}
	
	public boolean documentIsAvailable() {
		Set<Integer> availableStatus =  new HashSet<Integer>();
		availableStatus.addAll(Arrays.asList(new Integer[] {6,7,8})); 
		return availableStatus.contains(episodeToEdit.getEpisodeStatus().getIdStatus());
	}
	
	public void agSelected(SelectEvent event) {
		System.out.println("Selezionata l agenzia :" + selectedAgency);
		getProductionListByAgencyId(selectedAgency);
	}	
	
    public synchronized void onEventSelect(SelectEvent selectEvent) {
        event = (ScheduleEvent) selectEvent.getObject();
        selectedEpisode = ((CustomScheduleEvent)event.getData()).getEpisode().getId();
        episodeToEdit = episodeService.getEpisodeById(selectedEpisode);
        if (episodeToEdit.getSpectators()!= null) episodeToEdit.getSpectators().size();
        
        if((episodeToEdit.getEpisodeStatus().getIdStatus()==1)&&(episodeToEdit.getSpectators().size()==0)) {
        	EpisodeStatusType newStatus = new EpisodeStatusType();
        	newStatus = episodeStatusTypeService.getEpisodeStatusTypeByIdStatus(2);
        	//episodeToEdit.getEpisodeStatus().setIdStatus(2);
        	episodeToEdit.setEpisodeStatus(newStatus);
        	try {
				episodeToEdit=episodeService.updateEpisode(episodeToEdit);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        
		//lazyModelSpectatorEpisode = new LazySpectatorEpisodeDataModel(spectatorEpisodeService);
		((LazySpectatorEpisodeDataModel)lazyModelSpectatorEpisode).setEpisodeId(selectedEpisode);
		
		// in base allo stato dell'evento selezionato popola la lista degli stati della puntata 
		loadStatusTypeList(episodeToEdit);
		
        // se selezioni un evento dal calendario senza filtrare la produzione a sinistra
        if(selectedProduction == null) {
        	selectedProduction = episodeService.getEpisodeById(selectedEpisode).getProduction().getId();
        	getEpisodesListByProductionId(selectedProduction);
       	  }
    }
    
    //idStatus  1:nuovo  2:lavorazione  3:completa  4:approvata 5:terminata 7:lista creata
    public void loadStatusTypeList(Episode ep) {

    	episodeStatusTypeList = episodeStatusTypeService.getEpisodeStatusTypeOrdered();
    	int idStatus=ep.getEpisodeStatus().getIdStatus();
    	switch(idStatus) {
	    	case 1:  //puntata nuova
//	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 1);
//	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 2);
	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 3);
	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 4);
	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 7);
	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 6);
	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 8);	    		
//	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 5);
	    		break;

	    	case 2:  //puntata in lavorazione
	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 1);
//	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 2);
//	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 3);
	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 4);
	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 7);
	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 6);
	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 8);	    		
//	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 5);
	    		break;

	    	case 3:  //puntata completa
	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 1);
//	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 2);
//	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 3);
//	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 4);
//	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 7);
//	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 6);
//	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 8);	    		
	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 5);
	    		break;
	    		
	    	case 4:  //puntata approvata
	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 1);
//	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 2);
	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 3);
//	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 4);
//	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 7);
//	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 6);
	    		if (isOnlyList()) episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 6);	    		
	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 8);	    		
//	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 5);
	    		break;
	    	case 5:  //puntata terminata
	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 1);
//	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 2);
	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 3);
	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 4);
	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 7);
	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 6);
	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 8);	    		
//	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 5);
	    		//checkConnectedUserRole(episodeStatusTypeList,5);
	    		break;
	    		
	    	case 7: //Lista Creata
	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 1);
//	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 2);	    		
	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 3);
	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 4);
//	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 7);
	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 6);
	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 8);	    		
//	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 5);
	    		break;
	    	
	    	case 6: //Lista e Inviti Creati
	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 1);
//	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 2);	    		
	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 3);
	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 4);
	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 7);
//	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 6);
	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 8);	    		
//	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 5);
	    		break;

	    	case 8: //Evento terminato
	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 1);
//	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 2);	    		
	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 3);
	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 4);
	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 7);
	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 6);
//	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 8);	    		
//	    		episodeStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 5);
	    		break;
	    	default:
	    	break;    		
    	}
    	checkConnectedUserRole(episodeStatusTypeList,idStatus);
    }
    
    public void checkConnectedUserRole(List<EpisodeStatusType> epStatusTypeList , Integer idStatus) {
    	switch(idStatus) {
    	
    		case 1: //puntata nuova
    			if (!haveUserRole("ROLE_AGENCY") && !haveUserRole("ROLE_ADMIN")) {
    				//epStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 2);
    				//epStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 3); 
    			}
    			break;
    		
    		case 2:  //puntata in lavorazione 
    			if (!haveUserRole("ROLE_AGENCY") && !haveUserRole("ROLE_ADMIN")) {
    				//epStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 3);
    			}
    			break;
    		
    		case 3:  //puntata completa
    			if (!haveUserRole("ROLE_PRODUCTION") && !haveUserRole("ROLE_ADMIN")) {
    				//epStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 2);
    				//epStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 4);     				
    			}
    			break;
    		
    		case 4:  //puntata approvata
    			if (!haveUserRole("ROLE_SUPERVISION") && !haveUserRole("ROLE_ADMIN")) {
    				//epStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 5);   
    			}
    			break;
    		
    		
    		case 5:   //puntata terminata
    			//epStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() != 5);   
    			break;  
    		
    		case 7:
    			if (!haveUserRole("ROLE_SUPERVISION") && !haveUserRole("ROLE_ADMIN")) {
    				epStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 5);   
    			}
    			break;
    			
    		case 6:
    			if (!haveUserRole("ROLE_SUPERVISION") && !haveUserRole("ROLE_ADMIN")) {
    				epStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 5);   
    			}
    			break;
    			
    		case 8:
    			if (!haveUserRole("ROLE_SUPERVISION") && !haveUserRole("ROLE_ADMIN")) {
    				epStatusTypeList.removeIf(epstatus -> epstatus.getIdStatus() == 5);   
    			}
    			break;
    		
    		default: 
    			if (!haveUserRole("ROLE_ADMIN") && !haveUserRole("ROLE_ADMIN")) {
    				epStatusTypeList.removeAll(epStatusTypeList);
    			}
    		break;	
    		
    	} 
    }
	
    
	public boolean haveUserRole(String roles) {
		
		String[] listRoles = roles.split(",");
		for (int i = 0; i < listRoles.length; i++) {
			listRoles[i] = listRoles[i].trim();
			
		}
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication!=null && authentication.getAuthorities()!=null) {
			//String currentPrincipalName = authentication.getName();
			for (GrantedAuthority r : authentication.getAuthorities()) {
				//System.out.println("Ruolo:"+r.getAuthority());
				for (int i = 0; i < listRoles.length; i++) {
					if (r.getAuthority()!=null && r.getAuthority().equals(listRoles[i])) {
						//System.out.println("trovato ruolo !!!");
						return true;
					}					
				}
			}
		}
		return false;
	}
	
	public void confirmDeleteSpectatorEpisode(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("spectatorEpisodeId")) {
    			spectatorEpisodeToDelete = ((Long)((UIParameter)com).getValue());
			}
		}    	
	}
	
	public void cancelDelete(ActionEvent ev) {
    	System.out.println("\nDelete SpectatorEpisode canceled.");
    	spectatorEpisodeToDelete= null;
	}
	

	
	public void delete(ActionEvent ev) {
    	System.out.println("\nDelete Test ...");
    	try {
    		spectatorEpisodeService.deleteSpectatorEpisode(spectatorEpisodeToDelete);
    		emailMakerService.purgeEmail(2, spectatorEpisodeToDelete);
    		emailMakerService.resendEmail(episodeToEdit,true);
    		
    		
//    		AppUserVO uu = userService.getUserByLogin(navigationBean.getUserName());
//			auditService.addAudit(new Audit(uu.getLastName() + " " + uu.getFirstName()+" ("+navigationBean.getUserName()+")", "Delete", "SpectatorEpisode", spectatorEpisodeToDelete, "", "", oldHtml, null));
//
//    		
    		
    		addInfoMessage(getMessagesBoundle().getString("spectatorEpisode.deleted"));
		} catch (Exception e) {
			e.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("spectatorEpisode.deletingError"), e);
		}
	}
	
	public void addSpectatorEpisode(ActionEvent e) {
		spectatorEpisodeToEdit = new SpectatorEpisode();
		descriptionAddEpisode = "";
	}
	
	public void modifySpectatorEpisode(ActionEvent e) {
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("spectatorEpisodeId")) {
    			spectatorEpisodeToEdit = spectatorEpisodeService.getSpectatorEpisodeById((Long)((UIParameter)com).getValue());
    			descriptionAddEpisode = (spectatorEpisodeToEdit!=null?spectatorEpisodeToEdit.getDescription():"");
    			spectatorToEdit = spectatorEpisodeToEdit.getSpectator();		
    			selectedSpectatorType = (spectatorToEdit!=null && spectatorToEdit.getSpectatorType()!=null?spectatorToEdit.getSpectatorType().getId():null);
    		}
		}
	}
	
	
	public boolean disableCommand() {
		boolean bret=false;
		Set<Integer> foo = new HashSet<Integer>();
		foo.addAll(Arrays.asList(new Integer[] {3,4,7,6,8,5})); 
		bret=foo.contains(episodeToEdit.getEpisodeStatus().getIdStatus());
		return bret;
	}
	
	public boolean renderCommand() {
	
		boolean bret=false;
		Set<Integer> foo = new HashSet<Integer>();
		foo.addAll(Arrays.asList(new Integer[] {1,2,8}));
		bret= (foo.contains(episodeToEdit.getEpisodeStatus().getIdStatus())); 
		return bret;
	}
	
	public void cancelEditSpectatorEpisode(ActionEvent e) {
		spectatorEpisodeToEdit = null;
		descriptionAddEpisode = "";
	}
	
	public void saveSpectatorEpisode(ActionEvent e) {
  	
    	try {
    		
    		SpectatorEpisode oldSpectEp = spectatorEpisodeService.getSpectatorEpisodeById(spectatorEpisodeToEdit.getId());
			String oldHtml = null;
			if (oldSpectEp!=null) {
				oldHtml = MakeHTML.makeHTML(oldSpectEp, "sign", "pdfSignBase64Str", "agencies", "documents", "episodes", "incidents", "documentFileDownload", "productionType", "agency", "owner", "managerEmail"
            			, "episodes", "productionEmployee", "logo", "production"
            			, "studio", "documentFileDownload", "spectators");
				oldSpectEp=SpectatorEpisode.copy(oldSpectEp);
			}
    		spectatorEpisodeToEdit.setDescription(descriptionAddEpisode);
    		if (selectedSpectatorType!=null) {
    			spectatorToEdit.setSpectatorType(spectatorTypeService.getSpectatorTypeById(selectedSpectatorType));
    		}
    		else spectatorToEdit.setSpectatorType(null);
    		spectatorEpisodeToEdit = spectatorEpisodeService.updateSpectatorEpisode(spectatorEpisodeToEdit);
    		spectatorToEdit = spectatorService.updateSpectator(spectatorToEdit);
    		
    		// aggiorniamo lo stato della puntata
    		Episode epToEdit  = spectatorEpisodeToEdit.getEpisode();
    		epToEdit.setEpisodeStatus(episodeStatusTypeService.getEpisodeStatusTypeByIdStatus(2));
    		epToEdit = episodeService.updateEpisode(epToEdit);
    		System.out.println("puntata con nome "+ epToEdit.getVirtualName()+" aggiornata da maschera spettatore puntata ");

    		String newHtml = MakeHTML.makeHTML(spectatorEpisodeToEdit, "sign", "pdfSignBase64Str", "agencies", "documents", "episodes", "incidents", "documentFileDownload", "productionType", "agency", "owner", "managerEmail"
        			, "episodes", "productionEmployee", "logo", "production"
        			, "studio", "documentFileDownload", "spectators");
			AppUserVO uu = userService.getUserByLogin(navigationBean.getUserName());
			auditService.addAudit(new Audit(uu.getLastName() + " " + uu.getFirstName()+" ("+navigationBean.getUserName()+")", "Update", "SpectatorEpisode", spectatorEpisodeToEdit.getId(), "", "", oldHtml, newHtml, oDiff.getDiff(oldSpectEp, SpectatorEpisode.copy(spectatorEpisodeToEdit))));
			addInfoMessage(getMessagesBoundle().getString("spectatorEpisode.saved"));
    	} catch (PersistenceException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();

    	} catch (DataIntegrityViolationException hibernateEx){
    		addErrorMessage(getMessagesBoundle().getString("duplicateRecord"));
    		hibernateEx.printStackTrace();
    				
		} catch (Exception e1) {
			addErrorMessage(getMessagesBoundle().getString("spectatorEpisode.savingError"), e1);
			e1.printStackTrace();
		}
    	spectatorEpisodeToEdit = null;
    	spectatorToEdit = null;
    	EngineStatusService.refershListener(this);
	}
	
	public boolean disableAddSpecBtn() {
		boolean res = false;
		try{
			Integer stato = episodeToEdit.getEpisodeStatus().getIdStatus();	
			
			Date datafinepuntata =  episodeToEdit.getDateTo();
			Date currentDate = new Date();
			
			// la puntata non è né nuova, né in lavorazione oppure se la data corrente è maggiore di quella di fine puntata
			if( (stato != 1 && stato != 2) || currentDate.compareTo(datafinepuntata) > 0 ) {				
				res = true;
			}
		}catch(Exception e) {
			System.out.println("controlla stato puntata" + e.toString());
		}
		return res;
	}
	
	public boolean disableEpStatusSelectMenu() {
		boolean dis = false;
		try {
			Date datafinepuntata =  episodeToEdit.getDateTo();
			Date currentDate = new Date();
			if( currentDate.compareTo(datafinepuntata) > 0) {
				dis = true ;
			}
		} catch(Exception e) {
			System.out.println("Controlla la puntata da modificare " + e.toString());
		}
		return dis || isDisabledDropDwnStatus();
	}
	

   
	public void handleExcelSpectatorsUpload(FileUploadEvent fuevt) {
		try {
			UploadedFile file = fuevt.getFile();
			if(file.equals(null)) { // file vuoto
				addWarningMessage(getMessagesBoundle().getString("spectator.fileNull"));				
			}
			else {
				System.out.println("il file appena caricato è " + file.getFileName());
				InputStream fileInputStream = file.getInputstream();				
				OPCPackage pkg = OPCPackage.open(fileInputStream);
				XSSFWorkbook workbook = new XSSFWorkbook(pkg);
				String episodeNote="";
				
				int numSpAdded = 0;
				// Get the first worksheet
				XSSFSheet sheet = workbook.getSheetAt(0);
				
				// Iterate through each rows
				Iterator<Row> rowIterator = sheet.iterator();
				
				while(rowIterator.hasNext()){
					 // Get Each Row
					Row row = rowIterator.next();
					System.out.println("SIAMO iterando la riga  " + row.getRowNum());
					if(row.getRowNum() != 0) {
						Iterator<Cell> cellIterator = row.cellIterator(); 
						Spectator newSpectator = new Spectator();
						try {
							
							 while (cellIterator.hasNext()) {
								 Cell cell = cellIterator.next();
								 int columnIndex = cell.getColumnIndex();
								 switch (columnIndex+1)
				                    {
				                    case 1:
				                    	System.out.println("nome = "+ cell.getStringCellValue().toUpperCase().trim()+ "\n");
//				                    	newSpectator.setName(cell.toUpperCase().trim());
				                    	newSpectator.setName(TextUtil.cleanTextContent(cell.getStringCellValue().toUpperCase().trim()));
				                        break;
				                    case 2:
				                    	System.out.println("cognome = "+ cell.getStringCellValue().toUpperCase().trim()+ "\n");
//				                    	newSpectator.setSurname(cell.toUpperCase().trim());
				                    	newSpectator.setSurname(TextUtil.cleanTextContent(cell.getStringCellValue().toUpperCase().trim()));
				                        break;
				                    case 3:
				                    	System.out.println("datadinascita = "+ cell.getDateCellValue()+ "\n");
				                    	newSpectator.setBirthDate(cell.getDateCellValue());
				                        break;
				                    case 4:
				                    	System.out.println("codice fiscale = "+ cell+ "\n");
				                    	newSpectator.setFiscalCode(cell.getStringCellValue().toUpperCase().trim());
				                        break;
									case 5:
										System.out.println("cellulare = " + cell.getNumericCellValue() + "\n");
										newSpectator.setMobile(String.format ("%.0f", cell.getNumericCellValue()));
										break;
									case 6:
										System.out.println("email = " + cell + "\n");
										newSpectator.setEmail(cell.getStringCellValue());
										break;
									case 7:
										System.out.println("sesso = " + cell + "\n");
										if (cell!=null && (cell.getStringCellValue().equalsIgnoreCase("M") || cell.getStringCellValue().equalsIgnoreCase("Male")))
											newSpectator.setGender(Gender.M);
										if (cell!=null && (cell.getStringCellValue().equalsIgnoreCase("F") || cell.getStringCellValue().equalsIgnoreCase("Female")))
											newSpectator.setGender(Gender.F);
										if (cell == null || cell.getStringCellValue().equalsIgnoreCase("Unknown"))
											newSpectator.setGender(Gender.UNKNOWN);
										break;	
									case 8:
										String msg = cell.getStringCellValue().length()>=10?cell.getStringCellValue().substring(10).trim()+" ...":cell.getStringCellValue();
										System.out.println("note = " + msg  + "\n");
										episodeNote = TextUtil.cleanTextContent(cell.getStringCellValue().toUpperCase().trim());
										break;
				                    }
							 }
							 
//							Controlliamo ora se esiste già uno spettatore con lo stesso codice fiscale nell'anagrafica degli spettatori
//							se questo spettatore esiste già nell anagrafica degli spettatori
							 
							 boolean checkIfExists = false;
							 
//							 Controllo se lo spettatore esiste già					 
							 if (!(newSpectator.getFiscalCode()==null || newSpectator.getFiscalCode().isEmpty())) {
								 checkIfExists = !(spectatorService.getSpectatorFinder().and("fiscalCode").eq(newSpectator.getFiscalCode()).list().isEmpty());
							 	 if (checkIfExists)
							 		 newSpectator = spectatorService.getSpectatorFinder().and("fiscalCode").eq(newSpectator.getFiscalCode()).result();
							 }else if (!(newSpectator.getSurname().isEmpty() || newSpectator.getName().isEmpty() || newSpectator.getBirthDate()==null)){
//								 Controllo per Nome e Cognome e Data di Nascita
								 Finder<Spectator> spfi = spectatorService.getSpectatorFinder();
				    			 spfi.and("name").eq(newSpectator.getName());
				    			 spfi.and("surname").eq(newSpectator.getSurname());
				    			 spfi.and("birthDate").eq(newSpectator.getBirthDate());
				    			 checkIfExists=(spfi.count()>0);
				    			 if (checkIfExists) 
				    				 newSpectator = spectatorService.getSpectatorFinder().and("name").eq(newSpectator.getName()).and("surname").eq(newSpectator.getSurname()).and("birthDate").eq(newSpectator.getBirthDate()).result();
							 }else if (!(newSpectator.getSurname().isEmpty() || newSpectator.getName().isEmpty())){ 
//								 Controllo per Nome e Cognome
								 Finder<Spectator> spfi = spectatorService.getSpectatorFinder();
				    			 spfi.and("name").eq(newSpectator.getName());
				    			 spfi.and("surname").eq(newSpectator.getSurname());
				    			 checkIfExists=(spfi.count()>0);
				    			 if (checkIfExists) 
				    				 newSpectator = spectatorService.getSpectatorFinder().and("name").eq(newSpectator.getName()).and("surname").eq(newSpectator.getSurname()).result();
							 } else {
								 checkIfExists=false;
							 }
							 
//							 Se il nominativo già esiste
							 if (checkIfExists) {
//								 Valuto se non è in blacklist ed eventualmente invio la email
								 if (blackListSpectatorService.isNotInBlackList(
										 newSpectator, 
										 selectedProduction, 
										 selectedEpisode, 
										 currentAgencyUser,
										 loggedUser,
										 true)) {
									
//									Verifichiamo se questo spettatore è gia associato a quest'agenzia;
//									se non è ancora stato associato a quest agenzia , lo associamo
									if(agencySpectatorService.getAgencySpectatorFinder().and("agency.idAgency").eq(selectedAgency).and("spectator.id").eq(newSpectator.getId()).list().isEmpty()) {
										AgencySpectator agsp = new AgencySpectator(agencyService.getAgencyById(selectedAgency),newSpectator);
										agsp = agencySpectatorService.addAgencySpectator(agsp);
										
										String newHtml = MakeHTML.makeHTML(agsp, "agencies", "documents", "episodes", "incidents", "documentFileDownload", "spectators");
										AppUserVO uu = userService.getUserByLogin(navigationBean.getUserName());
										auditService.addAudit(new Audit(uu.getLastName() + " " + uu.getFirstName()+" ("+navigationBean.getUserName()+")", "Massive load: Add Spectator to agency", "AgencySpectator", agsp.getId().getIdAgency(), "", "", null, newHtml));
									}							
//									Popoliamo la liste degli spettatori aggiunti.
									newSpectator.setNote("Spettatore già presente in anagrafica. E' stato comunque reso disponibile per l'Agenzia");
									spectatorsNotAddedList.add(newSpectator);
									
//									Associamo lo spettatore alla puntata 
									assignSpectatorToEpisode(newSpectator, selectedEpisode, episodeNote);
									numSpAdded++;
								 }else {
//									Popoliamo la liste degli spettatori non aggiunti perchè in BlackList SEVERE
									 newSpectator.setNote(getUnwantedMessage());
									spectatorsNotAddedList.add(newSpectator);	
								 }
//						      Se questo spettatore non esiste ancora nell'anagrafica degli spettatori allora 
//							  lo inserisce e lo associa agli spettatori dell'agenzia	
							} else {
//								Se almeno ho nome e cognome allora posso inserirlo
								if (!(newSpectator.getSurname()==null || newSpectator.getSurname().isEmpty() || newSpectator.getName()==null || newSpectator.getName().isEmpty())) {
//									Definisco un nuovo record di correlazione tra Agenzia e Spettatore
									List<AgencySpectator> aglist = new ArrayList<>();
									AgencySpectator agspnew = new AgencySpectator(agencyService.getAgencyById(selectedAgency), newSpectator);
									aglist.add(agspnew);
//									Associo allo spettatore l'Agenzia
									newSpectator.setAgencies(aglist);
									newSpectator.setSpectatorType(spectatorTypeService.getSpectatorTypeByIdSpectatorType(1));
//									Prova ad aggiornare codice fiscale e altri attributi rimanenti allo Spetattore
									try {
										ManageFiscalCode mfc = new ManageFiscalCode(newSpectator.getFiscalCode(), countryCodeService);
										newSpectator.setBirthDate(mfc.getBornDate());
										newSpectator.setCity(mfc.getCountryCode());
										newSpectator.setGender(mfc.getGender());
									} catch (Exception e) {
										System.out.println("Errore nel codice fiscale");
									}
//									Inserisce il nuovo spettatore
									newSpectator = spectatorService.addSpectator(newSpectator); 
									
									String newHtml = MakeHTML.makeHTML(newSpectator, "agencies", "documents", "episodes", "incidents", "documentFileDownload");
									AppUserVO uu = userService.getUserByLogin(navigationBean.getUserName());
									auditService.addAudit(new Audit(uu.getLastName() + " " + uu.getFirstName()+" ("+navigationBean.getUserName()+")", "Massive load: Add Spectator", "Spectator", newSpectator.getId(), "", "", null, newHtml, oDiff.getDiff(null, Spectator.clone(newSpectator))));
//									Aggiunge lo spettatore all'episodio corrente
					    			assignSpectatorToEpisode(newSpectator, selectedEpisode,episodeNote);
					    			numSpAdded++;
//					    		Se non ho nome e cognome non posso inserirlo	
								}else {
									newSpectator.setNote("Spettatore non aggiunto perchè in mancano o il Nome o il Cognome");
									spectatorsNotAddedList.add(newSpectator);	
								}
							}
						} catch(Exception e) {
							e.printStackTrace();
							System.out.println("errore dati sul file Excel - riga  " + row.getRowNum());
							newSpectator.setNote("Controlla la riga " + row.getRowNum() + " del file excel");
							spectatorsNotAddedList.add(newSpectator);							
						}
					}
				}
				addInfoMessage(getMessagesBoundle().getString("spectator.saved") + " \n Aggiunti " + numSpAdded +" spettatori");
			}
		} catch(Exception e) {
			addErrorMessage(getMessagesBoundle().getString("spectator.errorReadingFile") + e);
		}
	}
	
	public void assignSpectatorToEpisode(Spectator sp, Long idEpisode, String episodeNote) {
		try {
	    	SpectatorEpisode spEp = new SpectatorEpisode(productionService.getProductionById(selectedProduction), 
	    			episodeService.getEpisodeById(idEpisode), sp );
	    	
	    	if(!(spectatorAddedToEpisode(sp, idEpisode) || spectatorAddedToAnotherConcurrentEpisode(sp, idEpisode))) {

	    		//associamo lo spettatore alla puntata
	        	spectatorEpisodeService.addSpectatorEpisode(spEp);   
	        	
            	// aggiorniamo SpettatorePuntata appena creato assegnando al GUID il valore dell'ID 
            	spEp.setGuid(Long.toString(spEp.getId()));
            	// Aggiungiamo la Nota se trovata
            	spEp.setDescription(episodeNote);
            	spEp = spectatorEpisodeService.updateSpectatorEpisode(spEp);
            	String newHtml = MakeHTML.makeHTML(spEp, "sign", "pdfSignBase64Str", "agencies", "documents", "episodes", "incidents", "documentFileDownload", "productionType", "agency", "owner", "managerEmail"
            			, "episodes", "productionEmployee", "logo", "production"
            			, "studio", "documentFileDownload", "spectators");
				AppUserVO uu = userService.getUserByLogin(navigationBean.getUserName());
				auditService.addAudit(new Audit(uu.getLastName() + " " + uu.getFirstName()+" ("+navigationBean.getUserName()+")", "Add Spectator in list", "SpectatorEpisode", spEp.getId(), "", "", null, newHtml, oDiff.getDiff(null, SpectatorEpisode.copy(spEp))));

               	//controlliamo se questa puntata è nuova e cambiamo lo stato da "nuovo"  a "in lavorazione"
            	if(spEp.getEpisode().getEpisodeStatus().getIdStatus() == 1) {
            		Episode ep = spEp.getEpisode();
            		EpisodeStatusType epst = episodeStatusTypeService.getEpisodeStatusTypeFinder().and("idStatus").eq(2).result();
            		ep.setEpisodeStatus(epst);
            		ep = episodeService.updateEpisode(ep);         		
            	}
            	System.out.println("Spettatore con id "+ sp.getId() + "associato correttamente");  
            	
	    	} else {
	    		System.out.println("Spettatore con id " + sp.getId() + " già aggiunto alla puntata con id = " +idEpisode);
	    	}			
			
		} catch(Exception e){
			System.out.println("errore con associazione dello spettatore con id "+ sp.getId() + " alla puntata con id = "+ idEpisode );
		}
	}
	
	
	public EpisodeStatusType getEpisodeStatus(Long idEpStatus) {
		return episodeStatusTypeService.getEpisodeStatusTypeById(idEpStatus);
	}
	
	
	public Object getSpectatorById(long id) {
		return spectatorService.getSpectatorById(id);
	}	
	
	public Object getEpisodeById(long id) {
		return episodeService.getEpisodeById(id);
	}
	
	public SpectatorEpisodeService getSpectatorEpisodeService() {
		return spectatorEpisodeService;
	}

	public void setSpectatorEpisodeService(SpectatorEpisodeService spectatorEpisodeService) {
		this.spectatorEpisodeService = spectatorEpisodeService;
	}

	public SpectatorEpisode getSpectatorEpisodeToEdit() {
		return spectatorEpisodeToEdit;
	}

	public void setSpectatorEpisodeToEdit(SpectatorEpisode spectatorEpisodeToEdit) {
		this.spectatorEpisodeToEdit = spectatorEpisodeToEdit;
	}

	public LazyDataModel<SpectatorEpisode> getLazyModelSpectatorEpisode() {
		return lazyModelSpectatorEpisode;
	}

	public void setLazyModelSpectatorEpisode(LazyDataModel<SpectatorEpisode> lazyModelSpectatorEpisode) {
		this.lazyModelSpectatorEpisode = lazyModelSpectatorEpisode;
	}

	public List<SelectItem> getProductions() {
		return productions;
	}

	public void setProductions(List<SelectItem> productions) {
		this.productions = productions;
	}

	public Long getSelectedProduction() {
		return selectedProduction;
	}

	public void setSelectedProduction(Long selectedProduction) {
		this.selectedProduction = selectedProduction;
	}

	public Long getSelectedEpisode() {
		return selectedEpisode;
	}

	public void setSelectedEpisode(Long selectedEpisode) {
		this.selectedEpisode = selectedEpisode;
	}

	public List<SelectItem> getEpisodes() {
		return episodes;
	}

	public void setEpisodes(List<SelectItem> episodes) {
		this.episodes = episodes;
	}

	public Spectator getSpectatorToInsert() {
		return spectatorToInsert;
	}

	public void setSpectatorToInsert(Spectator spectatorToInsert) {
		this.spectatorToInsert = spectatorToInsert;
	}

	public Spectator getSpectatorToEdit() {
		return spectatorToEdit;
	}

	public void setSpectatorToEdit(Spectator spectatorToEdit) {
		this.spectatorToEdit = spectatorToEdit;
	}

	public List<SelectItem> getSpectatorTypes() {
		return spectatorTypes;
	}

	public void setSpectatorTypes(List<SelectItem> spectatorTypes) {
		this.spectatorTypes = spectatorTypes;
	}

	public Long getSelectedSpectatorType() {
		return selectedSpectatorType;
	}

	public void setSelectedSpectatorType(Long selectedSpectatorType) {
		this.selectedSpectatorType = selectedSpectatorType;
	}

	public boolean isSpectatorNotAddedToEpisode() {
		return spectatorNotAddedToEpisode;
	}

	public void setSpectatorNotAddedToEpisode(boolean spectatorNotAddedToEpisode) {
		this.spectatorNotAddedToEpisode = spectatorNotAddedToEpisode;
	}

	public Long getSelectedAgency() {
		return selectedAgency;
	}

	public void setSelectedAgency(Long selectedAgency) {
		this.selectedAgency = selectedAgency;
	}

	public List<SelectItem> getAgencies() {
		return agencies;
	}

	public void setAgencies(List<SelectItem> agencies) {
		this.agencies = agencies;
	}

	public DefaultScheduleModel getEventModel() {
		return eventModel;
	}

	public void setEventModel(DefaultScheduleModel eventModel) {
		this.eventModel = eventModel;
	}

	public ScheduleEvent getEvent() {
		return event;
	}

	public void setEvent(ScheduleEvent event) {
		this.event = event;
	}

	public Date getDateSelect() {
		return dateSelect;
	}

	public void setDateSelect(Date dateSelect) {
		this.dateSelect = dateSelect;
	}

	public EpisodeStatusTypeService getEpisodeStatusTypeService() {
		return episodeStatusTypeService;
	}

	public void setEpisodeStatusTypeService(EpisodeStatusTypeService episodeStatusTypeService) {
		this.episodeStatusTypeService = episodeStatusTypeService;
	}

	public Episode getEpisodeToEdit() {
		return episodeToEdit;
	}

	public void setEpisodeToEdit(Episode episodeToEdit) {
		this.episodeToEdit = episodeToEdit;
	}

	public List<EpisodeStatusType> getEpisodeStatusTypeList() {
		return episodeStatusTypeList;
	}

	public void setEpisodeStatusTypeList(List<EpisodeStatusType> episodeStatusTypeList) {
		this.episodeStatusTypeList = episodeStatusTypeList;
	}
	
	public EpisodeStatusType getEpisodeStatusTypeById(Long id) {
		return episodeStatusTypeService.getEpisodeStatusTypeById(id);
	}

	public AgencySpectatorService getAgencySpectatorService() {
		return agencySpectatorService;
	}

	public void setAgencySpectatorService(AgencySpectatorService agencySpectatorService) {
		this.agencySpectatorService = agencySpectatorService;
	}

	public String getCheckCfInserted() {
		return checkCfInserted;
	}

	public void setCheckCfInserted(String checkCfInserted) {
		this.checkCfInserted = checkCfInserted;
	}

	public String getFisCodWarningStyle() {
		return fisCodWarningStyle;
	}

	public void setFisCodWarningStyle(String fisCodWarningStyle) {
		this.fisCodWarningStyle = fisCodWarningStyle;
	} 
	
	
//    public Gender[] getGenders() {
//        return Gender.values();
//    }

	public List <Gender> getGenders() {
		List<Gender> lst = Arrays.asList(Gender.values());
		return lst;
	}
	
	public List<Spectator> getSpectatorsNotAdded() {
		return spectatorsNotAddedList;
	}

	public void setSpectatorsNotAdded(List<Spectator> spectatorsNotAdded) {
		this.spectatorsNotAddedList = spectatorsNotAdded;
	} 

	
	public StreamedContent pdfInviteList()  throws FileNotFoundException{		
		StreamedContent res=null;
		try {
			emailMakerService.setEpisode(episodeToEdit);
			res =emailMakerService.getPDFList();
			if (res.getName()==null) {
				throw new FileNotFoundException();
			}
		} catch (Exception e) {
			addErrorMessage(getMessagesBoundle().getString("spectatorEpisode.pdfInviteListError"),e);
		}
		return res;	
	}
	
	public void selectedEpisode(ActionEvent e) {
		HtmlCommandLink component = (HtmlCommandLink)e.getSource();
		for (UIComponent com : component.getChildren()) {
			if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("spectatorEpisodeId")) {
				spectatorEpisodeToEdit = spectatorEpisodeService.getSpectatorEpisodeById((Long)((UIParameter)com).getValue());
			}
		} 
	}

	public StreamedContent pdfInvite() throws FileNotFoundException  {
		
		StreamedContent res = new DefaultStreamedContent();
		try {
			SpectatorEpisode spectatorEpisodeToInvite = spectatorEpisodeService.getSpectatorEpisodeById(spectatorEpisodeId);
			res = emailMakerService.getPDFInvite(spectatorEpisodeToInvite);
			if (res.getName()==null) {
				throw new FileNotFoundException();
			}
		} catch (Exception e) {
			addErrorMessage(getMessagesBoundle().getString("spectatorEpisode.pdfInviteError"),e);
		}
		return res;
		//return null;
	}

	@Override
	public  synchronized void onMailSent(EmailQueue emailQueue) {
		setDisabledDropDwnStatus(false);
//		refreshStatusDropDownList(episodeToEdit, episodeToEdit.getEpisodeStatus().getIdStatus());
		System.out.println(emailQueue.toString());
	}
	
	public boolean base64InviteListNotAvailable() {
		return emailMakerService.base64InviteListNotAvailable(episodeToEdit);
	}
	
	public void resendList() {		
		emailMakerService.resendEmail(episodeToEdit);
		addInfoMessage(getMessagesBoundle().getString("spectatorEpisode.resendList"));
	}
	
	public void sendInvites() {

		emailMakerService.sendInvites(episodeToEdit);
		addInfoMessage(getMessagesBoundle().getString("spectatorEpisode.sendInvites"));
	}
	
	public void cancelSendInvites (ActionEvent ev) {
    	System.out.println("\ncancelSendInvites SpectatorEpisode canceled.");
//    	spectatorEpisodeToDelete= null;
	}
	
	public void sendInvite(ActionEvent e) {
		HtmlCommandLink component = (HtmlCommandLink)e.getSource();
		for (UIComponent com : component.getChildren()) {
			if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("spectatorEpisodeId")) {
				spectatorEpisodeToEdit = spectatorEpisodeService.getSpectatorEpisodeById((Long)((UIParameter)com).getValue());
				emailMakerService.sendInvite(spectatorEpisodeToEdit);
			}
		}
	}

	public void createEmails(boolean bMarkToSendMail,int selFunction) {
		
	    try {
	    	emailMakerService.createEmails(episodeToEdit, bMarkToSendMail, selFunction); 
	    	episodeToEdit=emailMakerService.getEpisode();
	    	loadStatusTypeList(episodeToEdit);
		} catch (Exception e2) {
			e2.printStackTrace();
			addErrorMessage(getMessagesBoundle().getString("spectatorEpisode.sendInvite") + e2);
		}  
	}

	
	public boolean isDisabledDropDwnStatus() {
		return disabledDropDwnStatus;
	}

	public void setDisabledDropDwnStatus(boolean disabledDropDwnStatus) {
		this.disabledDropDwnStatus = disabledDropDwnStatus;
	}

	public String getDescriptionAddEpisode() {
		return descriptionAddEpisode;
	}

	public void setDescriptionAddEpisode(String descriptionAddEpisode) {
		this.descriptionAddEpisode = descriptionAddEpisode;
	}

	public long getSpectatorEpisodeId() {
		return spectatorEpisodeId;
	}

	public void setSpectatorEpisodeId(long spectatorEpisodeId) {
		this.spectatorEpisodeId = spectatorEpisodeId;
	}

	public boolean isOnlyList() {
		return onlyList;
	}

	public void setOnlyList(boolean onlyList) {
		this.onlyList = onlyList;
	}

	public String getUnwantedMessage() {
		return unwantedMessage;
	}

	private void setUnwantedMessage(String unwantedMessage) {
		this.unwantedMessage = unwantedMessage;
	}


}
