package com.crismasecurity.mediasetStudios.web.utils;

import java.util.List;

import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;




/**
 * 
 * Class name: SortableList
 *
 * Description: The SortableList class is a utility class used by the sortable data table. 
 * 
 *
 * Company: 
 *
 * @author Giovanni
 * @date 03/set/2010
 *
 */
public abstract class SortableList<T> extends BaseBean {

    // Serial
	private static final long serialVersionUID = -6204633404178064603L;
	protected String sortColumnName;
    protected boolean ascending;

    // we only want to resort if the oder or column has changed.
    protected String oldSort;
    protected boolean oldAscending;


    protected SortableList(String defaultSortColumn) {
    	super();
        sortColumnName = defaultSortColumn;
        ascending = isDefaultAscending(defaultSortColumn);
        oldSort = sortColumnName;
        // make sure sortColumnName on first render
//        oldAscending = !ascending;
        oldAscending = ascending;
    }

    /**
     * Sort the list.
     */
    protected abstract void sort(List<T> list);

    /**
     * Is the default sortColumnName direction for the given column "ascending" ?
     */
    protected abstract boolean isDefaultAscending(String sortColumn);

    /**
     * Gets the sortColumnName column.
     *
     * @return column to sortColumnName
     */
    public String getSortColumnName() {
        return sortColumnName;
    }

    /**
     * Sets the sortColumnName column
     *
     * @param sortColumnName column to sortColumnName
     */
    public void setSortColumnName(String sortColumnName) {
        oldSort = this.sortColumnName;
        this.sortColumnName = sortColumnName;

    }

    /**
     * Is the sortColumnName ascending.
     *
     * @return true if the ascending sortColumnName otherwise false.
     */
    public boolean isAscending() {
        return ascending;
    }

    /**
     * Set sortColumnName type.
     *
     * @param ascending true for ascending sortColumnName, false for desending sortColumnName.
     */
    public void setAscending(boolean ascending) {
        oldAscending = this.ascending;
        this.ascending = ascending;
    }
    /*
    public int compareColumn(String value1, String value2){
    	String u1 = value1==null ? "" : value1;
    	String u2 = value2==null ? "" : value2;
    	return ascending ? u1.compareTo(u2) : u2.compareTo(u1);
    }*/
    
    public int compareColumn(Comparable value1, Comparable value2){
    	if (value1==null && value2==null) return 0;
    	if (value1==null && value2!=null) return ascending ? 1 : -1;
    	if (value1!=null && value2==null) return ascending ? -1 : 1;
		return ascending ? value1.compareTo(value2) : value2.compareTo(value1);
    }
}
