package com.crismasecurity.mediasetStudios.web.utils.jsf;

import java.io.Serializable;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.apache.logging.log4j.LogManager; 
import org.apache.logging.log4j.Logger;

import com.crismasecurity.mediasetStudios.web.main.NavigationTabsBean;



/**
 * 
 * Class name: BaseBean
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author Giovanni
 * @date 01/set/2010
 *
 */
public class BaseBean implements Serializable {


	private static final long serialVersionUID = -192801473997860440L;
    private Logger log;
    private static boolean JSF_POPUP_MESSAGES_ENABLED=false;
    

	public BaseBean() {
    	log = LogManager.getLogger(getClass()); 
    }


    /**
	 * Used to initialize the managed bean.
	 */
	protected void init() {

    }

	
	protected ResourceBundle getMessagesBoundle() {
		FacesContext context = FacesContext.getCurrentInstance();
	    return context.getApplication().evaluateExpressionGet(context, "#{msgs}", ResourceBundle.class);
	}


//    /**
//     * Utility method for building SelectItem[] from a series of
//     * localised entries in a MessageBundle. The SelectItem value
//     * is the MessageBundle key, and the label is the localised
//     * MessageBundle value.
//     *  
//     * @param prefix Beginning part of the MessageBundle key
//     * @param suffix Ending part of the MessageBundle key
//     * @param first First number of the series
//     * @param last Lat number of the series
//     */
//    protected static SelectItem[] buildSelectItemArray(
//            String prefix, String suffix, int first, int last) {
//        int num = last - first + 1;
//        SelectItem[] ret = new SelectItem[num];
//        for(int i = 0; i < num; i++) {
//            String key = prefix + Integer.toString(first+i) + suffix;
//            ret[i] = buildSelectItem(key);
//        }
//        return ret;
//    }
//    
//    protected static SelectItem buildSelectItem(String key) {
//        return new SelectItem(
//                key, /*MessageBundleLoader.getMessage(key)*/"fiffffff");
//    }
//    
//    /**
//     * Converts string arrays for displays.
//     *
//     * @param stringArray string array to convert
//     * @return a string concatenating the elements of the string array
//     */
//    protected static String convertToString(String[] stringArray) {
//        if (stringArray == null) {
//            return "";
//        }
//        StringBuffer itemBuffer = new StringBuffer();
//        for (int i = 0, max = stringArray.length; i < max; i++) {
//            if (i > 0) {
//                itemBuffer.append(" , ");
//            }
//            itemBuffer.append(/*MessageBundleLoader.getMessage(stringArray[i])*/"gigggggggg");
//        }
//        return itemBuffer.toString();
//    }
    
	protected void addErrorMessage(String message, Throwable e) {
		addErrorMessage(message, e, null);
	}
	protected void addErrorMessage(String message, String componentId) {
		addErrorMessage(message, null, componentId);
	}
    protected void addErrorMessage(String message) {
    	addErrorMessage(message, null, null);
//    	if (NavigationTabsBean.MESSAGES_DEBUG_LEVEL>-1)  FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, message));
    }
    protected void addErrorMessage(String message, Throwable e , String componentId) {
    	String error = null;
    	if (e!=null) {
    		Throwable ex = e;
    		while (ex.getCause()!=null) {
				ex=ex.getCause();
			}
    		error = " Error:"+ex.getMessage();
    	}
    	if (JSF_POPUP_MESSAGES_ENABLED) ((NavigationTabsBean)FacesUtils.getManagedBean("navigationTabsBean")).addErrorMessage(message);
    	if (error!=null && JSF_POPUP_MESSAGES_ENABLED) ((NavigationTabsBean)FacesUtils.getManagedBean("navigationTabsBean")).addErrorMessage(error);
    	
    	if (NavigationTabsBean.MESSAGES_DEBUG_LEVEL>-1) {
    		//FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, message));
    		//FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, error, error));
    		FacesContext.getCurrentInstance().addMessage(componentId, new FacesMessage(FacesMessage.SEVERITY_ERROR, message+" "+error, message+" - "+error));
    	}
    }
    protected void addWarningMessage(String message) {
    	addWarningMessage(message, null);
	}
    protected void addWarningMessage(String message, String componentId) {
    	if (JSF_POPUP_MESSAGES_ENABLED) ((NavigationTabsBean)FacesUtils.getManagedBean("navigationTabsBean")).addWarningMessage(message);
    	if (NavigationTabsBean.MESSAGES_DEBUG_LEVEL>0) FacesContext.getCurrentInstance().addMessage(componentId, new FacesMessage(FacesMessage.SEVERITY_WARN, message, message));
    }
    protected void addInfoMessage(String message) {
    	addInfoMessage(message, null);
	}
    protected void addInfoMessage(String message, String componentId) {
    	if(JSF_POPUP_MESSAGES_ENABLED) ((NavigationTabsBean)FacesUtils.getManagedBean("navigationTabsBean")).addInfoMessage(message);
    	if (NavigationTabsBean.MESSAGES_DEBUG_LEVEL>1) FacesContext.getCurrentInstance().addMessage(componentId, new FacesMessage(FacesMessage.SEVERITY_INFO, message, message));
    }
    protected void addDebugMessage(String message) {
    	addDebugMessage(message, null);
	}
    protected void addDebugMessage(String message, String componentId) {
    	if(JSF_POPUP_MESSAGES_ENABLED) ((NavigationTabsBean)FacesUtils.getManagedBean("navigationTabsBean")).addDebugMessage(message);
    	if (NavigationTabsBean.MESSAGES_DEBUG_LEVEL>-2) FacesContext.getCurrentInstance().addMessage(componentId, new FacesMessage(FacesMessage.SEVERITY_INFO, message, message));
    }
    
    protected Logger getLog() {
		return log;
	}
}
