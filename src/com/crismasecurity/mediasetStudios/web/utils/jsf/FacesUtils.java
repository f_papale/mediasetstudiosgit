package com.crismasecurity.mediasetStudios.web.utils.jsf;

import java.util.Iterator;
import java.util.Map;
import java.util.ResourceBundle;

import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.application.ApplicationFactory;
import javax.faces.application.FacesMessage;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIViewRoot;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.ValueBinding;
import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.crismasecurity.mediasetStudios.web.main.NavigationTabsBean;




/**
 * JSF utilities.
 */
@SuppressWarnings("deprecation")
public class FacesUtils {
    /**
     * Get servlet context.
     *
     * @return the servlet context
     */
    public static ServletContext getServletContext() {
        return (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
    }

    public static ExternalContext getExternalContext() {
        FacesContext fc = FacesContext.getCurrentInstance();
        return fc.getExternalContext();
    }

    public static HttpSession getHttpSession(boolean create) {
        return (HttpSession) FacesContext.getCurrentInstance().
                getExternalContext().getSession(create);
    }

    /**
     * Get managed bean based on the bean name.
     *
     * @param beanName the bean name
     * @return the managed bean associated with the bean name
     */
    public static Object getManagedBean(String beanName) {

        return getValueBinding(getJsfEl(beanName)).getValue(FacesContext.getCurrentInstance());
    }

    /**
     * Remove the managed bean based on the bean name.
     *
     * @param beanName the bean name of the managed bean to be removed
     */
    public static void resetManagedBean(String beanName) {
    	//System.out.println("resetManagedBean: "+beanName);
    	//TODO Verificare perch� non funziona il reset con Spring
    	try{
    		getValueBinding(getJsfEl(beanName)).setValue(FacesContext.getCurrentInstance(), null);
    	} catch (Exception e) {
    		//System.err.println("e="+e.getMessage());
    		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove(beanName);
		}
    }

    /**
     * Store the managed bean inside the session scope.
     *
     * @param beanName    the name of the managed bean to be stored
     * @param managedBean the managed bean to be stored
     */
    public static void setManagedBeanInSession(String beanName, Object managedBean) {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(beanName, managedBean);
    }

    /**
     * Get parameter value from request scope.
     *
     * @param name the name of the parameter
     * @return the parameter value
     */
    public static String getRequestParameter(String name) {
        return (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(name);
    }
    
    
    /**
     * Get parameter value from request scope.
     *
     * @param name the name of the parameter
     * @param classe del tipo di ritorno atteso
     * @param defaultValue valore di ritorno di default
     * @return the parameter value
     */
    public static <T> T getRequestParameter(String name, Class<T> classe, T defaultValue) {
    	String param = getRequestParameter(name);
    	if (param==null || param.trim().equals("")) return defaultValue;
    	try {
    		if (classe.equals(Long.class) ) {
    			Long v = Long.parseLong(param);
    			return (T)v;
    		} else if (classe.equals(Integer.class)) {
    			Integer v = Integer.parseInt(param);
    			return (T) v;
    		} else if (classe.equals(Double.class)) {
    			Double v = Double.parseDouble(param);
    			return (T) v;
    		} else if (classe.equals(Float.class)) {
    			Float v = Float.parseFloat(param);
    			return (T) v;
    		}
    	} catch (NumberFormatException e) {
			return defaultValue;
		}
    	
    	try{
    		return (T) param;
    	} catch (ClassCastException e) {
		}
        return defaultValue;
    }

    /**
     * Add information message.
     *
     * @param msg the information message
     */
    public static void addInfoMessage(String msg) {
        addInfoMessage(null, msg);
    }

    /**
     * Add information message to a specific client.
     *
     * @param clientId the client id
     * @param msg      the information message
     */
    public static void addInfoMessage(String clientId, String msg) {
        FacesContext.getCurrentInstance().addMessage(clientId, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, msg));
    }

    /**
     * Add error message.
     *
     * @param msg the error message
     */
    public static void addErrorMessage(String msg) {
        addErrorMessage(null, msg);
    }

    /**
     * Add error message to a specific client.
     *
     * @param clientId the client id
     * @param msg      the error message
     */
    public static void addErrorMessage(String clientId, String msg) {
        FacesContext.getCurrentInstance().addMessage(clientId, new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg));
    }

    private static Application getApplication() {
        ApplicationFactory appFactory = (ApplicationFactory) FactoryFinder.getFactory(FactoryFinder.APPLICATION_FACTORY);
        return appFactory.getApplication();
    }

    public static ValueBinding getValueBinding(String el) {
        return getApplication().createValueBinding(el);
    }

    public static HttpServletRequest getServletRequest() {
        return (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    }

    public static Object getElValue(String el) {
        return getValueBinding(el).getValue(FacesContext.getCurrentInstance());
    }

    public static String getJsfEl(String value) {
        return "#{" + value + "}";
    }
    
    public static void resetAllSessionManagedBeans(){
    	 Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
    	 if (sessionMap!=null){
    		 Iterator<String> it = sessionMap.keySet().iterator();
    		 while (it.hasNext()) {
				String key = (String) it.next();
				//System.out.println("key="+key);
				if (key.startsWith("{") && key.endsWith("}")){
					resetManagedBean(key);
				}
			}
    	 }
    }
    
    
    public static void showErrorMessage(String message) {
    	((NavigationTabsBean)getManagedBean("navigationTabsBean")).addWarningMessage(message);
    }
    public static void showWarningMessage(String message) {
    	((NavigationTabsBean)getManagedBean("navigationTabsBean")).addWarningMessage(message);
    }
    public static void showInfoMessage(String message) {
    	((NavigationTabsBean)getManagedBean("navigationTabsBean")).addInfoMessage(message);
    }
    public static void showDebugMessage(String message) {
    	((NavigationTabsBean)getManagedBean("navigationTabsBean")).addDebugMessage(message);
    }
    
    public static String getUser() {
    	FacesContext context = FacesContext.getCurrentInstance();
    	Map<String, Object> map = context.getExternalContext().getSessionMap();
    	String login = (String) map.get("SPRING_SECURITY_LAST_USERNAME");
    	return login;
    }
    
    public static boolean isUserLoggedIn() {
    	FacesContext context = FacesContext.getCurrentInstance();
    	Map<String, Object> map = context.getExternalContext().getSessionMap();
    	String login= (String) map.get("SPRING_SECURITY_LAST_USERNAME");
    	Exception loginException= (Exception) map.get("SPRING_SECURITY_LAST_EXCEPTION");

    	if (login!=null && loginException==null) return true;
    	return false;
    }
    
    public static String getUploadFolder(){
    	FacesContext context = FacesContext.getCurrentInstance();
    	return context.getExternalContext().getInitParameter("com.icesoft.faces.uploadDirectory");
    }
    
    public static void refresh() {
    	FacesContext context = FacesContext.getCurrentInstance();
    	Application application = context.getApplication();
    	ViewHandler viewHandler = application.getViewHandler();
    	UIViewRoot viewRoot = viewHandler.createView(context, context.getViewRoot().getViewId());
    	//ViewHandler viewHandler = application.getViewHandler();
    	//UIViewRoot viewRoot = viewHandler.createView(context, context.getViewRoot().getViewId());
    	context.setViewRoot(viewRoot);
    	context.renderResponse(); //Optional
    } 
    
    
    /**
     * Returns the base url (e.g, <tt>http://myhost:8080/myapp</tt>) suitable for
     * using in a base tag or building reliable urls.
     */
    public static String getBaseUrl() {
    	final FacesContext facesContext = FacesContext.getCurrentInstance();

        if (facesContext == null) {return null;}
        ServletRequest request = (ServletRequest) facesContext.getExternalContext().getRequest();

        String baseURL = null;
        String serverUrl = getServerURL(request);
        if (serverUrl != null) {
        	baseURL = serverUrl;
        	HttpServletRequest httpRequest = (request instanceof HttpServletRequest?(HttpServletRequest) request:null);
        	if (httpRequest != null) {
        		baseURL += httpRequest.getContextPath() + '/';
        	}
        }
        if (baseURL == null) {
        	System.out.println("Could not retrieve base url correctly");
        }



        

    	//System.out.println("FaceUtils - getBaseUrl: " + baseURL);
    	return baseURL;
    }
    
    /**
     * @return Server URL as : protocol://serverName:port/
     */
    private static String getServerURL(ServletRequest request) {

    	String baseURL = null;
    	HttpServletRequest httpRequest = (request instanceof HttpServletRequest?(HttpServletRequest) request:null);
    	if (httpRequest != null) {
			String serverName = httpRequest.getServerName();
			int serverPort = httpRequest.getServerPort();
			String scheme = httpRequest.getScheme();
			
			StringBuilder sbaseURL = new StringBuilder();
			sbaseURL.append(scheme);
			sbaseURL.append("://");
			sbaseURL.append(serverName);
			if (serverPort != 0) {
				if ("http".equals(scheme) && serverPort != 80 || "https".equals(scheme) && serverPort != 443) {
					sbaseURL.append(':');
					sbaseURL.append(serverPort);
				}
			}
			//sbaseURL.append('/');
			
			baseURL = sbaseURL.toString();
		}

    	if (baseURL == null) {
    		System.out.println("Could not retrieve base url correctly");
    	}
    	return baseURL;
    }


    public static String getLocalizedMessage(String key){
    	String message = "";
    	try {
	    	FacesContext context = FacesContext.getCurrentInstance();
			ResourceBundle bundle = context.getApplication().getResourceBundle(context, "msgs"); // msgs � il nome del bundle configurato in faces-config.xml
			message = bundle.getString(key);
    	} catch (Exception e) {
    		System.err.println(e.getMessage());
		}
    	return message;
    }
    
    
    
    public static void setCookie(String cookieName, String value, int expiryInSeconds) {

        FacesContext facesContext = FacesContext.getCurrentInstance();

        HttpServletRequest request = (HttpServletRequest) facesContext.getExternalContext().getRequest();
        Cookie cookie = null;

        Cookie[] userCookies = request.getCookies();
        if (userCookies != null && userCookies.length > 0 ) {
            for (int i = 0; i < userCookies.length; i++) {
                if (userCookies[i].getName().equals(cookieName)) {
                    cookie = userCookies[i];
                    break;
                }
            }
        }

        if (cookie != null) {
            cookie.setValue(value);
        } else {
            cookie = new Cookie(cookieName, value);
            cookie.setPath(request.getContextPath());
        }

        cookie.setMaxAge(expiryInSeconds);

        HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
        response.addCookie(cookie);
      }

      public static Cookie getCookie(String cookieName) {

        FacesContext facesContext = FacesContext.getCurrentInstance();

        HttpServletRequest request = (HttpServletRequest) facesContext.getExternalContext().getRequest();
        Cookie cookie = null;

        Cookie[] userCookies = request.getCookies();
        if (userCookies != null && userCookies.length > 0 ) {
            for (int i = 0; i < userCookies.length; i++) {
                if (userCookies[i].getName().equals(cookieName)) {
                    cookie = userCookies[i];
                    return cookie;
                }
            }
        }
        return null;
      }
      
      public static String getCookieValue(String cookieName) {

          FacesContext facesContext = FacesContext.getCurrentInstance();

          HttpServletRequest request = (HttpServletRequest) facesContext.getExternalContext().getRequest();
          Cookie cookie = null;

          Cookie[] userCookies = request.getCookies();
          if (userCookies != null && userCookies.length > 0 ) {
              for (int i = 0; i < userCookies.length; i++) {
                  if (userCookies[i].getName().equals(cookieName)) {
                      cookie = userCookies[i];
                      return cookie.getValue();
                  }
              }
          }
          return null;
        }
    
}
