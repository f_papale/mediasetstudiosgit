/**
 * 
 */
package com.crismasecurity.mediasetStudios.web.utils;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * Class name: CharacterEncodingFilter
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author Giovanni
 * @date 31/lug/2009
 *
 */
public class CharacterEncodingFilter implements Filter
{
        private String       encoding;
        private FilterConfig filterConfig;

        /**
         * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
         */
        public void init(FilterConfig fc) throws ServletException
        {
                this.filterConfig = fc;
                this.encoding     = filterConfig.getInitParameter("encoding");
        }

        /**
         * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
         */
        public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException
        {
                // Before reading the first information we have to set the encoding to
                // utf-8. Otherwise the characterencoding is lost and special characters
                // as for example "�" are misinterpreted.
                req.setCharacterEncoding(encoding);

                chain.doFilter(req, resp);
        }

        /**
         * @see javax.servlet.Filter#destroy()
         */
        public void destroy()
        {
        }
} 
