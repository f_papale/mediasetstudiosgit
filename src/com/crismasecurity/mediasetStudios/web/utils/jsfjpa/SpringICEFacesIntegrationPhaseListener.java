package com.crismasecurity.mediasetStudios.web.utils.jsfjpa;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.jsf.FacesContextUtils;
import org.springframework.orm.jpa.EntityManagerHolder;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.orm.jpa.EntityManagerFactoryUtils;
import org.springframework.util.Assert;
import org.apache.logging.log4j.*;

import javax.faces.event.PhaseListener;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseEvent;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityManager;
import javax.persistence.FlushModeType;

public class SpringICEFacesIntegrationPhaseListener implements PhaseListener {
  //Serial
	private static final long serialVersionUID = 8045211068705875674L;


private static final Logger logger = LogManager.getLogger(SpringICEFacesIntegrationPhaseListener.class);


  public static final String DEFAULT_EM_FACTORY_BEAN_NAME = "entityManagerFactory";
  private String entityManagerFactoryBeanName = DEFAULT_EM_FACTORY_BEAN_NAME;
  private boolean singleSession = true;

  /**
   * Holds the thread data for the current request cycle.
   */
  private static ThreadLocal ts = new ThreadLocal();

  /**
   * This class is a bit of hack
   * In the OpenSessionInViewFilter, the filtering is accomplished in one method.
   * This is not an option with this approach.
   * This class holds all of the local variables that were in the original doFilter method
   * of OpenSessionInViewFilter.
   *
   * @author Rick
   */
  private class ThreadData {
    private boolean participate;

    private EntityManagerFactory entityManagerFactory;
    private EntityManager em;
    private int beforeTimes;
    private int afterTimes;
  }

  /**
   * @see javax.faces.event.PhaseListener(javax.faces.event.PhaseEvent )
   */
  public void beforePhase(PhaseEvent pe) {

    logger.debug("######## BEFORE PHASE = " + pe.getPhaseId() + " viewId= "
            + (pe.getFacesContext().getViewRoot() != null ? pe.getFacesContext().getViewRoot().getId() : "no view root"));

    /* Setup the Hibernate Spring in the current thread using Spring API */
    if (pe.getPhaseId() == PhaseId.RESTORE_VIEW) {
      ThreadData td = getThreadData();
      /* This code is here b/c MyFaces seems to call every phase handler method twice. */
      if (td.beforeTimes == 0) {
        setupEntityManagerFactory(pe, td);
      }
    } /* HACK ALERT!
            The before restore_view does not get called for Apache JSF Portlet
            bridge when first viewing the JSF Portlet.
           Thus we will do this just before the render_response phase.*/
    else if (pe.getPhaseId() == PhaseId.RENDER_RESPONSE) {

      ThreadData td = getThreadDataNoCreate();
      if (td == null) {
        //if (logger.isDebugEnabled())
        logger.info("BEFORE RENDER_RESPONSE SETTING UP SESSION");
        td = getThreadData();
        setupEntityManagerFactory(pe, td);
      }

    }
  }

  /**
   * Get the current thread data
   */
  private ThreadData getThreadData() {
    ThreadData td = (ThreadData) ts.get();
    if (td == null) {
      td = new ThreadData();
      ts.set(td);
    } else {
      td.beforeTimes++;
    }
    return td;

  }

  /**
   * Get the thread data don't create it if it does not exist
   */
  private ThreadData getThreadDataNoCreate() {
    ThreadData td = (ThreadData) ts.get();
    return td;

  }

  /**
   * Setup the EntityManagerFactory. This was taken from the first half of the doFilter method.
   */
  private void setupEntityManagerFactory(PhaseEvent pe, ThreadData td) {

    logger.debug("SETUP EMF");

    td.entityManagerFactory = lookupEntityManagerFactory(pe.getFacesContext());
    Assert.notNull(td.entityManagerFactory);
    td.participate = false;

    if (isSingleSession()) {
      // single session mode
      if (TransactionSynchronizationManager.hasResource(td.entityManagerFactory)) {
        // Do not modify the Session: just set the participate flag.
        td.participate = true;
      } else {
        //td.session = getSession(td.sessionFactory);
        td.em = td.entityManagerFactory.createEntityManager();
        td.em.setFlushMode(FlushModeType.COMMIT);

        TransactionSynchronizationManager.bindResource(td.entityManagerFactory, new EntityManagerHolder(td.em));
      }
    }
  }

  /**
   * @see javax.faces.event.PhaseListener(javax.faces.event.PhaseEvent )
   */
  public void afterPhase(PhaseEvent pe) {
    logger.debug("AFTER = " + pe.getPhaseId() + " viewId= " + pe.getFacesContext().getViewRoot().getId());

    if (pe.getPhaseId() == PhaseId.RENDER_RESPONSE) {
      if (logger.isDebugEnabled())
        logger.debug("############################# IN AFTER RENDER_RESPONSE PHASE CLEANING UP SESSION ");
      ThreadData td = getThreadDataNoCreate();
      if (td == null) return;
      td.afterTimes++;
      cleanup(td);

      ts.set(null);
    }
  }

  /**
   * Cleanup the Hibernate session. This was taken from the first half of the doFilter method.
   */
  private void cleanup(ThreadData td) {
    if (logger.isDebugEnabled())
      logger.debug("CLEAN UP SESSION");
    if (!td.participate) {
      if (isSingleSession()) {
        // single session mode
        try {
          TransactionSynchronizationManager.unbindResource(td.entityManagerFactory);
          if (logger.isDebugEnabled())
            logger.debug("UNBINDING RESOURCE");
          closeSession(td.em);
          if (logger.isDebugEnabled())
            logger.debug("SESSION CLOSED");
        }
        catch (RuntimeException ex) {
          ex.printStackTrace();
        }
      }
    }
  }


  /**
   * Register for all Phase Events.
   *
   * @see javax.faces.event.PhaseListener(javax.faces.event.PhaseEvent )
   */
  public PhaseId getPhaseId() {
    return PhaseId.ANY_PHASE;
  }


  /**
   * Set the bean name of the SessionFactory to fetch from Spring's
   * root application context. Default is "sessionFactory".
   *
   * @see #DEFAULT_EM_FACTORY_BEAN_NAME
   */
  public void setEntityManagerFactoryBeanName(String entityManagerFactoryBeanName) {
    this.entityManagerFactoryBeanName = entityManagerFactoryBeanName;
  }

  /**
   * Return the bean name of the EntityManagerFactory to fetch from Spring's
   * root application context.
   */
  protected String getEntityManagerFactoryBeanName() {
    return entityManagerFactoryBeanName;
  }

  /**
   * Return whether to use a single session for each request.
   */
  protected boolean isSingleSession() {
    return singleSession;
  }


  /**
   * Look up the EntityManagerFactory that this filter should use.
   * <p>Default implementation looks for a bean with the specified name
   * in Spring's root application context.
   *
   * @return the EntityManagerFactory to use
   * @see #getEntityManagerFactoryBeanName
   */
  protected EntityManagerFactory lookupEntityManagerFactory(FacesContext facesContext) {
    WebApplicationContext wac = FacesContextUtils.getRequiredWebApplicationContext(facesContext);
    return (EntityManagerFactory) wac.getBean(getEntityManagerFactoryBeanName(), EntityManagerFactory.class);
  }


  /**
   * Close the active EntityManager
   *
   * @param em
   */
  protected void closeSession(EntityManager em) {
    EntityManagerFactoryUtils.closeEntityManager(em);
  }


}
