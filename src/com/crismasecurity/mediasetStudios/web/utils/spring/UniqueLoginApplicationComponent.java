package com.crismasecurity.mediasetStudios.web.utils.spring;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.enterprise.context.ApplicationScoped;

import org.springframework.stereotype.Component;


@ApplicationScoped
@Component
public class UniqueLoginApplicationComponent {

private Map<String, Long> usersPresenceTime = null; // Mappa utente - millisecondi ultimo ping timestamp (millisecondi)
	
	public UniqueLoginApplicationComponent() {
		super();
		
		usersPresenceTime = new ConcurrentHashMap<String, Long>();
	}
	
	
	public void removeUser(String user) {
		usersPresenceTime.remove(user);
	}
	
	public void updateUserTime(String user) {
		usersPresenceTime.put(user, System.currentTimeMillis());
	}
	
	public boolean isYetLogged(String user) {
		if (usersPresenceTime.get(user)!=null) {
			long tt = usersPresenceTime.get(user);
			if ((System.currentTimeMillis()-tt)<(1000*10)) {
				// Tolleranza 10 sec
				return true;
			}
			else {
				removeUser(user); // Rimuovo per sicurezza
				return false;
			}
		}
		else return false;
	}
	
}
