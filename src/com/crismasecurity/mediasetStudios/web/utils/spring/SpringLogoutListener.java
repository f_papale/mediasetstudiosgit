package com.crismasecurity.mediasetStudios.web.utils.spring;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.session.SessionDestroyedEvent;
import org.springframework.stereotype.Component;

import com.crismasecurity.mediasetStudios.core.entity.Audit;
import com.crismasecurity.mediasetStudios.core.security.AuthenticatedUserDto;
import com.crismasecurity.mediasetStudios.core.security.services.AppUserService;
import com.crismasecurity.mediasetStudios.core.security.vos.security.AppUserVO;
import com.crismasecurity.mediasetStudios.core.services.AuditService;


@Component
public class SpringLogoutListener implements ApplicationListener<SessionDestroyedEvent> {

	private Logger log = LogManager.getLogger(SpringLogoutListener.class);

	@Autowired
    private AuditService auditService;
	@Autowired
    private AppUserService userService;
	@Autowired
    private UniqueLoginApplicationComponent uniqueLoginApplicationComponent;
	
    @Override
    public void onApplicationEvent(SessionDestroyedEvent event)
    {
        List<SecurityContext> lstSecurityContext = event.getSecurityContexts();
        
        AuthenticatedUserDto ud;
        for (SecurityContext securityContext : lstSecurityContext)
        {
            ud = (AuthenticatedUserDto) securityContext.getAuthentication().getPrincipal();
            if (ud!=null) {
            	log.info("Logout for: " + ud.getDisplayName());
            	
            	try {
            		AppUserVO uu = userService.getUserByLogin(ud.getUsername());
                	auditService.addAudit(new Audit(uu.getLastName() + " " + uu.getFirstName()+" ("+ud.getUsername()+")", "Logout (session expired)", ""));
                	
                	
                	
                	uniqueLoginApplicationComponent.removeUser(ud.getUsername());
        		} catch (Exception e2) {
        			e2.printStackTrace();
        		}
            }
        }
    }

}