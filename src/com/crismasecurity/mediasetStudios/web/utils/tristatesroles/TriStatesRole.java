package com.crismasecurity.mediasetStudios.web.utils.tristatesroles;

public class TriStatesRole {

	private String nkey;
	private String label;
	private Long key_w;
	private Long key_r;
	private String trivalue;
	
	public TriStatesRole(){
		
	}
	
	public TriStatesRole(
			String nkey
			,String label
			,Long key_w
			,Long key_r
			,String trivalue
			){
		
		setTreStatesRole(nkey,label,key_w,key_r,trivalue );
	}
	
	
	public void setTreStatesRole (
		String nkey
		,String label
		,Long key_w
		,Long key_r
		,String trivalue
		){
		this.setNkey(nkey);
		this.label=label;
		this.key_w=key_w;
		this.key_r=key_r;
		this.trivalue=trivalue;
   }

	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public Long getKey_w() {
		return key_w;
	}
	public void setKey_w(Long key_w) {
		this.key_w = key_w;
	}
	public Long getKey_r() {
		return key_r;
	}
	public void setKey_r(Long key_r) {
		this.key_r = key_r;
	}
	public String getTrivalue() {
		return trivalue;
	}
	public void setTrivalue(String trivalue) {
		this.trivalue = trivalue;
	}

	public String getNkey() {
		return nkey;
	}

	public void setNkey(String nkey) {
		this.nkey = nkey;
	}

}
