package com.crismasecurity.mediasetStudios.web.utils.tristatesroles;

import com.crismasecurity.mediasetStudios.core.security.vos.security.RoleVO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;


public class ManageTriStateRoles {

	private Map<String, String > selectedOptionsTriStateRole;
	private Map<String, String> optionRoles;
	private Map<String, TriStatesRole> workingRoles;
	private List<RoleVO> allOriginalRoles;
	

	public ManageTriStateRoles() {
		init();
		this.workingRoles  = new HashMap<String, TriStatesRole>(); 
	}
	
	public ManageTriStateRoles(	List<RoleVO> _allOriginalRoles) {
		this.allOriginalRoles=_allOriginalRoles;
		this.workingRoles  = new HashMap<String, TriStatesRole>(); 
	}
	
	public void buildWorkingRoles (List<RoleVO> _selectedOriginalGroupRoles){
		
		String nKey = ""; 
		String descrizione = "";
		Long key_w = 0L;
		Long key_r = 0L;
		String triValue = "";

		init();
		

		this.workingRoles.clear();
		this.selectedOptionsTriStateRole.clear();
		
		for(RoleVO roleVO : allOriginalRoles) {
			
			if ((roleVO.getNome().length()==14) && (roleVO.getNome().toUpperCase().substring(12,14).contentEquals("_W") || roleVO.getNome().toUpperCase().substring(12,14).contentEquals("_R"))) {
				nKey = roleVO.getNome().toUpperCase().substring(5,8);
				try {
					descrizione  = roleVO.getDescrizioneBreve().substring(0, roleVO.getDescrizioneBreve().lastIndexOf("-") - 1);	
				}catch (Exception e) {
					descrizione  = roleVO.getDescrizioneBreve();
				}
				
				if (roleVO.getNome().toUpperCase().substring(12,14).contentEquals("_W")) {
					
					key_r = 0L;
					if (workingRoles.containsKey(nKey))
						key_r = workingRoles.get(nKey).getKey_r();
					key_w = roleVO.getId();
					if (getSelectedRoles(_selectedOriginalGroupRoles).contains(roleVO.getId())) {  
						triValue="1";
					}else { 
						triValue="0";						
					}
				} else if (roleVO.getNome().toUpperCase().substring(12,14).contentEquals("_R")) {
					key_w = 0L;
					if (workingRoles.containsKey(nKey))
						key_w = workingRoles.get(nKey).getKey_w();					
					key_r = roleVO.getId();
					if (getSelectedRoles(_selectedOriginalGroupRoles).contains(roleVO.getId())) {  
						triValue="2";
					}else { 
						triValue="0";
					}
				}
			}else {
				nKey = roleVO.getNome().toUpperCase();
				descrizione  =roleVO.getDescrizioneBreve();
				key_w = roleVO.getId();
				key_r = 0L;
				if (getSelectedRoles(_selectedOriginalGroupRoles).contains(roleVO.getId())) 
					triValue="1";
				else 
					triValue="0";
			}
			
			TriStatesRole newTriRole = new TriStatesRole(nKey, descrizione, key_w, key_r, triValue);
			if (workingRoles.containsKey(nKey)) {
				workingRoles.replace(nKey,newTriRole);
			} else {
				workingRoles.put(nKey, newTriRole);
			}
			
			optionRoles.put (newTriRole.getLabel(), nKey);
			if (! newTriRole.getTrivalue().contentEquals("0")) {
				selectedOptionsTriStateRole.put(nKey, newTriRole.getTrivalue());
			}
		}

	}
	
	private List<RoleVO> getDecodedRoles(){
		
		RoleVO roleToInsert = null; 
		List<RoleVO> roles = new ArrayList<RoleVO>();
		Iterator<Entry<String, TriStatesRole>> it = workingRoles.entrySet().iterator();
		while (it.hasNext()) {
			Entry<String, TriStatesRole> pair = it.next();
			TriStatesRole item = (TriStatesRole) pair.getValue();
			if (! item.getTrivalue().contentEquals("0")) {
				if (item.getTrivalue().contentEquals("1")) {
					roleToInsert = allOriginalRoles.stream()
					.filter(triStateRole -> triStateRole.getId().equals(item.getKey_w()))
					.findAny()
					.orElse(null);
			} else if(item.getTrivalue().contentEquals("2")){
				roleToInsert = allOriginalRoles.stream()
				.filter(triStateRole -> triStateRole.getId().equals(item.getKey_r()))
				.findAny()
				.orElse(null);
				if (roleToInsert == null) {
					roleToInsert = allOriginalRoles.stream()
					.filter(triStateRole -> triStateRole.getId().equals(item.getKey_w()))
					.findAny()
					.orElse(null);					
				}
			}
			roles.add(roleToInsert);
			it.remove();
		   }
		}
		return roles;
	}
	
	public List<RoleVO> decodeRoles( Map<String, String > selectedOptionsTriStateRole){

		TriStatesRole newTriRole = null;
		Iterator<Entry<String, String>> it = selectedOptionsTriStateRole.entrySet().iterator();
		while (it.hasNext()) {
			Entry<String, String> pair = it.next();
			newTriRole = this.workingRoles.get(pair.getKey());
			try {
				newTriRole.setTrivalue(((pair.getValue()=="") || (pair.getValue()==null))?"0":pair.getValue());
				this.workingRoles.replace(pair.getKey(), newTriRole);
			} catch (Exception e) {
				System.out.println(e);
			}
		}
		return getDecodedRoles();
	}
	
	private void init() {
    	optionRoles = new HashMap<String, String>();
    	selectedOptionsTriStateRole = new HashMap<String, String>();
	}	
	
    private List<Long> getSelectedRoles(List<RoleVO> _selectedGroupRoles) {
    	List<Long> rolesId = new ArrayList<Long>();
    	if (_selectedGroupRoles!=null) {
	    	for (RoleVO roleVO : _selectedGroupRoles) {
	    		rolesId.add(roleVO.getId());
			}
    	}
    	return rolesId;
    }
    
	public Map<String,String > getSelectedOptionsTriStateRole() {
		return selectedOptionsTriStateRole;
	}

	public void setSelectedOptionsTriStateRole(final Map<String,String > _selectedOptionsTriStateRole) {
		this.selectedOptionsTriStateRole = _selectedOptionsTriStateRole;
	}

	public Map<String,String> getOptionRoles() {
		return optionRoles;
	}

	public void setOptionRoles(final Map<String,String> _optionRoles) {
		this.optionRoles = _optionRoles;
	}

	public Map<String, TriStatesRole> getWorkingRoles() {
		return workingRoles;
	}

	public void setWorkingRoles(final Map<String, TriStatesRole> _workingRoles) {
		this.workingRoles = _workingRoles;
	}

}
