package com.crismasecurity.mediasetStudios.web.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.SystemUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.crismasecurity.mediasetStudios.core.security.entities.AppUser;
import com.crismasecurity.mediasetStudios.core.security.entities.Group;
import com.crismasecurity.mediasetStudios.core.security.vos.security.RoleVO;

 
/**
 * Convert a POJO into a simple 2 column HTML table.
 */
public class MakeHTML {
	private static final class HTMLStyle extends ToStringStyle {
 
		private static final long serialVersionUID = 2885090168747687747L;

		private List<String> excludedFields;
		
		public HTMLStyle(String ... excludedFields) {
			setFieldSeparator("</td></tr>"+ SystemUtils.LINE_SEPARATOR + "<tr><td>");
 
			setContentStart("<table class='htmlObjectTable'>"+ SystemUtils.LINE_SEPARATOR +
								"<thead><tr><th>Field</th><th>Value</th></tr></thead>" +
								"<tbody><tr><td>");
 
			setFieldNameValueSeparator("</td><td>");
 
			setContentEnd("</td></tr>"+ SystemUtils.LINE_SEPARATOR + "</tbody></table>");
 
			setArrayContentDetail(true);
			setUseShortClassName(true);
			setUseClassName(false);
			setUseIdentityHashCode(false);
			
			this.excludedFields = new ArrayList<String>();
			
			for(String fi : excludedFields){
				this.excludedFields.add(fi);
			}
		}
 
		@Override
		public void appendDetail(StringBuffer buffer, String fieldName, Object value) {
			//System.out.println("Tratto campo:" + fieldName+ "    value:" + value + "   valueclass:" + (value!=null?value.getClass().getName():"null"));
			//System.out.println("Excluded fields: " + excludedFields);
			boolean excluded = false;
			for(String fi : excludedFields){
				if (fieldName!=null && fieldName.equalsIgnoreCase(fi)) {
					//System.out.println("Confronto campo:" + fi + "  con:" + fieldName);
					excluded = true;
					break;
				}
		    }
			
			if (!excluded) {
				Object rv = getReadableValue(value);
				
				if (value.getClass().getName().startsWith("com.crismasecurity.mediasetStudios.core.security.entities.AppUser") ||
						value.getClass().getName().startsWith("java.lang") || value.getClass().getName().startsWith("java.util.Date")
						|| value.getClass().getName().startsWith("java.sql.Timestamp")
						) {
					super.appendDetail(buffer, fieldName, rv);
				}
				else {
					buffer.append(ReflectionToStringBuilder.toString(value, this));
				}
			}
			else {
				//super.appendDetail(buffer, fieldName, "<...>");
			}
		}

		
		@Override
		protected void appendDetail(StringBuffer buffer, String fieldName, Collection<?> coll) {
//			System.out.println("Tratto A campo:" + fieldName+ "    value:" + coll + "   valueclass:" + (coll!=null?coll.getClass().getName():"null"));
			boolean excluded = false;
			for(String fi : excludedFields){
				if (fieldName!=null && fieldName.equalsIgnoreCase(fi)) {
					//System.out.println("Confronto campo:" + fi + "  con:" + fieldName);
					excluded = true;
					break;
				}
		    }
			
			if (!excluded) {
				String res ="";
				if (coll!=null) {
					for (Object object : coll) {
						Object rv = getReadableValue(object);
						res += rv.toString() + "<BR /> ";
					}
				}
				
				super.appendDetail(buffer, fieldName, res);
			}
		}

		@Override
		protected void appendDetail(StringBuffer buffer, String fieldName, Map<?, ?> map) {
//			System.out.println("Tratto B campo:" + fieldName+ "    value:" + map + "   valueclass:" + (map!=null?map.getClass().getName():"null"));
			boolean excluded = false;
			for(String fi : excludedFields){
				if (fieldName!=null && fieldName.equalsIgnoreCase(fi)) {
					//System.out.println("Confronto campo:" + fi + "  con:" + fieldName);
					excluded = true;
					break;
				}
		    }
			
			if (!excluded) {
				String res ="";
				if (map!=null) {
					for (Object key : map.keySet()) {
						Object rv = getReadableValue(key);
						Object rvv = getReadableValue(map.get(key));
						res += "["+rv.toString() + " -- " + rvv.toString()+"]<BR /> ";
					}
				}
				
				super.appendDetail(buffer, fieldName, res);
			}
		}

		@Override
		protected void appendDetail(StringBuffer arg0, String fieldName, Object[] ar) {
//			System.out.println("Tratto C campo:" + fieldName+ "    value:" + ar + "   valueclass:" + (ar!=null?ar.getClass().getName():"null"));
			boolean excluded = false;
			for(String fi : excludedFields){
				if (fieldName!=null && fieldName.equalsIgnoreCase(fi)) {
					//System.out.println("Confronto campo:" + fi + "  con:" + fieldName);
					excluded = true;
					break;
				}
		    }
			
			if (!excluded) {
				String res ="";
				if (ar!=null) {
					for (Object object : ar) {
						Object rv = getReadableValue(object);
						res += rv.toString() + "<BR /> ";
					}
				}
				super.appendDetail(arg0, fieldName, res);
			}
		}
		
		
		
		private Object getReadableValue(Object value) {
			if (value.getClass().getName().startsWith("com.crismasecurity.mediasetStudios.core.security.entities.AppUser")) {
				AppUser u = (AppUser) value;
				return (u!=null?u.getLastName() + " " + u.getFirstName():"");
			}
			else if (value.getClass().getName().startsWith("com.crismasecurity.mediasetStudios.core.security.entities.Group")) {
				Group u = (Group) value;
				return (u!=null?u.getName():"");
			}
			else if (value.getClass().getName().startsWith("com.crismasecurity.mediasetStudios.core.security.vos.security.RoleVO")) {
				RoleVO u = (RoleVO) value;
				return (u!=null?u.getNome():"");
			}
			else if (value.getClass().getName().startsWith("java.lang")) {
				return value;
			} 
			else if (value.getClass().getName().startsWith("java.util.Date")) {
				Date aa = (Date)value;
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
				return (aa!=null?sdf.format(aa):"");
			}
			else if (value.getClass().getName().startsWith("java.sql.Timestamp")) {
				java.sql.Timestamp aa = (java.sql.Timestamp)value;
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
				return (aa!=null?sdf.format(aa):"");
			}
			
			
			
			else {
				return value;
			}
		}
		
		
	}
 
	//... -> It means that zero or more String objects
	static public String makeHTML(Object object, String ... excludedFields) {
		ReflectionToStringBuilder.setDefaultStyle(new HTMLStyle(excludedFields));
		return	ReflectionToStringBuilder.toStringExclude(object, excludedFields);
		// return	ReflectionToStringBuilder.toString(object, new HTMLStyle(excludedFields));
	}
}