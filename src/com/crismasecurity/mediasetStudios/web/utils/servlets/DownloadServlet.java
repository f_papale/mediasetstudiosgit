package com.crismasecurity.mediasetStudios.web.utils.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;


public class DownloadServlet extends HttpServlet {

	//Serial
	private static final long serialVersionUID = 8035830464501393185L;

	
	
	/**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
    
		String uniqueId= request.getParameter("uniqueId");
		
		if (uniqueId!=null && !uniqueId.equals("")){
			response.setContentType("application/octet-stream");
			response.setHeader("Cache-Control","private"); //HTTP 1.1
			response.setHeader("Pragma","private"); //HTTP 1.0
			response.setDateHeader("Expires", 0); //prevents caching at the proxy server
			
			try {
				ApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
				/*
				String[] beanDefinitionNames = ctx.getBeanDefinitionNames();
				for (String string : beanDefinitionNames) {
					System.out.println(string);
				}
				*/
				DownloadListBean downloadListBean = ctx.getBean(DownloadListBean.class);
				byte[] stream = downloadListBean.getObjectsToDownload().get(uniqueId);
				String name = "file";
				if (stream!=null) {
					name = downloadListBean.getObjectsToDownloadNames().get(uniqueId);
					
					System.out.println("#stream:"+stream);
					System.out.println("#name:"+name);
//					log.info("#stream:"+stream);
//					log.info("#name:"+name);
					
					response.setHeader("Content-disposition", "inline;filename=\""+name+"\""); 
					response.getOutputStream().write(stream);
					
					// Rimuovo l'oggetto
					downloadListBean.removeDownloadObject(uniqueId);
				}
			} catch (Throwable ex) {
				ex.printStackTrace();
			}
		}
	}

 
}
