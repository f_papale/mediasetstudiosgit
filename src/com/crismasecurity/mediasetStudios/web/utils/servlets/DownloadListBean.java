package com.crismasecurity.mediasetStudios.web.utils.servlets;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.crismasecurity.mediasetStudios.web.utils.jsf.FacesUtils;


/**
 * 
 * Class name: DownloadListBean
 *
 * Description: Contiene la lista degli elementi da scaricare che saranno gestiti dalla DownlaodServlet.
 * 				Se si vuole che l'utente scarichi un file basta aggiungerlo alla lista di download di questa classe dandogli un id univoco (metodo addDownloadObject)
 * 				e richiamare l'indirizzo della servlet.
 * 
 * 
 *
 * Company: EasyMarine
 *
 * @author Giovanni
 * @date 31/ott/2012
 *
 */
@Component
@Scope("session")  
@Qualifier("downloadListBean")  
public class DownloadListBean implements Serializable {

	// Serial
	private static final long serialVersionUID = -3403521602419519830L;
	
	private Map<String, byte[]> objectsToDownload;
	private Map<String, String> objectsToDownloadNames;
	

	/**
     * default empty constructor
     */
    public DownloadListBean() {
    	objectsToDownload = new HashMap<String, byte[]>();
    	objectsToDownloadNames = new HashMap<String, String>();
    }
  
    public void addDownloadObject(String uniqueId, byte[] stream, String name) {
    	objectsToDownload.put(uniqueId, stream);
    	objectsToDownloadNames.put(uniqueId, name);
    }
	
    public void removeDownloadObject(String uniqueId) {
    	objectsToDownload.remove(uniqueId);
    	objectsToDownloadNames.remove(uniqueId);
    }

    public String getDownloadUrl(String uniqueId) {
    	return FacesUtils.getServletContext().getContextPath() + "/downloadObject?uniqueId=" + uniqueId;
    }
    
    
	protected Map<String, byte[]> getObjectsToDownload() {
		return objectsToDownload;
	}

	protected Map<String, String> getObjectsToDownloadNames() {
		return objectsToDownloadNames;
	}
}
