/**
 * 
 */
package com.crismasecurity.mediasetStudios.web.utils.reporting;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import org.apache.logging.log4j.*;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

/**
 * Class name: JasperReportsFactory
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author 
 * @date 21/mag/2012
 *
 */
@Service
public class JasperReportsFactory implements ResourceLoaderAware {

	private static final Logger logger = LogManager.getLogger(JasperReportsFactory.class);

	private ResourceLoader resourceLoader;

	/**
	 *
	 * @param reportPath Resource path to master Jasper Report file accepting .jasper or .jrxml files.
	 * @param subReports Map of subreport parameter keys and corresponding path to resource.
	 * @param parameterMap Map of report parameter keys and corresponding Objects for report.
	 * @param beanCollection Collection of model beans
	 * @return
	 * @throws JasperReportsException
	 */
	@SuppressWarnings("unchecked")
	public ByteArrayOutputStream complieReportAsPdfByteArrayOutputStream(JasperReportDetails  jasperReportDetails, Map<String, Object> additionalReportParametersMap, Collection beanCollection) throws Exception {
		if (logger.isInfoEnabled()) {
			logger.info("Compiling Jasper Report to PDF OutPutStream. {{ JasperReportDetails }} " + jasperReportDetails.getDescription() );
		}

		if(jasperReportDetails.getMasterReportUrl() == null) {
			throw new IllegalArgumentException("A masterReportUrl is required.");
		}

		if(beanCollection == null ) {
			throw new IllegalArgumentException("BeanCollection can not be empty");
		}

		JRBeanCollectionDataSource collection = new JRBeanCollectionDataSource(beanCollection);

		Map<String, Object> reportParameters = new HashMap<String, Object>();
		reportParameters.putAll(jasperReportDetails.getReportParameters());

		JasperReport masterJasperReport = loadReport(resourceLoader.getResource(jasperReportDetails.getMasterReportUrl()));

		// Compile sub reports and add all subreports to the report parameter map
		if(jasperReportDetails.getSubReportUrls() != null && jasperReportDetails.getSubReportUrls().size() > 0){
			Map comiledSubReports = new HashMap<String, Object>(jasperReportDetails.getSubReportUrls().size());
			for (Enumeration urls = jasperReportDetails.getSubReportUrls().propertyNames(); urls.hasMoreElements();) {
				String key = (String) urls.nextElement();
				String path = jasperReportDetails.getSubReportUrls().getProperty(key);
				Resource resource = resourceLoader.getResource(path);
				comiledSubReports.put(key, loadReport(resource));
			}
			reportParameters.putAll(comiledSubReports);
		}

		// Add aditional parameters
		if(additionalReportParametersMap != null && additionalReportParametersMap.size() > 0) {
			reportParameters.putAll(additionalReportParametersMap);
		}

		// Fill and export jasper report
		try {

			if (logger.isInfoEnabled()) {
				logger.info("Filling Jasper Report with report parameters and collection. ");
			}

			JasperPrint jasperPrint = JasperFillManager.fillReport(masterJasperReport, reportParameters, collection);
			ByteArrayOutputStream baos = new ByteArrayOutputStream(4096);

			if (logger.isInfoEnabled()) {
				logger.info("Exporting Jasper Report to pdf stream.");
			}

			JasperExportManager.exportReportToPdfStream(jasperPrint, baos);

			return baos;
		} catch (JRException jre) {
			throw new Exception("Unable to compile and fill pdf report", jre);
		} catch (Exception e){
			throw new Exception("Unkown exception while trying to compile and fill pdf report", e);
		}
	}

	public File complieReportAsPdfFile(File file, JasperReportDetails  jasperReportDetails, Map<String, Object> additionalReportParametersMap, Collection beanCollection) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("Compling PDF Report. ");
		}

		ByteArrayOutputStream baos = complieReportAsPdfByteArrayOutputStream(jasperReportDetails, additionalReportParametersMap, beanCollection);

		FileOutputStream fos = null;

		try {
			fos = new FileOutputStream(file);
		} catch (FileNotFoundException e) {
			throw new Exception("File ["+file.getPath()+"] not found.",e);
		}
		try {
			fos.write(baos.toByteArray());
			fos.close();
		} catch (IOException e) {
			throw new Exception(e);
		}
		return file;
	}

	/**
	 * Loads a <code>JasperReport</code> from the specified <code>Resource</code>. If
	 * the <code>Resource</code> points to an uncompiled report design file then the
	 * report file is compiled dynamically and loaded into memory.
	 * @param resource the <code>Resource</code> containing the report definition or design
	 * @return a <code>JasperReport</code> instance
	 */
	private JasperReport loadReport(Resource resource) throws Exception {
		try {
			String fileName = resource.getFilename();

			if (fileName.endsWith(".jasper")) {
				// Load pre-compiled report.
				if (logger.isInfoEnabled()) {
					logger.info("Loading pre-compiled Jasper Report from " + resource.getDescription() );
				}
				return (JasperReport) JRLoader.loadObject(resource.getInputStream());
			}

			else if (fileName.endsWith(".jrxml")) {
				// Compile report on-the-fly.
				if (logger.isInfoEnabled()) {
					logger.info("Compiling Jasper Report loaded from " + resource.getDescription() );
				}
				JasperDesign design = JRXmlLoader.load(resource.getInputStream());
				return JasperCompileManager.compileReport(design);
			}

			else {
				throw new IllegalArgumentException(
						"Report [" + resource.getDescription() + "] must end in either .jasper or .jrxml");
			}
		}
		catch (IOException ex) {
			throw new Exception(
					"Could not load JasperReports report from resource [" + resource.getDescription()+ "]", ex);
		}
		catch (JRException ex) {
			throw new Exception(
					"Could not parse JasperReports report  from resource [" + resource.getDescription() + "]", ex);
		}
	}

	public void setResourceLoader(ResourceLoader resourceLoader) {
		this.resourceLoader = resourceLoader;
	}
}