package com.crismasecurity.mediasetStudios.web.audit;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
/*import org.icefaces.ace.component.fileentry.FileEntry;
import org.icefaces.ace.component.fileentry.FileEntryEvent;
import org.icefaces.ace.component.fileentry.FileEntryResults;*/
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.crismasecurity.mediasetStudios.core.entity.Audit;
import com.crismasecurity.mediasetStudios.core.services.AuditService;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;
import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.FontFactory;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;




//@ManagedBean(name= AuditBean.BEAN_NAME) // JSF managed
//@SessionScoped // JSF managed
//@ViewScoped // JSF managed

//Spring managed
@Controller (AuditBean.BEAN_NAME) 
//@Scope("session")
@Scope("view")
public class AuditBean extends BaseBean {

	private static final long serialVersionUID = -138725537985474694L;

	public static final String BEAN_NAME = "auditBean";
    public String getBeanName() { return BEAN_NAME; } 

    @Autowired
    private AuditService contractService ;

    private LazyDataModel<Audit> lazyModelAudits;
	private Audit selectedAudit = null;
    
    
	private Date filterFrom;
	private Date filterTo;
    
	/**
     * 
     */
	public AuditBean() {
		getLog().info("!!!! Costruttore di AuditBean !!!");

    }

	@PostConstruct
	public void init() {
		getLog().info("INIT AuditBean!");
		
		lazyModelAudits = new LazyAuditDataModel(contractService);
	}

	@PreDestroy
	public void destroy() {
		getLog().info("Destroy AuditBean!");
	}

	
	
	public void filterByDate() {
		System.out.println("Filter by date !");
		
		Calendar cal = Calendar.getInstance();
		if (filterFrom!=null) {
			cal.setTime(filterFrom);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 1);
			filterFrom = cal.getTime();
		}
		if (filterTo!=null) {
			cal.setTime(filterTo);
			cal.set(Calendar.HOUR_OF_DAY, 23);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);
			filterTo = cal.getTime();
		}
		
		((LazyAuditDataModel)lazyModelAudits).setFilterFrom(filterFrom);
		((LazyAuditDataModel)lazyModelAudits).setFilterTo(filterTo);
	}
	
	
	 public void onRowSelect(SelectEvent event) {
//        FacesMessage msg = new FacesMessage("Audit Selected", ((Audit) event.getObject()).getId());
//        FacesContext.getCurrentInstance().addMessage(null, msg);
		 
		 System.out.println("Selezionato audit id:" + (selectedAudit!=null?selectedAudit.getId():""));
		 
		 // Per ora la selezione non fa niente
    }
	
	
	public void closeSelectedAudit() {
		selectedAudit = null;
	}
	
	
	
	
	
	public void preProcessPDF(Object document) throws IOException, BadElementException, DocumentException {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		DateFormat dfDate = new SimpleDateFormat("dd/MM/yyyy");
		
        Document pdf = (Document) document;
        
        
        HeaderFooter footer = new HeaderFooter( new Phrase( df.format(new Date()) + " page:", FontFactory.getFont(FontFactory.COURIER, 8f) ), true );
        footer.setBorder( Rectangle.NO_BORDER );
        footer.setAlignment( Element.ALIGN_RIGHT );
        pdf.setFooter(footer);
        
        
        String title = "Audit Trail";
        Paragraph tit = new Paragraph(title);
        tit.setAlignment(Element.ALIGN_CENTER);
        //tit.setSpacingAfter(10);
        
        HeaderFooter header = new HeaderFooter( tit, false );
        header.setBorder( Rectangle.NO_BORDER );
        header.setAlignment( Element.ALIGN_CENTER);
        pdf.setHeader(header);
        
        
        
        pdf.setPageSize(PageSize.A4.rotate()); // Landscape
        pdf.open();
        //pdf.setPageSize(PageSize.A4);
        
        
        
        String subtitle = "Filters: ";
        if (filterFrom!=null) subtitle += " From " + dfDate.format(filterFrom);
		if (filterTo!=null) subtitle += " To " + dfDate.format(filterTo);
		
        Paragraph e = new Paragraph(new Phrase(8f,subtitle,FontFactory.getFont(FontFactory.COURIER, 8f, Color.BLACK)));
        e.setAlignment(Element.ALIGN_RIGHT);     
//        //e.setIndentationLeft(100);
//        e.setSpacingBefore(100);
        e.setSpacingAfter(30);
        pdf.add(e);
        
 
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        String logo = externalContext.getRealPath("") + File.separator + "resources" + File.separator + "images" + File.separator + "logo.png";
        Image im = Image.getInstance(logo);
        im.scaleToFit(60, 60);
        im.setAbsolutePosition(30f, pdf.getPageSize().getHeight()-70);
        pdf.add(im);
        
        
        
        
        
        
        
        
//        String title = "Audit Trail";
//        Paragraph tit = new Paragraph(title);
//        tit.setAlignment(Element.ALIGN_CENTER);
//        //tit.setSpacingBefore(-40);
//        tit.setSpacingAfter(10);
//        pdf.add(tit);
        
        
        
        
    }
	
	
	
	public LazyDataModel<Audit> getLazyModelAudits() {
		return lazyModelAudits;
	}

	public Audit getSelectedAudit() {
		return selectedAudit;
	}

	public void setSelectedAudit(Audit selectedAudit) {
		this.selectedAudit = selectedAudit;
	}

	public Date getFilterFrom() {
		return filterFrom;
	}

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public Date getFilterTo() {
		return filterTo;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}
	
}
