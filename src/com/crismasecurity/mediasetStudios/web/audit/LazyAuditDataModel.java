package com.crismasecurity.mediasetStudios.web.audit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.crismasecurity.mediasetStudios.core.entity.Audit;
import com.crismasecurity.mediasetStudios.core.services.AuditService;


 
/**
 * Dummy implementation of LazyDataModel that uses a list to mimic a real datasource like a database.
 */
public class LazyAuditDataModel extends LazyDataModel<Audit> {
     
	private static final long serialVersionUID = -7803934729089718942L;
	
	private AuditService contractService;
	
	private Date filterFrom;
	private Date filterTo;
     
    public LazyAuditDataModel(AuditService contractService) {
        this.contractService = contractService;
    }
     
    @Override
    public Audit getRowData(String rowKey) {
    	if (rowKey==null || rowKey.trim().equals("")) return null;
    	return contractService.getAuditById(Long.parseLong(rowKey));
    }
 
    @Override
    public Object getRowKey(Audit audit) {
        return audit.getId();
    }
 
    @Override
    public List<Audit> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
    	
    	
    	// Converto ordinamento
    	int sortInt =0; // UNSORTED
    	if (sortOrder.equals(SortOrder.ASCENDING)) sortInt = 1;
    	if (sortOrder.equals(SortOrder.DESCENDING)) sortInt = 2;
    	
    	if(sortField == null) { // Default sort
            sortField = "timestamp";
            sortInt = 2;
        }
    	
    	
    	if (filterFrom!=null) filters.put("timestamp#ge", filterFrom);
    	if (filterTo!=null) filters.put("timestamp#le", filterTo);
    	
    	List<Audit> ret = contractService.getAuditLazyLoading(first, pageSize, sortField, sortInt, filters);
    	this.setRowCount((int)contractService.getAuditLazyLoadingCount(filters));
//    	System.out.println("Numero elementi trovati: " + this.getRowCount());
//    	for (Audit audit : ret) {
//			System.out.println("Trovato: " + audit);
//		}
    	return ret;    
    }

	public void setFilterFrom(Date filterFrom) {
		this.filterFrom = filterFrom;
	}

	public void setFilterTo(Date filterTo) {
		this.filterTo = filterTo;
	}
}