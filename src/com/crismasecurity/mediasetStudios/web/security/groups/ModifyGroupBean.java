package com.crismasecurity.mediasetStudios.web.security.groups;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;

import com.crismasecurity.mediasetStudios.core.entity.Audit;
import com.crismasecurity.mediasetStudios.core.entity.RoleMasterSlave;
import com.crismasecurity.mediasetStudios.core.security.services.AppUserService;
import com.crismasecurity.mediasetStudios.core.security.services.GroupService;
import com.crismasecurity.mediasetStudios.core.security.services.RoleService;
import com.crismasecurity.mediasetStudios.core.security.vos.security.AppUserVO;
import com.crismasecurity.mediasetStudios.core.security.vos.security.GroupVO;
import com.crismasecurity.mediasetStudios.core.security.vos.security.RoleVO;
import com.crismasecurity.mediasetStudios.core.services.AuditService;
import com.crismasecurity.mediasetStudios.core.services.RoleMasterSlaveService;
import com.crismasecurity.mediasetStudios.core.utils.PropertiesUtils;
import com.crismasecurity.mediasetStudios.web.NavigationBean;
import com.crismasecurity.mediasetStudios.web.utils.MakeHTML;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;
import com.crismasecurity.mediasetStudios.web.utils.tristatesroles.ManageTriStateRoles;
import com.crismasecurity.mediasetStudios.web.utils.tristatesroles.TriStatesRole;




//@ManagedBean(name= "modifyGroupBean") // JSF managed
//@SessionScoped // JSF managed
//@ViewScoped // JSF managed

//Spring managed
@Controller ("modifyGroupBean") 
//@Scope("session")
@Scope("view")
public class ModifyGroupBean extends BaseBean {

	//Serial
	private static final long serialVersionUID = -6550476690484201415L;

	@Autowired
	private RoleService roleService;
	
	@Autowired
	private RoleMasterSlaveService roleMasterSlaveService; 
	
	@Autowired
	private GroupService groupService;
	
	@Autowired
	private NavigationBean navigationBean;
	
	@Autowired
	private AuditService auditService;
	@Autowired
	private AppUserService userService;
	
	private GroupVO group;
	
	private String groupId;
	
	private int menuSections = 8;
	private Map<String, String>[] selectedOptionsTriStateRole = new Map[menuSections ];
	private Map<String, String>[] optionRoles = new Map[menuSections ];
	private Map<String, TriStatesRole>[] workingGroupRoles = new Map[menuSections ];
	
	private ManageTriStateRoles[] manageTriStateRoles = new ManageTriStateRoles[menuSections ];

	private boolean enableRoleMasterSlave;

	private boolean bLoaded=false;
	
	
    /**
     * default empty constructor
     */
    public ModifyGroupBean() {
    	group = new GroupVO();
		enableRoleMasterSlave = Boolean.parseBoolean(PropertiesUtils.getInstance().readProperty("roles.addrelatedroles","false"));
    }

	@PostConstruct
	public void initCustomModel()  {
		System.out.println("INIT ModifyGroupBean");
		initRoles();
	}
	
	
	public void modifyGroup() {
		if (! bLoaded) {
			bLoaded=true;		
	    	GroupVO vo = null;
	    	if (groupId!=null && !groupId.equals("")) vo = groupService.loadGroup(Long.decode(groupId));
	    	if (vo!=null) group=vo;
	    	else group = new GroupVO();
    		presetRoles(group);
    		System.out.println("Il gruppo ha id:" + group.getId());
    	}
    }
    
    protected void newGroup() {
    	group = new GroupVO();
    }
    
    
    /**
     * Method that is backed to a submit button of a form.
     */
    public void updateGroup(ActionEvent e) throws java.io.IOException {
    	System.out.println("Aggiorno il gruppo con id:" + group.getId() + " ");
    	
    	if (group.getId()==null || (group.getId()!=null && group.getId()<1)) {
    		System.out.println("Il gruppo non ha l'id valorizzato!");
    		addWarningMessage("Can not update a group not yet created!");
            return;
    	}
    	
    	try {
    		System.out.println("Carico il gruppo con quell'id!");
	    	GroupVO groupL = groupService.loadGroup(group.getId());
	    	
	    	
	    	String oldHtml = null;
			if (groupL!=null) {
				oldHtml = MakeHTML.makeHTML(groupL/*, "roles"*/);
			}
	    	
	    	if (groupL!=null) {
	    		if (!groupL.getName().equals(group.getName())) {
	    			// Controllo che non vi sia un altro gruppo con lo stesso nome
	    			System.out.println("Nome cambiato, controllo se esiste un altro gruppo con lo stesso nome!");
	    			if (groupService.findByName(group.getName())!=null) {
	    				addErrorMessage("A group with same name already exists!");
	    	            return;
	    			}
	    		}
	    		
	    		System.out.println("Ricreo i Ruoli da aggiungere al gruppo");
	
	    		System.out.println("Ruoli da aggiungere al gruppo creati");
	    		
	    		List<RoleVO> role2add = new ArrayList<RoleVO>();
	    		for (int i = 0; i < menuSections; i++) {
	    			role2add.addAll(updateRoles(i));
				}
	    		
    			group.setRoles(role2add);
    			group = groupService.updateGroup(group);
	    		
	    		String newHtml = MakeHTML.makeHTML(group/*, "roles"*/);
				AppUserVO uu = userService.getUserByLogin(navigationBean.getUserName());
				auditService.addAudit(new Audit(uu.getLastName() + " " + uu.getFirstName()+" ("+navigationBean.getUserName()+")", "Update", "Group", group.getId(), "", "", oldHtml, newHtml));

	        	addInfoMessage("Gruppo aggiornato!");
	        	System.out.println("Group updated!");
	        	presetRoles(group);
				return;
	    	}
	    	else {
	    		System.out.println("Gruppo non trovato!");
	    		addErrorMessage("Error updating group!");
	            return;
	    	}
    	} catch (AccessDeniedException e1) {
    		e1.printStackTrace();
    		addErrorMessage("No enought permission to modify this group!", e1);
    		return;
		} catch (Exception e1) {
    		e1.printStackTrace();
    		addErrorMessage("Error: ", e1);
    		return;
		}
    }
    
    public void addAsNew(ActionEvent e) throws java.io.IOException {
    	
    	System.out.println("Inserisco il nuovo gruppo!");

    	// Controllo che non esiste un gruppo con lo stesso nome
    	
    	if (groupService.existGroupName(group.getName())) {
    		addErrorMessage("A group with same name already exists!");
    		return;
    	}
    	
    	group.setId(null);
    	
    	try {
    		System.out.println("Creo i Ruoli da aggiungere al gruppo");

    		System.out.println("Ruoli da aggiungere al gruppo creati");
    		
    		List<RoleVO> role2add = new ArrayList<RoleVO>();
    		for (int i = 0; i < menuSections; i++) {
    			role2add.addAll(updateRoles(i));
			}
    		group.setRoles(role2add);
    		group = groupService.createGroup(group);
    		
    		String newHtml = MakeHTML.makeHTML(group/*, "roles"*/);
			AppUserVO uu = userService.getUserByLogin(navigationBean.getUserName());
			auditService.addAudit(new Audit(uu.getLastName() + " " + uu.getFirstName()+" ("+navigationBean.getUserName()+")", "Create", "Group", group.getId(), "", "", null, newHtml));
			presetRoles(group);
	
    	} catch (AccessDeniedException e1) {
    		e1.printStackTrace();
			 addErrorMessage("No enought permission to create the group!", e1);
			 return;
		} catch (Exception ex) {
			ex.printStackTrace();
			 addErrorMessage("Error: ", ex);
			 return;
		}
    	
    	addInfoMessage("Group created!");
        return;
    }
    
    
    private List<RoleVO> updateRoles(int sezione){
    	
		List<RoleVO> roles2add = getManageTriStateRoles()[sezione].decodeRoles(getSelectedOptionsTriStateRole()[sezione]);
		List<RoleVO> roleComplete = new ArrayList<RoleVO>(); 
		List<RoleVO> dummy = new ArrayList<RoleVO>();
		
		for (RoleVO roleVO : roles2add) {
			dummy = roleComplete.stream()
					.filter(item -> item.getId().equals(roleVO.getId()))
					.collect(Collectors.toList());
			if (dummy.isEmpty()) {
				roleComplete.add(roleVO);
			}
			if (enableRoleMasterSlave) {
				System.out.println("Ricreo i Ruoli correlati ai Master da aggiungere al gruppo");
    			for (RoleMasterSlave roleMS : roleMasterSlaveService.getRoleSlaveByIdRoleMaster(roleVO.getId())) {
    				RoleVO itemRoleVO = roleService.findRoleById(roleMS.getSlaveRole().getId().toString());
    				dummy = roleComplete.stream()
    						.filter(item -> item.getId().equals(itemRoleVO.getId()))
    						.collect(Collectors.toList());
    				if (dummy.isEmpty()) {
    					roleComplete.add(itemRoleVO);
    				}
    			}
			}
		}
		return roleComplete;
    }    
    
    //******************* SELECT  RUOLI **********************//
    
    public List<Long> getSelectedRoles() {
    	
    	List<RoleVO> roles = group.getRoles();
    	List<Long> rolesId = new ArrayList<Long>();
    	
    	if (roles!=null) {
	    	for (RoleVO roleVO : roles) {
	    		rolesId.add(roleVO.getId());
			}
    	}
    	return rolesId;
    }
    
    
    public void setSelectedRoles(List<String> groupsId) {
    	
    	List<RoleVO> groups = new ArrayList<RoleVO>();
    	if (groupsId!=null) {
    		for (String long1 : groupsId) {
    			System.out.println("Ruoli selezionati: " + long1);
				RoleVO role = roleService.findRoleById(long1);
				groups.add(role);
			}
    	}
    	group.setRoles(groups);
    }
    
    public SelectItem[] getAllAvailableRoles() {
    	
    	List<RoleVO> roles = roleService.findAllRoles();
    	SelectItem[] availables = new SelectItem[roles.size()];
    	
    	int i=0;
    	for (RoleVO roleVO : roles) {
    		availables[i] = new SelectItem(roleVO.getId(), roleVO.getDescrizioneBreve(), roleVO.getDescrizione()+" (" + roleVO.getNome() + ")");
    		i++;
		}
    	return availables;
    }
    
    
    //************************************************************//
    
    //******************* TRI SELECT  RUOLI **********************//
    
    private void initRoles() {

    	
    	for (int i = 0; i < menuSections ; i++) {
//        	Hash delle <label, key> da visualizzare
    		optionRoles[i] = new HashMap<String, String>();	
//        	Hash delle <key, value> associate alle checkbox  
        	selectedOptionsTriStateRole[i] = new HashMap<String, String>();
//          Hash  <key, TriStatesvalue> associate alle checkbox
        	workingGroupRoles[i] = new HashMap<String, TriStatesRole>();    
        	
//        	Prende la lista completa dei ruoli disponibili in Read e Write per costruire il TriState di ognuno, 
//        	i ruoli nel db associati al gruppo e la struttura Hash <key, TriStatesvalue> associate alle checkbox
        	manageTriStateRoles[i] = new ManageTriStateRoles(roleService.findRoleBySection(i+1));    		
		}
    }
    
    
    private void presetRoles(GroupVO _group) {
////    	Prende la lista completa dei ruoli nel db associati al gruppo e la struttura Hash <key, TriStatesvalue> associate alle checkbox
//    	getManageTriStateRoles().buildWorkingRoles(_group.getRoles());
////    	Restituisce la lista <key,value> degli elementi già selezionati nel DB
//    	setSelectedOptionsTriStateRole(getManageTriStateRoles().getSelectedOptionsTriStateRole());
////    	Restituisce l'elenco label delle checkbox
//    	setOptionRoles(getManageTriStateRoles().getOptionRoles());
////    	Restituisce le <key, TriStatesvalue> associate alle checkbox
//    	setWorkingGroupRoles(getManageTriStateRoles().getWorkingRoles());
    	if (_group==null)	_group.setRoles(new ArrayList<RoleVO>());
    	for (int i = 0; i < menuSections ; i++) {
    		manageTriStateRoles[i].buildWorkingRoles(_group.getRolesBySezione(i+1));
    		selectedOptionsTriStateRole[i]=getManageTriStateRoles()[i].getSelectedOptionsTriStateRole();
    		optionRoles[i]=getManageTriStateRoles()[i].getOptionRoles();
    		workingGroupRoles[i]=getManageTriStateRoles()[i].getWorkingRoles();
    	}
    }

    
    public void manageRelatedRoles() {
    	
    }

//	public Map<String, String > getSelectedOptionsTriStateRole() {
//		return this.selectedOptionsTriStateRole;
//	}
//
//	public void setSelectedOptionsTriStateRole(final Map<String, String> selectedOptionsTriStateRole) {
//		this.selectedOptionsTriStateRole = selectedOptionsTriStateRole;
//	}
//
//	public Map<String,String> getOptionRoles() {
//		return this.optionRoles;
//	}
//
//	public void setOptionRoles(final Map<String,String> optionRoles) {
//		this.optionRoles = optionRoles;
//	}
	
    //************************************************************//

	public GroupVO getGroup() {
		return group;
	}

	public void setGroup(GroupVO group) {
		this.group = group;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public Map<String, String>[] getSelectedOptionsTriStateRole() {
		return selectedOptionsTriStateRole;
	}

	public void setSelectedOptionsTriStateRole(Map<String, String>[] selectedOptionsTriStateRole) {
		this.selectedOptionsTriStateRole = selectedOptionsTriStateRole;
	}

	public Map<String, String>[] getOptionRoles() {
		return optionRoles;
	}

	public void setOptionRoles(Map<String, String>[] optionRoles) {
		this.optionRoles = optionRoles;
	}

	public Map<String, TriStatesRole>[] getWorkingGroupRoles() {
		return workingGroupRoles;
	}

	public void setWorkingGroupRoles(Map<String, TriStatesRole>[] workingGroupRoles) {
		this.workingGroupRoles = workingGroupRoles;
	}

	public ManageTriStateRoles[] getManageTriStateRoles() {
		return manageTriStateRoles;
	}

	public void setManageTriStateRoles(ManageTriStateRoles[] manageTriStateRoles) {
		this.manageTriStateRoles = manageTriStateRoles;
	}
}