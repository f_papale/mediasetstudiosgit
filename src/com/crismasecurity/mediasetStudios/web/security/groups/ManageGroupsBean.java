package com.crismasecurity.mediasetStudios.web.security.groups;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;

import com.crismasecurity.mediasetStudios.core.entity.Audit;
import com.crismasecurity.mediasetStudios.core.security.services.AppUserService;
import com.crismasecurity.mediasetStudios.core.security.services.GroupService;
import com.crismasecurity.mediasetStudios.core.security.vos.security.AppUserVO;
import com.crismasecurity.mediasetStudios.core.security.vos.security.GroupVO;
import com.crismasecurity.mediasetStudios.core.services.AuditService;
import com.crismasecurity.mediasetStudios.web.NavigationBean;
import com.crismasecurity.mediasetStudios.web.utils.MakeHTML;
import com.crismasecurity.mediasetStudios.web.utils.SortableList;


//@ManagedBean(name= "manageGroupsBean") // JSF managed
//@SessionScoped // JSF managed
//@ViewScoped // JSF managed

//Spring managed
@Controller ("manageGroupsBean") 
//@Scope("session")
@Scope("view") 
public class ManageGroupsBean extends SortableList<GroupVO> {

	//Serial
	private static final long serialVersionUID = 2101175133673172947L;


	@Autowired
	private GroupService groupService;
	
	@Autowired
	private NavigationBean navigationBean;
	
	@Autowired
	private AuditService auditService;
	
	@Autowired
	private AppUserService userService;
	
//	@Autowired
//	private ModifyGroupBean modifyGroupBean;
	
	
	private String nameFilter;
	
	private boolean showDelete;
	
	
    /**
     * default empty constructor
     */
    public ManageGroupsBean() {
    	super("name");
    	showDelete = false;
    }

//    /**
//     * Method that is backed to a submit button of a form.
//     */
//    public void modify(ActionEvent e) {
////    	FacesContext context = FacesContext.getCurrentInstance();
////    	Map<String, String> map = context.getExternalContext().getRequestParameterMap();
////    	String groupId= (String) map.get("groupId");
//    	
//    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
//    	Long groupId = null;
//    	for (UIComponent com : component.getChildren()) {
//    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("groupId")) {
//    			groupId = ((Long)((UIParameter)com).getValue());
//			}
//		}
//    	
//    	modifyGroupBean.modifyGroup(groupId+"");
////		return "modify_group";
//    }
    
    private String groupToDelete = null;
    public void confirmDelete(ActionEvent e) {
//    	FacesContext context = FacesContext.getCurrentInstance();
//    	Map<String, String> map = context.getExternalContext().getRequestParameterMap();
//    	groupToDelete= (String) map.get("groupId");
    	
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("groupId")) {
    			groupToDelete = ((Long)((UIParameter)com).getValue())+"";
			}
		}
    	
//    	QLogger.debug("\n\n\n!!!!!!!!!!! Richiesta conferma per eliminazione gruppo " + groupToDelete);
    	
    	showDelete = true;
//    	return "";
    }
    public void cancelDelete(ActionEvent ev) {
    	System.out.println("\nDelete Group canceled.");
    	groupToDelete= null;
    	showDelete = false;
    }
    
    public String newGroup() {

//    	modifyGroupBean.newGroup();
		return "modify_group";
    }
    
    public void delete(ActionEvent ev) {
//      FacesContext context = FacesContext.getCurrentInstance();
//    	Map<String, String> map = context.getExternalContext().getRequestParameterMap();
//    	groupToDelete= (String) map.get("groupId");
    	
    	try {
    		GroupVO grToDelete = groupService.loadGroup(Long.decode(groupToDelete));

			String oldHtml = null;
			if (grToDelete!=null) {
				oldHtml = MakeHTML.makeHTML(grToDelete/*, "roles"*/);
				
			}
    		
    		if (grToDelete.getUsersId()!=null && grToDelete.getUsersId().size()>0) {
	   			 FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Unable to delete non void Group ("+grToDelete.getUsersId().size()+" members present)!", "Unable to delete non void Group ("+grToDelete.getUsersId().size()+" members present)!"));
	   			 addErrorMessage("Unable to delete non void Group ("+grToDelete.getUsersId().size()+" members present)!");
	   			showDelete = false;
				 return;
    		}
    		
    		
    		groupService.deleteGroup(grToDelete);
    		
    		AppUserVO uu = userService.getUserByLogin(navigationBean.getUserName());
			auditService.addAudit(new Audit(uu.getLastName() + " " + uu.getFirstName()+" ("+navigationBean.getUserName()+")", "Delete", "Group", grToDelete.getId(), "", "", oldHtml, null));
        	
			
    	} catch (AccessDeniedException e) {
			 FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Insufficient permission to delete Group.", "Insufficient permission to delete Group."));
			 addErrorMessage("Insufficient permission to delete Group.", e);
			 showDelete = false;
			 return;
		}
    	catch (Exception e) {
			 addErrorMessage("Error: ", e);
			 showDelete = false;
			 return;
		}
    	
    	showDelete = false;
    	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Group deleted.", "Group deleted."));
    	addDebugMessage("Group deleted.");
    }
    

	public List<GroupVO> getGroups() {
		
		List<GroupVO> groups = null;
		
		Map<String, Object> args = new HashMap<String, Object>();
		
		if (nameFilter!=null && !nameFilter.equals("")) {
			args.put("name", nameFilter);
		}
		
		if (!args.isEmpty()) groups = groupService.findByFields(args);
		else groups = groupService.findAllGroups();
		
		sort(groups);
		//System.out.println("Ordino per colonna: " + sortColumnName + "   ascendente? :" + ascending);
		
		return groups;
	}

	/**
     * Determines the sortColumnName order.
     *
     * @param   sortColumn to sortColumnName by.
     * @return  whether sortColumnName order is ascending or descending.
     */
    protected boolean isDefaultAscending(String sortColumn) {
        return true;
    }

    /**
     *  Sorts the list of users.
     */
    protected void sort(List<GroupVO> groups) {
        Comparator<GroupVO> comparator = new Comparator<GroupVO>() {
            public int compare(GroupVO u1, GroupVO u2) {
                if (sortColumnName == null) {
                    return 0;
                }
                if (sortColumnName.equals("id")) {
                    return ascending ?
                            u1.getId().compareTo(u2.getId()) :
                            u2.getId().compareTo(u1.getId());
                } else if (sortColumnName.equals("name")) {
                    return ascending ? u1.getName().compareTo(u2.getName()) :
                            u2.getName().compareTo(u1.getName());
                } else if (sortColumnName.equals("numeroUtenti")) {
                    return ascending ? Integer.valueOf(u1.getNumeroUtenti()).compareTo(Integer.valueOf(u2.getNumeroUtenti())) :
                    	Integer.valueOf(u2.getNumeroUtenti()).compareTo(Integer.valueOf(u1.getNumeroUtenti()));
                } else return 0;
            }
        };
        Collections.sort(groups, comparator);
    }

    
    
	public String search() {
		return "";
	}
	
	public String getNameFilter() {
		return nameFilter;
	}

	public void setNameFilter(String nameFilter) {
		this.nameFilter = nameFilter;
	}

	public boolean isShowDelete() {
		return showDelete;
	}

	public void setShowDelete(boolean showDelete) {
		this.showDelete = showDelete;
	}

	public String getGroupToDelete() {
		return groupToDelete;
	}

	public void setGroupToDelete(String groupToDelete) {
		this.groupToDelete = groupToDelete;
	}

}