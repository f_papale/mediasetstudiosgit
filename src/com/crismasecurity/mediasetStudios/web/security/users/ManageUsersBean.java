package com.crismasecurity.mediasetStudios.web.security.users;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import com.crismasecurity.mediasetStudios.core.entity.AgencyUser;
import com.crismasecurity.mediasetStudios.core.entity.Audit;
import com.crismasecurity.mediasetStudios.core.entity.Spectator;
import com.crismasecurity.mediasetStudios.core.security.services.AppUserService;
import com.crismasecurity.mediasetStudios.core.security.services.GroupService;
import com.crismasecurity.mediasetStudios.core.security.vos.security.AppUserVO;
import com.crismasecurity.mediasetStudios.core.security.vos.security.GroupVO;
import com.crismasecurity.mediasetStudios.core.services.AgencyUserService;
import com.crismasecurity.mediasetStudios.core.services.AuditService;
import com.crismasecurity.mediasetStudios.web.LoginBean;
import com.crismasecurity.mediasetStudios.web.NavigationBean;
import com.crismasecurity.mediasetStudios.web.utils.MakeHTML;
import com.crismasecurity.mediasetStudios.web.utils.SortableList;



//@ManagedBean(name= "manageUsersBean") // JSF managed
//@SessionScoped // JSF managed
//@ViewScoped // JSF managed

//Spring managed
@Controller ("manageUsersBean") 
//@Scope("session")
@Scope("view")
public class ManageUsersBean extends SortableList<AppUserVO> {

	//Serial
	private static final long serialVersionUID = 1551638261142138261L;

	@Autowired
	private AppUserService userService;
	
	@Autowired
	private GroupService groupService;
	
	@Autowired
	private AgencyUserService agencyUserService;
	
	
	@Autowired
	private NavigationBean navigationBean;
	
	@Autowired
	private AuditService auditService;
	
	
//	@Autowired
//	private ModifyUserBean modifyUserBean;
	
	private String nameFilter;
	private String surnameFilter;
	private String loginFilter;
	
	private Long groupFilter;
	
	private boolean showDelete;
	
    /**
     * default empty constructor
     */
    public ManageUsersBean() {
    	super("firstName");
    	showDelete = false;
    }

//    /**
//     * Method that is backed to a submit button of a form.
//     */
//    public void modify(ActionEvent e) {
////    	FacesContext context = FacesContext.getCurrentInstance();
////    	Map<String, String> map = context.getExternalContext().getRequestParameterMap();
////    	String userId= (String) map.get("userId");
//    	
//    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
//    	Long userId = null;
//    	for (UIComponent com : component.getChildren()) {
//    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("userId")) {
//    			userId = ((Long)((UIParameter)com).getValue());
//			}
//		}
//    	
//    	
//    	modifyUserBean.modifyUser(userId+"");
////		return "modify_user";
//    }
    
    private String userToDelete = null;
    public void confirmDelete(ActionEvent e) {
//    	FacesContext context = FacesContext.getCurrentInstance();
//    	Map<String, String> map = context.getExternalContext().getRequestParameterMap();
//    	userToDelete= (String) map.get("userId");
    	
    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
    	for (UIComponent com : component.getChildren()) {
    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("userId")) {
    			userToDelete = ((Long)((UIParameter)com).getValue())+"";
			}
		}
    	
    //	QLogger.debug("\n\n\n!!!!!!!!!!! Richiesta conferma per eliminazione utente " + userToDelete);    	
    	showDelete = true;
//    	return "";
    }
    public void cancelDelete(ActionEvent ev) {
    	System.out.println("\nDelete User canceled.");
    	userToDelete= null;
    	showDelete = false;
    }
    
    
    public String newUser() {

//    	modifyUserBean.newUser();
		return "modify_user";
    }
    
    public void delete(ActionEvent ev) {

//    	FacesContext context = FacesContext.getCurrentInstance();
//    	Map<String, String> map = context.getExternalContext().getRequestParameterMap();
//    	userToDelete= (String) map.get("userId");
    	
    	System.out.println("Deleting user " + userToDelete);
    	try {
    		
    		AppUserVO oldSpect = userService.loadUser(userToDelete);
			String oldHtml = null;
			if (oldSpect!=null) {
				oldHtml = MakeHTML.makeHTML(oldSpect, "groups");
				
			}
    		
    		List<AgencyUser> list = agencyUserService.getAgencyUserFinder().and("user.id").eq(Long.valueOf(userToDelete)).list();
			if(list.size() > 0) {
				for(AgencyUser au : list) {
					agencyUserService.deleteAgencyUser(au.getId());
				}
			}
    		userService.deleteUser(userService.loadUser(userToDelete));
    		

			AppUserVO uu = userService.getUserByLogin(navigationBean.getUserName());
			auditService.addAudit(new Audit(uu.getLastName() + " " + uu.getFirstName()+" ("+navigationBean.getUserName()+")", "Delete", "User", oldSpect.getId(), "", "", oldHtml, null));
        		        	
    		
    		
    	} catch (AccessDeniedException e) {
			 FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Insufficient privileges to delete user.", "Insufficient privileges to delete user."));
			 addErrorMessage("Insufficient privileges to delete user.", e);
			 return;
		}
    	catch (Exception e) {
			 addErrorMessage("Delete user: ", e);
			 return;
		}
    	
    	showDelete = false;
    	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "User erased.", "User erased."));
    	addDebugMessage("User erased.");
    }
    

	public List<AppUserVO> getUsers() {
		
		List<AppUserVO> users = null;
		Map<String, Object> args = new HashMap<String, Object>();
		
		if (loginFilter!=null && !loginFilter.equals("")) {
			args.put("login", loginFilter);
		}
		if (nameFilter!=null && !nameFilter.equals("")) {
			args.put("firstName", nameFilter);
		}
		if (surnameFilter!=null && !surnameFilter.equals("")) {
			args.put("lastName", surnameFilter);
		}		
		if (groupFilter!=null && groupFilter.longValue()>0) {
			args.put("groups.id", groupFilter);
		}
		
		if (!args.isEmpty()) users = userService.findByFields(args);
		else users = userService.findAllUsers();
		
		sort(users);
		//System.out.println("Oreder by column: " + sortColumnName + "   ascending? :" + ascending);
		
		return users;
	}

	
	
	/**
     * Determines the sortColumnName order.
     *
     * @param   sortColumn to sortColumnName by.
     * @return  whether sortColumnName order is ascending or descending.
     */
    protected boolean isDefaultAscending(String sortColumn) {
        return true;
    }

    /**
     *  Sorts the list of users.
     */
    protected void sort(List<AppUserVO> users) {
        Comparator<AppUserVO> comparator = new Comparator<AppUserVO>() {
            public int compare(AppUserVO u1, AppUserVO u2) {
                if (sortColumnName == null) {
                    return 0;
                }
                if (sortColumnName.equals("id")) {
                    return ascending ?
                            u1.getId().compareTo(u2.getId()) :
                            u2.getId().compareTo(u1.getId());
                } else if (sortColumnName.equals("firstName")) {
                    return ascending ? u1.getFirstName().compareTo(u2.getFirstName()) :
                            u2.getFirstName().compareTo(u1.getFirstName());
                } else if (sortColumnName.equals("lastName")) {
                    return ascending ? u1.getLastName().compareTo(u2.getLastName()) :
                            u2.getLastName().compareTo(u1.getLastName());
                } else if (sortColumnName.equals("login")) {
                    return ascending ? u1.getLogin().compareTo(u2.getLogin()) :
                            u2.getLogin().compareTo(u1.getLogin());
                } else if (sortColumnName.equals("groups")) {
                    return ascending ? u1.getGroupsNames().compareTo(u2.getGroupsNames()) :
                            u2.getGroupsNames().compareTo(u1.getGroupsNames());
                } else return 0;
            }
        };
        Collections.sort(users, comparator);
    }

    
    
    
	public javax.faces.model.SelectItem[] getGruppiItemsList() {
    	
		List<GroupVO> groups = groupService.findAllGroups();
		if (groups==null || groups.size()==0) return null;
	    javax.faces.model.SelectItem[] availables = new javax.faces.model.SelectItem[groups.size()+1];
	    availables[0] = new javax.faces.model.SelectItem("", "", "");
	    int i=1;
	    for (GroupVO g : groups) {
	    	availables[i] = new javax.faces.model.SelectItem(g.getId(), g.getName(), g.getName());
	    	i++;
		}
	    return availables;
    }
	
	
	public String search() {
		return "";
	}
	
	public String getNameFilter() {
		return nameFilter;
	}

	public void setNameFilter(String nameFilter) {
		this.nameFilter = nameFilter;
	}

	public Long getGroupFilter() {
		return groupFilter;
	}

	public void setGroupFilter(Long groupFilter) {
		this.groupFilter = groupFilter;
	}

	public boolean isShowDelete() {
		return showDelete;
	}

	public void setShowDelete(boolean showDelete) {
		this.showDelete = showDelete;
	}

	public String getUserToDelete() {
		return userToDelete;
	}

	public void setUserToDelete(String userToDelete) {
		this.userToDelete = userToDelete;
	}

	public String getSurnameFilter() {
		return surnameFilter;
	}

	public void setSurnameFilter(String surnameFilter) {
		this.surnameFilter = surnameFilter;
	}

	public String getLoginFilter() {
		return loginFilter;
	}

	public void setLoginFilter(String loginFilter) {
		this.loginFilter = loginFilter;
	}


}