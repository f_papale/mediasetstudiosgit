package com.crismasecurity.mediasetStudios.web.security.users;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import com.crismasecurity.mediasetStudios.core.entity.Agency;


@FacesConverter("agencyUserConverter")
public class AgencyUserConverter implements Converter {

	public Object getAsObject(FacesContext fc, UIComponent component, String value) {
		System.out.println("init converter AgencyUserConverter " + value);
        if(value != null && value.trim().length() > 0) {
            try {
            	@SuppressWarnings("deprecation")
            	ModifyUserBean bean = (ModifyUserBean) fc.getApplication().createValueBinding("#{modifyUserBean}").getValue(fc);
            	return bean.getAgencyById((value!=null?Long.parseLong(value):null));
            } catch(NumberFormatException e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid Agency."));
            }
        }
        else {
        	System.out.println("errore AgencyUserConverter " + value);
            return null;
        }
	}

	public String getAsString(FacesContext fc, UIComponent component, Object obj) {
		System.out.println("getAsString:"+ obj);
        if(obj != null) {
            return String.valueOf(((Agency) obj).getIdAgency());
        }
        else {
            return null;
        }
	}

}
