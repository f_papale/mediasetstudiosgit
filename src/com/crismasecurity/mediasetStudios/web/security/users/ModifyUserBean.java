package com.crismasecurity.mediasetStudios.web.security.users;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.DualListModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;

import com.crismasecurity.mediasetStudios.core.entity.Agency;
import com.crismasecurity.mediasetStudios.core.entity.AgencyUser;
import com.crismasecurity.mediasetStudios.core.entity.AgencyUserId;
import com.crismasecurity.mediasetStudios.core.entity.Audit;
import com.crismasecurity.mediasetStudios.core.entity.DistributionList;
import com.crismasecurity.mediasetStudios.core.security.dao.AppUserDAO;
import com.crismasecurity.mediasetStudios.core.security.services.AppUserService;
import com.crismasecurity.mediasetStudios.core.security.services.GroupService;
import com.crismasecurity.mediasetStudios.core.security.services.RoleService;
import com.crismasecurity.mediasetStudios.core.security.vos.security.AppUserVO;
import com.crismasecurity.mediasetStudios.core.security.vos.security.GroupVO;
import com.crismasecurity.mediasetStudios.core.security.vos.security.RoleVO;
import com.crismasecurity.mediasetStudios.core.services.AgencyService;
import com.crismasecurity.mediasetStudios.core.services.AgencyUserService;
import com.crismasecurity.mediasetStudios.core.services.AuditService;
import com.crismasecurity.mediasetStudios.core.utils.PropertiesUtils;
import com.crismasecurity.mediasetStudios.web.LoginBean;
import com.crismasecurity.mediasetStudios.web.NavigationBean;
import com.crismasecurity.mediasetStudios.web.utils.MakeHTML;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;


//@ManagedBean(name= "modifyUserBean") // JSF managed
//@SessionScoped // JSF managed
//@ViewScoped // JSF managed

//Spring managed
@Controller ("modifyUserBean") 
//@Scope("session")
@Scope("view")
public class ModifyUserBean extends BaseBean{

	//serial
	private static final long serialVersionUID = -7740960700676478291L;

	@Autowired
	private AppUserDAO appUserDao;
	
	@Autowired
	private AppUserService userService;
	
	@Autowired
	private GroupService groupService;

	@Autowired
	private RoleService roleService;
	
	@Autowired
	private AgencyService agencyService;
	
	@Autowired
	private AgencyUserService agencyUserService;
	
	@Autowired
	private NavigationBean navigationBean;
	
	@Autowired
	private AuditService auditService;

	private AppUserVO user;
	
	private String userId;
	
	private boolean adminRole = false;

	private boolean flgAddRelatedAgencies=false, flgShowHiddenAgencies=false;
	
	private DualListModel<Agency> agencyListes;
    private List<Agency> sourceAgencyList;
    private List<Agency> targetAgencyList;
	
    /**
     * default empty constructor
     */
    
    public ModifyUserBean() {
    	System.out.println("Costruttore ModifyUserBean!!!!!!!!!!!!!!!!!!!!");
    	flgAddRelatedAgencies = Boolean.parseBoolean(PropertiesUtils.getInstance().readProperty("user.addrelatedagencies","false"));

//    	user = new AppUserVO();
    }
    
	@PostConstruct
	public void init() {
    	flgShowHiddenAgencies = Boolean.parseBoolean(PropertiesUtils.getInstance().readProperty("unwanted.showHiddenAgencies","false"));	
	}
    
    public void addOrUpdateUser(ActionEvent e) throws java.io.IOException {
    	System.out.println("Sono entrato dentro addOrUpdateUser");
    	if(user == null || user.getId() == null) {
    		this.addAsNew();
    	} else {
    		this.updateUser();
    	}
    }
    
    public void modifyUser() {
    	if (user!=null) return;
    	System.out.println("modifyUser - userId:" + userId);
    	AppUserVO usvo = null;
    	if (userId!=null && !userId.equals(""))  usvo = userService.loadUser(userId);
    	if (usvo!=null) {
    		user=usvo;
    		checkIsAdmin(user);
    	}
    	else {
    		user = new AppUserVO();
    		checkIsAdmin(user);
    	}
    	presetAgencyList(user.getId());
    	
    	System.out.println("L'utente ha id:" + user.getId());
    }
    

    protected void newUser() {
    	user = new AppUserVO();
    	checkIsAdmin(user);
    	presetAgencyList(user.getId());
    }
    
    private void checkIsAdmin(AppUserVO user) {
    	
    	if (!((user==null) || (user.getId()==null))) {
    		try {
            	List<RoleVO> rl = new ArrayList<RoleVO>();
            	user.getGroups().forEach(x -> rl.addAll(x.getRoles()));
            	Set<RoleVO> hSet = new HashSet<RoleVO>(rl); 
//                hSet.addAll(rl);
            	hSet.forEach(x -> setAdminRole(isAdminRole() || (x.getNome().toUpperCase().equals("ROLE_ADMIN")) || (x.getNome().toUpperCase().equals("ROLE_STADMIN"))
            			));				
			} catch (Exception e) {
				setAdminRole(false);
			}
        	System.out.println("is Admin Role = " +isAdminRole());
    	}
    }
    
	public void agSelected(SelectEvent event) {
		System.out.println("Sono entrato nel listener: ");
	}
    
    
    /**
     * Method that is backed to a submit button of a form.
     */
//    public void updateUser(ActionEvent e) throws java.io.IOException {
    public String updateUser() throws java.io.IOException {	
    	System.out.println("Aggiorno l'utente con id:" + user.getId() + " ");
    	
    	if (user.getId()==null || (user.getId()!=null && user.getId()<1)) {
    		System.out.println("L'utente non ha l'id valorizzato!");
//    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Non posso aggiornare un utente non ancora inserito!", "Non posso aggiornare un utente non ancora inserito!"));
    		addErrorMessage("Can not update a user not yet created!");
            return ("");
    	}
   	
    	
    	try {
    		System.out.println("Carico l'utente con quell'id!");
	    	AppUserVO userL = userService.loadUser(user.getId().toString());
	    	
	    	
			String oldHtml = null;
			if (userL!=null) {
				oldHtml = MakeHTML.makeHTML(userL, "groups");
			}
	    	
	    	if (userL!=null) {
	    		if (!userL.getLogin().equals(user.getLogin())) {
	    			// Controllo che non vi sia un'altro utente con lo stesso login
	    			System.out.println("Login cambiata, controllo se esiste un'altro utente con la stessa login!");
	    			if (userService.existUser(user)) {
//	    				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Esiste gi� un utente con lo stesso login!", "Esiste gi� un utente con lo stesso login!"));
	    				addErrorMessage("A user with same login already exists!");
	    	            return ("");
	    			}
	    		}
	    		
	    		
	    		if (user.getNewPassword()!=null  && !user.getNewPassword().equals("") && !LoginBean.isPasswordComplexityOk(user.getNewPassword())) {
	    			addWarningMessage("Le password NON rispetta le regole di complessità (almeno un numero, almeno una lettera maiuscola, almeno una lettera minuscola, almeno un carattere speciale tra @#%$^, nessuno spazio consentito e lunghezza di almeno 8 caratteri)!");
	    			return ("");
	    		}
	    		
	    		
	    		System.out.println("Effettuo update...");
	    		checkIsAdmin(user);
	    		if (isAdminRole()==true) user.setDisabled(false);;
	    		 
	    		user = userService.updateUser(user);
	    		
	    		if (user.getGroups()!=null) System.out.println("Dopo aggiornamento gruppi:" + user.getGroups().size());
	    		else System.out.println("Dopo aggiornamento gruppi:0");
	    			        	
    			for (AgencyUser agencyUserItem : agencyUserService.getAgencyUserFinder().and("user.id").eq(user.getId()).setDistinct().list()) {
    				agencyUserService.deleteAgencyUser(agencyUserItem.getId());
    			}
    			
	    		if (isSelectedAgenciesGroup() && getAgencyListes().getTarget().size()>0) {
	    			boolean flgCurrentSelected=true;
	        		for (Agency agencyListItem : getAgencyListes().getTarget()) {
	        			AgencyUser agencyUser = new AgencyUser(agencyListItem,appUserDao.get(user.getId()));
	        			agencyUser.setDescription(null);
	        			agencyUser.setFlgCurrentSelected(flgCurrentSelected);
	        			flgCurrentSelected=false;
	        			agencyUserService.updateAgencyUser(agencyUser);
	    			}
	    		
		        	if(flgCurrentSelected) {
		    			addErrorMessage("Please select an agency!");
		    			return ("");
		        	}
	    		}   			
				String newHtml = MakeHTML.makeHTML(user, "groups");
				AppUserVO uu = userService.getUserByLogin(navigationBean.getUserName());
				auditService.addAudit(new Audit(uu.getLastName() + " " + uu.getFirstName()+" ("+navigationBean.getUserName()+")", "Update", "User", user.getId(), "", "", oldHtml, newHtml));
	        		        	   			
	        	addInfoMessage("User updated!");
	        	System.out.println("Update effettuato!");
				return ("users_management");
	    	}
	    	else {
	    		System.out.println("Utente non trovato!");
//	    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Errore nell'aggiornamento utente non trovato!", "Errore nell'aggiornamento utente non trovato!"));
	    		addErrorMessage("Error updating user!");
	            return ("");
	    	}
    	} catch (AccessDeniedException e1) {
    		e1.printStackTrace();
//    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Non hai i permessi per modificare questo Utente!", "Non hai i permessi per modificare questo Utente!"));
    		addErrorMessage("You are not allowed to modify this user!", e1);
    		return ("");
		} catch (Exception ex) {
    		ex.printStackTrace();
//    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Non hai i permessi per modificare questo Utente!", "Non hai i permessi per modificare questo Utente!"));
    		addErrorMessage("You are not allowed to modify this user-agency record!", ex);
    		return ("");
		}
    }
//  public void addAsNew(ActionEvent e) throws java.io.IOException {  
    public String addAsNew() throws java.io.IOException {
    	
    	System.out.println("Inserisco il nuovo utente!");

    	// Controllo se esiste un utente con lo stessa login
    	if (userService.existUser(user)) {
//    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Esiste gi� un utente con la stessa login!", "Esiste gi� un utente con la stessa login!"));
    		addErrorMessage("A user with same login already exists!");
    		return ("");
    	}
//    	if(this.isSelectedAgenciesGroup() ) {
//			addErrorMessage("Please select an agency!");
//			return ("");
//    	}
    	
    	
    	if ((!user.isAdfsUser() && !user.isLdapUser()) && (user.getNewPassword()==null || (user.getNewPassword()!=null && (user.getNewPassword().equals("") || !LoginBean.isPasswordComplexityOk(user.getNewPassword()))))) {
			addWarningMessage("Le password NON rispetta le regole di complessità (almeno un numero, almeno una lettera maiuscola, almeno una lettera minuscola, almeno un carattere speciale tra @#%$^, nessuno spazio consentito e lunghezza di almeno 8 caratteri)!");
			return ("");
		}
    	
    	user.setId(null);
    	System.out.println(user);
    	try {
    		user = userService.createUser(user);
			for (AgencyUser agencyUserItem : agencyUserService.getAgencyUserFinder().and("user.id").eq(user.getId()).setDistinct().list()) {
				agencyUserService.deleteAgencyUser(agencyUserItem.getId());
			}    		
    		if (isSelectedAgenciesGroup() && getAgencyListes().getTarget().size()>0) {
    			boolean flgCurrentSelected=true;
        		for (Agency agencyListItem : getAgencyListes().getTarget()) {
        			AgencyUser agencyUser = new AgencyUser(agencyListItem,appUserDao.get(user.getId()));
        			agencyUser.setDescription(null);
        			agencyUser.setFlgCurrentSelected(flgCurrentSelected);
        			flgCurrentSelected=false;
        			agencyUserService.updateAgencyUser(agencyUser);
    			}
    		
	        	if(flgCurrentSelected) {
	    			addErrorMessage("Please select an agency!");
	    			return ("");
	        	}
    		}  	
    		
    		String newHtml = MakeHTML.makeHTML(user, "groups");
			AppUserVO uu = userService.getUserByLogin(navigationBean.getUserName());
			auditService.addAudit(new Audit(uu.getLastName() + " " + uu.getFirstName()+" ("+navigationBean.getUserName()+")", "Create", "User", user.getId(), "", "", null, newHtml));
        			        	
    		
    	} catch (AccessDeniedException e1) {
    		e1.printStackTrace();
//			 FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Non hai i permessi per inserire l'utente!", "Non hai i permessi per inserire l'utente!"));
			 addErrorMessage("You are not allowed to create a user!", e1);
			 return ("");
		} catch (Exception ex) {
			ex.printStackTrace();
			 addErrorMessage("Error: ", ex);
			 return ("");
		}

    	
//    	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Utente inserito!", "Utente inserito!"));
    	addInfoMessage("User created!");
    	
        return ("users_management");
    }
    
    private Boolean isSelectedRole(String sRole) {
    	List<Long> ids = getSelectedRoles();
    	List<RoleVO> roles = roleService.findAllRoles();
    	for (RoleVO roleVO : roles) {
    		if(ids.contains(roleVO.getId()) && roleVO.getNome().contains(sRole.toUpperCase())) {
    			return true;
    		}
		}
    	return false;
    }
    
    
    public Boolean isSelectedAgenciesGroup() {
    	return (isSelectedRole("ROLE_AGENCY"));
    }
    
    public Boolean isSelectedProductionsGroup() {
    	return (isSelectedRole("ROLE_PRODUCTION"));
    }
    

    //******************* SHUTLE **********************************//
    
    public List<Long> getSelectedGroups() {
    	
    	List<GroupVO> groups = user.getGroups();
    	List<Long> groupsId = new ArrayList<Long>();
    	
    	if (groups!=null) {
	    	for (GroupVO groupVO : groups) {
	    		groupsId.add(groupVO.getId());
			}
    	}
    	return groupsId;
    }
    
    private List<Long> getSelectedRoles() {
    	
    	List<GroupVO> groups = user.getGroups();
    	List<Long> rolesID = new ArrayList<Long>();
    	
    	if (groups!=null) {
	    	for (GroupVO groupVO : groups) {
	    		groupVO.getRoles();
	    		for (RoleVO roleVO : groupVO.getRoles())
	    			rolesID.add(roleVO.getId());
			}
    	}
    	return rolesID;
    }
//    
//    public Long getSelectedAgencies() {
//    	
//    	List<Agency> agencies = new ArrayList<Agency>();
//    	if(selectedAgencies != null  && selectedAgencies > -1) {
//    		if(agencyUserService.getAgencyUserById(new AgencyUserId(selectedAgencies,user.getId())) != null) {	
//    			agencies.add(agencyUserService.getAgencyUserById(new AgencyUserId(selectedAgencies,user.getId())).getAgency());
//    	
//    		}
//    	}
//    	Long agenciesId = null;
//    	
//    	if (agencies!=null) {
//	    	for (Agency agency : agencies) {
//	    		agenciesId = agency.getIdAgency();
//			}
//    	}
//
//    	return agenciesId;
//    }
//    
    
    public void setSelectedGroups(List<String> groupsId) {
    	
    	List<GroupVO> groups = new ArrayList<GroupVO>();
    	if (groupsId!=null) {
    		for (String long1 : groupsId) {
				System.out.println("Gruppi selezionati: " + long1);
				GroupVO gr = groupService.loadGroup(new Long(long1));
				groups.add(gr);
			}
    	}
    	user.setGroups(groups);
    }

 
    public SelectItem[] getAllAvailableAgencies() {
    	
    	List<Agency> agencies = agencyService.getAgency();
    	
    	List<SelectItem> availables = new ArrayList<SelectItem>();
    	
    	boolean getMultiAgencies = isSelectedProductionsGroup() && flgAddRelatedAgencies;
    	for (Agency agency : agencies) {
    		if (getMultiAgencies) {
        		if (agency.getAgencyType().isFlgProduction() && (!agency.isHidden()||flgShowHiddenAgencies))
        			availables.add(new SelectItem(agency.getIdAgency(), agency.getName()));    			
    		}
    		else
    			if (!agency.isHidden()||flgShowHiddenAgencies) {
    				availables.add(new SelectItem(agency.getIdAgency(), agency.getName()));	
    			}
		}
    	return availables.toArray(new SelectItem[availables.size()]);
    }
    
    
    public SelectItem[] getAllAvailableGroups() {
    	
    	List<GroupVO> groups = groupService.findAllGroups();
    	SelectItem[] availables = new SelectItem[groups.size()];
    	
    	int i=0;
    	for (GroupVO groupVO : groups) {
    		availables[i] = new SelectItem(groupVO.getId(), groupVO.getName());
    		i++;
		}
    	return availables;
    }
    
    //************************************************************//

    
	public AppUserVO getUser() {
		return user;
	}

	public void setUser(AppUserVO user) {
		this.user = user;
	}


	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}


	public boolean isAdminRole() {
		return adminRole;
	}


	public void setAdminRole(boolean adminRole) {
		this.adminRole = adminRole;
	}


	public Agency getAgencyById(Long idAgency) {
		return agencyService.getAgencyById(idAgency);
	}

	private void presetAgencyList(Long selectedUser) {
		this.targetAgencyList = new ArrayList<Agency>();
		this.agencyUserService.getAgencyUserFinder().and("user.id").eq(selectedUser).list().forEach(x -> this.targetAgencyList.add(x.getAgency()));
		this.sourceAgencyList=agencyService.getAgency(flgShowHiddenAgencies);
		this.sourceAgencyList.removeAll(this.targetAgencyList);
		setAgencyListes(new DualListModel<Agency>(this.sourceAgencyList, this.targetAgencyList));
	}


	public DualListModel<Agency> getAgencyListes() {
		return this.agencyListes;
	}


	public void setAgencyListes(DualListModel<Agency> agencyListes) {
		this.agencyListes = agencyListes;
	}



}