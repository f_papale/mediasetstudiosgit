package com.crismasecurity.mediasetStudios.web.security.roles;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;

import com.crismasecurity.mediasetStudios.core.security.services.RoleService;
import com.crismasecurity.mediasetStudios.core.security.vos.security.RoleVO;
import com.crismasecurity.mediasetStudios.core.utils.UserMessageAreaType;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;



//@ManagedBean(name= "modifyRoleBean") // JSF managed
//@SessionScoped // JSF managed
//@ViewScoped // JSF managed

//Spring managed
@Controller ("modifyRoleBean") 
//@Scope("session")
@Scope("view")
public class ModifyRoleBean extends BaseBean {

	//Serial
	private static final long serialVersionUID = 8351651415955390897L;

	@Autowired
	private RoleService roleService;

	private RoleVO role;
	
	private List<javax.faces.model.SelectItem> availablesAreaType;
	private List<String> selectedAreas;
	
	private String roleId;
	
	
    /**
     * default empty constructor
     */
    public ModifyRoleBean() {
    	role = new RoleVO();
    }

	@PostConstruct
	public void initCustomModel()  {
		System.out.println("INIT ModifyRoleBean");
		
		availablesAreaType = new ArrayList<javax.faces.model.SelectItem>();
		for (int i = 0; i < UserMessageAreaType.values().length; i++) {
			availablesAreaType.add(new javax.faces.model.SelectItem(UserMessageAreaType.values()[i].getName(), UserMessageAreaType.values()[i].getName()+" ("+UserMessageAreaType.values()[i].getDescription() + ")"));
		}
	}
	
	
	public void modifyRole() {
    	RoleVO vo = roleService.findRoleById(roleId);
    	if (vo!=null) role=vo;
    	else role = new RoleVO();

    	selectedAreas = new ArrayList<String>();
    	for (UserMessageAreaType ar : role.getInterestedAreas()) {
    		selectedAreas.add(ar.getName());
		}
    	
    	System.out.println("Il ruolo ha id:" + role.getId());
    }
    
    
    /**
     * Method that is backed to a submit button of a form.
     */
    public void updateRole(ActionEvent e) throws java.io.IOException {
    	System.out.println("Aggiorno il ruolo con id:" + role.getId() + " ");
    	
    	if (role.getId()==null || (role.getId()!=null && role.getId()<1)) {
    		System.out.println("Il ruolo non ha l'id valorizzato!");
    		//FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Non posso aggiornare un gruppo non ancora inserito!", "Non posso aggiornare un gruppo non ancora inserito!"));
    		addWarningMessage("Can not update a role not yet created!");
            return;
    	}
    	
    	try {
    		System.out.println("Carico il ruolo con quell'id!");
	    	RoleVO roleL = roleService.findRoleById(role.getId()+"");
	    	if (roleL!=null) {
	    		
	    		role.getInterestedAreas().clear();
	    		for (String a : selectedAreas) {
	    			role.getInterestedAreas().add(UserMessageAreaType.getByName(a));
				}
	    		
	    		role = roleService.updateRole(role);
	    		
	        	addInfoMessage("Role aggiornato!");
	        	System.out.println("Role updated!");
				return;
	    	}
	    	else {
	    		System.out.println("role non trovato!");
	    		addErrorMessage("Error updating role!");
	            return;
	    	}
    	} catch (AccessDeniedException e1) {
    		e1.printStackTrace();
    		addErrorMessage("No enought permission to modify this role!", e1);
    		return;
		}
    }
    

	public RoleVO getRole() {
		return role;
	}

	public void setRole(RoleVO role) {
		this.role = role;
	}

	public List<javax.faces.model.SelectItem> getAvailablesAreaType() {
		return availablesAreaType;
	}

	public void setAvailablesAreaType(
			List<javax.faces.model.SelectItem> availablesAreaType) {
		this.availablesAreaType = availablesAreaType;
	}

	public List<String> getSelectedAreas() {
		return selectedAreas;
	}

	public void setSelectedAreas(List<String> selectedAreas) {
		this.selectedAreas = selectedAreas;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
}