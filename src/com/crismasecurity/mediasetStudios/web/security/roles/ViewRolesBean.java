package com.crismasecurity.mediasetStudios.web.security.roles;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.crismasecurity.mediasetStudios.core.security.services.RoleService;
import com.crismasecurity.mediasetStudios.core.security.vos.security.RoleVO;
import com.crismasecurity.mediasetStudios.web.utils.SortableList;



//@ManagedBean(name= "viewRolesBean") // JSF managed
//@SessionScoped // JSF managed
//@ViewScoped // JSF managed

//Spring managed
@Controller ("viewRolesBean") 
//@Scope("session")
@Scope("view")
public class ViewRolesBean extends SortableList<RoleVO> {

	//Serial
	private static final long serialVersionUID = -6548581429007827308L;

	@Autowired
	private RoleService roleService;
	
	
//	@Autowired
//	private ModifyRoleBean modifyRoleBean;
	
	
	private String nameFilter;
	
	
    /**
     * default empty constructor
     */
    public ViewRolesBean() {
    	super("name");
    }

//    /**
//     * Method that is backed to a submit button of a form.
//     */
//    public void modify(ActionEvent e) {
////    	FacesContext context = FacesContext.getCurrentInstance();
////    	Map<String, String> map = context.getExternalContext().getRequestParameterMap();
////    	String roleId= (String) map.get("roleId");
//    	
//    	HtmlCommandLink component = (HtmlCommandLink)e.getSource();
//    	Long roleId = null;
//    	for (UIComponent com : component.getChildren()) {
//    		if(com instanceof UIParameter && ((UIParameter)com).getName().equalsIgnoreCase("roleId")) {
//    			roleId = ((Long)((UIParameter)com).getValue());
//			}
//		}
//    	
//    	
//    	modifyRoleBean.modifyRole(roleId+"");
////		return "modify_role";
//    }
//    


	public List<RoleVO> getRoles() {
		
		List<RoleVO> roles = null;
		
		if (nameFilter!=null && !nameFilter.equals("")) {
			roles = roleService.findRoleByName(nameFilter);
		}
		else roles = roleService.findAllRoles();
		
		sort(roles);
		//System.out.println("Ordino per colonna: " + sortColumnName + "   ascendente? :" + ascending);
		
		return roles;
	}

	/**
     * Determines the sortColumnName order.
     *
     * @param   sortColumn to sortColumnName by.
     * @return  whether sortColumnName order is ascending or descending.
     */
    protected boolean isDefaultAscending(String sortColumn) {
        return true;
    }

    /**
     *  Sorts the list of users.
     */
    protected void sort(List<RoleVO> roles) {
        Comparator<RoleVO> comparator = new Comparator<RoleVO>() {
            public int compare(RoleVO u1, RoleVO u2) {
                if (sortColumnName == null) {
                    return 0;
                }
                if (sortColumnName.equals("id")) {
                    return ascending ?
                            u1.getId().compareTo(u2.getId()) :
                            u2.getId().compareTo(u1.getId());
                } else if (sortColumnName.equals("name")) {
                    return ascending ? u1.getNome().compareTo(u2.getNome()) :
                            u2.getNome().compareTo(u1.getNome());
                } else return 0;
            }
        };
        Collections.sort(roles, comparator);
    }

    
    
	public String search() {
		return "";
	}
	
	public String getNameFilter() {
		return nameFilter;
	}

	public void setNameFilter(String nameFilter) {
		this.nameFilter = nameFilter;
	}

}