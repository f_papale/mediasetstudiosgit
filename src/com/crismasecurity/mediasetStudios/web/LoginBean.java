package com.crismasecurity.mediasetStudios.web;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.WebAttributes;
import org.springframework.stereotype.Controller;

import com.crismasecurity.mediasetStudios.core.entity.Audit;
import com.crismasecurity.mediasetStudios.core.entity.Episode;
import com.crismasecurity.mediasetStudios.core.security.CustomUsernamePasswordAuthenticationToken;
import com.crismasecurity.mediasetStudios.core.security.entities.AppUser;
import com.crismasecurity.mediasetStudios.core.security.services.AppUserService;
import com.crismasecurity.mediasetStudios.core.security.vos.security.AppUserVO;
import com.crismasecurity.mediasetStudios.core.security.vos.security.GroupVO;
import com.crismasecurity.mediasetStudios.core.security.vos.security.RoleVO;
import com.crismasecurity.mediasetStudios.core.services.AuditService;
import com.crismasecurity.mediasetStudios.core.utils.PropertiesUtils;
import com.crismasecurity.mediasetStudios.core.utils.UserMessageAreaType;
import com.crismasecurity.mediasetStudios.web.utils.jsf.BaseBean;
import com.crismasecurity.mediasetStudios.web.utils.jsf.FacesUtils;
import com.crismasecurity.mediasetStudios.web.utils.spring.UniqueLoginApplicationComponent;
import com.onelogin.saml2.Auth;
import com.onelogin.saml2.settings.Saml2Settings;
import com.onelogin.saml2.settings.SettingsBuilder;


import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.primefaces.push.EventBus;
import org.primefaces.push.EventBusFactory;

import java.io.FileReader;


//@ManagedBean(name= "loginBean") // JSF managed
//@SessionScoped // JSF managed
//@ViewScoped // JSF managed

//Spring managed
@Controller ("loginBean") 
@Scope("session")
//@Scope("view")
public class LoginBean extends BaseBean {

	private static final long serialVersionUID = -7485624581367930930L;

	// propertieshttps://stackoverflow.com/tags
    private String userId;

    private String password;

    
    private boolean showpassword = false;
    
    @Autowired
    private UniqueLoginApplicationComponent uniqueLoginApplicationComponent;

	@Autowired
    private AppUserService appUserService;

    @Autowired
    private AuthenticationManager authenticationManager;
    private String errors;
    
    @Autowired
    private AuditService auditService;
    
    
    private String adfsSessionIndex;
    
    private boolean disableMultipleUserAccess = false;
    
    private String fullVersion;
    
    
    private boolean basicLogin=true;
    


    ////////////////// Da rimuovere //////////////////
//	private void executeQuery() {
//		
//		// Mi connetto al DB del controllo accessi
//	    String connectionUrl = "jdbc:sqlserver://WINSQLPROD03\\Prod01;SelectMethod=cursor;databaseName=Studios;;integratedSecurity=true;useUnicode=true;characterEncoding=UTF-8;charactetrResultSets=UTF-8";
//	    System.out.println(connectionUrl);
//	    // Declare the JDBC objects.
//	    Connection con = null;
//	    PreparedStatement preparedStatement = null;
//
//	      
//	    try {
//	        // Establish the connection.
//	        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
//	        con = DriverManager.getConnection(connectionUrl); // , "ct006838", "Security2017"
//
//	        String deleteTableSQL ="truncate table [mediastudios_documentSpectator]";
//	      
//	      	preparedStatement = con.prepareStatement(deleteTableSQL);
//	      	
//	      	// execute insert SQL stetement
//	      	preparedStatement.executeUpdate();
//	
//	      	// Chiudo il precedente statement
//	  		if (preparedStatement != null) try { preparedStatement.close(); } catch(Exception e) {}
//	      	
//	     } 
//	    catch(Throwable e) {
//	    	e.printStackTrace();
//	    }
//	     finally {
//	         if (preparedStatement != null) try { preparedStatement.close(); } catch(Exception e) {}
//	         if (con != null) try { con.close(); } catch(Exception e) {}
//	         System.out.println("finally!!!");
//	     }
//	}
    //////////////////////////////////////////////////
    
    
    /**
     * default constructor
     */
    public LoginBean() {
        Exception ex = (Exception) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(WebAttributes.AUTHENTICATION_EXCEPTION);

        if (ex != null) {
//            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ex.getMessage()));
            if (ex.getMessage()!=null && ex.getMessage().equalsIgnoreCase("Bad credentials")) {
//            	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Nome utente e/o password errati!", "Nome utente e/o password errati!"));
//            	errors = "Invalid login";
            	errors = getMessagesBoundle().getString("login.invalidlogin");
            } else {
            	errors="* " + ex.getMessage();
            }
            
            getLog().error("Login bean print error in costructor: " + ex.getMessage());
        }
        
//        executeQuery(); // TODO !!!!!!!!!!!!!!!!!!!!!!!! da rimuovere!!!!!!!!!!!!!!!!!!!
    }
    
    
//    public void userIdSubmitted(ValueChangeEvent e) {
//    	System.out.println("UserId specified: " + e.getNewValue());
//    	if (e.getNewValue()!=null && !((String)e.getNewValue()).equals("")) {
//	    	rooms = new ArrayList<SelectItem>();
//	    	Reservation res = bookingService.getReservationByLogin((String)e.getNewValue());
//	    	if (res!=null && res.isInCheckIn()) {
//	    		if (res.getRooms()!=null) {
//	    			for (ReservationRoom ro : res.getRooms()) {
//						rooms.add(new SelectItem(ro.getId().toString(), ro.getName()));
//					}
//	    		}
//	    	}
////	    	if (rooms.size()==0) rooms.add(new SelectItem("", "none"));
//    	}
//    }
    
	@PostConstruct
	public void init() {
		setDisableMultipleUserAccess(Boolean.parseBoolean(PropertiesUtils.getInstance().readProperty("acces.user.disableMultipleUserAccess","false")));
        setFullVersion(getVersion());
        setEnvironment();
	}
	
	private void setEnvironment() {
		try {
			basicLogin = Boolean.parseBoolean(PropertiesUtils.getInstance().readProperty("loginmode.basic","true"));	
		} catch (Exception e) {
			basicLogin=true;
			// TODO: handle exception
		}
		
	}
	
	
	private String getVersion()
	{
	    String path = "/version.properties";
	    InputStream stream = getClass().getResourceAsStream(path);
	    if (stream == null)
	        return "UNKNOWN";
	    Properties props = new Properties();
	    try {
	        props.load(stream);
	        stream.close();
	        return (String) "(Ver. " + props.get("version")+")";
	    } catch (IOException e) {
	        return "UNKNOWN";
	    }
	} 
	
    public void keepSessionAlive(){
    	System.out.println("...keepSessionAlive");
    }
    
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void backToLogin(ActionEvent e) {
    	showpassword = false;
    	errors = "";
    }
    public void goToPassword(ActionEvent e) {

		if (getUserId() == null || getUserId().equals("")) {
//			errors="A value is required!";
			errors = getMessagesBoundle().getString("login.valuerequired");
			showpassword = false;
    		return;
		}
    	
        
        try {
        	showpassword = true;
        	errors = "";
        } catch(Exception ee) {
        	errors = ee.getMessage();
        	showpassword = false;
        }
    }
    
    public void userLoggedAndAlive() {
    	//getLog().info("userLoggedAndAlive " + userId);
    	uniqueLoginApplicationComponent.updateUserTime(userId);
    }
    
    public String login(){
//    	try {
//    		if (getUserId() == null || getUserId().equals(""))
//    			throw new Exception("User Id is required."); 
//    	}catch (Exception eNull) {
//    		addErrorMessage("", eNull);
//    	}
    	
    	if (uniqueLoginApplicationComponent.isYetLogged(userId)) {
//    		errors = "User yet logged!"; 
    		errors = getMessagesBoundle().getString("login.useryetlogged");
    	}
    	else {
    	
	    	errors="";
	        
	        try {
	        	
	        	String encodedPassword = password;
	        	try {
	        		encodedPassword = appUserService.encodePasswordForUser(userId, password);
	    		} catch (UsernameNotFoundException e2) {
	    		}
	        	
	        	Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
	        	CustomUsernamePasswordAuthenticationToken authToken = new CustomUsernamePasswordAuthenticationToken(getUserId(), encodedPassword);
	        	authToken.setPlainPassword(password);
	//        	UsernamePasswordAuthenticationToken authToken = new  UsernamePasswordAuthenticationToken(getUserId(), encodedPassword);
	        	authToken.setDetails(authentication.getDetails());
	
	//        	ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
	//        	((HttpServletRequest) ec.getRequest()).getSession().setAttribute(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_LAST_USERNAME_KEY,authToken.getName());
	
	        	Authentication newAuth = authenticationManager.authenticate(authToken);
	        	SecurityContextHolder.getContext().setAuthentication(newAuth);
	//        	this.user = (UserGipc) newAuth.getPrincipal(); //save my principal
	
	        	
	        	
	        	AppUserVO uu = userService.getUserByLogin(userId);
	        	auditService.addAudit(new Audit(uu.getLastName() + " " + uu.getFirstName()+" ("+userId+")", "Logon", ""));
	        	
	        	
	        	uniqueLoginApplicationComponent.updateUserTime(userId);
	        	
	        	return "homePage";
	        } catch (UsernameNotFoundException ex) {
	        	ex.printStackTrace();
	        	errors+="" + ex.getMessage();
	//        	ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
	//        	((HttpServletRequest) ec.getRequest()).getSession().setAttribute(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_LAST_USERNAME_KEY,null);
	
	        } catch (Exception ex) {
	        	ex.printStackTrace();
	        	errors+="" + ex.getMessage();
	//        	ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
	//        	((HttpServletRequest) ec.getRequest()).getSession().setAttribute(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_LAST_USERNAME_KEY,null);
	        }
    	}


        return "";        
    }

    public void logout(ActionEvent e) throws java.io.IOException {
    	
    	getLog().info("logout....");
    	//HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
    	//new SecurityContextLogoutHandler().logout(request, null, null);
    	
    	AppUserVO uu = userService.getUserByLogin(userId);
    	try {
    		//AppUserVO uu = userService.getUserByLogin(userId);
        	auditService.addAudit(new Audit(uu.getLastName() + " " + uu.getFirstName()+" ("+userId+")", "Logout", ""));
		} catch (Exception e2) {
			e2.printStackTrace();
		}
    	
    	if (uu.isAdfsUser()) {
    		try {
    			
    			String relayState = PropertiesUtils.getInstance().readPropertyFromFile("onelogin.saml2.sp.single_logout_service.url", "onelogin.adfs.properties");
    			if (relayState==null) relayState = "";
    			
    			System.out.println("Faccio il logout adfs..... userid:" + getUserId() + "    adfsSessionIndex:" + adfsSessionIndex);
	    		HttpServletRequest request = FacesUtils.getServletRequest();
				HttpServletResponse response = (HttpServletResponse)FacesUtils.getExternalContext().getResponse();
				
	    		Saml2Settings settings = new SettingsBuilder().fromFile("onelogin.adfs.properties").build();
				settings.setLogoutRequestSigned(false);
		
				Auth auth = new Auth(settings, request, response);
				//String relayState = "https://test.studios.mediaset.it/MediasetStudios/springSecurityLogin.jsf";
				//auth.logout(relayState);
				String target = auth.logout(relayState, getUserId(), adfsSessionIndex, true, "urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified");
				System.out.println("Fatto !!");
    		} catch (Exception e2) {
				e2.printStackTrace();
			}
    	}
    	
    	System.out.println("Faccio logout normale !!!");
    	FacesContext.getCurrentInstance().getExternalContext().redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath()+"/logout.url");
    }
 
  
    @Autowired
    AppUserService userService;
    public String getUserDescription() {
//    	// <tr:outputText value="#{sessionScope.SPRING_SECURITY_LAST_USERNAME}"/>
//    	FacesContext context = FacesContext.getCurrentInstance();
//    	Map<String, Object> map = context.getExternalContext().getSessionMap();
//    	String login= (String) map.get("SPRING_SECURITY_LAST_USERNAME");
//    	
//    	if (login!=null) {
//    			return userService.getUserDescription(login);
//    	}
//    	return "none";
    	
    	
//    	System.out.println("getUserDescription for user:" + userId);
    	if (userId!=null) {
    		if (userService.getUserByLogin(userId)!=null) {
//    			System.out.println("E' un utente!");
    			return userService.getUserDescription(userId);
    		}
    	}
    	
    	return "";
    }
    
    

	
    /**
     * Indica se è un utente locale oppure no (quindi Ldap o ADFS)
     * @return
     */
    public boolean isLocalUser() {
    	if (userId!=null) {
    		AppUserVO uuu = userService.getUserByLogin(userId);
    		if (uuu!=null && !uuu.isAdfsUser() && !uuu.isLdapUser()) return true;
    	}
    	
    	return false;
    }
    
    
    public String getUserLoginName() {
    	// <tr:outputText value="#{sessionScope.SPRING_SECURITY_LAST_USERNAME}"/>
//    	FacesContext context = FacesContext.getCurrentInstance();
//    	Map<String, Object> map = context.getExternalContext().getSessionMap();
//    	String login= (String) map.get("SPRING_SECURITY_LAST_USERNAME");
//    	
//    	return login;
    	return userId;
    }
    
    public String getUserLoginNameFull() {

    	String login= userId;
    	List<String> areas = new ArrayList<String>();
    	AppUserVO us = userService.getUserByLogin(login);
    	if (us.getGroups()!=null) {
    		for (GroupVO gr : us.getGroups()) {
    			if (gr != null) {
    				areas.add(gr.getName());
    			}
//				if (gr.getRoles()!=null) {
//					for (RoleVO rol : gr.getRoles()) {
//						if (rol.getDescrizione()!=null) areas.add(rol.getDescrizioneBreve());
//					}
//				}
				
			}
    	}
    	return areas.toString();
    }
    
    
    public List<UserMessageAreaType> getUserInterestedAreas() {
    	// <tr:outputText value="#{sessionScope.SPRING_SECURITY_LAST_USERNAME}"/>
//    	FacesContext context = FacesContext.getCurrentInstance();
//    	Map<String, Object> map = context.getExternalContext().getSessionMap();
//    	String login= (String) map.get("SPRING_SECURITY_LAST_USERNAME");
    	String login= userId;
    	List<UserMessageAreaType> areas = new ArrayList<UserMessageAreaType>();
    	AppUserVO us = userService.getUserByLogin(login);
    	if (us.getGroups()!=null) {
    		for (GroupVO gr : us.getGroups()) {
				if (gr.getRoles()!=null) {
					for (RoleVO rol : gr.getRoles()) {
						if (rol.getInterestedAreas()!=null) areas.addAll(rol.getInterestedAreas());
					}
				}
			}
    	}
    	return areas;
    }
    
    public boolean isUserLoggedIn() {
    	// <tr:outputText value="#{sessionScope.SPRING_SECURITY_LAST_USERNAME}"/>
//    	FacesContext context = FacesContext.getCurrentInstance();
//    	Map<String, Object> map = context.getExternalContext().getSessionMap();
//    	String login= (String) map.get("SPRING_SECURITY_LAST_USERNAME");
//    	Exception loginException= (Exception) map.get("SPRING_SECURITY_LAST_EXCEPTION");
//
//    	if (login!=null && loginException==null) return true;
//    	return false;
    	
    	
    	if (SecurityContextHolder.getContext().getAuthentication() != null &&
    			 SecurityContextHolder.getContext().getAuthentication().isAuthenticated() &&
    			 //when Anonymous Authentication is enabled
    			 !(SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken)) {
    		//System.out.println("Loged-in!");
    		return true; 
    	}
    	//System.out.println("Loged-OUT!");
    	return false;
    }
    
    /**
	 * for index.xhtml
	 * @throws IOException
	 */
	public void checkIfLoggedInAndHaveToChangePage() throws IOException {
		if (isUserLoggedIn()) {
			// Pagina index
			ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
	        ec.redirect(ec.getRequestContextPath() + "/secured/index.jsf");

		}
	}

	public String getErrors() {
		return errors;
	}

	public void setErrors(String errors) {
		this.errors = errors;
	}

	public void changePassword(ActionEvent e) {
		try {
//			FacesContext context = FacesContext.getCurrentInstance();
//	    	Map<String, Object> map = context.getExternalContext().getSessionMap();
//	    	String login= (String) map.get("SPRING_SECURITY_LAST_USERNAME");
			String login= userId;
	    	
			if (newPassword!=null && !newPassword.equals("") && !isPasswordComplexityOk(newPassword)) {
//				addWarningMessage("Le password NON rispetta le regole di complessità (almeno un numero, almeno una lettera maiuscola, almeno una lettera minuscola, almeno un carattere speciale tra @#%$^, nessuno spazio consentito e lunghezza di almeno 8 caratteri)!");
				addWarningMessage(getMessagesBoundle().getString("login.passwordnotcomplex"));
				return;
			}
        	try {
        		AppUser userI= userService.getUserEntityByLogin(login);
        		String encodedPassword = appUserService.encodePasswordForUser(login, newPassword);
        		
        		if (userI.getPassword().equalsIgnoreCase(encodedPassword)) {
//        			addWarningMessage("Le password inserita risulta uguale alla vecchia password. Prego specificarne un'altra.");
        			addWarningMessage(getMessagesBoundle().getString("login.passwordused"));
    				return;
        		}
        		
    		} catch (Exception e2) {
    			e2.printStackTrace();
    		}
			
			
	    	if (login!=null) {
	    		AppUserVO user= userService.getUserByLogin(login);
	    		
	    		user.setNewPassword(newPassword);
	    		userService.updateUser(user);
//	    		addInfoMessage("Password changed!");
	    		addInfoMessage(getMessagesBoundle().getString("login.changed"));
	    	}
		} catch (Exception e2) {
			e2.printStackTrace();
//			addWarningMessage("Error changing password!");
			addWarningMessage(getMessagesBoundle().getString("login.changingerror"));
		}
	}
	
	public void changeExpiredPassword(ActionEvent e) {
		try {
			String login= userId;
	    	
			if (newPassword==null || newPassword.trim().equals("")) {
//				addWarningMessage("Please specify a password!");
				addWarningMessage(getMessagesBoundle().getString("login.specifypassword"));
				return;
			}
			if (!newPassword.equals(newPasswordConfirm)) {
//				addWarningMessage("Le password non combaciano!");
				addWarningMessage(getMessagesBoundle().getString("login.passwordnotmatch"));
				return;
			}
			
			if (!isPasswordComplexityOk(newPassword)) {
//				addWarningMessage("Le password NON rispetta le regole di complessità (almeno un numero, almeno una lettera maiuscola, almeno una lettera minuscola, almeno un carattere speciale tra @#%$^, nessuno spazio consentito e lunghezza almeno 8 caratteri)!");
				addWarningMessage(getMessagesBoundle().getString("login.passwordnotcomplex"));
				return;
			}
			
			try {
        		AppUser userI= userService.getUserEntityByLogin(login);
        		String encodedPassword = appUserService.encodePasswordForUser(login, newPassword);
        		
        		if (userI.getPassword().equalsIgnoreCase(encodedPassword)) {
//        			addWarningMessage("Le password inserita risulta uguale alla vecchia password. Prego specificarne un'altra.");
        			addWarningMessage(getMessagesBoundle().getString("login.passwordused"));
    				return;
        		}
        		
    		} catch (Exception e2) {
    			e2.printStackTrace();
    		}
			
	    	if (login!=null) {
	    		AppUserVO user= userService.getUserByLogin(login);
	    		user.setNewPassword(newPassword);
	    		user.setPasswordChangeDate(new Date());
	    		userService.updateUser(user);
//	    		addInfoMessage("Password changed!");
				addWarningMessage(getMessagesBoundle().getString("login.changed"));
	    		
	    		logout(e); // esco
	    	}
		} catch (Exception e2) {
			e2.printStackTrace();
//			addWarningMessage("Error changing password!");
			addWarningMessage(getMessagesBoundle().getString("login.changingerror"));
		}
	}
	
	
	public static boolean isPasswordComplexityOk(String password) {
		
		/**
		 *  ^                 # start-of-string
		 *	(?=.*[0-9])       # a digit must occur at least once
		 *	(?=.*[a-z])       # a lower case letter must occur at least once
		 *	(?=.*[A-Z])       # an upper case letter must occur at least once
		 *	(?=.*[@#$%^&+=])  # a special character must occur at least once
		 *	(?=\S+$)          # no whitespace allowed in the entire string
		 *	.{8,}             # anything, at least eight places though
		 *	$                 # end-of-string
		 */
		String pattern = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$";
	    return password.matches(pattern);
		

	}
	
	
	
	
	
	
	
	
	
	
	
	/**************************** ADFS ********************************/
	public String getAdfsEndpointUrl() {
		try {
			
			String relayState = PropertiesUtils.getInstance().readPropertyFromFile("onelogin.saml2.sp.assertion_consumer_service.url", "onelogin.adfs.properties");
			if (relayState==null) relayState = "";
		
			HttpServletRequest request = FacesUtils.getServletRequest();
			HttpServletResponse response = (HttpServletResponse)FacesUtils.getExternalContext().getResponse();
	
			Saml2Settings settings = new SettingsBuilder().fromFile("onelogin.adfs.properties").build();
			settings.setAuthnRequestsSigned(false);
	
			Auth auth = new Auth(settings, request, response);
			String target = auth.login(relayState, false, false, false, true);
			System.out.println("SSO url:" + target);
			return target;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	
	public void checkAdfsAuth() {
		try {
			
			HttpServletRequest request = FacesUtils.getServletRequest();
			HttpServletResponse response = (HttpServletResponse)FacesUtils.getExternalContext().getResponse();
			
			
			Saml2Settings settings = new SettingsBuilder().fromFile("onelogin.adfs.properties").build();
			settings.setAuthnRequestsSigned(false);
			
			Auth auth = new Auth(settings, request, response);
			auth.processResponse();
	
			String errore = "";
			if (!auth.isAuthenticated()) {
//				errore="Autenticazione fallita!";
				errore=getMessagesBoundle().getString("login.authenticationfail");
			}
	
			List<String> errors = auth.getErrors();
	
			if (!errors.isEmpty()) {
				errore=StringUtils.join(errors, ", ");
				if (auth.isDebugActive()) {
					String errorReason = auth.getLastErrorReason();
					if (errorReason != null && !errorReason.isEmpty()) {
						errore="\n " + auth.getLastErrorReason();
					}
				}
				
				addWarningMessage(errore);
				System.out.println("!!!! ADFS error: " + errore);
				
			} else {
				Map<String, List<String>> attributes = auth.getAttributes();
				String nameId = auth.getNameId();
				String nameIdFormat = auth.getNameIdFormat();
				adfsSessionIndex = auth.getSessionIndex(); // Questa mi serve poi per il logout !!!!!!!!!!!!!!!
				String nameidNameQualifier = auth.getNameIdNameQualifier();
				String nameidSPNameQualifier = auth.getNameIdSPNameQualifier();
	
				// DEBUG //
				addInfoMessage("nameId:" + nameId + "     nameIdFormat:"+ nameIdFormat + "     sessionIndex:" + adfsSessionIndex + "      nameidNameQualifier:" + nameidNameQualifier + "     nameidSPNameQualifier:" + nameidSPNameQualifier);
				System.out.println("!!!!!!!!!!!AUTENTICAZIONE ADFS  -   nameId:" + nameId + "     nameIdFormat:"+ nameIdFormat + "     sessionIndex:" + adfsSessionIndex + "      nameidNameQualifier:" + nameidNameQualifier + "     nameidSPNameQualifier:" + nameidSPNameQualifier);
				if (attributes!=null) {
					System.out.println("Attributes...");
					Set<String> keys = attributes.keySet();
					for (String key : keys) {
						System.out.print("Name: " + key + "   Values:");
						List<String> vals = attributes.get(key);
						for (String val : vals) {
							System.out.print(val + "; ");
						}
						System.out.println();
					}
				}

				if (uniqueLoginApplicationComponent.isYetLogged(nameId)) {
//		    		errors = "User yet logged!"; 
					addWarningMessage(getMessagesBoundle().getString("login.useryetlogged"));
		    	}
				else {
				
					// faccio autenticazione !!!!!!!!!!!!!!!!
					UserDetails user = null;
			        try {
			        	user = userService.loadUserByUsername(nameId);
					} catch (UsernameNotFoundException e) {
						System.err.println(e.getMessage());
					}
			        if (user!=null) {
			        	System.out.println("Utente trovato faccio autenticazione!!!!");
			        	
			        	setUserId(nameId); // Questo serve !!!
			        	
			        	
			        	Collection<? extends GrantedAuthority> authorities = user.getAuthorities();
			        	Authentication newAuth = new org.springframework.security.authentication.UsernamePasswordAuthenticationToken(user, password, authorities);
			        	SecurityContextHolder.getContext().setAuthentication(newAuth);
			        }
			        else {
			        	// Utente non trovato localmente!!!!
			        	addWarningMessage("Utente non profilato sul sistema MediasetStudios! Rivolgersi all'amministratore!");
			        	System.out.println("Utente non profilato sul sistema MediasetStudios! Rivolgersi all'amministratore!");
			        	
			        }
				}
				
	
				// Redirect su home (oppure rimane sulla pagina di login se non autenticato)
				FacesContext facesContext = FacesContext.getCurrentInstance();
		        String outcome = "homePage";
		        facesContext.getApplication().getNavigationHandler().handleNavigation(facesContext, null, outcome);
		  
			}
		} catch (Throwable e) {
			//e.printStackTrace();
			//getLog().error(e.getMessage());
		}
	}
	/******************************************************************/
	
	
	

	
	private String newPasswordConfirm;
	private String newPassword;

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	


	public boolean isShowpassword() {
		return showpassword;
	}


	public void setShowpassword(boolean showpassword) {
		this.showpassword = showpassword;
	}


	public String getNewPasswordConfirm() {
		return newPasswordConfirm;
	}


	public void setNewPasswordConfirm(String newPasswordConfirm) {
		this.newPasswordConfirm = newPasswordConfirm;
	}


	public boolean isDisableMultipleUserAccess() {
		return disableMultipleUserAccess;
	}


	public void setDisableMultipleUserAccess(boolean disableMultipleUserAccess) {
		this.disableMultipleUserAccess = disableMultipleUserAccess;
	}


	public String getFullVersion() {
		return fullVersion;
	}


	public void setFullVersion(String fullVersion) {
		this.fullVersion = fullVersion;
	}


	public boolean isBasicLogin() {
		return basicLogin;
	}
    
}