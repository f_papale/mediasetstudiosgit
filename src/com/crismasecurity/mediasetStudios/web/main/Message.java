/**
 * 
 */
package com.crismasecurity.mediasetStudios.web.main;

/**
 * Class name: Message
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author Giovanni
 * @date 25/nov/2010
 *
 */
public class Message {
	private String message;
	private String color;
	private int type; // 0=error; 1=warning; 2=info; 3=debug
	
	
	
	public Message(int type, String message) {
		super();
		this.message = message;
		this.type=type;
		if(type==0) color = "#f8b686";
		if(type==1) color = "#f7f886";
		if(type==2) color = "#b5f886";
		if(type==3) color = "#ffffff";
	}
	
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}


	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
	
	public String getMessageType(){
		switch(this.type){
			case 0:
				return "Error";
			case 1:
				return "Warning";
			case 2:
				return "Info";
			case 3:
				return "Debug";
			default:
				return "Message";
		}
	}
	

}
