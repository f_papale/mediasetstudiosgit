package com.crismasecurity.mediasetStudios.web.main;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import com.crismasecurity.mediasetStudios.core.security.services.AppUserService;


/**
 * 
 * Class name: NavigationTabsBean
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author Giovanni
 * @date 31/ago/2010
 *
 */
//@ManagedBean(name= "navigationTabsBean") // JSF managed
//@SessionScoped // JSF managed
//@ViewScoped // JSF managed

//Spring managed
@Controller ("navigationTabsBean") 
@Scope("session")

public class NavigationTabsBean implements Serializable {

	// Serial
	private static final long serialVersionUID = 8115407169104680397L;
	
	private int selectedTab;
	
	// Messages
	private boolean showPopupMessages = false;
	private List<Message> messages;
	public static int MESSAGES_DEBUG_LEVEL = 2; // -1=disabled; 0=error; 1=error,warning; 2=error,warning,info; 3=error,warning,info, debug
	
	@Autowired
	private AppUserService appUserService;

	/**
     * default empty constructor
     */
    public NavigationTabsBean() {

    	selectedTab = -1; // Nessun tab
    	messages = new ArrayList<Message>();
    }
  
    
    
	public int getSelectedTab() {
		if (selectedTab == -1) {
			// Calcolo quale è il primo tab attivo
	    	if (appUserService.hasAuthority("ROLE_ADMIN")) selectedTab =0;
	    	else if (appUserService.hasAuthority("BF_PDF_WRITER")) selectedTab =0;
	    	else if (appUserService.hasAuthority("BF_PDF_READER")) selectedTab =1;
	    	else if (appUserService.hasAuthority("BF_PDF_ADMIN")) selectedTab =2;
	    	System.out.println("Primo tab attivo: " + selectedTab);
		}
		return selectedTab;
	}
	public void setSelectedTab(int selectedTab) {
		this.selectedTab = selectedTab;
	}

	
	
	/** Messages **/
	
	public void hidePopupMessages(AjaxBehaviorEvent event) {
		showPopupMessages = false;
    }

    public void viewPopupMessages(AjaxBehaviorEvent event) {
    	showPopupMessages = true;
    }
	
	 public void hidePopupMessages(ActionEvent event) {
		 showPopupMessages = false;
		 messages.clear();
	 }
	 public void addErrorMessage(String message) {
		 if (MESSAGES_DEBUG_LEVEL>-1) {
			 messages.add(new Message(0, message));
			 showPopupMessages = true;
		 }
	 }
	 public void addWarningMessage(String message) {
		 if (MESSAGES_DEBUG_LEVEL>0) {
			 messages.add(new Message(1, message));
			 showPopupMessages = true;
		 }
	 }
	 public void addInfoMessage(String message) {
		 if (MESSAGES_DEBUG_LEVEL>1) {
			 messages.add(new Message(2,message));
			 showPopupMessages = true;
		 }
	 }
	 public void addDebugMessage(String message) {
		 if (MESSAGES_DEBUG_LEVEL>2) {
			 messages.add(new Message(3, message));
			 showPopupMessages = true;
		 }
	 }
	public boolean isShowPopupMessages() {
		return showPopupMessages;
	}
	public void setShowPopupMessages(boolean showPopupMessages) {
		this.showPopupMessages = showPopupMessages;
	}
	public List<Message> getMessages() {
		return messages;
	}
	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}
	
	
}
