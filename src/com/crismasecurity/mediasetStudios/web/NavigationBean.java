package com.crismasecurity.mediasetStudios.web;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import com.crismasecurity.mediasetStudios.core.entity.Audit;
import com.crismasecurity.mediasetStudios.core.entity.FunctionRoles;
import com.crismasecurity.mediasetStudios.core.security.services.AppUserService;
import com.crismasecurity.mediasetStudios.core.security.vos.security.AppUserVO;
import com.crismasecurity.mediasetStudios.core.services.AuditService;
import com.crismasecurity.mediasetStudios.core.services.FunctionRolesService;
import com.crismasecurity.mediasetStudios.core.utils.PropertiesUtils;
import com.crismasecurity.mediasetStudios.web.utils.UserAgentInfo;
import com.crismasecurity.mediasetStudios.web.utils.jsf.FacesUtils;


//@ManagedBean(name= "navigationBean") // JSF managed
//@SessionScoped // JSF managed
//@ViewScoped // JSF managed

//Spring managed
@Controller ("navigationBean") 
@Scope("session")
//@Scope("view")
public class NavigationBean implements Serializable {

	private static final long serialVersionUID = -3386427803532917353L;

	private String clientBrowserInfo; // Informazioni sul browser utente
	private String clientBrowserPluginsStatus; // Stato dei plugins PDF. Nell'ordine <ADOBE>;<GENERIC>;<FIREFOX>

	
	@Autowired
    private AppUserService userService;
	@Autowired
	private LoginBean loginBean;
	@Autowired
	private AuditService auditService;
	@Autowired FunctionRolesService functionRolesService;
	
	private boolean appSideShowIntercome=false;
	private boolean appSideShowCameraShot=false;
	private boolean appSideShowScan=false;
	
	private int menuIndex = 0;
	
	
	
	public NavigationBean() {
	}	
	
	
	public void actionResetBeans(ActionEvent evt){
		System.out.println("actionResetBeans " + evt.getComponent().getId());
		//FacesUtils.resetAllSessionManagedBeans();
		FacesUtils.resetManagedBean("monitorsPanelBean");
		
	}
	
	
	public void auditLogFunctionName(String functionName){
		System.out.println("auditLogFunctionName: " + functionName);
		
		
		try {
			AppUserVO uu = userService.getUserByLogin(getUserName());
			auditService.addAudit(new Audit(uu.getLastName() + " " + uu.getFirstName()+" ("+getUserName()+")", "Function access", functionName, null, "", "", null, null));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	public boolean haveUserRole(String roles) {
		
		String[] listRoles = roles.split(",");
		for (int i = 0; i < listRoles.length; i++) {
			listRoles[i] = listRoles[i].trim();
			
		}
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication!=null && authentication.getAuthorities()!=null) {
			//String currentPrincipalName = authentication.getName();
			for (GrantedAuthority r : authentication.getAuthorities()) {
				//System.out.println("Ruolo:"+r.getAuthority());
				for (int i = 0; i < listRoles.length; i++) {
					if (r.getAuthority()!=null && r.getAuthority().equals(listRoles[i])) {
						//System.out.println("trovato ruolo !!!");
						return true;
					}					
				}
			}
		}
		return false;
	}
	
	public boolean hasAnyRole(String roles) {
				
		String[] listRoles = roles.split(",");
		for (int i = 0; i < listRoles.length; i++) {
			listRoles[i] = listRoles[i].trim();
			
		}
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication!=null && authentication.getAuthorities()!=null) {
			//String currentPrincipalName = authentication.getName();
			for (GrantedAuthority r : authentication.getAuthorities()) {
				//System.out.println("Ruolo:"+r.getAuthority());
				for (int i = 0; i < listRoles.length; i++) {
					if (r.getAuthority()!=null && r.getAuthority().equals(listRoles[i])) {
						//System.out.println("trovato ruolo !!!");
						return true;
					}					
				}
			}
		}
		return false;
	}

	
	private class FunctionRoleClass 
	{
		String role;
		String function;
		String redirectPath;
		
		public FunctionRoleClass(String role, String function, String redirectPath) {
			this.role= role;
			this.function=function;
			this.redirectPath=redirectPath; 
		}
	}
	
	
	public String getValidUserRadirectPath(String excludeRoles) {
		
		List<FunctionRoleClass> lstFuntionRoles = new ArrayList<FunctionRoleClass>();
		for (FunctionRoles funct : functionRolesService.getFunctionRoles()) {
			for (String role : funct.getRoles().split(",")) {
				if (! excludeRoles.contains(role)) {
					lstFuntionRoles.add(new FunctionRoleClass(role,funct.getName(), funct.getRedirectPath()) );
				}
			}
		}
		
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication!=null && authentication.getAuthorities()!=null) {
			for (GrantedAuthority r : authentication.getAuthorities()) {
				if (r.getAuthority()!=null) {
					for (FunctionRoleClass functionRoleClass : lstFuntionRoles) {
						if (r.getAuthority().equals(functionRoleClass.role)){
							return functionRoleClass.redirectPath;
						}
					}
				}
			}
		}
		return getRedirectPath("all");
	}
	
	public String getGrantString(String section) {
		
		String strGrant="";
		try {
			strGrant=functionRolesService.getFunctionRolesByName(section.toLowerCase())	;
		} catch (Exception e) {
			strGrant=functionRolesService.getFunctionRolesByName("all")	;
		}
		return strGrant.toUpperCase();
	}
	
	public String getRedirectPath(String function) {

		String sPath="";
		try {
			sPath=functionRolesService.getFunctionRolesFinder().and("name").eq(function.toLowerCase()).setDistinct().result().getRedirectPath()	;
		} catch (Exception e) {
			sPath=functionRolesService.getFunctionRolesFinder().and("name").eq("all").setDistinct().result().getRedirectPath()	;;
		}
		return sPath;
	}

	
	
	/**
	 * for index.xhtml
	 * @throws IOException
	 */
	
	
	public void checkIfHaveToChangePage() throws IOException {
		
		/****
		 * !!!!!!!!!!!!!!!!!!!!!!!!!!! Attenzione!!!!  Ricordarsi di abilitare il ruolo anche sul applicationSecurity.xml altrimenti continua a reindirizzare!
		 */
		
		
		if (isUserPasswordExpired()) {
			// Pagina amministratore
			ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
	        ec.redirect(ec.getRequestContextPath() + "/secured/passwordExpired.jsf");
		}
		else if (isUserAdmin()) {
			// Pagina amministratore
			ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
	        ec.redirect(ec.getRequestContextPath() + "/secured/management/home.jsf");
		}
//		else if (haveUserRole("ROLE_AGENCY"))  {
//			// Pagina amministratore
//			ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
//	        ec.redirect(ec.getRequestContextPath() + "/secured/management/spectatorEpisodeCalendar.jsf");
//		}
//		else if (haveUserRole("ROLE_PRODUCTION"))  {
//			// Pagina amministratore
//			ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
//	        ec.redirect(ec.getRequestContextPath() + "/secured/management/production.jsf");
//		}
//		else if (haveUserRole("ROLE_SECURITY"))  {
//			// Pagina amministratore
//			ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
//	        ec.redirect(ec.getRequestContextPath() + "/secured/reception/reception.jsf");
//		}
//		else if (haveUserRole("ROLE_SUPERVISION"))  {
//			// Pagina amministratore
//			ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
//	        ec.redirect(ec.getRequestContextPath() + "/secured/reception/reception.jsf");
//		}
//		else if (haveUserRole("ROLE_STADMIN"))  {
//			// Pagina amministratore
//			ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
//	        ec.redirect(ec.getRequestContextPath() + "/secured/management/home.jsf");
//		}
		
		else if (haveUserRole("ROLE_AGENCY"))  {
			// Pagina amministratore
			ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
//	        ec.redirect(ec.getRequestContextPath() + "/secured/management/spectatorEpisodeCalendar.jsf");
			ec.redirect(ec.getRequestContextPath() + getRedirectPath("spectatorEpisodeCalendar_management")); 
		}
		else if (haveUserRole("ROLE_PRODUCTION"))  {
			// Pagina amministratore
			ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
//	        ec.redirect(ec.getRequestContextPath() + "/secured/management/production.jsf");
	        ec.redirect(ec.getRequestContextPath() + getRedirectPath("production_management")); 
		}
		else if (haveUserRole("ROLE_SECURITY,ROLE_SUPERVISION"))  {
			// Pagina amministratore
			ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
//	        ec.redirect(ec.getRequestContextPath() + "/secured/reception/reception.jsf");
	        ec.redirect(ec.getRequestContextPath() + getRedirectPath("reception")); 
		}
//		else if (hasAnyRole("ROLE_SUPERVISION"))  {
//			// Pagina amministratore
//			ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
//	        ec.redirect(ec.getRequestContextPath() + "/secured/reception/reception.jsf");
//		}
		else if (haveUserRole("ROLE_STADMIN"))  {
			// Pagina amministratore
			ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
//	        ec.redirect(ec.getRequestContextPath() + "/secured/management/home.jsf");
	        ec.redirect(ec.getRequestContextPath() + getRedirectPath("home")); 
		}
//		else if (haveUserRole("AGT_MAN_W"))  {
//			// Pagina amministratore
//			ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
////	        ec.redirect(ec.getRequestContextPath() + "/secured/management/home.jsf");
//	        ec.redirect(ec.getRequestContextPath() + getRedirectPath("agencytype_management")); 
//		}
		else {
			// Pagina gestore documenti
			ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
//	        ec.redirect(ec.getRequestContextPath() + "/secured/index.jsf");
//	        Popo
//			ec.redirect(ec.getRequestContextPath() + getValidUserRadirectPath("ROLE_ADMIN,ROLE_PRODUCTION,ROLE_SECURITY,ROLE_SUPERVISION,ROLE_AGENCY,ROLE_STADMIN"));
			ec.redirect(ec.getRequestContextPath() + getValidUserRadirectPath(""));
		}
	}

	public boolean isMobileBrowser() {
		HttpServletRequest request = (HttpServletRequest)FacesUtils.getExternalContext().getRequest();
		
		UserAgentInfo uai = new UserAgentInfo(request);
//		System.out.println("isMobileBrowser?" + uai.detectTierIphone());
		return uai.detectTierIphone();
		
	}
	
	
	public String getUserName() {
		//LoginBean loginBean = (LoginBean)FacesUtils.getManagedBean("loginBean");
		String userId = loginBean.getUserId();
		return userId;
	}
	
	public boolean isUserPasswordExpired() {
		
		if (getUserName()!=null && !getUserName().equals("")) {
			
			AppUserVO vo = userService.getUserByLogin(getUserName());
			if (vo!=null && !vo.isLdapUser()&& !vo.isAdfsUser() && vo.getPasswordChangeDate()!=null) {  
				String days = PropertiesUtils.getInstance().readProperty("access.users.password_expiration_days","30");
//				if (days==null || days.equals("")) days = "30"; // default
				int daysInt = Integer.parseInt(days);
				if (((new Date()).getTime() - vo.getPasswordChangeDate().getTime()) >  (((long)daysInt)*24*60*60*1000)) {
					// Password scaduta
					return true;
				}
			}
			
			if (vo!=null && !vo.isLdapUser() && !vo.isAdfsUser() && vo.getPasswordChangeDate()==null) { // FIXME se si vuole che al primo accesso la password venga cambiata allora abilitare questo! 
				// Primo accesso
				return true;
			}
		}
		
		return false;
	}
	
	
	public boolean isUserAdmin() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication!=null && authentication.getAuthorities()!=null) {
			//String currentPrincipalName = authentication.getName();
			for (GrantedAuthority role : authentication.getAuthorities()) {
				System.out.println("Ruolo:"+role.getAuthority());
				if (role.getAuthority()!=null && role.getAuthority().equals("ROLE_ADMIN")) {
					System.out.println("Amministratore !!!");
					return true;
				}
			}
		}
		return false;
	}


	

	public String getClientBrowserInfo() {
		return clientBrowserInfo;
	}

	public void setClientBrowserInfo(String clientBrowserInfo) {
		this.clientBrowserInfo = clientBrowserInfo;
	}
	
	
	
	public int getRemoteClientScreenResolutionWidth() {
		int screenWidth = -1;
		 if (getClientBrowserInfo()!=null) {
         	String screenSize = getClientBrowserInfo().substring(getClientBrowserInfo().indexOf("Screensize: ")+12);
         	//System.out.println("screenSize:" + screenSize);
         	if (screenSize!=null) {
         		String screenWidthStr = screenSize.substring(0, screenSize.indexOf("x"));
         		if (screenWidthStr!=null) {
         			//System.out.println("screenWidthStr:" + screenWidthStr);
         			screenWidth = Integer.parseInt(screenWidthStr);
         		}
         	}
         }
		 return screenWidth;
	}
	public int getRemoteClientScreenResolutionHeight() {
		int screenHeight = -1;
		 if (getClientBrowserInfo()!=null) {
         	String screenSize = getClientBrowserInfo().substring(getClientBrowserInfo().indexOf("Screensize: ")+12);
         	//System.out.println("screenSize:" + screenSize);
         	if (screenSize!=null) {
         		String screenHeightStr = screenSize.substring(screenSize.indexOf("x")+1);
         		if (screenHeightStr!=null) {
         			//System.out.println("screenHeightStr:" + screenHeightStr);
         			screenHeight = Integer.parseInt(screenHeightStr);
         		}
         	}
         }
		 return screenHeight;
	}
	private int getRemoteClientScreenResolutionWidthPercent(int percent) {
		return (getRemoteClientScreenResolutionWidth()/100)*percent;
		
	}
	public int getRemoteClientScreenResolutionWidth20Percent() {
		return getRemoteClientScreenResolutionWidthPercent(20);
		
	}
	public int getRemoteClientScreenResolutionWidth80Percent() {
		return getRemoteClientScreenResolutionWidthPercent(80);
		
	}

	public String getClientBrowserPluginsStatus() {
		return clientBrowserPluginsStatus;
	}

	public void setClientBrowserPluginsStatus(String clientBrowserPluginsStatus) {
		this.clientBrowserPluginsStatus = clientBrowserPluginsStatus;
	}



	public boolean isAppSideShowIntercome() {
		return appSideShowIntercome;
	}


	public void setAppSideShowIntercome(boolean appSideShowIntercome) {
		this.appSideShowIntercome = appSideShowIntercome;
	}


	public boolean isAppSideShowCameraShot() {
		return appSideShowCameraShot;
	}


	public void setAppSideShowCameraShot(boolean appSideShowCameraShot) {
		this.appSideShowCameraShot = appSideShowCameraShot;
	}


	public boolean isAppSideShowScan() {
		return appSideShowScan;
	}


	public void setAppSideShowScan(boolean appSideShowScan) {
		this.appSideShowScan = appSideShowScan;
	}


	public int getMenuIndex() {
		return menuIndex;
	}


	public void setMenuIndex(int menuIndex) {
		this.menuIndex = menuIndex;
	}

}
