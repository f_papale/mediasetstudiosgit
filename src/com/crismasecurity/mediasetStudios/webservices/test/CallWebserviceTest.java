/**
 * 
 */
package com.crismasecurity.mediasetStudios.webservices.test;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64;

import com.crismasecurity.mediasetStudios.webservices.Foo;
import com.google.gson.Gson;

/**
 * Class name: CallWebserviceTest
 *
 * Description: 
 * 
 *
 * Company: CrismaItalia
 *
 * @author Crisma
 * @date 29/gen/2016
 *
 */
public class CallWebserviceTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		String address="127.0.0.1";
		//String address="192.168.1.125";
		
		
		if (args!=null && args.length >0) {
			int i=0;
			for (String s: args) {
				if (i==0) address = s;
	            System.out.println(i+" - " + s);
	            
	            i++;
	        }
		}
		System.out.println("Usage: CallWebserviceTest [ip]");
		System.out.println();
		System.out.println("address: ex. 127.0.0.1 (default)");
		System.out.println();
		System.out.println();
		System.out.println("Acquired data:");
		System.out.println("address: " +address);
		
		
		Foo objToSend = new Foo();
		objToSend.setPassword("ZXVSXCV");
		objToSend.setUsername("ffff");
		
		try {
			sendPost(address,objToSend);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	private static void sendPost(String address, Foo objToSend) throws Exception {

		//String url = "http://"+address+":8080/MediasetstudiosApplicationGateway/webservices/MediasetStudiosAgWebServices/UpdateFooX";
		String url = "http://"+address+":8080/MediasetstudiosApplicationGateway/webservices/MediasetStudiosAgWebServices/authUpdateFooX";
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		//add reuqest header
		con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/json");
		//Quello che segue deve essere chiamato solamente se la chiamata è con autorizzazione
		con.setRequestProperty("Authorization", "Basic "+ java.util.Base64.getEncoder().encodeToString("test:ssss".getBytes()));
		
		Gson gson = new Gson();
		

		// Java object to JSON, and assign to a String
		String urlParameters = gson.toJson(objToSend);
		System.out.println(urlParameters);
		
		
		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		
		//print result
		System.out.println(response.toString());

	}
	
}
