package com.crismasecurity.mediasetStudios.webservices.reportMaker;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * dati provenienti da SSIS
 * 
 * 
 * - emailID nome della Email per la quale creare il report
 * - 
 *
 * @author Crisma
 *
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReportMakerJson {

    private Long emailID;


    /**
     * 
     */
	public ReportMakerJson() {
		super();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((emailID == null) ? 0 : emailID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReportMakerJson other = (ReportMakerJson) obj;
		if (emailID == null) {
			if (other.emailID != null)
				return false;
		} else if (!emailID.equals(other.emailID))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ReportMakerJson [emailID=" + emailID + "]";
	}

	public Long getEmailID() {
		return emailID;
	}

	public void setEmailID(String emailID) {
		this.emailID = Long.parseLong(emailID);
	}

  

}

