package com.crismasecurity.mediasetStudios.webservices.reportMaker;

import com.crismasecurity.mediasetStudios.core.entity.Location;
import com.crismasecurity.mediasetStudios.core.entity.Media;

public interface ReportMakerListener {

	//public void eventReceived(Location location, Media media, String error);
	public void eventReceived(String base64Report, String error);
	
}
