/**
 * 
 */
package com.crismasecurity.mediasetStudios.webservices;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.crismasecurity.mediasetStudios.core.entity.BadgesTransits;
import com.crismasecurity.mediasetStudios.core.entity.BlackListSpectator;
import com.crismasecurity.mediasetStudios.core.entity.EmailQueue;
import com.crismasecurity.mediasetStudios.core.entity.Episode;
import com.crismasecurity.mediasetStudios.core.entity.Location;
import com.crismasecurity.mediasetStudios.core.entity.Media;
import com.crismasecurity.mediasetStudios.core.entity.SpectatorEpisode;
import com.crismasecurity.mediasetStudios.core.entity.Statistics;
import com.crismasecurity.mediasetStudios.core.services.BadgesTransitsService;
import com.crismasecurity.mediasetStudios.core.services.EmailQueueService;
import com.crismasecurity.mediasetStudios.core.services.LocationService;
import com.crismasecurity.mediasetStudios.core.services.MediaService;
import com.crismasecurity.mediasetStudios.core.services.MediaStatusTypeService;
import com.crismasecurity.mediasetStudios.core.services.SpectatorEpisodeService;
import com.crismasecurity.mediasetStudios.core.services.StatisticsService;
import com.crismasecurity.mediasetStudios.core.services.EpisodeService;
import com.crismasecurity.mediasetStudios.core.utils.DateUtils;
import com.crismasecurity.mediasetStudios.core.utils.PropertiesUtils;

import com.crismasecurity.mediasetStudios.webservices.desktopReader.DesktopReaderJson;
import com.crismasecurity.mediasetStudios.webservices.desktopReader.DesktopReaderListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.crismasecurity.mediasetStudios.web.utils.reporting.JasperReportDetails;
import com.crismasecurity.mediasetStudios.web.utils.reporting.JasperReportsFactory;

/**
 * Class name: DomusWebServices
 *
 * Description:  ATTT!!!!!!!   tutti i servizi che iniziano per auth sono sottoposti ad autenticazione (vedi file applicationSecurity.xml), ad esempio authGetData, authTest, authPippo etc.
 *				Lato client che richiama il servizio nell'header va inserito il tag "Authorization: Basic XXXXX"   dove XXXXX � calcolato come Base64(<username>+":"+<password>)
 *				Per l'autenticazione, se ne occupa CustomAuthenticationProviderForREST ---> da finire di implementare
 *
 * Company: 
 *
 * @author Crisma
 * @date 15/nov/2013
 *
 */
@Controller // Oppure mettere @RestController cosi' non c'� bisogno di mettere @ResponseBody su ogni metodo
@RequestMapping("/MediasetStudiosWebServices")
public class MediasetStudiosWebServices {

	@Autowired
	private BadgesTransitsService badgesTransitsService;

	@Autowired
	private LocationService locationService;
	
	@Autowired
	private MediaService mediaService;
	
	@Autowired
	private MediaStatusTypeService mediaStatusTypeService;
	
	@Autowired
	private EmailQueueService emailQueueService;
	
	@Autowired
	private EpisodeService episodeService;
	
	@Autowired
	private SpectatorEpisodeService spectatorEpisodeService;
	
	@Autowired
    private JasperReportsFactory jasperReportsFactory;	
	
	@Autowired
	private StatisticsService statisticsService;
		
	private List<DesktopReaderListener> desktopReaderListeners;
//	private List<ReportMakerListener> reportMakerListeners;
	private List<AntennaTransitsListener> antennaTransitsListeners;

	@PostConstruct
	private void init() {
		System.out.println("Init di MediasetStudiosWebServices");
		desktopReaderListeners = new ArrayList<DesktopReaderListener>();
//		reportMakerListeners = new ArrayList<ReportMakerListener>();
		antennaTransitsListeners = new ArrayList<AntennaTransitsListener>();
	}
	

	@RequestMapping(method=RequestMethod.POST, headers="Accept=application/json", value="/updateFooX")
	public @ResponseBody Foo updateFooX(@RequestBody Foo foo) {
		
		// Url da richiamare IN PUT: <indirizzo>:8080/MediasetStudios/webservices/MediasetStudiosWebServices/updateFooX
		
		foo.setUsername("hashahahaha");
	    return foo;
	}

	
	/****************************** ANTENNE ********************************************************/
	@RequestMapping(method=RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.TEXT_PLAIN_VALUE, value="/records")
	public @ResponseBody String records(@RequestBody List<BookingRecord> bookingRecords) {
		
		// Url da richiamare IN POST: <indirizzo>:8080/MediasetStudios/webservices/MediasetStudiosWebServices/records
		try {
			
		
			if (bookingRecords!=null) {
				if (bookingRecords.size()==0) System.out.println("MediasetStudiosWebServices - records: bookingRecords list is empty");
				for (BookingRecord rec : bookingRecords) {
					System.out.println("Record: " + rec);
					
					BadgesTransits transito = new BadgesTransits();
					transito.setAntennaTimeStamp(rec.getDatestamp());
					transito.setMediaId(rec.getTagId());
					transito.setReaderAntenna(rec.getReaderAntenna());
					transito.setReaderIpAddress(rec.getiPaddress());
					transito.setTimeStamp(rec.getTimeStamp());
					transito.setTransitType(rec.getType());
					
					Media media = mediaService.getMediaByCode(rec.getTagId());
					if (media!=null) {
						transito.setMediaLabel(media.getLabel());
						
	//					SpectatorEpisode sep = spectatorEpisodeService.getSpectatorEpisodeFinder().and("media.id").eq(media.getId()).setDistinct().result();
						List<SpectatorEpisode> seps = spectatorEpisodeService.getSpectatorEpisodeFinder().and("media.id").eq(media.getId()).setDistinct().list();
						SpectatorEpisode sep = null;
						if (seps!=null && seps.size()>0) sep = seps.get(0);
						if (sep!=null) {
							if (sep.getProduction()!=null) transito.setProductionName(sep.getProduction().getName());
							if (sep.getSpectator()!=null) transito.setSpectator(sep.getSpectator().getSurname() + "" + sep.getSpectator().getName());
							if (sep.getEpisode()!=null) transito.setEpisode(sep.getEpisode().getDateFromOnlyDate() + " " + sep.getEpisode().getDateFromOnlyHours());
							
							//Se è una restituzione di badge allora
							if (rec.getType()!=null && rec.getType().equalsIgnoreCase("return")){
								// Urna di restituzione
//								Libera il badge
								media.setMediaStatusType(mediaStatusTypeService.getMediaStatusTypeByIdStatus(1)); // stato libero
								try {
									mediaService.updateMedia(media);
								} catch (Exception e2) {
									e2.printStackTrace();
								}
								sep.setMedia(null);
								sep.setBadgeReturnDate(new Date());
//								Se non è stato fatto il CheckOut allora lo faccio
								try {
									
									if(!sep.isCheckout()) {
										sep.setCheckout(true);
										sep.setCheckoutDate(new Date());
									}
									
									sep = spectatorEpisodeService.updateSpectatorEpisode(sep);
									
									// avverto listeners del return (checkout)
									for (AntennaTransitsListener l : antennaTransitsListeners) {
										l.transitCheckoutReceived(sep);
									}
								} catch (Exception e1) {
									e1.printStackTrace();
								}
							}
//							Se invece è un check antenna finale 
							else if (rec.getType()!=null && rec.getType().equalsIgnoreCase("check")){							
								// avverto listeners della mancata riconsegna (check)
								for (AntennaTransitsListener l : antennaTransitsListeners) {
									l.transitNoReturnedReceived(sep);
								}
							}
//-----------------------------------------------------------------------------------------------------------------------------------------------							
							else if (rec.getType()!=null && rec.getType().equalsIgnoreCase("hybrid")) {
								
								System.out.println("Transito sotto hybrid...");		
								
								if (sep.getEpisode()==null) transito.setError("Transito su hybrid...Episodio non selezionato");
								else {
									
//								In questa circostanza si presume che il CheckOut venga sempre fatto dall'antenna di tipo "return"
//									
//								Se sono in fase di CheckIn
//									Se NON ha fatto il CheckIn --> Fa il CheckIn
//								    Se HA fatto già il CheckIn
//								    	Se sono passati più di 10min --> Perchè se ne sta andando
//											Se HA restituito il badge e quindi ha fatto il checkOut --> NOP
//											Se NON ha restituito il badge e quindi NON fatto il checkOut --> Allarme
//										Se NON sono passati più di 10min --> allora staziona sotto l'antenna...
//									                    ma se ne potrebbe anche andare quindi è inutile il Timer perchè deve dare comunque allarme!!! :)
//							    Se sono in fase di CheckOut
//									Se ha fatto il checkIn
//										Se HA restituito il badge e quindi ha fatto il checkOut --> NOP
//										Se NON ha restituito il badge e quindi NON fatto il checkOut --> Allarme	
									
									System.out.println("Transito sotto hybrid per spettacolo: " + sep.getEpisode().getProduction().getName());
//									Se sono in fase di CheckIn
									if (sep.getEpisode().isCheckinStarted()) {
										System.out.println("Transito sotto hybrid - Processo di CheckIn iniziato....");	
//										Se NON ha fatto il CheckIn --> Fa il CheckIn
										if (sep.getEntranceDate()==null) { 
											sep.setCheckin(true);
											if (sep.getCheckinDate()==null) sep.setCheckinDate(new Date());
											sep.setEntranceDate(new Date());
											System.out.println("Transito sotto hybrid - Eseguito CheckIn ....");
										} else {
//											Calcolo da quanto tempo ha fatto il CheckIn
											System.out.println("Transito sotto hybrid - Eseguito CheckIn poco fà....");
											Calendar cal = Calendar.getInstance();
											long traTimeLong = (10*60*1000); // default 10 min
											String traTime = PropertiesUtils.getInstance().readProperty("antenna.transits.minimum_time");
											if (traTime!=null && !traTime.trim().equals("")) {
												traTimeLong = (10*1000); // default 10 min
											}
//											Se sono passati più di 10min allora immagino che la persona se ne sta andando ha già riconsegnato il Badge facendo il checkout. 	
											if ((cal.getTimeInMillis()-sep.getEntranceDate().getTime())> traTimeLong) {
												System.out.println("Transito sotto hybrid - Eseguito CheckIn più di 10min fa e sta forse uscendo...");
//												Se NON ha restituito il badge e quindi NON fatto il checkOut --> Allarme
												if (sep.getBadgeReturnDate()==null) {
													System.out.println("Transito sotto hybrid - Eseguito CheckIn più di 10min fa e sta uscendo senza aver riconsegnato il badge... ALLARME.");
													for (AntennaTransitsListener l : antennaTransitsListeners) {
														l.transitNoReturnedReceived(sep);
													}
												}
											} else {
												System.out.println("Transito hybrid scartato! ...stazionamento nei pressi dell'antenna!");											
											}
										}
//									        Se sono in fase di CheckOut										
									} else if (sep.getEpisode().isCheckoutStarted()) {
//										Se ha fatto il checkIn										
										if (sep.getEntranceDate()!=null) {
//											Se NON ha restituito il badge e quindi NON fatto il checkOut --> Allarme											
											if (sep.getBadgeReturnDate()==null) {
												System.out.println("Transito sotto hybrid - Eseguito CheckIn ma badge non riconsegnato... ALLARME. ");
												for (AntennaTransitsListener l : antennaTransitsListeners) {
													l.transitNoReturnedReceived(sep);
												}												
											}
										}
									}
								}
							}						
							else {
//-----------------------------------------------------------------------------------------------------------------------------------------------								
								// altrimenti gestisce il checkin/checkout 
							
								boolean callListners = false;
								boolean callListnersIsCheckout = false;
								// Marco Checkin o checkout
								if (sep.getEpisode()==null) transito.setError("Episodio non selezionato");
								else {
//									Se è iniziato il checkOut allora imposta che sta facendo il checkout
									if (sep.getEpisode().isCheckoutStarted()) {
										sep.setCheckout(true);
										sep.setCheckoutDate(new Date());
										
										callListners = true;
										callListnersIsCheckout = true;
									}
//									Se è sono in checkIn e il checkOut non è ancora iniziato allora 
									else if (sep.getEpisode().isCheckinStarted()) {
//										Se la persona ha fatto già il CheckIn quindi è entrata
										if (sep.getEntranceDate()!=null) { // sep.isCheckin()
											// verifico che lo spettatore non stazioni nei pressi dell'antenna e quindi scarto i passaggi più vicini di x minuti
											Calendar cal = Calendar.getInstance();
											long traTimeLong = (10*60*1000); // default 10 min
											String traTime = PropertiesUtils.getInstance().readProperty("antenna.transits.minimum_time");
											if (traTime!=null && !traTime.trim().equals("")) {
												//traTimeLong = Long.parseLong(traTime)*60*1000;
//!!!!!!!!!!!!!!!!!TODO da rimettere apposto!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
												traTimeLong = (10*1000); // default 10 min
											}
//											Se sono passati più di 10min allora immagino che la persona se ne sta andando e ne faccio il Checkout
											if ((cal.getTimeInMillis()-sep.getEntranceDate().getTime())> traTimeLong) {
//												Se sono passati più di 10 minuti allora considero che sta facendo un checkout
												sep.setCheckout(true);
												sep.setCheckoutDate(new Date());
												
												callListners = true;
												callListnersIsCheckout = true;
											}
//											Altrimenti sta camminando nei paraggi dell'antenna
											else System.out.println("Transito scartato! ...stazionamento nei pressi dell'antenna!");
										}
//										Se la persona non ha fatto ancora il CheckIn allora immagino che lo stia facendo ora
										else {
											sep.setCheckin(true);
											if (sep.getCheckinDate()==null) sep.setCheckinDate(new Date());
											sep.setEntranceDate(new Date());
											
											callListners = true;
											callListnersIsCheckout = false;
										}
									}
									else transito.setError("Checkin non iniziato");
								}
								// Salvo lo stato e 
								try {
									sep = spectatorEpisodeService.updateSpectatorEpisode(sep);
									
									
									if (callListners) {
										if (callListnersIsCheckout) {
											// avverto listeners del checkout
											for (AntennaTransitsListener l : antennaTransitsListeners) {
												l.transitCheckoutReceived(sep);
											}
										}
										else {
											// avverto listeners del checkin
											for (AntennaTransitsListener l : antennaTransitsListeners) {
												l.transitCheckinReceived(sep);
											}
										}
									}
									
									
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						}
						else transito.setError("Badge non assegnato");
					}
					else transito.setError("Badge non in anagrafica");
					
					
					try {
						badgesTransitsService.addBadgesTransits(transito);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			else System.out.println("MediasetStudiosWebServices - records: bookingRecords list is null!");
		} catch (Throwable e) {
			
			e.printStackTrace();
		}

	    return "OK";
	}
	/***********************************************************************************************/
	
	
	
	
	/******************************* LETTORE DA TAVOLO ************************************
	 *  
	 */
	
	@RequestMapping(method=RequestMethod.POST, consumes = "application/json", produces = "application/json", value="/desktopReader")
	public @ResponseBody String desktopReader(@RequestBody DesktopReaderJson event) {
		
		// Url da richiamare IN POST: http://<indirizzo>:8080/MediasetStudios/webservices/MediasetStudiosWebServices/desktopReader
		
		
		System.out.println("Evento da lettore da tavolo: " + event);
		
		Location loc = locationService.getLocationByName(event.getDeviceId());
		Media media = mediaService.getMediaByCode(event.getTagId());
		
		if (loc!=null && media !=null) {
			
			boolean dataValid = true;
			if (media.getStartDate()!=null && (new Date()).before(media.getStartDate())) dataValid= false;
			else if (media.getEndDate()!=null && (new Date()).after(media.getEndDate())) dataValid= false;
			
			if (media.getMediaStatusType()!=null /*&& media.getMediaStatusType().getIdStatus()==1*/ && media.isActive() && dataValid) {
				for (DesktopReaderListener l : desktopReaderListeners) {
					l.eventReceived(loc, media, null,event.getTagId());
				}
			}
			else {
//				if (media.isActive() && dataValid) {
//					for (DesktopReaderListener l : desktopReaderListeners) {
//						l.eventReceived(loc, media, "Impossibile assegnare il badge. Si trova nello stato: " + media.getMediaStatusType().getDescription());
//					}
//				}
//				else 
				if (dataValid) {
					for (DesktopReaderListener l : desktopReaderListeners) {
						l.eventReceived(loc, media, "Azione non permessa. Badge non attivo!", event.getTagId());
					}
				}
				else {
					for (DesktopReaderListener l : desktopReaderListeners) {
						l.eventReceived(loc, media, "Azione non permessa. Data attuale non compatibile con la data di validità del badge!", event.getTagId());
					}
				}
			}
		    return "{\"Status\":\"ok\"}";
		}
		else {
			if (loc==null) return "{\"Status\":\"no device found!\"}";
			else {
				for (DesktopReaderListener l : desktopReaderListeners) {
					l.eventReceived(loc, null, "Badge con id '"+event.getTagId()+"'  non trovato in anagrafica",event.getTagId());
				}
				
				return "{\"Status\":\"no tag found!\"}";
			}
		}
	}
	

	public void addDesktopReaderListener(DesktopReaderListener beanInstance) {
		desktopReaderListeners.add(beanInstance);
	}


	public void removeDesktopReaderListener(DesktopReaderListener beanInstance) {
		desktopReaderListeners.remove(beanInstance);
	}
	

	public List<DesktopReaderListener> getDesktopReaderListeners() {
		return desktopReaderListeners;
	}

	
	/*************************************************************************/

	
	
	@RequestMapping( value="/getData2", method=RequestMethod.GET, produces=MediaType.TEXT_PLAIN_VALUE)
    public @ResponseBody String getData2() {
		
		// Per richiamarlo, dal browser: http://127.0.0.1:8080/MediasetstudiosApplicationGateway/webservices/MediasetStudiosAgWebServices/getData2
		
		System.out.println("getData2 ...");
		return "ppppppppppp";
    }
	
	/******************************* WIPE DATA ************************************
	 *  
	 */	
	@RequestMapping( value="/wipeData", method=RequestMethod.GET, produces=MediaType.TEXT_PLAIN_VALUE)
    public @ResponseBody String wipedata() {
		
//		Per richiamarlo, dal browser: http://127.0.0.1:8080/MediasetstudiosApplicationGateway/webservices/MediasetStudiosAgWebServices/wipedata
//		Può essere richiamato anche attraverso la classe com.crismasecurity.mediasetStudios.webservices.test.CallWebserviceTest
//		oppure tramite curl
//		curl -H "Content-Type: application/json" -X POST --url http://localhost:8080/MediasetStudios/webservices/MediasetStudiosWebServices/wipeData
			
//		System.out.println("WipeData ...");
		String response=null;
		try {
//			WipeData wipeData = new WipeData();
//			wipeData.setDateRef(DateUtils.getDate());
//			wipeData.doWipe();
			response="{\"Status\":\"ok\"}";
		} catch (Exception e) {
			response="{\"Status\":\"nok\"}";
			// TODO: handle exception
		}
		return response;
    }
	
	
	//Questa è la versione con l'autenticazione integrata.	
	@RequestMapping(method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE,  value="/authWipeData")
	public @ResponseBody String authWipeData() {
//		Per richiamarlo usare un tool tipo "Advanced REST client". 
//		immettere come indirizzo: <indirizzo>:8080/MediasetStudiosApplicationGateway/webservices/MediasetStudiosWebServices/authWipeData
//		come metodo : POST
//		come header aggiungere: Content-Type---> application/json
//		
//		Può essere richiamato anche attraverso la classe com.crismasecurity.mediasetStudios.webservices.test.CallWebserviceTest
//		oppure tramite curl
//		curl -H "Content-Type: application/json" -H "authorization: Basic YWRtaW46VmlhcG9uekAyMDE4" -X POST --url http://localhost:8080/MediasetStudios/webservices/MediasetStudiosWebServices/authWipeData
		
		String response=null;
		System.out.println("authWipeData...");
		try {
//			WipeData wipeData = new WipeData();
//			wipeData.setDateRef(DateUtils.getDate());
//			wipeData.doWipe();
			response="{\"Status\":\"ok\"}";
		} catch (Exception e) {
			// TODO: handle exception
			response="{\"Status\":\"nok\"}";
		}
		return response;
	}
	
	@RequestMapping(method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE, value="/getStatistics")
	public @ResponseBody String getStatistics(@RequestBody String domain) {
		
		// Per richiamarlo usare un tool tipo "Advanced REST client". immettere come indirizzo: <indirizzo>:8080/MediasetStudios/webservices/MediasetStudiosWebServices/getStatistics
		// come metodo : POST
		// come body : unwanted 
		// come body content type: text/plain
		// come headers: Content-Type: application/json
		
		// Può essere richiamato anche attraverso la classe com.crismasecurity.mediasetStudios.webservices.test.CallWebserviceTest
		SimpleDateFormat dt1 = new SimpleDateFormat("yyyy/MM/dd"); 
		List<Statistics> statistics = statisticsService.getStatisticsByDomain(domain);
		statistics.forEach(s ->
			{
				try {
					s.setUpdatedOn(dt1.parse(dt1.format(s.getUpdatedOn())));	
				} catch (Exception e) {
					e.printStackTrace();
				}
			}	
		);

		Gson gson = new GsonBuilder()
				  .excludeFieldsWithoutExposeAnnotation()
				  .create();
		String json = gson.toJson(statistics );
		return json;
	}
	
	
	@RequestMapping(method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE, consumes=MediaType.TEXT_PLAIN_VALUE, value="/getStatisticsTextExt")
	public @ResponseBody String getStatisticsText() {
		
		HashMap<String, String> vars= new HashMap<String, String>();
		SimpleDateFormat dt1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		
		String message = "<p>Studios - Importazione Persone Indesiderate " + dt1.format(new Date()) +"</p>";
		// Può essere richiamato anche attraverso la classe com.crismasecurity.mediasetStudios.webservices.test.CallWebserviceTest
		statisticsService.getStatisticsByDomain("unwanted").forEach(s-> vars.put(s.getMeasure(),s.getValue()));
		Integer res = Integer.parseInt( vars.get("loadedUnwanted"))-Integer.parseInt(vars.get("onErrorUnwanted"));
//		Studios - Importazione Persone Indesiderate 2021/10/08 02:52:58</p>
		message +="<p><br></p>";	
		message +="<p>Aggiornate: "+vars.get("updatedUnwanted")+"</p>";
		message +="<p>Aggiunte: "+vars.get("addedUnwanted")+"</p>";
		message +="<p>Cancellate: "+vars.get("deletedUnwanted")+"</p>";
		message +="<p><br></p>";
		message +="<p>Totale processati: "+res+"</p>";

		return "{\"message\":\""+message+"\"}";
	}
	
	
	@RequestMapping(
			method=RequestMethod.GET
			, value="/getStatisticsText/{domain}"
			)
	@ResponseBody 
	public String getStatisticsFullText(
			@PathVariable String domain) {
		// Per richiamarlo usare un tool tipo "Advanced REST client". immettere come indirizzo: <indirizzo>:8080/MediasetStudios/webservices/MediasetStudiosWebServices/getStatisticsText/unwanted		
		HashMap<String, String> vars= new HashMap<String, String>();
		Integer res=0;
		Statistics dummy =null;
		
		SimpleDateFormat dt1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		domain = domain.trim();
		String message ="";
		switch (domain) {
			case "": case "unwanted":
				dummy = Collections.max(statisticsService.getStatisticsByDomain("unwanted"), Comparator.comparing(Statistics::getUpdatedOn));
				// Può essere richiamato anche attraverso la classe com.crismasecurity.mediasetStudios.webservices.test.CallWebserviceTest
				statisticsService.getStatisticsByDomain("unwanted").forEach(s-> vars.put(s.getMeasure(),s.getValue()));
				res = Integer.parseInt( vars.get("loadedUnwanted"))-Integer.parseInt(vars.get("onErrorUnwanted"));
//				Studios - Importazione Persone Indesiderate 2021/10/08 02:52:58</p>
				message ="<p><br></p>";	
				message +="<p>Aggiornate: "+vars.get("updatedUnwanted")+"</p>";
				message +="<p>Aggiunte: "+vars.get("addedUnwanted")+"</p>";
				message +="<p>Cancellate: "+vars.get("deletedUnwanted")+"</p>";
				message +="<p><br></p>";
				message +="<p>Totale processati: "+ res +"</p>";
				
				message = "<p>Studios - Importazione Persone Indesiderate " + dt1.format(dummy.getUpdatedOn()) +"</p>"+message;
				message="{\"message\":\""+message+"\"}";
				break;
			default:
				dummy = Collections.max(statisticsService.getStatisticsByDomain("unwanted"), Comparator.comparing(Statistics::getUpdatedOn));
				// Può essere richiamato anche attraverso la classe com.crismasecurity.mediasetStudios.webservices.test.CallWebserviceTest
				statisticsService.getStatisticsByDomain("unwanted").forEach(s-> vars.put(s.getMeasure(),s.getValue()));
				res = Integer.parseInt( vars.get("loadedUnwanted"))-Integer.parseInt(vars.get("onErrorUnwanted"));
//				Studios - Importazione Persone Indesiderate 2021/10/08 02:52:58</p>
				message ="<p><br></p>";	
				message +="<p>Aggiornate: "+vars.get("updatedUnwanted")+"</p>";
				message +="<p>Aggiunte: "+vars.get("addedUnwanted")+"</p>";
				message +="<p>Cancellate: "+vars.get("deletedUnwanted")+"</p>";
				message +="<p><br></p>";
				message +="<p>Totale processati: "+res+"</p>";
				
				message = "<p>Studios - Importazione Persone Indesiderate " + dt1.format(dummy.getUpdatedOn()) +"</p>" + message;
				message="{\"message\":\""+message+"\"}";
				break;
		}
		return message;
		

	}
		
	/******************************* REPORT MAKER ************************************
	 *  
	 */
	
//	@RequestMapping(method=RequestMethod.POST, consumes = "application/json", produces = "application/json", value="/reportMaker2")
//	public @ResponseBody String reportMaker2(@RequestBody ReportMakerJson event) {
//		
//		// Url da richiamare IN PUT: http://<indirizzo>:8080/MediasetStudios/webservices/MediasetStudiosWebServices/desktopReader
//		// Per richiamarlo usare un tool tipo "Advanced REST client". immettere come indirizzo: <indirizzo>:8080/MediasetStudios/webservices/MediasetStudiosAgWebServices/updateFooX
//		// come metodo : POST
//		// come header aggiungere: Content-Type---> application/json
//		// Può essere richiamato anche attraverso la classe com.crismasecurity.mediasetStudios.webservices.test.CallWebserviceTest	
//		
//		System.out.println("Evento creazione Report: " + event);
//		String response="{\"Status\":\"ok\"}";
//		//Recupero l'Email
//		EmailQueue email = emailQueueService.getEmailQueueById(event.getEmailID());
//		//Recupero l'oggetto Episode
//		
//		//Recupero l'oggetto Episode
//		
//		if (email!=null) {
//			response= MakeReportEpisode(email);	
//		} else {
//			response="{\"Status\":\"nok\"}";
//		}
//		return response;
//	}
	
//	public void addReportMakerListener(ReportMakerListener beanInstance) {
//		reportMakerListeners.add(beanInstance);
//	}
//
//
//	public void removeReportMakerListener(ReportMakerListener beanInstance) {
//		reportMakerListeners.remove(beanInstance);
//	}
//	
//
//	public List<ReportMakerListener> getReportMakerListeners() {
//		return reportMakerListeners;
//	}
	
	@RequestMapping( value="/reportMaker", method=RequestMethod.GET, produces=MediaType.TEXT_PLAIN_VALUE)
    public @ResponseBody String reportMaker(@RequestParam(value = "id", required = true) Long idEmail) {
		
		
		String response="{\"Status\":\"ok\"}";
		//Recupero l'Email
		EmailQueue email = emailQueueService.getEmailQueueById(idEmail);
		//Recupero l'oggetto Episode
		
		if (email!=null) {
			response= MakeReportEpisode(email);	
			if (response==null) response="{\"Status\":\"nok\"}";
		} else {
			response="{\"Status\":\"nok\"}";
		}
		return response;
	}

	private String MakeReportEpisode(EmailQueue emailToSend) {
		
		String sReturn=null;
		String reportDescription=null;
		String reportFileName=null;
		Boolean bContinue=false;
		Collection<Episode> beansEpisode = new ArrayList<Episode>();
		Collection<SpectatorEpisode> beansSpectatorEpisode = new ArrayList<SpectatorEpisode>();
		
		switch (emailToSend.getEntityType()) {
		case 1:
			reportDescription="Lista Spettatori";
			reportFileName= PropertiesUtils.getInstance().readProperty("report.listaSpettatori").trim();
			Episode episodeToInvite = episodeService.getEpisodeById(emailToSend.getEntityID());
			bContinue=(episodeToInvite!=null);  
            // Dati da visualizzare (un oggetto per ogni areaDetail in iReport)
			beansEpisode.add(episodeToInvite);
			break;
		case 2:
			reportDescription="Invito Spettatore";
			reportFileName= PropertiesUtils.getInstance().readProperty("report.invitoSpettatore").trim();
			SpectatorEpisode spectatorEpisodeToInvite = spectatorEpisodeService.getSpectatorEpisodeById(emailToSend.getEntityID());
			bContinue=(spectatorEpisodeToInvite!=null);  
            // Dati da visualizzare (un oggetto per ogni areaDetail in iReport)
			beansSpectatorEpisode.add(spectatorEpisodeToInvite);
			break;
		default:
			break;
		}
		
		if (bContinue)  {
			try {
				System.out.println("Genero pdf...");
				JasperReportDetails det = new JasperReportDetails();
	            
				// Nome report
	            det.setDescription(reportDescription);
	            
	            // Path report
	            det.setMasterReportUrl(File.separator+"WEB-INF"+File.separator+"reports"+File.separator+"spectator"+File.separator+reportFileName+".jasper");
	            
	            // Subreports url
	            Properties srUrls = new Properties();
	            //srUrls.put("SUBREPORT_URL", File.separator+"WEB-INF"+File.separator+"reports"+File.separator+"invite"+File.separator+"subRep.jasper");
	            
	            det.setSubReportUrls(srUrls);
	            
	            // Parametri da passare al report
	            Map<String, Object> reportParameters = new HashMap<String, Object>();
	            
	            reportParameters.put("REPORT_RESOURCES_PATH", getAbsolutePath()+ File.separator+"WEB-INF"+File.separator+"reports"+File.separator+"spectator"+File.separator);	            
//	            reportParameters.put("SPECTATOR", episodeToInvite);
//	            reportParameters.put("SIGN", new java.io.ByteArrayInputStream(redrawSignature(extractSignature(spectatorToEdit.getSign()))));
	            
//	            if (spectatorToEdit.getSpectator().getDocumentPath()!=null && !spectatorToEdit.getSpectator().getDocumentPath().equals(""))
//	            	reportParameters.put("ID_DOCUMENT", new java.io.ByteArrayInputStream(FileUtils.getBytesFromFile(filesDir+"/"+spectatorToEdit.getSpectator().getDocumentPath())));
//	            else reportParameters.put("ID_DOCUMENT", null);
//	            if (doc!=null)
//	            	reportParameters.put("ID_DOCUMENT", new java.io.ByteArrayInputStream(doc));
//	            else reportParameters.put("ID_DOCUMENT", null);	            
	            
	            det.setReportParameters(reportParameters);
	            

	            Map<String, Object> reportParams = new HashMap<String, Object>();
	            reportParams.put("REPORT_TITLE", reportDescription);
	            ByteArrayOutputStream baos=null;
	            switch (emailToSend.getEntityType()) {
				case 1:
					baos= jasperReportsFactory.complieReportAsPdfByteArrayOutputStream(det, reportParams, beansEpisode);
					break;
				case 2:
					baos= jasperReportsFactory.complieReportAsPdfByteArrayOutputStream(det, reportParams, beansSpectatorEpisode);
					break;
				default:
					break;
				} 
	            
	            if (baos!=null) {
		            // Solo per test !!!!!!!!!!!!!!!!
		            //FileUtils.writeToFile("D:\\"+reportFileName+".pdf", new ByteArrayInputStream(baos.toByteArray()));
		            
		            sReturn=java.util.Base64.getEncoder().encodeToString(baos.toByteArray());
		            if (sReturn!=null) {
			            emailToSend.setSendDate(java.sql.Date.valueOf(LocalDate.now()));
			            emailToSend.setPdfRepBase64Str(sReturn);
			            emailToSend= emailQueueService.updateEmailQueue(emailToSend);	 
			            sReturn="{\"Status\":\"ok\"}";
		            } else
		            {
			            sReturn="{\"Status\":\"nok\"}";
		            }
		            
        	
	            } else {
	            	sReturn="{\"Status\":\"nok\"}";
	            }
	            
			} catch (Exception e2) {
				sReturn="{\"Status\":\"nok\"}";;
				e2.printStackTrace();
				System.out.println("Errore MakeReportEpisode");	
			}	
		}
		return sReturn;
	}

	//---------------------------------------------------------------------------------------------------------------------	
	  private String getAbsolutePath() throws UnsupportedEncodingException {

			String path = this.getClass().getClassLoader().getResource("").getPath();
			String fullPath = URLDecoder.decode(path, "UTF-8");
			String pathArr[] = fullPath.split("/WEB-INF/classes/");
			System.out.println(fullPath);
			System.out.println(pathArr[0]);
			fullPath = pathArr[0];
			String reponsePath = "";

			// to read a file from webcontent
			//reponsePath = new File(fullPath).getPath() + File.separatorChar + "newfile.txt";
			reponsePath = new File(fullPath).getPath();
			System.out.println("getAbsolutePath:" + reponsePath);
			return reponsePath;

	  }

	public boolean addAntennaTransitsListener(AntennaTransitsListener l) {
		return antennaTransitsListeners.add(l);
	}

	public boolean removeAntennaTransitsListener(AntennaTransitsListener l) {
		return antennaTransitsListeners.remove(l);
	}		
	
	/*************************************************************************/

	/*
	@RequestMapping(method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE, value="/updateFooX")
	public @ResponseBody Foo updateFooX(@RequestBody Foo foo) {
		
		// Per richiamarlo usare un tool tipo "Advanced REST client". immettere come indirizzo: <indirizzo>:8080/MediasetStudiosApplicationGateway/webservices/MediasetStudiosAgWebServices/updateFooX
		// come metodo : POST
		// come header aggiungere: Content-Type---> application/json
		
		// Può essere richiamato anche attraverso la classe com.crismasecurity.mediasetStudios.webservices.test.CallWebserviceTest
		
		System.out.println("updateFooX..." + foo);
		foo.setUsername("hashahahaha");
	    return foo;
	}

*/
	//Questa è la versione con l'autenticazione integrata.	
	@RequestMapping(method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE, value="/authUpdateFooX")
	public @ResponseBody Foo authUpdateFooX(@RequestBody Foo foo) {
		
		// Per richiamarlo usare un tool tipo "Advanced REST client". immettere come indirizzo: <indirizzo>:8080/MediasetStudiosApplicationGateway/webservices/MediasetStudiosAgWebServices/updateFooX
		// come metodo : POST
		// come header aggiungere: Content-Type---> application/json
		
		// Può essere richiamato anche attraverso la classe com.crismasecurity.mediasetStudios.webservices.test.CallWebserviceTest
		
		System.out.println("updateFooX..." + foo);
		foo.setUsername("hashahahaha");
	    return foo;
	}
	
	
}
