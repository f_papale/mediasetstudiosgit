package com.crismasecurity.mediasetStudios.webservices.authUtils;


import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.crismasecurity.mediasetStudios.core.security.entities.AppUser;
import com.crismasecurity.mediasetStudios.core.security.services.AppUserService;

 
 
@Component
public class CustomAuthenticationProviderForREST implements AuthenticationProvider {
 
    @Autowired
    private AppUserService userService;
 
    @Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
    String username = authentication.getName();
    String password = (String) authentication.getCredentials();
   
//    System.out.println("authenticate rest user--!!!!!!!!!!!!! user: " + username + "   pass:" + password);
        
        
        // TODO verificare se � un cliente valido! Ad esempio facendo la chiamata al sistema Bonwin
        
//        List<GrantedAuthority> grList = new ArrayList<GrantedAuthority>();
//        GrantedAuthority grRole = new SimpleGrantedAuthority("ROLE_ADMIN"); // se si cambia qui ricordarsi di cambiarlo anche in applicationSecurity.xml
//		grList.add(grRole);
		
//        UserDetails user = new User(username, password, true, true, true, true, grList);
	    
//        if (username==null || !username.startsWith("test")) user=null; 
        // TODO implementare il controllo !!!!!!!!!!!!!!!!!!!
        
      UserDetails user = userService.loadUserByUsername(username);
   
	  if (user == null || !user.getUsername().equalsIgnoreCase(username)) {
	      throw new BadCredentialsException("Username not found.");
	  }
	   
      AppUser uu = userService.getUserEntityByLogin(user.getUsername());
      
      String encodedPassword = userService.encodePasswordForUser(username, password);
      if (!encodedPassword.equals(uu.getPassword())) {
          throw new BadCredentialsException("Wrong password.");
          }
   
          Collection<? extends GrantedAuthority> authorities = user.getAuthorities();
          
          boolean roleOk = false;
          if (authorities==null || authorities.size()==0) roleOk = true;
          else {
	          for (GrantedAuthority grantedAuthority : authorities) {
	        	  if (grantedAuthority.getAuthority().equals("ROLE_ADMIN")) {
				  roleOk = true;
				  break;
			  }
	      }
      }
      if (!roleOk) throw new BadCredentialsException("Wrong role.");
             
      return new UsernamePasswordAuthenticationToken(user, password, authorities);
	}
    
    
	@Override
	public boolean supports(Class<?> authentication) {
		return true;
	}
}