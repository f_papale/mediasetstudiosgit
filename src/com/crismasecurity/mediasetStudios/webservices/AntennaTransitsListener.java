package com.crismasecurity.mediasetStudios.webservices;

import com.crismasecurity.mediasetStudios.core.entity.SpectatorEpisode;

public interface AntennaTransitsListener {


	public void transitCheckoutReceived(SpectatorEpisode sep);
	public void transitCheckinReceived(SpectatorEpisode sep);
	public void transitNoReturnedReceived(SpectatorEpisode sep);
	
}
