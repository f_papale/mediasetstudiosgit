package com.crismasecurity.mediasetStudios.webservices;

import java.util.Date;

public class BookingRecord {
	
	public String readerAntenna;
	public String datestamp;
	public Date timeStamp;
	public String iPaddress;
	public String type;
	public String secure;
	public String tagId;
	
	
	public BookingRecord(String tagId, String readerAntenna, String datestamp, Date timeStamp, String iPaddress, String type, String secure) {
        super();
        this.tagId = tagId;
        this.readerAntenna = readerAntenna;
        this.datestamp = datestamp;
        this.timeStamp = timeStamp;
        this.iPaddress = iPaddress;
        this.type = type;
        this.secure = secure;
    }
	public BookingRecord() {
		super();
	}
    

    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    public String getReaderAntenna() {
        return readerAntenna;
    }

    public void setReaderAntenna(String readerAntenna) {
        this.readerAntenna = readerAntenna;
    }

    public String getDatestamp() {
        return datestamp;
    }

    public void setDatestamp(String datestamp) {
        this.datestamp = datestamp;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getiPaddress() {
        return iPaddress;
    }

    public void setiPaddress(String iPaddress) {
        this.iPaddress = iPaddress;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSecure() {
        return secure;
    }

    public void setSecure(String secure) {
        this.secure = secure;
    }



	@Override
	public String toString() {
		return "BookingRecord [readerAntenna=" + readerAntenna + ", datestamp=" + datestamp + ", timeStamp=" + timeStamp
				+ ", iPaddress=" + iPaddress + ", type=" + type + ", secure=" + secure + ", tagId=" + tagId + "]";
	}
    
}
