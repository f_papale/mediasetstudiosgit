package com.crismasecurity.mediasetStudios.webservices.desktopReader;

import com.crismasecurity.mediasetStudios.core.entity.Location;
import com.crismasecurity.mediasetStudios.core.entity.Media;

public interface DesktopReaderListener {

	public void eventReceived(Location location, Media media, String error, String mediaId);
	

}
