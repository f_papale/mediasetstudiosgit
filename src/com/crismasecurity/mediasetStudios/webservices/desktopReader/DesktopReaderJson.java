package com.crismasecurity.mediasetStudios.webservices.desktopReader;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * dati provenienti dal lettore da tavolo
 * 
 * 
 * - DeviceId nome della postazione
 * - TagId:  id del badge
 *
 * @author Crisma
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DesktopReaderJson {

    public String DeviceId;

    public String TagId;

    /**
     * 
     */
	public DesktopReaderJson() {
		super();
	}
	
	

	public String getDeviceId() {
		return DeviceId;
	}

	public void setDeviceId(String deviceId) {
		DeviceId = deviceId;
	}

	public String getTagId() {
		return TagId;
	}

	public void setTagId(String tagId) {
		TagId = tagId;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((DeviceId == null) ? 0 : DeviceId.hashCode());
		result = prime * result + ((TagId == null) ? 0 : TagId.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DesktopReaderJson other = (DesktopReaderJson) obj;
		if (DeviceId == null) {
			if (other.DeviceId != null)
				return false;
		} else if (!DeviceId.equals(other.DeviceId))
			return false;
		if (TagId == null) {
			if (other.TagId != null)
				return false;
		} else if (!TagId.equals(other.TagId))
			return false;
		return true;
	}



	@Override
	public String toString() {
		return "DesktopReaderJson [DeviceId=" + DeviceId + ", TagId=" + TagId + "]";
	}  
}

