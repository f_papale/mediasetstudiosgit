package com.crismasecurity.mediasetStudios.webservices;

public class Foo {


	private String username;
	private String password;
	
	
	public Foo() {
		super();
	}
	public Foo(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}
	
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "Foo [username=" + username + ", password=" + password + "]";
	}
	
	
	
	
	
}
