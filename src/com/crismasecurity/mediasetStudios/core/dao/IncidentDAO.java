package com.crismasecurity.mediasetStudios.core.dao;

import com.crismasecurity.mediasetStudios.core.entity.Incident;
import com.widee.base.dao.base.GenericDAO;

/**
 * 
 * Class name: SpectatorDAO
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author 	Vincenzo Scaccia
 * @date 	30/may/2018
 *
 */
public interface IncidentDAO extends GenericDAO<Incident, Long>{

}
