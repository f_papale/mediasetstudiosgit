package com.crismasecurity.mediasetStudios.core.dao;

import com.crismasecurity.mediasetStudios.core.entity.EpisodeStatusType;
import com.widee.base.dao.base.GenericDAO;

/**
 * Class name: EpisodeStatusTypeDAO
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author fpapale
 * @date 25/05/2018
 *
 */
public interface EpisodeStatusTypeDAO extends GenericDAO<EpisodeStatusType, Long>{
}
