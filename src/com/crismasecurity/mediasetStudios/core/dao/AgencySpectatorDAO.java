package com.crismasecurity.mediasetStudios.core.dao;

import com.crismasecurity.mediasetStudios.core.entity.AgencySpectator;
import com.crismasecurity.mediasetStudios.core.entity.AgencySpectatorId;
import com.widee.base.dao.base.GenericDAO;

/**
 * Class name: AgencyDAO
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author fpapale
 * @date 25/05/2018
 *
 */
public interface AgencySpectatorDAO extends GenericDAO<AgencySpectator, AgencySpectatorId>{
}
 