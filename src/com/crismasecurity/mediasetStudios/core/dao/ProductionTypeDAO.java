package com.crismasecurity.mediasetStudios.core.dao;

import com.crismasecurity.mediasetStudios.core.entity.ProductionType;
import com.widee.base.dao.base.GenericDAO;

/**
 * Class name: ProductionTypeDAO
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author fpapale
 * @date 25/05/2018
 *
 */
public interface ProductionTypeDAO extends GenericDAO<ProductionType, Long>{
}
