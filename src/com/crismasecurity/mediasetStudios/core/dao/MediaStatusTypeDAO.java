package com.crismasecurity.mediasetStudios.core.dao;

import com.crismasecurity.mediasetStudios.core.entity.MediaStatusType;
import com.widee.base.dao.base.GenericDAO;

/**
 * Class name: MediaStatusTypeDAO
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author fpapale
 * @date 25/05/2018
 *
 */
public interface MediaStatusTypeDAO extends GenericDAO<MediaStatusType, Long>{
}
