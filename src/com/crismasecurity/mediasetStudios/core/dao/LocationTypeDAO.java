package com.crismasecurity.mediasetStudios.core.dao;

import com.crismasecurity.mediasetStudios.core.entity.LocationType;
import com.widee.base.dao.base.GenericDAO;

public interface LocationTypeDAO extends GenericDAO<LocationType, Long>{

}
