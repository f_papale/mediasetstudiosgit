package com.crismasecurity.mediasetStudios.core.dao;

import com.crismasecurity.mediasetStudios.core.entity.SpectatorEpisode;
import com.widee.base.dao.base.GenericDAO;

/**
 * 
 * Class name: SpectatorDAO
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author 	fdongmo
 * @date 	30/may/2018
 *
 */
public interface SpectatorEpisodeDAO extends GenericDAO<SpectatorEpisode, Long>{

}
