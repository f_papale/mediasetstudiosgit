package com.crismasecurity.mediasetStudios.core.dao;

import com.crismasecurity.mediasetStudios.core.entity.DocumentSpectator;
import com.widee.base.dao.base.GenericDAO;

/**
 * Class name: DocumentSpectatorDAO
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author fdongmo
 * @date 03/08/2018
 *
 */
public interface DocumentSpectatorDAO extends GenericDAO<DocumentSpectator, Long>{
}
 