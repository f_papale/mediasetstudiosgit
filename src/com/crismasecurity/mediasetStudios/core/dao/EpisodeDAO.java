package com.crismasecurity.mediasetStudios.core.dao;

import com.crismasecurity.mediasetStudios.core.entity.Episode;
import com.widee.base.dao.base.GenericDAO;

/**
 * Class name: EpisodeDAO
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author fpapale
 * @date 25/05/2018
 *
 */
public interface EpisodeDAO extends GenericDAO<Episode, Long>{
}
