package com.crismasecurity.mediasetStudios.core.dao;

import com.crismasecurity.mediasetStudios.core.entity.SeverityLevel;
import com.widee.base.dao.base.GenericDAO;

public interface SeverityLevelDAO extends GenericDAO <SeverityLevel,Long> {

}
 