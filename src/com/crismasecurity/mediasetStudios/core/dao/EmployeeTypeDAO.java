package com.crismasecurity.mediasetStudios.core.dao;

import com.crismasecurity.mediasetStudios.core.entity.EmployeeType;
import com.widee.base.dao.base.GenericDAO;

/**
 * Class name: EmployeeTypeDAO
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author fpapale
 * @date 25/05/2018
 *
 */
public interface EmployeeTypeDAO extends GenericDAO<EmployeeType, Long>{
}
