package com.crismasecurity.mediasetStudios.core.dao;

import java.math.BigDecimal;
import java.util.List;

import com.crismasecurity.mediasetStudios.core.entity.Spectator;
import com.widee.base.dao.base.GenericDAO;

/**
 * 
 * Class name: SpectatorDAO
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author 	fdongmo
 * @date 	30/may/2018
 *
 */
public interface SpectatorDAO extends GenericDAO<Spectator, Long>{

	public List<BigDecimal> getSpectator2Wipe(int month);
	public List<Object> getAdmittedSpectatorList(int idAgency);
	public boolean removeDuplicatedSpectators();
}
