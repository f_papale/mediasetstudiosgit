package com.crismasecurity.mediasetStudios.core.dao;

import com.crismasecurity.mediasetStudios.core.entity.Employee;
import com.widee.base.dao.base.GenericDAO;

/**
 * Class name: EmployeeDAO
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author fpapale
 * @date 25/05/2018
 *
 */
public interface EmployeeDAO extends GenericDAO<Employee, Long>{
}
