package com.crismasecurity.mediasetStudios.core.dao;

import com.crismasecurity.mediasetStudios.core.entity.AgencyUser;
import com.crismasecurity.mediasetStudios.core.entity.AgencyUserId;
import com.widee.base.dao.base.GenericDAO;

/**
 * Class name: AgencyUserDAO
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author gnocco
 * @date 20/july/2018
 *
 */
public interface AgencyUserDAO extends GenericDAO<AgencyUser, AgencyUserId>{
}
