package com.crismasecurity.mediasetStudios.core.dao;

import com.crismasecurity.mediasetStudios.core.entity.ProductionCenter;
import com.widee.base.dao.base.GenericDAO;

/**
 * Class name: ProductionCenterDAO
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author fpapale
 * @date 25/05/2018
 *
 */
public interface ProductionCenterDAO extends GenericDAO<ProductionCenter, Long>{
}
