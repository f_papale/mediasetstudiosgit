package com.crismasecurity.mediasetStudios.core.dao;

import com.crismasecurity.mediasetStudios.core.entity.BlackListSpectator;
import com.widee.base.dao.base.GenericDAO;

public interface BlackListSpectatorDAO extends GenericDAO<BlackListSpectator, Long>{ 

}
