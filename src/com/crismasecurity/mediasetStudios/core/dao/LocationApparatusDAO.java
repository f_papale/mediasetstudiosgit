package com.crismasecurity.mediasetStudios.core.dao;

import com.crismasecurity.mediasetStudios.core.entity.LocationApparatus;
import com.crismasecurity.mediasetStudios.core.entity.ProductionEmployee;
import com.widee.base.dao.base.GenericDAO;

/**
 * 
 * Class name: ProductionEmployeeDAO
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author 	Vincenzo Scaccia
 * @date 	12/jul/2018
 *
 */
public interface LocationApparatusDAO extends GenericDAO<LocationApparatus, Long>{

}
