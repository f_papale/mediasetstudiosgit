package com.crismasecurity.mediasetStudios.core.dao;

import com.crismasecurity.mediasetStudios.core.entity.ProductionEmployee;
import com.widee.base.dao.base.GenericDAO;

/**
 * 
 * Class name: ProductionEmployeeDAO
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author 	fpapale
 * @date 	12/jul/2018
 *
 */
public interface ProductionEmployeeDAO extends GenericDAO<ProductionEmployee, Long>{

}
