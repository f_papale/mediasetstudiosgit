package com.crismasecurity.mediasetStudios.core.dao;

import com.crismasecurity.mediasetStudios.core.entity.Liberatoria;
import com.crismasecurity.mediasetStudios.core.entity.Location;
import com.widee.base.dao.base.GenericDAO;

public interface LiberatoriaDAO extends GenericDAO<Liberatoria, Long>{

}
