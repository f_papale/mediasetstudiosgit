package com.crismasecurity.mediasetStudios.core.dao;

import com.crismasecurity.mediasetStudios.core.entity.DistributionList;
import com.widee.base.dao.base.GenericDAO;

/**
 * 
 * Class name: DistributionListDAO
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author 	fdongmo
 * @date 	02/aug/2018
 *
 */
public interface DistributionListDAO extends GenericDAO<DistributionList, Long>{

}
