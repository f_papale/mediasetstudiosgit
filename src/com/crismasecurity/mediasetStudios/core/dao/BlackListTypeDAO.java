package com.crismasecurity.mediasetStudios.core.dao;

import com.crismasecurity.mediasetStudios.core.entity.BlackListType;
import com.crismasecurity.mediasetStudios.core.entity.IncidentType;
import com.widee.base.dao.base.GenericDAO;

public interface BlackListTypeDAO extends GenericDAO<BlackListType, Long>{ 

}
