package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.IncidentTypeDAO;
import com.crismasecurity.mediasetStudios.core.entity.IncidentType;
import com.widee.base.dao.base.impl.GenericDAOImpl;

@Repository (value="IncidentTypeDAO") 
public class IncidentTypeDAOImpl extends  GenericDAOImpl<IncidentType, Long>  implements IncidentTypeDAO{

	public IncidentTypeDAOImpl() {
			super(IncidentType.class);
	}

}
