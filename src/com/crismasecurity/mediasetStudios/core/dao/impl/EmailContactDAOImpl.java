package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.EmailContactDAO;
import com.crismasecurity.mediasetStudios.core.entity.EmailContact;
import com.widee.base.dao.base.impl.GenericDAOImpl;

@Repository (value="EmailContactDAO") 
public class EmailContactDAOImpl extends  GenericDAOImpl<EmailContact, Long>  implements EmailContactDAO{

	public EmailContactDAOImpl() {
			super(EmailContact.class);
	}

}
