package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.ProductionDAO;
import com.crismasecurity.mediasetStudios.core.entity.Production;
import com.widee.base.dao.base.impl.GenericDAOImpl;



/**
 * Class name: ContractDAOImpl
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author Gio
 * @date 30/jan/2012
 *
 */
@Repository (value="ProductionDAO") 
public class ProductionDAOImpl extends GenericDAOImpl<Production, Long> implements ProductionDAO {

	public ProductionDAOImpl() {
		super(Production.class);
	}
}


