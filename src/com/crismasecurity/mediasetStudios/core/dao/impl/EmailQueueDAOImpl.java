package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.EmailQueueDAO;
import com.crismasecurity.mediasetStudios.core.entity.EmailQueue;
import com.widee.base.dao.base.impl.GenericDAOImpl;



/**
 * Class name: ContractDAOImpl
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author Gio
 * @date 30/jan/2012
 *
 */
@Repository (value="EmailQueueDAO") 
public class EmailQueueDAOImpl extends GenericDAOImpl<EmailQueue, Long> implements EmailQueueDAO {

	public EmailQueueDAOImpl() {
		super(EmailQueue.class);
	}
}


