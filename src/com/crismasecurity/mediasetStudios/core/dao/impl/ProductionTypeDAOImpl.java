package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.ProductionTypeDAO;
import com.crismasecurity.mediasetStudios.core.entity.ProductionType;
import com.widee.base.dao.base.impl.GenericDAOImpl;



/**
 * Class name: ProductionTypeDAOImpl
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author	fpapale
 * @date	25/05/2018
 *
 */
@Repository (value="ProductionTypeDAO") 
public class ProductionTypeDAOImpl extends GenericDAOImpl<ProductionType, Long> implements ProductionTypeDAO {

	public ProductionTypeDAOImpl() {
		super(ProductionType.class);
	}
}


