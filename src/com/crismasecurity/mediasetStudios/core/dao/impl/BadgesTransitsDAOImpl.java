package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.BadgesTransitsDAO;
import com.crismasecurity.mediasetStudios.core.entity.BadgesTransits;
import com.widee.base.dao.base.impl.GenericDAOImpl;



/**
 * Class name: ContractDAOImpl
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author Gio
 * @date 30/jan/2012
 *
 */
@Repository (value="BadgesTransitsDAO") 
public class BadgesTransitsDAOImpl extends GenericDAOImpl<BadgesTransits, Long> implements BadgesTransitsDAO {

	public BadgesTransitsDAOImpl() {
		super(BadgesTransits.class);
	}
}


