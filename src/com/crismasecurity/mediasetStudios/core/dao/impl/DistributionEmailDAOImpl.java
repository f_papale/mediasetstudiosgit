package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.DistributionEmailDAO;
import com.crismasecurity.mediasetStudios.core.entity.DistributionEmail;
import com.crismasecurity.mediasetStudios.core.entity.DistributionEmailId;
import com.widee.base.dao.base.impl.GenericDAOImpl;

/**
 * Class name: DistributionDAOImpl
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author fab
 * @date 30/jan/2012
 *
 */
@Repository (value="DistributionEmailDAO") 
public class DistributionEmailDAOImpl extends GenericDAOImpl<DistributionEmail, DistributionEmailId> implements DistributionEmailDAO {

	public DistributionEmailDAOImpl() {
		super(DistributionEmail.class);
	}
}