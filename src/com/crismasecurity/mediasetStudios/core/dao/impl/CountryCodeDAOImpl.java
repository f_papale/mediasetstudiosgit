package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.CountryCodeDAO;
import com.crismasecurity.mediasetStudios.core.entity.CountryCode;
import com.widee.base.dao.base.impl.GenericDAOImpl;

@Repository (value="CountryCodeDAO") 
public class CountryCodeDAOImpl extends  GenericDAOImpl <CountryCode, Long> implements CountryCodeDAO{
	public CountryCodeDAOImpl() {
		super(CountryCode.class);
	}
}
