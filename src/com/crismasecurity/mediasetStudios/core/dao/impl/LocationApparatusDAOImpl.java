package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.LocationApparatusDAO;
import com.crismasecurity.mediasetStudios.core.entity.LocationApparatus;
import com.widee.base.dao.base.impl.GenericDAOImpl;

@Repository (value="LocationApparatusDAO")
public class LocationApparatusDAOImpl extends GenericDAOImpl<LocationApparatus, Long> implements LocationApparatusDAO {
	public LocationApparatusDAOImpl() {
		super(LocationApparatus.class);
	}
}
