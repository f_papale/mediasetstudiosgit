package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.DistributionListDAO;
import com.crismasecurity.mediasetStudios.core.entity.DistributionList;
import com.widee.base.dao.base.impl.GenericDAOImpl;

@Repository (value="DistributionListDAO") 
public class DistributionListDAOImpl extends  GenericDAOImpl<DistributionList, Long>  implements DistributionListDAO{

	public DistributionListDAOImpl() {
			super(DistributionList.class);
	}

}
