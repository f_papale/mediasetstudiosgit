package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.EpisodeDAO;
import com.crismasecurity.mediasetStudios.core.entity.Episode;
import com.widee.base.dao.base.impl.GenericDAOImpl;



/**
 * Class name: ContractDAOImpl
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author Gio
 * @date 30/jan/2012
 *
 */
@Repository (value="EpisodeDAO") 
public class EpisodeDAOImpl extends GenericDAOImpl<Episode, Long> implements EpisodeDAO {

	public EpisodeDAOImpl() {
		super(Episode.class);
	}
}


