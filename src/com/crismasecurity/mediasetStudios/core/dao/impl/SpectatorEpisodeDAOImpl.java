package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;
import com.crismasecurity.mediasetStudios.core.dao.SpectatorEpisodeDAO;
import com.crismasecurity.mediasetStudios.core.entity.SpectatorEpisode;
import com.widee.base.dao.base.impl.GenericDAOImpl;

@Repository (value="SpectatorEpisodeDAO")
public class SpectatorEpisodeDAOImpl extends GenericDAOImpl<SpectatorEpisode, Long> implements SpectatorEpisodeDAO {
	public SpectatorEpisodeDAOImpl() {
		super(SpectatorEpisode.class);
	}
}
