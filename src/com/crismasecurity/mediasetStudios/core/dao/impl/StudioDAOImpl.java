package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.StudioDAO;
import com.crismasecurity.mediasetStudios.core.entity.Studio;
import com.widee.base.dao.base.impl.GenericDAOImpl;


/**
 * Class name: ContractDAOImpl
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author Gio
 * @date 30/jan/2012
 *
 */
@Repository (value="StudioDAO") 
public class StudioDAOImpl extends GenericDAOImpl<Studio, Long> implements StudioDAO {

	public StudioDAOImpl() {
		super(Studio.class);
	}
}


