package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.AuditDAO;
import com.crismasecurity.mediasetStudios.core.entity.Audit;
import com.widee.base.dao.base.impl.GenericDAOImpl;



/**
 * Class name: AuditDAOImpl
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author Gio
 * @date 30/jan/2012
 *
 */
@Repository (value="AuditDAO") // Il nome serve solo per i test con la classe TestDao
public class AuditDAOImpl extends GenericDAOImpl<Audit, Long> implements AuditDAO {

	public AuditDAOImpl() {
		super(Audit.class);
	}
}


