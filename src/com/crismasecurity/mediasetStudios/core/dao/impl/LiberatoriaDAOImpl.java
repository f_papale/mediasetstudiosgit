package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.LiberatoriaDAO;
import com.crismasecurity.mediasetStudios.core.dao.LocationDAO;
import com.crismasecurity.mediasetStudios.core.entity.Liberatoria;
import com.crismasecurity.mediasetStudios.core.entity.Location;
import com.widee.base.dao.base.impl.GenericDAOImpl;

@Repository (value="LiberatoriaDAO") 
public class LiberatoriaDAOImpl extends GenericDAOImpl<Liberatoria, Long> implements LiberatoriaDAO{
	
	public LiberatoriaDAOImpl() {
		super(Liberatoria.class);
	}
}
