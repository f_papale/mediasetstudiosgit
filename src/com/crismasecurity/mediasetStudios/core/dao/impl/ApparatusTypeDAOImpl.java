package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.ApparatusTypeDAO;
import com.crismasecurity.mediasetStudios.core.entity.ApparatusType;
import com.widee.base.dao.base.impl.GenericDAOImpl;

@Repository (value="ApparatusTypeDAO") 
public class ApparatusTypeDAOImpl extends  GenericDAOImpl<ApparatusType, Long>  implements ApparatusTypeDAO{

	public ApparatusTypeDAOImpl() {
			super(ApparatusType.class);
	}

}
