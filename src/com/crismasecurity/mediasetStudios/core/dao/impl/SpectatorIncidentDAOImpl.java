package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.SpectatorIncidentDAO;
import com.crismasecurity.mediasetStudios.core.entity.SpectatorIncident;
import com.widee.base.dao.base.impl.GenericDAOImpl;

@Repository (value="SpectatorIncidentDAO")

public class SpectatorIncidentDAOImpl extends GenericDAOImpl<SpectatorIncident, Long> implements SpectatorIncidentDAO {
	public SpectatorIncidentDAOImpl() {
		super(SpectatorIncident.class);
	}
}
