package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.DocumentSpectatorDAO;
import com.crismasecurity.mediasetStudios.core.entity.DocumentSpectator;
import com.widee.base.dao.base.impl.GenericDAOImpl;



/**
 * Class name: DocumentSpectatorDAOImpl
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author fdongmo
 * @date 03/aug/2018
 *
 */
@Repository (value="DocumentSpectatorDAO") 
public class DocumentSpectatorDAOImpl extends GenericDAOImpl<DocumentSpectator, Long> implements DocumentSpectatorDAO {

	public DocumentSpectatorDAOImpl() {
		super(DocumentSpectator.class);
	}
}


