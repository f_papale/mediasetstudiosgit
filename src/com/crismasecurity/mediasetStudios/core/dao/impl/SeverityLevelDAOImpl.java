package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.SeverityLevelDAO;
import com.crismasecurity.mediasetStudios.core.entity.SeverityLevel;
import com.widee.base.dao.base.impl.GenericDAOImpl;

@Repository (value="SeverityLevelDAO") 
public class SeverityLevelDAOImpl extends  GenericDAOImpl <SeverityLevel, Long> implements SeverityLevelDAO{
	public SeverityLevelDAOImpl() {
		super(SeverityLevel.class);
	}
}
 