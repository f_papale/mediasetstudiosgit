package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.ApparatusDAO;
import com.crismasecurity.mediasetStudios.core.entity.Apparatus;
import com.widee.base.dao.base.impl.GenericDAOImpl;

@Repository (value="ApparatusDAO") 
public class ApparatusDAOImpl extends  GenericDAOImpl<Apparatus, Long>  implements ApparatusDAO{

	public ApparatusDAOImpl() {
			super(Apparatus.class);
	}

}
