package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.MediaStatusTypeDAO;
import com.crismasecurity.mediasetStudios.core.entity.MediaStatusType;
import com.widee.base.dao.base.impl.GenericDAOImpl;



/**
 * Class name: MediaStatusTypeDAOImpl
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author fab
 * @date 06/08/2018
 *
 */
@Repository (value="MediaStatusTypeDAO") 
public class MediaStatusTypeDAOImpl extends GenericDAOImpl<MediaStatusType, Long> implements MediaStatusTypeDAO {

	public MediaStatusTypeDAOImpl() {
		super(MediaStatusType.class);
	}
}


