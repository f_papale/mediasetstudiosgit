package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.EpisodeStatusTypeDAO;
import com.crismasecurity.mediasetStudios.core.entity.EpisodeStatusType;
import com.widee.base.dao.base.impl.GenericDAOImpl;



/**
 * Class name: EpisodeStatusTypeDAOImpl
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author	fpapale
 * @date	25/05/2018
 *
 */
@Repository (value="EpisodeStatusTypeDAO") 
public class EpisodeStatusTypeDAOImpl extends GenericDAOImpl<EpisodeStatusType, Long> implements EpisodeStatusTypeDAO {

	public EpisodeStatusTypeDAOImpl() {
		super(EpisodeStatusType.class);
	}
}


