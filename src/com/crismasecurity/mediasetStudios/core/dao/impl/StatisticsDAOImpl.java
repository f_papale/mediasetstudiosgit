package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.StatisticsDAO;
import com.crismasecurity.mediasetStudios.core.entity.Statistics;
import com.widee.base.dao.base.impl.GenericDAOImpl;



/**
 * Class name: StatisticsDAOImpl
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author fab
 * @date 06/08/2018
 *
 */
@Repository (value="StatisticsDAO") 
public class StatisticsDAOImpl extends GenericDAOImpl<Statistics, Long> implements StatisticsDAO {

	public StatisticsDAOImpl() {
		super(Statistics.class);
	}
}


