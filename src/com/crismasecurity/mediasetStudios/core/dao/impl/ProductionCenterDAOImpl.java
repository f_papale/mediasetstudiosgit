package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.ProductionCenterDAO;
import com.crismasecurity.mediasetStudios.core.entity.ProductionCenter;
import com.widee.base.dao.base.impl.GenericDAOImpl;



/**
 * Class name: ProductionCenterDAOImpl
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author 	Gio
 * @date 	30/jan/2012
 *
 */
@Repository (value="ProductionCenterDAO") 
public class ProductionCenterDAOImpl extends GenericDAOImpl<ProductionCenter, Long> implements ProductionCenterDAO {

	public ProductionCenterDAOImpl() {
		super(ProductionCenter.class);
	}
}


