package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.AgencySpectatorDAO;
import com.crismasecurity.mediasetStudios.core.entity.AgencySpectator;
import com.crismasecurity.mediasetStudios.core.entity.AgencySpectatorId;
import com.widee.base.dao.base.impl.GenericDAOImpl;



/**
 * Class name: ContractDAOImpl
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author Gio
 * @date 30/jan/2012
 *
 */
@Repository (value="AgencySpectatorDAO") 
public class AgencySpectatorDAOImpl extends GenericDAOImpl<AgencySpectator, AgencySpectatorId> implements AgencySpectatorDAO {

	public AgencySpectatorDAOImpl() {
		super(AgencySpectator.class);
	}
}


