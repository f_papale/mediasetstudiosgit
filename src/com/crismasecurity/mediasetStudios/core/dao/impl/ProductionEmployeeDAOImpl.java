package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;
import com.crismasecurity.mediasetStudios.core.dao.ProductionEmployeeDAO;
import com.crismasecurity.mediasetStudios.core.entity.ProductionEmployee;
import com.widee.base.dao.base.impl.GenericDAOImpl;

@Repository (value="ProductionEmployeeDAO")
public class ProductionEmployeeDAOImpl extends GenericDAOImpl<ProductionEmployee, Long> implements ProductionEmployeeDAO {
	public ProductionEmployeeDAOImpl() {
		super(ProductionEmployee.class);
	}
}
