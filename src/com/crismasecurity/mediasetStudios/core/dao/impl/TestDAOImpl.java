package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.TestDAO;
import com.crismasecurity.mediasetStudios.core.entity.Test;
import com.widee.base.dao.base.impl.GenericDAOImpl;



/**
 * Class name: ContractDAOImpl
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author Gio
 * @date 30/jan/2012
 *
 */
@Repository (value="TestDAO") 
public class TestDAOImpl extends GenericDAOImpl<Test, Long> implements TestDAO {

	public TestDAOImpl() {
		super(Test.class);
	}
}


