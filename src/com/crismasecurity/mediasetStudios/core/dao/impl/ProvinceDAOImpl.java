package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.ProvinceDAO;
import com.crismasecurity.mediasetStudios.core.entity.Province;
import com.widee.base.dao.base.impl.GenericDAOImpl;

/**
 * Class name: ProvinceDAOImpl
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author Fra
 * @date 07/jun/2018
 *
 */
@Repository (value="ProvinceDAO") 
public class ProvinceDAOImpl extends GenericDAOImpl <Province, Long> implements ProvinceDAO{

	public ProvinceDAOImpl() {
		super(Province.class);
	}

}
