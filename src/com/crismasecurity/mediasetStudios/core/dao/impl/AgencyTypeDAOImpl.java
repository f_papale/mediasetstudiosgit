package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.AgencyTypeDAO;
import com.crismasecurity.mediasetStudios.core.entity.AgencyType;
import com.widee.base.dao.base.impl.GenericDAOImpl;



/**
 * Class name: AgencyTypeDAOImpl
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author	fpapale
 * @date	25/05/2018
 *
 */
@Repository (value="AgencyTypeDAO") 
public class AgencyTypeDAOImpl extends GenericDAOImpl<AgencyType, Long> implements AgencyTypeDAO {

	public AgencyTypeDAOImpl() {
		super(AgencyType.class);
	}
}


