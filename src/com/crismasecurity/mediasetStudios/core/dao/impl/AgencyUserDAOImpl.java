package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.AgencyUserDAO;
import com.crismasecurity.mediasetStudios.core.entity.AgencyUser;
import com.crismasecurity.mediasetStudios.core.entity.AgencyUserId;
import com.widee.base.dao.base.impl.GenericDAOImpl;



/**
 * Class name: AgencyUSerDAOImpl
 *
 * Description: 
 * 
 * 
 * Company: 
 *
 * @author gnocco
 * @date 20/july/2018
 *
 */
@Repository (value="AgencyUserDAO") 
public class AgencyUserDAOImpl extends GenericDAOImpl<AgencyUser, AgencyUserId> implements AgencyUserDAO {

	public AgencyUserDAOImpl() {
		super(AgencyUser.class);
	}
}


