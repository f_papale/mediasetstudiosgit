package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.BlackListSpectatorDAO;
import com.crismasecurity.mediasetStudios.core.entity.BlackListSpectator;
import com.widee.base.dao.base.impl.GenericDAOImpl;

@Repository (value="BlackListSpectatorDAO")
public class BlackListSpectatorDAOImpl extends  GenericDAOImpl<BlackListSpectator, Long>  implements BlackListSpectatorDAO{

	public BlackListSpectatorDAOImpl() {
		super(BlackListSpectator.class);
	}

}
