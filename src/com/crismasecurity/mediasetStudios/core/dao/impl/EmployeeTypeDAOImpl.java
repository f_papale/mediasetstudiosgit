package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.EmployeeTypeDAO;
import com.crismasecurity.mediasetStudios.core.entity.EmployeeType;
import com.widee.base.dao.base.impl.GenericDAOImpl;



/**
 * Class name: EmployeeTypeDAOImpl
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author	fpapale
 * @date	25/05/2018
 *
 */
@Repository (value="EmployeeTypeDAO") 
public class EmployeeTypeDAOImpl extends GenericDAOImpl<EmployeeType, Long> implements EmployeeTypeDAO {

	public EmployeeTypeDAOImpl() {
		super(EmployeeType.class);
	}
}


