package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.LocationDAO;
import com.crismasecurity.mediasetStudios.core.entity.Location;
import com.widee.base.dao.base.impl.GenericDAOImpl;

@Repository (value="LocationDAO") 
public class LocationDAOImpl extends GenericDAOImpl<Location, Long> implements LocationDAO{
	
	public LocationDAOImpl() {
		super(Location.class);
	}
}
