package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.RegionDAO;
import com.crismasecurity.mediasetStudios.core.entity.Region;
import com.widee.base.dao.base.impl.GenericDAOImpl;

@Repository (value="RegionDAO") 
public class RegionDAOImpl extends  GenericDAOImpl <Region, Long> implements RegionDAO{
	public RegionDAOImpl() {
		super(Region.class);
	}
}
