package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.AgencyDAO;
import com.crismasecurity.mediasetStudios.core.entity.Agency;
import com.widee.base.dao.base.impl.GenericDAOImpl;



/**
 * Class name: ContractDAOImpl
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author Gio
 * @date 30/jan/2012
 *
 */
@Repository (value="AgencyDAO") 
public class AgencyDAOImpl extends GenericDAOImpl<Agency, Long> implements AgencyDAO {

	public AgencyDAOImpl() {
		super(Agency.class);
	}
}


