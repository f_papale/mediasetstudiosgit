package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.RoleMasterSlaveDAO;
import com.crismasecurity.mediasetStudios.core.entity.RoleMasterSlave;
import com.widee.base.dao.base.impl.GenericDAOImpl;

@Repository (value="RoleMasterSlaveDAO") 
public class RoleMasterSlaveDAOImpl extends  GenericDAOImpl<RoleMasterSlave, Long>  implements RoleMasterSlaveDAO{

	public RoleMasterSlaveDAOImpl() {
			super(RoleMasterSlave.class);
	}

}
