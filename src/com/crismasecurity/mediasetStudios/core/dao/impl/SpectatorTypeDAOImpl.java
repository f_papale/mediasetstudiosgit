package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.SpectatorTypeDAO;
import com.crismasecurity.mediasetStudios.core.entity.SpectatorType;
import com.widee.base.dao.base.impl.GenericDAOImpl;

@Repository (value="SpectatorTypeDAO") 
public class SpectatorTypeDAOImpl extends  GenericDAOImpl<SpectatorType, Long>  implements SpectatorTypeDAO{

	public SpectatorTypeDAOImpl() {
			super(SpectatorType.class);
	}

}
