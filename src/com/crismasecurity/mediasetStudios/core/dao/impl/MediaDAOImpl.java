package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.MediaDAO;
import com.crismasecurity.mediasetStudios.core.entity.Media;
import com.widee.base.dao.base.impl.GenericDAOImpl;



/**
 * Class name: MediaDAOImpl
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author fab
 * @date 06/08/2018
 *
 */
@Repository (value="MediaDAO") 
public class MediaDAOImpl extends GenericDAOImpl<Media, Long> implements MediaDAO {

	public MediaDAOImpl() {
		super(Media.class);
	}
}


