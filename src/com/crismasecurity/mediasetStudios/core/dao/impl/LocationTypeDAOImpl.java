package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.LocationTypeDAO;
import com.crismasecurity.mediasetStudios.core.entity.LocationType;
import com.widee.base.dao.base.impl.GenericDAOImpl;

@Repository (value="LocationTypeDAO") 
public class LocationTypeDAOImpl extends  GenericDAOImpl<LocationType, Long>  implements LocationTypeDAO{

	public LocationTypeDAOImpl() {
			super(LocationType.class);
	}

}
