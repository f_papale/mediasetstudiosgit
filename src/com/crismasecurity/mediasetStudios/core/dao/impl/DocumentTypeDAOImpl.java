package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.DocumentTypeDAO;
import com.crismasecurity.mediasetStudios.core.entity.DocumentType;
import com.widee.base.dao.base.impl.GenericDAOImpl;

@Repository (value="DocumentTypeDAO") 
public class DocumentTypeDAOImpl extends  GenericDAOImpl<DocumentType, Long>  implements DocumentTypeDAO{

	public DocumentTypeDAOImpl() {
			super(DocumentType.class);
	}

}
