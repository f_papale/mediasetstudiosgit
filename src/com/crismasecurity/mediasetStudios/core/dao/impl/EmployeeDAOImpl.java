package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.EmployeeDAO;
import com.crismasecurity.mediasetStudios.core.entity.Employee;
import com.widee.base.dao.base.impl.GenericDAOImpl;



/**
 * Class name: EmployeeDAOImpl
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author Gio
 * @date 30/jan/2012
 *
 */
@Repository (value="EmployeeDAO") 
public class EmployeeDAOImpl extends GenericDAOImpl<Employee, Long> implements EmployeeDAO {

	public EmployeeDAOImpl() {
		super(Employee.class);
	}
}


