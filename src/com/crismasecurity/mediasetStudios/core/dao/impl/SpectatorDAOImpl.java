package com.crismasecurity.mediasetStudios.core.dao.impl;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import org.springframework.stereotype.Repository;
import com.crismasecurity.mediasetStudios.core.dao.SpectatorDAO;
import com.crismasecurity.mediasetStudios.core.entity.Spectator;

import com.widee.base.dao.base.impl.GenericDAOImpl;

@Repository (value="SpectatorDAO")
public class SpectatorDAOImpl extends GenericDAOImpl<Spectator, Long> implements SpectatorDAO {
	

	public SpectatorDAOImpl() {
		super(Spectator.class);
	}
	
	@Override
	public List<BigDecimal> getSpectator2Wipe(int month) {
		
		List<BigDecimal> results =null;
		String sql = "SELECT id FROM [dbo].[getSpectator2Wipe] (-" + month + ")";
		try {
			Query query = getEntityManager().createNativeQuery(sql);
			results = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return results;
	}
	
	
	@Override
	public boolean removeDuplicatedSpectators(){
		
		boolean results = false;
		try {
			StoredProcedureQuery storedProcedure = getEntityManager().createStoredProcedureQuery("[dbo].[removeDuplicateSpectators]");
//		Set parameters
//		storedProcedure.registerStoredProcedureParameter("param1", Double.class, ParameterMode.IN);
//		storedProcedure.registerStoredProcedureParameter("result", Double.class, ParameterMode.OUT);
//		storedProcedure.setParameter("subtotal", 1f);
//		Execute SP
			storedProcedure.execute();
			results=true;
		} catch (Exception e) {
			e.printStackTrace();
		}
//		Get result
//		Double tax = (Double)storedProcedure.getOutputParameterValue("tax");
		return results;		
	}
	
	@Override
	public List<Object> getAdmittedSpectatorList(int idAgency) {
		
		List<Object> results =null;
		String sql = "SELECT * FROM [dbo].[getSpectatorAdmitted] (" + idAgency + ") order by 2";
		try {
			Query query = getEntityManager().createNativeQuery(sql);
			results = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return results;
	}	
}
