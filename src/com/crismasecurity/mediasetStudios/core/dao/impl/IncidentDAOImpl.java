package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.IncidentDAO;
import com.crismasecurity.mediasetStudios.core.entity.Incident;
import com.widee.base.dao.base.impl.GenericDAOImpl;

@Repository (value="IncidentDAO") 
public class IncidentDAOImpl extends  GenericDAOImpl<Incident, Long>  implements IncidentDAO{

	public IncidentDAOImpl() {
			super(Incident.class);
	}

}
