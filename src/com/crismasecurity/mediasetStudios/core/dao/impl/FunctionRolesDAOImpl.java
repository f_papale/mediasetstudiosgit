package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.FunctionRolesDAO;
import com.crismasecurity.mediasetStudios.core.entity.FunctionRoles;
import com.widee.base.dao.base.impl.GenericDAOImpl;

@Repository (value="FunctionRolesDAO") 
public class FunctionRolesDAOImpl extends  GenericDAOImpl<FunctionRoles, Long>  implements FunctionRolesDAO{

	public FunctionRolesDAOImpl() {
			super(FunctionRoles.class);
	}

}
