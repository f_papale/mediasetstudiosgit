package com.crismasecurity.mediasetStudios.core.dao.impl;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.dao.BlackListTypeDAO;
import com.crismasecurity.mediasetStudios.core.entity.BlackListType;
import com.widee.base.dao.base.impl.GenericDAOImpl;

@Repository (value="BlackListTypeDAO") 
public class BlackListTypeDAOImpl extends  GenericDAOImpl<BlackListType, Long>  implements BlackListTypeDAO{

	public BlackListTypeDAOImpl() {
		super(BlackListType.class);
	}

}
