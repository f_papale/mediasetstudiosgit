package com.crismasecurity.mediasetStudios.core.dao;

import com.crismasecurity.mediasetStudios.core.entity.BadgesTransits;
import com.widee.base.dao.base.GenericDAO;

/**
 * Class name: BadgesTransitsDAO
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author 
 * @date 25/05/2018 
 *
 */
public interface BadgesTransitsDAO extends GenericDAO<BadgesTransits, Long>{
}
