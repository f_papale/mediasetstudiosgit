package com.crismasecurity.mediasetStudios.core.dao;

import com.crismasecurity.mediasetStudios.core.entity.Statistics;
import com.widee.base.dao.base.GenericDAO;

/**
 * Class name: MediaStatusTypeDAO
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author fpapale
 * @date 25/05/2018
 *
 */
public interface StatisticsDAO extends GenericDAO<Statistics, Long>{
}
