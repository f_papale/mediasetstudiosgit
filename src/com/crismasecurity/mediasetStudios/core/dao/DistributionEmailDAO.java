package com.crismasecurity.mediasetStudios.core.dao;

import com.crismasecurity.mediasetStudios.core.entity.DistributionEmail;
import com.crismasecurity.mediasetStudios.core.entity.DistributionEmailId;
import com.widee.base.dao.base.GenericDAO;

/**
 * Class name: DistributionEmailDAO
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author fpapale
 * @date 25/05/2018
 *
 */
public interface DistributionEmailDAO extends GenericDAO<DistributionEmail, DistributionEmailId>{
}
 