package com.crismasecurity.mediasetStudios.core.dao;

import com.crismasecurity.mediasetStudios.core.entity.Location;
import com.widee.base.dao.base.GenericDAO;

public interface LocationDAO extends GenericDAO<Location, Long>{

}
