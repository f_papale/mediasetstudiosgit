package com.crismasecurity.mediasetStudios.core.dao;

import com.crismasecurity.mediasetStudios.core.entity.AgencyType;
import com.widee.base.dao.base.GenericDAO;

/**
 * Class name: AgencyTypeDAO
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author fpapale
 * @date 25/05/2018
 *
 */
public interface AgencyTypeDAO extends GenericDAO<AgencyType, Long>{
}
