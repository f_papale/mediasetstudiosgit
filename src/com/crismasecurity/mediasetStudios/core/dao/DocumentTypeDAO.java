package com.crismasecurity.mediasetStudios.core.dao;

import com.crismasecurity.mediasetStudios.core.entity.DocumentType;
import com.widee.base.dao.base.GenericDAO;

/**
 * 
 * Class name: DocumentTypeDAO
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author 	fdongmo
 * @date 	02/aug/2018
 *
 */
public interface DocumentTypeDAO extends GenericDAO<DocumentType, Long>{

}
