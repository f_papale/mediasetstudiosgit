package com.crismasecurity.mediasetStudios.core.dao;

import com.crismasecurity.mediasetStudios.core.entity.RoleMasterSlave;
import com.widee.base.dao.base.GenericDAO;

/**
 * 
 * Class name: SpectatorTypeDAO
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author 	fdongmo
 * @date 	30/may/2018
 *
 */
public interface RoleMasterSlaveDAO extends GenericDAO<RoleMasterSlave, Long>{

}
