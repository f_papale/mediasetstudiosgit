package com.crismasecurity.mediasetStudios.core.dao;

import com.crismasecurity.mediasetStudios.core.entity.EmailQueue;
import com.widee.base.dao.base.GenericDAO;

/**
 * Class name: EmailQueueDAO
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author fpapale
 * @date 25/05/2018 
 *
 */
public interface EmailQueueDAO extends GenericDAO<EmailQueue, Long>{
}
