package com.crismasecurity.mediasetStudios.core.dao;

import com.crismasecurity.mediasetStudios.core.entity.CountryCode;
import com.widee.base.dao.base.GenericDAO;

public interface CountryCodeDAO extends GenericDAO <CountryCode, Long>{
}
