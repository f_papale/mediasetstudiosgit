package com.crismasecurity.mediasetStudios.core.dao;

import com.crismasecurity.mediasetStudios.core.entity.Province;
import com.widee.base.dao.base.GenericDAO;

public interface ProvinceDAO extends GenericDAO <Province, Long>{
}
