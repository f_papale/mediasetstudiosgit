package com.crismasecurity.mediasetStudios.core.dao;

import com.crismasecurity.mediasetStudios.core.entity.LocationApparatus;
import com.crismasecurity.mediasetStudios.core.entity.ProductionEmployee;
import com.crismasecurity.mediasetStudios.core.entity.SpectatorIncident;
import com.widee.base.dao.base.GenericDAO;

/**
 * 
 * Class name: SpectatorIncidentDAO
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author 	Vincenzo Scaccia
 * @date 	12/jul/2018
 *
 */
public interface SpectatorIncidentDAO extends GenericDAO<SpectatorIncident, Long>{

}
