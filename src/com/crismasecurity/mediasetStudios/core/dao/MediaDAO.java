package com.crismasecurity.mediasetStudios.core.dao;

import com.crismasecurity.mediasetStudios.core.entity.Media;
import com.widee.base.dao.base.GenericDAO;

/**
 * Class name: MediaDAO
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author fpapale
 * @date 25/05/2018 
 *
 */
public interface MediaDAO extends GenericDAO<Media, Long>{
}
