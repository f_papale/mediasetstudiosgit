package com.crismasecurity.mediasetStudios.core.dao;

import com.crismasecurity.mediasetStudios.core.entity.Region;
import com.widee.base.dao.base.GenericDAO;

public interface RegionDAO extends GenericDAO<Region, Long>{

}
