package com.crismasecurity.mediasetStudios.core.dao;

import com.crismasecurity.mediasetStudios.core.entity.Studio;
import com.widee.base.dao.base.GenericDAO;

/**
 * Class name: StudioDAO
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author fpapale
 * @date 25/05/2018
 *
 */
public interface StudioDAO extends GenericDAO<Studio, Long>{
}
