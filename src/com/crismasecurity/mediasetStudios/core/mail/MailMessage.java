package com.crismasecurity.mediasetStudios.core.mail;

import java.util.Map;

import com.crismasecurity.mediasetStudios.core.mail.javamail.EmailStatus;

public class MailMessage {
	
	private Map<String, org.springframework.core.io.ByteArrayResource> attchs;
	private String[] destinationAddress;
	private String subject;
	private String text;
	private Long id;
	private boolean syncroMail=false;
	private EmailStatus emailStatus=EmailStatus.toSend; 
	private boolean bHTML=false;
	
	@Override
	public String toString() {
		return "MailMessage [id=" + id + ", syncroMail=" + syncroMail + ", emailStatus=" + emailStatus + "]";
	}
	public MailMessage (String[] destinationAddress, String subject, String text, Boolean bHTML) {
		this.destinationAddress = destinationAddress;
		this.subject = subject;
		this.text = text;
		this.id=0L;
		this.syncroMail=false;
		this.emailStatus=EmailStatus.toSend;
		this.setHTML(bHTML);
	}
	public MailMessage (Boolean syncroMail, Long id, String[] destinationAddress, String subject, String text, boolean bHTML) {
		this.destinationAddress = destinationAddress;
		this.subject = subject;
		this.text = text;
		this.id=id;
		this.syncroMail=syncroMail;
		this.emailStatus=EmailStatus.toSend;
		this.setHTML(bHTML);
	}

	
	public Map<String, org.springframework.core.io.ByteArrayResource> getAttchs() {
		return attchs;
	}
	
	public void setAttchs(
			Map<String, org.springframework.core.io.ByteArrayResource> attchs) {
		this.attchs = attchs;
	}
	public String[] getDestinationAddress() {
		return destinationAddress;
	}
	public void setDestinationAddress(String[] destinationAddress) {
		this.destinationAddress = destinationAddress;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public boolean isSyncroMail() {
		return syncroMail;
	}
	public void setSyncroMail(boolean syncroMail) {
		this.syncroMail = syncroMail;
	}
	public EmailStatus getEmailStatus() {
		return emailStatus;
	}
	public void setEmailStatus(EmailStatus emailStatus) {
		this.emailStatus = emailStatus;
	}
	public boolean isHTML() {
		return bHTML;
	}
	public void setHTML(boolean bHTML) {
		this.bHTML = bHTML;
	}
}
