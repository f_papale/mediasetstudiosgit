package com.crismasecurity.mediasetStudios.core.mail.javamail;

import com.crismasecurity.mediasetStudios.core.mail.MailMessage;

public interface InterfaceSendingEmail {
	public boolean emailStatus(MailMessage email);
}
