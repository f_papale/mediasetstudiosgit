package com.crismasecurity.mediasetStudios.core.mail.javamail;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.mail.MailParseException;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import com.crismasecurity.mediasetStudios.core.mail.MailMessage;
import com.crismasecurity.mediasetStudios.core.utils.PropertiesUtils;


/**
 * 
 * @author Gio
 *
 */
@Service
public class EmailSender extends JavaMailSenderImpl implements InitializingBean {
  
    private List<MailMessage> mails = null;
    private Boolean smtpSSLEnabled;
    private Thread sendingMailThread=null;
    
    private static List<InterfaceSendingEmail> listeners = new ArrayList<InterfaceSendingEmail>();
    

	public static void addListener(InterfaceSendingEmail l) {
		listeners.add(l);
	}
	
	public static void removeListener(InterfaceSendingEmail l) {
		listeners.remove(l);
	}
    
	@Override
	public void afterPropertiesSet() throws Exception {
		mails = new ArrayList<MailMessage>();
		// Avvio il thread che si occupa dell'invio delle mail
		SendMailThread tr = new SendMailThread(this);
		sendingMailThread=new Thread(tr);
		sendingMailThread.start();
	}

	public void returnStatusSent(MailMessage mail) {
		boolean returnStatusOk= true;
		System.out.println("Da MailSender: "+mail.toString());
		for (InterfaceSendingEmail lstnr : listeners) {
			returnStatusOk = lstnr.emailStatus(mail);
			if (!returnStatusOk && sendingMailThread.isAlive())
				sendingMailThread.interrupt(); 
		}
	}

	protected void setAuthProperties() throws Exception {
        Properties properties = new Properties();
        if (smtpSSLEnabled) {
            properties.setProperty("mail.smtp.auth", "true");
            properties.setProperty("mail.smtp.starttls.enable", "true");
            properties.setProperty("mail.smtp.ssl.protocols", "SSLv3 TLSv1");
            properties.setProperty("mail.smtp.timeout", "8500");
            setJavaMailProperties(properties);
        }
    }
	
	public synchronized void sendInstantMail(MailMessage mailMessage) {
		sendMail(mailMessage.getAttchs(), mailMessage.getDestinationAddress(), mailMessage.getSubject(), mailMessage.getText(),mailMessage.isHTML());	
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	
	protected  void sendMail(Map<String, org.springframework.core.io.ByteArrayResource> attchs, String[] destinationAddress, String subject, String text, boolean bHTML) {
		
		this.setHost(PropertiesUtils.getInstance().readProperty("mail.host"));
        this.setPassword(PropertiesUtils.getInstance().readProperty("mail.password"));
        if (PropertiesUtils.getInstance().readProperty("mail.port")!=null && !PropertiesUtils.getInstance().readProperty("mail.port").equals("")) this.setPort(Integer.valueOf(PropertiesUtils.getInstance().readProperty("mail.port")));
        this.setUsername(PropertiesUtils.getInstance().readProperty("mail.username"));
        this.setSmtpSSLEnabled(Boolean.valueOf(PropertiesUtils.getInstance().readProperty("mail.smtpSSLEnabled")));
		
        try {
			setAuthProperties();
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		MimeMessage message = this.createMimeMessage();

		try {
			MimeMessageHelper helper = new MimeMessageHelper(message, true);

			helper.setFrom(PropertiesUtils.getInstance().readProperty("mail.from"));
			helper.setTo(destinationAddress);
			helper.setSubject(subject);
			helper.setText(text,bHTML);
			

			if (attchs!=null) {
				for (Iterator<String> iterator = attchs.keySet().iterator(); iterator.hasNext();) {
					String name = (String) iterator.next();
					helper.addAttachment(name, attchs.get(name));
				}
			}
		} catch (MessagingException e) {
			e.printStackTrace();
			throw new MailParseException(e);
		}

		this.send(message);
		
	}

	public synchronized void  addMailToSend(MailMessage mail) {
		if(mail.isSyncroMail()) {
			sendInstantMail(mail);
		}else {
			mails.add(mail);
		}
	}
	
	protected synchronized List<MailMessage> getMails() {
		return mails;
	}
	
	protected synchronized List<MailMessage> getMailsAndClean() {
		List<MailMessage> mailsToPass = new ArrayList<MailMessage>();
		mailsToPass.addAll(mails);
		mails.clear();
		return mailsToPass;
	}


	public  Boolean getSmtpSSLEnabled() {
		return smtpSSLEnabled;
	}

	public  void setSmtpSSLEnabled(Boolean smtpSSLEnabled) {
		this.smtpSSLEnabled = smtpSSLEnabled;
	}
}

