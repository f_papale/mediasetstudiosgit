package com.crismasecurity.mediasetStudios.core.mail.javamail;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.mail.MailParseException;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import com.crismasecurity.mediasetStudios.core.mail.MailMessage;
import com.crismasecurity.mediasetStudios.core.utils.PropertiesUtils;



/**
 * Class name: ManageThermalRadarAlarmThread
 *
 * Description: 
 * 
 *
 * Company: CrismaItalia
 *
 * @author Crisma
 * @date 05/nov/2014
 *
 */
public class SendMailThread extends JavaMailSenderImpl implements Runnable {

	private EmailSender context;
	private Boolean smtpSSLEnabled;

	
	public SendMailThread (EmailSender context) {
		super();
		this.context = context;
	}
	
	private void setAuthProperties() throws Exception {
        Properties properties = new Properties();
        if (smtpSSLEnabled) {
            properties.setProperty("mail.smtp.auth", "true");
            properties.setProperty("mail.smtp.starttls.enable", "true");
            properties.setProperty("mail.smtp.ssl.protocols", "SSLv3 TLSv1");
            properties.setProperty("mail.smtp.timeout", "8500");
            setJavaMailProperties(properties);
        }
    }
	public void setSmtpSSLEnabled(final Boolean smtpSSLEnabled) {
        this.smtpSSLEnabled = smtpSSLEnabled;
    }

	
	private void log(Object msg) {
		System.out.println("[SendMailThread]:" + msg);
	}

    public void run() {
    	log("Starting thread ...");

    	while (true) {
    		List<MailMessage> mails = context.getMailsAndClean();
			for (MailMessage mailMessage : mails) {
				try {
					mailMessage.setEmailStatus(EmailStatus.sending);
					sendMail(mailMessage.getAttchs(), mailMessage.getDestinationAddress(), mailMessage.getSubject(), mailMessage.getText(), mailMessage.isHTML());
					mailMessage.setEmailStatus(EmailStatus.sent);
				} catch (Throwable e) {
					mailMessage.setEmailStatus(EmailStatus.notsent);
				} finally {
					context.returnStatusSent(mailMessage);
				}
			}
			
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
//    	log("FINE thread !!!!!!!!!");
    }
    
    
    private void sendMail(Map<String, org.springframework.core.io.ByteArrayResource> attchs, String[] destinationAddress, String subject, String text, boolean bHTML) throws Throwable {
		
    	try {
			this.setHost(PropertiesUtils.getInstance().readProperty("mail.host"));
	        this.setPassword(PropertiesUtils.getInstance().readProperty("mail.password"));
	        if (PropertiesUtils.getInstance().readProperty("mail.port")!=null && !PropertiesUtils.getInstance().readProperty("mail.port").equals("")) this.setPort(Integer.valueOf(PropertiesUtils.getInstance().readProperty("mail.port")));
	        this.setUsername(PropertiesUtils.getInstance().readProperty("mail.username"));
	        this.setSmtpSSLEnabled(Boolean.valueOf(PropertiesUtils.getInstance().readProperty("mail.smtpSSLEnabled")));
	        
			setAuthProperties();

			MimeMessage message = this.createMimeMessage();
	
			try {
				MimeMessageHelper helper = new MimeMessageHelper(message, true);
//				MimeMessageHelper helper = new MimeMessageHelper(message);
	
				helper.setFrom(PropertiesUtils.getInstance().readProperty("mail.from"));
				helper.setTo(destinationAddress);
				helper.setSubject(subject);
				helper.setText(text,bHTML);

				if (attchs!=null) {
					for (Iterator<String> iterator = attchs.keySet().iterator(); iterator.hasNext();) {
						String name = (String) iterator.next();
						helper.addAttachment(name, attchs.get(name));
					}
				}
			} catch (MessagingException e) {
				e.printStackTrace();
				throw new MailParseException(e);
			}

			this.send(message);

    	} catch (Throwable e1) {
			e1.printStackTrace();
    		throw e1;
		}
	}

	
}