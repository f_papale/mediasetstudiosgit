package com.crismasecurity.mediasetStudios.core.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Type;
import org.javers.core.metamodel.annotation.DiffIgnore;

import com.crismasecurity.mediasetStudios.core.utils.crypto.JpaCryptoConverter;
import com.crismasecurity.mediasetStudios.core.utils.crypto.JpaCryptoConverterByte;


/**
 * 
 * Class name: DocumentSpectator
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author 	fdongmo
 * @date 	02/aug/2018
 *
 */
@Entity
@javax.persistence.Table(name="mediastudios_documentSpectator")
/*
@Table(
		   name="mediastudios_agencyType",
		   uniqueConstraints=
		       @UniqueConstraint(columnNames={"description"})
		)
*/
public class DocumentSpectator implements Serializable {

	private static final long serialVersionUID = -3226648395288124030L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(nullable = false)
	private Long id ; // Unique ID
	
	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn(name="idDocumentType")
	private DocumentType documentType;
	
	//Okkio che se non inserisco il cascade non crea o cancella lo Spectator in cascata
	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn(name="idSpectator")
	private Spectator spectator;
	
	@Basic	
	private String description;  // Descrizione  / nome del file;
	
	//@Lob
	@DiffIgnore
	@Column(name = "docInBlob", columnDefinition="VARBINARY(MAX)")
	@Convert(converter = JpaCryptoConverterByte.class)
	private byte[]  docInBlob;  // documento codificato in BLOB;
	
	@Basic 
	private Date documentCreationDate; // data creazione documento
	
	@Basic
	private Date beginValidityDate;  // Data inizio validita

	@Basic
	private Date endValidityDate;  // Data fine validita

    public DocumentSpectator() {
    	super();
    }
    
    public DocumentSpectator(DocumentType dt, Spectator spectator) {
    	super();
    	this.setDocumentType(dt);
    	this.setSpectator(spectator);
    }
    
    public DocumentSpectator(DocumentType dt, Spectator spectator, String description) {
    	super();
    	this.setDocumentType(dt);
    	this.setSpectator(spectator);
    	this.setDescription(description);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
 
        if (o == null || getClass() != o.getClass())
            return false;
 
        DocumentSpectator that = (DocumentSpectator) o;
        return Objects.equals(documentType, that.documentType) &&
               Objects.equals(spectator, that.spectator);
    }
 
    @Override
    public int hashCode() {
        return Objects.hash(spectator);
    }

	@Override
	public String toString() {
		return "Descrizione [idTipoDocumento = " + getDocumentType().getId() + ", idSpectator = "+ getSpectator().getId() + "  description = " + getDescription() + "]";
	}


	public Spectator getSpectator() {
		return spectator;
	}

	public void setSpectator(Spectator spectator) {
		this.spectator = spectator;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public DocumentType getDocumentType() {
		return documentType;
	}

	public void setDocumentType(DocumentType documentType) {
		this.documentType = documentType;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDocumentCreationDate() {
		return documentCreationDate;
	}

	public void setDocumentCreationDate(Date documentCreationDate) {
		this.documentCreationDate = documentCreationDate;
	}

	public Date getBeginValidityDate() {
		return beginValidityDate;
	}

	public void setBeginValidityDate(Date beginValidityDate) {
		this.beginValidityDate = beginValidityDate;
	}

	public Date getEndValidityDate() {
		return endValidityDate;
	}

	public void setEndValidityDate(Date endValidityDate) {
		this.endValidityDate = endValidityDate;
	}

	public byte[] getDocInBlob() {
		return docInBlob;
	}

	public void setDocInBlob(byte[] docInBlob) {
		this.docInBlob = docInBlob;
	}


}
