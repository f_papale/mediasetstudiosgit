package com.crismasecurity.mediasetStudios.core.entity;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Embeddable;
import javax.persistence.Column;

@Embeddable
public class DistributionEmailId 
	implements Serializable{

	private static final long serialVersionUID = -4063077885798255521L;

	@Column(name= "idDistributionList")
	private Long idDistributionList;
	
	@Column(name= "idEmailContact")
	private Long idEmailContact;

	@SuppressWarnings("unused")
	private DistributionEmailId() {
	}

	public DistributionEmailId(Long idDistributionList, Long idEmailContact) {
		this.setidDistributionList(idDistributionList);
		this.setidEmailContact(idEmailContact); 
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
 
        if (o == null || getClass() != o.getClass()) 
            return false;
 
        DistributionEmailId that = (DistributionEmailId) o;
        return Objects.equals(idDistributionList, that.idDistributionList) && 
               Objects.equals(idEmailContact, that.idEmailContact);
    }
 
    @Override
    public int hashCode() {
        return Objects.hash(idDistributionList, idEmailContact);
    }

	public Long getidDistributionList() {
		return idDistributionList;
	}

	public void setidDistributionList(Long idDistributionList) {
		this.idDistributionList = idDistributionList;
	}

	public Long getidEmailContact() {
		return idEmailContact;
	}

	public void setidEmailContact(Long idEmailContact) {
		this.idEmailContact = idEmailContact;
	}
}
