package com.crismasecurity.mediasetStudios.core.entity;

import java.io.Serializable;


import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


/**
 * 
 * Class name: EpisodeStatusType
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author 	fpapale
 * @date 	25/may/2018
 *
 */
@Entity
//@javax.persistence.Table(name="mediastudios_episodeStatusType")

@Table(
		   name="mediastudios_episodeStatusType",
		   uniqueConstraints=
		       @UniqueConstraint(columnNames={"idStatus"})
		)

public class EpisodeStatusType implements Serializable {

	private static final long serialVersionUID = -6807514043587929768L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(nullable = false)
	private Long id ; // Unique ID
	
//	@Column
//	@Type(type = "org.hibernate.type.NumericBooleanType")
//	private boolean statusOk; 

	@Basic
	private Integer idStatus; 
	
	@Basic
	private Integer orderBy; 
	
	
	@Basic
	private String description; 
	
	@Basic
	private String owner; 

	
//	@Basic
//	private Date date;
	
	
//	@ManyToOne (fetch=FetchType.EAGER)
//	private Company masterCompany;
	
//	@ManyToMany(fetch=FetchType.LAZY) // no cascade
//	@JoinTable(name = "mediasetstudios_contracts_master_company_employees",
//	  joinColumns = @JoinColumn(name = "contract_id"),
//	  inverseJoinColumns = @JoinColumn(name = "master_company_employee_id"),
//	  uniqueConstraints=@UniqueConstraint(columnNames={"contract_id","master_company_employee_id"})
//	)
//	private List<CompanyEmployee> masterCompanyEmployees;

//	@OneToMany(cascade=CascadeType.ALL, orphanRemoval=true, fetch=FetchType.LAZY)
//	private List<Allegato5Data> listaAllegati5;
	
//	@OneToMany(cascade=CascadeType.ALL, orphanRemoval=true, mappedBy="contract", fetch=FetchType.LAZY)
//	private List<SubcontractData> subcontractCompanies;

    public EpisodeStatusType() {
    	super();
    }
  


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EpisodeStatusType other = (EpisodeStatusType) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}







	@Override
	public String toString() {
		return "EpisodeStatusType [id=" + id + ", idStatus=" + idStatus + ", orderBy=" + orderBy + ", description="
				+ description + ", owner=" + owner + "]";
	}



	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description= description;
	}



	public Integer getIdStatus() {
		return idStatus;
	}



	public void setIdStatus(Integer idStatus) {
		this.idStatus = idStatus;
	}



	public String getOwner() {
		return owner;
	}



	public void setOwner(String owner) {
		this.owner = owner;
	}



	public Integer getOrderBy() {
		return orderBy;
	}



	public void setOrderBy(Integer orderBy) {
		this.orderBy = orderBy;
	}


}
