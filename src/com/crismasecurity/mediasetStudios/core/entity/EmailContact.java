package com.crismasecurity.mediasetStudios.core.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import org.javers.core.metamodel.annotation.DiffIgnore;

import com.crismasecurity.mediasetStudios.core.utils.TextUtil;

/**
 * 
 * Class name: Contatti Email
 *
 * Description: Entity relativa alla classe Contatti Email
 * 
 *
 * Company: 
 *
 * @author 	
 * @date 	04/jul/2018
 *
 */

@Entity
//@javax.persistence.Table(name="mediastudios_emailContact")

@javax.persistence.Table(
		   name="mediastudios_emailContact",
		   uniqueConstraints=
		   @javax.persistence.UniqueConstraint(columnNames={"surname","name", "managerEmail"})
		)

public class EmailContact implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2995851711028826260L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(nullable = false)
	private Long id ; // Unique ID

	@DiffIgnore
    @OneToMany(
        mappedBy = "emailContact",
        cascade = CascadeType.ALL,
        orphanRemoval = true
    )
    private List<DistributionEmail> distributionsEmails = new ArrayList<>();

	
	@Basic
	private String name;  //Nome
	
	@Basic
	private String surname; //cognome
	
	@Basic
	private String managerEmail; // email Responsabile Produzione;
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof EmailContact)) {
			return false;
		}
		EmailContact other = (EmailContact) obj;
		if (this.getId() == null) {
			if (other.getId() != null) {
				return false;
			}
		} else if (!(this.getId()).equals(other.getId())) {
			return false;
		}
		
//		if (managerEmail == null) {
//			if (other.managerEmail != null) {
//				return false;
//			}
//		} else if (!managerEmail.equals(other.managerEmail)) {
//			return false;
//		}
//		if (name == null) {
//			if (other.name != null) {
//				return false;
//			}
//		} else if (!name.equals(other.name)) {
//			return false;
//		}
//		if (surname == null) {
//			if (other.surname != null) {
//				return false;
//			}
//		} else if (!surname.equals(other.surname)) {
//			return false;
//		}
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	@Override
	public String toString() {
		return "EmailContact [id=" + id + ", name=" + getName() + ", surname=" + getSurname() + ", managerEmail=" + getManagerEmail()
				+ "]";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name.trim().toUpperCase();
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname.trim().toUpperCase();
	}

	public String getManagerEmail() {
		return managerEmail;
	}

	public void setManagerEmail(String managerEmail) {
		this.managerEmail = TextUtil.rearrangeEmailList(managerEmail.trim().toLowerCase());
	}

	/**
	 * @return the distributions
	 */
	public List<DistributionEmail> getDistributionsEmails() {
		return distributionsEmails;
	}

	/**
	 * @param distributions the distributions to set
	 */
	public void setDistributionsEmails(List<DistributionEmail> distributionsEmails) {
		this.distributionsEmails = distributionsEmails;
	}
}