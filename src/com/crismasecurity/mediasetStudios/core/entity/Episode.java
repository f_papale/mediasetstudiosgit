package com.crismasecurity.mediasetStudios.core.entity;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;
import org.primefaces.model.StreamedContent;
import com.crismasecurity.mediasetStudios.core.utils.DateUtils;


/**
 * 
 * Class name: Episode
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author 	fpapale
 * @date 	25/may/2018
 *
 */
@Entity
@javax.persistence.Table(name="mediastudios_episode")
/*
@Table(
		   name="mediastudios_agencyType",
		   uniqueConstraints=
		       @UniqueConstraint(columnNames={"description"})
		)
*/
public class Episode implements Serializable {

	

	private static final long serialVersionUID = -3921975231769415815L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(nullable = false)
	private Long id ; // Unique ID
	
	@Column
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean checkinStarted; 
	@Column
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean checkoutStarted; 

//	@ManyToOne (fetch = FetchType.EAGER)
//	@JoinColumn (name = "idEpisode")
//	private Episode episode ;
	
//	@Basic	
//	private String name; 		// Denominazione;

	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn (name = "idProduction")
	private Production production;
	

	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn (name = "idStudio")
	private Studio studio;
	
	
	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn (name = "idEpisodeStatus")
	private EpisodeStatusType episodeStatus;
	
	@Basic
	private Date dateFrom;
	
	@Basic
	private Date dateTo;
	
	@Basic
	private Long requestedSpectators;
	
	@Basic
	private Long effectiveSpectators;
	
	@Basic
	private Date attendanceDateHour;

	@Transient
	private StreamedContent documentFileDownload;
	

	//
	
	
//	@ManyToMany(fetch=FetchType.LAZY) // no cascade
//	@JoinTable(name = "mediasetstudios_contracts_master_company_employees",
//	  joinColumns = @JoinColumn(name = "contract_id"),
//	  inverseJoinColumns = @JoinColumn(name = "master_company_employee_id"),
//	  uniqueConstraints=@UniqueConstraint(columnNames={"contract_id","master_company_employee_id"})
//	)
//	private List<CompanyEmployee> masterCompanyEmployees;
//	
//	@OneToMany(cascade=CascadeType.ALL, orphanRemoval=true, fetch=FetchType.LAZY)
//	private List<Allegato5Data> listaAllegati5;
	
	
	@OneToMany(cascade=CascadeType.ALL, orphanRemoval=true, mappedBy="episode", fetch=FetchType.LAZY)
	private List<SpectatorEpisode> spectators;
	
	
	/**
     * Costruttore
     */
    public Episode() {
    	super();
    }
    
    
    public int getSpectatorsNumberWithCheckin() {
    	if(spectators!=null) {
    		int tot=0;
    		for (SpectatorEpisode a : spectators) {
				if (a.isCheckin()) tot++;
			}
    		return tot;
    	}
    	return 0;
    }
    
    public int getSpectatorsNumberWithCheckout() {
    	if(spectators!=null) {
    		int tot=0;
    		for (SpectatorEpisode a : spectators) {
				if (a.isCheckout()) tot++;
			}
    		return tot;
    	}
    	return 0;
    }
    
    public String getDateFromOnlyMonth(){
    	if (dateFrom!=null) {
    		return (new SimpleDateFormat("MMM")).format(dateFrom);
    	}
    	return "";
    }
    
    
    public String getDateFromToOnlyMonth(){
    	if (dateTo!=null) {
    		return (new SimpleDateFormat("MM")).format(dateTo);
    	}
    	return "";
    }

    public String getDateFromOnlyYear(){
    	if (dateFrom!=null) {
    		return (new SimpleDateFormat("yyyy")).format(dateFrom);
    	}
    	return "";
    }
    
    
    public String getDateFromToOnlyYear(){
    	if (dateTo!=null) {
    		return (new SimpleDateFormat("yyyy")).format(dateTo);
    	}
    	return "";
    }
    
    public String getDateFromOnlyDate(){
    	if (dateFrom!=null) {
    		return (new SimpleDateFormat("dd/MM/yyyy")).format(dateFrom);
    	}
    	return "";
    }
    
    
    public String getDateFromOnlyDateTime(){
    	if (dateFrom!=null) {
    		return (new SimpleDateFormat("dd/MM/yyyy HH:mm")).format(dateFrom);
    	}
    	return "";
    }

    
    public String getDateFromOnlyHours(){
    	if (dateFrom!=null) {
    		return (new SimpleDateFormat("HH:mm:ss")).format(dateFrom);
    	}
    	return "";
    }
    public String getDateToOnlyDate(){
    	if (dateTo!=null) {
    		return (new SimpleDateFormat("dd/MM/yyyy")).format(dateTo);
    	}
    	return "";
    }
    public String getDateToOnlyHours(){
    	if (dateTo!=null) {
    		return (new SimpleDateFormat("HH:mm:ss")).format(dateTo);
    	}
    	return "";
    }
    
    public String getAttendanceDateHourOnlyHours(){
    	if (dateTo!=null) {
    		return (new SimpleDateFormat("HH:mm")).format(attendanceDateHour);
    	}
    	return "";
    }
    
	public Long getId() {
		return id;
	}
		
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Episode other = (Episode) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}


	public Production getProduction() {
		return production;
	}


	public void setProduction(Production production) {
		this.production = production;
	}


	public Studio getStudio() {
		return studio;
	}


	public void setStudio(Studio studio) {
		this.studio = studio;
	}


	public EpisodeStatusType getEpisodeStatus() {
		return episodeStatus;
	}


	public void setEpisodeStatus(EpisodeStatusType episodeStatus) {
		this.episodeStatus = episodeStatus;
	}


	public Date getDateFrom() {
		return dateFrom;
	}


	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}


	public Date getDateTo() {
		return dateTo;
	}


	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}


	public Long getRequestedSpectators() {
		return requestedSpectators;
	}


	public void setRequestedSpectators(Long requestedSpectators) {
		this.requestedSpectators = requestedSpectators;
	}


	public Long getEffectiveSpectators() {
		return effectiveSpectators;
	}


	public void setEffectiveSpectators(Long effectiveSpectators) {
		this.effectiveSpectators = effectiveSpectators;
	}


	public List<SpectatorEpisode> getSpectators() {
		return spectators;
	}


	public void setSpectators(List<SpectatorEpisode> spectators) {
		this.spectators = spectators;
	}
	
	public String getVirtualName() {
		return ("Puntata del");
	}
	
	@Override
	public String toString() {
		return "Episode [id=" + id + ", episodeStatus=" + episodeStatus + ", dateFrom=" + dateFrom + ", dateTo="
				+ dateTo + ", requestedSpectators=" + requestedSpectators + ", effectiveSpectators="
				+ effectiveSpectators  + "]";
	}


	public boolean isCheckinStarted() {
		return checkinStarted;
	}


	public void setCheckinStarted(boolean checkinStarted) {
		this.checkinStarted = checkinStarted;
	}


	public boolean isCheckoutStarted() {
		return checkoutStarted;
	}


	public void setCheckoutStarted(boolean checkoutStarted) {
		this.checkoutStarted = checkoutStarted;
	}


	public StreamedContent getDocumentFileDownload() {
		return documentFileDownload;
	}


	public void setDocumentFileDownload(StreamedContent documentFileDownload) {
		this.documentFileDownload = documentFileDownload;
	}


	public Date getAttendanceDateHour() {
		return attendanceDateHour;
	}


	public void setAttendanceDateHour(Date attendanceDateHour) {
		this.attendanceDateHour = attendanceDateHour;
	}

	public boolean isCheckoutTerminated() {
		//return ((DateUtils.getDayDiffNotAbs(getDateTo(), new Date())>0) && (isCheckoutStarted()));
		return ((DateUtils.HoursElapsedFromNow(getDateTo())>=1)&&(isCheckoutStarted()));
	}

	public boolean isEpisodeTerminated() {
//		return ((DateUtils.getDayDiffNotAbs(getDateTo(), new Date())>0));
		return (DateUtils.HoursElapsedFromNow(getDateTo())>=0);
	}

}
