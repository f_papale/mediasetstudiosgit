package com.crismasecurity.mediasetStudios.core.entity;

import java.io.Serializable;
import java.util.Base64;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;
import org.primefaces.model.ByteArrayContent;
import org.primefaces.model.StreamedContent;

import com.crismasecurity.mediasetStudios.core.utils.TextUtil;

/**
 * 
 * Class name: Agenzia
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author 	fpapale
 * @date 	25/may/2018
 *
 */

@Entity
@javax.persistence.Table(name="mediastudios_production")
public class Production implements Serializable, Comparable<Production>  {

	private static final long serialVersionUID = -6681398179677486305L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(nullable = false)
	private Long id ; // Unique ID idProduction
	
//	@Column
//	@Type(type = "org.hibernate.type.NumericBooleanType")
//	private boolean statusOk; 	

	@ManyToOne (fetch=FetchType.EAGER)
	@JoinColumn (name = "idProductionType")	
	private ProductionType productionType ;
	
	@ManyToOne (fetch=FetchType.EAGER)
	@JoinColumn (name = "idAgency")	
	private Agency agency ;

	@ManyToOne (fetch=FetchType.EAGER)
	@JoinColumn (name = "idOwner")	
	private Agency owner ;
	
//	@ManyToOne (fetch=FetchType.EAGER)
//	private ProductionType productionType ;

	@Basic	
	private String name; 		// Descrizione;	
	@Basic	
	private String description; // Descrizione;

	
	@Basic
	private String season;		// Stagione;
	@Basic
	private int episodesNum;	// NumeroPuntate;
	@Basic
	private int spectatorsReq;	// Spettatori Richiesti
	@Column
	@Type(type = "org.hibernate.type.DateType")
	private Date startDate;		// DataInizio
	@Column
	@Type(type = "org.hibernate.type.DateType")
	private Date endDate;		// DataFine
	
	@Column(columnDefinition = "VARCHAR(MAX)")
	private String managerEmail; // email Responsabile Produzione;
	
	@ManyToOne (fetch=FetchType.LAZY)
	@JoinColumn (name = "idDistributionList")	
	private DistributionList distributionList ;
	
	@Basic
	private String features;	// Caratteristiche

	
	@OneToMany(
			cascade=CascadeType.ALL, 
			orphanRemoval=true, 
			mappedBy="production", 
			fetch=FetchType.LAZY)
	private List<Episode> episodes;
	
	@OneToMany(
			cascade=CascadeType.ALL, 
			orphanRemoval=true, 
			mappedBy="production", 
			fetch=FetchType.LAZY)
	private List<ProductionEmployee> productionEmployee;
	
	
	@Column(columnDefinition = "NVARCHAR(MAX)")
	//@Lob
	private String logo;		// Logo puntata. Formato--> <content_type>#<file_name>#base64encoded_file_content
	
//	@Basic
//	private Date date;
	
	
//	@ManyToOne (fetch=FetchType.EAGER)
//	private Company masterCompany;
//	
//	@ManyToMany(fetch=FetchType.LAZY) // no cascade
//	@JoinTable(name = "mediasetstudios_contracts_master_company_employees",
//	joinColumns = @JoinColumn(name = "contract_id"),
//	inverseJoinColumns = @JoinColumn(name = "master_company_employee_id"),
//	uniqueConstraints=@UniqueConstraint(columnNames={"contract_id","master_company_employee_id"})
//	)
//	private List<CompanyEmployee> masterCompanyEmployees;
//	
//	@OneToMany(cascade=CascadeType.ALL, orphanRemoval=true, fetch=FetchType.LAZY)
//	private List<Allegato5Data> listaAllegati5;
	
	
//	@OneToMany(cascade=CascadeType.ALL, orphanRemoval=true, mappedBy="contract", fetch=FetchType.LAZY)
//	private List<SubcontractData> subcontractCompanies;
	
	
	/**
     * Costruttore
     */
    public Production() {
    	super();
    }
    
    @Transient
    private StreamedContent logoResource;
    
    
    public StreamedContent getLogoResource() {
    	if (logoResource==null){
	    	if (getLogo()!=null && !getLogo().equals("")) {
				try {
					int indHash= getLogo().indexOf("#");
					String contentType = getLogo().substring(0, indHash);
					int indHash2= getLogo().indexOf("#", indHash+1);
					String filename = getLogo().substring(indHash+1, indHash2);
					String content1 = getLogo().substring(indHash2+1);
					
					logoResource = new ByteArrayContent(Base64.getDecoder().decode(content1), contentType, filename);
				} catch(Exception e1) {
					e1.printStackTrace();
				}
			}
			else logoResource = null;
    	}
    	return logoResource;
    }
    
    
    
    
    
    
    

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Production other = (Production) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "Production [id=" + id + ", productionType=" + productionType + ", agency=" + agency + ", owner=" + owner
				+ ", name=" + name + ", description=" + description + ", season=" + season + ", episodesNum="
				+ episodesNum + ", spectatorsReq=" + spectatorsReq + ", startDate=" + startDate + ", endDate=" + endDate
				+ ", managerEmail=" + managerEmail + ", features=" + features /*+ ", episodes=" + episodes
				+ ", productionEmployee=" + productionEmployee */+ "]";
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSeason() {
		return season;
	}


	public void setSeason(String season) {
		this.season = season;
	}


	public int getEpisodesNum() {
		return episodesNum;
	}


	public void setEpisodesNum(int episodesNum) {
		this.episodesNum = episodesNum;
	}


	public int getSpectatorsReq() {
		return spectatorsReq;
	}


	public void setSpectatorsReq(int spectatorsReq) {
		this.spectatorsReq = spectatorsReq;
	}


	public Date getStartDate() {
		return startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public String getFeatures() {
		return features;
	}


	public void setFeatures(String features) {
		this.features = features;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public ProductionType getProductionType() {
		return productionType;
	}


	public void setProductionType(ProductionType productionType) {
		this.productionType = productionType;
	}


	public List<Episode> getEpisodes() {
		return episodes;
	}


	public void setEpisodes(List<Episode> episodes) {
		this.episodes = episodes;
	}


	public Agency getAgency() {
		return agency;
	}


	public void setAgency(Agency agency) {
		this.agency = agency;
	}


	public List<ProductionEmployee> getProductionEmployee() {
		return productionEmployee;
	}


	public void setProductionEmployee(List<ProductionEmployee> productionEmployee) {
		this.productionEmployee = productionEmployee;
	}


	public Agency getOwner() {
		return owner;
	}


	public void setOwner(Agency owner) {
		this.owner = owner;
	}


	public String getManagerEmail() {
		return managerEmail;
	}


	public void setManagerEmail(String managerEmail) {
		this.managerEmail = TextUtil.rearrangeEmailList(managerEmail);
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
		logoResource = null; // rigenero content
	}
	
	public DistributionList getDistributionList() {
		return distributionList;
	}
	
	public void setDistributionList(DistributionList distributionList) {
		this.distributionList = distributionList;
	}

	@Override
	public int compareTo(Production p) {
		// TODO Auto-generated method stub
		return  this.getId().compareTo(p.getId());
	}
	
	public int compareByName(Production p) {
		// TODO Auto-generated method stub
		return  this.getName().compareTo(p.getName());
	}
	
	public int compareBySeasonName(Production p) {
		// TODO Auto-generated method stub
		return  (this.getSeason()+" "+getName()).compareTo((p.getSeason()+" "+p.getName()));
	}

}
