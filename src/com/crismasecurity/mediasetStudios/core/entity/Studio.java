package com.crismasecurity.mediasetStudios.core.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


/**
 * 
 * Class name: Studio
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author 	fpapale
 * @date 	25/may/2018
 *
 */
@Entity
//@javax.persistence.Table(name="mediastudios_studio")
@Table(
		   name="mediastudios_studio",
		   uniqueConstraints=
		       @UniqueConstraint(columnNames={"idProductionCenter","name"})
		   )

public class Studio implements Serializable {

	private static final long serialVersionUID = -3380077910551641204L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(nullable = false)
	private Long id ; // Unique ID
	
//	@Column
//	@Type(type = "org.hibernate.type.NumericBooleanType")
//	private boolean statusOk; 	
	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn (name = "idProductionCenter")
	private ProductionCenter productionCenter;
	
	@Basic	
	private String name; 		// Denominazione;
	@Basic
	private String site; 		// Ubicazione;
	@Basic
	private long capacity;		// Capienza;

//	@Basic
//	private Date date;
	
	
//	@ManyToOne (fetch=FetchType.EAGER)
//	private Company masterCompany;
//	
//	@ManyToMany(fetch=FetchType.LAZY) // no cascade
//	@JoinTable(name = "mediasetstudios_contracts_master_company_employees",
//	  joinColumns = @JoinColumn(name = "contract_id"),
//	  inverseJoinColumns = @JoinColumn(name = "master_company_employee_id"),
//	  uniqueConstraints=@UniqueConstraint(columnNames={"contract_id","master_company_employee_id"})
//	)
//	private List<CompanyEmployee> masterCompanyEmployees;
//	
//	@OneToMany(cascade=CascadeType.ALL, orphanRemoval=true, fetch=FetchType.LAZY)
//	private List<Allegato5Data> listaAllegati5;
	
	
	
	/**
     * Costruttore
     */
    public Studio() {
    	super();
    }
    
  
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Studio other = (Studio) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	
	@Override
	public String toString() {
		return "Denominazione [id = " + id + ", name = " + getName() + "]";
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getSite() {
		return site;
	}


	public void setSite(String site) {
		this.site = site;
	}


	public long getCapacity() {
		return capacity;
	}


	public void setCapacity(long capacity) {
		this.capacity = capacity;
	}


	public ProductionCenter getProductionCenter() {
		return productionCenter;
	}


	public void setProductionCenter(ProductionCenter productionCenter) {
		this.productionCenter = productionCenter;
	}

}
