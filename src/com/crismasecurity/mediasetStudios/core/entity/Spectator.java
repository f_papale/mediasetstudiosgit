package com.crismasecurity.mediasetStudios.core.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.javers.core.metamodel.annotation.DiffIgnore;
import org.primefaces.model.StreamedContent;

import com.crismasecurity.mediasetStudios.core.utils.Gender;
import com.crismasecurity.mediasetStudios.core.utils.TextUtil;
import com.crismasecurity.mediasetStudios.core.utils.crypto.JpaCryptoConverter;
import com.ibm.icu.util.GregorianCalendar;

/**
 * 
 * Class name: Spettatore
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author 	fdongmo
 * @date 	30/may/2018
 *
 */

@Entity
@Table(name="mediastudios_spectator")
/*
@Table(
		name="mediastudios_spectator",
		uniqueConstraints={
				@UniqueConstraint(
						columnNames={"surname", "name", "birthDate"}
				)
			}
		)
*/

public class Spectator implements Serializable {

	private static final long serialVersionUID = 59435812222119837L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(nullable = false)
	private Long id ; // Unique ID
	
	
	@DiffIgnore
    @OneToMany(
        mappedBy = "spectator",
        cascade = CascadeType.ALL,
        orphanRemoval = true
//        , fetch = FetchType.EAGER
    )
    private List<AgencySpectator> agencies = new ArrayList<>();
    
//	@DiffIgnore
    @OneToMany(
            mappedBy = "spectator",
            cascade = CascadeType.ALL,
            orphanRemoval = true
            
     )
    private List<DocumentSpectator> documents = new ArrayList<>();
    
	@DiffIgnore
    @OneToMany(
    		fetch = FetchType.LAZY,
            mappedBy = "spectator",
            cascade = CascadeType.ALL,
            orphanRemoval = true
     )
    private List<SpectatorEpisode> episodes = new ArrayList<>();
	
	@DiffIgnore
    @OneToMany(
			cascade=CascadeType.ALL, 
			orphanRemoval=true, 
			mappedBy="spectator", 
			fetch=FetchType.LAZY)
	private List<SpectatorIncident> incidents = new ArrayList<>();
    

	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn (name = "idSpectatorType")
	private SpectatorType spectatorType ;
	
	@Column
	@Convert(converter = JpaCryptoConverter.class)	
	private String fiscalCode; 		// codice fiscale;
	
	@Column
//	@Convert(converter = JpaCryptoConverter.class)	
	private String en_surname;			// cognome
	
	@Column
//	@Convert(converter = JpaCryptoConverter.class)	
	private String surname;			// cognome

	@Basic
	private String name;			// nome
	
	@Basic
	private String fullName;		// fullName
	
	@Basic
	private Gender gender;          //sesso 
	
	@Column
	@Type(type = "org.hibernate.type.DateType")
	private Date birthDate;			// data nascita	
	
	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn (name = "idMunicipality")
	private CountryCode city;
	
	@Basic
	private String birthCity;	// altro Luogo di nascita;
	
	@Basic
	private String address1;	// Indirizzo1;
	@Basic
	private String address2;	// Indirizzo2;
	@Basic
	private String telephone;	// Telefono
	@Column
	@Convert(converter = JpaCryptoConverter.class)	
	private String mobile;		// Cellulare
	@Column
	@Convert(converter = JpaCryptoConverter.class)	
	private String email;		// Email

	@Basic
	private String note;		// Note :  il campo serve per inserire una nota relativa allo spettatore che non viene caricato nell'anagrafica	
	
//	@Basic
//	private String documentPath;
	@Transient
	private StreamedContent documentFileDownload;
	
	@DiffIgnore
	@Column(nullable=false, columnDefinition="int default 0")
//	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean toReview = false;
	
//	@Column
//	@Generated( GenerationTime.INSERT )
//	@ColumnDefault( "CURRENT_DATE" )
	
//	@Column
//	@Temporal(TemporalType.TIMESTAMP)	

	@CreationTimestamp
	@Temporal(TemporalType.DATE)
	@Column(name = "createDate")
	private Date createDate;	

	/**
     * Costruttore
     */
    public Spectator() {
    	super();
    }
    
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((surname == null) ? 0 : surname.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Spectator other = (Spectator) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (surname == null) {
			if (other.surname != null)
				return false;
		} else if (!surname.equals(other.surname))
			return false;
		return true;
	}

	public Integer getAge() {
		if (birthDate!=null) {
			Calendar nas = Calendar.getInstance();
			nas.setTime(birthDate);
			
			GregorianCalendar cal = new GregorianCalendar();
	        int y, m, d, a;         

	        y = cal.get(Calendar.YEAR);
	        m = cal.get(Calendar.MONTH);
	        d = cal.get(Calendar.DAY_OF_MONTH);
	        cal.set(nas.get(Calendar.YEAR), nas.get(Calendar.MONTH), nas.get(Calendar.DAY_OF_MONTH));
	        a = y - cal.get(Calendar.YEAR);
	        if ((m < cal.get(Calendar.MONTH)) || ((m == cal.get(Calendar.MONTH)) && (d < cal.get(Calendar.DAY_OF_MONTH)))) {
	                --a;
	        }
	        if(a < 0) return null; //throw new IllegalArgumentException("Age < 0");
	        return a;
		}
		else return null;
	}
	
	public String getFullName() {

//		return getName()+((getName()!=null&&!getName().equals("")&&getSurname()!=null&&!getSurname().equals(""))?" ":"")+getSurname();
		return this.fullName;
	}
	
	public String getFullSurname() {
		return getSurname()+((getName()!=null&&!getName().equals("")&&getSurname()!=null&&!getSurname().equals(""))?" ":"")+getName();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFiscalCode() {
		if ((fiscalCode!=null)&&(!fiscalCode.isEmpty()))
			return fiscalCode.toUpperCase();
		else 
			return fiscalCode;
	}

	public void setFiscalCode(String fiscalCode) {
		if ((fiscalCode!=null)&&(!fiscalCode.isEmpty()))
			this.fiscalCode = fiscalCode.toUpperCase();
		else 
			this.fiscalCode=fiscalCode;
	}

	public String getSurname() {
		if (this.surname!=null && !this.surname.isEmpty())
		return surname.toUpperCase().trim();
		else return surname;
	}

	public void setSurname(String surname) {
		if ((surname!=null)&&(!surname.isEmpty()))
			this.surname = surname.toUpperCase().trim();
		else 
			this.surname = surname;
		this.setFullName((getSurname()!=null&&!getSurname().equals("")?getSurname():"")+(getName()!=null&&!getName().equals("")?" "+getName():""));
	}

	public String getName() {
		if (this.name!=null && !this.name.isEmpty())
		return name.toUpperCase().trim();
		else return name;
	}

	public void setName(String name) {
		if ((name!=null) && (!name.isEmpty()))
			this.name = name.toUpperCase().trim();
		else 
			this.name = name;
		this.setFullName((getSurname()!=null&&!getSurname().equals("")?getSurname():"")+(getName()!=null&&!getName().equals("")?" "+getName():""));		
	}

	@Override
	public String toString() {
		return "Denominazione [id = " + id + ", name = " + name + "]";
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}
	
	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = TextUtil.cleanTextContent(email);
	}


	public SpectatorType getSpectatorType() {
		return spectatorType;
	}


	public void setSpectatorType(SpectatorType spectatorType) {
		this.spectatorType = spectatorType;
	}


    public Gender[] getGenders() {
        return Gender.values();
    }


	public StreamedContent getDocumentFileDownload() {
		return documentFileDownload;
	}


	public void setDocumentFileDownload(StreamedContent documentFileDownload) {
		this.documentFileDownload = documentFileDownload;
	}

	public List<AgencySpectator> getAgencies() {
		return agencies;
	}


	public void setAgencies(List<AgencySpectator> agencies) {
		this.agencies = agencies;
	}


	public CountryCode getCity() {
		return city;
	}


	public void setCity(CountryCode city) {
		this.city = city;
	}


	public List<DocumentSpectator> getDocuments() {
		return documents;
	}


	public void setDocuments(List<DocumentSpectator> documents) {
		this.documents = documents;
	}
	
	public List<SpectatorEpisode> getEpisodes() {
		return episodes;
	}


	public void setEpisodes(List<SpectatorEpisode> episodes) {
		this.episodes = episodes;
	}


	public String getNote() {
		return note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public List<SpectatorIncident> getIncidents() {
		return incidents;
	}


	public void setIncidents(List<SpectatorIncident> incidents) {
		this.incidents = incidents;
	}

	public String getEn_surname() {
		return en_surname;
	}

	public void setEn_surname(String en_surname) {
		this.en_surname = en_surname;
	}
	
    public static Spectator clone( Spectator other ) {
    	Spectator newSpectator = new Spectator ();
    	newSpectator.id=other.id;
    	newSpectator.address1=other.address1;
    	newSpectator.address2=other.address2;
    	newSpectator.agencies=other.agencies;
    	newSpectator.birthDate=other.birthDate;
    	newSpectator.city=other.city;
    	newSpectator.documents=other.documents;
    	newSpectator.email=other.email;
    	newSpectator.fiscalCode=other.fiscalCode;
    	newSpectator.gender=other.gender;
    	newSpectator.incidents=other.incidents;
    	newSpectator.mobile=other.mobile;
    	newSpectator.name=other.name;
    	newSpectator.spectatorType=other.spectatorType;
    	newSpectator.note=other.note;
    	newSpectator.surname=other.surname;
    	newSpectator.telephone=other.telephone;
    	newSpectator.birthCity=other.birthCity;
    	newSpectator.fullName=other.fullName;

        return newSpectator;
   }


	public boolean isToReview() {
		return toReview;
	}


	public void setToReview(boolean toReview) {

		this.toReview = toReview;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getBirthCity() {
		return birthCity;
	}

	public void setBirthCity(String birthCity) {
		this.birthCity = birthCity;
	}
}
