package com.crismasecurity.mediasetStudios.core.entity;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.hibernate.annotations.Type;
import org.hibernate.bytecode.internal.javassist.FieldHandled;
import org.hibernate.bytecode.internal.javassist.FieldHandler;
import org.javers.core.metamodel.annotation.DiffIgnore;

/**
 * 
 * Class name: Spettatore
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author 	fdongmo
 * @date 	30/may/2018
 *
 */

@Entity
@javax.persistence.Table(name="mediastudios_spectator_episode", indexes = {

	       @Index(columnList = "idEpisode", name = "idEpisode_hidx", unique = false),
	       @Index(columnList = "idSpectator, signDate" , name = "idSpectatorSignDate_hidx", unique = false),

	})
public class SpectatorEpisode implements Serializable, FieldHandled {

	private static final long serialVersionUID = 89393485691130082L;

	private FieldHandler fieldHandler;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(nullable = false)
	private Long id ; // Unique ID
	
	@Basic	
	private String guid; // codice univoco;
	
	// Attenzione la Produzione deve essere eliminata altrimenti mi trovo una relazione tra SpettatoreEpisodio e Produzione. In realta la Produzione la devo prendere da Episodi
	@ManyToOne (fetch = FetchType.LAZY)
	@JoinColumn (name = "idProduction")
	private Production production;
	
	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn (name = "idSpectator")
	private Spectator spectator;
	
	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn (name = "idEpisode")
	private Episode episode;
	
	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn (name = "idMedia")
	private Media media;
	
	@Basic	
	private String description;
	
	@Column
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean vip; 	
	
	@Column
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean checkin; 	
	
	@Column
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean checkout; 

	@Column
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean preCheckin; 
	
	@DiffIgnore
	@Column(nullable=false, columnDefinition="int default 0")
//	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean spectatorToReview = false;
	
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	//@Type(type = "org.hibernate.type.DateType")
	private Date checkinDate;
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	//@Type(type = "org.hibernate.type.DateType")
	private Date checkoutDate;
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	//@Type(type = "org.hibernate.type.DateType")
	private Date badgeReturnDate;
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	//@Type(type = "org.hibernate.type.DateType")
	private Date signDate;
	
	@DiffIgnore
	@Column(columnDefinition = "NVARCHAR(MAX)")
	//@Lob
	private String sign;
	
//	@Column(columnDefinition = "NVARCHAR(MAX)")
//	@Convert(converter = JpaCryptoConverter.class)
//	private String pdfSignBase64Str;		// liberatoria
	
//	@DiffIgnore
	@OneToOne(
			mappedBy = "spectatorEpisode"
			, cascade = CascadeType.ALL
			, orphanRemoval = true
			, fetch = FetchType.LAZY
			, optional = true)
	@LazyToOne(LazyToOneOption.NO_PROXY)
	private Liberatoria liberatoria;
	
	
	/*@Lob @Basic(fetch = FetchType.LAZY)
	@Column(name="pdf_sign_doc") // , columnDefinition="BLOB NOT NULL"
	protected byte[] pdfSign;*/ // Questom da problemi sul DISTINCT !!!!!
	
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	//@Type(type = "org.hibernate.type.DateType")
	private Date entranceDate;
	
	@Transient
	private int order;

	
	/**
     * Costruttore
     */
    public SpectatorEpisode() {
    	super();
    }
    
    public SpectatorEpisode(Production pr, Episode ep, Spectator sp ){
    	this.production = pr;
    	this.episode = ep;
    	this.spectator = sp;
    	this.vip=false;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SpectatorEpisode other = (SpectatorEpisode) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SpectatorEpisode [id=" + id + ", guid=" + guid + ", production=" + (production!=null?production.getId():"") + ", spectator="
				+ (spectator!=null?spectator.getId():"") + ", episode=" + (episode!=null?episode.getId():"") + ", media=" + (media!=null?media.getId():"") + ", description=" + description + ", vip="
				+ vip + ", checkin=" + checkin + ", checkout=" + checkout + ", preCheckin=" + preCheckin
				+ ", checkinDate=" + checkinDate + ", checkoutDate=" + checkoutDate + ", sign=" + sign
				+ ", entranceDate=" + entranceDate + "]";
	}

	
	public boolean existsLiberatoria() {
		if (liberatoria==null) return false;
		return true;
	}
	
	
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}

	public String getSign() {
		return sign;
	}


	public void setSign(String sign) {
		this.sign = sign;
	}


	public Production getProduction() {
		return production;
	}


	public void setProduction(Production production) {
		this.production = production;
	}


	public Spectator getSpectator() {
		return spectator;
	}


	public void setSpectator(Spectator spectator) {
		this.spectator = spectator;
	}


	public Episode getEpisode() {
		return episode;
	}


	public void setEpisode(Episode episode) {
		this.episode = episode;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public boolean isCheckin() {
		return checkin;
	}


	public void setCheckin(boolean checkin) {
		this.checkin = checkin;
	}


	public boolean isCheckout() {
		return checkout;
	}


	public void setCheckout(boolean checkout) {
		this.checkout = checkout;
	}


	public Date getCheckinDate() {
		return checkinDate;
	}

    public String getCheckinOnlyDate(){
    	if (checkinDate!=null) {
    		return (new SimpleDateFormat("dd/MM/yyyy HH:mm")).format(checkinDate);
    	}
    	return "";
    }

	public void setCheckinDate(Date checkinDate) {
		this.checkinDate = checkinDate;
	}


	public Date getCheckoutDate() {
		return checkoutDate;
	}


	public void setCheckoutDate(Date checkoutDate) {
		this.checkoutDate = checkoutDate;
	}


	public Date getEntranceDate() {
		return entranceDate;
	}


	public void setEntranceDate(Date entranceDate) {
		this.entranceDate = entranceDate;
	}


	public String getGuid() {
		return guid;
	}


	public void setGuid(String guid) {
		this.guid = guid;
	}

	public Media getMedia() {
		return media;
	}

	public void setMedia(Media media) {
		this.media = media;
	}

	public boolean isPreCheckin() {
		return preCheckin;
	}

	public void setPreCheckin(boolean preCheckin) {
		this.preCheckin = preCheckin;
	}

	public boolean isVip() {
		return vip;
	}

	public void setVip(boolean vip) {
		this.vip = vip;
	}

	public Liberatoria getLiberatoria() {
		if (fieldHandler != null) {
		   return (Liberatoria) fieldHandler.readObject(this, "liberatoria", liberatoria);
		}
		
		return liberatoria;
	}

	public void setLiberatoria(Liberatoria liberatoria) {
		if (fieldHandler != null) {
			   this.liberatoria = (Liberatoria) fieldHandler.writeObject(this, "liberatoria", this.liberatoria, liberatoria);
			   return;
		}
		
		this.liberatoria = liberatoria;
	}

	public Date getBadgeReturnDate() {
		return badgeReturnDate;
	}

	public void setBadgeReturnDate(Date badgeReturnDate) {
		this.badgeReturnDate = badgeReturnDate;
	}
	
	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public FieldHandler getFieldHandler() {
		return fieldHandler;
	}

	public void setFieldHandler(FieldHandler fieldHandler) {
		this.fieldHandler = fieldHandler;
	}

	public Date getSignDate() {
		return signDate;
	}

	public void setSignDate(Date signDate) {
		this.signDate = signDate;
	}
	
    public static SpectatorEpisode copy( SpectatorEpisode other ) {
    	SpectatorEpisode newSpectatorEpisode = new SpectatorEpisode ();
    	newSpectatorEpisode.id=other.id;
    	newSpectatorEpisode.badgeReturnDate=other.badgeReturnDate;
    	newSpectatorEpisode.checkin=other.checkin;
    	newSpectatorEpisode.checkinDate=other.checkoutDate;
    	newSpectatorEpisode.checkout=other.checkout;
    	newSpectatorEpisode.description=other.description;
    	newSpectatorEpisode.entranceDate=other.entranceDate;
    	newSpectatorEpisode.media=other.media;
    	newSpectatorEpisode.preCheckin=other.preCheckin;
    	newSpectatorEpisode.signDate=other.signDate;
    	newSpectatorEpisode.spectator=other.spectator;
    	newSpectatorEpisode.liberatoria=other.liberatoria;
    	newSpectatorEpisode.production=other.production;
    	newSpectatorEpisode.sign=other.sign;
    	newSpectatorEpisode.signDate=other.signDate;
    	newSpectatorEpisode.spectatorToReview=other.spectatorToReview;
    	newSpectatorEpisode.vip=other.vip;
//    	try {
//			newSpectatorEpisode=(SpectatorEpisode) other.clone();
//		} catch (CloneNotSupportedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
        return newSpectatorEpisode;
   }
    

	public boolean isSpectatorToReview() {
		return spectatorToReview;
	}

	public void setSpectatorToReview(boolean spectatorToReview) {
		this.spectatorToReview = spectatorToReview;
		this.getSpectator().setToReview(spectatorToReview);
	}
}
