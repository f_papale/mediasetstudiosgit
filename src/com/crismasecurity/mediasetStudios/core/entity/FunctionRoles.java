package com.crismasecurity.mediasetStudios.core.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * 
 * Class name: FunctionRoles
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author 	vincenzo.scaccia
 * @date 	04/jul/2018
 *
 */

@Entity
/*
@javax.persistence.Table(name="mediastudios_FunctionRoles")
*/
@Table(
		   name="mediastudios_functionRoles",
		   uniqueConstraints=
		       @UniqueConstraint(columnNames={"name"})
		)

public class FunctionRoles implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2933392681871934068L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(nullable = false)
	private Long id ; // Unique ID
	
	@Column(nullable = false)
	private String name;

	private String sortOrder;
	
	private String description;
	
	@Column(columnDefinition = "NVARCHAR(MAX)")
	private String notes;
	
	@Column(columnDefinition = "NVARCHAR(MAX)")
	private String redirectPath;
	
	@Column(columnDefinition = "NVARCHAR(MAX)",nullable = false)
	private String roles;

	/**
	 * Costruttore
	 */
	public FunctionRoles() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name.toLowerCase();
	}
	
	public String getRoles() {
		return roles;
	}

	public void setRoles(String roles) {
		this.roles = roles;
	}
	
	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getRedirectPath() {
		return redirectPath;
	}

	public void setRedirectPath(String redirectPath) {
		this.redirectPath = redirectPath;
	}
	

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FunctionRoles other = (FunctionRoles) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "FunctionRoles [id=" + id + ", name=" + name + ", sortOrder=" + sortOrder + ", description="
				+ description + ", notes=" + notes + ", redirectPath=" + redirectPath + ", roles=" + roles + "]";
	}


}
