package com.crismasecurity.mediasetStudios.core.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Type;

/**
 * 
 * Class name: Media (badge Tag BLE ecc...)
 *
 * Description: Entity relativa alla classe Media
 * 
 *
 * Company: 
 *
 * @author 	fabrizio.papale
 * @date 	06/aug/2018
 *
 */

@Entity

//@javax.persistence.Table(name="mediastudios_media")

@Table(
		   name="mediastudios_media",
		   uniqueConstraints=
		       @UniqueConstraint(columnNames={"idMediaType","code"})
		)


public class Media implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -186750018385385795L;


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(nullable = false)
	private Long id ; // Unique ID
	
	
	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn (name = "idMediaType")
	private MediaType mediaType ;

	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn (name = "idStatus")
	private MediaStatusType mediaStatusType ;
	/*
	 * libero, assegnato, disabilitato, bloccato, non riconsegnato
	 */
	
	@Basic
	private String code;  	//code

	@Basic
	private String label;  	//label
	
	@Basic
	private String barCode;  //BarCode
	
	@Column
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean active;	// Attivo

	@Basic
	private Date startDate;  //Datainizio Validità
		
	@Basic
	private Date endDate;  	//Datafine Validità

	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((label == null) ? 0 : label.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Media other = (Media) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (label == null) {
			if (other.label != null)
				return false;
		} else if (!label.equals(other.label))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public MediaType getMediaType() {
		return mediaType;
	}

	public void setMediaType(MediaType mediaType) {
		this.mediaType = mediaType;
	}

	public MediaStatusType getMediaStatusType() {
		return mediaStatusType;
	}

	public void setMediaStatusType(MediaStatusType mediaStatusType) {
		this.mediaStatusType = mediaStatusType;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Override
	public String toString() {
		return "Media [id=" + id + ", mediaType=" + mediaType + ", mediaStatusType=" + mediaStatusType + ", code="
				+ code + ", label=" + label + ", barCode=" + barCode + ", active=" + active + ", startDate=" + startDate
				+ ", endDate=" + endDate + "]";
	}


}
