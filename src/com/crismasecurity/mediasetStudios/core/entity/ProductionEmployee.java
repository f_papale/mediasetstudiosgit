package com.crismasecurity.mediasetStudios.core.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * 
 * Class name: ProductionEmployee
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author 	fabrizio
 * @date 	12/jul/2018
 *
 */

@Entity
//@javax.persistence.Table(name="mediastudios_productionEmployee")
@Table(
		   name="mediastudios_productionEmployee",
		   uniqueConstraints=
		       @UniqueConstraint(columnNames={"idProduction","idEmployee"})
		)

public class ProductionEmployee implements Serializable {

	private static final long serialVersionUID = 3680143575548539161L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(nullable = false)
	private Long id ; // Unique ID

	@ManyToOne (fetch = FetchType.LAZY)
	@JoinColumn (name = "idProduction")
	private Production production;
	
	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn (name = "idEmployee")
	private Employee employee;
	
	@Basic	
	private String role;

	/**
     * Costruttore
     */
    public ProductionEmployee() {
    	super();
    }
    
    public ProductionEmployee(Production pr, Employee em, String role ){
    	this.setProduction(pr);
    	this.setEmployee(em);
    	this.setRole(role);
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getEmployee() == null) ? 0 : getEmployee().hashCode());
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result + ((getProduction() == null) ? 0 : getProduction().hashCode());
		return result;
	}

	@Override
	public String toString() {
		return "ProductionEmployee [id=" + id + ", production=" + production + ", employee=" + employee + ", role="
				+ role + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductionEmployee other = (ProductionEmployee) obj;
		if (getId() == null) {
			if (other.getId() != null)
				return false;
		} else if (!getId().equals(other.getId()))
			return false;
		
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Production getProduction() {
		return production;
	}

	public void setProduction(Production production) {
		this.production = production;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
}
