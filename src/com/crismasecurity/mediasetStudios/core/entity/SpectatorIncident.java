package com.crismasecurity.mediasetStudios.core.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(
		   name="mediastudios_spectator_incident",
		   uniqueConstraints=
		       @UniqueConstraint(columnNames={"idSpectator","idIncident"})
		)
//@javax.persistence.Table(name="mediastudios_spectator_incident")
public class SpectatorIncident implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5573221492133543484L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(nullable = false)
	private Long id ; // Unique ID
	
	
	@ManyToOne (fetch = FetchType.LAZY)
	@JoinColumn (name = "idSpectator")
	private Spectator spectator;
	
	
	@ManyToOne (fetch = FetchType.LAZY)
	@JoinColumn (name = "idIncident")
	private Incident incident;
	
	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn (name = "idTypeBlackList")
	private BlackListType blackListType;

	
	

	public SpectatorIncident() {
		super();
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Spectator getSpectator() {
		return spectator;
	}


	public void setSpectator(Spectator spectator) {
		this.spectator = spectator;
	}


	public Incident getIncident() {
		return incident;
	}


	public void setIncident(Incident incident) {
		this.incident = incident;
	}


	public BlackListType getBlackListType() {
		return blackListType;
	}


	public void setBlackListType(BlackListType blackListType) {
		this.blackListType = blackListType;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SpectatorIncident other = (SpectatorIncident) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	
	
	
	
	
}
