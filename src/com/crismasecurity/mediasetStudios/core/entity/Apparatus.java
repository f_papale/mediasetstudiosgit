package com.crismasecurity.mediasetStudios.core.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * 
 * Class name: Apparato
 *
 * Description: Entity relativa alla classe Apparato
 * 
 *
 * Company: 
 *
 * @author 	vincenzo.scaccia
 * @date 	04/jul/2018
 *
 */

@Entity
/*
@javax.persistence.Table(name="mediastudios_apparatus")
*/
@Table(
		   name="mediastudios_apparatus",
		   uniqueConstraints=
		       @UniqueConstraint(columnNames={"description"})
		)

public class Apparatus implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8333486335318992536L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(nullable = false)
	private Long id ; // Unique ID
	
	
	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn (name = "idTypeApparatus")
	private ApparatusType apparatusType ;
	
	
	
	
//	@OneToMany(
//	        mappedBy = "gap",
//	        cascade = CascadeType.ALL,
//	        orphanRemoval = true
//	 )
//	 private List<GapApparatus> apparatus = new ArrayList<>();
	
	
	@Basic
	private String description;  //descrizione
	
	@Basic
	private String features; //caratteristiche

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Apparatus other = (Apparatus) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ApparatusType getApparatusType() {
		return apparatusType;
	}

	public void setApparatusType(ApparatusType apparatusType) {
		this.apparatusType = apparatusType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFeatures() {
		return features;
	}

	public void setFeatures(String features) {
		this.features = features;
	}

	@Override
	public String toString() {
		return "Apparatus [id=" + id + ", apparatusType=" + apparatusType + ", description=" + description
				+ ", features=" + features + "]";
	}

	
	
}
