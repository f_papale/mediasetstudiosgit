package com.crismasecurity.mediasetStudios.core.entity;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;

import com.crismasecurity.mediasetStudios.core.security.entities.AppUser;


/**
 * 
 * Class name: AgencyUser
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author 	gnocco
 * @date 	20/july/2018
 *
 */
@Entity

@javax.persistence.Table(name="mediastudios_agencyUser")

//@Table(
//		   name="mediastudios_agencyUser",
//		   uniqueConstraints=
//		       @UniqueConstraint(columnNames={"user_CODI_USER","flgCurrentSelected"})
//		)

public class AgencyUser implements Serializable {

	private static final long serialVersionUID = 3953275487577691608L;
	
	@EmbeddedId
	private AgencyUserId id;

	@ManyToOne (fetch = FetchType.EAGER)
	@MapsId("idAgency")
	private Agency agency;
	
	//Okkio che se non inserisco il cascade non crea o cancella lo User in cascata
	@ManyToOne (fetch = FetchType.EAGER)
	@MapsId("idUser")
	private AppUser user;
	
	@Transient
	private String idString; 
	
	@Basic	
	private String description;  // Descrizione;
	
	@Column(nullable = false)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean flgCurrentSelected=false;

    public AgencyUser() {
    	super();
    }
    
    public AgencyUser(Agency agency, AppUser user) {
    	super();
    	this.setAgency(agency);
    	this.setUser(user);
    	this.setId(new AgencyUserId(agency.getIdAgency(), user.getId()));
    }
    
    public AgencyUser(Agency agency, AppUser user, String description) {
    	super();
    	this.setAgency(agency);
    	this.setUser(user);
    	this.setId(new AgencyUserId(agency.getIdAgency(), user.getId()));
    	this.setDescription(description);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
 
        if (o == null || getClass() != o.getClass())
            return false;
 
        AgencyUser that = (AgencyUser) o;
        return Objects.equals(agency, that.agency) &&
               Objects.equals(user, that.user);
    }
 
    @Override 
    public int hashCode() {
        return Objects.hash(user);
    }



	@Override
	public String toString() {
		return "AgencyUser [id=" + id + ", agency=" + agency + ", user=" + user + ", idString=" + idString
				+ ", description=" + description + ", flgCurrentSelected=" + flgCurrentSelected + "]";
	}

	public AgencyUserId getId() {
		return id;
	}

	public void setId(AgencyUserId id) {
		this.id = id;
	}

	public Agency getAgency() {
		return agency;
	}

	public void setAgency(Agency agency) {
		this.agency = agency;
	}

	public AppUser getUser() {
		return user;
	}

	public void setUser(AppUser user) {
		this.user = user;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getFlgCurrentSelected() {
		return flgCurrentSelected;
	}

	public void setFlgCurrentSelected(boolean flgCurrentSelected) {
		this.flgCurrentSelected = flgCurrentSelected;
	}

	public String getIdString() {
		return agency.getIdAgency().toString()+"-"+user.getId().toString();
	}

//	public void setIdString(String idString) {
//		this.idString = idString;
//	}

}
