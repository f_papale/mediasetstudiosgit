package com.crismasecurity.mediasetStudios.core.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.crismasecurity.mediasetStudios.core.utils.crypto.JpaCryptoConverter;


/**
 * 
 * Class name: Liberatoria
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author 	
 * @date 	25/may/2018
 *
 */
@Entity
//@javax.persistence.Table(name="mediastudios_agencyType")
@Table(name="mediastudios_liberatorie")
/**/		
public class Liberatoria implements Serializable {
	

	private static final long serialVersionUID = -3442779081356105547L;
	
	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
//	@Column(nullable = false)
	private Long id ; // Unique ID

	@OneToOne(
			fetch = FetchType.LAZY
//			, cascade = CascadeType.ALL
			)
    @MapsId
    private SpectatorEpisode spectatorEpisode;
	
	@Column(columnDefinition = "NVARCHAR(MAX)")
	@Convert(converter = JpaCryptoConverter.class)
	private String pdfSignBase64Str;		// liberatoria
	
	/**
     * Costruttore
     */
    public Liberatoria() {
    	super();
    }
    


	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Liberatoria other = (Liberatoria) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}



	public String getPdfSignBase64Str() {
		return pdfSignBase64Str;
	}



	public void setPdfSignBase64Str(String pdfSignBase64Str) {
		this.pdfSignBase64Str = pdfSignBase64Str;
	}



	public SpectatorEpisode getSpectatorEpisode() {
		return spectatorEpisode;
	}



	public void setSpectatorEpisode(SpectatorEpisode spectatorEpisode) {
		this.spectatorEpisode = spectatorEpisode;
	}
	


}
