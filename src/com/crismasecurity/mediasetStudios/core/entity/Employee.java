package com.crismasecurity.mediasetStudios.core.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Type;

import com.crismasecurity.mediasetStudios.core.utils.TextUtil;


/**
 * 
 * Class name: Employee
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author 	fpapale
 * @date 	25/may/2018
 *
 */
@Entity
//@javax.persistence.Table(name="mediastudios_employee")

@Table(
		   name="mediastudios_employee",
		   uniqueConstraints=
		       @UniqueConstraint(columnNames={"name","surname"})
		)

public class Employee implements Serializable {
	
	private static final long serialVersionUID = -4288355476160388937L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(nullable = false)
	private Long id ; // Unique ID
	
//	@Column
//	@Type(type = "org.hibernate.type.NumericBooleanType")
//	private boolean statusOk;
	
	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn (name = "idEmployeeType")
	private EmployeeType employeeType ;

    @OneToMany(
            mappedBy = "employee",
            cascade = CascadeType.ALL,
            orphanRemoval = true
        )
    private List<ProductionEmployee> productionEmployee = new ArrayList<>();
    	
	@Basic	
	private String surname; 			// Cognome;	
	@Basic	
	private String name; 				// Nome;
	@Basic
	private String telephone;			// Telefono
	@Basic
	private String mobile;				// Cellulare
	@Column
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean internalExternal;	// InternoEsterno
	@Basic
	private String externalCompany ;	// SocietaEsterna
	@Basic
	private String email ;				// email
//	@Basic
//	private Date date;
	
	
//	@ManyToOne (fetch=FetchType.EAGER)
//	private Company masterCompany;
//	
//	@ManyToMany(fetch=FetchType.LAZY) // no cascade
//	@JoinTable(name = "mediasetstudios_contracts_master_company_employees",
//	  joinColumns = @JoinColumn(name = "contract_id"),
//	  inverseJoinColumns = @JoinColumn(name = "master_company_employee_id"),
//	  uniqueConstraints=@UniqueConstraint(columnNames={"contract_id","master_company_employee_id"})
//	)
//	private List<CompanyEmployee> masterCompanyEmployees;
//	
//	@OneToMany(cascade=CascadeType.ALL, orphanRemoval=true, fetch=FetchType.LAZY)
//	private List<Allegato5Data> listaAllegati5;
	
	
//	@OneToMany(cascade=CascadeType.ALL, orphanRemoval=true, mappedBy="contract", fetch=FetchType.LAZY)
//	private List<SubcontractData> subcontractCompanies;
	
	
	/**
     * Costruttore
     */
    public Employee() {
    	super();
    }
    
  
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}



	@Override
	public String toString() {
		return "Denominazione [id = " + id + ", name = " + getName() + "]";
	}


	public String getSurname() {
		return surname;
	}


	public void setSurname(String surname) {
		this.surname = surname;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getTelephone() {
		return telephone;
	}


	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}


	public String getMobile() {
		return mobile;
	}


	public void setMobile(String mobile) {
		this.mobile = mobile;
	}


	public boolean isInternalExternal() {
		return internalExternal;
	}


	public void setInternalExternal(boolean internalExternal) {
		this.internalExternal = internalExternal;
	}


	public String getExternalCompany() {
		return externalCompany;
	}


	public void setExternalCompany(String externalCompany) {
		this.externalCompany = externalCompany;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
//		this.email = email;
		this.email = TextUtil.rearrangeEmailList(email);
	}


	public EmployeeType getEmployeeType() {
		return employeeType;
	}


	public void setEmployeeType(EmployeeType employeeType) {
		this.employeeType = employeeType;
	}


	public List<ProductionEmployee> getProductionEmployee() {
		return productionEmployee;
	}


	public void setProductionEmployee(List<ProductionEmployee> productionEmployee) {
		this.productionEmployee = productionEmployee;
	}


}
