package com.crismasecurity.mediasetStudios.core.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * 
 * Class name: TipoSpettatore
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author 	fdongmo
 * @date 	30/may/2018
 *
 */


@Entity
//@javax.persistence.Table(name="mediastudios_spectatorType")
@Table(
		   name="mediastudios_spectatorType",
		   uniqueConstraints=
		       @UniqueConstraint(columnNames={"description"})
		)
public class SpectatorType implements Serializable{


	private static final long serialVersionUID = -7183627279557848263L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(nullable = false)
	private Long id ; // Unique ID
	
	@Basic
	private String description;
	
	@Basic
	private int idSpectatorType;

	@Override
	public String toString() {
		return "SpectatorType [id=" + id + ", description=" + description + ", idSpectatorType=" + idSpectatorType
				+ "]";
	}
	
	/**
     * Costruttore
     */
    public SpectatorType() {
    	super();
    }

    
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	} 
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}



	public int getIdSpectatorType() {
		return idSpectatorType;
	}

	public void setIdSpectatorType(int idSpectatorType) {
		this.idSpectatorType = idSpectatorType;
	}
}
