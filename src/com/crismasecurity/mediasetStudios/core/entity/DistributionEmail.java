package com.crismasecurity.mediasetStudios.core.entity;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Table;
import javax.persistence.CascadeType;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;


/**
 * 
 * Class name: DistributionEmail
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author 	fpapale
 * @date 	18/mar/2020
 *
 */
@Entity

@Table(
		name="mediastudios_distributionEmail"
		)
/*

@Table(
		   name="mediastudios_DistributionEmail",
		   uniqueConstraints=
		       @UniqueConstraint(columnNames={"idDistributionList","idEmailContact"})
		)
*/

public class DistributionEmail implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9038714310578529160L;

	@EmbeddedId
	private DistributionEmailId id;

//	@ManyToOne (fetch = FetchType.EAGER)
	@ManyToOne (fetch = FetchType.LAZY)
	@MapsId("idDistributionList")
	private DistributionList distributionList;
	
	//Okkio che se non inserisco il cascade non crea o cancella lo EmailContact in cascata
//	@ManyToOne (fetch = FetchType.EAGER, cascade=CascadeType.ALL)
//	@ManyToOne (fetch = FetchType.EAGER)
	@ManyToOne (fetch = FetchType.LAZY)
	@MapsId("idEmailContact")
	private EmailContact emailContact;

    public DistributionEmail() {
    	super();
    }
    
    public DistributionEmail(DistributionList distributionList, EmailContact emailContact) {
    	super();
    	this.setDistributionList(distributionList);
    	this.setEmailContact(emailContact);
    	this.setId(new DistributionEmailId(distributionList.getId(), emailContact.getId()));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
 
        if (o == null || getClass() != o.getClass())
            return false;
 
        DistributionEmail that = (DistributionEmail) o;
        return Objects.equals(distributionList, that.distributionList) &&
               Objects.equals(emailContact, that.emailContact);
    }
 
    @Override
    public int hashCode() {
        return Objects.hash(emailContact);
    }

	@Override
	public String toString() {
		return "Descrizione [idDistributionList = " + getDistributionList().getId() + ", idEmailContact = "+ getEmailContact().getId()  + "]";
	}

	public DistributionEmailId getId() {
		return id;
	}

	public void setId(DistributionEmailId id) {
		this.id = id;
	}

	public DistributionList getDistributionList() {
		return distributionList;
	}

	public void setDistributionList(DistributionList distributionList) {
		this.distributionList = distributionList;
	}

	public EmailContact getEmailContact() {
		return emailContact;
	}

	public void setEmailContact(EmailContact emailContact) {
		this.emailContact = emailContact;
	}
}
