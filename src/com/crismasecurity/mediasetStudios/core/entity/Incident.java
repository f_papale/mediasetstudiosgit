
package com.crismasecurity.mediasetStudios.core.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Type;

@Entity
@javax.persistence.Table(name="mediastudios_incident")
/*
@Table(
		   name="mediastudios_agencyType",
		   uniqueConstraints=
		       @UniqueConstraint(columnNames={"description"})
		)
*/
public class Incident {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idIncident", nullable = false)
	private Long idIncident ; // Unique ID 

	
	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn (name = "idIncidentType")
	private IncidentType incidentType ;
	
	
	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn (name = "idSeverityLevel")
	//private GravityLevel gravityLevel;
	private SeverityLevel severityLevel;
	
	
		
//	@OneToMany(
//			cascade=CascadeType.ALL, 
//			orphanRemoval=true, 
//			fetch=FetchType.LAZY)
//	private List<SpectatorEpisode> spectators = new ArrayList<SpectatorEpisode>();
	
	
	/*@ManyToOne (fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumns({
		@JoinColumn(name = "idProduction", referencedColumnName="idProduction"),
		@JoinColumn(name = "idEpisode", referencedColumnName="idEpisode"),
		@JoinColumn(name = "idSpectator", referencedColumnName="idSpectator")
	})
	private SpectatorEpisode spectatorEpisode;*/
	
	
	@Column(columnDefinition = "VARCHAR(MAX)")
//	@Convert(converter = JpaCryptoConverter.class)
	private String description;

	
	@Column(columnDefinition = "VARCHAR(MAX)")
//	@Convert(converter = JpaCryptoConverter.class)
	private String longDescription;
	
	
	@Column
	@Type(type = "org.hibernate.type.DateType")
	private Date dateTimeIncident;		// Data Ora Incident 
	
//	private Production production;
//	
//	private Episode episode;
//	
//	private Spectator spectators;
	
//	@OneToMany(
//			cascade=CascadeType.ALL, 
//			orphanRemoval=true, 
//			fetch=FetchType.EAGER, mappedBy="incident")
//	private List<BlackListSpectator> blackListSpectators;
	
	
	
	@OneToMany(
            mappedBy = "incident",
            cascade = CascadeType.ALL,
            orphanRemoval = true
        )
    private List<SpectatorIncident> spectatorIncident = new ArrayList<>();
	
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getIdIncident() {
		return idIncident;
	}

	public void setIdIncident(Long idIncident) {
		this.idIncident = idIncident;
	}

	public Date getDateTimeIncident() {
		return dateTimeIncident;
	}

	public void setDateTimeIncident(Date dateTimeIncident) {
		this.dateTimeIncident = dateTimeIncident;
	}

	public IncidentType getIncidentType() {
		return incidentType;
	}

	public void setIncidentType(IncidentType incidentType) {
		this.incidentType = incidentType;
	}

	

	public SeverityLevel getSeverityLevel() {
		return severityLevel;
	}

	public void setSeverityLevel(SeverityLevel severityLevel) {
		this.severityLevel = severityLevel;
	}

	
//
//	public List<BlackListSpectator> getBlackListSpectators() {
//		return blackListSpectators;
//	}
//
//	public void setBlackListSpectators(List<BlackListSpectator> blackListSpectators) {
//		this.blackListSpectators = blackListSpectators;
//	}

	public List<SpectatorIncident> getSpectatorIncident() {
		return spectatorIncident;
	}

	public void setSpectatorIncident(List<SpectatorIncident> spectatorIncident) {
		this.spectatorIncident = spectatorIncident;
	}

	public String getLongDescription() {
		return longDescription;
	}

	public void setLongDescription(String longDescription) {
		this.longDescription = longDescription;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idIncident == null) ? 0 : idIncident.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Incident other = (Incident) obj;
		if (idIncident == null) {
			if (other.idIncident != null)
				return false;
		} else if (!idIncident.equals(other.idIncident))
			return false;
		return true;
	}
}
