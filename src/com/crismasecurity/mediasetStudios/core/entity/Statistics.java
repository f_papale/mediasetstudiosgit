package com.crismasecurity.mediasetStudios.core.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.UpdateTimestamp;

import com.google.gson.annotations.Expose;

/**
 * 
 * Class name: Statistics
 *
 * Description: Entity relativa alla classe Statistics
 * 
 *
 * Company: 
 *
 * @author 	
 * @date 	
 *
 */

@Entity
/*
@javax.persistence.Table(name="mediastudios_statistics")
*/
@Table(
   name="mediastudios_statistics",
	uniqueConstraints={
			@UniqueConstraint(
					columnNames={"domain", "measure", "value"}
			)
	}
)


public class Statistics implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8333486335318992536L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(nullable = false)
	private Long id ; // Unique ID

	@Basic
	private String domain;  //Dominio
	
	@Basic
	@Expose
	private String measure; //Misura
	
	@Basic
	@Expose
	private String value; //Valore
	
	@Expose
	@UpdateTimestamp // Timestamp di aggiornamento
	@Column(name = "updatedOn")
	private Date updatedOn;


	public Statistics() {
		super();
	}
	
	public Statistics(String domain, String measure, String value) {
		setDomain(domain);
		setMeasure(measure);
		setValue(value);
	}
	
	public void updateStatistics(String domain, String measure, String value) {
		setDomain(domain);
		setMeasure(measure);
		setValue(value);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Statistics other = (Statistics) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "Statistics [id=" + id + ", domain=" + domain + ", measure=" + measure + ", value=" + value
				+ ", updatedOn=" + updatedOn + "]";
	}

	public Long getId() {
		return id;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getMeasure() {
		return measure;
	}

	public void setMeasure(String measure) {
		this.measure = measure;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}	
		
}
