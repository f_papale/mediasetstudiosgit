package com.crismasecurity.mediasetStudios.core.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * 
 * Class name: ProductionEmployee
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author 	Vincenzo Scaccia
 * @date 	12/jul/2018
 *
 */

@Entity
@javax.persistence.Table(name="mediastudios_locationApparatus")
/*
@Table(
		   name="mediastudios_agencyType",
		   uniqueConstraints=
		       @UniqueConstraint(columnNames={"description"})
		)
*/
public class LocationApparatus implements Serializable {

	private static final long serialVersionUID = 3680143575548539161L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(nullable = false)
	private Long id ; // Unique ID

	@ManyToOne (fetch = FetchType.LAZY)
	@JoinColumn (name = "idLocation")
	private Location location;
	
	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn (name = "idApparatus")
	private Apparatus apparatus;
	
	@Basic	
	private String description;
	
	
	/**
     * Costruttore
     */
    public LocationApparatus() {
    	super();
    }
    
    public LocationApparatus(Location location, Apparatus app, String description){
    	this.setLocation(location);
    	this.setApparatus(app);
    	this.description = description;
    }

	
	@Override
	public String toString() {
		return "LocationApparatus [id=" + id + ", location=" + location + ", apparatus=" + apparatus + ", description="
				+ description + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((apparatus == null) ? 0 : apparatus.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((location == null) ? 0 : location.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LocationApparatus other = (LocationApparatus) obj;
		if (apparatus == null) {
			if (other.apparatus != null)
				return false;
		} else if (!apparatus.equals(other.apparatus))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (location == null) {
			if (other.location != null)
				return false;
		} else if (!location.equals(other.location))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Apparatus getApparatus() {
		return apparatus;
	}

	public void setApparatus(Apparatus apparatus) {
		this.apparatus = apparatus;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
	
}
