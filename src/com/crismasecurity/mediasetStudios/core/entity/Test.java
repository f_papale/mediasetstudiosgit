package com.crismasecurity.mediasetStudios.core.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 * 
 * Class name: Test
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author 
 * @date 20/octo/2017
 *
 */
@Entity
@javax.persistence.Table(name="mediastudios_tests")
public class Test implements Serializable {

	private static final long serialVersionUID = -6521685927162965228L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(nullable = false)
	private Long id ; // Unique ID
	
//	@Column
//	@Type(type = "org.hibernate.type.NumericBooleanType")
//	private boolean statusOk; 	
	@Basic
	private String test; 

	@Basic
	private Date date;
	
	
//	@ManyToOne (fetch=FetchType.EAGER)
//	private Company masterCompany;
//	
//	@ManyToMany(fetch=FetchType.LAZY) // no cascade
//	@JoinTable(name = "mediasetstudios_contracts_master_company_employees",
//	  joinColumns = @JoinColumn(name = "contract_id"),
//	  inverseJoinColumns = @JoinColumn(name = "master_company_employee_id"),
//	  uniqueConstraints=@UniqueConstraint(columnNames={"contract_id","master_company_employee_id"})
//	)
//	private List<CompanyEmployee> masterCompanyEmployees;
//	
//	@OneToMany(cascade=CascadeType.ALL, orphanRemoval=true, fetch=FetchType.LAZY)
//	private List<Allegato5Data> listaAllegati5;
	
	
//	@OneToMany(cascade=CascadeType.ALL, orphanRemoval=true, mappedBy="contract", fetch=FetchType.LAZY)
//	private List<SubcontractData> subcontractCompanies;
	
	
	
	/**
     * Costruttore
     */
    public Test() {
    	super();
    }
    
  
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Test other = (Test) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}


	public String getTest() {
		return test;
	}

	public void setTest(String test) {
		this.test = test;
	}


	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	@Override
	public String toString() {
		return "Test [id=" + id + ", test=" + test + ", date=" + date + "]";
	}

}
