package com.crismasecurity.mediasetStudios.core.entity;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Table;
import javax.persistence.CascadeType;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.UniqueConstraint;


/**
 * 
 * Class name: AgencySpectator
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author 	fpapale
 * @date 	25/may/2018
 *
 */
@Entity

@Table(
		name="mediastudios_agencySpectator"
		)
/*

@Table(
		   name="mediastudios_agencySpectator",
		   uniqueConstraints=
		       @UniqueConstraint(columnNames={"idAgency","idSpectator"})
		)
*/

public class AgencySpectator implements Serializable {

	private static final long serialVersionUID = 3953275487577691608L;
	
	@EmbeddedId
	private AgencySpectatorId id;

//	@ManyToOne (fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	@ManyToOne (fetch = FetchType.LAZY)
	@MapsId("idAgency")
	private Agency agency;
	
	//Okkio che se non inserisco il cascade non crea o cancella lo Spectator in cascata
//	@ManyToOne (fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	@ManyToOne (fetch = FetchType.LAZY)
	@MapsId("idSpectator")
	private Spectator spectator;
	
	@Basic	
	private String description;  // Descrizione;

    public AgencySpectator() {
    	super();
    }
    
    public AgencySpectator(Agency agency, Spectator spectator) {
    	super();
    	this.setAgency(agency);
    	this.setSpectator(spectator);
    	this.setId(new AgencySpectatorId(agency.getIdAgency(), spectator.getId()));
    }
    
    public AgencySpectator(Agency agency, Spectator spectator, String description) {
    	super();
    	this.setAgency(agency);
    	this.setSpectator(spectator);
    	this.setId(new AgencySpectatorId(agency.getIdAgency(), spectator.getId()));
    	this.setDescription(description);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
 
        if (o == null || getClass() != o.getClass())
            return false;
 
        AgencySpectator that = (AgencySpectator) o;
        return Objects.equals(agency, that.agency) &&
               Objects.equals(spectator, that.spectator);
    }
 
    @Override
    public int hashCode() {
        return Objects.hash(spectator);
    }

	@Override
	public String toString() {
		return "Descrizione [idAgency = " + getAgency().getIdAgency() + ", idSpectator = "+ getSpectator().getId() + "  description = " + getDescription() + "]";
	}

	public AgencySpectatorId getId() {
		return id;
	}

	public void setId(AgencySpectatorId id) {
		this.id = id;
	}

	public Agency getAgency() {
		return agency;
	}

	public void setAgency(Agency agency) {
		this.agency = agency;
	}

	public Spectator getSpectator() {
		return spectator;
	}

	public void setSpectator(Spectator spectator) {
		this.spectator = spectator;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
