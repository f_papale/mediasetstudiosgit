package com.crismasecurity.mediasetStudios.core.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@javax.persistence.Table(name="mediastudios_badges_transits")
public class BadgesTransits implements Serializable {

	private static final long serialVersionUID = -1797512639836047210L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(nullable = false)
	private Long id ; // Unique ID
	
	@Basic
	public String readerAntenna;
	@Basic
	public String antennaTimeStamp;
	@Basic
	public Date timeStamp;
	@Basic
	public String readerIpAddress;
	@Basic
	public String transitType;
	@Basic
	public String mediaId;
	
	@Basic
	public String error;
	

	@Basic
	private String mediaLabel; //Media
	
	@Basic	
	private String productionName; // Production;	
	
	@Basic	
	private String spectator; // spectator
	
	@Basic	
	private String episode; // Episode --> dateFrom

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BadgesTransits other = (BadgesTransits) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "BadgesTransits [id=" + id + ", readerAntenna=" + readerAntenna + ", antennaTimeStamp="
				+ antennaTimeStamp + ", timeStamp=" + timeStamp + ", readerIpAddress=" + readerIpAddress
				+ ", transitType=" + transitType + ", mediaId=" + mediaId + ", error=" + error + ", mediaLabel="
				+ mediaLabel + ", productionName=" + productionName + ", spectator=" + spectator + ", episode="
				+ episode + "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getReaderAntenna() {
		return readerAntenna;
	}

	public void setReaderAntenna(String readerAntenna) {
		this.readerAntenna = readerAntenna;
	}

	public String getAntennaTimeStamp() {
		return antennaTimeStamp;
	}

	public void setAntennaTimeStamp(String antennaTimeStamp) {
		this.antennaTimeStamp = antennaTimeStamp;
	}

	public Date getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getReaderIpAddress() {
		return readerIpAddress;
	}

	public void setReaderIpAddress(String readerIpAddress) {
		this.readerIpAddress = readerIpAddress;
	}

	public String getTransitType() {
		return transitType;
	}

	public void setTransitType(String transitType) {
		this.transitType = transitType;
	}

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getMediaLabel() {
		return mediaLabel;
	}

	public void setMediaLabel(String mediaLabel) {
		this.mediaLabel = mediaLabel;
	}

	public String getProductionName() {
		return productionName;
	}

	public void setProductionName(String productionName) {
		this.productionName = productionName;
	}

	public String getSpectator() {
		return spectator;
	}

	public void setSpectator(String spectator) {
		this.spectator = spectator;
	}

	public String getEpisode() {
		return episode;
	}

	public void setEpisode(String episode) {
		this.episode = episode;
	}
	
}
