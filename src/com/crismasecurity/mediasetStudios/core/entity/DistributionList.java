package com.crismasecurity.mediasetStudios.core.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * 
 * Class name: Apparato
 *
 * Description: Entity relativa alla classe Apparato
 * 
 *
 * Company: 
 *
 * @author 	vincenzo.scaccia
 * @date 	04/jul/2018
 *
 */

@Entity

//@javax.persistence.Table(name="mediastudios_distributionList")


@javax.persistence.Table(
		   name="mediastudios_distributionList",
		   uniqueConstraints=
				   @javax.persistence.UniqueConstraint(columnNames={"name"})
		)

public class DistributionList implements Serializable{	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6844147576914717170L;


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(nullable = false)
	private Long id ; // Unique ID
	
	@Basic
	private String name;  //nome
	@Basic

	private String description;  //descrizione	
	

    @OneToMany(
    		mappedBy = "distributionList",
    		cascade = CascadeType.ALL,
    		orphanRemoval = true
    )
    private List<DistributionEmail> distributionEmails;
  
	

    
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof DistributionList))
			return false;
		DistributionList other = (DistributionList) obj;
		if (this.getId() == null) {
			if (other.getId() != null)
				return false;
		} else if (!this.getId().equals(other.getId()))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DistributionList [" + (id != null ? "id=" + id + ", " : "")
				+ (name != null ? "name=" + name + ", " : "")
				+ (description != null ? "description=" + description : "") + "]";
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public List<DistributionEmail> getDistributionEmails() {
		return distributionEmails;
	}
	public void setDistributionEmails(List<DistributionEmail> distributionEmails) {
		this.distributionEmails = distributionEmails;
	}
}
