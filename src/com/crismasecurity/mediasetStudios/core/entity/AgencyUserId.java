package com.crismasecurity.mediasetStudios.core.entity;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Embeddable;
import javax.persistence.Column;

@Embeddable
public class AgencyUserId 
	implements Serializable{

	private static final long serialVersionUID = 1839901364533125710L;

	@Column(name= "idAgency")
	private Long idAgency;
	
	@Column(name= "idUser")
	private Long idUser;

	@SuppressWarnings("unused")
	private AgencyUserId() {
	}

	public AgencyUserId(Long idAgency, Long idUser) {
		this.setIdAgency(idAgency);
		this.setIdUser(idUser);
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
 
        if (o == null || getClass() != o.getClass()) 
            return false;
 
        AgencyUserId that = (AgencyUserId) o;
        return Objects.equals(idAgency, that.idAgency) && 
               Objects.equals(idUser, that.idUser);
    }
 
    @Override
    public int hashCode() {
        return Objects.hash(idAgency, idUser);
    }

	public Long getIdAgency() {
		return idAgency;
	}

	public void setIdAgency(Long idAgency) {
		this.idAgency = idAgency;
	}

	public Long getIdUser() {
		return idUser;
	}

	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}
}
