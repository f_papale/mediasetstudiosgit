package com.crismasecurity.mediasetStudios.core.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.crismasecurity.mediasetStudios.core.utils.TextUtil;

/**
 * 
 * Class name: Queue Email
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author 	fpapale
 * @date 	21/spt/2018
 *
 */

@Entity
@javax.persistence.Table(name="mediastudios_emailQueue")
public class EmailQueue implements Serializable {

	private static final long serialVersionUID = -3189361094553514585L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(nullable = false)
	private Long id ; // Unique ID
	
	@Basic	
	private int entityType; 

	@Basic	
	private Long entityID; 
	
//	@Basic	
	@Column(columnDefinition = "NVARCHAR(MAX)")
	private String fromEmail; 

	@Column(columnDefinition = "NVARCHAR(MAX)")	
	private String toEmail;

	@Column(columnDefinition = "NVARCHAR(MAX)")	
	private String ccEmail;
	
	@Column(columnDefinition = "NVARCHAR(MAX)")
	private String subject;
	
	@Column(columnDefinition = "NVARCHAR(MAX)")
	private String body;
	
	@Basic	
	private String attachFileName;

	@Column(columnDefinition = "NVARCHAR(MAX)")
	private String pdfRepBase64Str;		// doc
	
	@Basic	
	private int priority;  

	
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	//@Type(type = "org.hibernate.type.DateType")
	private Date sendDate;
	
	@Basic	
	private int sendStatus;
	

	/**
     * Costruttore
     */
    public EmailQueue() {
    	super();
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmailQueue other = (EmailQueue) obj;
		if (getId() == null) {
			if (other.getId() != null)
				return false;
		} else if (!getId().equals(other.getId()))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "EmailQueue [id=" + id + ", entityType=" + entityType + ", entityID=" + entityID + ", fromEmail="
				+ fromEmail + ", toEmail=" + toEmail + ", ccEmail=" + ccEmail + ", subject=" + subject + ", body="
				+ body + ", attachFileName=" + attachFileName + ", priority=" + priority + ", sendDate=" + sendDate
				+ ", sendStatus=" + sendStatus + "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFromEmail() {
		return fromEmail;
	}

	public void setFromEmail(String fromEmail) {
//		this.fromEmail = TextUtil.rearrangeEmailList(fromEmail);
		this.fromEmail = fromEmail;
	}

	public String getToEmail() {
		return toEmail;
	}

	public void setToEmail(String toEmail) {
//		this.toEmail = TextUtil.rearrangeEmailList(toEmail);
		this.toEmail = toEmail;
	}

	public String getCcEmail() {
		return ccEmail;
	}

	public void setCcEmail(String ccEmail) {
//		this.ccEmail = TextUtil.rearrangeEmailList(ccEmail);
		this.ccEmail = ccEmail;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getPdfRepBase64Str() {
		return pdfRepBase64Str;
	}

	public void setPdfRepBase64Str(String pdfRepBase64Str) {
		this.pdfRepBase64Str = pdfRepBase64Str;
	}

	public Date getSendDate() {
		return sendDate;
	}

	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}

	public int getSendStatus() {
		return sendStatus;
	}

	public void setSendStatus(int sendStatus) {
		this.sendStatus = sendStatus;
	}
	
	public int getEntityType() {
		return entityType;
	}

	public void setEntityType(int entityType) {
		this.entityType = entityType;
	}

	public Long getEntityID() {
		return entityID;
	}

	public void setEntityID(Long entityID) {
		this.entityID = entityID;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getAttachFileName() {
		return attachFileName;
	}

	public void setAttachFileName(String attachFileName) {
		this.attachFileName = attachFileName;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}
}
