package com.crismasecurity.mediasetStudios.core.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
//@javax.persistence.Table(name="mediastudios_location")

@Table(
		   name="mediastudios_location",
		   uniqueConstraints=
		       @UniqueConstraint(columnNames={"name","idTypeLocation"})
		)

public class Location implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7501584618158767637L;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(nullable = false)
	private Long id ; // Unique ID
	
	
	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn (name = "idTypeLocation")
	private LocationType locationType ;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="idProductionCenter")
	private ProductionCenter productionCenter;
	
	@Basic	
	private String name;  // Descrizione;
	
	@Basic	
	private String description;  // Descrizione;
	

	//FIXME - Da rivedere con Fabrizio e Giovanni come memorizzare le coordinate geografiche
//	@Basic
//	private String latitude;
//	
//	@Basic
//	private String longitude;
	
	
	/*@OneToMany (fetch = FetchType.LAZY)
	@JoinColumn (name = "idProductionCenter")
	private List<ProductionCenter> listProductionCenter;*/
	
	//@ManyToOne (fetch = FetchType.LAZY)
	//@JoinColumn (name = "idProductionCenter")
	//private ProductionCenter productionCenter;
	
	
	@OneToMany(
	        mappedBy = "apparatus",
	        cascade = CascadeType.ALL,
	        orphanRemoval = true
	    )
	
	private List<LocationApparatus> locationApparatus = new ArrayList<>();
		
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

		public LocationType getLocationType() {
		return locationType;
	}

	public void setLocationType(LocationType locationType) {
		this.locationType = locationType;
	}


	public ProductionCenter getProductionCenter() {
		return productionCenter;
	}


	public void setProductionCenter(ProductionCenter productionCenter) {
		this.productionCenter = productionCenter;
	}


	public List<LocationApparatus> getLocationApparatus() {
		return locationApparatus;
	}


	public void setLocationApparatus(List<LocationApparatus> locationApparatus) {
		this.locationApparatus = locationApparatus;
	}

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}
}
