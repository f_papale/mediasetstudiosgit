package com.crismasecurity.mediasetStudios.core.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.crismasecurity.mediasetStudios.core.utils.TextUtil;


/**
 * 
 * Class name: ProductionCenter
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author 	fpapale
 * @date 	25/may/2018
 *
 */
@Entity
//@javax.persistence.Table(name="mediastudios_productionCenter")

@Table(
		   name="mediastudios_productionCenter",
		   uniqueConstraints=
		       @UniqueConstraint(columnNames={"name"})
		)

public class ProductionCenter implements Serializable {

	private static final long serialVersionUID = -75383178416223209L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(nullable = false)
	private Long id ; // Unique ID
	
//	@Column
//	@Type(type = "org.hibernate.type.NumericBooleanType")
//	private boolean statusOk; 	
	@Basic
	private String name; 
	
	@Basic
	private String description; 

//	@ManyToOne (fetch=FetchType.EAGER)
//	@JoinColumn (name = "idMunicipality")	
//	private Municipality municipality;	
	
	@Basic
	private Long idMunicipality;// idComune;
	
	@Basic
	private String locality; 	// Località;

	@Basic
	private String address1; 	// Indirizzo1;

	@Basic
	private String address2; 	// Indirizzo2;

	@Basic
	private String telephone; 	// Telefono;
	
	@Basic
	private String email; 		// Email;

	@ManyToOne (fetch=FetchType.LAZY)
	@JoinColumn (name = "idDistributionList")	
	private DistributionList distributionList ;
	
	@OneToMany(
			cascade=CascadeType.ALL, 
			orphanRemoval=true, 
			mappedBy="productionCenter", 
			fetch=FetchType.LAZY)
	private List<Location> locations;
	
//	@Basic
//	private Date date;
	
	
//	@ManyToOne (fetch=FetchType.EAGER)
//	private Company masterCompany;
//	
//	@ManyToMany(fetch=FetchType.LAZY) // no cascade
//	@JoinTable(name = "mediasetstudios_contracts_master_company_employees",
//	  joinColumns = @JoinColumn(name = "contract_id"),
//	  inverseJoinColumns = @JoinColumn(name = "master_company_employee_id"),
//	  uniqueConstraints=@UniqueConstraint(columnNames={"contract_id","master_company_employee_id"})
//	)
//	private List<CompanyEmployee> masterCompanyEmployees;
	
//	@OneToMany(cascade=CascadeType.ALL, orphanRemoval=true, mappedBy="productionCenter", fetch=FetchType.EAGER)
//	private List<CheckinDesk> desks;
/*	
	@OneToMany(cascade=CascadeType.ALL, orphanRemoval=true, mappedBy="productionCenter", fetch=FetchType.EAGER)
	private List<CheckinDesk> desks;
*/	
	
//	@OneToMany(cascade=CascadeType.ALL, orphanRemoval=true, mappedBy="contract", fetch=FetchType.LAZY)
//	private List<SubcontractData> subcontractCompanies;
	
	
	/**
     * Costruttore
     */
    public ProductionCenter() {
    	super();
    }
    
  
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductionCenter other = (ProductionCenter) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
	public String getDescription() {
		return description;
	}

	
	public void setDescription(String description) {
		this.description= description;
	}


	@Override
	public String toString() {
		return "Descrizione [id = " + id + ", description = " + description + "]";
	}


	public Long getIdMunicipality() {
		return idMunicipality;
	}


	public void setIdMunicipality(Long idMunicipality) {
		this.idMunicipality = idMunicipality;
	}


	public String getLocality() {
		return locality;
	}


	public void setLocality(String locality) {
		this.locality = locality;
	}


	public String getAddress1() {
		return address1;
	}


	public void setAddress1(String address1) {
		this.address1 = address1;
	}


	public String getAddress2() {
		return address2;
	}


	public void setAddress2(String address2) {
		this.address2 = address2;
	}


	public String getTelephone() {
		return telephone;
	}


	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		
		this.email = TextUtil.cleanTextContent(email);
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public List<Location> getLocations() {
		return locations;
	}


	public void setLocations(List<Location> locations) {
		this.locations = locations;
	}


	public DistributionList getDistributionList() {
		return distributionList;
	}


	public void setDistributionList(DistributionList distributionList) {
		this.distributionList = distributionList;
	}
}
