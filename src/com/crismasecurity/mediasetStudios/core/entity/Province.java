package com.crismasecurity.mediasetStudios.core.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
/**
 * 
 * Class name: Province
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author 	fdongmo
 * @date 	06/june/2018
 *
 */
@Entity
@javax.persistence.Table(name="mediastudios_province")
public class Province implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3719579621414164792L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(nullable = false)
	private Long id ; // Unique ID
	

	@Basic
	private String codProv; 

	@Basic
	private String provDescription; 

	
	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn (name = "idRegion")
	private Region region;
	
	/**
     * Costruttore
     */
    public Province() {
    	super();
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codProv == null) ? 0 : codProv.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Province other = (Province) obj;
		if (codProv == null) {
			if (other.codProv != null)
				return false;
		} else if (!codProv.equals(other.codProv))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Province [codProv=" + codProv + ", provDescription=" + getProvDescription() + ", region=" + region + "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodProv() {
		return codProv;
	}

	public void setCodProv(String codProv) {
		this.codProv = codProv;
	}

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	public String getProvDescription() {
		return provDescription;
	}

	public void setProvDescription(String provDescription) {
		this.provDescription = provDescription;
	}


}
