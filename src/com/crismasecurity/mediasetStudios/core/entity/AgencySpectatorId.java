package com.crismasecurity.mediasetStudios.core.entity;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Embeddable;
import javax.persistence.Column;

@Embeddable
public class AgencySpectatorId 
	implements Serializable{

	private static final long serialVersionUID = 1839901364533125710L;

	@Column(name= "idAgency")
	private Long idAgency;
	
	@Column(name= "idSpectator")
	private Long idSpectator;

	@SuppressWarnings("unused")
	private AgencySpectatorId() {
	}

	public AgencySpectatorId(Long idAgency, Long idSpectator) {
		this.setIdAgency(idAgency);
		this.setIdSpectator(idSpectator); 
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
 
        if (o == null || getClass() != o.getClass()) 
            return false;
 
        AgencySpectatorId that = (AgencySpectatorId) o;
        return Objects.equals(idAgency, that.idAgency) && 
               Objects.equals(idSpectator, that.idSpectator);
    }
 
    @Override
    public int hashCode() {
        return Objects.hash(idAgency, idSpectator);
    }

	public Long getIdAgency() {
		return idAgency;
	}

	public void setIdAgency(Long idAgency) {
		this.idAgency = idAgency;
	}

	public Long getIdSpectator() {
		return idSpectator;
	}

	public void setIdSpectator(Long idSpectator) {
		this.idSpectator = idSpectator;
	}
}
