package com.crismasecurity.mediasetStudios.core.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
//@javax.persistence.Table(name="mediastudios_blacklistspectator")

@Table(
		   name="mediastudios_blacklistspectator",
		   uniqueConstraints=
		       @UniqueConstraint(columnNames={"idIncident", "idSpectator", "idTypeBlackList"})
		)

public class BlackListSpectator implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -174720340196915648L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(nullable = false)
	private Long idBlackListSpectator;
	
	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn (name = "idIncident")
	private Incident incident;
	
	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn (name = "idTypeBlackList")
	private BlackListType blackListType;
	
	@OneToOne (fetch = FetchType.EAGER)
	@JoinColumn (name = "idSpectator")
	private Spectator spectator;
	
	@Basic
	private String signaler;

	@Basic
	private String site;

	@Column
	@Type(type = "org.hibernate.type.DateType")
	private Date dateTimeBlackList;		// Data Ora Incident  
	
	@Basic
	private String reason;
	
	@Column(columnDefinition = "NVARCHAR(MAX)")
	private String reasonExt;

	@UpdateTimestamp
	@Temporal(TemporalType.DATE)
	@Column(name = "updatedOn")
	private Date updatedOn;	
	
	public Incident getIncident() {
		return incident;
	}

	public void setIncident(Incident incident) {
		this.incident = incident;
	}

	public BlackListType getBlackListType() {
		return blackListType;
	}


	public void setBlackListType(BlackListType blackListType) {
		this.blackListType = blackListType;
	}


	public Spectator getSpectator() {
		return spectator;
	}


	public void setSpectator(Spectator spectator) {
		this.spectator = spectator;
	}



	public Long getIdBlackListSpectator() {
		return idBlackListSpectator;
	}


	public void setIdBlackListSpectator(Long idBlackListSpectator) {
		this.idBlackListSpectator = idBlackListSpectator;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((blackListType == null) ? 0 : blackListType.hashCode());
		result = prime * result + ((dateTimeBlackList == null) ? 0 : dateTimeBlackList.hashCode());
		result = prime * result + ((idBlackListSpectator == null) ? 0 : idBlackListSpectator.hashCode());
		result = prime * result + ((incident == null) ? 0 : incident.hashCode());
		result = prime * result + ((reason == null) ? 0 : reason.hashCode());
		result = prime * result + ((spectator == null) ? 0 : spectator.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BlackListSpectator other = (BlackListSpectator) obj;
		if (blackListType == null) {
			if (other.blackListType != null)
				return false;
		} else if (!blackListType.equals(other.blackListType))
			return false;
		if (dateTimeBlackList == null) {
			if (other.dateTimeBlackList != null)
				return false;
		} else if (!dateTimeBlackList.equals(other.dateTimeBlackList))
			return false;
		if (idBlackListSpectator == null) {
			if (other.idBlackListSpectator != null)
				return false;
		} else if (!idBlackListSpectator.equals(other.idBlackListSpectator))
			return false;
		if (incident == null) {
			if (other.incident != null)
				return false;
		} else if (!incident.equals(other.incident))
			return false;
		if (reason == null) {
			if (other.reason != null)
				return false;
		} else if (!reason.equals(other.reason))
			return false;
		if (spectator == null) {
			if (other.spectator != null)
				return false;
		} else if (!spectator.equals(other.spectator))
			return false;
		return true;
	}


	
	@Override
	public String toString() {
		return "BlackListSpectator [idBlackListSpectator=" + idBlackListSpectator + ", incident=" + incident
				+ ", blackListType=" + blackListType + ", spectator=" + spectator + ", dateTimeBlackList="
				+ dateTimeBlackList + ", reason=" + reason + "]";
	}


	public Date getDateTimeBlackList() {
		return dateTimeBlackList;
	}


	public void setDateTimeBlackList(Date dateTimeBlackList) {
		this.dateTimeBlackList = dateTimeBlackList;
	}


	public String getReason() {
		return reasonExt;
	}


	public void setReason(String reason) {
		this.reasonExt = reason;
	}


	public String getReasonExt() {
		return reasonExt;
	}


	public void setReasonExt(String reasonExt) {
		this.reasonExt = reasonExt;
	}

	public String getSignaler() {
		return signaler;
	}

	public void setSignaler(String signaler) {
		this.signaler = signaler;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public void cloneFrom(BlackListSpectator cloneFrom) {
		this.blackListType=cloneFrom.blackListType;
		this.dateTimeBlackList=cloneFrom.dateTimeBlackList;
		this.incident=cloneFrom.incident;
		this.reason=cloneFrom.reason;
		this.reasonExt=cloneFrom.reasonExt;
		this.signaler=cloneFrom.signaler;
		this.site=cloneFrom.site;
		this.spectator=cloneFrom.spectator;
	}
	
}
