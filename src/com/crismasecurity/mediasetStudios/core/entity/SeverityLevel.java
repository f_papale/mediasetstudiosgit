package com.crismasecurity.mediasetStudios.core.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * 
 * Class name: SeverityLevel
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author 	fdongmo
 * @date 	06/june/2018 
 *
 */
@Entity
//@javax.persistence.Table(name="mediastudios_severityLevel")
@Table(
		   name="mediastudios_severityLevel",
		   uniqueConstraints=
		       @UniqueConstraint(columnNames={"description"})
		)
public class SeverityLevel implements Serializable {


	private static final long serialVersionUID = -7602705916474037696L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(nullable = false)
	private Long id ; // Unique ID
	
	@Basic
	private String description; 
	
	/**
     * Costruttore
     */
    public SeverityLevel() {
    	super();
    }
    
  
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SeverityLevel other = (SeverityLevel) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
	public String getDescription() {
		return description;
	}

	
	public void setDescription(String description) {
		this.description= description;
	}


	@Override
	public String toString() {
		return "Descrizione [id = " + id + ", description = " + description + "]";
	}

}
