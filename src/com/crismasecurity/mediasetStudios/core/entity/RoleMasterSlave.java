package com.crismasecurity.mediasetStudios.core.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.crismasecurity.mediasetStudios.core.security.entities.Role;


/**
 * 
 * Class name: AgencyType
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author 	fpapale
 * @date 	25/may/2018
 *
 */
@Entity
//@javax.persistence.Table(name="mediastudios_roleMasterSlave")
/**/	

@Table(
   name="mediastudios_roleMasterSlave",
   uniqueConstraints = @UniqueConstraint(columnNames={"idMasterRole", "idSlaveRole"})
		)


public class RoleMasterSlave implements Serializable {

	private static final long serialVersionUID = 6161657137819764669L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(nullable = false)
	private Long id ; // Unique ID

	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn (name = "idMasterRole")
	private Role masterRole ;
	
	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn (name = "idSlaveRole")
	private Role slaveRole ;

	
	/**
     * Costruttore
     */
    public RoleMasterSlave() {
    	super();
    }
    


	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((masterRole == null) ? 0 : masterRole.hashCode());
		result = prime * result + ((slaveRole == null) ? 0 : slaveRole.hashCode());
		return result;
	}


	public Role getMasterRole() {
		return masterRole;
	}



	public void setMasterRole(Role masterRole) {
		this.masterRole = masterRole;
	}



	public Role getSlaveRole() {
		return slaveRole;
	}



	public void setSlaveRole(Role slaveRole) {
		this.slaveRole = slaveRole;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RoleMasterSlave other = (RoleMasterSlave) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (masterRole == null) {
			if (other.masterRole != null)
				return false;
		} else if (!masterRole.equals(other.masterRole))
			return false;
		if (slaveRole == null) {
			if (other.slaveRole != null)
				return false;
		} else if (!slaveRole.equals(other.slaveRole))
			return false;
		return true;
	}

}
