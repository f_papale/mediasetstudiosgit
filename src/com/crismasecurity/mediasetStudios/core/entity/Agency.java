package com.crismasecurity.mediasetStudios.core.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Type;

import com.crismasecurity.mediasetStudios.core.utils.TextUtil;


/**
 * 
 * Class name: Agency
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author 	fpapale
 * @date 	25/may/2018
 *
 */
@Entity
/*
@javax.persistence.Table(name="mediastudios_agency") 
*/
@Table(
		   name="mediastudios_agency",
		   uniqueConstraints=
		       @UniqueConstraint(columnNames={"name"})
		)

public class Agency implements Serializable {  
	
	private static final long serialVersionUID = -4288355476160388937L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idAgency", nullable = false)
	private Long idAgency ; // Unique ID 
	
//	@Column
//	@Type(type = "org.hibernate.type.NumericBooleanType")
//	private boolean statusOk; 	
	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn (name = "idAgencyType")
	private AgencyType agencyType ;
/*	
	@ManyToMany(fetch=FetchType.LAZY) // no cascade
	@JoinTable(name = "mediasetstudios_agencySpectator",
	  joinColumns = @JoinColumn(name = "contract_id"),
	  inverseJoinColumns = @JoinColumn(name = "master_company_employee_id"),
	  uniqueConstraints=@UniqueConstraint(columnNames={"contract_id","master_company_employee_id"})
	)
*/
	
	@OneToMany(
			cascade=CascadeType.ALL, 
			orphanRemoval=true, 
			mappedBy="agency", 
			fetch=FetchType.LAZY
			)
	private List<AgencySpectator> spectators = new ArrayList<>();
	
	
	@Basic
	private String name; 		// Denominazione;
	
	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn (name = "idMunicipality")
	private CountryCode city;
	
	@Basic
	private String address1;	// Indirizzo1;
	@Basic
	private String address2;	// Indirizzo2;
	@Basic
	private String telephone;	// Telefono
	@Basic
	private String mobile;		// Cellulare
	@Basic
	private String contact;		// Referente
	@Basic
	private String email;		// Email
	@Column
	@Type(type = "org.hibernate.type.NumericBooleanType")
	@ColumnDefault("0")
	private boolean hidden;	// Attivo
		

//	@Basic
//	private Date date;
	
	
//	@ManyToOne (fetch=FetchType.EAGER)
//	private Company masterCompany;
//	
//	@ManyToMany(fetch=FetchType.LAZY) // no cascade
//	@JoinTable(name = "mediasetstudios_contracts_master_company_employees",
//	  joinColumns = @JoinColumn(name = "contract_id"),
//	  inverseJoinColumns = @JoinColumn(name = "master_company_employee_id"),
//	  uniqueConstraints=@UniqueConstraint(columnNames={"contract_id","master_company_employee_id"})
//	)
//	private List<CompanyEmployee> masterCompanyEmployees;
//	
//	@OneToMany(cascade=CascadeType.ALL, orphanRemoval=true, fetch=FetchType.LAZY)
//	private List<Allegato5Data> listaAllegati5;
	
	
//	@OneToMany(cascade=CascadeType.ALL, orphanRemoval=true, mappedBy="contract", fetch=FetchType.LAZY)
//	private List<SubcontractData> subcontractCompanies;
	
	
	/**
     * Costruttore
     */
    public Agency() {
    	super();
    }
    
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idAgency == null) ? 0 : idAgency.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Agency other = (Agency) obj;
		if (idAgency == null) {
			if (other.idAgency != null)
				return false;
		} else if (!idAgency.equals(other.idAgency))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}



	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name= name;
	}


	@Override
	public String toString() {
		return "Denominazione [idAgency = " + getIdAgency() + ", name = " + name + "]";
	}



	public String getAddress1() {
		return address1;
	}


	public void setAddress1(String address1) {
		this.address1 = address1;
	}


	public String getAddress2() {
		return address2;
	}


	public void setAddress2(String address2) {
		this.address2 = address2;
	}


	public String getTelephone() {
		return telephone;
	}


	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}


	public String getMobile() {
		return mobile;
	}


	public void setMobile(String mobile) {
		this.mobile = mobile;
	}


	public String getContact() {
		return contact;
	}


	public void setContact(String contact) {
		this.contact = contact;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = TextUtil.rearrangeEmailList(email);
	}


	public AgencyType getAgencyType() {
		return agencyType;
	}


	public void setAgencyType(AgencyType agencyType) {
		this.agencyType = agencyType;
	}


	public Long getIdAgency() {
		return idAgency;
	}


	public void setIdAgency(Long idAgency) {
		this.idAgency = idAgency;
	}

	public List<AgencySpectator> getSpectators() {
		return spectators;
	}

	public void setSpectators(List<AgencySpectator> spectators) {
		this.spectators = spectators;
	}


	
    public void addSpectator(Spectator spectator) {
        AgencySpectator agencySpectator = new AgencySpectator(this, spectator);
        spectators.add(agencySpectator);
        spectator.getAgencies().add(agencySpectator);
    }
 
    public void addSpectator(Spectator spectator, String description) {
        AgencySpectator agencySpectator = new AgencySpectator(this, spectator, description);
        spectators.add(agencySpectator);
        spectator.getAgencies().add(agencySpectator);   
    }
 
    public void removeSpectator(Spectator spectator) {
        for (Iterator<AgencySpectator> iterator = spectators.iterator(); iterator.hasNext(); ) {
            AgencySpectator agencySpectator = iterator.next();
 
            if (agencySpectator.getAgency().equals(this) && agencySpectator.getSpectator().equals(spectator)) {
                iterator.remove();
                agencySpectator.getSpectator().getAgencies().remove(agencySpectator);
                agencySpectator.setAgency(null);
                agencySpectator.setSpectator(null);
            }
        }
    }

	public CountryCode getCity() {
		return city;
	}

	public void setCity(CountryCode city) {
		this.city = city;
	}

	public boolean isHidden() {
		return hidden;
	}

	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}
}

