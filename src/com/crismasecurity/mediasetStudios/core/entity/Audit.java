package com.crismasecurity.mediasetStudios.core.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

//import com.crismasecurity.mediasetStudios.core.utils.ObjectDiff.ObjectDiff;
import com.crismasecurity.mediasetStudios.web.utils.MakeHTML;



/**
 * 
 * Class name: Audit
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author 
 * @date 20/octo/2017
 *
 */
@Entity
@javax.persistence.Table(name="mediastudios_audit")
public class Audit implements Serializable {

	private static final long serialVersionUID = -8108024435316331609L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(nullable = false)
	private Long id ; // Unique ID
	
	@Basic
	private String userLogin; // Utente che ha eseguito l'azione
	
	@Basic
	private Date timestamp; // timestamp
	@Basic
	private String action; // es insert, update, login, etc
	@Basic
	private String object; // es. richiesta
	@Basic
	private Long objectId; // es. idRichiesta
	
	@Basic
	private String subject; // Nome e cognome dell'owner richiesta o dell'utente che ha fatto il login
	
	@Basic
	private String status; // Stato richiesta, es. bozza, approvata, etc
	
	@Lob
	@Column
	private String oldHtmlRecord;
	@Lob
	@Column
	private String newHtmlRecord;

	@Lob
	@Column(name = "jsonContent", columnDefinition="VARCHAR(MAX)")
	private String jsonContent;	
	
	/**
     * Costruttore
     */
    public Audit() {
    	super();
    	
    	timestamp = new Date();
    }
    

  
	


	public Audit(String userLogin, String action, String subject) {
		super();
		this.userLogin = userLogin;
		this.action = action;
		this.subject = subject;
		this.jsonContent=null;
		timestamp = new Date();
	}



	public Audit(String userLogin, String action, String object, Long objectId, String subject,
			String status, String oldHtmlRecord, String newHtmlRecord, String jsonContent) {
		super();
		this.userLogin = userLogin;
		this.action = action;
		this.object = object;
		this.objectId = objectId;
		this.subject = subject;
		this.status = status;
		this.oldHtmlRecord = oldHtmlRecord;
		this.newHtmlRecord = newHtmlRecord;
		this.jsonContent=jsonContent;
		
		timestamp = new Date();
	}	

//	
	
//	public Audit(String userLogin, String action, String object, Long objectId, String subject,
//			String status, String oldHtmlRecord, String newHtmlRecord, Object oldRecord, Object newRecord) {
//		super();
//		ObjectDiff oDiff = new ObjectDiff(oldRecord,newRecord);
//		this.userLogin = userLogin;
//		this.action = action;
//		this.object = object;
//		this.objectId = objectId;
//		this.subject = subject;
//		this.status = status;
//		this.oldHtmlRecord = oldHtmlRecord;
//		this.newHtmlRecord = newHtmlRecord;
//		this.jsonContent=oDiff.getJson();
//		
//		timestamp = new Date();
//	}

	public Audit(String userLogin, String action, String object, Long objectId, String subject,
			String status, String oldHtmlRecord, String newHtmlRecord) {
		super();
		this.userLogin = userLogin;
		this.action = action;
		this.object = object;
		this.objectId = objectId;
		this.subject = subject;
		this.status = status;
		this.oldHtmlRecord = oldHtmlRecord;
		this.newHtmlRecord = newHtmlRecord;
		this.jsonContent=null;
		timestamp = new Date();
	}


	public Audit(String userLogin, String action, String object, Long objectId, String subject,
			String status, Object oldRecord, Object newRecord) {
		super();
		this.userLogin = userLogin;
		this.action = action;
		this.object = object;
		this.objectId = objectId;
		this.subject = subject;
		this.status = status;
		
		
		this.oldHtmlRecord = (oldRecord!=null?MakeHTML.makeHTML(oldRecord):"");
		this.newHtmlRecord = (newRecord!=null?MakeHTML.makeHTML(newRecord):"");
		this.jsonContent=null;
		
		timestamp = new Date();
	}




	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}




	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Audit other = (Audit) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}


	

	public String getUserLogin() {
		return userLogin;
	}




	public void setUserLogin(String userLogin) {
		this.userLogin = userLogin;
	}






	public Date getTimestamp() {
		return timestamp;
	}






	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}






	public String getAction() {
		return action;
	}






	public void setAction(String action) {
		this.action = action;
	}






	public String getObject() {
		return object;
	}






	public void setObject(String object) {
		this.object = object;
	}






	public Long getObjectId() {
		return objectId;
	}






	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}






	public String getSubject() {
		return subject;
	}






	public void setSubject(String subject) {
		this.subject = subject;
	}






	public String getStatus() {
		return status;
	}






	public void setStatus(String status) {
		this.status = status;
	}






	public String getOldHtmlRecord() {
		return oldHtmlRecord;
	}






	public void setOldHtmlRecord(String oldHtmlRecord) {
		this.oldHtmlRecord = oldHtmlRecord;
	}






	public String getNewHtmlRecord() {
		return newHtmlRecord;
	}






	public void setNewHtmlRecord(String newHtmlRecord) {
		this.newHtmlRecord = newHtmlRecord;
	}






	@Override
	public String toString() {
		return "Audit [id=" + id + ", userLogin=" + userLogin + ", timestamp=" + timestamp + ", action=" + action
				+ ", object=" + object + ", objectId=" + objectId + ", subject=" + subject + ", status=" + status
				+ ", getId()=" + getId() + ", hashCode()=" + hashCode() + ", getUserLogin()=" + getUserLogin()
				+ ", getTimestamp()=" + getTimestamp() + ", getAction()=" + getAction() + ", getObject()=" + getObject()
				+ ", getObjectId()=" + getObjectId() + ", getSubject()=" + getSubject() + ", getStatus()=" + getStatus()
				+ ", getOldHtmlRecord()=" + getOldHtmlRecord() + ", getNewHtmlRecord()=" + getNewHtmlRecord()
				+ ", getClass()=" + getClass() + ", toString()=" + super.toString() + "]";
	}






	public String getJsonContent() {
		return jsonContent;
	}






	public void setJsonContent(String jsonContent) {
		this.jsonContent = jsonContent;
	}



}
