package com.crismasecurity.mediasetStudios.core.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
/**
 * 
 * Class name: CountryCode
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author 	fdongmo
 * @date 	06/june/2018
 *
 */
@Entity
/*
@javax.persistence.Table(name="mediastudios_countryCode")
*/
@Table(
		   name="mediastudios_countryCode",
		   uniqueConstraints=
		       @UniqueConstraint(columnNames={"countryCode"})
		)

public class CountryCode implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 497943057147358630L;


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(nullable = false)
	private Long id ; // Unique ID
	

	@Basic
	private String countryCode; 

	@Basic
	private String city; 

	
	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn (name = "idProvince")
	private Province province;
	
	/**
     * Costruttore
     */
    public CountryCode() {
    	super();
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getCountryCode() == null) ? 0 : getCountryCode().hashCode());
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CountryCode other = (CountryCode) obj;
		if (getCountryCode() == null) {
			if (other.getCountryCode() != null)
				return false;
		} else if (!getCountryCode().equals(other.getCountryCode()))
			return false;
		if (getId() == null) {
			if (other.getId() != null)
				return false;
		} else if (!getId().equals(other.getId()))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public Province getProvince() {
		return province;
	}

	public void setProvince(Province province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "CountryCode [id=" + id + ", countryCode=" + countryCode + ", city=" + city + ", province=" + province
				+ "]";
	}
}
