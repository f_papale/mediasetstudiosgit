package com.crismasecurity.mediasetStudios.core.security.services;

import java.util.List;
import java.util.Map;

import org.springframework.security.access.AccessDeniedException;

import com.crismasecurity.mediasetStudios.core.security.vos.security.GroupVO;


public interface GroupService {
	
	GroupVO createGroup (GroupVO group);
	
	GroupVO updateGroup ( GroupVO group );

	void deleteGroup ( GroupVO group );

	GroupVO loadGroup ( Long id ) throws AccessDeniedException;

	List<GroupVO> findAllGroups ();

	boolean existGroupName (String groupName );
	
	public GroupVO findByName(String name) throws AccessDeniedException;
	
	public void addRoleToGroup(Long roleId, Long groupId);
	
	public void removeRoleFromGroup(Long roleId, Long groupId);
	
	public List<GroupVO> findByFields(Map<String, Object> args);
	
}
