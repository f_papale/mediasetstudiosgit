package com.crismasecurity.mediasetStudios.core.security.services;

import java.util.List;

import com.crismasecurity.mediasetStudios.core.security.vos.security.RoleVO;




public interface RoleService {
	
	RoleVO createRole ( RoleVO role );
	
	RoleVO updateRole( RoleVO role );
	
	void deleteRole ( RoleVO role );
	
	List<RoleVO> findAllRoles ();
	
	RoleVO findRoleById ( String id );
	
	List<RoleVO> findRoleByName( String name );
	
	List<RoleVO> findRoleBySection ( int section);

}
