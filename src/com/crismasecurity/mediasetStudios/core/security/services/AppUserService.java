package com.crismasecurity.mediasetStudios.core.security.services;

import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.crismasecurity.mediasetStudios.core.security.entities.AppUser;
import com.crismasecurity.mediasetStudios.core.security.vos.security.AppUserVO;



public interface AppUserService {
	
	AppUserVO createUser(AppUserVO user) ;
	
	AppUserVO updateUser(AppUserVO user) throws AccessDeniedException;
	
	void deleteUser(AppUserVO user) throws AccessDeniedException;
	
	AppUserVO loadUser(String id) throws AccessDeniedException;
	
	List<AppUserVO> findAllUsers();
	
	boolean existUser (AppUserVO user );
	
	public void addUserInGroup(Long userid, Long groupId) throws AccessDeniedException;
	public void removeUserfromGroup(Long userId, Long groupId) throws AccessDeniedException;
	
	public AppUserVO getUserByLogin(String login) throws AccessDeniedException;
	public AppUser getUserEntityByLogin(String login) throws AccessDeniedException;
	
	public List<AppUserVO> findByFields(Map<String, Object> args);
	
	public String getUserDescription(String login);
	
	public String encodePasswordForUser ( String userId, String plainPassword ) throws UsernameNotFoundException;
	public  AppUser convertToEntity(AppUserVO uservo);
	
	public boolean hasAuthority(String auth);
	
	
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException;
}
