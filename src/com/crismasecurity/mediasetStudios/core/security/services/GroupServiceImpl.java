package com.crismasecurity.mediasetStudios.core.security.services;

import static org.springframework.transaction.annotation.Propagation.REQUIRED;
import static org.springframework.transaction.annotation.Propagation.SUPPORTS;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.impl.LogFactoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.widee.base.hibernate.criteria.Finder;
import com.crismasecurity.mediasetStudios.core.security.dao.GroupDAO;
import com.crismasecurity.mediasetStudios.core.security.dao.RoleDAO;
import com.crismasecurity.mediasetStudios.core.security.entities.AppUser;
import com.crismasecurity.mediasetStudios.core.security.entities.Group;
import com.crismasecurity.mediasetStudios.core.security.entities.Role;
import com.crismasecurity.mediasetStudios.core.security.vos.security.GroupVO;
import com.crismasecurity.mediasetStudios.core.security.vos.security.RoleVO;



@Service("groupService")
public class GroupServiceImpl implements GroupService, Serializable {
   
	private static final long serialVersionUID = 0L;

	protected static final Log log = LogFactoryImpl.getLog( GroupServiceImpl.class );
	
	
	protected GroupVO convertToVo(Group group) {
		GroupVO groupvo = new GroupVO();
		try {
			BeanUtils.copyProperties(groupvo, group);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		
		List<RoleVO> rolevo = new ArrayList<RoleVO>();
		if (group.getRoles()!=null) {
			for (Role role : group.getRoles()) {
				rolevo.add((new RoleServiceImpl()).convertToVo(role));
			}
		}
		groupvo.setRoles(rolevo);
		
		List<Long> userIds = new ArrayList<Long>();
		if (group.getUsers()!=null) {
			for (AppUser user : group.getUsers()) {
				userIds.add(user.getId());
			}
		}
		groupvo.setUsersId(userIds);
		
//		List<Long> archiviIds = new ArrayList<Long>();
//		if (group.getArchiviGranted()!=null) {
//			for (Archivio arc : group.getArchiviGranted()) {
//				archiviIds.add(arc.getId());
//			}
//		}
//		groupvo.setArchiviGrantedIds(archiviIds);
//		
//		List<Long> museiIds = new ArrayList<Long>();
//		if (group.getMuseiGranted()!=null) {
//			for (Museo mus : group.getMuseiGranted()) {
//				museiIds.add(mus.getId());
//			}
//		}
//		groupvo.setMuseiGrantedIds(museiIds);
//		
//		List<Long> sistemiMusealiIds = new ArrayList<Long>();
//		if (group.getSistemiMusealiGranted()!=null) {
//			for (SistemaMuseale sm : group.getSistemiMusealiGranted()) {
//				sistemiMusealiIds.add(sm.getId());
//			}
//		}
//		groupvo.setSistemiMusealiGrantedIds(sistemiMusealiIds);
		
		
		return groupvo;
	}
	
	
	protected  Group convertToEntity(GroupVO groupvo) {
		Group group = new Group();
		
		group.setName(groupvo.getName());
		if (groupvo.getId()!=null) group.setId(groupvo.getId());
		
		List<Role> roles = new ArrayList<Role>();
		if (groupvo.getRoles()!=null) {
			for (RoleVO rolevo : groupvo.getRoles()) {
				roles.add((new RoleServiceImpl()).convertToEntity(rolevo));
			}
		}
		group.setRoles(roles);
		

		
		return group;
	}
	
	
	@Transactional ( propagation = SUPPORTS )
//	@Secured ( {"ROLE_ADMIN" } )
	@Secured({ "ROLE_ADMIN", "ROLE_STADMIN", "ROLE_GRO_MAN_W","ROLE_GRO_MAN_R" })
	public List<GroupVO> findByFields(Map<String, Object> args){
		List<GroupVO> usvo = new ArrayList<GroupVO>();
		//List<Group> u = groupDAO.findByField(args, null);
		Set<String> fields = args.keySet();
		Finder<Group> finder = groupDAO.getFinder();
		for (String field : fields) {
			if (args.get(field) instanceof String) {
				System.out.println("Like for field " + field);
				finder = finder.and(field).like(args.get(field));
			}
			else {
				System.out.println("Equal for field " + field);
				finder = finder.and(field).eq(args.get(field));
			}
		}
		List<Group> u = finder.addOrderDesc("name").setDistinct().list();
		
		for (Group us : u) {
			usvo.add(convertToVo(us));
		}
		return usvo;
	}
	
	
	@Transactional( propagation=REQUIRED )
//	@Secured ( {"ROLE_ADMIN" } )
	@Secured({ "ROLE_ADMIN", "ROLE_STADMIN", "ROLE_GRO_MAN_W","ROLE_GRO_MAN_R" })
	public GroupVO createGroup ( GroupVO groupvo ) {
		Group group = convertToEntity(groupvo);
		group = groupDAO.create(group);
		return convertToVo(group );
	}
	

	@Transactional( propagation=REQUIRED )
//	@Secured ( {"ROLE_ADMIN" } )
	@Secured({"ROLE_ADMIN", "ROLE_STADMIN", "ROLE_GRO_MAN_W","ROLE_GRO_MAN_R" })
	public GroupVO updateGroup ( GroupVO groupvo ) {
		
		Group groupLoad = groupDAO.get( groupvo.getId() );
		
		Group group = convertToEntity(groupvo);
		
		try {
			
			BeanUtils.copyProperties(groupLoad, group);
		} catch (Exception e) {e.printStackTrace();}
		groupLoad.setRoles(group.getRoles() );

//		// Bug?? se group.getStrutturaArchivioId() == null in groupLoad mette 0!!!!!!!!!!
//		groupLoad.setStrutturaArchivioId(group.getStrutturaArchivioId());
//		groupLoad.setStrutturaMuseoId(group.getStrutturaMuseoId());
//		groupLoad.setStrutturaSistemaMusealeId(group.getStrutturaSistemaMusealeId());
//		
		// !!!!!! Gli utenti non vengono modificati
		
		groupDAO.update(groupLoad );
		return convertToVo(groupLoad);
	}


	@Transactional( propagation=REQUIRED )
//	@Secured ( {"ROLE_ADMIN" } )
	@Secured({ "ROLE_ADMIN", "ROLE_STADMIN", "ROLE_GRO_MAN_W","ROLE_GRO_MAN_R" })
	public void deleteGroup ( GroupVO groupvo ) {
		if (groupvo!=null && groupvo.getId()!=null)
		groupDAO.delete( groupvo.getId() );
	}


	@Transactional( propagation=SUPPORTS )
//	@Secured ( {"ROLE_ADMIN" } )
	@Secured({"ROLE_ADMIN", "ROLE_STADMIN", "ROLE_GRO_MAN_W","ROLE_GRO_MAN_R" })
	public List<GroupVO> findAllGroups () {
		
		List<GroupVO> groupvos = new ArrayList<GroupVO>();
//		List<Group> groups = groupDAO.getAll();
		List<Group> groups = groupDAO.getFinder().addOrderAsc("name").list();
		for (Group group : groups) {
			List<Group> gr = new ArrayList<Group>();
			gr.add(group);
			if (AppUserServiceImpl.hasPermissionGranted(gr)) {
				//System.out.println("Ritorno gruppo: " + group.getName());
				groupvos.add(convertToVo(group));
			}
		}
		return groupvos;
	}


	@Transactional( propagation=SUPPORTS )
//	@Secured ( {"ROLE_ADMIN" } )
	@Secured({ "ROLE_ADMIN", "ROLE_STADMIN", "ROLE_GRO_MAN_W","ROLE_GRO_MAN_R"})
	public GroupVO loadGroup ( Long id ) throws AccessDeniedException {
		Group group = groupDAO.get( id );
		List<Group> gr = new ArrayList<Group>();
		gr.add(group);
		if (!AppUserServiceImpl.hasPermissionGranted(gr)) throw new AccessDeniedException("Non hai i permessi per caricare questo gruppo!!");
		return convertToVo(group);
	}
	
	@Transactional( propagation=SUPPORTS )
//	@Secured ( {"ROLE_ADMIN" } )
	@Secured({ "ROLE_ADMIN", "ROLE_STADMIN", "ROLE_GRO_MAN_W","ROLE_GRO_MAN_R"})
	public boolean existGroupName (String groupName ) {
		if (groupDAO.findByName(groupName).size()>0) return true;
		return false;
	}
	
	@Autowired (required = true )
	private GroupDAO groupDAO;

	@Transactional( propagation=SUPPORTS )
//	@Secured ( {"ROLE_ADMIN" } )
	@Secured({"ROLE_ADMIN", "ROLE_STADMIN", "ROLE_GRO_MAN_W","ROLE_GRO_MAN_R" })
	public GroupVO findByName(String name)  throws AccessDeniedException {
		List<Group> grs = groupDAO.findByName(name);
		if (grs!=null && grs.size()>0) {
			List<Group> gr = new ArrayList<Group>();
			gr.add(grs.get(0));
			if (!AppUserServiceImpl.hasPermissionGranted(gr)) throw new AccessDeniedException("Non hai i permessi per caricare questo gruppo!");
			return convertToVo(grs.get(0));
		}
		return null;
	}


	@Autowired (required = true )
	private RoleDAO roleDAO;
	
	@Transactional( propagation=REQUIRED)
//	@Secured ( {"ROLE_ADMIN" } )
	@Secured({"ROLE_ADMIN", "ROLE_STADMIN", "ROLE_GRO_MAN_W","ROLE_GRO_MAN_R"})
	public void addRoleToGroup(Long roleId, Long groupId) {
		Role role = roleDAO.get(roleId);
		Group group = groupDAO.get(groupId);
		
		if (!group.getRoles().contains(role)) {
			group.getRoles().add(role);
			groupDAO.update(group);
		}
	}


	@Transactional( propagation=REQUIRED )
//	@Secured ( {"ROLE_ADMIN" } )
	@Secured({"ROLE_ADMIN", "ROLE_STADMIN", "ROLE_GRO_MAN_W","ROLE_GRO_MAN_R"})
	public void removeRoleFromGroup(Long roleId, Long groupId) {
		Role role = roleDAO.get(roleId);
		Group group = groupDAO.get(groupId);
		
		group.getRoles().remove(role);
		groupDAO.update(group);
	}

}
