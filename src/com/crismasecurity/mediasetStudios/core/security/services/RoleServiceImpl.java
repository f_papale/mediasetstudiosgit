package com.crismasecurity.mediasetStudios.core.security.services;

import static org.springframework.transaction.annotation.Propagation.REQUIRED;
import static org.springframework.transaction.annotation.Propagation.SUPPORTS;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.security.dao.RoleDAO;
import com.crismasecurity.mediasetStudios.core.security.entities.Role;
import com.crismasecurity.mediasetStudios.core.security.vos.security.RoleVO;



@Service("roleService")
public class RoleServiceImpl implements RoleService, Serializable {

	//serial
	private static final long serialVersionUID = 662244822753198390L;


	@Autowired
	private RoleDAO roleDAO; 
	
	
	protected RoleVO convertToVo(Role role) {
		RoleVO rolevo = new RoleVO();
		try {
			BeanUtils.copyProperties(rolevo, role);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		
		return rolevo;
	}
	
	
	protected  Role convertToEntity(RoleVO rolevo) {
		Role role = new Role();
		
		role.setNome(rolevo.getNome());
		role.setDescrizione(rolevo.getDescrizione());
		role.setDescrizioneBreve(rolevo.getDescrizioneBreve());
		role.setOrdine(rolevo.getOrdine());
		role.setSezione(rolevo.getSezione());
		if (rolevo.getId()!=null) role.setId(rolevo.getId());

		role.setInterestedAreas(rolevo.getInterestedAreas());
		
		return role;
	}
	
	
	@Transactional( propagation=REQUIRED )
//	@Secured ( {"ROLE_ADMIN" } )
	@Secured({ "ROLE_ADMIN", "ROLE_STADMIN","ROLE_ROL_MAN_W","ROLE_ROL_MAN_R" })
	public RoleVO createRole ( RoleVO rolevo ) {
		Role role = convertToEntity(rolevo);
		
		role = roleDAO.create( role );
		
		return convertToVo(role);
	}

	@Transactional( propagation=REQUIRED )
//	@Secured ( {"ROLE_ADMIN" } )
	@Secured({"ROLE_ADMIN", "ROLE_STADMIN","ROLE_ROL_MAN_W","ROLE_ROL_MAN_R" })
	public RoleVO updateRole ( RoleVO rolevo ) {
		
		Role roleLoad = roleDAO.get( rolevo.getId() );
		
		Role role = convertToEntity(rolevo);
		
		try {
			BeanUtils.copyProperties(roleLoad, role);
		} catch (Exception e) {e.printStackTrace();}

		roleDAO.update(roleLoad );
		return convertToVo(roleLoad);
	}

	@Transactional( propagation=REQUIRED )
//	@Secured ( {"ROLE_ADMIN" } )
	@Secured({ "ROLE_ADMIN", "ROLE_STADMIN","ROLE_ROL_MAN_W","ROLE_ROL_MAN_R" })
	public void deleteRole ( RoleVO rolevo ) {
		if (rolevo!=null && rolevo.getId()!=null)
			roleDAO.delete( rolevo.getId() );
	}

	@Transactional( propagation=SUPPORTS )
	@Secured({ "ROLE_ADMIN", "ROLE_STADMIN","ROLE_ROL_MAN_W","ROLE_ROL_MAN_R"})
	public List<RoleVO> findAllRoles () {
		
		List<RoleVO> rolevos = new ArrayList<RoleVO>();
		List<Role> roles = roleDAO.getFinder().setDistinct().addOrderAsc("descrizioneBreve").list();
		for (Role role : roles) {
			
			rolevos.add(convertToVo(role));
		}
		return rolevos;
	}
	
	@Transactional( propagation=SUPPORTS )
//	@Secured ( {"ROLE_ADMIN"} )
	@Secured({ "ROLE_ADMIN", "ROLE_STADMIN","ROLE_ROL_MAN_W","ROLE_ROL_MAN_R" })
	public RoleVO findRoleById ( String id ) {
		return convertToVo(roleDAO.get(Long.decode(id)));
	}
	
	
	@Transactional( propagation=SUPPORTS )
	@Secured({ "ROLE_ADMIN", "ROLE_STADMIN","ROLE_ROL_MAN_W","ROLE_ROL_MAN_R" })
	public List<RoleVO> findRoleByName ( String name ) {
		List<RoleVO> rolesVOs = new ArrayList<RoleVO>();
		List<Role> roles = roleDAO.findByRole(name);
		for (Role role : roles) {
			RoleVO roleVO = convertToVo(role);
			rolesVOs.add(roleVO);
		}
		return rolesVOs;
	}
	
	@Transactional( propagation=SUPPORTS )
	@Secured({ "ROLE_ADMIN", "ROLE_STADMIN","ROLE_ROL_MAN_W","ROLE_ROL_MAN_R" })
	public List<RoleVO> findRoleBySection ( int section) {
		List<RoleVO> rolesVOs = new ArrayList<RoleVO>();
		List<Role> roles = roleDAO.findBySection(section);
		for (Role role : roles) {
			RoleVO roleVO = convertToVo(role);
			rolesVOs.add(roleVO);
		}
		return rolesVOs;
	}

}
