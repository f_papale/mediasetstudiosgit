package com.crismasecurity.mediasetStudios.core.security.services;

import static org.springframework.transaction.annotation.Propagation.REQUIRED;
import static org.springframework.transaction.annotation.Propagation.SUPPORTS;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.persistence.NoResultException;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.impl.LogFactoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.authentication.dao.SaltSource;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.widee.base.hibernate.criteria.Finder;
import com.crismasecurity.mediasetStudios.core.security.AuthenticatedUserDto;
import com.crismasecurity.mediasetStudios.core.security.dao.AppUserDAO;
import com.crismasecurity.mediasetStudios.core.security.dao.GroupDAO;
import com.crismasecurity.mediasetStudios.core.security.entities.AppUser;
import com.crismasecurity.mediasetStudios.core.security.entities.Group;
import com.crismasecurity.mediasetStudios.core.security.entities.Role;
import com.crismasecurity.mediasetStudios.core.security.services.AppUserService;
import com.crismasecurity.mediasetStudios.core.security.services.GroupServiceImpl;
import com.crismasecurity.mediasetStudios.core.security.utils.BusinessFunctionGrantedAuthority;
import com.crismasecurity.mediasetStudios.core.security.vos.security.AppUserVO;
import com.crismasecurity.mediasetStudios.core.security.vos.security.GroupVO;



@SuppressWarnings("deprecation")
@Service("appUserService")
public class AppUserServiceImpl implements AppUserService, UserDetailsService, /*AuthenticationProvider, AuthenticationUserDetailsService,*/ Serializable {

	private static final long serialVersionUID = 0L;

	protected static final Log log = LogFactoryImpl.getLog( AppUserServiceImpl.class );
	
	@Autowired
	private AppUserDAO appUserDao;

	@Autowired
	private PasswordEncoder passwordEncoder;
	
	
	protected AppUserVO convertToVo(AppUser user) {
		AppUserVO uservo = new AppUserVO();
		try {
			BeanUtils.copyProperties(uservo, user);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		
		String groupsNames = "";
		List<GroupVO> groupsvo = new ArrayList<GroupVO>();
		if (user.getGroups()!=null) {
			for (Group group : user.getGroups()) {
				GroupVO g = (new GroupServiceImpl()).convertToVo(group);
				groupsvo.add(g);
				groupsNames+=g.getName()+", ";
			}
		}
		uservo.setGroups(groupsvo);
		uservo.setGroupsNames(groupsNames);
		
		
		return uservo;
	}
	
	
	@Override	
	@Transactional ( propagation = SUPPORTS )
//	@Secured ( {"ROLE_ADMIN" } )
	@Secured({ "ROLE_ADMIN", "ROLE_STADMIN","ROLE_USM_MAN_W","ROLE_USM_MAN_R" })
	public List<AppUserVO> findByFields(Map<String, Object> args){
		List<AppUserVO> usvo = new ArrayList<AppUserVO>();
		
//		List<AppUser> u = appUserDao.findByField(args, null);
		Set<String> fields = args.keySet();
		Finder<AppUser> finder = appUserDao.getFinder();
		for (String field : fields) {
			if (args.get(field) instanceof String) {
				System.out.println("Like for field " + field);
				finder = finder.and(field).like(args.get(field));
			}
			else {
				System.out.println("Equal for field " + field);
				finder = finder.and(field).eq(args.get(field));
			}
		}
		List<AppUser> u = finder.addOrderDesc("firstName").setDistinct().list();
		for (AppUser us : u) {
			usvo.add(convertToVo(us));
		}
		return usvo;
	}
	
	public  AppUser convertToEntity(AppUserVO uservo) {
		AppUser user = new AppUser();
		
		user.setFirstName(uservo.getFirstName());
		user.setLastName(uservo.getLastName());
		user.setLogin(uservo.getLogin());
			
		user.setLdapUser(uservo.isLdapUser());
		user.setAdfsUser(uservo.isAdfsUser());
		user.setDisabled(uservo.isDisabled());
		
		user.setPasswordChangeDate(uservo.getPasswordChangeDate());
		
		if (uservo.getId()!=null) user.setId(uservo.getId());			
		
		if(uservo.getNewPassword() != null && !"".equals(uservo.getNewPassword())) {
			user.setPassword(uservo.getNewPassword());
			user.setPassword( encodePassword( user ) );
			
			user.setPasswordChangeDate(new Date());
		}

		List<Group> groups = new ArrayList<Group>();
		if (uservo.getGroups()!=null) {
			for (GroupVO groupvo : uservo.getGroups()) {
				groups.add((new GroupServiceImpl()).convertToEntity(groupvo));
			}
		}
		user.setGroups(groups);
		
		return user;
	}
	
	
	@Transactional ( propagation = REQUIRED )
//	@Secured ( {"ROLE_ADMIN" } )
	@Secured({ "ROLE_ADMIN", "ROLE_STADMIN","ROLE_USM_MAN_W","ROLE_USM_MAN_R" })
	public AppUserVO createUser (AppUserVO uservo ) {
		
		AppUser user = convertToEntity(uservo);
		
		user = appUserDao.create( user );

		return convertToVo(user);
	}

	@Transactional ( propagation = REQUIRED )
//	@Secured ( {"ROLE_ADMIN" } )
	//@Secured({ "ROLE_ADMIN" })
	public AppUserVO updateUser ( AppUserVO uservo )  throws AccessDeniedException {
		AppUser userLoad = appUserDao.get( uservo.getId() );
		
		if (!hasPermissionGranted(userLoad.getGroups())) throw new AccessDeniedException("Non hai i permessi per aggiornare questo utente!!");
		
		userLoad.setFirstName(uservo.getFirstName());
		userLoad.setLastName(uservo.getLastName());
		userLoad.setLogin(uservo.getLogin());
		userLoad.setLdapUser(uservo.isLdapUser());
		userLoad.setAdfsUser(uservo.isAdfsUser());
		userLoad.setDisabled(uservo.isDisabled());
		
		AppUser user = convertToEntity(uservo);
//		try {
//			BeanUtils.copyProperties(userLoad, user);
//		} catch (Exception e) {e.printStackTrace();}
		userLoad.setGroups(user.getGroups() );
		
		if (uservo.getNewPassword()!=null && !uservo.getNewPassword().equals("")) {
			user.setPassword(uservo.getNewPassword());
			userLoad.setPassword( encodePassword( user ) );
			
			userLoad.setPasswordChangeDate(new Date());
		}

		appUserDao.update( userLoad );
		return convertToVo(userLoad);
	}

	@Transactional ( propagation = REQUIRED )
	@Secured({ "ROLE_ADMIN", "ROLE_STADMIN","ROLE_USM_MAN_W","ROLE_USM_MAN_R" })
	public void deleteUser ( AppUserVO uservo ) throws AccessDeniedException {
		if (uservo!=null && uservo.getId()!=null) {
			
			if (!hasPermissionGranted(appUserDao.get(uservo.getId()).getGroups())) throw new AccessDeniedException("Non hai i permessi per cancellare questo utente!");
			
			appUserDao.delete( uservo.getId() );
		}
	}

	@Transactional ( propagation = SUPPORTS )
	public List<AppUserVO> findAllUsers () {
		List<AppUserVO> usvos = new ArrayList<AppUserVO>();
		List<AppUser> uss = appUserDao.getAll();
		for (AppUser appUser : uss) {
			if (hasPermissionGranted(appUser.getGroups())) usvos.add(convertToVo(appUser));
		}
		return usvos;
	}

	
	
	/**
	 * Controllo se l'utente loggato ha i permessi per gestire questi gruppi
	 * @param user
	 * @return
	 */
	public static boolean hasPermissionGranted(List<Group> groups) {
		
		return true;
	}
	
	
	
	@Transactional ( propagation = SUPPORTS )
//	@Secured ( {"ROLE_ADMIN" } )
	@Secured({ "ROLE_ADMIN", "ROLE_STADMIN","ROLE_USM_MAN_W","ROLE_USM_MAN_R" })
	public AppUserVO loadUser ( String id ) throws AccessDeniedException {
		
		AppUser user = appUserDao.get(Long.decode(id) );
		
		if (!hasPermissionGranted(user.getGroups())) throw new AccessDeniedException("Insufficient permissions.");
		
		return convertToVo(user);
	}

	
	////******************************* Spring security User Stuff *******************************************//
	
	@Autowired
	private Properties businessFunctions;
	
	/** 
	 * Will create an implementation of UserDetails object populating it with 
	 * custom GrantedAuthorities
	 */
	@SuppressWarnings("deprecation")
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {
			
			try {
			  List<AppUser> lusers = appUserDao.findByLoginName(username);
			  if (lusers==null || lusers.size()==0) throw new UsernameNotFoundException("User with login name '" + username + "' not found!");
			  
			  if(appUserDao.findByLoginName(username).get(0).isDisabled()) throw new UsernameNotFoundException("User with login name '" + username + "' id disabled!");
			  
		      AppUser user = appUserDao.findByLoginName(username).get(0);
		      
		      List<GrantedAuthority> grList = new ArrayList<GrantedAuthority>();
			  if (user.getGroups()!=null && user.getGroups().size()>0) {
				  
				  	// Aggiungo i Ruoli
					for (Group group : user.getGroups()) { 
//						System.out.println("Utente " + username + " fa parte del gruppo " + group.getName() + " con ruoli:");
						for (Role role : group.getRoles()) {
							System.out.println("Ruolo: " + role.getNome());
							GrantedAuthority grRole = new SimpleGrantedAuthority(role.getNome());
							// Aggiungo il Ruolo
							grList.add(grRole);
							
							
							// Aggiungo le Businness Functions Associate al ruolo (Gerarchia)
							Set<String> aditionalAuthorities = StringUtils.commaDelimitedListToSet(businessFunctions.getProperty(grRole.getAuthority()));
							/*
							 * Create GrantedAuthority for each business function and add it to the list of
							 * GrantedAuthorities representing roles
							 */
							for (String aditionalAuthority : aditionalAuthorities) {
								
								BusinessFunctionGrantedAuthority authority = new BusinessFunctionGrantedAuthority(aditionalAuthority);
								
								grList.add(authority);
										
								/*
								 * Add parent (role) GrantedAuthority
								 */
								authority.addParentAuthority(grRole);	
							}
						}
					}
			  }
			  
			  for (GrantedAuthority gr : grList) {
				System.out.println("Aggiunto ruolo:" + gr.getAuthority().toString());
			}
			  
//		      System.out.println("Creati i grants per l'utente " + username);
		      //AuthenticatedUserDto authus = new AuthenticatedUserDto(user.getLogin(), user.getPassword(), true, true, true, true, grList, user.getFirstName()+" "+user.getLastName(), user.getId() /*, struttureRiferimentoMuseo, struttureRiferimentoSistemaMuseale, struttureRiferimentoArchivio*/);
		      AuthenticatedUserDto authus = new AuthenticatedUserDto(user.getLogin(), ((user.isLdapUser()||user.isAdfsUser())?" ":user.getPassword()), true, true, true, true, grList, user.getFirstName()+" "+user.getLastName(), user.getId() /*, struttureRiferimentoMuseo, struttureRiferimentoSistemaMuseale, struttureRiferimentoArchivio*/);

		      
//		      System.out.println("Oggetto AuthenticatedUserDto per l'utente " + username + " creato!");
		      return authus;
		    } catch (NoResultException e) {
		    	//e.printStackTrace();
		    	throw new UsernameNotFoundException(e.getLocalizedMessage());
		    } catch (Exception e) {
		    	//e.printStackTrace();
		      	throw new UsernameNotFoundException(e.getLocalizedMessage());
		    }
	}
	/**
	 * 
	 * @return
	 */
	public Properties getBusinessFunctions() {
		return businessFunctions;
	}
	/**
	 * 
	 * @param businessFunctions
	 */
	public void setBusinessFunctions(Properties businessFunctions) {
		this.businessFunctions = businessFunctions;
	}
	
	///*******************************************************************************************************//
	
	
	
	
	
	
	
	

//	@Transactional ( propagation = SUPPORTS )
//	public UserDetails loadUserDetails ( Authentication authentication ) throws UsernameNotFoundException {
//		if ( authentication.getPrincipal() != null && authentication.getPrincipal() instanceof UserDetails ) {
//			return (UserDetails) authentication.getPrincipal();
//		} else {
//			return loadUserByUsername( authentication.getName() );
//		}
//	}

	
	private String encodePassword ( AppUser user ) {

		if ( user.getPassword() == null || "".equals( user.getPassword().trim() ) ) {
			return user.getPassword();
		}

		
		Object salt = null;
		if ( saltSource != null ) {
			List<GrantedAuthority> grList = new ArrayList<GrantedAuthority>();
			if (user.getGroups()!=null && user.getGroups().size()>0) {
				for (Group group : user.getGroups()) {
					for (Role role : group.getRoles()) {
						grList.add(new SimpleGrantedAuthority(role.getNome()));
					}
				}
			}
			salt = saltSource.getSalt( new AuthenticatedUserDto(user.getLogin(), user.getPassword(), true, true, true, true, grList, user.getFirstName()+" "+user.getLastName(), user.getId()/*, struttureRiferimentoMuseo, struttureRiferimentoSistemaMuseale, struttureRiferimentoArchivio*/));
		}
		return passwordEncoder.encodePassword( user.getPassword(), salt );
	}

	@Transactional ( propagation = SUPPORTS )
	public String encodePasswordForUser ( String login, String plainPassword ) throws UsernameNotFoundException {
		AppUserVO usvo = getUserByLogin(login);
		if (usvo==null) throw new UsernameNotFoundException("User" + login + " not found!!");
		usvo.setNewPassword(plainPassword);
		AppUser user = convertToEntity(usvo);
		return user.getPassword();
	}


	private SaltSource saltSource;

	public void setSaltSource ( SaltSource saltSource ) {
		this.saltSource = saltSource;
	}

	@Transactional ( propagation = SUPPORTS )
//	@Secured ( {"ROLE_ADMIN" } )
	@Secured({ "ROLE_ADMIN", "ROLE_STADMIN","ROLE_USM_MAN_W","ROLE_USM_MAN_R" })
	public boolean existUser ( AppUserVO uservo ) {
//		if (uservo.getId()!=null) {
//			if (appUserDao.get(uservo.getId())!=null) return true;
//		}
		if (uservo.getLogin()!=null) {
			if (appUserDao.findByLoginName(uservo.getLogin()).size()>0) return true;
		}

		return false;
	}
	
	
	@Autowired
	private GroupDAO groupDao;
	
	@Transactional( propagation=REQUIRED)
//	@Secured ( {"ROLE_ADMIN" } )
	@Secured({ "ROLE_ADMIN", "ROLE_STADMIN","ROLE_USM_MAN_W","ROLE_USM_MAN_R" })
	public void addUserInGroup(Long userId, Long groupId) throws AccessDeniedException {
		Group groupl = groupDao.get(groupId);
		AppUser userl = appUserDao.get(userId);
		
		if (!hasPermissionGranted(userl.getGroups())) throw new AccessDeniedException("Non hai i permessi per assegnare questo utente a un gruppo!");
		
		if (!userl.getGroups().contains(groupl) ) {
			userl.getGroups().add(groupl);
			appUserDao.update(userl);
		}
	}

	@Transactional( propagation=REQUIRED)
//	@Secured ( {"ROLE_ADMIN" } )
	@Secured({ "ROLE_ADMIN", "ROLE_STADMIN","ROLE_USM_MAN_W","ROLE_USM_MAN_R" })
	public void removeUserfromGroup(Long userId, Long groupId) throws AccessDeniedException {
		Group groupl = groupDao.get(groupId);
		AppUser userl = appUserDao.get(userId);
		
		if (!hasPermissionGranted(userl.getGroups())) throw new AccessDeniedException("Non hai i permessi per rimuovere questo utente dal gruppo!");
		
		userl.getGroups().remove(groupl);
		appUserDao.update(userl);
	}
	
	
	

	@Transactional ( propagation = SUPPORTS )
//	@Secured ( {"ROLE_ADMIN" } )
	//@Secured({ "ROLE_ADMIN", "BF_SECURITY_READER" })
	public AppUserVO getUserByLogin(String login) throws AccessDeniedException {
		List<AppUser> us = appUserDao.findByLoginName(login);
		if (us!=null && us.size()>0) {
			
			AppUser user = us.get(0);
			
			if (!hasPermissionGranted(user.getGroups())) throw new AccessDeniedException("Non hai i permessi per caricare questo utente!");
			
			return convertToVo(user);
		}
		return null;
	}

	public AppUser getUserEntityByLogin(String login) throws AccessDeniedException {
		List<AppUser> us = appUserDao.findByLoginName(login);
		if (us!=null && us.size()>0) {
			
			AppUser user = us.get(0);
			
			if (!hasPermissionGranted(user.getGroups())) throw new AccessDeniedException("Non hai i permessi per caricare questo utente!");
			
			return user;
		}
		return null;
	}
	
	@Transactional ( propagation = SUPPORTS )
	public String getUserDescription(String login) {
		List<AppUser> us = appUserDao.findByLoginName(login);
		if (us!=null && us.size()>0) {
			
			AppUser user = us.get(0);
			
			//if (!hasPermissionGranted(user.getGroups())) throw new AccessDeniedException("Non hai i permessi per caricare questo utente!");
			
			return user.getLastName() + " " + user.getFirstName();
//			return user.getFirstName();
		}
		return null;
	}
	
	public boolean hasAuthority(String auth) {
		AuthenticatedUserDto user = (AuthenticatedUserDto)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		System.out.println("hasAuthority " + auth + "? - per user: " + SecurityContextHolder.getContext().getAuthentication().getPrincipal());
    	if (user.getAuthorities()!=null)  {
    		for (GrantedAuthority gr : user.getAuthorities()) {
    			System.out.println("Autority trovata: " + gr.getAuthority());
    			if (gr.getAuthority().equalsIgnoreCase(auth)) {
    				return true;
    			}
			}
    	}
    	return false;
	}

/*
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String username = authentication.getName();
        String password = (String) authentication.getCredentials();
   
        System.out.println("authenticate--!!!!!!!!!!!!! user: " + username + "   pass:" + password);
        UserDetails user = loadUserByUsername(username);
   
          if (user == null || !user.getUsername().equalsIgnoreCase(username)) {
              throw new BadCredentialsException("Username not found.");
          }
   
          if (!password.equals(user.getPassword())) {
              throw new BadCredentialsException("Wrong password.");
          }
   
          Collection<? extends GrantedAuthority> authorities = user.getAuthorities();
   
          return new UsernamePasswordAuthenticationToken(user, password, authorities);
	}
	@Override
	public boolean supports(Class<?> authentication) {
		return true;
	}
	
	*/
}
