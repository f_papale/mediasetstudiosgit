/**
 * 
 */
package com.crismasecurity.mediasetStudios.core.security;

import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class AuthenticatedUserDto extends User {

	// Serial
	private static final long serialVersionUID = -7207052610679926168L;

	private String displayName;

	private Long userId;

	public String getDisplayName() {
		return displayName;
	}

	public Long getUserId() {
		return userId;
	}


	public AuthenticatedUserDto(String username, String password,
			boolean enabled, boolean accountNonExpired,
			boolean credentialsNonExpired, boolean accountNonLocked,
			List<GrantedAuthority> authorities, String displayName, Long userId)
	throws IllegalArgumentException {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
		this.displayName = displayName;
		this.userId = userId;
	}


}
