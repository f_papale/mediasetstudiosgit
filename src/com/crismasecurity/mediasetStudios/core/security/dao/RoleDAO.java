/**
 * 
 */
package com.crismasecurity.mediasetStudios.core.security.dao;

import java.util.List;

import com.crismasecurity.mediasetStudios.core.security.entities.Role;
import com.widee.base.dao.base.GenericDAO;



/**
 * Class name: RoleDAO
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author Giovanni
 * @date 26/giu/2009
 *
 */
public interface RoleDAO extends GenericDAO<Role, Long> {
	
	public List<Role> findByRole(String role);
	public List<Role> findBySection(int section);
}


