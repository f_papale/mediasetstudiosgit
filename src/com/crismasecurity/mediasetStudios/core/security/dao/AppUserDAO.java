/**
 * 
 */
package com.crismasecurity.mediasetStudios.core.security.dao;

import java.util.List;

import com.crismasecurity.mediasetStudios.core.security.entities.AppUser;
import com.widee.base.dao.base.GenericDAO;


 
/**
 * Class name: AppUserDAO
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author Giovanni
 * @date 26/giu/2009
 *
 */
public interface AppUserDAO extends GenericDAO<AppUser, Long>{

	public List<AppUser> findByName(String name);
    
	public List<AppUser> findByLoginName(String name);
}


