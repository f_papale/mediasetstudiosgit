/**
 * 
 */
package com.crismasecurity.mediasetStudios.core.security.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.security.dao.AppUserDAO;
import com.crismasecurity.mediasetStudios.core.security.entities.AppUser;
import com.widee.base.dao.base.impl.GenericDAOImpl;



/**
 * Class name: AppUserDAO
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author Giovanni
 * @date 26/giu/2009
 *
 */
@Repository
public class AppUserDAOImpl extends GenericDAOImpl<AppUser, Long> implements AppUserDAO {

	/**
	 * @param persistentClass
	 */
	public AppUserDAOImpl() {
		super(AppUser.class);
	}

	@SuppressWarnings("unchecked")
	public List<AppUser> findByName(String name){
		Query query = getEntityManager().createNamedQuery("AppUser.findByName");
		query.setParameter(1, name);
		return query.getResultList();
    }
    
	@SuppressWarnings("unchecked")
	public List<AppUser> findByLoginName(String name){
		Query query = getEntityManager().createNamedQuery("AppUser.findByLoginName");
		query.setParameter(1, name);
		return query.getResultList();
    }
    
}


