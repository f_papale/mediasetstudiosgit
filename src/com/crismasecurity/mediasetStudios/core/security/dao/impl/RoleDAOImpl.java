/**
 * 
 */
package com.crismasecurity.mediasetStudios.core.security.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.security.dao.RoleDAO;
import com.crismasecurity.mediasetStudios.core.security.entities.Role;
import com.widee.base.dao.base.impl.GenericDAOImpl;



/**
 * Class name: RoleDAO
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author Giovanni
 * @date 26/giu/2009
 *
 */
@Repository
public class RoleDAOImpl extends GenericDAOImpl<Role, Long> implements RoleDAO {
	
	/**
	 * @param persistentClass
	 */
	public RoleDAOImpl() {
		super(Role.class);
	}

	@SuppressWarnings("unchecked")
	public List<Role> findByRole(String role){
		Query query = getEntityManager().createNamedQuery("Role.findByRole");
		query.setParameter(1, role);
		return query.getResultList();
    }
	
	@SuppressWarnings("unchecked")
	public List<Role> findBySection(int section){
		Query query = getEntityManager().createNamedQuery("Role.findBySection");
		query.setParameter(1, section);
		return query.getResultList();
    }	
}


