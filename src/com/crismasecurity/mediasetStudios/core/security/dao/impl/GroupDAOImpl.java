/**
 * 
 */
package com.crismasecurity.mediasetStudios.core.security.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.crismasecurity.mediasetStudios.core.security.dao.GroupDAO;
import com.crismasecurity.mediasetStudios.core.security.entities.Group;
import com.widee.base.dao.base.impl.GenericDAOImpl;



/**
 * Class name: GroupDAOImpl
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author Giovanni
 * @date 26/giu/2009
 *
 */
@Repository
public class GroupDAOImpl extends GenericDAOImpl<Group, Long> implements GroupDAO {

	/**
	 * @param persistentClass
	 */
	public GroupDAOImpl() {
		super(Group.class);
	}

	@SuppressWarnings("unchecked")
	public List<Group> findByName(String name){
		Query query = getEntityManager().createNamedQuery("Group.findByName");
		query.setParameter(1, name);
		return query.getResultList();
    }
 
    
}


