/**
 * 
 */
package com.crismasecurity.mediasetStudios.core.security.dao;

import java.util.List;

import com.crismasecurity.mediasetStudios.core.security.entities.Group;
import com.widee.base.dao.base.GenericDAO;



/**
 * Class name: GroupDAO
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author Giovanni
 * @date 26/giu/2009
 *
 */
public interface GroupDAO extends GenericDAO<Group, Long>{

	public List<Group> findByName(String name);
}


