/**
 * 
 */
package com.crismasecurity.mediasetStudios.core.security.vos.security;

import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.crismasecurity.mediasetStudios.core.utils.UserMessageAreaType;





/**
 * Class name: Role
 *
 * Description: 
 * 
 *
 * Company: OceanPro
 *
 * @author Giovanni
 * @date 25/giu/2009
 *
 */
public class RoleVO {

	private Long id;

	private String nome;
	private String descrizione;
	private String descrizioneBreve;
	private int sezione;
	private int ordine;
	private List<UserMessageAreaType> interestedAreas;
	
	public RoleVO() {}
	
	public RoleVO(String name) {
		this.nome = name;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}

	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	  
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public String getDescrizioneBreve() {
		return descrizioneBreve;
	}

	public void setDescrizioneBreve(String descrizioneBreve) {
		this.descrizioneBreve = descrizioneBreve;
	}
	
	public List<UserMessageAreaType> getInterestedAreas() {
		return interestedAreas;
	}

	public void setInterestedAreas(List<UserMessageAreaType> interestedAreas) {
		this.interestedAreas = interestedAreas;
	}

	public int getSezione() {
		return sezione;
	}

	public void setSezione(int sezione) {
		this.sezione = sezione;
	}

	public int getOrdine() {
		return ordine;
	}

	public void setOrdine(int ordine) {
		this.ordine = ordine;
	}
}
