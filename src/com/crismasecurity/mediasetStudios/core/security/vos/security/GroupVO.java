package com.crismasecurity.mediasetStudios.core.security.vos.security;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


public class GroupVO implements Serializable {
	
	// Serial
	private static final long serialVersionUID = 2528790163428340481L;

	private Long id;
	
    private String name;
    
    private List<RoleVO> roles;
    
    private List<Long> usersId;

    
    
    /**
     * Costruttore
     */
    public GroupVO() {
    }

    public GroupVO(String name) {
        this.name = name;
    }


    public int getNumeroUtenti() {
    	if (usersId==null) return 0;
    	return usersId.size();
    }
    
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}



	public List<RoleVO> getRolesBySezione(int sezione) {
		
		
		List<RoleVO> dummy = new ArrayList<RoleVO>();
		try {
			dummy = roles
					.stream()
					.filter(r -> r.getSezione() == sezione)
					.collect(Collectors.toList());
		} catch (Exception e) {
			System.out.println("Ruoli ancora non assegnati al gruppo");
		}
		return dummy;
	}
	

	public List<RoleVO> getRoles() {
		return roles;
	}
	
	public void setRoles(List<RoleVO> roles) {
		this.roles = roles;
	}
	
	public boolean equals(Object obj) {
		 return EqualsBuilder.reflectionEquals(this, obj);
	}

	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	  
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public List<Long> getUsersId() {
		return usersId;
	}

	public void setUsersId(List<Long> usersId) {
		this.usersId = usersId;
	}
}
