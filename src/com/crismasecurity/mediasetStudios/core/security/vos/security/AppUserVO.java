package com.crismasecurity.mediasetStudios.core.security.vos.security;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


public class AppUserVO implements Serializable {
	
	// Serial
	private static final long serialVersionUID = 6337581304514441767L;

	private Long id;
	
    private String firstName;

    private String lastName;

    private String login;
    
    private String newPassword; // Non viene valorizzato ma serve per cambiare password
    
    private Date passwordChangeDate;
    
    private List<GroupVO> groups;
    
    private String groupsNames; // Serve solo lato view per avere l'elenco dei gruppi
    
    private boolean ldapUser;
    
    private boolean adfsUser;
    
    private boolean disabled=false;
    
    /**
     * Costruttore
     */
    public AppUserVO() {
    }

    public AppUserVO(String login, String firstName, String lastName, List<GroupVO> groups) {
        this.login = login;
        this.firstName = firstName;
        this.lastName = lastName;
        this.groups = groups;
    }

    public String getLogin() {
        return login;
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setLogin(String login) {
		this.login = login;
	}


	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}

	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	  
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public List<GroupVO> getGroups() {
		return groups;
	}

	public void setGroups(List<GroupVO> groups) {
		this.groups = groups;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getGroupsNames() {
		return groupsNames;
	}

	public void setGroupsNames(String groupsNames) {
		this.groupsNames = groupsNames;
	}

	public boolean isLdapUser() {
		return ldapUser;
	}

	public void setLdapUser(boolean ldapUser) {
		this.ldapUser = ldapUser;
	}

	public Date getPasswordChangeDate() {
		return passwordChangeDate;
	}

	public void setPasswordChangeDate(Date passwordChangeDate) {
		this.passwordChangeDate = passwordChangeDate;
	}

	public boolean isAdfsUser() {
		return adfsUser;
	}

	public void setAdfsUser(boolean adfsUser) {
		this.adfsUser = adfsUser;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}
}
