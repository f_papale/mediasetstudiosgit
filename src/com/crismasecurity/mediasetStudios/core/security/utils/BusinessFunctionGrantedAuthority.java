/**
 * 
 */
package com.crismasecurity.mediasetStudios.core.security.utils;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.acls.domain.GrantedAuthoritySid;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;


/**
 * Simple extension to GrantedAuthorityImpl which only adds a List of
 * parent (owning) granted authorities.
 * 
 * @author oleg.zhurakousky@springsource.com
 */
public class BusinessFunctionGrantedAuthority implements GrantedAuthority {

	//Serial
	private static final long serialVersionUID = 4265846068596398194L;
	
	private List<GrantedAuthority> parentAuthorities = new ArrayList<GrantedAuthority>();
	private String role;
	/**
	 * 
	 * @param role
	 * @param businessFunctions
	 */
	public BusinessFunctionGrantedAuthority(String role) {
		//super(role);
		this.role = role;
	}
	/**
	 * 
	 * @param authority
	 */
	public void addParentAuthority(GrantedAuthority authority){
		parentAuthorities.add(authority);
	}
	/**
	 * 
	 * @return
	 */
	public List<GrantedAuthority> getParentAuthorities() {
		return parentAuthorities;
	}
	/**
	 * 
	 */
	public String toString(){
		return role;
	}
	@Override
	public String getAuthority() {
		return role;
	}
}
