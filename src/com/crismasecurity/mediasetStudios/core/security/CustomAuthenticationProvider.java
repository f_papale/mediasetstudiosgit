package com.crismasecurity.mediasetStudios.core.security;


import java.util.Collection;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.crismasecurity.mediasetStudios.core.security.entities.AppUser;
import com.crismasecurity.mediasetStudios.core.security.services.AppUserService;
import com.crismasecurity.mediasetStudios.core.utils.PropertiesUtils;

 
 
@Component
public class CustomAuthenticationProvider implements AuthenticationProvider { 
 
    @Autowired
    private AppUserService userService;
    @Autowired 
	private PasswordEncoder passwordEncoder;
 
    @Override
	public Authentication authenticate(Authentication auth) throws AuthenticationException {

    	CustomUsernamePasswordAuthenticationToken authentication = (CustomUsernamePasswordAuthenticationToken) auth;
		String username = authentication.getName();
        String password = (String) authentication.getCredentials();
   
        //System.out.println("authenticate--!!!!!!!!!!!!! user: " + username + "   pass:" + password);
        System.out.println("authenticate--!!!!!!!!!!!!! user: " + username);
        UserDetails user = null;
        try {
        	user = userService.loadUserByUsername(username);
		} catch (UsernameNotFoundException e) {
			System.err.println(e.getMessage());
		}
        
   
        if (user!=null) {
        	// Utente del sistema
        	
          if (!user.getUsername().equalsIgnoreCase(username)) { // non dovrebbe mai succedere
              throw new BadCredentialsException("Username not found.");
          }
   
          AppUser usere = userService.getUserEntityByLogin(username);
          if (usere!=null && usere.isLdapUser()){
        	  // LDAP
        	  try {
        		  checkLdapUser(usere,authentication.getPlainPassword());
			} catch (Exception e) {
				throw new BadCredentialsException(e.getMessage(), e);
			}
          }
          else if (usere!=null && usere.isAdfsUser()) {
        	  // ADFS
        	  
        	  throw new BadCredentialsException("Sembra che questo sia un utente di dominio. Prego accedere attraverso il tasto apposito!");
        	  //qui vuol dire che sta tentando di fare l'accesso non tramite ADFS server ma tramite la pagina di login di medisetStudios e non dalla pagina di adfs ....errore!!!
          }
          
          else {
        	  // UTENTE SISTEMA
	          if (!password.equals(user.getPassword())) {
	              throw new BadCredentialsException("Wrong password.");
	          }
          }
   
          Collection<? extends GrantedAuthority> authorities = user.getAuthorities();
          return new UsernamePasswordAuthenticationToken(user, password, authorities);
        }
   
        else {
          throw new BadCredentialsException("Wrong username");
        }
          
	}
	@Override
	public boolean supports(Class<?> authentication) {
		return true;
	}
	
	
	
	
	
	
	
	
	
	private boolean checkLdapUser(AppUser usere, String password) throws Exception {
		String userName = usere.getLogin();
		String passWord = password;

		String FILTER = PropertiesUtils.getInstance().readProperty("ldap.filter");
		String base = PropertiesUtils.getInstance().readProperty("ldap.baseDN");
		String domain = PropertiesUtils.getInstance().readProperty("ldap.domain");
				
		
		String CompleteUserName = domain + userName;
		//String CompleteUserName = userName;
						
		if (domain.equals(""))
		{
			CompleteUserName = userName;
		} else
		{				
			CompleteUserName = userName + "@" + domain;
		}
		
		
		String distName = "";
		Hashtable<String, String> srchEnv = new Hashtable<String, String>(11);

		srchEnv.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		srchEnv.put(Context.PROVIDER_URL, PropertiesUtils.getInstance().readProperty("ldap.url") + ":" + PropertiesUtils.getInstance().readProperty("ldap.port"));
		srchEnv.put(Context.SECURITY_AUTHENTICATION, "simple");
		srchEnv.put(Context.SECURITY_PRINCIPAL, PropertiesUtils.getInstance().readProperty("ldap.queryUser.username"));
		srchEnv.put(Context.SECURITY_CREDENTIALS, PropertiesUtils.getInstance().readProperty("ldap.queryUser.password"));
		
		String[] returnAttribute = {"st"};
		SearchControls srchControls = new SearchControls();
		srchControls.setReturningAttributes(returnAttribute);
		srchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		String searchFilter = "(" + FILTER + "=" + CompleteUserName + ")";
		DirContext srchContext = new InitialDirContext(srchEnv);
		NamingEnumeration srchResponse = srchContext.search(base, searchFilter, srchControls);
		//GG Close First connection
		srchContext.close();

		// Probably want to test for nulls here
		//distName = srchResponse.nextElement().toString();
		if (srchResponse.hasMore() == false) {
			throw new BadCredentialsException(userName + " isn't an LDAP User.");
		}

		System.out.println("DN : " + distName.toString());

		

		Hashtable<String, String> authEnv = new Hashtable<String, String>(11);
		authEnv.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		authEnv.put(Context.PROVIDER_URL, PropertiesUtils.getInstance().readProperty("ldap.url") + ":" + PropertiesUtils.getInstance().readProperty("ldap.port"));
		authEnv.put(Context.SECURITY_AUTHENTICATION, "simple");
		authEnv.put(Context.SECURITY_PRINCIPAL, CompleteUserName);
		authEnv.put(Context.SECURITY_CREDENTIALS, passWord);
		try {
			DirContext authContext = new InitialDirContext(authEnv);
			
			//GG Close Second Connection
			authContext.close();
			
			return true;
		}
		catch (AuthenticationException authEx) {
			System.out.println("LDAP: Authentication failed!");
			authEx.printStackTrace();
			throw new Exception("LDAP: Authentication failed!", authEx);
		}
		catch (NamingException namEx) {
			System.out.println("LDAP: Something went wrong!");
			namEx.printStackTrace();
			throw new Exception("LDAP: Authentication failed!", namEx);
		}
	}
	
}