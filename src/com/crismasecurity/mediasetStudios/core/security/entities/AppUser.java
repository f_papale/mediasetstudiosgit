package com.crismasecurity.mediasetStudios.core.security.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.PrimaryKeyJoinColumns;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Type;

@Entity
@Table (name="s_users")
@NamedQueries({ 
	  @NamedQuery(name= "AppUser.findByName" ,query= "select u from AppUser u where u.firstName=?1" ),
	  @NamedQuery(name= "AppUser.findByLoginName" ,query= "select u from AppUser u where u.login =?1" )
	  })
public class AppUser implements Serializable {
	
	// Serial
	private static final long serialVersionUID = 6337581304514441767L;

	@Id
//	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	@SequenceGenerator(name = "userSeq", sequenceName="S_USER_SEQ", allocationSize=10)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "userSeq")
	@Column(name="CODI_USER", nullable = false)
	private Long id;
	
	@Column(name="DESC_NOME")
    private String firstName;

	@Column(name="DESC_COGN")
    private String lastName;

	@Column(name="DESC_LOGN")
    private String login;

	@Column(name="DESC_PASW")
    private String password;
	
	@Basic
	private Date passwordChangeDate;
    
	
	@Column
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean ldapUser;
	
	@Column
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean adfsUser;
	
	@Column
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean disabled;
	
//    @ManyToMany( mappedBy="users")
//    @LazyCollection ( LazyCollectionOption.FALSE)
//    @Cascade ( value = CascadeType.DELETE_ORPHAN)
//    private List<Group> groups;
    
    @LazyCollection ( LazyCollectionOption.FALSE)
    @ManyToMany( //fetch= FetchType.EAGER,
            targetEntity=Group.class,
           cascade={CascadeType.MERGE}
    )
    @JoinTable( name="s_users_groups", 
    		joinColumns = { @JoinColumn( name="CODI_USER", referencedColumnName="CODI_USER")}, 
    		inverseJoinColumns= { @JoinColumn(name="CODI_GRUPPO", referencedColumnName="CODI_GRUPPO")} )
    @PrimaryKeyJoinColumns({
    	@PrimaryKeyJoinColumn(name="CODI_USER",referencedColumnName="CODI_USER"),
    	@PrimaryKeyJoinColumn(name="CODI_GRUPPO",referencedColumnName="CODI_GRUPPO")
    })
    @Fetch (FetchMode.SUBSELECT)
    private List<Group> groups;
    
    
    
    /**
     * Costruttore
     */
    public AppUser() {
    }

    public AppUser(String login, String firstName, String lastName,
                   String password, List<Group> groups) {
        this.login = login;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.groups = groups;
        this.disabled=false;
        
        this.passwordChangeDate = new Date();
        //assert !groups.isEmpty();
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}

	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	  
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public List<Group> getGroups() {
		return groups;
	}

	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}

	public boolean isLdapUser() {
		return ldapUser;
	}

	public void setLdapUser(boolean ldapUser) {
		this.ldapUser = ldapUser;
	}

	public Date getPasswordChangeDate() {
		return passwordChangeDate;
	}

	public void setPasswordChangeDate(Date passwordChangeDate) {
		this.passwordChangeDate = passwordChangeDate;
	}

	public boolean isAdfsUser() {
		return adfsUser;
	}

	public void setAdfsUser(boolean adfsUser) {
		this.adfsUser = adfsUser;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}
}
