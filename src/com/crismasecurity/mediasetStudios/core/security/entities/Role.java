/**
 * 
 */
package com.crismasecurity.mediasetStudios.core.security.entities;

import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.crismasecurity.mediasetStudios.core.utils.UserMessageAreaType;




/**
 * Class name: Role
 *
 * Description: 
 * 
 *
 * Company: OceanPro
 *
 * @author Giovanni
 * @date 25/giu/2009
 *
 */
@Entity
@Table (name="s_roles")
@NamedQueries({ 
  @NamedQuery(name= "Role.findByRole" ,query= "select r from Role r where r.nome = ?1" ),
  @NamedQuery(name= "Role.findBySection" ,query= "select r from Role r where r.sezione = ?1 order by r.ordine" )
})
public class Role {

	@Id
//	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	@SequenceGenerator(name = "roleSeq", sequenceName="S_ROLE_SEQ", allocationSize=10)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "roleSeq")
	@Column(name="CODI_ROLE", nullable = false)
	private Long id;

	@Column(name="DESC_NOME")
	private String nome;
	
	@Column(name="DESC_DESCRIZIONE")
	private String descrizione;
	
	@Column(name="DESC_DESCRIZIONE_BREVE")
	private String descrizioneBreve;
	
	@Column(name="SEZIONE")
	private Integer sezione;
	
	@Column(name="ORDINE")
	private Integer ordine;
	
	@ElementCollection(targetClass = UserMessageAreaType.class, fetch=FetchType.EAGER)
	@CollectionTable(name="s_role_interestedarea")
	//@JoinTable(name = "role_interestedAreas", joinColumns = JoinColumn(nome = "personID"))
	@Column(name = "interestedArea", nullable = false)
	@Enumerated(EnumType.STRING)
	private List<UserMessageAreaType> interestedAreas;

	
	public Role() {}
	
	public Role(String name) {
		this.nome = name;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}

	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	  
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public String getDescrizioneBreve() {
		return descrizioneBreve;
	}

	public void setDescrizioneBreve(String descrizioneBreve) {
		this.descrizioneBreve = descrizioneBreve;
	}

	public List<UserMessageAreaType> getInterestedAreas() {
		return interestedAreas;
	}

	public void setInterestedAreas(List<UserMessageAreaType> interestedAreas) {
		this.interestedAreas = interestedAreas;
	}

	public Integer getSezione() {
		return sezione;
	}

	public void setSezione(Integer sezione) {
		this.sezione = sezione;
	}

	public Integer getOrdine() {
		return ordine;
	}

	public void setOrdine(Integer ordine) {
		this.ordine = ordine;
	}


}
