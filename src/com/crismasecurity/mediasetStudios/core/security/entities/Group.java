package com.crismasecurity.mediasetStudios.core.security.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.PrimaryKeyJoinColumns;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table (name="s_groups")
@NamedQueries({ 
	  @NamedQuery(name= "Group.findByName" ,query= "select g from Group g where g.name = ?1" )
	  })
public class Group implements Serializable {
	
	// Serial
	private static final long serialVersionUID = 2528790163428340481L;

	@Id
//	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	@SequenceGenerator(name = "groupSeq", sequenceName="S_GROUP_SEQ", allocationSize=10)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "groupSeq")
	@Column(name="CODI_GRUPPO", nullable = false)
	private Long id;
	
	@Column(name="DESC_NOME")
    private String name;

    @ManyToMany( fetch=FetchType.LAZY,  mappedBy="groups")
    @Fetch (FetchMode.SUBSELECT)
    private List<AppUser> users;

    
    
    @LazyCollection ( LazyCollectionOption.FALSE )
//    @ManyToMany(  //fetch= FetchType.EAGER,
//            targetEntity=Role.class,
//            cascade={CascadeType.MERGE}
//    )
    @ManyToMany(  //fetch= FetchType.EAGER,
            targetEntity=Role.class
    )    
    @JoinTable( name="s_groups_roles", 
    		joinColumns = { @JoinColumn(name="CODI_GRUPPO", referencedColumnName="CODI_GRUPPO")}, 
    		inverseJoinColumns= { @JoinColumn( name="CODI_ROLE", referencedColumnName="CODI_ROLE")} )
    @PrimaryKeyJoinColumns({
    	@PrimaryKeyJoinColumn(name="CODI_GRUPPO",referencedColumnName="CODI_GRUPPO"),
    	@PrimaryKeyJoinColumn(name="CODI_ROLE",referencedColumnName="CODI_ROLE")
    })
    @Fetch (FetchMode.SUBSELECT)
    private List<Role> roles;
    
    
//    /** Struttura di appartenenza (valori ammessi: 0=non appartenente -1=appartiene a tutte le strutture id_struttura=appartiene alla struttura specificata )
//     * 
//     *  
//     * Strutture sulle quali il gruppo ha i permessi: i permessi si ottengono analizzando la coppia, cio� se (ad es. consideriamo i musei) StrutturaMuseoId = -1
//     * allora si hanno i permessi su tutti i musei, se StrutturaMuseoId=0 allora si hanno permessi su nessun museo, se StrutturaMuseoId = null
//     * allora si deve considerare la lista corrispondente e cio� museiGranted che conterr� l'elenco dei musei sui quali si hanno i permessi. 
//     * 
//     * **/
//    @Column(name="CODI_MUSEO")
//    private Long StrutturaMuseoId;
//    @Column(name="CODI_SISTEMA")
//    private Long StrutturaSistemaMusealeId;
//    @Column(name="CODI_ARCHIVIO")
//    private Long StrutturaArchivioId;
//    
//    @OneToMany (targetEntity=Museo.class, fetch=FetchType.LAZY)
//    @JoinTable(
//            name="tab_cult_gesc_group_musei_granted",
//            joinColumns = @JoinColumn( name="CODI_GRUPPO"),
//            inverseJoinColumns = @JoinColumn( name="CODI_MUSEO")
//    )
//	@Fetch (FetchMode.SUBSELECT)
//	private List<Museo> museiGranted;
//    
//    @OneToMany (targetEntity=SistemaMuseale.class, fetch=FetchType.LAZY)
//    @JoinTable(
//            name="tab_cult_gesc_group_sistemimuseali_granted",
//            joinColumns = @JoinColumn( name="CODI_GRUPPO"),
//            inverseJoinColumns = @JoinColumn( name="CODI_SISTEMA")
//    )
//	@Fetch (FetchMode.SUBSELECT)
//	private List<SistemaMuseale> sistemiMusealiGranted;
//    
//    @OneToMany (targetEntity=Archivio.class, fetch=FetchType.LAZY)
//    @JoinTable(
//            name="tab_cult_gesc_group_archivi_granted",
//            joinColumns = @JoinColumn( name="CODI_GRUPPO"),
//            inverseJoinColumns = @JoinColumn( name="CODI_ARCHIVIO")
//    )
//	@Fetch (FetchMode.SUBSELECT)
//	private List<Archivio> archiviGranted;
//    
//    /************************************************/
    
    
    /**
     * Costruttore
     */
    public Group() {
    }

    public Group(String name) {
        this.name = name;
    }

    
    
    public List<AppUser> getUsers() {
        return users;
    }
//	public void setUsers(List<AppUser> users) {
//		this.users = users;
//  }

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public List<Role> getRoles() {
		return roles;
	}
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	
	
	
	
	
	public boolean equals(Object obj) {
		 return EqualsBuilder.reflectionEquals(this, obj);
	}

	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	  
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

//	public Long getStrutturaMuseoId() {
//		return StrutturaMuseoId;
//	}
//
//	public void setStrutturaMuseoId(Long strutturaMuseoId) {
//		StrutturaMuseoId = strutturaMuseoId;
//	}
//
//	public Long getStrutturaSistemaMusealeId() {
//		return StrutturaSistemaMusealeId;
//	}
//
//	public void setStrutturaSistemaMusealeId(Long strutturaSistemaMusealeId) {
//		StrutturaSistemaMusealeId = strutturaSistemaMusealeId;
//	}
//
//	public Long getStrutturaArchivioId() {
//		return StrutturaArchivioId;
//	}
//
//	public void setStrutturaArchivioId(Long strutturaArchivioId) {
//		StrutturaArchivioId = strutturaArchivioId;
//	}
//
//	public List<Museo> getMuseiGranted() {
//		return museiGranted;
//	}
//
//	public void setMuseiGranted(List<Museo> museiGranted) {
//		this.museiGranted = museiGranted;
//	}
//
//	public List<SistemaMuseale> getSistemiMusealiGranted() {
//		return sistemiMusealiGranted;
//	}
//
//	public void setSistemiMusealiGranted(List<SistemaMuseale> sistemiMusealiGranted) {
//		this.sistemiMusealiGranted = sistemiMusealiGranted;
//	}
//
//	public List<Archivio> getArchiviGranted() {
//		return archiviGranted;
//	}
//
//	public void setArchiviGranted(List<Archivio> archiviGranted) {
//		this.archiviGranted = archiviGranted;
//	}
}
