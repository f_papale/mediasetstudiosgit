package com.crismasecurity.mediasetStudios.core.services;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.Episode;
import com.crismasecurity.mediasetStudios.core.entity.SpectatorEpisode;
import com.widee.base.hibernate.criteria.Finder;

public interface EpisodeService {
	public List<Episode> getEpisode();
	public Episode addEpisode(Episode r) throws Exception;
	public Episode updateEpisode(Episode r) throws Exception;
	public void deleteEpisode(long id);
	public Finder<Episode> getEpisodeFinder();
	public Episode getEpisodeById(Long id);
	public List<Episode> getEpisodesByIdStatus(int id);
	public List<SpectatorEpisode> getSpectatorEpisodeByEpisodeId(Long id);
	public List<Episode> getEpisodeLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getEpisodeLazyLoadingCount(Map<String,Object> filters);
	public List<Episode> getEpisodesBeforeThan(Date dateRef);


}
