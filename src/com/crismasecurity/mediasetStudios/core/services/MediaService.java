package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.Media;
import com.widee.base.hibernate.criteria.Finder;

public interface MediaService { 
	public List<Media> getMedia();
	public Media addMedia(Media r) throws Exception;
	public Media updateMedia(Media r) throws Exception;
	public void deleteMedia(long id);
	public Finder<Media> getMediaFinder();
	public Media getMediaById(Long id);
	public List<Media> getMediaLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getMediaLazyLoadingCount(Map<String,Object> filters);
	
	
	public Media getMediaByCode(String code);
}
