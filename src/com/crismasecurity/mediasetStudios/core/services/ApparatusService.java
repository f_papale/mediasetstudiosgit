package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.Apparatus;
import com.widee.base.hibernate.criteria.Finder;


/**
 * 
 * Class name: ApparatusService
 *
 * Description: Interfaccia ApparatusService 
 * 
 *
 * Company: 
 *
 * @author 	vincenzo.scaccia
 * @date 	04/jul/2018
 *
 */


public interface ApparatusService {
	
	//Spectator
	public List<Apparatus> getApparatus();
	public Apparatus addApparatus(Apparatus r) throws Exception;
	public Apparatus updateApparatus(Apparatus r) throws Exception;
	public void deleteApparatus(long id);
	public Finder<Apparatus> getApparatusFinder();
	public Apparatus getApparatusById(Long id);
	public List<Apparatus> getApparatusLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getApparatusLazyLoadingCount(Map<String,Object> filters);
}
