package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.DistributionList;
import com.widee.base.hibernate.criteria.Finder;

public interface DistributionListService { 
	public List<DistributionList> getDistributionList();
	public DistributionList addDistributionList(DistributionList r) throws Exception;
	public DistributionList updateDistributionList(DistributionList r) throws Exception;
	public void deleteDistributionList(long id);
	public Finder<DistributionList> getDistributionListFinder();
	public DistributionList getDistributionListById(Long id);
	public List<DistributionList> getDistributionListLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getDistributionListLazyLoadingCount(Map<String,Object> filters);
}
