package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.AgencyUser;
import com.crismasecurity.mediasetStudios.core.entity.BlackListSpectator;
import com.crismasecurity.mediasetStudios.core.entity.Spectator;
import com.widee.base.hibernate.criteria.Finder;

public interface BlackListSpectatorService {
	
	public List<BlackListSpectator> getBlackListSpectator();
	public BlackListSpectator addBlackListSpectator(BlackListSpectator r) throws Exception;
	public BlackListSpectator updateBlackListSpectator(BlackListSpectator r) throws Exception;
	public void deleteBlackListSpectator(long id);
	public Finder<BlackListSpectator> getBlackListSpectatorFinder();
	public BlackListSpectator getBlackListSpectatorById(Long id);
	public long getBlackListLoadingCount(Map<String,Object> filters);
	public List<BlackListSpectator> getBlackListSpectatorLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String, Object> filters);
	public boolean isNotInBlackList(Spectator spectToCheck, Long selectedProduction, Long selectedEpisode, AgencyUser currentAgencyUser, String loggedUser, boolean sendMail);
	public boolean isNotInBlackList(Long idSpectator);
	public BlackListSpectator getBlackListSpectatorByIdSpectator(Long idSpectator);
	public boolean consolidateLast();
	public int getDeletedAfterConsolidate();
	public List<String> getListItemsDeletedAfterConsolidate();
}
