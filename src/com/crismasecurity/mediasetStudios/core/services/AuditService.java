package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.Audit;
import com.widee.base.hibernate.criteria.Finder;

public interface AuditService { 
	public List<Audit> getAudits();
	public Audit addAudit(Audit r) throws Exception;
	public Audit updateAudit(Audit r) throws Exception;
	public void deleteAudit(long id);
	public Finder<Audit> getAuditFinder();
	public Audit getAuditById(Long id);
	public List<Audit> getAuditLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getAuditLazyLoadingCount(Map<String,Object> filters);
	

}
