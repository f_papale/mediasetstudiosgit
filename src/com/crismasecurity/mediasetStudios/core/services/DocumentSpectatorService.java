package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.DocumentSpectator;
import com.widee.base.hibernate.criteria.Finder;

public interface DocumentSpectatorService {
	public List<DocumentSpectator> getDocumentSpectator();
	public DocumentSpectator addDocumentSpectator(DocumentSpectator r) throws Exception;
	public DocumentSpectator updateDocumentSpectator(DocumentSpectator r) throws Exception;
	public void deleteDocumentSpectator(long id);
	public Finder<DocumentSpectator> getDocumentSpectatorFinder();
	public DocumentSpectator getDocumentSpectatorById(Long id);
	public List<DocumentSpectator> getDocumentSpectatorLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getDocumentSpectatorLazyLoadingCount(Map<String,Object> filters);

	public DocumentSpectator getDocumentSpectatorBySpectatorIdAndDocumentTypeId(Long spectatorId, Integer docTypeId);
}
