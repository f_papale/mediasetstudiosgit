package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.Province;
import com.widee.base.hibernate.criteria.Finder;

public interface ProvinceService {
	public List<Province> getProvince();
	public Province addProvince(Province r) throws Exception;
	public Province updateProvince(Province r) throws Exception;
	public void deleteProvince(long id);
	public Finder<Province> getProvinceFinder();
	public Province getProvinceById(Long id);
	public List<Province> getProvinceLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getProvinceLazyLoadingCount(Map<String,Object> filters);

}
