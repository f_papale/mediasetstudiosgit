package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.EmailQueue;
import com.widee.base.hibernate.criteria.Finder;

public interface EmailQueueService {
	public List<EmailQueue> getEmailQueue();
	public EmailQueue addEmailQueue(EmailQueue r) throws Exception;
	public EmailQueue updateEmailQueue(EmailQueue r) throws Exception;
	public void deleteEmailQueue(long id);
	public Finder<EmailQueue> getEmailQueueFinder();
	public EmailQueue getEmailQueueById(Long id);
	public EmailQueue getEmailQueueByEntityID(Long entityId, int entitytypeID);
	public List<EmailQueue> getEmailQueueListByEntityID(Long entityId, int entitytypeID);
	public List<EmailQueue> getEmailQueueToSend(int topSize,boolean onlyList);
	public void deleteEmailQueueByEntityID(Long entityId, int entitytypeID);
	public List<EmailQueue> getEmailQueueLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getEmailQueueLazyLoadingCount(Map<String,Object> filters);
	public void purgeEmailQueue();

}
