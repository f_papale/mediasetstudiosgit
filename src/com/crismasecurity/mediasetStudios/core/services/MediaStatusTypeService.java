package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.MediaStatusType;
import com.widee.base.hibernate.criteria.Finder;

public interface MediaStatusTypeService {
	public List<MediaStatusType> getMediaStatusType();
	public MediaStatusType addMediaStatusType(MediaStatusType r) throws Exception;
	public MediaStatusType updateMediaStatusType(MediaStatusType r) throws Exception;
	public void deleteMediaStatusType(long id);
	public Finder<MediaStatusType> getMediaStatusTypeFinder();
	public MediaStatusType getMediaStatusTypeById(Long id);
	public List<MediaStatusType> getMediaStatusTypeLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getMediaStatusTypeLazyLoadingCount(Map<String,Object> filters);
	
	
	public MediaStatusType getMediaStatusTypeByIdStatus(Integer statusId);

}
