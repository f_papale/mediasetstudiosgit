package com.crismasecurity.mediasetStudios.core.services;

import org.primefaces.model.StreamedContent;

import com.crismasecurity.mediasetStudios.core.entity.Episode;
import com.crismasecurity.mediasetStudios.core.entity.SpectatorEpisode;

public interface EmailMakerService{ 
	public void createEmails(Episode episode, boolean bMarkToSendMail ,int selFunction);
	public void createEmails(boolean bMarkToSendMail ,int selFunction);
	public void setEpisode(Episode episode);
	public Episode getEpisode();
	public void resendEmail();
	public void resendEmail(Episode episode);
	public void sendInvites(Episode episode);
	public void sendInvite(SpectatorEpisode spectatorEpisode);
	public int getEmailSendStatus(SpectatorEpisode spectatorEpisode);
	public StreamedContent getPDFList();
	public StreamedContent getPDFList(Episode episode);
	public StreamedContent getPDFInvite();
	public StreamedContent getPDFInvite(SpectatorEpisode spectatorEpisode);
	public boolean base64InviteListNotAvailable(Episode episode);
	public void purgeEmail(int entityTypeID, Long entityId);
	public void purgeEmail(Episode episode);
	public void resendEmail(boolean recreateList);
	public void resendEmail(Episode episode, boolean recreateList);
	public void removeSpectatorFromList(SpectatorEpisode spectatorEpisode);
}
