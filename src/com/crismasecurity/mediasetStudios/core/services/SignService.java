package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.Location;
import com.crismasecurity.mediasetStudios.core.entity.Test;
import com.crismasecurity.mediasetStudios.web.reception.ReceptionBean;
import com.widee.base.hibernate.criteria.Finder;

public interface SignService {
	
	public void addSignListener(SignListener beanInstance);
	public void removeSignListener(SignListener beanInstance);
	
	public void signReceived(Location desk, String sign);
	
	public List<SignListener> getSignListeners();

}
