package com.crismasecurity.mediasetStudios.core.services;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.Spectator;
import com.widee.base.hibernate.criteria.Finder;

public interface SpectatorService {
	//Spectator
	public List<Spectator> getSpectator();
	public Spectator addSpectator(Spectator r) throws Exception;
	public Spectator updateSpectator(Spectator r) throws Exception;
	public void deleteSpectator(long id);
	public Finder<Spectator> getSpectatorFinder();
	public Spectator getSpectatorById(Long id);
	public List<Spectator> getSpectatorLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getSpectatorLazyLoadingCount(Map<String,Object> filters);
	public List<BigDecimal> getSpectatorsID2Wipe(int retainlastmonths);
	public Spectator getSpectatorFromArchive(Spectator newSpectator);
	public void addAgencySpectator2Dummy(Spectator spectator) throws Exception;
	public Spectator addAgencySpectator2Dummy(Long spectator);
	public boolean removeDuplicatedSpectators();
}
