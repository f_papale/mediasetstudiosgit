package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.LocationType;
import com.widee.base.hibernate.criteria.Finder;


/**
 * 
 * Class name: ApparatusTypeService
 *
 * Description: Interfaccia ApparatusTypeService 
 * 
 *
 * Company: 
 *
 * @author 	vincenzo.scaccia
 * @date 	04/jul/2018
 *
 */


public interface LocationTypeService {
	
	public List<LocationType> getLocationType();
	public LocationType addLocationType(LocationType r) throws Exception;
	public LocationType updateLocationType(LocationType r) throws Exception;
	public void deleteLocationType(long id);
	public Finder<LocationType> getLocationTypeFinder();
	public LocationType getLocationTypeById(Long id);
	public List<LocationType> getLocationTypeLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getLocationTypeLoadingCount(Map<String,Object> filters);
}
