package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.Production;
import com.widee.base.hibernate.criteria.Finder;

public interface ProductionService {
	public List<Production> getProduction();
	public Production addProduction(Production r) throws Exception;
	public Production updateProduction(Production r) throws Exception;
	public void deleteProduction(long id);
	public Finder<Production> getProductionFinder();
	public Production getProductionById(Long id);
	public List<Production> getProductionLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getProductionLazyLoadingCount(Map<String,Object> filters);
}

