package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.BadgesTransits;
import com.widee.base.hibernate.criteria.Finder;

public interface BadgesTransitsService { 
	public List<BadgesTransits> getBadgesTransits();
	public BadgesTransits addBadgesTransits(BadgesTransits r) throws Exception;
	public BadgesTransits updateBadgesTransits(BadgesTransits r) throws Exception;
	public void deleteBadgesTransits(long id);
	public Finder<BadgesTransits> getBadgesTransitsFinder();
	public BadgesTransits getBadgesTransitsById(Long id);
	public List<BadgesTransits> getBadgesTransitsLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getBadgesTransitsLazyLoadingCount(Map<String,Object> filters);
}
