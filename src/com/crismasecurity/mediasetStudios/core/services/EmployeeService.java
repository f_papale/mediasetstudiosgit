package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.Employee;
import com.widee.base.hibernate.criteria.Finder;

public interface EmployeeService {
	public List<Employee> getEmployee();
	public Employee addEmployee(Employee r) throws Exception;
	public Employee updateEmployee(Employee r) throws Exception;
	public void deleteEmployee(long id);
	public Finder<Employee> getEmployeeFinder();
	public Employee getEmployeeById(Long id);
	public List<Employee> getEmployeeLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getEmployeeLazyLoadingCount(Map<String,Object> filters);

}
