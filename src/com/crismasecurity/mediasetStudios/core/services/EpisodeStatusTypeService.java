package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.EpisodeStatusType;
import com.widee.base.hibernate.criteria.Finder;

public interface EpisodeStatusTypeService {
	public List<EpisodeStatusType> getEpisodeStatusType();
	public EpisodeStatusType addEpisodeStatusType(EpisodeStatusType r) throws Exception;
	public EpisodeStatusType updateEpisodeStatusType(EpisodeStatusType r) throws Exception;
	public void deleteEpisodeStatusType(long id);
	public Finder<EpisodeStatusType> getEpisodeStatusTypeFinder();
	public EpisodeStatusType getEpisodeStatusTypeById(Long id);
	public EpisodeStatusType getEpisodeStatusTypeByIdStatus(int idStatus);
	public List<EpisodeStatusType> getEpisodeStatusTypeOrdered();
	public List<EpisodeStatusType> getEpisodeStatusTypeLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getEpisodeStatusTypeLazyLoadingCount(Map<String,Object> filters);
}
