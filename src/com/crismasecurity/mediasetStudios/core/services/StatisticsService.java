package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.Statistics;
import com.widee.base.hibernate.criteria.Finder;

public interface StatisticsService {
	public List<Statistics> getStatistics();
	public Statistics addStatistics(Statistics r) throws Exception;
	public Statistics updateStatistics(Statistics r) throws Exception;
	public void deleteStatistics(long id);
	public Finder<Statistics> getStatisticsFinder();
	public Statistics getStatisticsById(Long id);
	public List<Statistics> getStatisticsLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getStatisticsLazyLoadingCount(Map<String,Object> filters);
	public String getStatisticsByDomainMeasureValue(String domain, String measure);
	public Statistics getStatisticsByDomainMeasure(String domain, String measure);
	public List<Statistics> getStatisticsByDomain(String domain);
}
