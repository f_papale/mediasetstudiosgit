package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.ApparatusType;
import com.widee.base.hibernate.criteria.Finder;


/**
 * 
 * Class name: ApparatusTypeService
 *
 * Description: Interfaccia ApparatusTypeService 
 * 
 *
 * Company: 
 *
 * @author 	vincenzo.scaccia
 * @date 	04/jul/2018
 *
 */


public interface ApparatusTypeService {
	
	public List<ApparatusType> getApparatusType();
	public ApparatusType addApparatusType(ApparatusType r) throws Exception;
	public ApparatusType updateApparatusType(ApparatusType r) throws Exception;
	public void deleteApparatusType(long id);
	public Finder<ApparatusType> getApparatusTypeFinder();
	public ApparatusType getApparatusTypeById(Long id);
	public List<ApparatusType> getApparatusTypeLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getApparatusTypeLoadingCount(Map<String,Object> filters);
}
