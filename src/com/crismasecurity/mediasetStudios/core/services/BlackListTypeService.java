package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.BlackListType;
import com.widee.base.hibernate.criteria.Finder;

public interface BlackListTypeService {
	
	public List<BlackListType> getBlackListType();
	public BlackListType addBlackListType(BlackListType r) throws Exception;
	public BlackListType updateBlackListType(BlackListType r) throws Exception;
	public void deleteBlackListType(long id);
	public Finder<BlackListType> getBlackListTypeFinder();
	public BlackListType getBlackListTypeById(Long id);
	public BlackListType getBlackListTypeByLevel(String level);
	public List<BlackListType> getBlackListTypeLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getBlackListLoadingCount(Map<String,Object> filters);
	public BlackListType getBlackListTypeByIdType(int idType);
}
