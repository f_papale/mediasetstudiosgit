package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.IncidentType;
import com.widee.base.hibernate.criteria.Finder;

public interface IncidentTypeService {
	public List<IncidentType> getIncidentType();
	public IncidentType addIncidentType(IncidentType r) throws Exception;
	public IncidentType updateIncidentType(IncidentType r) throws Exception;
	public void deleteIncidentType(long id);
	public Finder<IncidentType> getIncidentTypeFinder();
	public IncidentType getIncidentTypeById(Long id);
	public List<IncidentType> getIncidentTypeLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getIncidentTypeLazyLoadingCount(Map<String,Object> filters);

}
