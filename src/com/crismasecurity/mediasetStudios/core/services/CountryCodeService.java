package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.CountryCode;
import com.widee.base.hibernate.criteria.Finder;

public interface CountryCodeService {
	public List<CountryCode> getCountryCode();
	public CountryCode addCountryCode(CountryCode r) throws Exception;
	public CountryCode updateCountryCode(CountryCode r) throws Exception;
	public void deleteCountryCode(long id);
	public Finder<CountryCode> getCountryCodeFinder();
	public CountryCode getCountryCodeById(Long id);
	public List<CountryCode> getCountryCodeLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getCountryCodeLazyLoadingCount(Map<String,Object> filters);

}
