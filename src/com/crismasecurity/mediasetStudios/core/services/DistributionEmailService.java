package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.DistributionEmail;
import com.crismasecurity.mediasetStudios.core.entity.DistributionEmailId;
import com.crismasecurity.mediasetStudios.core.entity.EmailContact;
import com.widee.base.hibernate.criteria.Finder;

public interface DistributionEmailService {
	public List<DistributionEmail> getDistributionEmail();
	public DistributionEmail addDistributionEmail(DistributionEmail r) throws Exception;
	public DistributionEmail updateDistributionEmail(DistributionEmail r) throws Exception;
	public void deleteDistributionEmail(DistributionEmailId id);
	public Finder<DistributionEmail> getDistributionEmailFinder();
	public DistributionEmail getDistributionEmailById(DistributionEmailId id);
	public List<DistributionEmail> getDistributionEmailLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getDistributionEmailLazyLoadingCount(Map<String,Object> filters);
	public void deleteDistributionEmailByIdDL(Long id);
	public void deleteDistributionEmailByIdEC(Long id);
	public String getEmailContactFromDistributionEmail(String distributionList);

}
