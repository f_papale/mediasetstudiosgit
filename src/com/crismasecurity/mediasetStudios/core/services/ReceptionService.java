package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import com.crismasecurity.mediasetStudios.core.entity.SpectatorEpisode;
import com.widee.base.hibernate.criteria.Finder;

public interface ReceptionService {
//	public List<CheckinDesk> getCheckinDesks();
//	public CheckinDesk addCheckinDesk(CheckinDesk r) throws Exception;
//	public CheckinDesk updateCheckinDesk(CheckinDesk r) throws Exception;
//	public void deleteCheckinDesk(long id);
//	public Finder<CheckinDesk> getCheckinDeskFinder();
//	public CheckinDesk getCheckinDeskById(Long id);

	
	public List<SpectatorEpisode> getSpectatorsEpisode();
	public SpectatorEpisode addSpectatorEpisode(SpectatorEpisode r) throws Exception;
	public SpectatorEpisode updateSpectatorEpisode(SpectatorEpisode r) throws Exception;
	public SpectatorEpisode updateSpectatorEpisodeNoLazy(SpectatorEpisode r) throws Exception;
	public void deleteSpectatorEpisode(long id);
	public Finder<SpectatorEpisode> getSpectatorEpisodeFinder();
	public SpectatorEpisode getSpectatorEpisodeById(Long id);
	public SpectatorEpisode getSpectatorEpisodeByIdNoLazy(Long id);
}
