package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.Test;
import com.widee.base.hibernate.criteria.Finder;

public interface TestService {
	public List<Test> getTests();
	public Test addTest(Test r) throws Exception;
	public Test updateTest(Test r) throws Exception;
	public void deleteTest(long id);
	public Finder<Test> getTestFinder();
	public Test getTestById(Long id);
	public List<Test> getTestLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getTestLazyLoadingCount(Map<String,Object> filters);

}
