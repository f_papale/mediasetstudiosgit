package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.LocationApparatus;
import com.crismasecurity.mediasetStudios.core.entity.SpectatorIncident;
import com.widee.base.hibernate.criteria.Finder;

public interface SpectatorIncidentService {
	public List<SpectatorIncident> getSpectatorIncident();
	public SpectatorIncident addSpectatorIncident(SpectatorIncident r) throws Exception;
	public SpectatorIncident updateSpectatorIncident(SpectatorIncident r) throws Exception;
	public void deleteSpectatorIncident(long id);
	public Finder<SpectatorIncident> getSpectatorIncidentFinder();
	public SpectatorIncident getSpectatorIncidentById(Long id);
	public List<SpectatorIncident> getSpectatorIncidentLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getSpectatorIncidentLazyLoadingCount(Map<String,Object> filters);
}
