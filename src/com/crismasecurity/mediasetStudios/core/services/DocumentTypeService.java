package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.DocumentType;
import com.widee.base.hibernate.criteria.Finder;

public interface DocumentTypeService {
	public List<DocumentType> getDocumentType();
	public DocumentType addDocumentType(DocumentType r) throws Exception;
	public DocumentType updateDocumentType(DocumentType r) throws Exception;
	public void deleteDocumentType(long id);
	public Finder<DocumentType> getDocumentTypeFinder();
	public DocumentType getDocumentTypeById(Long id);
	public List<DocumentType> getDocumentTypeLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getDocumentTypeLazyLoadingCount(Map<String,Object> filters);
	public DocumentType getDocumentTypeByTypeId(Integer id);
}
