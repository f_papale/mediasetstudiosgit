package com.crismasecurity.mediasetStudios.core.services;

import com.crismasecurity.mediasetStudios.core.entity.Location;
import com.crismasecurity.mediasetStudios.core.entity.SpectatorEpisode;

public interface SignListener {

	public void signReceived(Location desk, String sign);
	
	
	public Location getDesk();
	public SpectatorEpisode getSelectedSpectator();
	

}
