package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.MediaType;
import com.widee.base.hibernate.criteria.Finder;

public interface MediaTypeService {
	public List<MediaType> getMediaType();
	public MediaType addMediaType(MediaType r) throws Exception;
	public MediaType updateMediaType(MediaType r) throws Exception;
	public void deleteMediaType(long id);
	public Finder<MediaType> getMediaTypeFinder();
	public MediaType getMediaTypeById(Long id);
	public List<MediaType> getMediaTypeLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getMediaTypeLazyLoadingCount(Map<String,Object> filters);

}
