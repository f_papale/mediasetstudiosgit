package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.SeverityLevel;
import com.widee.base.hibernate.criteria.Finder;

public interface SeverityLevelService {
	public List<SeverityLevel> getSeverityLevel();
	public SeverityLevel addSeverityLevel(SeverityLevel r) throws Exception;
	public SeverityLevel updateSeverityLevel(SeverityLevel r) throws Exception;
	public void deleteSeverityLevel(long id);
	public Finder<SeverityLevel> getSeverityLevelFinder();
	public SeverityLevel getSeverityLevelById(Long id);
	public List<SeverityLevel> getSeverityLevelLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getSeverityLevelLazyLoadingCount(Map<String,Object> filters);

}
