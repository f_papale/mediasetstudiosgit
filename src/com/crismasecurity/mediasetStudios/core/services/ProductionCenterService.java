package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.ProductionCenter;
import com.widee.base.hibernate.criteria.Finder;

public interface ProductionCenterService {
	public List<ProductionCenter> getProductionCenter();
	public ProductionCenter addProductionCenter(ProductionCenter r) throws Exception;
	public ProductionCenter updateProductionCenter(ProductionCenter r) throws Exception;
	public void deleteProductionCenter(long id);
	public Finder<ProductionCenter> getProductionCenterFinder();
	public ProductionCenter getProductionCenterById(Long id);
	public List<ProductionCenter> getProductionCenterLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getProductionCenterLazyLoadingCount(Map<String,Object> filters);

}
