package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.AgencyType;
import com.widee.base.hibernate.criteria.Finder;

public interface AgencyTypeService {
	public List<AgencyType> getAgencyType();
	public AgencyType addAgencyType(AgencyType r) throws Exception;
	public AgencyType updateAgencyType(AgencyType r) throws Exception;
	public void deleteAgencyType(long id);
	public Finder<AgencyType> getAgencyTypeFinder();
	public AgencyType getAgencyTypeById(Long id);
	public List<AgencyType> getAgencyTypeLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getAgencyTypeLazyLoadingCount(Map<String,Object> filters);

}
