package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.SpectatorType;
import com.widee.base.hibernate.criteria.Finder;

public interface SpectatorTypeService {
	public List<SpectatorType> getSpectatorType();
	public SpectatorType addSpectatorType(SpectatorType r) throws Exception;
	public SpectatorType updateSpectatorType(SpectatorType r) throws Exception;
	public void deleteSpectatorType(long id);
	public Finder<SpectatorType> getSpectatorTypeFinder();
	public SpectatorType getSpectatorTypeById(Long id);
	public SpectatorType getSpectatorTypeByIdSpectatorType(int idSpectatorType);
	public List<SpectatorType> getSpectatorTypeLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getSpectatorTypeLazyLoadingCount(Map<String,Object> filters);
}
