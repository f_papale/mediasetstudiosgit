package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.Agency;
import com.widee.base.hibernate.criteria.Finder;

public interface AgencyService { 
	public List<Agency> getAgency();
	public Agency addAgency(Agency r) throws Exception;
	public Agency updateAgency(Agency r) throws Exception;
	public void deleteAgency(long id);
	public Finder<Agency> getAgencyFinder();
	public Agency getAgencyById(Long id);
	public List<Agency> getAgencyLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getAgencyLazyLoadingCount(Map<String,Object> filters);
	public Agency getDummyAgency();
	public List<Agency> getAgency(boolean showHidden);
}
