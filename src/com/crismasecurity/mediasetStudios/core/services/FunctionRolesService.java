package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.FunctionRoles;
import com.widee.base.hibernate.criteria.Finder;


/**
 * 
 * Class name: FunctionRolesService
 *
 * Description: Interfaccia FunctionRolesService 
 * 
 *
 * Company: 
 *
 * @author 	vincenzo.scaccia
 * @date 	04/jul/2018
 *
 */


public interface FunctionRolesService {
	
	//Spectator
	public List<FunctionRoles> getFunctionRoles();
	public FunctionRoles addFunctionRoles(FunctionRoles r) throws Exception;
	public FunctionRoles updateFunctionRoles(FunctionRoles r) throws Exception;
	public void deleteFunctionRoles(long id);
	public Finder<FunctionRoles> getFunctionRolesFinder();
	public FunctionRoles getFunctionRolesById(Long id);
	public List<FunctionRoles> getFunctionRolesLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getFunctionRolesLazyLoadingCount(Map<String,Object> filters);
	public String getFunctionRolesByName(String name);
}
