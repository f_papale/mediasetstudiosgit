package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.Studio;
import com.widee.base.hibernate.criteria.Finder;

public interface StudioService {
	public List<Studio> getStudio();
	public Studio addStudio(Studio r) throws Exception;
	public Studio updateStudio(Studio r) throws Exception;
	public void deleteStudio(long id);
	public Finder<Studio> getStudioFinder();
	public Studio getStudioById(Long id);
	public List<Studio> getStudioLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getStudioLazyLoadingCount(Map<String,Object> filters);

}
