package com.crismasecurity.mediasetStudios.core.services;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.Liberatoria;
import com.crismasecurity.mediasetStudios.core.entity.SpectatorEpisode;
import com.widee.base.hibernate.criteria.Finder;

public interface SpectatorEpisodeService {
	public List<SpectatorEpisode> getSpectatorEpisode();
	public SpectatorEpisode addSpectatorEpisode(SpectatorEpisode r) throws Exception;
	public SpectatorEpisode updateSpectatorEpisode(SpectatorEpisode r) throws Exception;
	public void deleteSpectatorEpisode(long id);
	public Finder<SpectatorEpisode> getSpectatorEpisodeFinder();
	public SpectatorEpisode getSpectatorEpisodeById(Long id);
	public List<SpectatorEpisode> getSpectatorEpisodeLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters, boolean distinct);
	public long getSpectatorEpisodeLazyLoadingCount(Map<String,Object> filters);
	public Liberatoria addLiberatoria(Liberatoria r) throws Exception;
	public Liberatoria updateLiberatoria(Liberatoria r) throws Exception;
	public SpectatorEpisode getMostRecentParticipation(SpectatorEpisode spectatorEpisode, Date beforeDateRef, int maxDays);
	public void deleteLiberatoria(long id) throws Exception;
	public void wipeLiberatoriaBySpectatorId(long idSpectator);

}
