package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.LocationApparatus;
import com.widee.base.hibernate.criteria.Finder;

public interface LocationApparatusService {
	public List<LocationApparatus> getLocationApparatus();
	public LocationApparatus addLocationApparatus(LocationApparatus r) throws Exception;
	public LocationApparatus updateLocationApparatus(LocationApparatus r) throws Exception;
	public void deleteLocationApparatus(long id);
	public Finder<LocationApparatus> getLocationApparatusFinder();
	public LocationApparatus getLocationApparatusById(Long id);
	public List<LocationApparatus> getLocationApparatusLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getLocationApparatusLazyLoadingCount(Map<String,Object> filters);
}
