package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.RoleMasterSlave;
import com.widee.base.hibernate.criteria.Finder;


/**
 * 
 * Class name: RoleMasterSlaveService
 *
 * Description: Interfaccia RoleMasterSlaveService 
 * 
 *
 * Company: 
 *
 * @author 	vincenzo.scaccia
 * @date 	04/jul/2018
 *
 */


public interface RoleMasterSlaveService {
	
	//Spectator
	public List<RoleMasterSlave> getRoleMasterSlave();
	public RoleMasterSlave addRoleMasterSlave(RoleMasterSlave r) throws Exception;
	public RoleMasterSlave updateRoleMasterSlave(RoleMasterSlave r) throws Exception;
	public void deleteRoleMasterSlave(long id);
	public Finder<RoleMasterSlave> getRoleMasterSlaveFinder();
	public RoleMasterSlave getRoleMasterSlaveById(Long id);
	public List<RoleMasterSlave> getRoleMasterSlaveLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getRoleMasterSlaveLazyLoadingCount(Map<String,Object> filters);
	public List<RoleMasterSlave> getRoleSlaveByIdRoleMaster(Long idMaster);
}
