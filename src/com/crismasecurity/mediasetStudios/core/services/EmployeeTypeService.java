package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.EmployeeType;
import com.widee.base.hibernate.criteria.Finder;

public interface EmployeeTypeService {
	public List<EmployeeType> getEmployeeType();
	public EmployeeType addEmployeeType(EmployeeType r) throws Exception;
	public EmployeeType updateEmployeeType(EmployeeType r) throws Exception;
	public void deleteEmployeeType(long id);
	public Finder<EmployeeType> getEmployeeTypeFinder();
	public EmployeeType getEmployeeTypeById(Long id);
	public List<EmployeeType> getEmployeeTypeLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getEmployeeTypeLazyLoadingCount(Map<String,Object> filters);

}
