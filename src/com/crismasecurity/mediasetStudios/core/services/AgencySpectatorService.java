package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.Agency;
import com.crismasecurity.mediasetStudios.core.entity.AgencySpectator;
import com.crismasecurity.mediasetStudios.core.entity.AgencySpectatorId;
import com.crismasecurity.mediasetStudios.core.entity.Spectator;
import com.widee.base.hibernate.criteria.Finder;

public interface AgencySpectatorService {
	public List<AgencySpectator> getAgencySpectator();
	public AgencySpectator addAgencySpectator(AgencySpectator r) throws Exception;
	public AgencySpectator updateAgencySpectator(AgencySpectator r) throws Exception;
	public void deleteAgencySpectator(AgencySpectatorId id);
	public Finder<AgencySpectator> getAgencySpectatorFinder();
	public AgencySpectator getAgencySpectatorById(Long id);
	public List<AgencySpectator> getAgencySpectatorLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getAgencySpectatorLazyLoadingCount(Map<String,Object> filters);
	public boolean removeAvailabilityUnwantedSpectators();
	public boolean isAgencySpectator(Long idAgency, Long idSpectator);
	public boolean isSpectatorInDummyAgency(Spectator spectator);

}
