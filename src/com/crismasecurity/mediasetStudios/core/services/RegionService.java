package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.Region;
import com.widee.base.hibernate.criteria.Finder;

public interface RegionService {
	public List<Region> getRegion();
	public Region addRegion(Region r) throws Exception;
	public Region updateRegion(Region r) throws Exception;
	public void deleteRegion(long id);
	public Finder<Region> getRegionFinder();
	public Region getRegionById(Long id);
	public List<Region> getRegionLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getRegionLazyLoadingCount(Map<String,Object> filters);

}
