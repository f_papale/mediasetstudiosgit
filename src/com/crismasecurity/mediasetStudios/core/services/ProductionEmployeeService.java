package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.ProductionEmployee;
import com.widee.base.hibernate.criteria.Finder;

public interface ProductionEmployeeService {
	public List<ProductionEmployee> getProductionEmployee();
	public ProductionEmployee addProductionEmployee(ProductionEmployee r) throws Exception;
	public ProductionEmployee updateProductionEmployee(ProductionEmployee r) throws Exception;
	public void deleteProductionEmployee(long id);
	public Finder<ProductionEmployee> getProductionEmployeeFinder();
	public ProductionEmployee getProductionEmployeeById(Long id);
	public List<ProductionEmployee> getProductionEmployeeLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getProductionEmployeeLazyLoadingCount(Map<String,Object> filters);
}
