package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.ProductionType;
import com.widee.base.hibernate.criteria.Finder;

public interface ProductionTypeService {
	public List<ProductionType> getProductionType();
	public ProductionType addProductionType(ProductionType r) throws Exception;
	public ProductionType updateProductionType(ProductionType r) throws Exception;
	public void deleteProductionType(long id);
	public Finder<ProductionType> getProductionTypeFinder();
	public ProductionType getProductionTypeById(Long id);
	public List<ProductionType> getProductionTypeLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getProductionTypeLazyLoadingCount(Map<String,Object> filters);

}
