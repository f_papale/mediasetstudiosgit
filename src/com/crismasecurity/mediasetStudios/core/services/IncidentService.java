package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.Incident;
import com.widee.base.hibernate.criteria.Finder;


/**
 * 
 * Class name: ApparatusService
 *
 * Description: Interfaccia ApparatusService 
 * 
 *
 * Company: 
 *
 * @author 	vincenzo.scaccia
 * @date 	04/jul/2018
 *
 */


public interface IncidentService {
	
	//Spectator
	public List<Incident> getIncident();
	public Incident addIncident(Incident r) throws Exception;
	public Incident updateIncident(Incident r) throws Exception;
	public void deleteIncident(long id);
	public Finder<Incident> getIncidentFinder();
	public Incident getIncidentById(Long id);
	public List<Incident> getIncidentLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getIncidentLazyLoadingCount(Map<String,Object> filters);
}
