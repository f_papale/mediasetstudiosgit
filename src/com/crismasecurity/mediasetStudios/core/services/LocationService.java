package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.Location;
import com.widee.base.hibernate.criteria.Finder;

public interface LocationService {
	
	public List<Location> getLocation();
	public Location addLocation(Location r) throws Exception;
	public Location updateLocation(Location r) throws Exception;
	public void deleteLocation(long id);
	public Finder<Location> getLocationFinder();
	public Location getLocationById(Long id);
	public List<Location> getLocationLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getLocationLazyLoadingCount(Map<String,Object> filters);
	
	public Location getLocationByName(String name);
}
