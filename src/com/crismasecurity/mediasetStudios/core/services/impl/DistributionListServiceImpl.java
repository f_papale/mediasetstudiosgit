package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.DistributionListDAO;
import com.crismasecurity.mediasetStudios.core.entity.DistributionList;
import com.crismasecurity.mediasetStudios.core.services.DistributionListService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

/**
 * @author Gio
 *
 */
@Service
@Transactional
public class DistributionListServiceImpl implements DistributionListService {


	@Autowired
	private DistributionListDAO DistributionListDAO;
	
	
	
	@PostConstruct
	public void init() {
	}

	@Override
	public List<DistributionList> getDistributionList() {
		return DistributionListDAO.getFinder().addOrderAsc("id").setDistinct().list();
	} 


	@Override
	public DistributionList addDistributionList(DistributionList r) throws Exception {
		return DistributionListDAO.create(r);
	}

	@Override
	public DistributionList updateDistributionList(DistributionList r) throws Exception {
		return DistributionListDAO.update(r);
	}

	@Override
	public void deleteDistributionList(long id) {
		DistributionListDAO.delete(id);
	}

	@Override
	public Finder<DistributionList> getDistributionListFinder() {
		return DistributionListDAO.getFinder();
	}

	@Override
	public DistributionList getDistributionListById(Long id) {
		
		DistributionList com = DistributionListDAO.getFinder().and("id").eq(id).setDistinct().result();
		return com;
	}

	@Override
	public List<DistributionList> getDistributionListLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return DistributionListDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
	}

	@Override
	public long getDistributionListLazyLoadingCount(Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return DistributionListDAO.sizeOfListWithField(filters, filtriInOr, null);
	}
}
