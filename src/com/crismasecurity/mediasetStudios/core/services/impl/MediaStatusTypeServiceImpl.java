package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.MediaStatusTypeDAO;
import com.crismasecurity.mediasetStudios.core.entity.MediaStatusType;
import com.crismasecurity.mediasetStudios.core.services.MediaStatusTypeService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

/**
 * @author Gio
 *
 */
@Service
@Transactional
public class MediaStatusTypeServiceImpl implements MediaStatusTypeService {


	@Autowired
	private MediaStatusTypeDAO mediaStatusTypeDAO;
	

	@PostConstruct
	public void init() {
	}

	
	@Override
	public List<MediaStatusType> getMediaStatusType() {
		return mediaStatusTypeDAO.getFinder().addOrderAsc("id").setDistinct().list();
	}


	@Override
	public MediaStatusType addMediaStatusType(MediaStatusType r) throws Exception {
		return mediaStatusTypeDAO.create(r);
	}


	@Override
	public MediaStatusType updateMediaStatusType(MediaStatusType r) throws Exception {
		return mediaStatusTypeDAO.update(r);
	}


	@Override
	public void deleteMediaStatusType(long id) {
		mediaStatusTypeDAO.delete(id);
	}


	@Override
	public Finder<MediaStatusType> getMediaStatusTypeFinder() {
		return mediaStatusTypeDAO.getFinder();
	}


	@Override
	public MediaStatusType getMediaStatusTypeById(Long id) {
		
		MediaStatusType com = mediaStatusTypeDAO.getFinder().and("id").eq(id).setDistinct().result();
		return com;
	}


	@Override
	public List<MediaStatusType> getMediaStatusTypeLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return mediaStatusTypeDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
	}

	@Override
	public long getMediaStatusTypeLazyLoadingCount(Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return mediaStatusTypeDAO.sizeOfListWithField(filters, filtriInOr, null);
	}


	@Override
	public MediaStatusType getMediaStatusTypeByIdStatus(Integer statusId) {
		return mediaStatusTypeDAO.getFinder().and("idStatus").eq(statusId).setDistinct().result();
	}

	
}
