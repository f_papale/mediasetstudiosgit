package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.TestDAO;
import com.crismasecurity.mediasetStudios.core.entity.Test;
import com.crismasecurity.mediasetStudios.core.services.TestService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

/**
 * @author Gio
 *
 */
@Service
@Transactional
public class TestServiceImpl implements TestService {


	@Autowired
	private TestDAO testDAO;
	
	
	
	@PostConstruct
	public void init() {
	}

	
	
	@Override
	public List<Test> getTests() {
		return testDAO.getFinder().addOrderAsc("test").setDistinct().list();
	}


	@Override
	public Test addTest(Test r) throws Exception {
		return testDAO.create(r);
	}


	@Override
	public Test updateTest(Test r) throws Exception {
		return testDAO.update(r);
	}


	@Override
	public void deleteTest(long id) {
		testDAO.delete(id);
	}


	@Override
	public Finder<Test> getTestFinder() {
		return testDAO.getFinder();
	}


	@Override
	public Test getTestById(Long id) {
		
		Test com = testDAO.getFinder().and("id").eq(id).setDistinct().result();
		return com;
	}


	@Override
	public List<Test> getTestLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return testDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
	}

	@Override
	public long getTestLazyLoadingCount(Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return testDAO.sizeOfListWithField(filters, filtriInOr, null);
	}

	
}
