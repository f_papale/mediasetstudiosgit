package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.ProductionCenterDAO;
import com.crismasecurity.mediasetStudios.core.entity.ProductionCenter;
import com.crismasecurity.mediasetStudios.core.services.ProductionCenterService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

/**
 * @author Gio
 *
 */
@Service
@Transactional
public class ProductionCenterServiceImpl implements ProductionCenterService {


	@Autowired
	private ProductionCenterDAO productionCenterDAO;
	
	
	
	@PostConstruct
	public void init() {
	}

	
	
	@Override
	public List<ProductionCenter> getProductionCenter() {
		return productionCenterDAO.getFinder().addOrderAsc("id").setDistinct().list();
	}


	@Override
	public ProductionCenter addProductionCenter(ProductionCenter r) throws Exception {
		return productionCenterDAO.create(r);
	}


	@Override
	public ProductionCenter updateProductionCenter(ProductionCenter r) throws Exception {
		return productionCenterDAO.update(r);
	}


	@Override
	public void deleteProductionCenter(long id) {
		productionCenterDAO.delete(id);
	}


	@Override
	public Finder<ProductionCenter> getProductionCenterFinder() {
		return productionCenterDAO.getFinder();
	}


	@Override
	public ProductionCenter getProductionCenterById(Long id) {
		
		ProductionCenter com = productionCenterDAO.getFinder().and("id").eq(id).setDistinct().result();
		return com;
	}


	@Override
	public List<ProductionCenter> getProductionCenterLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return productionCenterDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
	}

	@Override
	public long getProductionCenterLazyLoadingCount(Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return productionCenterDAO.sizeOfListWithField(filters, filtriInOr, null);
	}

	
}
