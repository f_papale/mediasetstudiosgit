package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.ApparatusTypeDAO;
import com.crismasecurity.mediasetStudios.core.entity.ApparatusType;
import com.crismasecurity.mediasetStudios.core.services.ApparatusTypeService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

/**
 * 
 * Class name: ApparatusTypeServiceImpl
 *
 * Description: Implementazione Servizio Spring ApparatusType
 * 
 *
 * Company: 
 *
 * @author 	vincenzo.scaccia
 * @date 	04/jul/2018
 *
 */

@Service
@Transactional
public class ApparatusTypeServiceImpl implements ApparatusTypeService{
	
	
	@Autowired
	private ApparatusTypeDAO apparatusTypeDAO;	
	
	
	@PostConstruct
	public void init() {
	}
	
	@Override
	public List<ApparatusType> getApparatusType() {
		return apparatusTypeDAO.getFinder().addOrderAsc("id").setDistinct().list();
	}

	@Override
	public ApparatusType addApparatusType(ApparatusType r) throws Exception {
		return apparatusTypeDAO.create(r);
	}

	@Override
	public ApparatusType updateApparatusType(ApparatusType r) throws Exception {
		return apparatusTypeDAO.update(r);
	}

	@Override
	public void deleteApparatusType(long id) {
		apparatusTypeDAO.delete(id);
	}

	@Override
	public Finder<ApparatusType> getApparatusTypeFinder() {
		return apparatusTypeDAO.getFinder();
	}

	@Override
	public ApparatusType getApparatusTypeById(Long id) {
		ApparatusType apType = apparatusTypeDAO.getFinder().and("id").eq(id).setDistinct().result();
		return apType;
	}

	@Override
	public List<ApparatusType> getApparatusTypeLazyLoading(int first, int pageSize, String sortField, int sortOrder,
			Map<String, Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return apparatusTypeDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
	}

	@Override
	public long getApparatusTypeLoadingCount(Map<String, Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return apparatusTypeDAO.sizeOfListWithField(filters, filtriInOr, null);
	}

}
