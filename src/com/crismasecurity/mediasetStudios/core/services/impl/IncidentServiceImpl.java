package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.IncidentDAO;
import com.crismasecurity.mediasetStudios.core.entity.Incident;
import com.crismasecurity.mediasetStudios.core.services.IncidentService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;


/**
 * 
 * Class name: ApparatusServiceImpl
 *
 * Description: Implementazione Servizio Spring Apparatus
 * 
 *
 * Company: 
 *
 * @author 	vincenzo.scaccia
 * @date 	04/jul/2018
 *
 */


@Service
@Transactional
public class IncidentServiceImpl implements IncidentService{

	
	@Autowired
	private IncidentDAO incidentDAO; 
	
	@PostConstruct
	public void init() {
	}
	
	@Override
	public List<Incident> getIncident() {
		return incidentDAO.getFinder().addOrderAsc("id").setDistinct().list();
	}

	@Override
	public Incident addIncident(Incident r) throws Exception {
		return incidentDAO.create(r);
	}

	@Override
	public Incident updateIncident(Incident r) throws Exception {
		return incidentDAO.update(r);
	}

	@Override
	public void deleteIncident(long id) {
		incidentDAO.delete(id);	
	}

	@Override
	public Finder<Incident> getIncidentFinder() {
		return incidentDAO.getFinder();
	}

	@Override
	public Incident getIncidentById(Long id) {
		Incident ic = incidentDAO.getFinder().and("id").eq(id).setDistinct().result();
		return ic;
	}

	@Override
	public List<Incident> getIncidentLazyLoading(int first, int pageSize, String sortField, int sortOrder,
			Map<String, Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return incidentDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
	}

	@Override
	public long getIncidentLazyLoadingCount(Map<String, Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return incidentDAO.sizeOfListWithField(filters, filtriInOr, null);
	}

}
