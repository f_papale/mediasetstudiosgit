

package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.ProductionEmployeeDAO;
import com.crismasecurity.mediasetStudios.core.entity.ProductionEmployee;
import com.crismasecurity.mediasetStudios.core.services.ProductionEmployeeService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

@Service
@Transactional
public class ProductionEmployeeServiceImpl implements ProductionEmployeeService {

	@Autowired
	private ProductionEmployeeDAO productionEmployeeDAO;	
	
	
	@PostConstruct
	public void init() {
	}
	
	@Override
	public List<ProductionEmployee> getProductionEmployee() {
		return productionEmployeeDAO.getFinder().addOrderAsc("id").setDistinct().list();
	}

	@Override
	public ProductionEmployee addProductionEmployee(ProductionEmployee r) throws Exception {
		return productionEmployeeDAO.create(r);
	}

	@Override
	public ProductionEmployee updateProductionEmployee(ProductionEmployee r) throws Exception {
		return productionEmployeeDAO.update(r);
	}

	@Override
	public void deleteProductionEmployee(long id) {
		productionEmployeeDAO.delete(id);
		
	}

	@Override
	public Finder<ProductionEmployee> getProductionEmployeeFinder() {
		return productionEmployeeDAO.getFinder();
	}

	@Override
	public ProductionEmployee getProductionEmployeeById(Long id) {
		ProductionEmployee sptype = productionEmployeeDAO.getFinder().and("id").eq(id).setDistinct().result();
		return sptype;
	}

	@Override
	public List<ProductionEmployee> getProductionEmployeeLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String, Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return productionEmployeeDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
	}

	@Override
	public long getProductionEmployeeLazyLoadingCount(Map<String, Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return productionEmployeeDAO.sizeOfListWithField(filters, filtriInOr, null);
	}
}
