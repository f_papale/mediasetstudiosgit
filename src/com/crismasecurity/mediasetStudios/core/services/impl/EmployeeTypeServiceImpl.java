package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.EmployeeTypeDAO;
import com.crismasecurity.mediasetStudios.core.entity.EmployeeType;
import com.crismasecurity.mediasetStudios.core.services.EmployeeTypeService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

/**
 * @author Gio
 *
 */
@Service
@Transactional
public class EmployeeTypeServiceImpl implements EmployeeTypeService {


	@Autowired
	private EmployeeTypeDAO employeeTypeDAO;	
	
	
	@PostConstruct
	public void init() {
	}


	@Override
	public List<EmployeeType> getEmployeeType() {
		return employeeTypeDAO.getFinder().addOrderAsc("id").setDistinct().list();
	}


	@Override
	public EmployeeType addEmployeeType(EmployeeType r) throws Exception {
		return employeeTypeDAO.create(r);
	}


	@Override
	public EmployeeType updateEmployeeType(EmployeeType r) throws Exception {
		return employeeTypeDAO.update(r);
	}


	@Override
	public void deleteEmployeeType(long id) {
		employeeTypeDAO.delete(id);
	}


	@Override
	public Finder<EmployeeType> getEmployeeTypeFinder() {
		return employeeTypeDAO.getFinder();
	}


	@Override
	public EmployeeType getEmployeeTypeById(Long id) {
		
		EmployeeType com = employeeTypeDAO.getFinder().and("id").eq(id).setDistinct().result();
		return com;
	}


	@Override
	public List<EmployeeType> getEmployeeTypeLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return employeeTypeDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
	}

	@Override
	public long getEmployeeTypeLazyLoadingCount(Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return employeeTypeDAO.sizeOfListWithField(filters, filtriInOr, null);
	}

	
}
