package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.BadgesTransitsDAO;
import com.crismasecurity.mediasetStudios.core.entity.BadgesTransits;
import com.crismasecurity.mediasetStudios.core.services.BadgesTransitsService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

/**
 * @author Gio
 *
 */
@Service
@Transactional
public class BadgesTransitsServiceImpl implements BadgesTransitsService {


	@Autowired
	private BadgesTransitsDAO badgesTransitsDAO;
	
	
	
	@PostConstruct
	public void init() {
	}

	@Override
	public List<BadgesTransits> getBadgesTransits() {
		return badgesTransitsDAO.getFinder().addOrderAsc("timeStamp").setDistinct().list();
	} 


	@Override
	public BadgesTransits addBadgesTransits(BadgesTransits r) throws Exception {
		return badgesTransitsDAO.create(r);
	}

	@Override
	public BadgesTransits updateBadgesTransits(BadgesTransits r) throws Exception {
		return badgesTransitsDAO.update(r);
	}

	@Override
	public void deleteBadgesTransits(long id) {
		badgesTransitsDAO.delete(id);
	}

	@Override
	public Finder<BadgesTransits> getBadgesTransitsFinder() {
		return badgesTransitsDAO.getFinder();
	}

	@Override
	public BadgesTransits getBadgesTransitsById(Long id) {
		
		BadgesTransits com = badgesTransitsDAO.getFinder().and("id").eq(id).setDistinct().result();
		return com;
	}

	@Override
	public List<BadgesTransits> getBadgesTransitsLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return badgesTransitsDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
	}

	@Override
	public long getBadgesTransitsLazyLoadingCount(Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return badgesTransitsDAO.sizeOfListWithField(filters, filtriInOr, null);
	}
}
