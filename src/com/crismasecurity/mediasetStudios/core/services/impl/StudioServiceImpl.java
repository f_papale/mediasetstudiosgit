package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.StudioDAO;
import com.crismasecurity.mediasetStudios.core.entity.Studio;
import com.crismasecurity.mediasetStudios.core.services.StudioService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

/**
 * @author Gio
 *
 */
@Service
@Transactional
public class StudioServiceImpl implements StudioService {

	@Autowired
	private StudioDAO studioDAO;
	

	@PostConstruct
	public void init() {
	}

	
	@Override
	public List<Studio> getStudio() {
		return studioDAO.getFinder().addOrderAsc("id").setDistinct().list();
	}


	@Override
	public Studio addStudio(Studio r) throws Exception {
		return studioDAO.create(r);
	}


	@Override
	public Studio updateStudio(Studio r) throws Exception {
		return studioDAO.update(r);
	}


	@Override
	public void deleteStudio(long id) {
		studioDAO.delete(id);
	}


	@Override
	public Finder<Studio> getStudioFinder() {
		return studioDAO.getFinder();
	}


	@Override
	public Studio getStudioById(Long id) {
		
		Studio com = studioDAO.getFinder().and("id").eq(id).setDistinct().result();
		return com;
	}


	@Override
	public List<Studio> getStudioLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return studioDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
	}

	@Override
	public long getStudioLazyLoadingCount(Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return studioDAO.sizeOfListWithField(filters, filtriInOr, null);
	}

	
}
