package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.FunctionRolesDAO;
import com.crismasecurity.mediasetStudios.core.entity.FunctionRoles;
import com.crismasecurity.mediasetStudios.core.services.FunctionRolesService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;


/**
 * 
 * Class name: FunctionRolesServiceImpl
 *
 * Description: Implementazione Servizio Spring FunctionRoles
 * 
 *
 * Company: 
 *
 * @author 	vincenzo.scaccia
 * @date 	04/jul/2018
 *
 */


@Service
@Transactional
public class FunctionRolesServiceImpl implements FunctionRolesService{

	
	@Autowired
	private FunctionRolesDAO FunctionRolesDAO;
	
	@PostConstruct
	public void init() {
	}
	
	@Override
	public List<FunctionRoles> getFunctionRoles() {
		return FunctionRolesDAO.getFinder().addOrderAsc("id").setDistinct().list();
	}

	@Override
	public FunctionRoles addFunctionRoles(FunctionRoles r) throws Exception {
		return FunctionRolesDAO.create(r);
	}

	@Override
	public FunctionRoles updateFunctionRoles(FunctionRoles r) throws Exception {
		return FunctionRolesDAO.update(r);
	}

	@Override
	public void deleteFunctionRoles(long id) {
		FunctionRolesDAO.delete(id);	
	}

	@Override
	public Finder<FunctionRoles> getFunctionRolesFinder() {
		return FunctionRolesDAO.getFinder();
	}

	@Override
	public FunctionRoles getFunctionRolesById(Long id) {
		FunctionRoles ap = FunctionRolesDAO.getFinder().and("id").eq(id).setDistinct().result();
		return ap;
	}
	
	@Override
	public String getFunctionRolesByName(String name) {
		FunctionRoles ap = FunctionRolesDAO.getFinder().and("name").eq(name).setDistinct().result();
		return ap.getRoles();
	}

	@Override
	public List<FunctionRoles> getFunctionRolesLazyLoading(int first, int pageSize, String sortField, int sortOrder,
			Map<String, Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return FunctionRolesDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
	}

	@Override
	public long getFunctionRolesLazyLoadingCount(Map<String, Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return FunctionRolesDAO.sizeOfListWithField(filters, filtriInOr, null);
	}

}
