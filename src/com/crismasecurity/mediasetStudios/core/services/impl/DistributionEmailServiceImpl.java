package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.DistributionEmailDAO;
import com.crismasecurity.mediasetStudios.core.entity.DistributionEmail;
import com.crismasecurity.mediasetStudios.core.entity.DistributionEmailId;
import com.crismasecurity.mediasetStudios.core.services.DistributionEmailService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

/**
 * @author Gio
 *
 */
@Service
@Transactional
public class DistributionEmailServiceImpl implements DistributionEmailService {


	@Autowired
	private DistributionEmailDAO distributionEmailDAO;
	
	
	
	@PostConstruct
	public void init() {
	}


	@Override
	public List<DistributionEmail> getDistributionEmail() {
		return distributionEmailDAO.getFinder().addOrderAsc("idDistributionEmail").addOrderAsc("idEmailContact").setDistinct().list();
	}


	@Override
	public DistributionEmail addDistributionEmail(DistributionEmail r) throws Exception {
		return distributionEmailDAO.create(r);
	}


	@Override
	public DistributionEmail updateDistributionEmail(DistributionEmail r) throws Exception {
		return distributionEmailDAO.update(r);
	}

	@Override
	public void deleteDistributionEmail(DistributionEmailId id) {
		distributionEmailDAO.delete(id);
	}
	
	@Override
	public void deleteDistributionEmailByIdDL(Long id) {
		
		DistributionEmailId  idl; 
		List<DistributionEmail> distributionEmails = distributionEmailDAO.getFinder().and("distributionList.id").eq(id).setDistinct().list(); ;
		for (DistributionEmail distributionEmail : distributionEmails) {
			idl= new DistributionEmailId(distributionEmail.getId().getidDistributionList(),distributionEmail.getId().getidEmailContact());
			distributionEmailDAO.delete(idl);
		}
	}
	
	@Override
	public void deleteDistributionEmailByIdEC(Long id) {
		
		DistributionEmailId  idl; 
		List<DistributionEmail> distributionEmails = distributionEmailDAO.getFinder().and("emailContact.id").eq(id).setDistinct().list(); ;
		for (DistributionEmail distributionEmail : distributionEmails) {
			idl= new DistributionEmailId(distributionEmail.getId().getidDistributionList(),distributionEmail.getId().getidEmailContact());
			distributionEmailDAO.delete(idl);
		}
	}

	@Override
	public Finder<DistributionEmail> getDistributionEmailFinder() {
		return distributionEmailDAO.getFinder();
	}

//	@Override
//	public DistributionEmail getDistributionEmailById(Long id) {
//		
//		DistributionEmail com = distributionEmailDAO.getFinder().and("id").eq(id).setDistinct().result();
//		return com;
//	}
	
	@Override
	public DistributionEmail getDistributionEmailById(DistributionEmailId id) {
		
		DistributionEmail com = distributionEmailDAO.getFinder().and("distributionList.id").eq(id.getidDistributionList()).and("idEmailContact").eq(id.getidEmailContact()).setDistinct().result();
		return com;
	}	

	@Override
	public List<DistributionEmail> getDistributionEmailLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
//		Nell'ultimo parametro mettere i seguenti valori a seconda che si voglia effettuare la DISTINCY o meno. 
//		Questo risulta necessario in quei casi in cui la tabella visualizzata non effettua l'ordinamento per colonna 
//		e da la un errore sulla presenza della DISTINCT
//		false = non mette il distinct
//		true = mette il distinct
		return distributionEmailDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, false);
	}

	@Override
	public long getDistributionEmailLazyLoadingCount(Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return distributionEmailDAO.sizeOfListWithField(filters, filtriInOr, null);
	}
	
//	@Override
//	public List<EmailContact> getEmailContactFromDistributionEmail(String distributionList) {
//		List<EmailContact> emailList = new ArrayList<EmailContact>();
//		List<DistributionEmail> lst = distributionEmailDAO.getFinder().and("distributionList.name").eq(distributionList.trim()).setDistinct().list();
//		lst.size();
//		
//		if (lst!=null) 
//			lst.forEach(s -> {
//				emailList.add(s.getEmailContact());
//			});
//		return emailList;
//	}
	
	@Override
	public String getEmailContactFromDistributionEmail(String distributionList) {
		String recipients="", sep="";
		List<DistributionEmail> lst = distributionEmailDAO.getFinder().and("distributionList.name").eq(distributionList.trim()).setDistinct().list();
		lst.size();
		if (!lst.equals("")) 
			for (DistributionEmail s: lst) {
				recipients=recipients+sep+s.getEmailContact().getManagerEmail();
	    		sep=";";
			}
		return recipients;
	}
}
