package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.BlackListTypeDAO;
import com.crismasecurity.mediasetStudios.core.entity.BlackListType;
import com.crismasecurity.mediasetStudios.core.services.BlackListTypeService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

/**
 * @author fra
 *
 */
@Service
@Transactional
public class BlackListTypeServiceImpl implements BlackListTypeService {


	@Autowired
	private BlackListTypeDAO blackListTypeDAO;
	
	
	
	@PostConstruct
	public void init() {
	}



	@Override
	public List<BlackListType> getBlackListType() {
		return blackListTypeDAO.getFinder().addOrderAsc("id").setDistinct().list();
	}



	@Override
	public BlackListType addBlackListType(BlackListType r) throws Exception {
		return blackListTypeDAO.create(r);
	}



	@Override
	public BlackListType updateBlackListType(BlackListType r) throws Exception {
		return blackListTypeDAO.update(r);
	}



	@Override
	public void deleteBlackListType(long id) {
		blackListTypeDAO.delete(id);
	}



	@Override
	public Finder<BlackListType> getBlackListTypeFinder() {
		return blackListTypeDAO.getFinder();
	}



	@Override
	public BlackListType getBlackListTypeById(Long id) {
		BlackListType com = blackListTypeDAO.getFinder().and("id").eq(id).setDistinct().result();
		return com;
	}



	@Override
	public List<BlackListType> getBlackListTypeLazyLoading(int first, int pageSize, String sortField, int sortOrder,
			Map<String, Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return blackListTypeDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
	}



	@Override
	public long getBlackListLoadingCount(Map<String, Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return blackListTypeDAO.sizeOfListWithField(filters, filtriInOr, null);
	}



	@Override
	public BlackListType getBlackListTypeByLevel(String level) {
		BlackListType com = blackListTypeDAO.getFinder().and("description").eq(level).setDistinct().result();
		if(com==null) com = 
				this.getBlackListTypeByIdType(2); 
		return com;
	}
	
	@Override
	public BlackListType getBlackListTypeByIdType(int idType) {
		BlackListType com = blackListTypeDAO.getFinder().and("idType").eq(idType).setDistinct().result();
		if(com==null) com = 
				this.getBlackListTypeById(2L); 
		return com;
	}

	
	
	

	
}
