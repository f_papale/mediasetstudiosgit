package com.crismasecurity.mediasetStudios.core.services.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.AgencyDAO;
import com.crismasecurity.mediasetStudios.core.dao.SpectatorDAO;
import com.crismasecurity.mediasetStudios.core.entity.Agency;
import com.crismasecurity.mediasetStudios.core.entity.AgencySpectator;
import com.crismasecurity.mediasetStudios.core.entity.Spectator;
import com.crismasecurity.mediasetStudios.core.services.SpectatorService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

@Service
@Transactional
public class SpectatorServiceImpl implements SpectatorService{

	@Autowired
	private SpectatorDAO spectatorDAO;
	
	@Autowired 
	private AgencyDAO agencyDAO;
	
	@PostConstruct
	public void init() {
	}

	@Override
	public List<Spectator> getSpectator() {
		return spectatorDAO.getFinder().addOrderAsc("id").setDistinct().list();
	}

	@Override
	public Spectator addSpectator(Spectator r) throws Exception {
		return spectatorDAO.create(r);
	}

	@Override
	public Spectator updateSpectator(Spectator r) throws Exception {
		return spectatorDAO.update(r);
	}
	
//	@Override
//	public Spectator updateSpectatorForReview(Spectator r) throws Exception {
//		List<SpectatorEpisode> ep spectatorEpisodeDAO.getFinder().and("idSpectator").eq(r.getId()).setDistinct().result()).;
//		return spectatorDAO.update(r);
//	}
	

	@Override
	public void deleteSpectator(long id) {
			spectatorDAO.delete(id);		
	}

	@Override
	public Finder<Spectator> getSpectatorFinder() {
		return spectatorDAO.getFinder();
	}

	@Override
	public Spectator getSpectatorById(Long id) {
		Spectator sp = spectatorDAO.getFinder().and("id").eq(id).setDistinct().result();
		return sp;
	}

	@Override
	public List<Spectator> getSpectatorLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String, Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return spectatorDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
	}

	@Override
	public long getSpectatorLazyLoadingCount(Map<String, Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return spectatorDAO.sizeOfListWithField(filters, filtriInOr, null);
	}

	@Override
	public List<BigDecimal> getSpectatorsID2Wipe(int retainlastmonths) {
		List<BigDecimal> result = spectatorDAO.getSpectator2Wipe(retainlastmonths);
		return result;
	}
	
	
	@Override 
	public boolean  removeDuplicatedSpectators() {
		boolean result = spectatorDAO.removeDuplicatedSpectators();
		return result;
	}
	
	@Override
	public Spectator getSpectatorFromArchive(Spectator spectator) {
		 boolean checkIfExists = false;							
//		 Controlliamo ora se esiste già uno spettatore con lo stesso codice fiscale nell'anagrafica degli spettatori
//		 se questo spettatore esiste già nell'anagrafica degli spettatori.
//		 Controllo se lo spettatore esiste già per Codice Fiscale	
		 if (spectator.getId()!=null) {
			 checkIfExists = !(spectatorDAO.getFinder().and("id").eq(spectator.getId()).list().isEmpty());
			 if (checkIfExists)
				 spectator = spectatorDAO.getFinder().and("id").eq(spectator.getId()).result();
		 }else if (!(spectator.getFiscalCode()==null || spectator.getFiscalCode().isEmpty())) {
			 checkIfExists = !(spectatorDAO.getFinder().and("fiscalCode").eq(spectator.getFiscalCode()).list().isEmpty());
			 if (checkIfExists)
				 spectator = spectatorDAO.getFinder().and("fiscalCode").eq(spectator.getFiscalCode()).result();
		 }else if (!(spectator.getSurname().isEmpty() || spectator.getName().isEmpty() || spectator.getBirthDate()==null)){
	//			 Controllo per Nome e Cognome e Data di Nascita
			 Finder<Spectator> spfi = spectatorDAO.getFinder();
			 spfi.and("name").eq(spectator.getName());
			 spfi.and("surname").eq(spectator.getSurname());
			 spfi.and("birthDate").eq(spectator.getBirthDate());
			 checkIfExists=(spfi.count()>0);
			 if (checkIfExists) 
				 try {
					 spectator = spectatorDAO.getFinder().and("name").eq(spectator.getName()).and("surname").eq(spectator.getSurname()).and("birthDate").eq(spectator.getBirthDate()).result();
				} catch (Exception e) {
					System.out.println("Duplicato :" +spectator.getFullName());
					spectator=spectatorDAO.getFinder().and("name").eq(spectator.getName()).and("surname").eq(spectator.getSurname()).and("birthDate").eq(spectator.getBirthDate()).list().get(0);
				} 	 				 
		 }else if (!(spectator.getSurname().isEmpty() || spectator.getName().isEmpty())){ 
	//			 Controllo per Nome e Cognome
			 Finder<Spectator> spfi = spectatorDAO.getFinder();
			 spfi.and("name").eq(spectator.getName());
			 spfi.and("surname").eq(spectator.getSurname());
//			 spfi.and("birthDate").eq(null);
			 checkIfExists=(spfi.count()>0);
			 if (checkIfExists) {
				 List<Spectator> ls = spectatorDAO.getFinder().and("name").eq(spectator.getName()).and("surname").eq(spectator.getSurname()).list();
				 if (ls.size()>0)
					 spectator = ls.get(0); 

//				 spectator = spectatorDAO.getFinder().and("name").eq(spectator.getName()).and("surname").eq(spectator.getSurname()).and("birthDate").eq(null).result();				 
			 }

		 } else {
			 checkIfExists=false;
		 }
		 if (spectator.getAgencies()!=null)
			 spectator.getAgencies().size();
			 
		 return spectator;
	}
	
	
	
	
	@Override
	public void addAgencySpectator2Dummy(Spectator spectator) throws Exception {
		Agency agencyDummy = null;
		try {
			agencyDummy = agencyDAO.getFinder().and("hidden").eq(true).setDistinct().result();
		} catch (Exception e) {
			agencyDummy=null;
		}
		if (agencyDummy !=null) {
			AgencySpectator agsp = new AgencySpectator(agencyDummy, spectator); 
			List<AgencySpectator> agencySpectators = spectator.getAgencies();
			if ((agencySpectators==null)||(agencySpectators.isEmpty())) 
				agencySpectators = new ArrayList<AgencySpectator>();
			agencySpectators.add(agsp);
			spectator.setAgencies(agencySpectators);
			spectatorDAO.update(spectator)		;	
		}
	}
	
	@Override
	public Spectator addAgencySpectator2Dummy(Long spectator) {
		Agency agencyDummy = null;
		Spectator oSpectator  = null;
		try {
			agencyDummy = agencyDAO.getFinder().and("hidden").eq(true).setDistinct().result();
		} catch (Exception e) {
			agencyDummy=null;
		}
		if (agencyDummy !=null) {
			 oSpectator = getSpectatorById(spectator);
			AgencySpectator agsp = new AgencySpectator(agencyDummy, oSpectator); 
			
			if ((oSpectator.getAgencies()==null)) 
				oSpectator.setAgencies(new ArrayList<AgencySpectator>());
			oSpectator.getAgencies().add(agsp);
			try {
				oSpectator= spectatorDAO.update(oSpectator);	
			} catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
			}
				
		}
		return oSpectator;
	}
}
