package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.AgencyTypeDAO;
import com.crismasecurity.mediasetStudios.core.entity.AgencyType;
import com.crismasecurity.mediasetStudios.core.services.AgencyTypeService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

/**
 * @author Gio
 *
 */
@Service
@Transactional
public class AgencyTypeServiceImpl implements AgencyTypeService {


	@Autowired
	private AgencyTypeDAO agencyTypeDAO;
	
	
	
	@PostConstruct
	public void init() {
	}

	@Override
	public List<AgencyType> getAgencyType() {
		return agencyTypeDAO.getFinder().addOrderAsc("id").setDistinct().list();
	}


	@Override
	public AgencyType addAgencyType(AgencyType r) throws Exception {
		return agencyTypeDAO.create(r);
	}


	@Override
	public AgencyType updateAgencyType(AgencyType r) throws Exception {
		return agencyTypeDAO.update(r);
	}


	@Override
	public void deleteAgencyType(long id) {
		agencyTypeDAO.delete(id);
	}


	@Override
	public Finder<AgencyType> getAgencyTypeFinder() {
		return agencyTypeDAO.getFinder();
	}


	@Override
	public AgencyType getAgencyTypeById(Long id) {
		
		AgencyType com = agencyTypeDAO.getFinder().and("id").eq(id).setDistinct().result();
		return com;
	}


	@Override
	public List<AgencyType> getAgencyTypeLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return agencyTypeDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
	}

	@Override
	public long getAgencyTypeLazyLoadingCount(Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return agencyTypeDAO.sizeOfListWithField(filters, filtriInOr, null);
	}

	
}
