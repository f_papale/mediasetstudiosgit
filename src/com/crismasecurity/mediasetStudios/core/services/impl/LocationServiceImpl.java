package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.LocationDAO;
import com.crismasecurity.mediasetStudios.core.entity.Location;
import com.crismasecurity.mediasetStudios.core.services.LocationService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

/**
 * 
 * Class name: ApparatusServiceImpl
 *
 * Description: Implementazione Servizio Spring Varco
 * 
 *
 * Company: 
 *
 * @author 	vincenzo.scaccia
 * @date 	04/jul/2018
 *
 */

@Service
@Transactional
public class LocationServiceImpl implements LocationService{
	
	
	@Autowired
	private LocationDAO locationDAO;
	
	
	@PostConstruct
	public void init() {
	}
	
	
	@Override
	public List<Location> getLocation() {
		return locationDAO.getFinder().addOrderAsc("id").setDistinct().list();
	}

	@Override
	public Location addLocation(Location r) throws Exception {
		return locationDAO.create(r);
	}

	@Override
	public Location updateLocation(Location r) throws Exception {
		return locationDAO.update(r);
	}

	@Override
	public void deleteLocation(long id) {
		locationDAO.delete(id);	
	}

	@Override
	public Finder<Location> getLocationFinder() {
		return locationDAO.getFinder();
	}

	@Override
	public Location getLocationById(Long id) {
		Location ap =null;
		try {
			ap = locationDAO.getFinder().and("id").eq(id).setDistinct().result();	
			if (ap!=null && ap.getProductionCenter()!=null) ap.getProductionCenter().toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ap;
	}

	@Override
	public List<Location> getLocationLazyLoading(int first, int pageSize, String sortField, int sortOrder,
			Map<String, Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		
		List<Location> tempList = locationDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
		System.out.println("Numero elementi: " + tempList.size());
		return tempList;
	}

	@Override
	public long getLocationLazyLoadingCount(Map<String, Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return locationDAO.sizeOfListWithField(filters, filtriInOr, null);
	}


	@Override
	public Location getLocationByName(String name) {
		return locationDAO.getFinder().and("name").eq(name).setDistinct().result();
	}

}
