package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.SpectatorEpisodeDAO;
import com.crismasecurity.mediasetStudios.core.entity.Spectator;
import com.crismasecurity.mediasetStudios.core.entity.SpectatorEpisode;
import com.crismasecurity.mediasetStudios.core.services.ReceptionService;
import com.widee.base.hibernate.criteria.Finder;

/**
 * @author Gio
 *
 */
@Service
@Transactional
public class ReceptionServiceImpl implements ReceptionService {


//	@Autowired
//	private CheckinDeskDAO checkinDeskDAO;
	
	@Autowired
	private SpectatorEpisodeDAO spectatorEpisodeDAO;
	
	
	
	@PostConstruct
	public void init() {
	}




//	@Override
//	public List<CheckinDesk> getCheckinDesks() {
//		return checkinDeskDAO.getAll();
//	}
//	
//	
//	
//	@Override
//	public CheckinDesk addCheckinDesk(CheckinDesk r) throws Exception {
//		return checkinDeskDAO.create(r);
//	}
//	
//	
//	
//	@Override
//	public CheckinDesk updateCheckinDesk(CheckinDesk r) throws Exception {
//		return checkinDeskDAO.update(r);
//	}
//	
//	
//	
//	@Override
//	public void deleteCheckinDesk(long id) {
//		checkinDeskDAO.delete(id);
//	}
//	
//	
//	
//	@Override
//	public Finder<CheckinDesk> getCheckinDeskFinder() {
//		return checkinDeskDAO.getFinder();
//	}
//	
//	
//	
//	@Override
//	public CheckinDesk getCheckinDeskById(Long id) {
//		CheckinDesk com = checkinDeskDAO.getFinder().and("id").eq(id).setDistinct().result();
//		return com;
//	}
//



	@Override
	public List<SpectatorEpisode> getSpectatorsEpisode() {
		return spectatorEpisodeDAO.getAll();
	}




	@Override
	public SpectatorEpisode addSpectatorEpisode(SpectatorEpisode r) throws Exception {
		return spectatorEpisodeDAO.create(r);
	}




	@Override
	public SpectatorEpisode updateSpectatorEpisode(SpectatorEpisode r) throws Exception {
		return spectatorEpisodeDAO.update(r);
	}

	@Override
	public SpectatorEpisode updateSpectatorEpisodeNoLazy(SpectatorEpisode r) throws Exception {
		SpectatorEpisode ss = spectatorEpisodeDAO.update(r);
		if (ss.getLiberatoria()!=null) ss.getLiberatoria().getPdfSignBase64Str();
		return ss;
	}


	@Override
	public void deleteSpectatorEpisode(long id) {
		spectatorEpisodeDAO.delete(id);
	}




	@Override
	public Finder<SpectatorEpisode> getSpectatorEpisodeFinder() {
		return spectatorEpisodeDAO.getFinder();
	}




	@Override
	public SpectatorEpisode getSpectatorEpisodeById(Long id) {
		return spectatorEpisodeDAO.getFinder().and("id").eq(id).setDistinct().result();
	}
	@Override
	public SpectatorEpisode getSpectatorEpisodeByIdNoLazy(Long id) {
		SpectatorEpisode ss = spectatorEpisodeDAO.getFinder().and("id").eq(id).setDistinct().result();
		if (ss.getLiberatoria()!=null) ss.getLiberatoria().getPdfSignBase64Str();
		return ss;
	}
	
}
