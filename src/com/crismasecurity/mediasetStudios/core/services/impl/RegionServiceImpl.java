package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.RegionDAO;
import com.crismasecurity.mediasetStudios.core.entity.Region;
import com.crismasecurity.mediasetStudios.core.services.RegionService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

/**
 * @author fra
 *
 */
@Service
@Transactional
public class RegionServiceImpl implements RegionService {


	@Autowired
	private RegionDAO regionDAO;
	
	
	
	@PostConstruct
	public void init() {
	}

	
	
	@Override
	public List<Region> getRegion() {
		return regionDAO.getFinder().addOrderAsc("id").setDistinct().list();
	}


	@Override
	public Region addRegion(Region r) throws Exception {
		return regionDAO.create(r);
	}


	@Override
	public Region updateRegion(Region r) throws Exception {
		return regionDAO.update(r);
	}


	@Override
	public void deleteRegion(long id) {
		regionDAO.delete(id);
	}


	@Override
	public Finder<Region> getRegionFinder() {
		return regionDAO.getFinder();
	}


	@Override
	public Region getRegionById(Long id) {
		
		Region com = regionDAO.getFinder().and("id").eq(id).setDistinct().result();
		return com;
	}


	@Override
	public List<Region> getRegionLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return regionDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
	}

	@Override
	public long getRegionLazyLoadingCount(Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return regionDAO.sizeOfListWithField(filters, filtriInOr, null);
	}

	
}
