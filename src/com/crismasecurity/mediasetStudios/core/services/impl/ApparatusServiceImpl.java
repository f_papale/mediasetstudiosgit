package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.ApparatusDAO;
import com.crismasecurity.mediasetStudios.core.entity.Apparatus;
import com.crismasecurity.mediasetStudios.core.services.ApparatusService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;


/**
 * 
 * Class name: ApparatusServiceImpl
 *
 * Description: Implementazione Servizio Spring Apparatus
 * 
 *
 * Company: 
 *
 * @author 	vincenzo.scaccia
 * @date 	04/jul/2018
 *
 */


@Service
@Transactional
public class ApparatusServiceImpl implements ApparatusService{

	
	@Autowired
	private ApparatusDAO apparatusDAO;
	
	@PostConstruct
	public void init() {
	}
	
	@Override
	public List<Apparatus> getApparatus() {
		return apparatusDAO.getFinder().addOrderAsc("id").setDistinct().list();
	}

	@Override
	public Apparatus addApparatus(Apparatus r) throws Exception {
		return apparatusDAO.create(r);
	}

	@Override
	public Apparatus updateApparatus(Apparatus r) throws Exception {
		return apparatusDAO.update(r);
	}

	@Override
	public void deleteApparatus(long id) {
		apparatusDAO.delete(id);	
	}

	@Override
	public Finder<Apparatus> getApparatusFinder() {
		return apparatusDAO.getFinder();
	}

	@Override
	public Apparatus getApparatusById(Long id) {
		Apparatus ap = apparatusDAO.getFinder().and("id").eq(id).setDistinct().result();
		return ap;
	}

	@Override
	public List<Apparatus> getApparatusLazyLoading(int first, int pageSize, String sortField, int sortOrder,
			Map<String, Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return apparatusDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
	}

	@Override
	public long getApparatusLazyLoadingCount(Map<String, Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return apparatusDAO.sizeOfListWithField(filters, filtriInOr, null);
	}

}
