package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.ProductionDAO;
import com.crismasecurity.mediasetStudios.core.entity.Production;
import com.crismasecurity.mediasetStudios.core.services.ProductionService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

/**
 * @author Gio
 *
 */
@Service
@Transactional
public class ProductionServiceImpl implements ProductionService {

	@Autowired
	private ProductionDAO productionDAO;

	@PostConstruct
	public void init() {
	}
	
	@Override
	public List<Production> getProduction() {
		return productionDAO.getFinder().addOrderAsc("id").setDistinct().list();
	}


	@Override
	public Production addProduction(Production r) throws Exception {
		return productionDAO.create(r);
	}


	@Override
	public Production updateProduction(Production r) throws Exception {
		return productionDAO.update(r);
	}


	@Override
	public void deleteProduction(long id) {
		productionDAO.delete(id);
	}

	@Override
	public Finder<Production> getProductionFinder() {
		return productionDAO.getFinder();
	}

	@Override
	public Production getProductionById(Long id) {
		
		Production com = productionDAO.getFinder().and("id").eq(id).setDistinct().result();
		if (com.getProductionEmployee()!=null) com.getProductionEmployee().size();
		if (com.getEpisodes()!=null) com.getEpisodes().size();
		return com;
	}
	
	@Override
	public List<Production> getProductionLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return productionDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
	}

	@Override
	public long getProductionLazyLoadingCount(Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return productionDAO.sizeOfListWithField(filters, filtriInOr, null);
	}
	
}
