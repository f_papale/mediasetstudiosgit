package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.EpisodeStatusTypeDAO;
import com.crismasecurity.mediasetStudios.core.entity.EpisodeStatusType;
import com.crismasecurity.mediasetStudios.core.services.EpisodeStatusTypeService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

/**
 * @author Gio
 *
 */
@Service
@Transactional
public class EpisodeStatusTypeServiceImpl implements EpisodeStatusTypeService {

	@Autowired
	private EpisodeStatusTypeDAO episodeStatusTypeDAO;
	

	@PostConstruct
	public void init() {
	}

	
	@Override
	public List<EpisodeStatusType> getEpisodeStatusType() {
		return episodeStatusTypeDAO.getFinder().addOrderAsc("id").setDistinct().list();
	}


	@Override
	public EpisodeStatusType addEpisodeStatusType(EpisodeStatusType r) throws Exception {
		return episodeStatusTypeDAO.create(r);
	}


	@Override
	public EpisodeStatusType updateEpisodeStatusType(EpisodeStatusType r) throws Exception {
		return episodeStatusTypeDAO.update(r);
	}


	@Override
	public void deleteEpisodeStatusType(long id) {
		episodeStatusTypeDAO.delete(id);
	}


	@Override
	public Finder<EpisodeStatusType> getEpisodeStatusTypeFinder() {
		return episodeStatusTypeDAO.getFinder();
	}


	@Override
	public EpisodeStatusType getEpisodeStatusTypeById(Long id) {
		
		EpisodeStatusType com = episodeStatusTypeDAO.getFinder().and("id").eq(id).setDistinct().result();
		return com;
	}


	@Override
	public List<EpisodeStatusType> getEpisodeStatusTypeLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return episodeStatusTypeDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
	}

	@Override
	public long getEpisodeStatusTypeLazyLoadingCount(Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return episodeStatusTypeDAO.sizeOfListWithField(filters, filtriInOr, null);
	}


	@Override
	public EpisodeStatusType getEpisodeStatusTypeByIdStatus(int idStatus) {
		EpisodeStatusType com = episodeStatusTypeDAO.getFinder().and("idStatus").eq(idStatus).setDistinct().result();
		return com;
	}


	@Override
	public List<EpisodeStatusType> getEpisodeStatusTypeOrdered() {
		List<EpisodeStatusType> com = episodeStatusTypeDAO.getFinder().and("owner").eq("A").addOrderAsc("orderBy").list();
		return com;
	}

	
}
