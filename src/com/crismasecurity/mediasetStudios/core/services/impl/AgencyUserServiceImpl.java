package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.AgencyUserDAO;
import com.crismasecurity.mediasetStudios.core.entity.AgencyUser;
import com.crismasecurity.mediasetStudios.core.entity.AgencyUserId;
import com.crismasecurity.mediasetStudios.core.services.AgencyUserService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

/**
 * @author Gio
 *
 */
@Service
@Transactional
public class AgencyUserServiceImpl implements AgencyUserService {


	@Autowired
	private AgencyUserDAO agencyUserDAO;
	

	@PostConstruct
	public void init() {
	}

	
	@Override
	public List<AgencyUser> getAgencyUser() {
		return agencyUserDAO.getFinder().addOrderAsc("idAgencyUser").setDistinct().list();
	}


	@Override
	public AgencyUser addAgencyUser(AgencyUser r) throws Exception {
		return agencyUserDAO.create(r);
	}


	@Override
	public AgencyUser updateAgencyUser(AgencyUser r) throws Exception {
		return agencyUserDAO.update(r);
	}


	@Override
	public void deleteAgencyUser(AgencyUserId id) {
		agencyUserDAO.delete(id);
	}


	@Override
	public Finder<AgencyUser> getAgencyUserFinder() {
		return agencyUserDAO.getFinder();
	}


	@Override
	public AgencyUser getAgencyUserById(AgencyUserId id) {
		
		AgencyUser com = agencyUserDAO.getFinder().and("user.id").eq(id.getIdUser()).setDistinct().result();
		return com;
	}
	
	@Override
	public AgencyUser getAgencyUserByIdString(String idString) {
		
		String[] ids = idString.split("-");
		AgencyUser com = agencyUserDAO.getFinder().and("agency.id").eq(Long.parseLong(ids[0])).and("user.id").eq(Long.parseLong(ids[1])).setDistinct().result();
		return com;
	}	



	@Override
	public List<AgencyUser> getAgencyUserLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return agencyUserDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
	}

	@Override
	public long getAgencyUserLazyLoadingCount(Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return agencyUserDAO.sizeOfListWithField(filters, filtriInOr, null);
	}


	@Override
	public void presetAgencyUser(long userId) {
		// TODO Auto-generated method stub
		boolean flgCurrentSelected = true;
		List<AgencyUser> ls = agencyUserDAO.getFinder().and("user.id").eq(userId).setDistinct().list();
		for (AgencyUser agencyUser : ls) {
			try {
				agencyUser.setFlgCurrentSelected(flgCurrentSelected);
				updateAgencyUser(agencyUser);
				flgCurrentSelected=false;
			} catch (Exception e) {
				// TODO: handle exception
			}
		} 
		
	}
}
