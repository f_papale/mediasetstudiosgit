package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.AgencyDAO;
import com.crismasecurity.mediasetStudios.core.entity.Agency;
import com.crismasecurity.mediasetStudios.core.entity.AgencySpectator;
import com.crismasecurity.mediasetStudios.core.services.AgencyService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

/**
 * @author Gio
 *
 */
@Service
@Transactional
public class AgencyServiceImpl implements AgencyService {


	@Autowired
	private AgencyDAO agencyDAO;
	
	
	
	@PostConstruct
	public void init() {
	}

	@Override
	public List<Agency> getAgency() {
		return agencyDAO.getFinder().and("hidden").eq(false).addOrderAsc("idAgency").setDistinct().list();
	} 

	@Override
	public List<Agency> getAgency(boolean showHidden) {
		if (!showHidden)
			return agencyDAO.getFinder().and("hidden").eq(false).addOrderAsc("idAgency").setDistinct().list();
		else
			return agencyDAO.getFinder().addOrderAsc("idAgency").setDistinct().list();
	} 

	@Override
	public Agency addAgency(Agency r) throws Exception {
		return agencyDAO.create(r);
	}

	@Override
	public Agency updateAgency(Agency r) throws Exception {
		return agencyDAO.update(r);
	}

	@Override
	public void deleteAgency(long id) {
		agencyDAO.delete(id);
	}

	@Override
	public Finder<Agency> getAgencyFinder() {
//		return agencyDAO.getFinder();
		return agencyDAO.getFinder().and("hidden").eq(false);
	}

	@Override
	public Agency getAgencyById(Long id) {
		
		Agency com = agencyDAO.getFinder().and("idAgency").eq(id).setDistinct().result();
		return com;
	}

	@Override
	public Agency getDummyAgency() {
		Agency com = null;
		try {
			com = agencyDAO.getFinder().and("hidden").eq(true).setDistinct().result();
		} catch (Exception e) {
			com=null;
		}
		return com;
	}

	
	@Override
	public List<Agency> getAgencyLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return agencyDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
	}

	@Override
	public long getAgencyLazyLoadingCount(Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return agencyDAO.sizeOfListWithField(filters, filtriInOr, null);
	}
}
