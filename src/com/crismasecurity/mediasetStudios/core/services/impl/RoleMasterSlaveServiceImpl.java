package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.RoleMasterSlaveDAO;
import com.crismasecurity.mediasetStudios.core.entity.RoleMasterSlave;
import com.crismasecurity.mediasetStudios.core.services.RoleMasterSlaveService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;


/**
 * 
 * Class name: RoleMasterSlaveServiceImpl
 *
 * Description: Implementazione 
 * 
 *
 * Company: 
 *
 * @author 	vincenzo.scaccia
 * @date 	04/jul/2018
 *
 */


@Service
@Transactional
public class RoleMasterSlaveServiceImpl implements RoleMasterSlaveService{

	
	@Autowired
	private RoleMasterSlaveDAO RoleMasterSlaveDAO;
	
	@PostConstruct
	public void init() {
	}
	
	@Override
	public List<RoleMasterSlave> getRoleMasterSlave() {
		return RoleMasterSlaveDAO.getFinder().addOrderAsc("id").setDistinct().list();
	}

	@Override
	public RoleMasterSlave addRoleMasterSlave(RoleMasterSlave r) throws Exception {
		return RoleMasterSlaveDAO.create(r);
	}

	@Override
	public RoleMasterSlave updateRoleMasterSlave(RoleMasterSlave r) throws Exception {
		return RoleMasterSlaveDAO.update(r);
	}

	@Override
	public void deleteRoleMasterSlave(long id) {
		RoleMasterSlaveDAO.delete(id);	
	}

	@Override
	public Finder<RoleMasterSlave> getRoleMasterSlaveFinder() {
		return RoleMasterSlaveDAO.getFinder();
	}

	@Override
	public RoleMasterSlave getRoleMasterSlaveById(Long id) {
		RoleMasterSlave ap = RoleMasterSlaveDAO.getFinder().and("id").eq(id).setDistinct().result();
		return ap;
	}
	

	@Override
	public List<RoleMasterSlave> getRoleSlaveByIdRoleMaster(Long idMaster) {
		return RoleMasterSlaveDAO.getFinder().and("masterRole.id").eq(idMaster).setDistinct().list();
	}	
	
	@Override
	public List<RoleMasterSlave> getRoleMasterSlaveLazyLoading(int first, int pageSize, String sortField, int sortOrder,
			Map<String, Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return RoleMasterSlaveDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
	}

	@Override
	public long getRoleMasterSlaveLazyLoadingCount(Map<String, Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return RoleMasterSlaveDAO.sizeOfListWithField(filters, filtriInOr, null);
	}

}
