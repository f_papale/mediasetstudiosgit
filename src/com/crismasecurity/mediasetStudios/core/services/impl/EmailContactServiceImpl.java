package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.EmailContactDAO;
import com.crismasecurity.mediasetStudios.core.entity.EmailContact;
import com.crismasecurity.mediasetStudios.core.services.EmailContactService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

/**
 * 
 * Class name: EmailContactServiceImpl
 *
 * Description: Implementazione Servizio Spring EmailContact
 * 
 *
 * Company: 
 *
 * @author 	vincenzo.scaccia
 * @date 	04/jul/2018
 *
 */

@Service
@Transactional
public class EmailContactServiceImpl implements EmailContactService{
	
	
	@Autowired
	private EmailContactDAO EmailContactDAO;	
	
	
	@PostConstruct
	public void init() {
	}
	
	@Override
	public List<EmailContact> getEmailContact() {
		return EmailContactDAO.getFinder().addOrderAsc("id").setDistinct().list();
	}

	@Override
	public EmailContact addEmailContact(EmailContact r) throws Exception {
		return EmailContactDAO.create(r);
	}

	@Override
	public EmailContact updateEmailContact(EmailContact r) throws Exception {
		return EmailContactDAO.update(r);
	}

	@Override
	public void deleteEmailContact(long id) {
		EmailContactDAO.delete(id);
	}

	@Override
	public Finder<EmailContact> getEmailContactFinder() {
		return EmailContactDAO.getFinder();
	}

	@Override
	public EmailContact getEmailContactById(Long id) {
		EmailContact apType = EmailContactDAO.getFinder().and("id").eq(id).setDistinct().result();
		return apType;
	}

	@Override
	public List<EmailContact> getEmailContactLazyLoading(int first, int pageSize, String sortField, int sortOrder,
			Map<String, Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return EmailContactDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
	}

	@Override
	public long getEmailContactLoadingCount(Map<String, Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return EmailContactDAO.sizeOfListWithField(filters, filtriInOr, null);
	}

}
