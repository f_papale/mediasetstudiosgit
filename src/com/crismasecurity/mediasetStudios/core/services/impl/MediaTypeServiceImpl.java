package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.MediaTypeDAO;
import com.crismasecurity.mediasetStudios.core.entity.MediaType;
import com.crismasecurity.mediasetStudios.core.services.MediaTypeService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

/**
 * @author Gio
 *
 */
@Service
@Transactional
public class MediaTypeServiceImpl implements MediaTypeService {


	@Autowired
	private MediaTypeDAO mediaTypeDAO;
	

	@PostConstruct
	public void init() {
	}

	
	@Override
	public List<MediaType> getMediaType() {
		return mediaTypeDAO.getFinder().addOrderAsc("id").setDistinct().list();
	}


	@Override
	public MediaType addMediaType(MediaType r) throws Exception {
		return mediaTypeDAO.create(r);
	}


	@Override
	public MediaType updateMediaType(MediaType r) throws Exception {
		return mediaTypeDAO.update(r);
	}


	@Override
	public void deleteMediaType(long id) {
		mediaTypeDAO.delete(id);
	}


	@Override
	public Finder<MediaType> getMediaTypeFinder() {
		return mediaTypeDAO.getFinder();
	}


	@Override
	public MediaType getMediaTypeById(Long id) {
		
		MediaType com = mediaTypeDAO.getFinder().and("id").eq(id).setDistinct().result();
		return com;
	}


	@Override
	public List<MediaType> getMediaTypeLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return mediaTypeDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
	}

	@Override
	public long getMediaTypeLazyLoadingCount(Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return mediaTypeDAO.sizeOfListWithField(filters, filtriInOr, null);
	}

	
}
