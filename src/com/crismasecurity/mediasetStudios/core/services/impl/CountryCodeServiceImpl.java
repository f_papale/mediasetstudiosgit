package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.CountryCodeDAO;
import com.crismasecurity.mediasetStudios.core.entity.CountryCode;
import com.crismasecurity.mediasetStudios.core.services.CountryCodeService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

/**
 * @author fpapale
 *
 */
@Service
@Transactional
public class CountryCodeServiceImpl implements CountryCodeService {


	@Autowired
	private CountryCodeDAO countryCodeDAO;
	
	
	
	@PostConstruct
	public void init() {
	}

	
	
	@Override
	public List<CountryCode> getCountryCode() {
		return countryCodeDAO.getFinder().addOrderAsc("id").setDistinct().list();
	}


	@Override
	public CountryCode addCountryCode(CountryCode r) throws Exception {
		return countryCodeDAO.create(r);
	}


	@Override
	public CountryCode updateCountryCode(CountryCode r) throws Exception {
		return countryCodeDAO.update(r);
	}


	@Override
	public void deleteCountryCode(long id) {
		countryCodeDAO.delete(id);
	}


	@Override
	public Finder<CountryCode> getCountryCodeFinder() {
		return countryCodeDAO.getFinder();
	}


	@Override
	public CountryCode getCountryCodeById(Long id) {
		
		CountryCode com = countryCodeDAO.getFinder().and("id").eq(id).setDistinct().result();
		return com;
	}


	@Override
	public List<CountryCode> getCountryCodeLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return countryCodeDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
	}

	@Override
	public long getCountryCodeLazyLoadingCount(Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return countryCodeDAO.sizeOfListWithField(filters, filtriInOr, null);
	}

	
}
