package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.EmployeeDAO;
import com.crismasecurity.mediasetStudios.core.entity.Employee;
import com.crismasecurity.mediasetStudios.core.services.EmployeeService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

/**
 * @author Gio
 *
 */
@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {


	@Autowired
	private EmployeeDAO employeeDAO;
	
	
	
	@PostConstruct
	public void init() {
	}

	
	@Override
	public List<Employee> getEmployee() {
		return employeeDAO.getFinder().addOrderAsc("id").setDistinct().list();
	}


	@Override
	public Employee addEmployee(Employee r) throws Exception {
		return employeeDAO.create(r);
	}


	@Override
	public Employee updateEmployee(Employee r) throws Exception {
		return employeeDAO.update(r);
	}


	@Override
	public void deleteEmployee(long id) {
		employeeDAO.delete(id);
	}


	@Override
	public Finder<Employee> getEmployeeFinder() {
		return employeeDAO.getFinder();
	}


	@Override
	public Employee getEmployeeById(Long id) {
		
		Employee com = employeeDAO.getFinder().and("id").eq(id).setDistinct().result();
		return com;
	}


	@Override
	public List<Employee> getEmployeeLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters) {
		//Map<String,Object> filtriInOr = new HashMap<String,Object>();
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return employeeDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
	}

	@Override
	public long getEmployeeLazyLoadingCount(Map<String,Object> filters) {
		//Map<String,Object> filtriInOr = new HashMap<String,Object>();
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return employeeDAO.sizeOfListWithField(filters, filtriInOr, null);
	}

	
}
