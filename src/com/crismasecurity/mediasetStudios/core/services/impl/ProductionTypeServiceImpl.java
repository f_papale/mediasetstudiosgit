package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.ProductionTypeDAO;
import com.crismasecurity.mediasetStudios.core.entity.ProductionType;
import com.crismasecurity.mediasetStudios.core.services.ProductionTypeService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

/**
 * @author Gio
 *
 */
@Service
@Transactional
public class ProductionTypeServiceImpl implements ProductionTypeService {


	@Autowired
	private ProductionTypeDAO productionTypeDAO;
	
	
	
	@PostConstruct
	public void init() {
	}

	
	
	@Override
	public List<ProductionType> getProductionType() {
		return productionTypeDAO.getFinder().addOrderAsc("id").setDistinct().list();
	}


	@Override
	public ProductionType addProductionType(ProductionType r) throws Exception {
		return productionTypeDAO.create(r);
	}


	@Override
	public ProductionType updateProductionType(ProductionType r) throws Exception {
		return productionTypeDAO.update(r);
	}


	@Override
	public void deleteProductionType(long id) {
		productionTypeDAO.delete(id);
	}


	@Override
	public Finder<ProductionType> getProductionTypeFinder() {
		return productionTypeDAO.getFinder();
	}


	@Override
	public ProductionType getProductionTypeById(Long id) {
		
		ProductionType com = productionTypeDAO.getFinder().and("id").eq(id).setDistinct().result();
		return com;
	}


	@Override
	public List<ProductionType> getProductionTypeLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return productionTypeDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
	}

	@Override
	public long getProductionTypeLazyLoadingCount(Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return productionTypeDAO.sizeOfListWithField(filters, filtriInOr, null);
	}

	
}
