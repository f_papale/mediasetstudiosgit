package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.IncidentTypeDAO;
import com.crismasecurity.mediasetStudios.core.entity.IncidentType;
import com.crismasecurity.mediasetStudios.core.services.IncidentTypeService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

/**
 * @author fra
 *
 */
@Service
@Transactional
public class IncidentTypeServiceImpl implements IncidentTypeService {


	@Autowired
	private IncidentTypeDAO incidentTypeDAO;
	
	
	
	@PostConstruct
	public void init() {
	}

	
	
	@Override
	public List<IncidentType> getIncidentType() {
		return incidentTypeDAO.getFinder().addOrderAsc("id").setDistinct().list();
	}


	@Override
	public IncidentType addIncidentType(IncidentType r) throws Exception {
		return incidentTypeDAO.create(r);
	}


	@Override
	public IncidentType updateIncidentType(IncidentType r) throws Exception {
		return incidentTypeDAO.update(r);
	}


	@Override
	public void deleteIncidentType(long id) {
		incidentTypeDAO.delete(id);
	}


	@Override
	public Finder<IncidentType> getIncidentTypeFinder() {
		return incidentTypeDAO.getFinder();
	}


	@Override
	public IncidentType getIncidentTypeById(Long id) {
		
		IncidentType com = incidentTypeDAO.getFinder().and("id").eq(id).setDistinct().result();
		return com;
	}


	@Override
	public List<IncidentType> getIncidentTypeLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return incidentTypeDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
	}

	@Override
	public long getIncidentTypeLazyLoadingCount(Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return incidentTypeDAO.sizeOfListWithField(filters, filtriInOr, null);
	}

	
}
