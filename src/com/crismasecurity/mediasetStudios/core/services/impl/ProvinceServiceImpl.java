package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.ProvinceDAO;
import com.crismasecurity.mediasetStudios.core.entity.Province;
import com.crismasecurity.mediasetStudios.core.services.ProvinceService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

/**
 * @author fra
 *
 */
@Service
@Transactional
public class ProvinceServiceImpl implements ProvinceService {


	@Autowired
	private ProvinceDAO provinceDAO;
	
	
	
	@PostConstruct
	public void init() {
	}

	
	
	@Override
	public List<Province> getProvince() {
		return provinceDAO.getFinder().addOrderAsc("id").setDistinct().list();
	}


	@Override
	public Province addProvince(Province r) throws Exception {
		return provinceDAO.create(r);
	}


	@Override
	public Province updateProvince(Province r) throws Exception {
		return provinceDAO.update(r);
	}


	@Override
	public void deleteProvince(long id) {
		provinceDAO.delete(id);
	}


	@Override
	public Finder<Province> getProvinceFinder() {
		return provinceDAO.getFinder();
	}


	@Override
	public Province getProvinceById(Long id) {
		
		Province com = provinceDAO.getFinder().and("id").eq(id).setDistinct().result();
		return com;
	}


	@Override
	public List<Province> getProvinceLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return provinceDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
	}

	@Override
	public long getProvinceLazyLoadingCount(Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return provinceDAO.sizeOfListWithField(filters, filtriInOr, null);
	}

	
}
