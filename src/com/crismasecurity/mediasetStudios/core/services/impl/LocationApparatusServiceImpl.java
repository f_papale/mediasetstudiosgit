

package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.LocationApparatusDAO;
import com.crismasecurity.mediasetStudios.core.entity.LocationApparatus;
import com.crismasecurity.mediasetStudios.core.services.LocationApparatusService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

@Service
@Transactional
public class LocationApparatusServiceImpl implements LocationApparatusService {

	@Autowired
	private LocationApparatusDAO locationApparatusDAO;	
	
	
	@PostConstruct
	public void init() {
	}


	@Override
	public List<LocationApparatus> getLocationApparatus() {
		return locationApparatusDAO.getFinder().addOrderAsc("id").setDistinct().list();
	}


	@Override
	public LocationApparatus addLocationApparatus(LocationApparatus r) throws Exception {
		return locationApparatusDAO.create(r);
	}


	@Override
	public LocationApparatus updateLocationApparatus(LocationApparatus r) throws Exception {
		return locationApparatusDAO.update(r);
	}


	@Override
	public void deleteLocationApparatus(long id) {
		locationApparatusDAO.delete(id);
	}


	@Override
	public Finder<LocationApparatus> getLocationApparatusFinder() {
		return locationApparatusDAO.getFinder();
	}


	@Override
	public LocationApparatus getLocationApparatusById(Long id) {
		LocationApparatus locationApparatus = locationApparatusDAO.getFinder().and("id").eq(id).setDistinct().result();
		return locationApparatus;
	}


	@Override
	public List<LocationApparatus> getLocationApparatusLazyLoading(int first, int pageSize, String sortField,
			int sortOrder, Map<String, Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return locationApparatusDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
	}


	@Override
	public long getLocationApparatusLazyLoadingCount(Map<String, Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return locationApparatusDAO.sizeOfListWithField(filters, filtriInOr, null);
	}
}
