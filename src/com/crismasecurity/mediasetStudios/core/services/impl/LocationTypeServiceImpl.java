package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.LocationTypeDAO;
import com.crismasecurity.mediasetStudios.core.entity.LocationType;
import com.crismasecurity.mediasetStudios.core.services.LocationTypeService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

/**
 * 
 * Class name: ApparatusTypeServiceImpl
 *
 * Description: Implementazione Servizio Spring ApparatusType
 * 
 *
 * Company: 
 *
 * @author 	vincenzo.scaccia
 * @date 	04/jul/2018
 *
 */

@Service
@Transactional
public class LocationTypeServiceImpl implements LocationTypeService{
	
	
	@Autowired
	private LocationTypeDAO locationTypeDAO;	
	
	
	@PostConstruct
	public void init() {
	}
	
	@Override
	public List<LocationType> getLocationType() {
		return locationTypeDAO.getFinder().addOrderAsc("id").setDistinct().list();
	}

	@Override
	public LocationType addLocationType(LocationType r) throws Exception {
		return locationTypeDAO.create(r);
	}

	@Override
	public LocationType updateLocationType(LocationType r) throws Exception {
		return locationTypeDAO.update(r);
	}

	@Override
	public void deleteLocationType(long id) {
		locationTypeDAO.delete(id);
	}

	@Override
	public Finder<LocationType> getLocationTypeFinder() {
		return locationTypeDAO.getFinder();
	}

	@Override
	public LocationType getLocationTypeById(Long id) {
		LocationType lcType = locationTypeDAO.getFinder().and("id").eq(id).setDistinct().result();
		return lcType;
	}

	@Override
	public List<LocationType> getLocationTypeLazyLoading(int first, int pageSize, String sortField, int sortOrder,
			Map<String, Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return locationTypeDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
	}

	@Override
	public long getLocationTypeLoadingCount(Map<String, Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return locationTypeDAO.sizeOfListWithField(filters, filtriInOr, null);
	}

}
