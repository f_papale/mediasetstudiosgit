

package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.DocumentTypeDAO;
import com.crismasecurity.mediasetStudios.core.entity.DocumentType;
import com.crismasecurity.mediasetStudios.core.services.DocumentTypeService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

@Service
@Transactional
public class DocumentTypeServiceImpl implements DocumentTypeService {

	@Autowired
	private DocumentTypeDAO documentTypeDAO;	
	
	
	@PostConstruct
	public void init() {
	}
	
	@Override
	public List<DocumentType> getDocumentType() {
		return documentTypeDAO.getFinder().addOrderAsc("id").setDistinct().list();
	}
	
	@Override
	public DocumentType getDocumentTypeByTypeId(Integer id) {
		return documentTypeDAO.getFinder().and("idDocumentType").eq(id).setDistinct().result();
	}

	@Override
	public DocumentType addDocumentType(DocumentType r) throws Exception {
		return documentTypeDAO.create(r);
	}

	@Override
	public DocumentType updateDocumentType(DocumentType r) throws Exception {
		return documentTypeDAO.update(r);
	}

	@Override
	public void deleteDocumentType(long id) {
		documentTypeDAO.delete(id);
		
	}

	@Override
	public Finder<DocumentType> getDocumentTypeFinder() {
		return documentTypeDAO.getFinder();
	}

	@Override
	public DocumentType getDocumentTypeById(Long id) {
		DocumentType sptype = documentTypeDAO.getFinder().and("id").eq(id).setDistinct().result();
		return sptype;
	}

	@Override
	public List<DocumentType> getDocumentTypeLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String, Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return documentTypeDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
	}

	@Override
	public long getDocumentTypeLazyLoadingCount(Map<String, Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return documentTypeDAO.sizeOfListWithField(filters, filtriInOr, null);
	}
}
