package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.AuditDAO;
import com.crismasecurity.mediasetStudios.core.entity.Audit;
import com.crismasecurity.mediasetStudios.core.services.AuditService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

/**
 * @author Gio
 *
 */
@Service
@Transactional
public class AuditServiceImpl implements AuditService {


	@Autowired
	private AuditDAO auditDAO;
	
	
	
	@PostConstruct
	public void init() {
	}

	
	@Override
	public List<Audit> getAudits() {
		return auditDAO.getAll();
	}


	@Override
	public Audit addAudit(Audit r) throws Exception {
		return auditDAO.create(r);
	}


	@Override
	public Audit updateAudit(Audit r) throws Exception {
		return auditDAO.update(r);
	}


	@Override
	public void deleteAudit(long id) {
		auditDAO.delete(id);
	}


	@Override
	public Finder<Audit> getAuditFinder() {
		return auditDAO.getFinder();
	}


	@Override
	public Audit getAuditById(Long id) {
		return auditDAO.getFinder().and("id").eq(id).setDistinct().result();
	}
	
	@Override
	public List<Audit> getAuditLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return auditDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, false);
	}


	@Override
	public long getAuditLazyLoadingCount(Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return auditDAO.sizeOfListWithField(filters, filtriInOr, null);
	}


}
