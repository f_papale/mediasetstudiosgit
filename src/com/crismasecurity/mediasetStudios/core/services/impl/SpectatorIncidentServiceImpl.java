

package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.SpectatorIncidentDAO;
import com.crismasecurity.mediasetStudios.core.entity.SpectatorIncident;
import com.crismasecurity.mediasetStudios.core.services.SpectatorIncidentService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

@Service
@Transactional
public class SpectatorIncidentServiceImpl implements SpectatorIncidentService {

	
	@Autowired
	private SpectatorIncidentDAO spectatorIncidentDAO;	
	
	@PostConstruct
	public void init() {
	}
	
	
	@Override
	public List<SpectatorIncident> getSpectatorIncident() {
		return spectatorIncidentDAO.getFinder().addOrderAsc("id").setDistinct().list();
	}

	@Override
	public SpectatorIncident addSpectatorIncident(SpectatorIncident r) throws Exception {
		return spectatorIncidentDAO.create(r);
	}

	@Override
	public SpectatorIncident updateSpectatorIncident(SpectatorIncident r) throws Exception {
		return spectatorIncidentDAO.update(r);
	}

	@Override
	public void deleteSpectatorIncident(long id) {
		spectatorIncidentDAO.delete(id);
	}

	@Override
	public Finder<SpectatorIncident> getSpectatorIncidentFinder() {
		return spectatorIncidentDAO.getFinder();
	}

	@Override
	public SpectatorIncident getSpectatorIncidentById(Long id) {
		SpectatorIncident spectatorIncident = spectatorIncidentDAO.getFinder().and("id").eq(id).setDistinct().result();
		return spectatorIncident;
	}

	@Override
	public List<SpectatorIncident> getSpectatorIncidentLazyLoading(int first, int pageSize, String sortField,
			int sortOrder, Map<String, Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return spectatorIncidentDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
	}

	@Override
	public long getSpectatorIncidentLazyLoadingCount(Map<String, Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return spectatorIncidentDAO.sizeOfListWithField(filters, filtriInOr, null);
	}

	
}
