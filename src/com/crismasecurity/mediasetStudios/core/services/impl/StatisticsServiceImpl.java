package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.StatisticsDAO;
import com.crismasecurity.mediasetStudios.core.entity.Statistics;
import com.crismasecurity.mediasetStudios.core.services.StatisticsService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

/**
 * @author Gio
 *
 */
@Service
@Transactional
public class StatisticsServiceImpl implements StatisticsService {


	@Autowired
	private StatisticsDAO statisticsDAO;
	

	@PostConstruct
	public void init() {
	}

	
	@Override
	public List<Statistics> getStatistics() {
		return statisticsDAO.getFinder().addOrderAsc("id").setDistinct().list();
	}


	@Override
	public Statistics addStatistics(Statistics r) throws Exception {
		return statisticsDAO.create(r);
	}


	@Override
	public Statistics updateStatistics(Statistics r) throws Exception {
		return statisticsDAO.update(r);
	}


	@Override
	public void deleteStatistics(long id) {
		statisticsDAO.delete(id);
	}


	@Override
	public Finder<Statistics> getStatisticsFinder() {
		return statisticsDAO.getFinder();
	}


	@Override
	public Statistics getStatisticsById(Long id) {
		Statistics com = statisticsDAO.getFinder().and("id").eq(id).setDistinct().result();
		return com;
	}


	@Override
	public List<Statistics> getStatisticsLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return statisticsDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
	}

	@Override
	public long getStatisticsLazyLoadingCount(Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return statisticsDAO.sizeOfListWithField(filters, filtriInOr, null);
	}

	@Override
	public String getStatisticsByDomainMeasureValue(String domain, String measure) {
		String sResult="";
		Statistics st = statisticsDAO.getFinder().and("domain").eq(domain).and("measure").eq(measure).setDistinct().result();
		if (!(st==null))
			sResult=st.getMeasure();
		return sResult;
	}

	@Override
	public Statistics getStatisticsByDomainMeasure(String domain, String measure) {
		return statisticsDAO.getFinder().and("domain").eq(domain).and("measure").eq(measure).setDistinct().result();
	}


	@Override
	public List<Statistics> getStatisticsByDomain(String domain) {
		return statisticsDAO.getFinder().and("domain").eq(domain).setDistinct().list();
	}
	
}
