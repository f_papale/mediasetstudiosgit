package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.DocumentSpectatorDAO;
import com.crismasecurity.mediasetStudios.core.entity.DocumentSpectator;
import com.crismasecurity.mediasetStudios.core.services.DocumentSpectatorService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

/**
 * @author Frank
 *
 */
@Service
@Transactional
public class DocumentSpectatorServiceImpl implements DocumentSpectatorService {


	@Autowired
	private DocumentSpectatorDAO documentSpectatorDAO;
	
	
	
	@PostConstruct
	public void init() {
	}

	
	
	@Override
	public List<DocumentSpectator> getDocumentSpectator() {
		return documentSpectatorDAO.getFinder().addOrderAsc("id").setDistinct().list();
	}


	@Override
	public DocumentSpectator addDocumentSpectator(DocumentSpectator r) throws Exception {
		return documentSpectatorDAO.create(r);
	}


	@Override
	public DocumentSpectator updateDocumentSpectator(DocumentSpectator r) throws Exception {
		return documentSpectatorDAO.update(r);
	}


	@Override
	public void deleteDocumentSpectator(long id) {
		documentSpectatorDAO.delete(id);
	}


	@Override
	public Finder<DocumentSpectator> getDocumentSpectatorFinder() {
		return documentSpectatorDAO.getFinder();
	}


	@Override
	public DocumentSpectator getDocumentSpectatorById(Long id) {
		
		DocumentSpectator com = documentSpectatorDAO.getFinder().and("id").eq(id).setDistinct().result();
		return com;
	}

	@Override
	public DocumentSpectator getDocumentSpectatorBySpectatorIdAndDocumentTypeId(Long spectatorId, Integer docTypeId) {
		
		List<DocumentSpectator> coms = documentSpectatorDAO.getFinder().and("spectator.id").eq(spectatorId).and("documentType.idDocumentType").eq(docTypeId).setDistinct().list();
		if (coms!=null && coms.size()>0) return coms.get(0);
		//DocumentSpectator com = documentSpectatorDAO.getFinder().and("spectator.id").eq(spectatorId).and("documentType.idDocumentType").eq(docTypeId).setDistinct().result();
		return null;
	}
	

	@Override
	public List<DocumentSpectator> getDocumentSpectatorLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return documentSpectatorDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
	}

	@Override
	public long getDocumentSpectatorLazyLoadingCount(Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return documentSpectatorDAO.sizeOfListWithField(filters, filtriInOr, null);
	}

	
}
