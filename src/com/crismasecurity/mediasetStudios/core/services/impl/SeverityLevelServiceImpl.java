package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.SeverityLevelDAO;
import com.crismasecurity.mediasetStudios.core.entity.SeverityLevel;
import com.crismasecurity.mediasetStudios.core.services.SeverityLevelService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

/**
 * @author fra
 *
 */
@Service
@Transactional
public class SeverityLevelServiceImpl implements SeverityLevelService {


	@Autowired
	private SeverityLevelDAO severityLevelDAO;
	
	
	
	@PostConstruct
	public void init() {
	}

	
	
	@Override
	public List<SeverityLevel> getSeverityLevel() {
		return severityLevelDAO.getFinder().addOrderAsc("id").setDistinct().list();
	}


	@Override
	public SeverityLevel addSeverityLevel(SeverityLevel r) throws Exception {
		return severityLevelDAO.create(r);
	}


	@Override
	public SeverityLevel updateSeverityLevel(SeverityLevel r) throws Exception {
		return severityLevelDAO.update(r);
	}


	@Override
	public void deleteSeverityLevel(long id) {
		severityLevelDAO.delete(id);
	}


	@Override
	public Finder<SeverityLevel> getSeverityLevelFinder() {
		return severityLevelDAO.getFinder();
	}


	@Override
	public SeverityLevel getSeverityLevelById(Long id) {
		
		SeverityLevel com = severityLevelDAO.getFinder().and("id").eq(id).setDistinct().result();
		return com;
	}


	@Override
	public List<SeverityLevel> getSeverityLevelLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return severityLevelDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
	}

	@Override
	public long getSeverityLevelLazyLoadingCount(Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return severityLevelDAO.sizeOfListWithField(filters, filtriInOr, null);
	}

	
}
