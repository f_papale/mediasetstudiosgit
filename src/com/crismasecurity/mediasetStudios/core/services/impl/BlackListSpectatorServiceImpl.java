package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.AgencySpectatorDAO;
import com.crismasecurity.mediasetStudios.core.dao.BlackListSpectatorDAO;
import com.crismasecurity.mediasetStudios.core.dao.LiberatoriaDAO;
import com.crismasecurity.mediasetStudios.core.dao.SpectatorDAO;
import com.crismasecurity.mediasetStudios.core.entity.AgencySpectator;
import com.crismasecurity.mediasetStudios.core.entity.AgencyUser;
import com.crismasecurity.mediasetStudios.core.entity.BlackListSpectator;
import com.crismasecurity.mediasetStudios.core.entity.Spectator;
import com.crismasecurity.mediasetStudios.core.entity.SpectatorEpisode;
import com.crismasecurity.mediasetStudios.core.services.BlackListSpectatorService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

/**
 * @author fra
 *
 */
@Service
@Transactional
public class BlackListSpectatorServiceImpl implements BlackListSpectatorService {

	@Autowired
	private BlackListSpectatorDAO blackListSpectatorDAO;
	
	@Autowired
	private SpectatorDAO spectatorDAO;
	
	@Autowired
	private LiberatoriaDAO liberatoriaDAO;
	
	@Autowired
	private AgencySpectatorDAO agencySpectatorDAO;
	
	
	private int deletedSpectators=0;
	
	private List<String> listItemsDeletedSpectators = new ArrayList<String>();
	
	
	@PostConstruct
	public void init() {
	}

	@Override
	public List<BlackListSpectator> getBlackListSpectator() {
		return blackListSpectatorDAO.getFinder().addOrderAsc("id").setDistinct().list();
	}

	@Override
	public BlackListSpectator addBlackListSpectator(BlackListSpectator r) throws Exception {
		return blackListSpectatorDAO.create(r);
	}

	@Override
	public BlackListSpectator updateBlackListSpectator(BlackListSpectator r) throws Exception {
		return blackListSpectatorDAO.update(r);
	}
	
	@Override
	public void deleteBlackListSpectator(long id) {
		blackListSpectatorDAO.delete(id);
	}

	@Override
	public Finder<BlackListSpectator> getBlackListSpectatorFinder() {
		return blackListSpectatorDAO.getFinder();
	}

	@Override
	public BlackListSpectator getBlackListSpectatorById(Long id) {
		BlackListSpectator com = blackListSpectatorDAO.getFinder().and("id").eq(id).setDistinct().result();
		return com;
	}
	
	@Override
	public BlackListSpectator getBlackListSpectatorByIdSpectator(Long idSpectator) {
		BlackListSpectator com = blackListSpectatorDAO.getFinder().and("spectator.id").eq(idSpectator).setDistinct().result();
		return com;
	}

	@Override
	public List<BlackListSpectator> getBlackListSpectatorLazyLoading(int first, int pageSize, String sortField,
			int sortOrder, Map<String, Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return blackListSpectatorDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
	}

	@Override
	public long getBlackListLoadingCount(Map<String, Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return blackListSpectatorDAO.sizeOfListWithField(filters, filtriInOr, null);
	}

	@Override 
	public boolean isNotInBlackList(Long idSpectator) {
		
		boolean ret = false;
		List<BlackListSpectator> lstSpe = blackListSpectatorDAO.getFinder().and("spectator.id").eq(idSpectator).setDistinct().list();
		ret = ((lstSpe !=null)&&(lstSpe.size()==0));
		return ret;
	}
	
	@Override
    public boolean isNotInBlackList(
    		Spectator spectToCheck, 
    		Long selectedProduction, 
    		Long selectedEpisode,
    		AgencyUser currentAgencyUser, 
    		String loggedUser,
    		boolean sendMail
    		){
    	
    	boolean returnCode=true;
    	
    	System.out.println("checkNotInBlackList");
    	if (spectToCheck!=null && spectToCheck.getId()!=null) {
//    		Vado a creare una lista contenente i nominativi che sono in black list
//    		Valuto se lo spettatore con id gia esistente è in blackList
    		List<BlackListSpectator> lb = getBlackListSpectatorFiltered(1,spectToCheck.getId(), null,null, null,null);

//    		Aggiungo in lista lo spettatore con id non esistente con Codice Fiscale uguale
    		lb.addAll(getBlackListSpectatorFiltered(2,null, spectToCheck.getFiscalCode(),null, null,null));

//    		Aggiungo in lista lo spettatore con id non esistente con Nome e Cognome e data di nascita uguale 
    		lb.addAll(getBlackListSpectatorFiltered(3,null, null,spectToCheck.getSurname().toString(), spectToCheck.getName().toString(),spectToCheck.getBirthDate()));

//    		Aggiungo in lista lo spettatore con id non esistente con Nome e Cognome uguale e data di nascita null
    		lb.addAll(getBlackListSpectatorFiltered(4,null, null,spectToCheck.getSurname().toString(), spectToCheck.getName().toString(),spectToCheck.getBirthDate()));
//    		Se esistono dei nominativi che potenzialmente matchano con quello dato
    		if (lb!=null && lb.size()>0) {
    			BlackListSpectator bs = lb.get(0); //Prendo solo il primo, dovrebbe essercene solo uno!
//    			A seconda che sia ALTO o MEDIO mi comporto differentemente
    			switch (bs.getBlackListType().getIdType()){
//    			Eliminato. Ora in ambedue i casi non manda più la email e blocca il nominativo
//    			
//    			case 1:
//    				if (sendMail) {
//		    			// se spettatore in blacklist, invio mail a resp sicurezza
//		    			//spectatorToInsert.
//		    	    	String internalMails = PropertiesUtils.getInstance().readProperty("addSpectator.in_blacklist.email_list");
//		    	    	List<String> emails = new ArrayList<String>();
//		    			emails = Arrays.asList(internalMails.split("\\s*;\\s*"));
//		    			
//		    			String oggettoMail = PropertiesUtils.getInstance().readProperty("addSpectator.in_blacklist.email_object");
//		    			oggettoMail = String.format(oggettoMail, spectToCheck.getName() + " " + spectToCheck.getSurname() , (bs.getBlackListType()!=null?bs.getBlackListType().getDescription():""));
//		    			
//		    			String sSecurityManagers=null;
//		    			List<String> emailsSecurityManagers = new ArrayList<String>();
//		    			Agency ag = (currentAgencyUser!=null?currentAgencyUser.getAgency():null);
//		    			String bodyMail = PropertiesUtils.getInstance().readProperty("addSpectator.in_blacklist.email_body");
//		    			Production pp=null;
//		    			Episode ep=null;
//		    			if (!(selectedProduction==null || selectedEpisode==null)) {
//							pp = productionService.getProductionById(selectedProduction); 
//			    			ep = episodeService.getEpisodeById(selectedEpisode);
//			       			
//			    			try {
//			    				sSecurityManagers= ep.getStudio().getProductionCenter().getEmail();	
//							} catch (Exception e) {
//								System.out.println("Errore Invio mail - SecurityManagers non estratti: "+ e); 
//							}
//			    			if (sSecurityManagers!=null) {
//		    					emailsSecurityManagers= Arrays.asList(sSecurityManagers.split("\\s*;\\s*"));
//		    					emails= ListUtil.getInstance().union(emails, emailsSecurityManagers);
//		    				}			    			
//			    			bodyMail = String.format(bodyMail, spectToCheck.getName() + " " + spectToCheck.getSurname() , (bs.getBlackListType()!=null?bs.getBlackListType().getDescription():""), (ag!=null?ag.getName():"") + " ("+ loggedUser+")", (ep!=null?ep.getDateFromOnlyDate()+" " + ep.getDateFromOnlyHours():""), (pp!=null?pp.getName():""));			    			
//		    			}else {
//		    				bodyMail = String.format(bodyMail, spectToCheck.getName() + " " + spectToCheck.getSurname() , (bs.getBlackListType()!=null?bs.getBlackListType().getDescription():""), (ag!=null?ag.getName():"") + " ("+ loggedUser+")", (ep!=null?ep.getDateFromOnlyDate()+" " + ep.getDateFromOnlyHours():""), (pp!=null?pp.getName():""));
//		    			}
//		    			System.out.println("Invio mail - Object:" + oggettoMail + "   body:"+ bodyMail);
//		    			// Invio mail
//		    			MailMessage mail = new MailMessage(emails.toArray(new String[0]), oggettoMail, bodyMail, false);
//		    			emailSender.addMailToSend(mail);
//    				}
//	    			returnCode=true;
//    				break;
//    			Blocca comunque il nominativo
    			case 1:
    				returnCode=false;
    				break;
//    			Blocca comunque il nominativo	
    			case 2:
    				returnCode=false;
    				break;
    			default:
    				returnCode=false;
    				break;
    			}
    		}
    	}
    	return returnCode;
    } 	
	

	@Override
	public boolean consolidateLast() {
		boolean returnCode=true;
		deletedSpectators=0;
		List<BlackListSpectator> lst= blackListSpectatorDAO.getFinder().setDistinct().list();
		if (lst!=null) {
			BlackListSpectator dummy = Collections.max(lst, Comparator.comparing(BlackListSpectator::getUpdatedOn));
			for (BlackListSpectator x : lst) {
//				if (
//						x.getSpectator().getFullName().contains("GUERRA ALESSANDRO")
//						|| 
//						x.getSpectator().getFullName().contains("BOVE FULVIO")) //44138
//					System.out.println("Stop...");
				if (x.getUpdatedOn().compareTo(dummy.getUpdatedOn())==-1) {
					try{
						String fullName =x.getSpectator().getFullName();
						blackListSpectatorDAO.delete(x.getIdBlackListSpectator()) ;
						deletedSpectators++;
						listItemsDeletedSpectators.add(fullName);

						if (x.getSpectator().getEpisodes()!=null)
						{
							x.getSpectator().getEpisodes().size();
							for (SpectatorEpisode se : x.getSpectator().getEpisodes()) {
								if (se.getLiberatoria()!=null)
									liberatoriaDAO.delete(se.getLiberatoria().getId());	
							}
						}
						
						if (x.getSpectator().getAgencies()!=null) {
							x.getSpectator().getAgencies().size();
							for (AgencySpectator as : x.getSpectator().getAgencies()) {
								agencySpectatorDAO.delete(as.getId());	
							}
						}

						spectatorDAO.delete(x.getSpectator().getId());
						    
					}catch (Exception e) {
						returnCode=false;
					}
				}	
			}
		}

		return returnCode;
	}	

	@Override
	public int getDeletedAfterConsolidate() {
		return deletedSpectators;
	}
	
	@Override
	public List<String> getListItemsDeletedAfterConsolidate(){
		return listItemsDeletedSpectators;
	}
	

	private List<BlackListSpectator> getBlackListSpectatorFiltered(
			int TypeFilter, 
			Long id, 
			String fiscalcode,
			String surname, 
			String name, 
			Date birthDate) {
		// TODO Auto-generated method stub
		
		List<BlackListSpectator> com= new ArrayList<BlackListSpectator>();
		switch (TypeFilter) {
		case 1:
			if( id!=null) {
				com = blackListSpectatorDAO.getFinder().and("spectator.id").eq(id).setDistinct().list();
			}
			break;
		case 2:
			if(!fiscalcode.isEmpty()) {
				com = blackListSpectatorDAO.getFinder().and("spectator.fiscalCode").eq(fiscalcode).setDistinct().list();
				}
			break;
		case 3:
			if (!(surname.isEmpty() || name.isEmpty() || birthDate==null)) {
				com = blackListSpectatorDAO.getFinder().and("spectator.surname").eq(surname).and("spectator.name").eq(name).and("spectator.birthDate").eq(birthDate).setDistinct().list();				
			}
			break;
		case 4:
			if (!(surname.isEmpty() || name.isEmpty()) && (birthDate==null)) {
				com = blackListSpectatorDAO.getFinder().and("spectator.surname").eq(surname).and("spectator.name").eq(name).setDistinct().list();
			}
			break;
		}
		com.size();
		return com;
	}

	public List<String> getListItemsDeletedSpectators() {
		return listItemsDeletedSpectators;
	}

	
	
}
