

package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.SpectatorTypeDAO;
import com.crismasecurity.mediasetStudios.core.entity.SpectatorType;
import com.crismasecurity.mediasetStudios.core.services.SpectatorTypeService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

@Service
@Transactional
public class SpectatorTypeServiceImpl implements SpectatorTypeService {

	@Autowired
	private SpectatorTypeDAO spectatorTypeDAO;	
	
	
	@PostConstruct
	public void init() {
	}
	
	@Override
	public List<SpectatorType> getSpectatorType() {
		return spectatorTypeDAO.getFinder().addOrderAsc("id").setDistinct().list();
	}

	@Override
	public SpectatorType addSpectatorType(SpectatorType r) throws Exception {
		return spectatorTypeDAO.create(r);
	}

	@Override
	public SpectatorType updateSpectatorType(SpectatorType r) throws Exception {
		return spectatorTypeDAO.update(r);
	}

	@Override
	public void deleteSpectatorType(long id) {
		spectatorTypeDAO.delete(id);
		
	}

	@Override
	public Finder<SpectatorType> getSpectatorTypeFinder() {
		return spectatorTypeDAO.getFinder();
	}

	@Override
	public SpectatorType getSpectatorTypeById(Long id) {
		SpectatorType sptype = spectatorTypeDAO.getFinder().and("id").eq(id).setDistinct().result();
		return sptype;
	}

	@Override
	public List<SpectatorType> getSpectatorTypeLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String, Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return spectatorTypeDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
	}

	@Override
	public long getSpectatorTypeLazyLoadingCount(Map<String, Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return spectatorTypeDAO.sizeOfListWithField(filters, filtriInOr, null);
	}

	@Override
	public SpectatorType getSpectatorTypeByIdSpectatorType(int idSpectatorType) {
		SpectatorType sptype = spectatorTypeDAO.getFinder().and("idSpectatorType").eq(idSpectatorType).setDistinct().result();
		return sptype;
	}
}
