package com.crismasecurity.mediasetStudios.core.services.impl;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.util.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.entity.EmailQueue;
import com.crismasecurity.mediasetStudios.core.entity.Episode;
import com.crismasecurity.mediasetStudios.core.entity.EpisodeStatusType;
import com.crismasecurity.mediasetStudios.core.entity.SpectatorEpisode;
import com.crismasecurity.mediasetStudios.core.services.EmailMakerService;
import com.crismasecurity.mediasetStudios.core.services.EmailQueueService;
import com.crismasecurity.mediasetStudios.core.services.EpisodeService;
import com.crismasecurity.mediasetStudios.core.services.EpisodeStatusTypeService;
import com.crismasecurity.mediasetStudios.core.utils.PropertiesUtils;

@Service
@Transactional
public class EmailMakerServiceImpl implements EmailMakerService  {

	@Autowired
    private EpisodeService episodeService;
    
    @Autowired
    private EpisodeStatusTypeService episodeStatusTypeService;
    
	@Autowired
	private EmailQueueService emailQueueService;
	
	@PostConstruct
	public void init() {
	}
	
	private Episode episode=null;
	private SpectatorEpisode spectatorEpisode=null;
	
	
    private Date getDate() {
		Date date = new Date();
		Instant instant = date.toInstant();
		return Date.from(instant);    	
    }
    
	@Override
	public void createEmails(Episode episode, boolean bMarkToSendMail, int selFunction) {
		this.episode=episode;
		
		makeEmails (bMarkToSendMail, selFunction);
	}

	@Override
	public void createEmails(boolean bMarkToSendMail, int selFunction) {
		if (this.episode!=null) {
			makeEmails (bMarkToSendMail, selFunction);	
		}	
	}
	
	
	private static String makeEmailAddress(String emailAddress) {
		
		String ret="";
		
		if ((emailAddress==null)||(emailAddress.isEmpty())) {
			ret = PropertiesUtils.getInstance().readProperty("mail.from", "");
		}else{
			if (emailAddress.contains(PropertiesUtils.getInstance().readProperty("mail.from",""))) {
				ret = emailAddress;
			}else {
				ret = emailAddress+";"+PropertiesUtils.getInstance().readProperty("mail.from","");
			}
		}
		return makeDistinctEmailList(ret);
	}
	private static String makeDistinctEmailList(String emails) {
		List<String> emailsList = new ArrayList<String>();
		
		String ret= null;
		if (emails!=null) {
			emailsList= Arrays.asList(emails.split("\\s*;\\s*"));
			Set<String> emailSet = new HashSet<String>(emailsList);
			ret=StringUtils.join(emailSet, ';');
		}
		return ret;
	}
	
	private void makeEmails(boolean bMarkToSendMail,int selFunction) {
			
		String sGenderMng="";
		Long id =  getEpisode().getId();
		try {
			Episode episodeToInvite=episodeService.getEpisodeById(id); 
			Boolean onDemoMode=Boolean.parseBoolean(PropertiesUtils.getInstance().readProperty("mail.debug","false"));
			
			if (episodeToInvite != null) {
				if ((selFunction % 2)==0) {
					try {
						System.out.println("Creo Email Lista...");
		         
			            EmailQueue emailToSend = new EmailQueue(); 
			            
			            emailToSend.setFromEmail(PropertiesUtils.getInstance().readProperty("mail.from",null));
//			            String sEmailTo= makeEmailAddress(episodeToInvite.getStudio().getProductionCenter().getEmail());
			            List<String> emailDistributionList = new ArrayList<String>();
			            episodeToInvite.getStudio().getProductionCenter().getDistributionList().getDistributionEmails().forEach(x -> emailDistributionList.add(x.getEmailContact().getManagerEmail()));
			            String sEmailTo = makeEmailAddress(String.join(";", emailDistributionList));
			            episodeToInvite.getProduction().getDistributionList().getDistributionEmails().forEach(x -> emailDistributionList.add(x.getEmailContact().getManagerEmail()));
			            String sDistributionList = makeEmailAddress(String.join(";", emailDistributionList));
			            
			            String sEmailCc = makeEmailAddress(episodeToInvite.getProduction().getAgency().getEmail()); //1036 g - 3141 pos - 30900 iso - 3489 ti - 683 dec
			            
			            if ((sEmailCc!=null)&&(!sEmailCc.isEmpty())) sEmailCc=makeDistinctEmailList(sEmailCc+";"+sDistributionList);
//			            if ((sEmailCc!=null)&&(!sEmailCc.isEmpty())) sEmailCc=makeDistinctEmailList(sEmailCc+";"+makeEmailAddress(episodeToInvite.getProduction().getManagerEmail()));
			            
			            emailToSend.setToEmail(onDemoMode?PropertiesUtils.getInstance().readProperty("mail.from",null):sEmailTo);
			            emailToSend.setCcEmail(onDemoMode?PropertiesUtils.getInstance().readProperty("mail.from",null):sEmailCc);
			            emailToSend.setSendDate(getDate());
			            if((emailToSend.getFromEmail()==null)||(emailToSend.getToEmail()==null)||(emailToSend.getCcEmail()==null)||(emailToSend.getFromEmail().isEmpty())||(emailToSend.getToEmail().isEmpty())||(emailToSend.getCcEmail().isEmpty())) {
			            	emailToSend.setSendStatus(4);
			            }else {
			            	emailToSend.setSendStatus(bMarkToSendMail?0:4);
			            }
			            emailToSend.setPriority(0);
			            String subject = PropertiesUtils.getInstance().readProperty("mail.subject1");
			            subject =subject.replaceAll("\\$1", episodeToInvite.getDateFromOnlyDate());
			            subject =subject.replaceAll("\\$2", episodeToInvite.getDateFromOnlyHours());
			            subject =subject.replaceAll("\\$3", episodeToInvite.getProduction().getName());
			            emailToSend.setSubject(subject);
			            String body = PropertiesUtils.getInstance().readProperty("mail.body1");
			            body=body.replaceAll("\\$1", episodeToInvite.getDateFromOnlyDate());
			            body=body.replaceAll("\\$2", episodeToInvite.getDateFromOnlyHours());
			            body=body.replaceAll("\\$3", episodeToInvite.getProduction().getName());
			            emailToSend.setBody(body);
			            emailToSend.setAttachFileName(PropertiesUtils.getInstance().readProperty("mail.attachFileName1"));
			            emailToSend.setEntityType(1);
			            emailToSend.setEntityID(episodeToInvite.getId());
			            //emailToSend.setPdfRepBase64Str(java.util.Base64.getEncoder().encodeToString(baos.toByteArray()));
						
			            emailQueueService.deleteEmailQueueByEntityID(emailToSend.getEntityID(),1);
			            emailToSend= emailQueueService.updateEmailQueue(emailToSend);
			            
						EpisodeStatusType nextStatus =new EpisodeStatusType();				
						nextStatus = episodeStatusTypeService.getEpisodeStatusTypeByIdStatus(7);
						getEpisode().setEpisodeStatus(nextStatus);
						setEpisode(episodeService.updateEpisode(getEpisode()));
						//addInfoMessage(getMessagesBoundle().getString("spectatorEpisode.sentList"));
						
					} catch (Exception e2) {
						e2.printStackTrace();
						System.out.println("Errore sendInvite");
						//addErrorMessage(getMessagesBoundle().getString("spectatorEpisode.sendInvite"), e2);
					}				
				}
				
				try {
					if ((selFunction % 3)==0) {
						System.out.println("Creo Email Inviti...");
						for (SpectatorEpisode selectItem : episodeToInvite.getSpectators()) {
				            switch (selectItem.getSpectator().getGender()==null?"-":selectItem.getSpectator().getGender().toString()) {
							case "M":
								sGenderMng="Egregio Signor";
								break;
							case "F":
								sGenderMng="Gentilissima Signora";
								break;
							case "-":
							default:
								sGenderMng="Spettabile";
								break;
							}
				            
				            EmailQueue emailToSend = new EmailQueue(); 
				            emailToSend.setFromEmail(PropertiesUtils.getInstance().readProperty("mail.from",null));
				            emailToSend.setToEmail(onDemoMode?PropertiesUtils.getInstance().readProperty("mail.from",null):makeEmailAddress(selectItem.getSpectator().getEmail()));
				            emailToSend.setCcEmail(onDemoMode?PropertiesUtils.getInstance().readProperty("mail.from",null):makeEmailAddress(selectItem.getProduction().getAgency().getEmail()));
				            emailToSend.setSendDate(getDate());
				            if((emailToSend.getFromEmail()==null)||(emailToSend.getToEmail()==null)||(emailToSend.getCcEmail()==null)||(emailToSend.getFromEmail().isEmpty())||(emailToSend.getToEmail().isEmpty())||(emailToSend.getCcEmail().isEmpty())) {
				            	emailToSend.setSendStatus(4);
				            }else {
				            	emailToSend.setSendStatus(bMarkToSendMail?0:4);
				            }
				            emailToSend.setPriority(0);
				            String subject = PropertiesUtils.getInstance().readProperty("mail.subject2");
				            subject =subject.replaceAll("\\$1", episodeToInvite.getDateFromOnlyDate());
				            subject =subject.replaceAll("\\$2", episodeToInvite.getDateFromOnlyHours());
				            subject =subject.replaceAll("\\$3", episodeToInvite.getProduction().getName());
				            subject =subject.replaceAll("\\$4", selectItem.getSpectator().getFullName().toUpperCase());
				            subject =subject.replaceAll("\\$5", episodeToInvite.getStudio().getProductionCenter().getName());
				            subject =subject.replaceAll("\\$6", sGenderMng);
				            emailToSend.setSubject(subject);
				            String body = PropertiesUtils.getInstance().readProperty("mail.body2");
				            body=body.replaceAll("\\$1", episodeToInvite.getDateFromOnlyDate());
				            body=body.replaceAll("\\$2", episodeToInvite.getDateFromOnlyHours());
				            body=body.replaceAll("\\$3", episodeToInvite.getProduction().getName());
				            body =body.replaceAll("\\$4", selectItem.getSpectator().getFullName().toUpperCase());
				            body =body.replaceAll("\\$5", episodeToInvite.getStudio().getProductionCenter().getName());
				            body =body.replaceAll("\\$6", sGenderMng);
				            emailToSend.setBody(body);
				            emailToSend.setAttachFileName(PropertiesUtils.getInstance().readProperty("mail.attachFileName2"));
				            emailToSend.setEntityType(2);
				            emailToSend.setEntityID(selectItem.getId());
				            
				            emailQueueService.deleteEmailQueueByEntityID(emailToSend.getEntityID(),2);
				            emailToSend= emailQueueService.updateEmailQueue(emailToSend);
						}					
					}
		//				Promuovo il prossimo stato in Lista Creata
					EpisodeStatusType nextStatus =new EpisodeStatusType();
					switch (selFunction) {
					case 2:
						nextStatus = episodeStatusTypeService.getEpisodeStatusTypeByIdStatus(7);
						break;
					case 3:
						nextStatus = episodeStatusTypeService.getEpisodeStatusTypeByIdStatus(6);
						break;
					case 6:
						nextStatus = episodeStatusTypeService.getEpisodeStatusTypeByIdStatus(6);
						break;
		
					default:
						break;
					}
					
					getEpisode().setEpisodeStatus(nextStatus);
					setEpisode(episodeService.updateEpisode(getEpisode()));
					//addInfoMessage(getMessagesBoundle().getString("spectatorEpisode.sentInvite"));
					// in base allo stato dell'evento selezionato popola la lista degli stati della puntata 
					//LoadStatusTypeList(episodeToEdit);
				} catch (Exception e) {
					//addErrorMessage(getMessagesBoundle().getString("spectatorEpisode.sendInvite") + e2);
					 System.out.println("makeEmails..."+e);
					 e.printStackTrace();
				}
			}
		} catch (Exception e) {
			System.out.println("makeEmails..."+e);
			e.printStackTrace();
			
		}
	}

	public Episode getEpisode() {
		return episode;
	}

	public void setEpisode(Episode episode) {
		this.episode = episode;
	}

	@Override
	public void resendEmail() {
		resendEmailService(false);
	}

	@Override
	public void resendEmail(boolean recreateList) {
		resendEmailService(recreateList);
	}
	
	
	@Override
	public void resendEmail(Episode episode) {
		setEpisode(episode);
		resendEmailService(false);
	}
	
	@Override
	public void resendEmail(Episode episode, boolean recreateList) {
		setEpisode(episode);
		resendEmailService(recreateList);
	}
	
	private void resendEmailService(boolean recreateList) {
		try {
			EmailQueue emailToResend= emailQueueService.getEmailQueueByEntityID(getEpisode().getId(), 1);
			emailToResend.setSendStatus(0);
			emailToResend.setPriority(10);
			if (recreateList) emailToResend.setPdfRepBase64Str(null);
			emailToResend=emailQueueService.updateEmailQueue(emailToResend);
		} catch (Exception e) {
			System.out.println("resendEmailService..."+e);
		
		}
	}
	
	private StreamedContent getPDFDocumentService(Long entityId, int entityType) {
		
		String sFilename=null;
		int maxLength=10;
		
		DefaultStreamedContent file=new DefaultStreamedContent();		
		try {
			
			EmailQueue emailQueue= new EmailQueue();
			emailQueue=emailQueueService.getEmailQueueByEntityID(entityId, entityType);
			switch (entityType) {
			case 1:
				if (getEpisode().getProduction().getName().trim().length()>=maxLength) {
					sFilename= emailQueue.getAttachFileName().replaceAll("\\$", getEpisode().getProduction().getName().substring(0, maxLength).replace(" ", "_").toString().trim() + "_" + getEpisode().getDateFromOnlyDate().toString());	
				} else {
					sFilename= emailQueue.getAttachFileName().replaceAll("\\$", getEpisode().getProduction().getName().replace(" ", "_").toString().trim() + "_" + getEpisode().getDateFromOnlyDate().toString());	
				}
				break;
			case 2:
				if (getEpisode().getProduction().getName().trim().length()>=maxLength) {
					sFilename= emailQueue.getAttachFileName().replaceAll("\\$", getEpisode().getProduction().getName().substring(0, maxLength).replace(" ", "_").toString().trim() + "_" + getEpisode().getDateFromOnlyDate().toString());	
				}else {
					sFilename= emailQueue.getAttachFileName().replaceAll("\\$", getEpisode().getProduction().getName().replace(" ", "_").toString().trim() + "_" + getEpisode().getDateFromOnlyDate().toString());					
				}
				break;
			default:
				break;
			}
			
			if (emailQueue.getPdfRepBase64Str()==null) {
				emailQueue.setSendStatus(3);
				emailQueue=emailQueueService.updateEmailQueue(emailQueue);
			}

			if (emailQueue.getSendStatus()!=3) {
//				String sFilename= emailQueue.getAttachFileName().replaceAll("\\$", getEpisode().getProduction().getName().substring(0, 10).replace(" ", "_").toString().trim() + "_" + getEpisode().getDateFromOnlyDate().toString());
				InputStream myInputStream = new ByteArrayInputStream(Base64.decode(emailQueue.getPdfRepBase64Str()));
				file = new DefaultStreamedContent(myInputStream);
				file.setName(sFilename);				
			}
		} catch(Exception e) {
			System.out.println("resendEmailService..."+e);
			e.printStackTrace();
//			addErrorMessage(getMessagesBoundle().getString("spectatorEpisode.pdfInviteList") + e);
		}
		return file;
	}

	@Override
	public StreamedContent getPDFList() {
		return getPDFDocumentService(getEpisode().getId(),1);
	}

	@Override
	public StreamedContent getPDFList(Episode episode) {
		setEpisode(episode);
		return getPDFDocumentService(getEpisode().getId(),1);
	}
	
	@Override
	public StreamedContent getPDFInvite() {
		return getPDFDocumentService(spectatorEpisode.getId(), 2);
	}

	@Override
	public StreamedContent getPDFInvite(SpectatorEpisode spectatorEpisode) {
		setSpectatorEpisode(spectatorEpisode);
		return getPDFDocumentService(spectatorEpisode.getId(), 2);
	}
	
	@Override
	public boolean base64InviteListNotAvailable(Episode episode) {
		
		boolean ret=false;
		
		setEpisode(episode);
		try {
			EmailQueue emailQueue= emailQueueService.getEmailQueueByEntityID(getEpisode().getId(), 1);
			ret=emailQueue.getPdfRepBase64Str().trim().isEmpty();			
		} catch (Exception e) {
			ret=false; 
		}
		return ret;
	}

	
	@Override
	public void purgeEmail(int entityTypeID, Long entityId ) {
		emailQueueService.deleteEmailQueueByEntityID(entityId, entityTypeID);
	}
	
	@Override
	public void purgeEmail(Episode episode) {
		if (!episode.equals(null)) {
			List<SpectatorEpisode> spectators = episode.getSpectators();
			try {
				emailQueueService.deleteEmailQueueByEntityID(episode.getId(),1);				
			} catch (Exception e) {
				System.out.println("purgeEmail-List Document..."+e);
			}
			for (SpectatorEpisode spectatorEpisode : spectators) {
				try {
					emailQueueService.deleteEmailQueueByEntityID(spectatorEpisode.getId(), 2);	
				} catch (Exception e) {
					System.out.println("purgeEmail-Invite Document..."+e);
				}
			}			
		}
//		emailQueueService.deleteEmailQueueByEntityID(entityId, entityTypeID);
	}
	
	@Override
	public void sendInvites(Episode episode) {
		for (SpectatorEpisode spectator: episode.getSpectators()) {
			sendInvite(spectator);
		}
	}

	@Override
	public void sendInvite(SpectatorEpisode spectatorEpisode) {
		try {
			EmailQueue emailToResend= emailQueueService.getEmailQueueByEntityID(spectatorEpisode.getId(), 2);
			emailToResend.setSendStatus(0);
			emailToResend.setPriority(10);
			emailToResend=emailQueueService.updateEmailQueue(emailToResend);
		} catch (Exception e) {
			System.out.println("resendEmailService..."+e);
			e.printStackTrace();
		}	
	}
	
	@Override
	public void removeSpectatorFromList(SpectatorEpisode spectatorEpisode) {
		purgeEmail(2, spectatorEpisode.getSpectator().getId());
		resendEmail(spectatorEpisode.getEpisode(),true);
	}
	
	private SpectatorEpisode getSpectatorEpisode() {
		return spectatorEpisode;
	}

	private void setSpectatorEpisode(SpectatorEpisode spectatorEpisode) {
		this.spectatorEpisode = spectatorEpisode;
	}

	public int getEmailSendStatus(SpectatorEpisode spectatorEpisode) {
		try {
			EmailQueue emailToEnquiry= emailQueueService.getEmailQueueByEntityID(spectatorEpisode.getId(), 2);
			return emailToEnquiry.getSendStatus();
		} catch (Exception e) {
			System.out.println("resendEmailService..."+e);
			e.printStackTrace();
			return -1;
		}
		
	}









}
