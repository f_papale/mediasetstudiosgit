package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.EpisodeDAO;
import com.crismasecurity.mediasetStudios.core.entity.Episode;
import com.crismasecurity.mediasetStudios.core.entity.SpectatorEpisode;
import com.crismasecurity.mediasetStudios.core.services.EpisodeService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

/**
 * @author Gio
 *
 */
@Service
@Transactional
public class EpisodeServiceImpl implements EpisodeService {

	@Autowired
	private EpisodeDAO episodeDAO;
	

	@PostConstruct
	public void init() {
	}
	
	@Override
	public List<Episode> getEpisode() {
		return episodeDAO.getFinder().addOrderAsc("id").setDistinct().list();
	}

	@Override
	public Episode addEpisode(Episode r) throws Exception {
		return episodeDAO.create(r);
	}

	@Override
	public Episode updateEpisode(Episode r) throws Exception {
		return episodeDAO.update(r);
	}

	@Override
	public void deleteEpisode(long id) {
		episodeDAO.delete(id);
	}

	@Override
	public Finder<Episode> getEpisodeFinder() {
		return episodeDAO.getFinder();
	}

	@Override
	public Episode getEpisodeById(Long id) {
		
		Episode com = episodeDAO.getFinder().and("id").eq(id).setDistinct().result();
		return com;
	}

	@Override
	public List<Episode> getEpisodeLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
//		Nell'ultimo parametro mettere i seguenti valori a seconda che si voglia effettuare la DISTINCY o meno. 
//		Questo risulta necessario in quei casi in cui la tabella visualizzata non effettua l'ordinamento per colonna 
//		e da la un errore sulla presenza della DISTINCT
//		false = non mette il distinct
//		true = mette il distinct		
		return episodeDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, false);
	}

	@Override
	public long getEpisodeLazyLoadingCount(Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return episodeDAO.sizeOfListWithField(filters, filtriInOr, null);
	}

	@Override
	public List<Episode> getEpisodesByIdStatus(int id) {
		List<Episode> ret = getEpisodeFinder().and("episodeStatus.idStatus").eq(id).list(); 
		return ret; 
	}

	@Override
	public List<SpectatorEpisode> getSpectatorEpisodeByEpisodeId(Long id) {
		List<SpectatorEpisode> com = episodeDAO.getFinder().and("id").eq(id).setDistinct().result().getSpectators();
		com.size();
		return com;
	}
	
	@Override
	public List<Episode> getEpisodesBeforeThan(Date dateRef) {
		List<Episode> com = episodeDAO.getFinder().and("dateTo").le(dateRef).and("episodeStatus.idStatus").ne(5).setDistinct().list();
		com.size();
		return com;
	}
}
