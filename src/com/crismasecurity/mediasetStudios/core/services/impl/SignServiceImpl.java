package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.entity.Location;
import com.crismasecurity.mediasetStudios.core.services.SignListener;
import com.crismasecurity.mediasetStudios.core.services.SignService;

/**
 * @author Gio
 *
 */
@Service
@Transactional
public class SignServiceImpl implements SignService {

	
	private List<SignListener> listeners;
	
	@PostConstruct
	public void init() {
		listeners = new CopyOnWriteArrayList<SignListener>();
//		listeners = Collections.synchronizedList(new ArrayList<SignListener>());
	}
	

	@Override
	public void addSignListener(SignListener beanInstance) {
		listeners.add(beanInstance);
	}

	@Override
	public void removeSignListener(SignListener beanInstance) {

		// rimuovo tutti quelli con lo stesso desk associato
//		List<SignListener> toremove = new ArrayList<SignListener>();
//		for (SignListener l : listeners) {
//			if (l.getDesk()!=null && beanInstance.getDesk()!=null && l.getDesk().getId().equals(beanInstance.getDesk().getId())) {
//				toremove.add(l);
//			}
//		}
//		listeners.removeAll(toremove);
		listeners.remove(beanInstance);
	}
	
	@Override
	public List<SignListener> getSignListeners() {
		return listeners;
	}


	@Override
	public void signReceived(Location desk, String sign) {	
		
		for (SignListener li : listeners) {
			li.signReceived(desk, sign);
		}
		
	}
	
//	@Override
//	public  void signReceived(Location desk, String sign) {		
//		synchronized (listeners) {
//			Iterator<SignListener> it = listeners.iterator();
//			while (it.hasNext()) {
//				SignListener li = it.next();
//				li.signReceived(desk, sign);
//			}
//		}
//	}
}
