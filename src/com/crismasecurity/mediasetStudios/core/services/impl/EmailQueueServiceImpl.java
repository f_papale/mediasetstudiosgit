package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.EmailQueueDAO;
import com.crismasecurity.mediasetStudios.core.entity.EmailQueue;
import com.crismasecurity.mediasetStudios.core.services.EmailQueueService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

/**
 * @author Gio
 *
 */
@Service
@Transactional
public class EmailQueueServiceImpl implements EmailQueueService {

	@Autowired
	private EmailQueueDAO emailQueueDAO;

	@PostConstruct
	public void init() {
	}

	@Override
	public List<EmailQueue> getEmailQueue() {
		return emailQueueDAO.getFinder().addOrderAsc("id").setDistinct().list();
	}


	@Override
	public EmailQueue addEmailQueue(EmailQueue r) throws Exception {
		return emailQueueDAO.create(r);
	}


	@Override
	public EmailQueue updateEmailQueue(EmailQueue r) throws Exception {
		return emailQueueDAO.update(r);
	}

	@Override
	public void deleteEmailQueue(long id) {
		emailQueueDAO.delete(id);
	}

	@Override
	public void purgeEmailQueue() {
		List<EmailQueue> lst = new ArrayList<EmailQueue>();
		lst = emailQueueDAO.getAll();
		for (EmailQueue emailQueue : lst) {
			deleteEmailQueue(emailQueue.getId());
		}
	}

	@Override
	public Finder<EmailQueue> getEmailQueueFinder() {
		return emailQueueDAO.getFinder();
	}

	@Override
	public EmailQueue getEmailQueueById(Long id) {
		
		EmailQueue com = emailQueueDAO.getFinder().and("id").eq(id).setDistinct().result();
		return com;
	}

	@Override
	public List<EmailQueue> getEmailQueueLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return emailQueueDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
	}

	@Override
	public long getEmailQueueLazyLoadingCount(Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return emailQueueDAO.sizeOfListWithField(filters, filtriInOr, null);
	}

	@Override
	public EmailQueue getEmailQueueByEntityID(Long entityId, int entitytypeID ) {
		EmailQueue com = emailQueueDAO.getFinder().and("entityType").eq(entitytypeID).and("entityID").eq(entityId).and("sendStatus").eq(1).setDistinct().result();
		if (com==null) {
			com=emailQueueDAO.getFinder().and("entityType").eq(entitytypeID).and("entityID").eq(entityId).and("sendStatus").eq(2).setDistinct().result();
		}
		return com;
	}

	@Override
	public void deleteEmailQueueByEntityID(Long entityId, int entitytypeID) {
		List<EmailQueue> com = new ArrayList<EmailQueue>();
		com = emailQueueDAO.getFinder().and("entityType").eq(entitytypeID).and("entityID").eq(entityId).list();
		for (EmailQueue emailQueue : com) {
			this.deleteEmailQueue(emailQueue.getId());
		}
	}

	@Override
	public  List<EmailQueue> getEmailQueueListByEntityID(Long entityId, int entitytypeID) {
		List<EmailQueue> lst = new ArrayList<EmailQueue>();
		lst = emailQueueDAO.getFinder().and("entityType").eq(entitytypeID).and("entityID").eq(entityId).and("sendStatus").eq(1).list();
		return lst;
	}

	@Override
	public List<EmailQueue> getEmailQueueToSend(int topSize, boolean onlyList) {
		if (onlyList) {
			List <EmailQueue> com = emailQueueDAO.getFinder().and("entityType").eq(1).and("sendStatus").eq(0).addOrderDesc("priority").list();
			com.addAll(emailQueueDAO.getFinder().and("entityType").eq(1).and("sendStatus").eq(2).addOrderDesc("priority").list());
			com.addAll(emailQueueDAO.getFinder().and("entityType").eq(1).and("sendStatus").eq(3).addOrderDesc("priority").list());	
			com.addAll(emailQueueDAO.getFinder().and("entityType").eq(1).and("sendStatus").eq(4).addOrderDesc("priority").list());
			if ((com.size()>0)&&(topSize<=com.size())) {
				if (com.size()>1) {
					return com.subList(0, topSize);
				}else {
					return com;
				}
			} else
				return com;
//			return ((com.size()>0)&&(topSize<=com.size()))?com.subList(0, topSize):com;
		}else {
			List <EmailQueue> com = emailQueueDAO.getFinder().and("sendStatus").eq(0).addOrderDesc("priority").list();
			com.addAll(emailQueueDAO.getFinder().and("sendStatus").eq(2).addOrderDesc("priority").list());
			com.addAll(emailQueueDAO.getFinder().and("sendStatus").eq(3).addOrderDesc("priority").list());	
			com.addAll(emailQueueDAO.getFinder().and("sendStatus").eq(4).addOrderDesc("priority").list());
			
			if ((com.size()>0)&&(topSize<=com.size())) {
				if (com.size()>1) {
					return com.subList(0, topSize);
				}else {
					return com;
				}
			} else
				return com;			
//			return ((com.size()>0)&&(topSize<=com.size()))?com.subList(0, topSize):com;
		}
	}
}
