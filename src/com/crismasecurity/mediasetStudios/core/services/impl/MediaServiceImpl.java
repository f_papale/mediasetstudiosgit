package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.MediaDAO;
import com.crismasecurity.mediasetStudios.core.entity.Media;
import com.crismasecurity.mediasetStudios.core.services.MediaService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

/**
 * @author Gio
 *
 */
@Service
@Transactional
public class MediaServiceImpl implements MediaService {


	@Autowired
	private MediaDAO mediaDAO;
	
	
	
	@PostConstruct
	public void init() {
	}

	@Override
	public List<Media> getMedia() {
		return mediaDAO.getFinder().addOrderAsc("id").setDistinct().list();
	} 


	@Override
	public Media addMedia(Media r) throws Exception {
		return mediaDAO.create(r);
	}

	@Override
	public Media updateMedia(Media r) throws Exception {
		return mediaDAO.update(r);
	}

	@Override
	public void deleteMedia(long id) {
		mediaDAO.delete(id);
	}

	@Override
	public Finder<Media> getMediaFinder() {
		return mediaDAO.getFinder();
	}

	@Override
	public Media getMediaById(Long id) {
		
		Media com = mediaDAO.getFinder().and("id").eq(id).setDistinct().result();
		return com;
	}

	@Override
	public List<Media> getMediaLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return mediaDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, true);
	}

	@Override
	public long getMediaLazyLoadingCount(Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return mediaDAO.sizeOfListWithField(filters, filtriInOr, null);
	}

	@Override
	public Media getMediaByCode(String code) {
		return mediaDAO.getFinder().and("code").eq(code).setDistinct().result();
	}
}
