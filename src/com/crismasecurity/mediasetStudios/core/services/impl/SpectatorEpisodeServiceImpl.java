

package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.LiberatoriaDAO;
import com.crismasecurity.mediasetStudios.core.dao.SpectatorDAO;
import com.crismasecurity.mediasetStudios.core.dao.SpectatorEpisodeDAO;
import com.crismasecurity.mediasetStudios.core.entity.Liberatoria;
import com.crismasecurity.mediasetStudios.core.entity.Spectator;
import com.crismasecurity.mediasetStudios.core.entity.SpectatorEpisode;
import com.crismasecurity.mediasetStudios.core.services.SpectatorEpisodeService;
import com.crismasecurity.mediasetStudios.core.utils.DateUtils;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

@Service
@Transactional
public class SpectatorEpisodeServiceImpl implements SpectatorEpisodeService {

	@PersistenceContext (name="entityManagerFactory", unitName="entityManagerFactory")
	@Qualifier(value = "entityManagerFactory")
    protected EntityManager em;

    public void setEntityManager(EntityManager em) {
        this.em = em;
    }
    
    public EntityManager getEntityManager() {
        return em;
    }

	@Autowired
	private SpectatorEpisodeDAO spectatorEpisodeDAO;	
	
	@Autowired
	private SpectatorDAO spectatorDAO;	
	@Autowired
	private LiberatoriaDAO  liberatoriaDAO;	
	
	@PostConstruct
	public void init() {
	}
	
	@Override
	public List<SpectatorEpisode> getSpectatorEpisode() {
		return spectatorEpisodeDAO.getFinder().addOrderAsc("id").setDistinct().list();
	}

	@Override
	public SpectatorEpisode addSpectatorEpisode(SpectatorEpisode r) throws Exception {
		SpectatorEpisode aa = spectatorEpisodeDAO.create(r);
		if (aa!=null && aa.getLiberatoria()!=null) aa.getLiberatoria().toString();
		return aa;
	}

	@Override
	public SpectatorEpisode updateSpectatorEpisode(SpectatorEpisode r) throws Exception {
		SpectatorEpisode aa =spectatorEpisodeDAO.update(r);
		if (aa!=null && aa.getLiberatoria()!=null) aa.getLiberatoria().toString();
		return aa;
	}

	@Override
	public void deleteSpectatorEpisode(long id) {
		spectatorEpisodeDAO.delete(id);
	}

	@Override
	public Finder<SpectatorEpisode> getSpectatorEpisodeFinder() {
		return spectatorEpisodeDAO.getFinder();
	}

	@Override
	public SpectatorEpisode getSpectatorEpisodeById(Long id) {
		SpectatorEpisode aa = spectatorEpisodeDAO.getFinder().and("id").eq(id).setDistinct().result();
		if (aa!=null && aa.getLiberatoria()!=null) aa.getLiberatoria().toString();
		return aa;
	}

	@Override
	public List<SpectatorEpisode> getSpectatorEpisodeLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String, Object> filters, boolean distinct) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return spectatorEpisodeDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, distinct);
	}

	@Override
	public long getSpectatorEpisodeLazyLoadingCount(Map<String, Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return spectatorEpisodeDAO.sizeOfListWithField(filters, filtriInOr, null);
	}
	
	@Override
	public SpectatorEpisode getMostRecentParticipation(SpectatorEpisode spectatorEpisode, Date beforeDateRef, int maxDays){		
		List<SpectatorEpisode> res = new ArrayList<SpectatorEpisode>();
//		Creo la lista delle partecipazione dello spettatore orditante dalla più recente
//		Prendo la lista delle partecipazioni piu recenti di 180 gg per il quale lo spettatore ha effettuato il checkIn 
		res = spectatorEpisodeDAO.getFinder().and("spectator.id").eq(spectatorEpisode.getSpectator().getId()).and("checkin").eq(true).and("signDate").ge(DateUtils.addDays(beforeDateRef, -maxDays)).addOrderDesc("signDate").setDistinct().list(); 
//		Se non ha mai partecipato questa è la prima volta e restituisco se stessa.
		if ((res==null) || res.isEmpty() || res.size()==0 ) 
			return spectatorEpisode;
//		Se ne trovo una restituisco la più recente.
		else {
			return res.get(0);
		}
	}
	
	@Override
	public Liberatoria addLiberatoria(Liberatoria r) throws Exception {
		return liberatoriaDAO.create(r);
	}
	
	@Override
	public Liberatoria updateLiberatoria(Liberatoria r) throws Exception {
		return liberatoriaDAO.update(r);
	}
	
	
	@Override
	public void deleteLiberatoria(long id) throws Exception {
		liberatoriaDAO.delete(id);
	}

	@Override
	public void wipeLiberatoriaBySpectatorId(long idSpectator) {
		List<Liberatoria> libs = liberatoriaDAO.getFinder().and("spectatorEpisode.spectator.id").eq(idSpectator).setDistinct().list();
		if (libs!=null) {
			for (Liberatoria l : libs) {
				liberatoriaDAO.delete(l.getId());
			}
		}
		
	}
}
