package com.crismasecurity.mediasetStudios.core.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crismasecurity.mediasetStudios.core.dao.AgencyDAO;
import com.crismasecurity.mediasetStudios.core.dao.AgencySpectatorDAO;
import com.crismasecurity.mediasetStudios.core.dao.SpectatorDAO;
import com.crismasecurity.mediasetStudios.core.entity.Agency;
import com.crismasecurity.mediasetStudios.core.entity.AgencySpectator;
import com.crismasecurity.mediasetStudios.core.entity.AgencySpectatorId;
import com.crismasecurity.mediasetStudios.core.entity.BlackListSpectator;
import com.crismasecurity.mediasetStudios.core.entity.Spectator;
import com.crismasecurity.mediasetStudios.core.services.AgencyService;
import com.crismasecurity.mediasetStudios.core.services.AgencySpectatorService;

import com.crismasecurity.mediasetStudios.core.services.BlackListSpectatorService;
import com.widee.base.dao.base.OrArgument;
import com.widee.base.hibernate.criteria.Finder;

/**
 * @author Gio
 *
 */
@Service
@Transactional
public class AgencySpectatorServiceImpl implements AgencySpectatorService {


	@Autowired
	private AgencySpectatorDAO agencySpectatorDAO;
	
	@Autowired 
	private BlackListSpectatorService blackListSpectatorService;
	
	@Autowired 
	private AgencyDAO agencyDAO;

	@Autowired 
	private SpectatorDAO spectatorDAO;
	
	@Autowired
	private AgencyService agencyService;
	
	
	@PostConstruct
	public void init() {
	}


	@Override
	public List<AgencySpectator> getAgencySpectator() {
		return agencySpectatorDAO.getFinder().addOrderAsc("idAgencySpectator").setDistinct().list();
	}


	@Override
	public AgencySpectator addAgencySpectator(AgencySpectator r) throws Exception {
		return agencySpectatorDAO.create(r);
	}


	@Override
	public AgencySpectator updateAgencySpectator(AgencySpectator r) throws Exception {
		return agencySpectatorDAO.update(r);
	}

	@Override
	public void deleteAgencySpectator(AgencySpectatorId id) {
		agencySpectatorDAO.delete(id);
	}

	@Override
	public Finder<AgencySpectator> getAgencySpectatorFinder() {
		return agencySpectatorDAO.getFinder();
	}

	@Override
	public AgencySpectator getAgencySpectatorById(Long id) {
		
		AgencySpectator com = agencySpectatorDAO.getFinder().and("id").eq(id).setDistinct().result();
		return com;
	}

	@Override
	public List<AgencySpectator> getAgencySpectatorLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
//		Nell'ultimo parametro mettere i seguenti valori a seconda che si voglia effettuare la DISTINCY o meno. 
//		Questo risulta necessario in quei casi in cui la tabella visualizzata non effettua l'ordinamento per colonna 
//		e da la un errore sulla presenza della DISTINCT
//		false = non mette il distinct
//		true = mette il distinct
		return agencySpectatorDAO.paginationListWithField(first, pageSize, filters, filtriInOr, sortField, sortOrder, null, false);
	}

	@Override
	public long getAgencySpectatorLazyLoadingCount(Map<String,Object> filters) {
		List<OrArgument> filtriInOr = new ArrayList<OrArgument>();
		return agencySpectatorDAO.sizeOfListWithField(filters, filtriInOr, null);
	}


	@Override
	public boolean removeAvailabilityUnwantedSpectators() {
		boolean bReturn=true;
		List<BlackListSpectator> lst = blackListSpectatorService.getBlackListSpectator();
		Agency aDummy = agencyService.getDummyAgency();

		for (BlackListSpectator blackListSpectator : lst) {
			List<AgencySpectator> dummy =  agencySpectatorDAO.getFinder().and("spectator.id").eq(blackListSpectator.getSpectator().getId()).and("agency.idAgency").ne(aDummy.getIdAgency()).setDistinct().list();
			for (AgencySpectator aSpectator : dummy) {
				try {
					agencySpectatorDAO.delete(aSpectator.getId());	
				} catch (Exception e) {
					bReturn=false;
				}			
			}	
		}
		return bReturn;
	}
	
	@Override
	public boolean isAgencySpectator(Long idAgency, Long idSpectator) {
		boolean result = ! agencySpectatorDAO.getFinder().and("agency.idAgency").eq(idAgency).and("spectator.id").eq(idSpectator).list().isEmpty();
		return result;
	}
	
	@Override
	public boolean isSpectatorInDummyAgency(Spectator spectator) {
		Agency agencyDummy = null;
		boolean result; 
		try {
			agencyDummy = agencyDAO.getFinder().and("hidden").eq(true).setDistinct().result();
		} catch (Exception e) {
			agencyDummy=null;
		}		
		result = ! agencySpectatorDAO.getFinder().and("agency.idAgency").eq(agencyDummy.getIdAgency()).and("spectator.id").eq(spectator.getId()).list().isEmpty();
		return result;
	}
		
}
