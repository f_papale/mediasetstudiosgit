package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.AgencyUser;
import com.crismasecurity.mediasetStudios.core.entity.AgencyUserId;
import com.widee.base.hibernate.criteria.Finder;

public interface AgencyUserService {
	public List<AgencyUser> getAgencyUser();
	public AgencyUser addAgencyUser(AgencyUser r) throws Exception;
	public AgencyUser updateAgencyUser(AgencyUser r) throws Exception;
	public void deleteAgencyUser(AgencyUserId id);
	public Finder<AgencyUser> getAgencyUserFinder();
	public AgencyUser getAgencyUserById(AgencyUserId id);
	public List<AgencyUser> getAgencyUserLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getAgencyUserLazyLoadingCount(Map<String,Object> filters);
	public AgencyUser getAgencyUserByIdString(String idString);
	public void presetAgencyUser(long userId);

}