package com.crismasecurity.mediasetStudios.core.services;

import java.util.List;
import java.util.Map;

import com.crismasecurity.mediasetStudios.core.entity.EmailContact;
import com.widee.base.hibernate.criteria.Finder;


/**
 * 
 * Class name: EmailContactService
 *
 * Description: Interfaccia EmailContactService 
 * 
 *
 * Company: 
 *
 * @author 	Fabrizio Papale
 * @date 	04/jul/2018
 *
 */


public interface EmailContactService {
	
	public List<EmailContact> getEmailContact();
	public EmailContact addEmailContact(EmailContact r) throws Exception;
	public EmailContact updateEmailContact(EmailContact r) throws Exception;
	public void deleteEmailContact(long id);
	public Finder<EmailContact> getEmailContactFinder();
	public EmailContact getEmailContactById(Long id);
	public List<EmailContact> getEmailContactLazyLoading(int first, int pageSize, String sortField, int sortOrder, Map<String,Object> filters);
	public long getEmailContactLoadingCount(Map<String,Object> filters);
}
