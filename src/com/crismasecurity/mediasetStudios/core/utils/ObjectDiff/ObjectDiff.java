package com.crismasecurity.mediasetStudios.core.utils.ObjectDiff;


import java.util.List;

import org.javers.core.Javers;
import org.javers.core.JaversBuilder;
import org.javers.core.diff.Diff;
import org.javers.core.diff.changetype.ValueChange;
import org.javers.core.diff.changetype.PropertyChange;

public class ObjectDiff {

	private Javers javers;
	private String json=null; 
	private Diff diff;
	private Object objectOld, objectNew;
	
	
	 public ObjectDiff() {
		this.javers = JaversBuilder.javers().build();
	}
	
	public ObjectDiff(Object objectOld, Object objectNew) {
		// TODO Auto-generated constructor stub
		this.javers = JaversBuilder.javers().build();
		this.setJson(getDiff(objectOld,objectNew)); 		
	}
	
	public String getDiff (Object objectOld, Object objectNew) {
		this.objectOld= objectOld;
		this.objectNew= objectNew;
		try {
			this.diff = javers.compare(this.objectOld, this.objectNew);
			List<PropertyChange> changes = diff.getChangesByType(PropertyChange.class);
			return javers.getJsonConverter().toJson(changes);
		} catch (Exception e) {	
			return null;
		}
	}

	public String getJson() {
		return json;
	}

	private void setJson(String json) {
		this.json = json;
	}
	
	public String toString() {
		return getJson();
	}
}
