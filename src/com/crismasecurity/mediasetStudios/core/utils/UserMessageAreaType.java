package com.crismasecurity.mediasetStudios.core.utils;

/**
 * 
 * Class name: UserMessageAreaType
 *
 * Description: Aree del sistema. Servono per etichettare i messaggi utente per area e visualizzare solo i messaggi delle aree a cui il ruolo utente � abilitato. 
 * 
 *
 * Company: 
 *
 * @author 
 * @date 21/set/2012
 *
 */

public enum UserMessageAreaType {
	GESTIONE("Area Gestione", "Include all messages coming from Management operations."),
	UTENTE("Area Utente","Include all messages coming from user's operations");
	
	private String name;
	private String description;
	private UserMessageAreaType(String name, String description) {
		this.name=name;
		this.description=description;
	}
	public String getName() {
		return name;
	}
	public String getDescription() {
		return description;
	}
	
	public static UserMessageAreaType getByName(String name) {
		for (int i = 0; i < UserMessageAreaType.values().length; i++) {
			if (UserMessageAreaType.values()[i].getName().equals(name)) return UserMessageAreaType.values()[i];
		} 
		return null;
	}
}