package com.crismasecurity.mediasetStudios.core.utils.crypto;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

//import javaxt.utils.Base64;
import org.apache.commons.codec.binary.Base64;

public class CryptoUtil {

	private static final String ALGORITHM = "AES/ECB/PKCS5Padding";
    private static final byte[] KEY = "e_dai_echecazzo!".getBytes(); // la password deve essere 16/24/32 byte
    
    
    public static String encode(String string)  {
    	if (string==null) return "";
    	
    	Key key = new SecretKeySpec(KEY, "AES");
        try {
           Cipher c = Cipher.getInstance(ALGORITHM);
           c.init(Cipher.ENCRYPT_MODE, key);
           //return Base64.encodeBytes(c.doFinal(string.getBytes()));
           return Base64.encodeBase64String(c.doFinal(string.getBytes()));
        } catch (Exception e) {
           throw new RuntimeException(e);
        }
    }
    
    public static byte[] encode(byte[] mybyte)  {
    	
    	byte[] ret = "".getBytes();
    	if (mybyte.length==0) return ret;
    	
    	Key key = new SecretKeySpec(KEY, "AES");
        try {
           Cipher c = Cipher.getInstance(ALGORITHM);
           c.init(Cipher.ENCRYPT_MODE, key);
           ret=c.doFinal(mybyte);
           return ret;
        } catch (Exception e) {
           throw new RuntimeException(e);
        }
    }
    
    public static String decode(String string)  {
    	if (string==null) return "";
    	
    	Key key = new SecretKeySpec(KEY, "AES");
        try {
          Cipher c = Cipher.getInstance(ALGORITHM);
          c.init(Cipher.DECRYPT_MODE, key);
          //return new String(c.doFinal(Base64.decode(string)));
          return new String(c.doFinal(Base64.decodeBase64(string)));
        } catch (Exception e) {
          throw new RuntimeException(e);
        }
    }
    
    public static byte[] decode(byte[] mybyte)  {
    	byte[] ret = "".getBytes();
    	if (mybyte.length==0) return ret;
    	
    	Key key = new SecretKeySpec(KEY, "AES");
        try {
          Cipher c = Cipher.getInstance(ALGORITHM);
          c.init(Cipher.DECRYPT_MODE, key);
          //return new String(c.doFinal(Base64.decode(string)));
          ret= c.doFinal(mybyte);
          return ret;
        } catch (Exception e) {
          throw new RuntimeException(e);
        }
    }
}
