package com.crismasecurity.mediasetStudios.core.utils.crypto;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
/**
 * JPA Converter per criptare (in scrittura) e decriptare (in lettura) i dati sensibili
 * @author Gio
 *
 */
@Converter
public class JpaCryptoConverterByte implements AttributeConverter<byte[], byte[]> {

    @Override
    public byte[] convertToDatabaseColumn(byte[] data) {
      try {
    	  long tt = System.currentTimeMillis();
    	  byte[] enc = CryptoUtil.encode(data);
    	  System.out.println("tempo codifica ... " + (System.currentTimeMillis()-tt) + " ms");
         return enc;
      } catch (Exception e) {
         throw new RuntimeException(e);
      }
    }

    @Override
    public byte[] convertToEntityAttribute(byte[] endcodedData) {
      try {
    	  long tt = System.currentTimeMillis();
    	  byte[] dec = CryptoUtil.decode(endcodedData);
    	  System.out.println("tempo decodifica ... " + (System.currentTimeMillis()-tt) + " ms");
    	  
    	  return dec;
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    }
}