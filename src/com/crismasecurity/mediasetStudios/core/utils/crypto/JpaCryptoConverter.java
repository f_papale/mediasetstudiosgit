package com.crismasecurity.mediasetStudios.core.utils.crypto;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
/**
 * JPA Converter per criptare (in scrittura) e decriptare (in lettura) i dati sensibili
 * @author Gio
 *
 */
@Converter
public class JpaCryptoConverter implements AttributeConverter<String, String> {

    @Override
    public String convertToDatabaseColumn(String data) {
      //System.out.println("convertToDatabaseColumn ...");
      // do encryption
      try {
         return CryptoUtil.encode(data);
      } catch (Exception e) {
         throw new RuntimeException(e);
      }
    }

    @Override
    public String convertToEntityAttribute(String endcodedData) {
      //System.out.println("convertToEntityAttribute ...");
      // do decryption
      try {
    	  return CryptoUtil.decode(endcodedData);
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    }
}