package com.crismasecurity.mediasetStudios.core.utils.wipe;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.crismasecurity.mediasetStudios.core.services.BlackListSpectatorService;
import com.crismasecurity.mediasetStudios.core.services.SpectatorEpisodeService;
import com.crismasecurity.mediasetStudios.core.services.SpectatorService;
import com.crismasecurity.mediasetStudios.core.utils.MessageManager;
import com.crismasecurity.mediasetStudios.core.utils.PropertiesUtils;

//@Scope("session")
@Service("wipedataservice")
public class WipeDataService{

	@Autowired
	private SpectatorService spectatorService;
	@Autowired
	private SpectatorEpisodeService spectatorEpisodeService;
	@Autowired
	private BlackListSpectatorService blackListSpectatorService;
	private MessageManager messageMngr = new MessageManager(this.getClass()); 
	private int retainlastmonths; 
	
    public void runWipeData()  {

	    int idJob2Run = 1;
	    int sleepingTime = 0;
	    messageMngr.addInfoMessage("runWipeData Started...");
		retainlastmonths = Integer.parseInt(PropertiesUtils.getInstance().readProperty("backup.retainlastmonths", "6"));
	    sleepingTime=Integer.parseInt(PropertiesUtils.getInstance().readProperty("schedule.sleepTime","5")); 
	    	    
	    try {
	    	while (idJob2Run!=0){
	    		if (idJob2Run==1) {
		    		try {
						Thread.sleep(sleepingTime*1000);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
	    		}
	    		messageMngr.addInfoMessage ("runWipeData - Manage idJob2Run = "+idJob2Run+" ...");
		    	switch (idJob2Run) {
		    	case 1:
		    		if (retainlastmonths!=0) {
			    		WipeOldData(retainlastmonths);
						messageMngr.addInfoMessage ("runWipeData - Runned WipeOldData...");
		    		}
		    		else {
		    			messageMngr.addInfoMessage ("runWipeData - disabled...");
		    		}
					idJob2Run=0;
					break;
					
				default:
					break;
				}
		   }
		} catch (Throwable e) {
			messageMngr.addErrorMessage(e,"runWipeData - Other Error...");
	    }
    }
    
	private void WipeOldData(int retainlastmonths) {

		List<BigDecimal> lst = spectatorService.getSpectatorsID2Wipe(retainlastmonths);
		if (lst.size()>0) {	
			messageMngr.addDebugMessage("WipeOldData - started... - Wiping "+lst.size()+" spectators...");
			for (BigDecimal spectatorId : lst) {
				messageMngr.addDebugMessage("WipeOldData - preparing Spectator to remove...");
				removeData(spectatorId.longValue());
				messageMngr.addDebugMessage("WipeOldData Spectator removed...");

			}
		} else
			messageMngr.addInfoMessage("WipeOldData - INFO nothing to wipe...");
	}

	
	protected void removeData(long spectatorId) {
		try {
			if (blackListSpectatorService.isNotInBlackList(spectatorId)) {
				spectatorEpisodeService.wipeLiberatoriaBySpectatorId(spectatorId);
				spectatorService.deleteSpectator(spectatorId);							
			}
		} catch (Exception e) {
			messageMngr.addErrorMessage(e, "removeData - Wiping Spectator ID = " + spectatorId + " from database in error...");
			e.printStackTrace();
		}
	}    
}