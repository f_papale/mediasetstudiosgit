package com.crismasecurity.mediasetStudios.core.utils;

public enum MessageType {
	DEBUG, INFO, WARNING, ERROR
}
