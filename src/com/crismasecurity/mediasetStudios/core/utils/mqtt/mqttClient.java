package com.crismasecurity.mediasetStudios.core.utils.mqtt;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class mqttClient implements MqttCallback {
    MqttClient client;

    public mqttClient() {
    }
//    https://github.com/andypiper/paho-mqtt/tree/master/org.eclipse.paho.sample.mqttv3app/src/org/eclipse/paho/sample/mqttv3app
//    public static void main(String[] args) {
//        new mqttDemo().doDemo();
//    }

    public void doDemo() {
        try {
            client = new MqttClient("tcp://172.16.1.71:1883", "Sending");
            client.connect();
            client.setCallback(this);
            client.subscribe("foo");
            MqttMessage message = new MqttMessage();
            message.setPayload("A single message from my computer fff".getBytes());
            client.publish("foo", message);
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void connectionLost(Throwable cause) {
    }

    @Override
    public void messageArrived(String topic, MqttMessage message)
            throws Exception {
     System.out.println(message);   
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
    }

}