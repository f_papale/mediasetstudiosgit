package com.crismasecurity.mediasetStudios.core.utils.timers;


import java.util.Date;
import javax.annotation.PostConstruct;

import org.quartz.CronScheduleBuilder;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.PersistJobDataAfterExecution;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Service;

import com.crismasecurity.mediasetStudios.core.utils.PropertiesUtils;
import com.crismasecurity.mediasetStudios.core.utils.wipe.WipeDataService;


//  @Configuration 
//  @ComponentScan("com.crismasecurity")
@Service
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
public class RipetitiveJobsQuartzConfiguration {
	
	@Autowired
	SchedulerFactoryBean scheduler;
	
	@Autowired
	MailingQueueService mailQueueService;
	
	@Autowired
	WipeDataService wipeDataService; 
	
	@Autowired
	EngineStatusService engineStatusService;
	
	@Autowired
	UnwantedListService unwantedListService;
	
    // --------------------------------------------------------------------------------------------------
	// Primo esempio: definisco un job (methodInvokingJobDetailFactoryBean) passandogli nome Bean e metodo da chiamare (in questo caso è una classe semplice che è taggata come servizio)
	@Bean
	public MethodInvokingJobDetailFactoryBean engineStatusJob() {
		System.out.println("!!!!!!!!!!!!!!"+engineStatusService.toString());
		MethodInvokingJobDetailFactoryBean obj = new MethodInvokingJobDetailFactoryBean();
		obj.setTargetObject(engineStatusService);
		//obj.setTargetObject(serv);
		obj.setTargetMethod("runEngineStatus");
		//obj.setTargetMethod(method);
		obj.setName("runEngineStatus");
		//obj.setName(method); 
        obj.setConcurrent(false);
        try {
			obj.afterPropertiesSet();
		} catch (ClassNotFoundException | NoSuchMethodException e) {
			e.printStackTrace();
		}
		return obj;
	}
	
	@Bean
	public MethodInvokingJobDetailFactoryBean mailQueueJob() {
		System.out.println("!!!!!!!!!!!!!!"+mailQueueService.toString());
		MethodInvokingJobDetailFactoryBean obj = new MethodInvokingJobDetailFactoryBean();
		obj.setTargetObject(mailQueueService);
		//obj.setTargetObject(serv);
		obj.setTargetMethod("runMailQueueService");
		//obj.setTargetMethod(method);
		obj.setName("runMailQueueService");
		//obj.setName(method);
        obj.setConcurrent(false);
        try {
			obj.afterPropertiesSet();
		} catch (ClassNotFoundException | NoSuchMethodException e) {
			e.printStackTrace();
		}
		return obj;
	}
	
	@Bean
	public MethodInvokingJobDetailFactoryBean wipeDataJob() {
		System.out.println("!!!!!!!!!!!!!!"+wipeDataService.toString());
		MethodInvokingJobDetailFactoryBean obj = new MethodInvokingJobDetailFactoryBean();
		obj.setTargetObject(wipeDataService);
		//obj.setTargetObject(serv);
		obj.setTargetMethod("runWipeData");
		//obj.setTargetMethod(method);
		obj.setName("runWipeData");
		//obj.setName(method);
        obj.setConcurrent(false);
        try {
			obj.afterPropertiesSet();
		} catch (ClassNotFoundException | NoSuchMethodException e) {
			e.printStackTrace();
		}
		return obj;
	}
	
	@Bean
	public MethodInvokingJobDetailFactoryBean unwantedLoadJob() {
		System.out.println("!!!!!!!!!!!!!!" + unwantedListService.toString());
		MethodInvokingJobDetailFactoryBean obj = new MethodInvokingJobDetailFactoryBean();
		obj.setTargetObject(unwantedListService);
		//obj.setTargetObject(serv);
		obj.setTargetMethod("runUnwantedListLoad");
		//obj.setTargetMethod(method);
		obj.setName("runUnwantedListLoad");
		//obj.setName(method);
        obj.setConcurrent(false);
        try {
			obj.afterPropertiesSet();
		} catch (ClassNotFoundException | NoSuchMethodException e) {
			e.printStackTrace();
		}
		return obj;
	}
	
//	@Bean
//	public MethodInvokingJobDetailFactoryBean methodInvokingJobDetailFactoryBean(Object serv, String method) {
//		System.out.println("!!!!!!!!!!!!!!"+serv.toString());
//		MethodInvokingJobDetailFactoryBean obj = new MethodInvokingJobDetailFactoryBean();
//		//obj.setTargetObject(engineStatusService);
//		obj.setTargetObject(serv);
//		//obj.setTargetMethod("runEngineStatus");
//		obj.setTargetMethod(method);
//		obj.setName(method);
//        obj.setConcurrent(false);
//        try {
//			obj.afterPropertiesSet();
//		} catch (ClassNotFoundException | NoSuchMethodException e) {
//			e.printStackTrace();
//		}
//		return obj;
//	}
//	
	//Schedulo il job creato ogni notte alle 4
	@Bean
	public Trigger statusTriggerBean(){
//		System.out.println(methodInvokingJobDetailFactoryBean().getObject());
		String statusChangeSchedule = PropertiesUtils.getInstance().readProperty("schedule.changestatus","0/5 0 0 ? * * *");
		Trigger trigger = TriggerBuilder.newTrigger()
//			.forJob(methodInvokingJobDetailFactoryBean().getObject())
//            .withSchedule(SimpleScheduleBuilder.simpleSchedule().withMisfireHandlingInstructionFireNow())
//            .withSchedule(CronScheduleBuilder.cronSchedule("0/10 * * ? * * *").withMisfireHandlingInstructionDoNothing()) // Ogni 10 second1 di ogni minuto (cioè ogni 60 secondi)
            .withSchedule(CronScheduleBuilder.cronSchedule(statusChangeSchedule).withMisfireHandlingInstructionDoNothing()) // Ogni secondo 5 di ogni minuto (cioè ogni 60 secondi)
//            .withSchedule(CronScheduleBuilder.cronSchedule("0 0 2 ? * * *").withMisfireHandlingInstructionDoNothing()) // Ogni giorno alle 2 di notte
//            .startAt(startDate.getTime())
            .build();
		return trigger;
	}
	
	//Schedulo il job creato ogni notte alle 4
	@Bean
	public Trigger mailTriggerBean(){
//		System.out.println(methodInvokingJobDetailFactoryBean().getObject());
		String sendEmailSchedule = PropertiesUtils.getInstance().readProperty("schedule.sendemail","0/60 0 0 ? * * *");
		Trigger trigger = TriggerBuilder.newTrigger()
//			.forJob(methodInvokingJobDetailFactoryBean().getObject())
//            .withSchedule(SimpleScheduleBuilder.simpleSchedule().withMisfireHandlingInstructionFireNow())
//            .withSchedule(CronScheduleBuilder.cronSchedule("0/10 * * ? * * *").withMisfireHandlingInstructionDoNothing()) // Ogni 10 second1 di ogni minuto (cioè ogni 60 secondi)
				.withSchedule(CronScheduleBuilder.cronSchedule(sendEmailSchedule).withMisfireHandlingInstructionDoNothing()) // Ogni secondo 60 di ogni minuto (cioè ogni 60 secondi)
//            .withSchedule(CronScheduleBuilder.cronSchedule("0 0 2 ? * * *").withMisfireHandlingInstructionDoNothing()) // Ogni giorno alle 2 di notte
//            .startAt(startDate.getTime())
            .build();
		return trigger;
	}
	
	
	@Bean
	public Trigger wipeTriggerBean(){
//		System.out.println(methodInvokingJobDetailFactoryBean().getObject());
		String wipeDataSchedule = PropertiesUtils.getInstance().readProperty("schedule.wipedata","0 0 2 ? * * *");
		Trigger trigger = TriggerBuilder.newTrigger()
				.withSchedule(CronScheduleBuilder.cronSchedule(wipeDataSchedule).withMisfireHandlingInstructionDoNothing()) // Ogni giorno alle 2 di notte
            .build();
		return trigger;
	}
	
	
	@Bean
	public Trigger unwantedLoadTriggerBean(){
//		System.out.println(methodInvokingJobDetailFactoryBean().getObject());
		String unwantedLoadSchedule = PropertiesUtils.getInstance().readProperty("schedule.unwantedLoad","0 0 5 ? * * *");
		Trigger trigger = TriggerBuilder.newTrigger()
				.withSchedule(CronScheduleBuilder.cronSchedule(unwantedLoadSchedule).withMisfireHandlingInstructionDoNothing()) // Ogni giorno alle 5 di notte
            .build();
		return trigger;
	}
	
	@PostConstruct
	public void init() {
		System.out.println("RipetitiveJobsQuartzConfiguration - schedulo i trigger ripetitivi...");

		 // TODO da riabilitare
		try {
       	 Date scheduleStatusMng = scheduler.getScheduler().scheduleJob(engineStatusJob().getObject(), statusTriggerBean());
       	 System.out.println("***data job Staus: " + scheduleStatusMng);

       	 Date scheduleMailQueue = scheduler.getScheduler().scheduleJob(mailQueueJob().getObject(), mailTriggerBean());
       	 System.out.println("***data job Mail: " + scheduleMailQueue);
       	 
       	 Date scheduleWipeData = scheduler.getScheduler().scheduleJob(wipeDataJob().getObject(), wipeTriggerBean());
       	 System.out.println("***data job Wipe Data: " + scheduleWipeData);
       	 
       	 Date scheduleUnwantedLoadData = scheduler.getScheduler().scheduleJob(unwantedLoadJob().getObject(), unwantedLoadTriggerBean());
       	 System.out.println("***data job Unwanted List Loader: " + scheduleUnwantedLoadData);       	 
       	 
        } catch (Exception e) {
       	 	e.printStackTrace();
        }
	}
	
	
// Secondo esempio: definisco un job passandogli un bean che estende QuartzJobBean (con la possibilità quindi di passargli istanze di oggetti tramite la map) assegnandogli un gruppo e un nome
//	@Bean
//	public JobDetailFactoryBean jobDetailFactoryBean(){
//		JobDetailFactoryBean factory = new JobDetailFactoryBean();
//		factory.setJobClass(MyJobTwo.class);
//		Map<String,Object> map = new HashMap<String,Object>();
//		map.put("name", "RAM");
//		map.put(MyJobTwo.COUNT, 1);
//		factory.setJobDataAsMap(map);
//		factory.setGroup("mygroup");
//		factory.setName("myjob");
//		return factory;
//	}
//  Schedulo il Job ogni 1 minuto 
//	@Bean
//	public CronTriggerFactoryBean cronTriggerFactoryBean(){
//		CronTriggerFactoryBean stFactory = new CronTriggerFactoryBean();
//		stFactory.setJobDetail(jobDetailFactoryBean().getObject());
//		stFactory.setStartDelay(3000);
//		stFactory.setName("mytrigger");
//		stFactory.setGroup("mygroup");
//		stFactory.setCronExpression("0 0/1 * 1/1 * ? *");
//		return stFactory;
//	}
	/**
	 * Esempio di classe
	 * 
	 *  import org.quartz.DisallowConcurrentExecution;
	 *	import org.quartz.JobDataMap;
	 *	import org.quartz.JobExecutionContext;
	 *	import org.quartz.JobExecutionException;
	 *	import org.quartz.JobKey;
	 *	import org.quartz.PersistJobDataAfterExecution;
	 *	import org.springframework.scheduling.quartz.QuartzJobBean;
	 *	@PersistJobDataAfterExecution
	 *	@DisallowConcurrentExecution
	 *	public class MyJobTwo extends QuartzJobBean {
	 *		public static final String COUNT = "count";
	 *		private String name;
	 *	        protected void executeInternal(JobExecutionContext ctx) throws JobExecutionException {
	 *	    	   JobDataMap dataMap = ctx.getJobDetail().getJobDataMap();
	 *	    	   int cnt = dataMap.getInt(COUNT);
	 *		   JobKey jobKey = ctx.getJobDetail().getKey();
	 *		   System.out.println(jobKey+": "+name+": "+ cnt);
	 *		   cnt++;
	 *		   dataMap.put(COUNT, cnt);
	 *	        }
	 *		public void setName(String name) {
	 *			this.name = name;
	 *		}
	 *	} 
	 *
	 */
	

	// SCHEDULO I TRIGGER CREATI
//	@Bean
//	public SchedulerFactoryBean schedulerFactoryBean() {
//		SchedulerFactoryBean scheduler = new SchedulerFactoryBean();
//		scheduler.setTriggers(
//				//cronTriggerFactoryBean().getObject(),
//				simpleTriggerFactoryBean().getObject()
//		);
//		return scheduler;
//	}
} 