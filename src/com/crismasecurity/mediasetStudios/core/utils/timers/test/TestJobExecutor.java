/**
 * 
 */
package com.crismasecurity.mediasetStudios.core.utils.timers.test;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.crismasecurity.mediasetStudios.core.entity.Test;
import com.crismasecurity.mediasetStudios.core.mail.javamail.EmailSender;
import com.crismasecurity.mediasetStudios.core.services.TestService;


/**
 * 
 * Class name: AdamTimerJobExecutor
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author Crisma
 * @date 12/feb/2013
 *
 */
public class TestJobExecutor implements org.quartz.Job {

	private Log logger = LogFactory.getLog(TestJobExecutor.class);
	
    private TestService testService;
    private Test doc;
    private EmailSender mailSender;

    
	/* (non-Javadoc)
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
	 */
	public synchronized void execute(JobExecutionContext context) throws JobExecutionException {
		
		
		logger.info("!!! TestJobExecutor - execute job : " + context.getJobDetail().getKey().getName());
		
		try {
			JobDataMap dataMap = context.getJobDetail().getJobDataMap();

			testService = (TestService)dataMap.get("testService");
			doc = (Test)dataMap.get("testActionItem");
			mailSender = (EmailSender)dataMap.get("mailSender");



			// FIXME !!!!!!!!!!!!!!!!!!!!!  faccio qualcosa!!!!!!!!!!!!!!!!!!!
			System.out.println("Eseguo schedulazione per " + doc);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		/*logger.info("ScheduledFireTime:" + context.getScheduledFireTime() );
		logger.info("FireTime:" + context.getFireTime());
		logger.info("NextFireTime:" + context.getNextFireTime() );
		logger.info("PreviousFireTime:" + context.getPreviousFireTime() );
		logger.info("ResourcesMonitoringJobExecutor - end!!!");*/
	}
    
    
	

	
}
