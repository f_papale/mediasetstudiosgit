/**
 * 
 */
package com.crismasecurity.mediasetStudios.core.utils.timers.test;

import static org.quartz.impl.matchers.GroupMatcher.jobGroupEquals;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.JobListener;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Service;

import com.crismasecurity.mediasetStudios.core.entity.Test;
import com.crismasecurity.mediasetStudios.core.mail.javamail.EmailSender;
import com.crismasecurity.mediasetStudios.core.services.TestService;


/**
 * Class name: CompanyDocumentsSchedulingManager
 *
 * Description: 
 * 
 *
 * Company: 
 *
 * @author Gio
 * @date 09/apr/2012
 *
 */
@Service
public class TestSchedulingManager  implements InitializingBean {

	
	@Autowired
	private SchedulerFactoryBean scheduler;
	@Autowired
	private TestService testService;
	@Autowired
	private EmailSender mailSender;
	
	
	
	private Log logger = LogFactory.getLog(TestSchedulingManager.class);
			
	public TestSchedulingManager() {
		logger.info("TestSchedulingManager constructor has been called.");
	}
	


	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	/**
	 * Questo viene eseguito automaticamente dopo il costruttore e dopo che l'autowiring � stato completato,
	 * cio� dopo la fine dell'inizializzazione del bean
	 * Serve per ricaricare i job da db
	 */
	public void afterPropertiesSet() throws Exception {
		logger.info("TestSchedulingManager Manager: afterPropertiesSet begin.");
		
		
		List<Test> docs = testService.getTests();
		for (Test doc : docs) {
			if (doc.getDate()!=null) { 
				logger.info("TestSchedulingManager - schedulo scadenza doc: "+doc.getTest() );
				JobListener jobListener = new TestListener(this);
				scheduler.getScheduler().getListenerManager().addJobListener(jobListener, jobGroupEquals("TEST_ACTION"));
	
				addNewJob(doc);
			}
		}
		

		logger.info("TestSchedulingManager: afterPropertiesSet end.");
	}


	/**
	 * 
	 * @param job
	 */
	public void stopAndRemoveJob(Test schedule) {
		if (schedule!=null) {
			try {
				String trigName = "TEST_ACTION_" + schedule.getId();
				logger.info("stopAndRemoveJob - scheduleItem JobId:" + trigName);
				scheduler.getScheduler().unscheduleJob(TriggerKey.triggerKey(trigName, "TEST_ACTION")); // Rimuove i triggers
				scheduler.getScheduler().deleteJob(JobKey.jobKey(trigName, "TEST_ACTION")); // Rimuove il job
			} catch (SchedulerException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	
	/**
	 * 
	 * @param job
	 */
	public boolean checkIfJobExists(Test schedule) {
		if (schedule!=null) {
			try {
				String trigName = "TEST_ACTION_" + schedule.getId();
				logger.info("checkIfJobExists - cert - scheduleItem JobId:" + trigName);
				Trigger ttt = scheduler.getScheduler().getTrigger(TriggerKey.triggerKey(trigName, "TEST_ACTION"));
				
				if (ttt!=null) return true;
				else return false;
			} catch (SchedulerException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return false;
	}
	
	
	
	
	/**
	 * 
	 * @param job
	 */
	public void stopAndRemoveJob(String jobName) {
		if (jobName!=null) {
			try {
				//String trigName = "RISORSA_PERMESSO" + schedule.getId();
				logger.info("stopAndRemoveJob - scheduleItem jobName:" + jobName);
				scheduler.getScheduler().unscheduleJob(TriggerKey.triggerKey(jobName, "TEST_ACTION")); // Rimuove i triggers
				scheduler.getScheduler().deleteJob(JobKey.jobKey(jobName, "TEST_ACTION")); // Rimuove il job
			} catch (SchedulerException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	
	/**
	 * 
	 * @param job
	 * @return
	 */
	public boolean addNewJob(Test job) {
		try {
			if (job!=null) {
				logger.info("Aggiungo la risorsa :" + job.getId());
				Trigger tri = createTriger(job);
				if (tri!=null){
					
					JobDataMap jdm = new JobDataMap();
					jdm.put("companyDocumentActionItem", job); // Dati che vengono passati all'istanza della classe XxxJobExecutor
					jdm.put("testService", testService);
					jdm.put("mailSender", mailSender);
					
					JobDetail jobDetail = JobBuilder.newJob(TestJobExecutor.class)
							.withIdentity(JobKey.jobKey("TEST_ACTION_" + job.getId(), "TEST_ACTION"))
							.usingJobData(jdm)
							.build();
					
					
//					jobDetail.setName("COMPANY_DOCUMENT_ACTION" + job.getId());
//					jobDetail.setJobClass(CompanyDocumentJobExecutor.class);
//					jobDetail.setGroup("COMPANY_DOCUMENT_ACTION");
//					jobDetail.setDescription("COMPANY_DOCUMENT_ACTION - ITEM");
			         try {
			        	 Date scheduleJob = scheduler.getScheduler().scheduleJob(jobDetail, tri);
			        	 logger.info("***data job:" + scheduleJob);
			         } catch (SchedulerException e) {
			        	 e.printStackTrace();
			        	// Rimuovo il job
			        	 stopAndRemoveJob(job);
			         }
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	
	public void rescheduleJob(Test item) {
		try {
			stopAndRemoveJob(item);
			logger.info(item);
			addNewJob(item);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// Crea il trigger
	private Trigger createTriger(Test item) {
		Trigger trigger = null;
		
		if(item==null || item.getDate()==null )	return null;

		String trigName = "TEST_ACTION_" + item.getId();
		
		// two months before expiration
		Calendar startDate = Calendar.getInstance();
		startDate.setTime(item.getDate());
		startDate.add(Calendar.DAY_OF_MONTH, -60); // tolgo l'offset

		trigger = TriggerBuilder.newTrigger() 
	             .withIdentity(TriggerKey.triggerKey(trigName, "TEST_ACTION"))
	             .withSchedule(SimpleScheduleBuilder.simpleSchedule().withMisfireHandlingInstructionFireNow())
	             .startAt(startDate.getTime())
	             .build();
	
//		trigger = TriggerBuilder<Trigger> new SimpleTriggerImpl(trigName, "COMPANY_DOCUMENT_ACTION", initDate, endDate, SimpleTrigger.REPEAT_INDEFINITELY, item.getTimerTypeSeconds().intValue()*1000); 
//		trigger.setGroup("COMPANY_DOCUMENT_ACTION");
//		trigger.setMisfireInstruction(SimpleTrigger.INSTRUCTION_NOOP);
		logger.info("trigger:" + trigger.getKey().getName() + " start:" + trigger.getStartTime());
		
		return trigger;
	}
}
