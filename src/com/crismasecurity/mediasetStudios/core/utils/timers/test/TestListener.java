/**
 * 
 */
package com.crismasecurity.mediasetStudios.core.utils.timers.test;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobListener;



/**
 * Class name: AdamTimerListener
 *
 * Description: 
 * 
 *
 *
 * @author gio
 * @date 09/apr/2012
 *
 */
public class TestListener implements JobListener {
	private Log logger = LogFactory.getLog(TestListener.class);
    private TestSchedulingManager manager = null;

    
    public TestListener(TestSchedulingManager manager) {
    	super();
    	this.manager=manager;
    }
    
    
    public String getName() {
         return getClass().getSimpleName();
    }

	/* (non-Javadoc)
	 * @see org.quartz.JobListener#jobExecutionVetoed(org.quartz.JobExecutionContext)
	 */
	public void jobExecutionVetoed(JobExecutionContext context) {
        logger.info(context.getJobDetail().getKey().getName() + " was vetoed and not executed()");
       // checkAndDelete(context);
        
        
	}

	/* (non-Javadoc)
	 * @see org.quartz.JobListener#jobToBeExecuted(org.quartz.JobExecutionContext)
	 */
	public void jobToBeExecuted(JobExecutionContext context) {
        logger.info(context.getJobDetail().getKey().getName() + " is about to be executed");
        
    	logger.info(context.getJobDetail().getKey().getName() + " ... ");
    	//MailMessageJobDetail mjd = (MailMessageJobDetail) context.getJobDetail();
        //List<MailMessage> readAllMessages = mjd.mailReader.readAllMessages();
	}

	/* (non-Javadoc)
	 * @see org.quartz.JobListener#jobWasExecuted(org.quartz.JobExecutionContext, org.quartz.JobExecutionException)
	 */
	public void jobWasExecuted(JobExecutionContext context, JobExecutionException arg1) {
        logger.info(context.getJobDetail().getKey().getName() + " was executed and it will be executed next time at " + context.getTrigger().getNextFireTime() + " may fire again? " + context.getTrigger().mayFireAgain());
//        checkAndDelete(context);
	}
	
//	/**
//	 * Controlla se il job � scaduto, se si lo elimina
//	 * @param context
//	 */
//	private void checkAndDelete(JobExecutionContext context) {
//		logger.debug("checkAndDelete!!");
//		if (!context.getTrigger().mayFireAgain()) {
//			logger.debug("context.getJobDetail(): " + context.getJobDetail());
//        	// Elimino il job dal db e dallo scheduler
//        	if (context.getJobDetail() instanceof AdamTimerJobDetail) {
//        		logger.debug(context.getJobDetail().getName() + " may not fire again, delete it!!");
//        		
//	        	manager.stopAndRemoveJob();
//	        	
//	        	logger.debug(context.getJobDetail().getName() + " deleted!!");
//        	}
//       }
//	}

}
