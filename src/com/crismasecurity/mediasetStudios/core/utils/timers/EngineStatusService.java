package com.crismasecurity.mediasetStudios.core.utils.timers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crismasecurity.mediasetStudios.core.entity.Episode;
import com.crismasecurity.mediasetStudios.core.entity.EpisodeStatusType;
import com.crismasecurity.mediasetStudios.core.entity.SpectatorEpisode;
import com.crismasecurity.mediasetStudios.core.services.EmailMakerService;
import com.crismasecurity.mediasetStudios.core.services.EmailQueueService;
import com.crismasecurity.mediasetStudios.core.services.EpisodeService;
import com.crismasecurity.mediasetStudios.core.services.EpisodeStatusTypeService;
import com.crismasecurity.mediasetStudios.core.utils.DateUtils;
import com.crismasecurity.mediasetStudios.core.utils.MessageManager;
import com.crismasecurity.mediasetStudios.core.utils.PropertiesUtils;

//@Scope("session")
@Service("engineStatusService")
public class EngineStatusService{
//	@Autowired
//	private ContractService contractService;
	
//	final static Logger logger = Logger.getLogger(EngineStatusService.class);
	
	@Autowired
    private EpisodeService episodeService;

    @Autowired
    private EpisodeStatusTypeService episodeStatusTypeService;
    
	@Autowired
	private EmailQueueService emailQueueService;
	
	@Autowired
	private EmailMakerService emailMakerService;
	
//	@PreDestroy
//	public void removeListener() {
//		MailSender.removeListener(this);
//	}
	
	private MessageManager messageMngr= new MessageManager(this.getClass()) ;
	
	private static List<IntefaceChangeStatus> listeners = new ArrayList<IntefaceChangeStatus>();

	public static void addListener(IntefaceChangeStatus l) {
		listeners.add(l);
	}
	
	public static void removeListener(IntefaceChangeStatus l) {
		listeners.remove(l);
	}
	
	public static boolean existListener(IntefaceChangeStatus l) {
		return listeners.contains(l);
	}
	
	public static void refershListener(IntefaceChangeStatus l) {
		if (!existListener(l)) addListener(l);
	}

	
    public void runEngineStatus()  {

//	    int idJob2Run = 1;
	    int idJob2Run = 5;
	    int idStartStatus=0;
	    int idNextStatus=0;
	    int idPrevStatus=0;
	    int delayHours=0;
	    int sleepingTime = 0;
	    boolean allInviteSent=false;
	    
//	    messageMngr.enableSystemOut(true);
	    messageMngr.addInfoMessage("runEngineStatus Started...");
	    List<Episode> lstEpisode= new ArrayList<Episode>();
	    List<SpectatorEpisode> lstSpectators = new ArrayList<SpectatorEpisode>(); 
	    EpisodeStatusType nextStatus=null;
	    sleepingTime=Integer.parseInt(PropertiesUtils.getInstance().readProperty("schedule.sleepTime","5")); 
	    delayHours=-Integer.parseInt(PropertiesUtils.getInstance().readProperty("mail.automailDelayHours","0"));
	    
	    Boolean terminateOldEpisodes=Boolean.parseBoolean(PropertiesUtils.getInstance().readProperty("schedule.terminateonlyoldepisodes","false"));
	    idJob2Run=terminateOldEpisodes==true?5:1;
	    
	    	    
	    try {
	    	while (idJob2Run!=0) {
	    		if (idJob2Run==1) {
		    		try {
						Thread.sleep(sleepingTime*1000);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
	    		}
	    		messageMngr.addInfoMessage ("runEngineStatus - Manage idJob2Run = "+idJob2Run+" ...");
		    	switch (idJob2Run) {
				case 1:

//				    Prendo tutti gli episodi che si trovano nello stato 3 = Lista Completa					
//					e li Approvo portandoli allo stato 4 se e solo se vi sono Spettatori altrimenti le porto in Lista in Lavorazione.
					messageMngr.addInfoMessage ("runEngineStatus - Promote status from 3 to 4: Complete to Approved from 3 to 2 On Working if there aren't Spectators...");
					idStartStatus=3;
					idNextStatus=4;
					idPrevStatus=2;
					
			  	    lstEpisode= episodeService.getEpisodesByIdStatus(idStartStatus);
				    for (Episode episode : lstEpisode) {
				    	messageMngr.addDebugMessage ("runEngineStatus - engineStatus promote episode status (with ID = "+episode.getId()+") from IDStatus " + idStartStatus + " to " + idNextStatus + " ...");		

//		    			Passo lo stato in Lista Approvata
						nextStatus =new EpisodeStatusType();		
						if (episodeService.getSpectatorEpisodeByEpisodeId(episode.getId()).size()>0) {
							nextStatus = episodeStatusTypeService.getEpisodeStatusTypeByIdStatus(idNextStatus);	
						}else {
							nextStatus = episodeStatusTypeService.getEpisodeStatusTypeByIdStatus(idPrevStatus);
						}
						episode.setEpisodeStatus(nextStatus);
						
					    episode=episodeService.updateEpisode(episode);
					}
				    if (lstEpisode.size()>0) {
						for (IntefaceChangeStatus lstnr : listeners) {
							lstnr.onStatusChanged(nextStatus.getIdStatus());
						}				    	
				    }
					break;
				case 2:
				case 3:
//					Prendo in considerazione tutti gli episodi nello stato Lista e Inviti Creati e devo controllare 
//					che effettivamente siano stati inviati.
//					Se devo eseguire lo step 2 allora prendo in considerazione gli episodi nello stato 6 (Crea Lista e Inviti); se sono 
//					nello step 3 allora prendo in considerazione gli episodi nello stato 7 (Crea sola Lista)
					idStartStatus=(idJob2Run==2?6:7);
					idNextStatus=8; //Lista/Inviti Inviati
					allInviteSent=false;
					lstEpisode= episodeService.getEpisodesByIdStatus(idStartStatus);
					for (Episode episode : lstEpisode) {
						messageMngr.addDebugMessage ("runEngineStatus - engineStatus promote status from " + idStartStatus + " to " + idNextStatus + " ...");		

//				    	Valuto se ho inviato la lista.
				    	try {
				    		allInviteSent=(emailQueueService.getEmailQueueByEntityID(episode.getId(),1)!=null && emailQueueService.getEmailQueueByEntityID(episode.getId(),1).getSendStatus()==1);
						} catch (Exception e) {
							allInviteSent=false;
							messageMngr.addWarningMessage (e, "runEngineStatus - Impossible promote status from "+ idStartStatus + " to " + idNextStatus + "... The invites are already sending...");	
						}
//				    	Valuto se ho inviato la lista.				    	
				    	if ((allInviteSent&&(idStartStatus==6))) {
//				    		Se ho inviato la lista valuto se ho inviato anche tutti gli inviti (status 6)
//				    		List<SpectatorEpisode> lstSpectators =episode.getSpectators();
				    		
					    	lstSpectators = episodeService.getSpectatorEpisodeByEpisodeId(episode.getId());
					    	for (SpectatorEpisode spectatorEpisode : lstSpectators) {
					    		try {
					    			switch (emailQueueService.getEmailQueueByEntityID(spectatorEpisode.getId(),2).getSendStatus()) {
									case 1:
										break;
									case 2:
//										Rilevando se sono state inviate le email non tengo in considerazione quelle per le quali l'indirizzo di email
//										non è indicato...
										allInviteSent = allInviteSent && (
										(emailQueueService.getEmailQueueByEntityID(spectatorEpisode.getId(),2).getToEmail().isEmpty())
										||
										(emailQueueService.getEmailQueueByEntityID(spectatorEpisode.getId(),2).getToEmail()==null)
										);
										break;										
									default:
										allInviteSent=false;
										break;
									}
//					    			allInviteSent = (allInviteSent && (emailQueueService.getEmailQueueByEntityID(spectatorEpisode.getId(),2).getSendStatus()==1));
//					    			allInviteSent = (allInviteSent && ((emailQueueService.getEmailQueueByEntityID(spectatorEpisode.getId(),2).getToEmail().isEmpty()) || (emailQueueService.getEmailQueueByEntityID(spectatorEpisode.getId(),2).getToEmail()==null)));
								} catch (Exception e) {
									allInviteSent=false;
									messageMngr.addErrorMessage (e, "runEngineStatus - Impossible promote status from "+ idStartStatus + " to " + idNextStatus+"...");	
								}
							}
				    	}
//				    	se ho inviato Liste e Inviti allora aggiorno lo stato dell'episodio.
				    	if (allInviteSent) {
				    		messageMngr.addInfoMessage("runEngineStatus - All invite sent...");
				    		nextStatus =new EpisodeStatusType();				
							nextStatus = episodeStatusTypeService.getEpisodeStatusTypeByIdStatus(idNextStatus);
							episode.setEpisodeStatus(nextStatus);
						    episode=episodeService.updateEpisode(episode);
						    messageMngr.addDebugMessage("runEngineStatus - Notify change status...");
							for (IntefaceChangeStatus lstnr : listeners) {
								lstnr.onStatusChanged(idNextStatus);
							}
				    	} else {
				    		messageMngr.addInfoMessage("runEngineStatus - All invite not sent...");
				    	}
				    }
					break;
				case 4:	
//					Prendo gli episodi che si trovano nello stato Approvato e li porto nello stato 7 (Crea sola Lista)
					idStartStatus=4;
//					idNextStatus=7;
					if (delayHours!=0) {
						lstEpisode= episodeService.getEpisodesByIdStatus(idStartStatus);
						for (Episode episode : lstEpisode) {
							if(DateUtils.HoursElapsedFromNow(episode.getDateFrom())>delayHours) {
								messageMngr.addInfoMessage("runEngineStatus - Send list Spectator in automatic mode " + delayHours + " hours before episode...");
								emailMakerService.createEmails(episode, true, 2);
								emailMakerService.createEmails(episode, false, 3);
							}
						}						
					}
					break;

				case 5:
//				    Prendo tutti gli episodi antecedenti ad oggi e li porto in stato "Evento Terminato"	
					messageMngr.addInfoMessage ("runEngineStatus - Promote old episode from status x to 5: x to Event Ended...");
					
					idNextStatus=5;
					Date yesterday = DateUtils.addDays(DateUtils.getDate(),-1);
			  	    lstEpisode= episodeService.getEpisodesBeforeThan(yesterday);
				    for (Episode episode : lstEpisode) {
				    	messageMngr.addDebugMessage ("runEngineStatus - engineStatus promote episode status (with ID = "+episode.getId()+") from ID Status" + episode.getEpisodeStatus().getIdStatus() + " to " + idNextStatus + " ...");		

//		    			Passo lo stato in Evento Terminato
						nextStatus =new EpisodeStatusType();				
						nextStatus = episodeStatusTypeService.getEpisodeStatusTypeByIdStatus(idNextStatus);
						episode.setEpisodeStatus(nextStatus);
					    episode=episodeService.updateEpisode(episode);
//					    Elimino anche tutte le eventuali email precedentemente create
					    emailMakerService.purgeEmail(episode);	
					    
					}
				    if (lstEpisode.size()>0) {
						for (IntefaceChangeStatus lstnr : listeners) {
							lstnr.onStatusChanged(idNextStatus);
						}				    	
				    }
					break;	
				case 6:
					//FIXME - Per i problemi dello scheduler che lanciava Job in concorrenza ho fatto in modo con idJob2Run=0 di lanciare un solo Job e ciclare sulla gestioen degli stati.
//					idJob2Run=-1;
					idJob2Run=0;
					break;	
				default:
					break;
				}
		    	idJob2Run++;
	    	}
		} catch (Throwable e) {
			messageMngr.addErrorMessage(e,"runEngineStatus - Other Error...");
	    }
    } 
}