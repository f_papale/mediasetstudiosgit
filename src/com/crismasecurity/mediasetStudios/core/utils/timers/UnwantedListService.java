package com.crismasecurity.mediasetStudios.core.utils.timers;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import javax.annotation.PostConstruct;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crismasecurity.mediasetStudios.core.entity.AgencySpectator;
import com.crismasecurity.mediasetStudios.core.entity.BlackListSpectator;
import com.crismasecurity.mediasetStudios.core.entity.BlackListType;
import com.crismasecurity.mediasetStudios.core.entity.Spectator;
import com.crismasecurity.mediasetStudios.core.entity.Statistics;
import com.crismasecurity.mediasetStudios.core.mail.MailMessage;
import com.crismasecurity.mediasetStudios.core.mail.javamail.EmailSender;
import com.crismasecurity.mediasetStudios.core.services.AgencyService;
import com.crismasecurity.mediasetStudios.core.services.AgencySpectatorService;
import com.crismasecurity.mediasetStudios.core.services.BlackListSpectatorService;
import com.crismasecurity.mediasetStudios.core.services.BlackListTypeService;
import com.crismasecurity.mediasetStudios.core.services.DistributionEmailService;
import com.crismasecurity.mediasetStudios.core.services.SpectatorService;
import com.crismasecurity.mediasetStudios.core.services.SpectatorTypeService;
import com.crismasecurity.mediasetStudios.core.services.StatisticsService;
import com.crismasecurity.mediasetStudios.core.utils.TextUtil;
import com.crismasecurity.mediasetStudios.core.utils.DateUtils;
import com.crismasecurity.mediasetStudios.core.utils.Gender;
import com.crismasecurity.mediasetStudios.core.utils.MessageManager;
import com.crismasecurity.mediasetStudios.core.utils.PropertiesUtils;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

//@Scope("session")
@Service("unwantedListService")
public class UnwantedListService {
	
	@Autowired
	private BlackListTypeService blackListTypeService;
	
	@Autowired
	private BlackListSpectatorService blackListSpectatorService;
	
	@Autowired
	private SpectatorService  spectatorService;
	
	@Autowired
	private AgencyService agencyService;
	
	@Autowired
	private AgencySpectatorService agencySpectatorService;
	
	@Autowired
	private SpectatorTypeService spectatorTypeService; 
	
	@Autowired
	private StatisticsService statisticsService;
	
	@Autowired
	private DistributionEmailService distributionEmailService;
	
	@Autowired
	private EmailSender emailSender;

	
	private List<Spectator> spectatorsNotAddedList = null;
	private String unwantedMessage=null;
	
	private MessageManager messageMngr= new MessageManager(this.getClass()) ;
	
	@PostConstruct
	public void init() {
		spectatorsNotAddedList = new ArrayList<Spectator>();
	}
	
    int deletedUnwanted=0, addedUnwanted=0, updatedUnwanted=0, loadedUnwanted=0;
    boolean removeDuplicateSpectators=false;
    String listItemDeleteUnwanted="";
    String tempFolder=""; 
	
    
    
    public void runUnwantedListLoad() {
    	
    	messageMngr.addInfoMessage("runUnwantedListService Started...");

	    int sleepTime=0;
	    String unwantedList;
	    boolean enabledLoad=true, enabledNotify=true;
	    int idJob2Run=1, forcedState=0;
	    	    
	    unwantedMessage= PropertiesUtils.getInstance().readProperty("unwanted.message","");
	    unwantedList =PropertiesUtils.getInstance().readProperty("unwanted.file","");
	    tempFolder = PropertiesUtils.getInstance().readProperty("tmp.files.location","");
	    
	
    
	    if (tempFolder.contentEquals("")) {
	    	tempFolder=System.getProperty("java.io.tmpdir");
	    }
	    
	    String separator = PropertiesUtils.getInstance().readProperty("unwanted.separator","\t");
	    try {
	    	enabledLoad= Boolean.parseBoolean(PropertiesUtils.getInstance().readProperty("unwanted.enable_scheduler","true"));  
		} catch (Exception e) {
			enabledLoad=false;
		}
	    
	    
	    try {
	    	enabledNotify= Boolean.parseBoolean(PropertiesUtils.getInstance().readProperty("unwanted.enabled_nofify","false"));  
		} catch (Exception e) {
			enabledNotify=false;
		}
	    

	    
	    try {
	    	forcedState=Integer.parseInt(PropertiesUtils.getInstance().readProperty("unwanted.forcedState","0"));
		} catch (Exception e) {
			forcedState=0;
		}
	    
	    if ((unwantedList=="")||(!enabledLoad) ) 
	    	idJob2Run=0;
	    if (forcedState!=0) 
	    	idJob2Run=forcedState;
	    
	    while (idJob2Run!=0)  {
	    	try {
				Thread.sleep(sleepTime*100);
			} catch (InterruptedException e1) {
				messageMngr.addErrorMessage(e1, "runUnwantedListService - Error on Thread Sleep...");
			}    

	    	switch (idJob2Run) {
	    	case 1:
	    		deletedUnwanted=0; 
	    		addedUnwanted=0; 
	    		updatedUnwanted=0;
	    		loadedUnwanted=0;
	    		listItemDeleteUnwanted="";
	    		spectatorsNotAddedList.clear();
			    
	    		try {
			    	removeDuplicateSpectators= Boolean.parseBoolean(PropertiesUtils.getInstance().readProperty("unwanted.removeDuplicateSpectators","false"));  
				} catch (Exception e) {
					removeDuplicateSpectators=false;
				}
	    		
	    		if (removeDuplicateSpectators) {
		    		if (doRemoveDuplicateSpectators()) {
		    			messageMngr.addWarningMessage("runUnwantedListService - removeDuplicateSpectators done...");
		    		}else {
		    			messageMngr.addErrorMessage("runUnwantedListService - removeDuplicateSpectators Error...");
		    		}	
	    		}

	    		if (handleCSVUnwantedList(unwantedList, separator)) {
	    			idJob2Run=2;
	    		}else {
	    			messageMngr.addErrorMessage("runUnwantedListService - handleCSVUnwantedList Error...");
	    			idJob2Run=0;
	    		}
	    		break;
	    	case 2:
	    		if (consolidateUnwantedItems()) {
	    			idJob2Run=3;	
	    		}else {
	    			messageMngr.addErrorMessage("runUnwantedListService - consolidateUnwantedItems Error...");
	    			idJob2Run=0;
	    		}
	    		
	    		break;
	    	case 3:
	    		if (removeAvailabilityUnwantedItems()) {
	    			idJob2Run=4;	
	    		}else {
	    			messageMngr.addErrorMessage("runUnwantedListService - removeAvailabilityUnwantedItems Error...");
	    			idJob2Run=0;
	    		}
	    		break;   
	    	case 4:
	    		if (statisticsUpdate()) {
	    			idJob2Run=5;	
	    		}else {
	    			messageMngr.addErrorMessage("runUnwantedListService - statisticsUpdate Error...");
	    			idJob2Run=0;
	    		}
	    		break;	   
	    	case 5:
	    		if (enabledNotify) {
		    		if (sendStatisticsByEmail()) {
		    			idJob2Run=0;	
		    		}else {
		    			messageMngr.addErrorMessage("runUnwantedListService - sendStatisticsByEmail Error...");
		    			idJob2Run=0;	    			
		    		}	    			
	    		}else {
	    			idJob2Run=0;	
	    		}
	    		break;
	    	default:
	    		messageMngr.addErrorMessage("runUnwantedListService - Unknow Error...");
	    		idJob2Run=0;
	    		break;
	    	}
	    }
    }
    
    
    private String[] getRecipients() {
    	
    	String[] aRecipients = null; 
    	
    	String distributionList = PropertiesUtils.getInstance().readProperty("unwanted.nofify2distribution","");
    	String  recipients = PropertiesUtils.getInstance().readProperty("unwanted.nofify2recipients","");
    	if (recipients==null || recipients=="" ) {
    		if (distributionList!=null && distributionList!="") {
    	    	String lst = distributionEmailService.getEmailContactFromDistributionEmail(distributionList);
    	    	if (lst!=null) {
        	    	aRecipients= lst.split(";");
    	    	}
    		}
    	}else {
    		aRecipients=recipients.split(";");
    	}
    	return aRecipients;
    }
    
    private boolean sendStatisticsByEmail() {
    	boolean result=true;
    	try {
        	String[] recipients = getRecipients();
        	if (recipients!=null) {
        		String bodyMail = makeReport();
        		String objectMail = "Studios - Acquisizione Lista Indesiderati";
        		MailMessage mail = new MailMessage(recipients, objectMail, bodyMail, true);
        		emailSender.sendInstantMail(mail);
        	}
		} catch (Exception e) {
			result=false;
			e.printStackTrace();
		}
    	return result;
    }    	
    
    private String makeReport() {
    	Integer res=0;
		String message ="";
		
    	HashMap<String, String> vars= new HashMap<String, String>();
    	SimpleDateFormat dt1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

		Statistics dummy = Collections.max(statisticsService.getStatisticsByDomain("unwanted"), Comparator.comparing(Statistics::getUpdatedOn));
		// Può essere richiamato anche attraverso la classe com.crismasecurity.mediasetStudios.webservices.test.CallWebserviceTest
		statisticsService.getStatisticsByDomain("unwanted").forEach(s-> vars.put(s.getMeasure(),s.getValue()));
		res = Integer.parseInt( vars.get("loadedUnwanted"))-Integer.parseInt(vars.get("onErrorUnwanted"));
//				Studios - Importazione Persone Indesiderate 2021/10/08 02:52:58</p>
		message ="<p><br></p>";	
		message +="<p>Aggiornate: "+vars.get("updatedUnwanted")+"</p>";
		message +="<p>Aggiunte: "+vars.get("addedUnwanted")+"</p>";
		message +="<p>Cancellate: "+vars.get("deletedUnwanted")+"</p>";
		message +="<p><br></p>";
		message +="<p>Totale processati: "+ res +"</p>";
		
		message = "<p>Studios - Importazione Lista Indesiderati " + dt1.format(dummy.getUpdatedOn()) +"</p>"+message;
//		message="{\"message\":\""+message+"\"}";
		return message;
    }
    
    
    private boolean doRemoveDuplicateSpectators() {
    	boolean exitResult = spectatorService.removeDuplicatedSpectators();
    	return exitResult;
    }
    
    private boolean statisticsUpdate() {
    	boolean exitResult = true;
    	
    	try {
    		Statistics statisticsLoadedUnwanted = statisticsService.getStatisticsByDomainMeasure("unwanted", "loadedUnwanted");
    		if (statisticsLoadedUnwanted==null) 
    			statisticsLoadedUnwanted = new Statistics("unwanted", "loadedUnwanted", String.valueOf(loadedUnwanted));
    		else
    			statisticsLoadedUnwanted.updateStatistics("unwanted", "loadedUnwanted", String.valueOf(loadedUnwanted));
    			
    			
    		Statistics statisticsDeletedUnwanted  = statisticsService.getStatisticsByDomainMeasure("unwanted", "deletedUnwanted");
    		if (statisticsDeletedUnwanted == null)  
    			statisticsDeletedUnwanted = new Statistics("unwanted", "deletedUnwanted", String.valueOf(deletedUnwanted));
    		else
    			statisticsDeletedUnwanted.updateStatistics("unwanted", "deletedUnwanted", String.valueOf(deletedUnwanted));
    		
    		
    		Statistics statisticsAddedUnwanted  = statisticsService.getStatisticsByDomainMeasure("unwanted", "addedUnwanted");
    		if(statisticsAddedUnwanted == null) 
    			statisticsAddedUnwanted = new Statistics("unwanted", "addedUnwanted", String.valueOf(addedUnwanted));
    		else
    			statisticsAddedUnwanted.updateStatistics("unwanted", "addedUnwanted", String.valueOf(addedUnwanted));
    		
    		
    		Statistics statisticsUpdatedUnwanted = statisticsService.getStatisticsByDomainMeasure("unwanted", "updatedUnwanted"); 
    		if(statisticsUpdatedUnwanted == null) 
    			statisticsUpdatedUnwanted = new Statistics("unwanted", "updatedUnwanted", String.valueOf(updatedUnwanted));
    		else
    			statisticsUpdatedUnwanted.updateStatistics("unwanted", "updatedUnwanted", String.valueOf(updatedUnwanted));
    		
    		Statistics statisticsOnErrorUnwanted = statisticsService.getStatisticsByDomainMeasure("unwanted", "onErrorUnwanted"); 
    		if(statisticsOnErrorUnwanted == null) 
    			statisticsOnErrorUnwanted = new Statistics("unwanted", "onErrorUnwanted", String.valueOf(spectatorsNotAddedList.size()));
    		else
    			statisticsOnErrorUnwanted.updateStatistics("unwanted", "onErrorUnwanted", String.valueOf(spectatorsNotAddedList.size()));
    		

//    		L'aggiornamento del timestamp scatta solo se il valore all'interno è cambiato. 
//    		Per costringere a cambiare il timestamp, quando il valore è rimasto invariato, 
//    		forzo il cambiamento del valore stesso ad un contenuto fittizio, quindi salvo beneficiando dell'aggiornamneto del timestamp
//    		e quindi rimetto il precedente e reale valore
    		String tmp=statisticsLoadedUnwanted.getValue();
    		statisticsLoadedUnwanted.setValue("NOP");
    		statisticsLoadedUnwanted=statisticsService.updateStatistics(statisticsLoadedUnwanted);
    		statisticsLoadedUnwanted.setValue(tmp);
    		statisticsLoadedUnwanted=statisticsService.updateStatistics(statisticsLoadedUnwanted);
    		
    		tmp=statisticsDeletedUnwanted.getValue();
    		statisticsDeletedUnwanted.setValue("NOP");
    		statisticsDeletedUnwanted=statisticsService.updateStatistics(statisticsDeletedUnwanted);
    		statisticsDeletedUnwanted.setValue(tmp);
    		statisticsDeletedUnwanted=statisticsService.updateStatistics(statisticsDeletedUnwanted);
    				
    		tmp=statisticsAddedUnwanted.getValue();
    		statisticsAddedUnwanted.setValue("NOP");
    		statisticsAddedUnwanted=statisticsService.updateStatistics(statisticsAddedUnwanted);
    		statisticsAddedUnwanted.setValue(tmp);
    		statisticsAddedUnwanted=statisticsService.updateStatistics(statisticsAddedUnwanted);
    		
    		tmp=statisticsUpdatedUnwanted.getValue();
    		statisticsUpdatedUnwanted.setValue("NOP");
    		statisticsUpdatedUnwanted=statisticsService.updateStatistics(statisticsUpdatedUnwanted);
    		statisticsUpdatedUnwanted.setValue(tmp);
    		statisticsUpdatedUnwanted=statisticsService.updateStatistics(statisticsUpdatedUnwanted);
    		
    		tmp=statisticsOnErrorUnwanted.getValue();
    		statisticsOnErrorUnwanted.setValue("NOP");
    		statisticsOnErrorUnwanted=statisticsService.updateStatistics(statisticsOnErrorUnwanted);
    		statisticsOnErrorUnwanted.setValue(tmp);
    		statisticsOnErrorUnwanted=statisticsService.updateStatistics(statisticsOnErrorUnwanted);

		} catch (Exception e) {
			e.printStackTrace();
			exitResult=false;
		}
    	return exitResult;
    }
    

    
    private File getFilteredFile(String unwantedList) {
    	
    	File tempFile =null;
	    try{
	    	tempFile = File.createTempFile("studios-", ".tmp", new File(tempFolder));
	    	
//	    	tempFile.deleteOnExit();
	    	System.out.println(System.getProperty("java.io.tmpdir"));

	        String totalStr = FileUtils.readFileToString(new File(unwantedList));
	        totalStr = totalStr.replace("\n", "").replace("\r", "\r\n");

            FileWriter fw = new FileWriter(tempFile);
	        fw.write(totalStr);
	        fw.close();
	       
	    }catch(Exception e){
	        e.printStackTrace();
	    }
	    return tempFile;
    }    
    
	public boolean handleCSVUnwantedList(String unwantedList, String separator) {
		boolean exitResult = true;
		String dateFormat = PropertiesUtils.getInstance().readProperty("unwanted.date_format","dd/MM/yy");
		CSVReader csvReader=null;
		File tempFile =null;
		

		try {

			tempFile =getFilteredFile(unwantedList);
			if (tempFile == null) {
				return false;
			}
			FileReader filereader = new FileReader(tempFile);
			
			if (filereader.equals(null)) { // file vuoto
				messageMngr.addErrorMessage("Handle Unwanted List - Error on file empty...");
				exitResult = false;
			} else {
				messageMngr.addInfoMessage("Handle Unwanted List - The file loaded is " + unwantedList+"....");
				CSVParser parser=null;
				try {
					if (separator.equals("\t"))
						parser = new CSVParserBuilder().withSeparator('\t').build();
					else
						parser = new CSVParserBuilder().withSeparator(separator.charAt(0)).build();
					
				} catch (Exception e) {
					parser = new CSVParserBuilder().withSeparator('\t').build();
				}

	    		csvReader = new CSVReaderBuilder(filereader)
					.withCSVParser(parser)
					.withSkipLines(1)
					.withKeepCarriageReturn(false)
					.build();
	    		   		
	    		String[] nextRecord;
	    		
	    		
				boolean bContinue=true;
				int rowCounter=0;
				loadedUnwanted=0;

				while ((nextRecord = csvReader.readNext()) != null) {				
					// Get Each Row
					bContinue=true;
					Spectator newSpectator = new Spectator();
					newSpectator.setAgencies(new ArrayList<AgencySpectator>());
					BlackListSpectator newBLSpectator = new BlackListSpectator();
					rowCounter++;
					
					messageMngr.addInfoMessage("Handle Unwanted List - Let's manage the row number " + rowCounter);
					int columnIndex=0;
					try {
						for (String cell : nextRecord) {
//								Cognome	
//								Nome	
//								C.Fiscale	
//								Sesso	
//								Data di Nascita	
//								Luogo di Nascita
//								Gravità	
//								Segnalatore	
//								Sede	
//								Data Segn.ne	
//								Note

							switch (columnIndex + 1) {
							case 1:
//									System.out.println("cognome = " + cell + "\n");
//									newSpectator.setSurname(cell.toUpperCase().trim());
								newSpectator.setSurname(TextUtil.cleanTextContent(cell.toUpperCase().trim()));
								break;
								
							case 2:
//									System.out.println("nome = " + cell + "\n");
//									newSpectator.setName(cell.toUpperCase().trim());
								newSpectator.setName(TextUtil.cleanTextContent(cell.toUpperCase().trim()));
								break;

							case 3:
//									System.out.println("codice fiscale = " + cell + "\n");
								newSpectator.setFiscalCode(cell.toUpperCase().trim());
								break;

							case 4:
//									System.out.println("sesso = " + cell + "\n");
								if (cell.toUpperCase().trim().equals("M") || cell.toUpperCase().trim().equals("MALE"))
									newSpectator.setGender(Gender.M);
								if (cell.toUpperCase().trim().equals("F") || cell.toUpperCase().trim().equals("FEMALE"))
									newSpectator.setGender(Gender.F);
								if (cell.toUpperCase().trim().equals("UNKNOWN") || cell == null)
									newSpectator.setGender(Gender.UNKNOWN);
								break;
								
							case 5:
//									System.out.println("datadinascita = " + cell.getDateCellValue() + "\n");
								newSpectator.setBirthDate(DateUtils.getDate(cell.trim(),dateFormat));
								break;
							case 6:
//									System.out.println("luogodinascita = " + cell + "\n");
								newSpectator.setBirthCity(cell.toUpperCase().trim());
								
								break;

							case 7: 
//									System.out.println("Livello/Gravità = " + cell + "\n");
								if (cell.toUpperCase().trim().equals("ALTA") || cell.toUpperCase().trim().equals("ALTO"))
									cell= "ALTO";
								if (cell.toUpperCase().trim().equals("MEDIA") || cell.toUpperCase().trim().equals("MEDIO"))
									cell= "MEDIO";
								
								if (!(cell.equals("ALTO") || cell.equals("MEDIO"))) 
										cell= "ALTO";
								
								BlackListType newBlackListType = new BlackListType();
								newBlackListType=blackListTypeService.getBlackListTypeByLevel(cell.toUpperCase().trim());
								newBLSpectator.setBlackListType(newBlackListType);
								break;
							
							case 8:
								newBLSpectator.setSignaler(cell.toUpperCase().trim());
								break;
								
							case 9:
								newBLSpectator.setSite(cell.toUpperCase().trim());
								break;
								
							case 10:
								newSpectator.setCreateDate(DateUtils.getDate(cell.trim(),dateFormat));
								newBLSpectator.setDateTimeBlackList(DateUtils.getDate(cell.trim(),dateFormat));
								break;
							
							case 11:
								newBLSpectator.setReason(cell.toUpperCase().trim());
								break;
							}
							columnIndex++;
						} // end cell iterator
						
//						 Se non esiste tra gli spettatori
//						 	lo creo in spettatori 
//						 Se non è associato all'agenzia fittizia 
//							lo associo
						 
//						 Se non esiste in black list:
//						 	lo inserisco in blacklist
//						 altrimenti
//						 	aggiono la blacklist
						
//						Controlla se lo spettatore esiste e nel caso restituisce quello in archivio
						newSpectator = spectatorService.getSpectatorFromArchive(newSpectator);
						if (newSpectator.getId()==null) {
							if ( !(newSpectator.getSurname().equals("") || newSpectator.getName().equals(""))) {
								try {
									newSpectator.setSpectatorType(spectatorTypeService.getSpectatorTypeByIdSpectatorType(1));
									newSpectator.setToReview(false);
									
									if (newSpectator.getAgencies()==null) 
										newSpectator.setAgencies(new ArrayList<AgencySpectator>());
									newSpectator=spectatorService.updateSpectator(newSpectator);
								} catch (Exception e) {
									bContinue=false;
									messageMngr.addErrorMessage(e, "Handle Unwanted List - Errore nell'inserimento tra gli spettatori da porre in Lista Indesiderati del nominativo " + newSpectator.getFullName().toUpperCase());
									spectatorsNotAddedList.add(newSpectator);
									e.printStackTrace();
								} 								
							}else {
								spectatorsNotAddedList.add(newSpectator);
								bContinue=false;
							}	
						}
						if (bContinue) {
						 							
//							Valuto se è già in blacklist
							BlackListSpectator blackListSpectator = blackListSpectatorService.getBlackListSpectatorByIdSpectator(newSpectator.getId());
//							Se non è in blacklist lo inserisco o comunque ne aggiorno le info sulla base di quanto letto
							if (blackListSpectator==null) 
								blackListSpectator = new BlackListSpectator();
							newBLSpectator.setSpectator(newSpectator);
							blackListSpectator.cloneFrom(newBLSpectator); 
							try {
								blackListSpectator.setUpdatedOn(null);
								boolean isInsert = (blackListSpectator.getIdBlackListSpectator()==null);
								blackListSpectatorService.updateBlackListSpectator(blackListSpectator);
								if (isInsert) {
									addedUnwanted++;
									messageMngr.addInfoMessage("Added: " + blackListSpectator.getSpectator().getFullName() );
								}
								else {
									updatedUnwanted++;	
									messageMngr.addInfoMessage("Updated: " + blackListSpectator.getSpectator().getFullName() );
								}	
							} catch (Exception e) {
								messageMngr.addErrorMessage(e, "Handle Unwanted List - errore dati sul file CSV - riga  " + rowCounter + ". Impossibile aggiornare nella Lista persone indesiderate il nominativo: "+blackListSpectator.getSpectator().getFullName());
								spectatorsNotAddedList.add(newSpectator);
								e.printStackTrace();
							}
//							Verifichiamo se questo spettatore è gia associato alla agenzia fittizia;
//							se non è ancora stato associato a quest agenzia , lo associamo
//							if(! newSpectator.getAgencies().stream().map(s->s.getAgency()).collect(Collectors.toList()).contains(agencyService.getDummyAgency())) {
							if(! agencySpectatorService.isSpectatorInDummyAgency(newSpectator)) {
								AgencySpectator agSpToAdd = new AgencySpectator(agencyService.getDummyAgency(), newSpectator);
								if (newSpectator.getAgencies()==null)
									newSpectator.setAgencies(new ArrayList<AgencySpectator>());
								agSpToAdd.setSpectator(newSpectator);
								newSpectator.getAgencies().add(agSpToAdd);
								newSpectator.setNote(unwantedMessage);
								newSpectator=spectatorService.updateSpectator(newSpectator);
							}								
						 }
						 loadedUnwanted=rowCounter;
					} catch (Exception e) {
						messageMngr.addErrorMessage(e, "Handle Unwanted List - Errore dati sul file CSV alla riga: " + rowCounter);
						newSpectator.setNote("Controlla la riga " + rowCounter + " del file CSV");
						spectatorsNotAddedList.add(newSpectator);
						e.printStackTrace();
					}
				}
				csvReader.close();
			}
			messageMngr.addInfoMessage("Handle Unwanted List - Spettatori salvati...");
		} catch (Exception e) {
			messageMngr.addErrorMessage(e, "Handle Unwanted List - UnwantedList Errore nella lettura del file.");
			if (csvReader != null)
				try {
					csvReader.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} 
			exitResult = false;
			e.printStackTrace();
		}		
		try {
			Files.deleteIfExists(tempFile.toPath());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return exitResult;
	}

	
	private boolean consolidateUnwantedItems() {
		boolean bReturn=blackListSpectatorService.consolidateLast();
		if (bReturn) {
			deletedUnwanted =  blackListSpectatorService.getDeletedAfterConsolidate();
			try {
				blackListSpectatorService.getListItemsDeletedAfterConsolidate().forEach( s -> listItemDeleteUnwanted += s + "\r");
				messageMngr.addInfoMessage("Deleted List: " + listItemDeleteUnwanted);
			} catch (Exception e) {
				messageMngr.addErrorMessage("Deleted List: Error: " + e.getMessage());
			}
		}
		return bReturn;
	}
	
	private boolean removeAvailabilityUnwantedItems() {
		return agencySpectatorService.removeAvailabilityUnwantedSpectators();
	}
}
