package com.crismasecurity.mediasetStudios.core.utils.timers;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;
import com.crismasecurity.mediasetStudios.core.entity.EmailQueue;
import com.crismasecurity.mediasetStudios.core.entity.Episode;
import com.crismasecurity.mediasetStudios.core.entity.SpectatorEpisode;
import com.crismasecurity.mediasetStudios.core.mail.MailMessage;
import com.crismasecurity.mediasetStudios.core.mail.javamail.InterfaceSendingEmail;
import com.crismasecurity.mediasetStudios.core.mail.javamail.EmailSender;
import com.crismasecurity.mediasetStudios.core.services.EmailQueueService;
import com.crismasecurity.mediasetStudios.core.services.EpisodeService;
import com.crismasecurity.mediasetStudios.core.services.SpectatorEpisodeService;
import com.crismasecurity.mediasetStudios.core.utils.PropertiesUtils;
import com.crismasecurity.mediasetStudios.core.utils.DateUtils;
import com.crismasecurity.mediasetStudios.core.utils.MessageManager;
import com.crismasecurity.mediasetStudios.web.utils.reporting.JasperReportDetails;
import com.crismasecurity.mediasetStudios.web.utils.reporting.JasperReportsFactory;

import java.util.Date;

//@Scope("session")
@Service("mailingQueueService")
public class MailingQueueService implements InterfaceSendingEmail {
//	@Autowired
//	private ContractService contractService;

//	final static Logger logger = Logger.getLogger(MailingQueueService.class);
	@Autowired
	private SpectatorEpisodeService spectatorEpisodeService;

    @Autowired
    private EpisodeService episodeService;
  
    @Autowired
	private EmailSender emailSender;
    
	@Autowired
	private EmailQueueService emailQueueService;
	
	@Autowired
    private JasperReportsFactory jasperReportsFactory;	
	
	private MessageManager messageMngr= new MessageManager(this.getClass()) ;
	
	@PostConstruct
	public void init() {
		EmailSender.addListener(this);
	}
	
	@PreDestroy
	public void removeListener() {
		EmailSender.removeListener(this);
	}
	
	private static List<InterfaceMailSent> listeners = new ArrayList<InterfaceMailSent>();


	public static void addListener(InterfaceMailSent l) {
		listeners.add(l);
	}
	
	public static void removeListener(InterfaceMailSent l) {
		listeners.remove(l);
	}
	
	public static boolean existListener(InterfaceMailSent l) {
		return listeners.contains(l);
	}
	
	public static void refershListener(InterfaceMailSent l) {
		if (!existListener(l)) addListener(l);
	}


    public void runMailQueueService() {
    	messageMngr.addInfoMessage("mailQueueService Started...");

	    int getTop = 0;
	    boolean disableSend=false;
	    boolean onlyList=false;
	    boolean setDisabledSend=false;
	    boolean writeOnly = false;
	    int sendTimeOut=0;
	    int sleepTime=0;
//	    FIXME - Ho posto idJob2Run=1 per far entrare nel ciclo in modo indefinito nella gestione delle email.
	    int idJob2Run=1;

//	    messageMngr.enableSystemOut(true);
	    try {
	    	getTop=Integer.parseInt(PropertiesUtils.getInstance().readProperty("mail.emailBufferCount","5"));
	    	setDisabledSend=Boolean.parseBoolean(PropertiesUtils.getInstance().readProperty("mail.disableSend","false"));
	    	onlyList=Boolean.parseBoolean(PropertiesUtils.getInstance().readProperty("mail.sendOnlyList","false"));
	    	sendTimeOut=Integer.parseInt(PropertiesUtils.getInstance().readProperty("mail.sendTimeout","0"));
	    	sleepTime=Integer.parseInt(PropertiesUtils.getInstance().readProperty("mail.sleepTime","60"));
	    	
		} catch (Exception e) {
			messageMngr.addErrorMessage(e ,"runMailQueueService - Error on loading parameters...");
			setDisabledSend=false;
			getTop = 0;
			onlyList=false;
			sendTimeOut=0;
		}
	    
	    while (idJob2Run!=0)  {
	    	try {
				Thread.sleep(sleepTime*1000);
			} catch (InterruptedException e1) {
				messageMngr.addErrorMessage(e1, "runMailQueueService - Error on Thread Sleep...");
			}
	    	
		    List<EmailQueue> lstEmailToSend= emailQueueService.getEmailQueueToSend(getTop, onlyList);		    
		    for (EmailQueue emailQueue : lstEmailToSend) {
		    	messageMngr.addInfoMessage("runMailQueueService - Sending mail - Object:"+emailQueue.getSubject() + "  body:"+ emailQueue.getBody());	
		    	List<String> emails = new ArrayList<>();
		    	try {
		    		emails = Arrays.asList((emailQueue.getToEmail().trim()+(emailQueue.getToEmail().isEmpty()?"":";")+emailQueue.getCcEmail()).split(";"));
		    	} catch (Exception e) {
		    		messageMngr.addErrorMessage(e, "runMailQueueService - Error on loading mail recipients...");
		    	}
		    	writeOnly=(emailQueue.getSendStatus()==4);
		    	disableSend=setDisabledSend || writeOnly;
		    	if (!(emails==null || emails.isEmpty())) {
					MailMessage mail = new MailMessage(emailQueue.getEntityType()==1?true:false,emailQueue.getId(),emails.toArray(new String[0]), emailQueue.getSubject(), emailQueue.getBody(), true);
					emailQueue.setSendStatus(0);
					if ((emailQueue.getPdfRepBase64Str()=="{\"Status\":\"nok\"}") || (emailQueue.getPdfRepBase64Str()==null) || (emailQueue.getPdfRepBase64Str().equals(""))) {
				    	try {
				    		String sReportBase64= reportMaker(emailQueue);
					    	if (sReportBase64!="{\"Status\":\"nok\"}") {
					    		emailQueue.setPdfRepBase64Str(sReportBase64);
					    		emailQueue.setSendDate(DateUtils.getDate());
					    	} else {
					    		emailQueue.setPdfRepBase64Str(String.valueOf(null));
					    		emailQueue.setSendStatus(2);
					    		messageMngr.addErrorMessage(null, "runMailQueueService - Error on creating 64Base report: Status nok ");
					    	}	    		
				    	} catch(Exception e) {
				    		messageMngr.addErrorMessage(e, "runMailQueueService - Error on creating 64Base report ");
				    		emailQueue.setSendStatus(2);
				    	}					
					}
			    	try {
			    		// Invio mail
						if ((emailQueue.getPdfRepBase64Str()!="{\"Status\":\"nok\"}") && (emailQueue.getPdfRepBase64Str()!=null)) {
							Map<String, org.springframework.core.io.ByteArrayResource> attchs = new HashMap<String, org.springframework.core.io.ByteArrayResource>();
							ByteArrayResource bAr= new ByteArrayResource(Base64.getDecoder().decode(emailQueue.getPdfRepBase64Str()));
							attchs.put(emailQueue.getAttachFileName().replace("$", emailQueue.getId().toString()),bAr);
							mail.setAttchs(attchs);
						}	
					} catch (Exception e) {
						messageMngr.addErrorMessage(e, "runMailQueueService - Error on creating attached files from Base64 ");
						emailQueue.setSendStatus(2);
					}
			    	try {
			    		if (emailQueue.getSendStatus()!=2) {
			    			messageMngr.addWarningMessage("runMailQueueService - Attempt to send email...");
				    		if (!disableSend) {
				    			//Mando le mail ad eccezione di quelle che si trovano gia in stato 3 (sending) da meno di 2 minuti (c'è ancora in processo attivo sotto....)
				    			if (!((emailQueue.getSendStatus()==3) && (DateUtils.sendingProcessDelay(emailQueue.getSendDate())<=1L))) {
					    			emailSender.addMailToSend(mail);	
					    			messageMngr.addInfoMessage("runMailQueueService - Email sent success...");			    				
				    			}else {
				    				messageMngr.addInfoMessage("runMailQueueService - Email not sent because already in process...");
				    			}
				    		}else
				    		{ 
				    			messageMngr.addInfoMessage("runMailQueueService - Email EMULATED sent success...");
				    		}
			    		}
			    		emailQueue.setPriority(0);
			    		if (mail.isSyncroMail() || disableSend) {
			    			emailQueue.setSendStatus(1);
			    		}else {
			    			emailQueue.setSendStatus(3);
			    		}
					} catch (Exception e) {

//						if ((emailQueue.getSendStatus()==2) && (DateUtils.minuteDiff(emailQueue.getSendDate(), new Date() )>=2)){
						
						if ((DateUtils.minuteDiff(emailQueue.getSendDate(), new Date() )>=sendTimeOut) && (sendTimeOut>0)){
							if (emailQueue.getPdfRepBase64Str().length()>0) {
								emailQueue.setSendStatus(1);
								messageMngr.addErrorMessage(e, "runMailQueueService - Email not really sent, but signed as sent on timeout of "+sendTimeOut+" minutes...");								
							} else {
								emailQueue.setSendStatus(2);
								messageMngr.addErrorMessage(e, "runMailQueueService - Email not sent...");
							}
						} else {
							emailQueue.setSendStatus(2);
							messageMngr.addErrorMessage(e, "runMailQueueService - Email not sent...");							
						}
					} 
			    	try {
						emailQueue=emailQueueService.updateEmailQueue(emailQueue);
						messageMngr.addInfoMessage("runMailQueueService - Status email updated...");
					} catch(Exception e) {
						messageMngr.addErrorMessage(e, "runMailQueueService - Status email not updated...");
					}finally {
						for (InterfaceMailSent lstnr : listeners) {
							lstnr.onMailSent(emailQueue);
						}
						messageMngr.addInfoMessage("runMailQueueService - Called all onMailSent...");
					}
		    	}		
			}	
		    messageMngr.addInfoMessage("mailQueueService End...");	
//		    idJob2Run=0;
	    }
    }


	@Override
	public boolean emailStatus(MailMessage mail) {
		boolean returnStatusOk=true;
		messageMngr.addInfoMessage("emailStatus Started...");
		messageMngr.addInfoMessage("Da MailQueueService: "+mail.toString());
		try {
			EmailQueue emailQueue=emailQueueService.getEmailQueueById(mail.getId());
			
			emailQueue.setSendDate(DateUtils.getDate());
			switch (mail.getEmailStatus()) {
			case sent:
				emailQueue.setSendStatus(1);
				break;
			case sending:
				emailQueue.setSendStatus(3);
				break;
			case notsent:
				emailQueue.setSendStatus(2);
				break;
			default:
				emailQueue.setSendStatus(2);
				break;
			}
			emailQueue=emailQueueService.updateEmailQueue(emailQueue);
			
			messageMngr.addInfoMessage("emailStatus - Status email updated...: "+emailQueue.getId()+" - "+emailQueue.getSendStatus());
			
		} catch (Exception e) {
			returnStatusOk=false;
			messageMngr.addErrorMessage(e, "emailStatus - Status email NOT updated because id not found...");
		}
		messageMngr.addInfoMessage("emailStatus End...");
		return returnStatusOk;
	}

    private String reportMaker(EmailQueue email) {
    	
    	messageMngr.addInfoMessage("reportMaker Started..."); 
 		String response="{\"Status\":\"ok\"}";
 		//Recupero l'oggetto Episode
 		if (email!=null) {
 			response= makeReportEpisode(email);	
 			if (response==null) {
 				response="{\"Status\":\"nok\"}";
 				messageMngr.addErrorMessage(null, "reportMaker ERROR - MakeReportEpisode return null response... ");
 			}
 		} else {
 			response="{\"Status\":\"nok\"}";
 			messageMngr.addErrorMessage(null, "reportMaker ERROR - passed null email... ");
 		}
 		messageMngr.addInfoMessage("reportMaker End...");
 		return response;
 	}
    
	private String makeReportEpisode(EmailQueue emailToSend) {
		
		String sReturn=null;
		String reportDescription=null;
		String reportFileName=null;
		Boolean bContinue=false;
		
		messageMngr.addInfoMessage("MakeReportEpisode Started...");
		Collection<Episode> beansEpisode = new ArrayList<Episode>();
		Collection<SpectatorEpisode> beansSpectatorEpisode = new ArrayList<SpectatorEpisode>();
		
		switch (emailToSend.getEntityType()) {
		case 1:
			reportDescription="Lista Spettatori";
			reportFileName= PropertiesUtils.getInstance().readProperty("report.listaSpettatori").trim();
			Episode episodeToInvite = episodeService.getEpisodeById(emailToSend.getEntityID());
			episodeToInvite.setSpectators(episodeService.getSpectatorEpisodeByEpisodeId(episodeToInvite.getId()));
			bContinue=(episodeToInvite!=null);  
            // Dati da visualizzare (un oggetto per ogni areaDetail in iReport)
			beansEpisode.add(episodeToInvite);
			break;
		case 2:
			reportDescription="Invito Spettatore";
			reportFileName= PropertiesUtils.getInstance().readProperty("report.invitoSpettatore").trim();
			SpectatorEpisode spectatorEpisodeToInvite = spectatorEpisodeService.getSpectatorEpisodeById(emailToSend.getEntityID());
			bContinue=(spectatorEpisodeToInvite!=null);  
            // Dati da visualizzare (un oggetto per ogni areaDetail in iReport)
			beansSpectatorEpisode.add(spectatorEpisodeToInvite);
			break;
		default:
			break;
		}

		if (bContinue)  {
			try {
				System.out.println("Genero pdf...");
				JasperReportDetails det = new JasperReportDetails();
	            
				// Nome report
	            det.setDescription(reportDescription);
	            
	            // Path report
	            det.setMasterReportUrl(File.separator+"WEB-INF"+File.separator+"reports"+File.separator+"spectator"+File.separator+reportFileName+".jasper");
	            
	            // Subreports url
	            Properties srUrls = new Properties();
	            //srUrls.put("SUBREPORT_URL", File.separator+"WEB-INF"+File.separator+"reports"+File.separator+"invite"+File.separator+"subRep.jasper");
	            
	            det.setSubReportUrls(srUrls);
	            
	            // Parametri da passare al report
	            Map<String, Object> reportParameters = new HashMap<String, Object>();
	            
	            reportParameters.put("REPORT_RESOURCES_PATH", getAbsolutePath()+ File.separator+"WEB-INF"+File.separator+"reports"+File.separator+"spectator"+File.separator);	            
//	            reportParameters.put("SPECTATOR", episodeToInvite);
//	            reportParameters.put("SIGN", new java.io.ByteArrayInputStream(redrawSignature(extractSignature(spectatorToEdit.getSign()))));
	            
//	            if (spectatorToEdit.getSpectator().getDocumentPath()!=null && !spectatorToEdit.getSpectator().getDocumentPath().equals(""))
//	            	reportParameters.put("ID_DOCUMENT", new java.io.ByteArrayInputStream(FileUtils.getBytesFromFile(filesDir+"/"+spectatorToEdit.getSpectator().getDocumentPath())));
//	            else reportParameters.put("ID_DOCUMENT", null);
//	            if (doc!=null)
//	            	reportParameters.put("ID_DOCUMENT", new java.io.ByteArrayInputStream(doc));
//	            else reportParameters.put("ID_DOCUMENT", null);	            
	            
	            det.setReportParameters(reportParameters);

	            Map<String, Object> reportParams = new HashMap<String, Object>();
	            reportParams.put("REPORT_TITLE", reportDescription);
	            ByteArrayOutputStream baos=null;
	            switch (emailToSend.getEntityType()) {
				case 1:
					baos= jasperReportsFactory.complieReportAsPdfByteArrayOutputStream(det, reportParams, beansEpisode);
					break;
				case 2:
					baos= jasperReportsFactory.complieReportAsPdfByteArrayOutputStream(det, reportParams, beansSpectatorEpisode);
					break;
				default:
					break;
				} 
	            
	            if (baos!=null) {
		            // Solo per test !!!!!!!!!!!!!!!!
		            //FileUtils.writeToFile("D:\\"+reportFileName+".pdf", new ByteArrayInputStream(baos.toByteArray()));
		            
		            sReturn=java.util.Base64.getEncoder().encodeToString(baos.toByteArray());
		            if (sReturn!=null) {
//			            emailToSend.setSendDate(java.sql.Date.valueOf(LocalDate.now()));
//			            emailToSend.setPdfRepBase64Str(sReturn);
//			            emailToSend= emailQueueService.updateEmailQueue(emailToSend);	 
//			            sReturn="{\"Status\":\"ok\"}";
		            } else
		            {
			            sReturn="{\"Status\":\"nok\"}";
			            messageMngr.addErrorMessage(null,"Error - MakeReportEpisode - Return Base64.Encoder null stream...");
		            }
      	
	            } else {
	            	sReturn="{\"Status\":\"nok\"}";
	            	messageMngr.addErrorMessage(null,"Error - MakeReportEpisode - Return ReportAsPdf null stream...");
	            }
	            
			} catch (Exception e2) {
				sReturn="{\"Status\":\"nok\"}";
				messageMngr.addErrorMessage(e2, "Error MakeReportEpisode");
			}	
		} else {
			messageMngr.addWarningMessage("MakeReportEpisode - No spectatorEpisode to Invite.... ");
		}
		messageMngr.addInfoMessage("MakeReportEpisode End...");
		return sReturn;
	}

	//---------------------------------------------------------------------------------------------------------------------	
	  private String getAbsolutePath() throws UnsupportedEncodingException {

			String path = this.getClass().getClassLoader().getResource("").getPath();
			String fullPath = URLDecoder.decode(path, "UTF-8");
			String pathArr[] = fullPath.split("/WEB-INF/classes/");
			System.out.println(fullPath);
			System.out.println(pathArr[0]);
			fullPath = pathArr[0];
			String reponsePath = "";

			//to read a file from webcontent
			//reponsePath = new File(fullPath).getPath() + File.separatorChar + "newfile.txt";
			reponsePath = new File(fullPath).getPath();
			System.out.println("getAbsolutePath:" + reponsePath);
			return reponsePath;

	  }


}

