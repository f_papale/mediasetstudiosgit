package com.crismasecurity.mediasetStudios.core.utils.timers;

import com.crismasecurity.mediasetStudios.core.entity.EmailQueue;

public interface InterfaceMailSent {

	public void onMailSent(EmailQueue emailQueue);
}
