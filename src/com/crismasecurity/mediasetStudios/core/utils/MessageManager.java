package com.crismasecurity.mediasetStudios.core.utils;

import org.apache.logging.log4j.LogManager; 
import org.apache.logging.log4j.Logger;

//import org.apache.log4j.Logger;


public class MessageManager {
	
	private Class<?> wkclass;
	private Logger logger;
	private boolean enableSystemOut = false;

	public MessageManager(Class<?> wrkclass) {
		super();
		wkclass=wrkclass;
//		logger= Logger.getLogger(wkclass.getClass());
		logger= LogManager.getLogger(wkclass.getClass());
		enableSystemOut = Boolean.parseBoolean(PropertiesUtils.getInstance().readProperty("schedule.debug","false"));
	}

	public  void addErrorMessage(Exception e, String message) {
		
		 if (enableSystemOut) System.out.println(wkclass.getName() + "-ERROR: "+message);
		 if (e!=null) {
			 e.printStackTrace();
			 logger.error(message, e);
		 }else {
			 logger.error(message);
		 }
	}
	
	public  void addErrorMessage(Throwable  e, String message) {
		
		if (enableSystemOut) System.out.println(wkclass.getName()+ " -ERROR: "+message);
		 if (e!=null) {
			 e.printStackTrace();
			 logger.error(message, e);
		 }else {
			 logger.error(message);
		 }
	}
	
	public  void addErrorMessage(String message) {
		if (enableSystemOut) System.out.println(wkclass.getName()+ " -ERROR: "+message);
		logger.error(message);
	}
	
	public  void addWarningMessage(String message) {
		if (enableSystemOut) System.out.println(wkclass.getName()+" -WARN: "+message);
		logger.warn(message);
	}
	
	public  void addWarningMessage(Exception e, String message) {
		
		 if (enableSystemOut) System.out.println(wkclass.getName() + "-WARN: "+message);
		 if (e!=null) {
			 logger.warn(message, e);
		 }else {
			 logger.warn(message);
		 }
	}
	
	public  void addInfoMessage(String message) {
		if (enableSystemOut) {
			System.out.println(wkclass.getName()+" -INFO: "+message);
			logger.info(message);
		}
	}
	
	public  void addDebugMessage(String message) {
		if (enableSystemOut) {
			System.out.println(wkclass.getName()+" -INFO: "+message);
			logger.debug(message);
		}
	}

	public boolean isEnabledSystemOut() {
		return enableSystemOut;
	}

	public void enableSystemOut(boolean write2Console) {
		this.enableSystemOut = write2Console;
	}
}
