package com.crismasecurity.mediasetStudios.core.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.joda.time.DateTime;


public class DateUtils {
   
	public static Date getDate() {
		Date date = new Date();
		Instant instant = date.toInstant();
		return Date.from(instant);    	
    }
	
	public static Date getDate(String strDate) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Date retDate=null;
		try {
			retDate = formatter.parse(strDate.trim());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
		}
		return retDate;
	}
	

	public static Date getDate(String strDate, String format) {
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		Date retDate=null;
		try {
			retDate = formatter.parse(strDate.trim());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
		}
		return retDate;
	}
	
    
	public static DateTime getDateTime(String strDate) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Date retDate=null;
		DateTime retDateTime=null;
		try {
			retDate = formatter.parse(strDate.trim());
			retDateTime = new DateTime(retDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
		}
		return retDateTime;
	}
    
	public static DateTime getDateTime(String strDate, String format) {
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		Date retDate=null;
		DateTime retDateTime=null;
		try {
			retDate = formatter.parse(strDate.trim());
			retDateTime = new DateTime(retDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
		}
		return retDateTime;
	}
	
    public static long minuteDiff(Date d1, Date d2) {
    	long diff = d2.getTime() - d1.getTime();
//    	long diffSeconds = diff / 1000;         
    	long diffMinutes = diff / (60 * 1000);         
//    	long diffHours = diff / (60 * 60 * 1000);
    	return diffMinutes; 
    }
    
    public static long HoursDiff(Date d1, Date d2) {
    	long diff = d2.getTime() - d1.getTime();
//    	long diffSeconds = diff / 1000;         
//    	long diffMinutes = diff / (60 * 1000);         
    	long diffHours = diff / (60 * 60 * 1000);
    	return diffHours; 
    }
    
    public static long HoursElapsedFromNow(Date d2) {
    	long diff = getDate().getTime() - d2.getTime();
//    	long diffSeconds = diff / 1000;         
//    	long diffMinutes = diff / (60 * 1000);         
    	long diffHours = diff / (60 * 60 * 1000);
    	return diffHours; 
    }
    
    public static long sendingProcessDelay(Date d1) {
    	return minuteDiff(getDate(),d1);
    }
    
    public static  Date addtimeToDate(Date dRef, int hour, int minute) {		
		Calendar cal = Calendar.getInstance();
		cal.setTime(dRef);
		cal.set(Calendar.HOUR_OF_DAY,hour);
		cal.set(Calendar.MINUTE,minute);
		return cal.getTime();
	}
    
    public static int getDayDiff(Date date1, Date date2) {
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();

        c1.setTime(date1);
        c2.setTime(date2);
        int diffDay = 0;

        if (c1.before(c2)) {
          diffDay = countDiffDay(c1, c2);
        } else {
          diffDay = countDiffDay(c2, c1);
        }

        return diffDay;
      }
    
    public static int getDayDiffNotAbs(Date date1, Date date2) {
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();

        c1.setTime(date1);
        c2.setTime(date2);
        int diffDay = 0;

        if (c1.before(c2)) {
        	diffDay = countRealDiffDay(c1, c2);
        } else {
        	diffDay = - countRealDiffDay(c2, c1);
        }

        return diffDay;
      }
    
    private static int countDiffDay(Calendar c1, Calendar c2) {
        int returnInt = 0;
        while (!c1.after(c2)) {
          c1.add(Calendar.DAY_OF_MONTH, 1);
          returnInt++;
        }

        if (returnInt > 0) {
          returnInt = returnInt - 1;
        }

        return (returnInt);
      }
    
    private static int countRealDiffDay(Calendar c1, Calendar c2) {
        int returnInt = 0;
        while (!c1.after(c2)) {
          c1.add(Calendar.DAY_OF_MONTH, 1);
          returnInt++;
        }

//        if (returnInt > 0) {
//          returnInt = returnInt - 1;
//        }

        return (returnInt);
      }
    
    public static Date getMaxDate(Date startDate, int howYears) {
		Calendar c = Calendar.getInstance();
		c.setTime(startDate);
		c.add(Calendar.YEAR, howYears);
		return c.getTime();
	}
	
    public static DayOfWeek getDayOfWeek(Date currDate) {

    	Calendar c = Calendar.getInstance();
    	c.setTime(currDate);
    	int daayOfWeek = (c.get(Calendar.DAY_OF_WEEK)==1?7:c.get(Calendar.DAY_OF_WEEK)-1);
    	return DayOfWeek.of(daayOfWeek)		;
//		LocalDate localDate =  currDate
//		return localDate.getDayOfWeek();		
	}
	
	
    public static Date addDays(Date date, int days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, days);
				
		return cal.getTime();
	}
	
	
	
    public static Date addWeeks(Date date, int weeks) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.WEEK_OF_YEAR, weeks);
				
		return cal.getTime();
	}
    
    public static Date addMonths(Date date, int months) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, months);
				
		return cal.getTime();
	}
	
    public static Date addYears(Date date, int years) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.YEAR, years);
				
		return cal.getTime();
	}
    
    public static Date getDateWithoutTime(Date date){
    	Calendar cal = Calendar.getInstance();
    	cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY,0);
		cal.set(Calendar.MINUTE,0);
		cal.set(Calendar.SECOND,0);
		cal.set(Calendar.MILLISECOND,0);
    	return cal.getTime();
    }
}

