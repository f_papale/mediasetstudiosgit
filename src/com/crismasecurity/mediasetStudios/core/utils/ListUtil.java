package com.crismasecurity.mediasetStudios.core.utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ListUtil {
	private static ListUtil instance = null;
	
	protected ListUtil() {
	      // Exists only to defeat instantiation.
	}
	
	public static ListUtil getInstance() {
		if(instance == null) {
			instance = new ListUtil();
		}
		return instance;
	}
	
   public static <T> List<T> union(List<T> list1, List<T> list2) {
       Set<T> set = new HashSet<T>();

       set.addAll(list1);
       set.addAll(list2);

       return new ArrayList<T>(set);
   }
   
   public static <T> List<T> intersection(List<T> list1, List<T> list2) {
       List<T> list = new ArrayList<T>();

       for (T t : list1) {
           if(list2.contains(t)) {
               list.add(t);
           }
       }

       return list;
   }   
   
   public static <T> List<T> subtrac(List<T> list1, List<T> list2) {
       List<T> list = new ArrayList<T>();
       for (T t : list1) {
           if(!(list2.contains(t))) {
               list.add(t);
           }
       }
       return list;
   }   
}
