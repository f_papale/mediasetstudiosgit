package com.crismasecurity.mediasetStudios.core.utils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;

public class HttpUtils {

	private static final String USER_AGENT = "Mozilla/5.0";

	public static void main(String[] args) throws Exception {

		//System.out.println("Testing 1 - Send Http GET request");
		HttpUtils.sendGet("https://api.airbnb.com/v1/authorize", null, null);

		System.out.println("\nTesting 2 - Send Http POST request");
		String pars = "client_id=3092nxybyb0otqw18e8nh5nty&locale=en-US&currency=USD&grant_type=password&password=gc512016&username=info@stellamarinaresort.com";
		HttpUtils.sendPost("https://api.airbnb.com/v1/authorize", pars);

	}

	// HTTP GET request
	public static String sendGet(String url, Map<String,String> headerParams, String bodyParameters) throws Exception {

		
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");

		//add request header
		con.setRequestProperty("User-Agent", HttpUtils.USER_AGENT);
		
		if (headerParams!=null) {
			Set<String> parNames = headerParams.keySet();
			for (String parName : parNames) {
				con.setRequestProperty(parName, headerParams.get(parName));
			}
		}

		if (bodyParameters!=null) {
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(bodyParameters);
			wr.flush();
			wr.close();
		}
		
		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		System.out.println(response.toString());
		return responseCode + "_" + response.toString();
	}

	/**
	 *  HTTP POST request
	 * @param url
	 * @param urlParameters --> example sn=C02G8416DRJM&cn=&locale=&caller=&num=12345
	 * @return response ---><response_code>_<response_txt>
	 * @throws Exception
	 */
	public static String sendPost(String url, String urlParameters) throws Exception {

		URL obj = new URL(url);
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

		//add reuqest header
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", HttpUtils.USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		System.out.println(response.toString());
		return responseCode + "_" + response.toString();
	}
}
