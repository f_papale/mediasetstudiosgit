package com.crismasecurity.mediasetStudios.core.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class TextUtil {

	private static final Log log = LogFactory.getLog(TextUtil.class);
	
    
    public static String readFileIntoString(File file)
    {
        StringBuilder contentBuilder = new StringBuilder();
        try (Stream<String> stream = Files.lines(Paths.get(file.toURI())))
        {
            stream.forEach(s -> contentBuilder.append(s).append("\n"));
        }
        catch (IOException e)
        {
            System.out.println("Error reading " + file.getAbsolutePath());
        }
//      log.info("readFileIntoString: " + contentBuilder.toString());
        return contentBuilder.toString();
    }
    
    public static String cleanTextContent(String text)
    {
        // strips off all non-ASCII characters
    	String oldText= text;
        text = text.replaceAll("[^\\x00-\\x7F]", "");
 
        // erases all the ASCII control characters
        text = text.replaceAll("[\\p{Cntrl}&&[^\r\n\t]]", "");
         
        // removes non-printable characters from Unicode 
        text = text.replaceAll("\\p{C}", "").trim();
//        log.info("cleanTextContent: " + oldText+ " --> "+ text);
        return text;
    }
    
    
    public static String rearrangeEmailList(String emails) {
    	String[] emailArray;
    	String result="";
    	try {
        	emailArray = emails.split(";");
        	for (int i = 0; i < emailArray.length; i++) {
    			if (validateMail(emailArray[i])) {
    				if (result.isEmpty())
    					result=emailArray[i];
    				else
    					result=String.join(";", result, emailArray[i]);
    			}
    		}
		} catch (Exception e) {
			result="";
		}
    	return result;
    }
    
    private static boolean validateMail(String mail)
    {
	    if (mail == null)
	    {
	    	return false;
	    }
	
	    Pattern p = Pattern.compile(".+@.+\\.[a-z]+", Pattern.CASE_INSENSITIVE);
	    Matcher m = p.matcher(mail);
	    boolean matchFound = m.matches();
	
	    //Condizioni più restrittive rispetto alle precedenti
	    String  expressionPlus="^[\\w\\-]([\\.\\w])+[\\w]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
	    Pattern pPlus = Pattern.compile(expressionPlus, Pattern.CASE_INSENSITIVE);
	    Matcher mPlus = pPlus.matcher(mail);
	    boolean matchFoundPlus = mPlus.matches();
	             
	    return matchFound && matchFoundPlus;   
    }
}
