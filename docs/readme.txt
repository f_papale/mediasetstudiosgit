# README #

Per far funzionare l'applicativo è necessario creare il DB e dare i permessi di lettura.
Questa versione utilizza MSSql server e per l'accesso, l'autenticazione sqlserver integrata (usa l'account utilizzato per far girare tomcat).

1) Creare il DB: nella cartella docs si trova il file script.sql che crea il db e riempie le tabelle utilizzate per l'accesso all'applicativo.

2)Per l'autenticazione integrata: copiare il file sqljdbc_auth.dll nella cartella windows/System32
  Se invece si vuole abilitare l'accesso con username e password modificare il file applicationRepository.xml 
  e in application.properties inserire lo username e password corrette per l'accesso al db
  
3) Deployare in Tomcat

Per accedere all'applicazione utilizzare: http://127.0.0.1:8080/MediasetStudios
username: admin
password: admin



Autenticazione sqlserver integrata (usa l'account applicativo):
copiare il file sqljdbc_auth.dll nella cartella windows/System32

Se si vuole abilitare l'accesso con username e password modificare il file 
eclipse-workspace\MediasetStudios\WebContent\WEB-INF\templates\applicationRepository.xml

eclipse-workspace\MediasetStudios\src\configuration.properties						E' il file contenente le variabili d'ambiente

eclipse-workspace\MediasetStudios\WebContent\WEB-INF\templates\baseTemplate.xhtml	E' il file in cui è definita la voce di menu
eclipse-workspace\MediasetStudios\WebContent\WEB-INF\applicationSecurity.xml		E' dove viene definita la sicurezza per subdirectory
eclipse-workspace\MediasetStudios\WebContent\WEB-INF\faces-config.xml				E' dove sono definite le navigation roules specificate nelle Actions
eclipse-workspace\MediasetStudios\src\resources\messages_it.properties				Localizzazione Italiano
eclipse-workspace\MediasetStudios\src\resources\messages_en.properties				Localiazzazione Inglese



-------------------------------------------------------------------------------------------------------------------------------ù

Gesione della Sicurezza

Innanizi tutto con il NavigationBean 

\web\main\NavigationBean

Contiene le primitive per la gestione della security RBAC (Role Bases Access Control)
	
	- haveUserRole(String role)
	- checkIfHaveToChangePage() () Ti fa la redirect alla pagina iniziale corretta a seconda del ruolo (da personalizzare)
	  In questo metodo ti consente di impostare la pagina in cui reidirizzare l'utente loggato a seconda del ruolo che ha. Poi l'accesso o meno al folder o al 
	  singolo file specificato dipende da quanto è indicato in un altro file che si chiama ApplicationSecurity.xml

	


WebContent\WEB-INF\applicationSecurity.xml
Concede gli accessi alle cartelle/file a seconda del ruolo a cui appartiene l'utente
	Tutto quello che sta nella cartella secured (escuso le sottocartelle!!) richiede l'autenticazione.
	ATT!! indicare i permessi specifici sulle sottocartelle -->
		
	<security:intercept-url pattern="/secured/*" access="isFullyAuthenticated()"/>

	<!-- <security:intercept-url pattern="/secured/commons/*" access="permitAll"/>
	<security:intercept-url pattern="/secured/pdfManagement/*" access="permitAll"/>
	<security:intercept-url pattern="/secured/csm/*" access="permitAll"/>  -->

	<security:intercept-url pattern="/secured/access-admin/*" access="hasAnyRole('ROLE_ADMIN')"/>


-----------------------------
Visibilità dei menu e delle sezioni in una generica pagina
 
A livello di XHTML è possibile utilizzare i tag <sec:authorize>
Il file basetemplate.xhtml viene utilizzato per abilitare o meno l'accesso alle voci di menu in accordo con il ruolo dell'utente

Andare su WEB-INF\templates\baseTemplate 
e abilitare i ruoli che hanno veisibilità del menu

    <sec:authorize ifAnyGranted="ROLE_ADMIN", ROLE_TEST>
		<p:tab title="Reception">
			<p:commandButton styleClass="ui-aladino_menu-button" value="#{msgs['reception']}" action="reception" icon="fa fa-fw fa-empire" >
		 	</p:commandButton>
			<p:commandButton styleClass="ui-aladino_menu-button" value="#{msgs['barcodeReader']}" action="barcodeReader" icon="fa fa-fw fa-barcode" >
		 	</p:commandButton>
		 	<p:commandButton styleClass="ui-aladino_menu-button" value="#{msgs['sign']}" action="sign" icon="fa fa-fw fa-sign-in" >
		 	</p:commandButton>
        </p:tab>        
    </sec:authorize>	


------------------------------------------------------------------------------------------------------------

Connessione JDBC a SQL Server
jdbc:sqlserver://;serverName=172.16.0.109\sql2014;databaseName=mediasetStudios


--- Visualizzazione messaggi di errore
Assicurarsi che sia valorizzato nel seguente tag la proprietà autoUpdate="true"
nel baseTemplate.xhtml
		<p:growl 
			globalOnly="true" 
			showDetail="false" 
			sticky="true" 
			autoUpdate="true"
		/>
		