USE [master]
GO
/****** Object:  Database [mediasetStudios]    Script Date: 22/03/2018 17:39:16 ******/
CREATE DATABASE [mediasetStudios] ON  PRIMARY 
( NAME = N'mediasetStudios', FILENAME = N'D:\Sviluppo\eclipse\Data\DB\mediasetStudios.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'mediasetStudios_log', FILENAME = N'D:\Sviluppo\eclipse\Data\DB\mediasetStudios_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [mediasetStudios].[dbo].[sp_fulltext_database] @action = 'disable'
end
GO
ALTER DATABASE [mediasetStudios] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [mediasetStudios] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [mediasetStudios] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [mediasetStudios] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [mediasetStudios] SET ARITHABORT OFF 
GO
ALTER DATABASE [mediasetStudios] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [mediasetStudios] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [mediasetStudios] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [mediasetStudios] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [mediasetStudios] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [mediasetStudios] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [mediasetStudios] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [mediasetStudios] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [mediasetStudios] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [mediasetStudios] SET  DISABLE_BROKER 
GO
ALTER DATABASE [mediasetStudios] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [mediasetStudios] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [mediasetStudios] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [mediasetStudios] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [mediasetStudios] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [mediasetStudios] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [mediasetStudios] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [mediasetStudios] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [mediasetStudios] SET  MULTI_USER 
GO
ALTER DATABASE [mediasetStudios] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [mediasetStudios] SET DB_CHAINING OFF 
GO
USE [mediasetStudios]
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [mediasetStudios]
GO
/****** Object:  Table [dbo].[mediastudios_tests]    Script Date: 22/03/2018 17:39:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mediastudios_tests](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[date] [datetime] NULL,
	[test] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[s_groups]    Script Date: 22/03/2018 17:39:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s_groups](
	[CODI_GRUPPO] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[DESC_NOME] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[CODI_GRUPPO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[s_groups_roles]    Script Date: 22/03/2018 17:39:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s_groups_roles](
	[CODI_GRUPPO] [numeric](19, 0) NOT NULL,
	[CODI_ROLE] [numeric](19, 0) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[s_role_interestedarea]    Script Date: 22/03/2018 17:39:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s_role_interestedarea](
	[Role_CODI_ROLE] [numeric](19, 0) NOT NULL,
	[interestedArea] [varchar](255) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[s_roles]    Script Date: 22/03/2018 17:39:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s_roles](
	[CODI_ROLE] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[DESC_DESCRIZIONE] [varchar](255) NULL,
	[DESC_DESCRIZIONE_BREVE] [varchar](255) NULL,
	[DESC_NOME] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[CODI_ROLE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[s_users]    Script Date: 22/03/2018 17:39:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s_users](
	[CODI_USER] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[DESC_NOME] [varchar](255) NULL,
	[DESC_COGN] [varchar](255) NULL,
	[DESC_LOGN] [varchar](255) NULL,
	[DESC_PASW] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[CODI_USER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[s_users_groups]    Script Date: 22/03/2018 17:39:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s_users_groups](
	[CODI_USER] [numeric](19, 0) NOT NULL,
	[CODI_GRUPPO] [numeric](19, 0) NOT NULL
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[mediastudios_tests] ON 

INSERT [dbo].[mediastudios_tests] ([id], [date], [test]) VALUES (CAST(1 AS Numeric(19, 0)), CAST(N'2018-03-22T01:00:00.000' AS DateTime), N'aaaa')
INSERT [dbo].[mediastudios_tests] ([id], [date], [test]) VALUES (CAST(2 AS Numeric(19, 0)), CAST(N'2018-03-24T01:00:00.000' AS DateTime), N'bbb')
SET IDENTITY_INSERT [dbo].[mediastudios_tests] OFF
SET IDENTITY_INSERT [dbo].[s_groups] ON 

INSERT [dbo].[s_groups] ([CODI_GRUPPO], [DESC_NOME]) VALUES (CAST(1 AS Numeric(19, 0)), N'Gruppo Amministratore')
SET IDENTITY_INSERT [dbo].[s_groups] OFF
INSERT [dbo].[s_groups_roles] ([CODI_GRUPPO], [CODI_ROLE]) VALUES (CAST(1 AS Numeric(19, 0)), CAST(1 AS Numeric(19, 0)))
INSERT [dbo].[s_role_interestedarea] ([Role_CODI_ROLE], [interestedArea]) VALUES (CAST(1 AS Numeric(19, 0)), N'GESTIONE')
SET IDENTITY_INSERT [dbo].[s_roles] ON 

INSERT [dbo].[s_roles] ([CODI_ROLE], [DESC_DESCRIZIONE], [DESC_DESCRIZIONE_BREVE], [DESC_NOME]) VALUES (CAST(1 AS Numeric(19, 0)), N'Allows full controll on whole system ', N'Administrator', N'ROLE_ADMIN')
SET IDENTITY_INSERT [dbo].[s_roles] OFF
SET IDENTITY_INSERT [dbo].[s_users] ON 

INSERT [dbo].[s_users] ([CODI_USER], [DESC_NOME], [DESC_COGN], [DESC_LOGN], [DESC_PASW]) VALUES (CAST(1 AS Numeric(19, 0)), N'admin', N'admin', N'admin', N'd033e22ae348aeb5660fc2140aec35850c4da997')
SET IDENTITY_INSERT [dbo].[s_users] OFF
INSERT [dbo].[s_users_groups] ([CODI_USER], [CODI_GRUPPO]) VALUES (CAST(1 AS Numeric(19, 0)), CAST(1 AS Numeric(19, 0)))
ALTER TABLE [dbo].[s_groups_roles]  WITH CHECK ADD  CONSTRAINT [FK_56x1vmca4fwtp7lqfxujrbr5t] FOREIGN KEY([CODI_ROLE])
REFERENCES [dbo].[s_roles] ([CODI_ROLE])
GO
ALTER TABLE [dbo].[s_groups_roles] CHECK CONSTRAINT [FK_56x1vmca4fwtp7lqfxujrbr5t]
GO
ALTER TABLE [dbo].[s_groups_roles]  WITH CHECK ADD  CONSTRAINT [FK_n7fmlrainiqbgpdtnhsuf9vqk] FOREIGN KEY([CODI_GRUPPO])
REFERENCES [dbo].[s_groups] ([CODI_GRUPPO])
GO
ALTER TABLE [dbo].[s_groups_roles] CHECK CONSTRAINT [FK_n7fmlrainiqbgpdtnhsuf9vqk]
GO
ALTER TABLE [dbo].[s_role_interestedarea]  WITH CHECK ADD  CONSTRAINT [FK_s2840y515500hcanjtvuswljy] FOREIGN KEY([Role_CODI_ROLE])
REFERENCES [dbo].[s_roles] ([CODI_ROLE])
GO
ALTER TABLE [dbo].[s_role_interestedarea] CHECK CONSTRAINT [FK_s2840y515500hcanjtvuswljy]
GO
ALTER TABLE [dbo].[s_users_groups]  WITH CHECK ADD  CONSTRAINT [FK_3cr5tt58jpvjn4aik3vutxtx5] FOREIGN KEY([CODI_GRUPPO])
REFERENCES [dbo].[s_groups] ([CODI_GRUPPO])
GO
ALTER TABLE [dbo].[s_users_groups] CHECK CONSTRAINT [FK_3cr5tt58jpvjn4aik3vutxtx5]
GO
ALTER TABLE [dbo].[s_users_groups]  WITH CHECK ADD  CONSTRAINT [FK_n7250370mqcpr5gywa9eaacb] FOREIGN KEY([CODI_USER])
REFERENCES [dbo].[s_users] ([CODI_USER])
GO
ALTER TABLE [dbo].[s_users_groups] CHECK CONSTRAINT [FK_n7250370mqcpr5gywa9eaacb]
GO
USE [master]
GO
ALTER DATABASE [mediasetStudios] SET  READ_WRITE 
GO
