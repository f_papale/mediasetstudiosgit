# README #

Per far funzionare l'applicativo è necessario creare il DB e dare i permessi di lettura.
Questa versione utilizza MSSql server e per l'accesso, l'autenticazione sqlserver integrata (usa l'account utilizzato per far girare tomcat).

1) Creare il DB: nella cartella docs si trova il file script.sql che crea il db e riempie le tabelle utilizzate per l'accesso all'applicativo.

2)Per l'autenticazione integrata: copiare il file sqljdbc_auth.dll nella cartella windows/System32
  Se invece si vuole abilitare l'accesso con username e password modificare il file applicationRepository.xml e in application.properties inserire lo username e password corrette per l'accesso al db
  
3) Deployare in Tomcat

Per accedere all'applicazione utilizzare: http://127.0.0.1:8080/MediasetStudios
username: admin
password: admin
